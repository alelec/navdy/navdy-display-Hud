.class public Lcom/navdy/hud/app/util/ScreenOrientation;
.super Ljava/lang/Object;
.source "ScreenOrientation.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/ScreenOrientation;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/ScreenOrientation;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentOrientation(Landroid/app/Activity;)I
    .locals 7
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 27
    .local v3, "rotation":I
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 28
    .local v0, "dm":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 29
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 30
    .local v4, "width":I
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 33
    .local v1, "height":I
    if-eqz v3, :cond_0

    const/4 v5, 0x2

    if-ne v3, v5, :cond_1

    :cond_0
    if-gt v1, v4, :cond_3

    :cond_1
    const/4 v5, 0x1

    if-eq v3, v5, :cond_2

    const/4 v5, 0x3

    if-ne v3, v5, :cond_4

    :cond_2
    if-le v4, v1, :cond_4

    .line 37
    :cond_3
    packed-switch v3, :pswitch_data_0

    .line 53
    sget-object v5, Lcom/navdy/hud/app/util/ScreenOrientation;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unknown screen orientation. Defaulting to portrait."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 55
    const/4 v2, 0x1

    .line 84
    .local v2, "orientation":I
    :goto_0
    return v2

    .line 39
    .end local v2    # "orientation":I
    :pswitch_0
    const/4 v2, 0x1

    .line 40
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 42
    .end local v2    # "orientation":I
    :pswitch_1
    const/4 v2, 0x0

    .line 43
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 45
    .end local v2    # "orientation":I
    :pswitch_2
    const/16 v2, 0x9

    .line 47
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 49
    .end local v2    # "orientation":I
    :pswitch_3
    const/16 v2, 0x8

    .line 51
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 62
    .end local v2    # "orientation":I
    :cond_4
    packed-switch v3, :pswitch_data_1

    .line 78
    sget-object v5, Lcom/navdy/hud/app/util/ScreenOrientation;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unknown screen orientation. Defaulting to landscape."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 79
    const/4 v2, 0x0

    .restart local v2    # "orientation":I
    goto :goto_0

    .line 64
    .end local v2    # "orientation":I
    :pswitch_4
    const/4 v2, 0x0

    .line 65
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 67
    .end local v2    # "orientation":I
    :pswitch_5
    const/4 v2, 0x1

    .line 68
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 70
    .end local v2    # "orientation":I
    :pswitch_6
    const/16 v2, 0x8

    .line 72
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 74
    .end local v2    # "orientation":I
    :pswitch_7
    const/16 v2, 0x9

    .line 76
    .restart local v2    # "orientation":I
    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 62
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static isPortrait(I)Z
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    const/4 v0, 0x1

    .line 21
    if-eq p0, v0, :cond_0

    const/16 v1, 0x9

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPortrait(Landroid/app/Activity;)Z
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 17
    invoke-static {p0}, Lcom/navdy/hud/app/util/ScreenOrientation;->getCurrentOrientation(Landroid/app/Activity;)I

    move-result v0

    invoke-static {v0}, Lcom/navdy/hud/app/util/ScreenOrientation;->isPortrait(I)Z

    move-result v0

    return v0
.end method
