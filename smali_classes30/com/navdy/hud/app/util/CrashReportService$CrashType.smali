.class public final enum Lcom/navdy/hud/app/util/CrashReportService$CrashType;
.super Ljava/lang/Enum;
.source "CrashReportService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/CrashReportService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CrashType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/util/CrashReportService$CrashType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/util/CrashReportService$CrashType;

.field public static final enum BLUETOOTH_DISCONNECTED:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

.field public static final enum GENERIC_NON_FATAL_CRASH:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

.field public static final enum OBD_RESET:Lcom/navdy/hud/app/util/CrashReportService$CrashType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    const-string v1, "BLUETOOTH_DISCONNECTED"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/util/CrashReportService$CrashType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->BLUETOOTH_DISCONNECTED:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    .line 51
    new-instance v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    const-string v1, "OBD_RESET"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/util/CrashReportService$CrashType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->OBD_RESET:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    .line 52
    new-instance v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    const-string v1, "GENERIC_NON_FATAL_CRASH"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/util/CrashReportService$CrashType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->GENERIC_NON_FATAL_CRASH:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->BLUETOOTH_DISCONNECTED:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->OBD_RESET:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->GENERIC_NON_FATAL_CRASH:Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->$VALUES:[Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/util/CrashReportService$CrashType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/util/CrashReportService$CrashType;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->$VALUES:[Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/util/CrashReportService$CrashType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    return-object v0
.end method
