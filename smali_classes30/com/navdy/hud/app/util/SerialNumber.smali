.class public Lcom/navdy/hud/app/util/SerialNumber;
.super Ljava/lang/Object;
.source "SerialNumber.java"


# static fields
.field public static instance:Lcom/navdy/hud/app/util/SerialNumber;


# instance fields
.field public final configurationCode:Ljava/lang/String;

.field public final dayOfWeek:I

.field public final factory:Ljava/lang/String;

.field public final revisionCode:Ljava/lang/String;

.field public final serialNo:Ljava/lang/String;

.field public final week:I

.field public final year:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/hud/app/util/SerialNumber;->parse(Ljava/lang/String;)Lcom/navdy/hud/app/util/SerialNumber;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/SerialNumber;->instance:Lcom/navdy/hud/app/util/SerialNumber;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "factory"    # Ljava/lang/String;
    .param p2, "year"    # I
    .param p3, "week"    # I
    .param p4, "dayOfWeek"    # I
    .param p5, "serialNo"    # Ljava/lang/String;
    .param p6, "configuration"    # Ljava/lang/String;
    .param p7, "revisionCode"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/util/SerialNumber;->factory:Ljava/lang/String;

    .line 40
    iput p2, p0, Lcom/navdy/hud/app/util/SerialNumber;->year:I

    .line 41
    iput p3, p0, Lcom/navdy/hud/app/util/SerialNumber;->week:I

    .line 42
    iput p4, p0, Lcom/navdy/hud/app/util/SerialNumber;->dayOfWeek:I

    .line 43
    iput-object p5, p0, Lcom/navdy/hud/app/util/SerialNumber;->serialNo:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/navdy/hud/app/util/SerialNumber;->configurationCode:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Lcom/navdy/hud/app/util/SerialNumber;->revisionCode:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/navdy/hud/app/util/SerialNumber;
    .locals 13
    .param p0, "serial"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x7

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 49
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v8, 0x10

    if-eq v0, v8, :cond_1

    .line 50
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/util/SerialNumber;

    const-string v1, "NDY"

    const-string v5, "0"

    const-string v6, "0"

    const-string v7, "0"

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/util/SerialNumber;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :goto_0
    return-object v0

    .line 53
    :cond_1
    invoke-virtual {p0, v2, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "factory":Ljava/lang/String;
    invoke-virtual {p0, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v8, "year"

    invoke-static {v0, v2, v8}, Lcom/navdy/hud/app/util/SerialNumber;->parseNumber(Ljava/lang/String;ILjava/lang/String;)I

    move-result v2

    .line 55
    .local v2, "year":I
    const/4 v0, 0x6

    invoke-virtual {p0, v12, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v8, "week"

    invoke-static {v0, v9, v8}, Lcom/navdy/hud/app/util/SerialNumber;->parseNumber(Ljava/lang/String;ILjava/lang/String;)I

    move-result v3

    .line 56
    .local v3, "week":I
    if-lt v3, v9, :cond_2

    const/16 v0, 0x34

    if-le v3, v0, :cond_3

    .line 58
    :cond_2
    const/4 v3, 0x1

    .line 60
    :cond_3
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v8, "dayOfWeek"

    invoke-static {v0, v9, v8}, Lcom/navdy/hud/app/util/SerialNumber;->parseNumber(Ljava/lang/String;ILjava/lang/String;)I

    move-result v4

    .line 61
    .local v4, "dayOfWeek":I
    if-lt v4, v9, :cond_4

    if-le v4, v10, :cond_5

    .line 63
    :cond_4
    const/4 v4, 0x1

    .line 65
    :cond_5
    const/16 v0, 0xa

    invoke-virtual {p0, v10, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 66
    .local v5, "serialNo":Ljava/lang/String;
    const/16 v0, 0xa

    const/16 v8, 0xe

    invoke-virtual {p0, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 67
    .local v6, "configuration":Ljava/lang/String;
    const/16 v0, 0xe

    const/16 v8, 0xf

    invoke-virtual {p0, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 69
    .local v7, "revision":Ljava/lang/String;
    new-instance v0, Lcom/navdy/hud/app/util/SerialNumber;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/util/SerialNumber;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static parseNumber(Ljava/lang/String;ILjava/lang/String;)I
    .locals 5
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I
    .param p2, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 73
    move v1, p1

    .line 75
    .local v1, "result":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 80
    :goto_0
    return v1

    .line 76
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v2, "SERIAL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid serial number field "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " value:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
