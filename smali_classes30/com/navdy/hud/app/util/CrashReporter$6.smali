.class Lcom/navdy/hud/app/util/CrashReporter$6;
.super Ljava/lang/Object;
.source "CrashReporter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/CrashReporter;->saveHockeyAppException(Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/CrashReporter;

.field final synthetic val$throwable:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/CrashReporter;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/CrashReporter;

    .prologue
    .line 551
    iput-object p1, p0, Lcom/navdy/hud/app/util/CrashReporter$6;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    iput-object p2, p0, Lcom/navdy/hud/app/util/CrashReporter$6;->val$throwable:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 555
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/util/CrashReporter$6;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->crashManagerListener:Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;
    invoke-static {v1}, Lcom/navdy/hud/app/util/CrashReporter;->access$1100(Lcom/navdy/hud/app/util/CrashReporter;)Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$6;->val$throwable:Ljava/lang/Throwable;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->saveException(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    :goto_0
    return-void

    .line 556
    :catch_0
    move-exception v0

    .line 557
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
