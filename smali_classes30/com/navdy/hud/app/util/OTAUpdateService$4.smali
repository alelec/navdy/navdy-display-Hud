.class Lcom/navdy/hud/app/util/OTAUpdateService$4;
.super Ljava/lang/Object;
.source "OTAUpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/OTAUpdateService;->cleanupOldFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/OTAUpdateService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/OTAUpdateService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/OTAUpdateService;

    .prologue
    .line 355
    iput-object p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService$4;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 358
    const/4 v4, -0x1

    .line 360
    .local v4, "incrementalVersion":I
    :try_start_0
    sget-object v7, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 365
    :goto_0
    const/4 v7, -0x1

    if-ne v4, v7, :cond_0

    .line 367
    iget-object v7, p0, Lcom/navdy/hud/app/util/OTAUpdateService$4;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v7, v7, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v8, "last_update_file_received"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 368
    .local v3, "fileName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 369
    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->extractIncrementalVersion(Ljava/lang/String;)I
    invoke-static {v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$700(Ljava/lang/String;)I

    move-result v4

    .line 372
    .end local v3    # "fileName":Ljava/lang/String;
    :cond_0
    if-lez v4, :cond_2

    .line 373
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v7

    sget-object v8, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/storage/PathManager;->getDirectoryForFileType(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;

    move-result-object v1

    .line 374
    .local v1, "directory":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 375
    .local v5, "otaUpdatesFileDirectory":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 376
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 377
    .local v0, "children":[Ljava/io/File;
    if-eqz v0, :cond_2

    .line 378
    array-length v8, v0

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v8, :cond_2

    aget-object v2, v0, v7

    .line 379
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/navdy/hud/app/util/OTAUpdateService;->isOtaFile(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->extractIncrementalVersion(Ljava/lang/String;)I
    invoke-static {v9}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$700(Ljava/lang/String;)I

    move-result v9

    if-gt v9, v4, :cond_1

    .line 380
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Clearing the old update file :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 381
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 378
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 361
    .end local v0    # "children":[Ljava/io/File;
    .end local v1    # "directory":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "otaUpdatesFileDirectory":Ljava/io/File;
    :catch_0
    move-exception v6

    .line 362
    .local v6, "t":Ljava/lang/NumberFormatException;
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot parse the incremental version of the build :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 387
    .end local v6    # "t":Ljava/lang/NumberFormatException;
    :cond_2
    return-void
.end method
