.class public Lcom/navdy/hud/app/util/GenericUtil;
.super Ljava/lang/Object;
.source "GenericUtil.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/GenericUtil;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/GenericUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkNotOnMainThread()V
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 54
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "cannot call on mainthread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    return-void
.end method

.method public static clearInputMethodManagerFocusLeak()V
    .locals 6

    .prologue
    .line 120
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 122
    .local v1, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :try_start_0
    const-class v3, Landroid/view/inputmethod/InputMethodManager;

    const-string v4, "finishInputLocked"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 123
    .local v0, "finishInputLockedMethod":Ljava/lang/reflect/Method;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 124
    sget-object v3, Lcom/navdy/hud/app/util/GenericUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "calling finishInputLocked"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 125
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v3, Lcom/navdy/hud/app/util/GenericUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "called finishInputLocked"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    .end local v0    # "finishInputLockedMethod":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v2

    .line 128
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/util/GenericUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "finishInputLocked"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getDrawableResourceIdFromName(Ljava/lang/String;)I
    .locals 4
    .param p0, "idName"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 37
    .local v1, "resources":Landroid/content/res/Resources;
    const-string v2, "drawable"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public static getErrorMessage(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 3
    .param p0, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 24
    if-nez p0, :cond_1

    .line 25
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 31
    :cond_0
    :goto_0
    return-object v0

    .line 27
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "str":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    invoke-virtual {p0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getIdsResourceIdFromName(Ljava/lang/String;)I
    .locals 4
    .param p0, "idName"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 42
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 43
    .local v1, "resources":Landroid/content/res/Resources;
    const-string v2, "id"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public static getStringResourceIdFromName(Ljava/lang/String;)I
    .locals 4
    .param p0, "idName"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 48
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 49
    .local v1, "resources":Landroid/content/res/Resources;
    const-string v2, "string"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public static indexOf([B[BII)I
    .locals 8
    .param p0, "srcBytes"    # [B
    .param p1, "searchBytes"    # [B
    .param p2, "startIndex"    # I
    .param p3, "endIndex"    # I

    .prologue
    const/4 v5, -0x1

    .line 91
    array-length v6, p1

    if-eqz v6, :cond_0

    sub-int v6, p3, p2

    add-int/lit8 v6, v6, 0x1

    array-length v7, p1

    if-ge v6, v7, :cond_2

    :cond_0
    move v1, v5

    .line 115
    :cond_1
    :goto_0
    return v1

    .line 95
    :cond_2
    array-length v6, p0

    array-length v7, p1

    sub-int v4, v6, v7

    .line 97
    .local v4, "maxScanStartPosIdx":I
    if-ge p3, v4, :cond_4

    .line 98
    move v3, p3

    .line 103
    .local v3, "loopEndIdx":I
    :goto_1
    move v1, p2

    .local v1, "i":I
    :goto_2
    if-gt v1, v3, :cond_6

    .line 104
    const/4 v0, 0x1

    .line 105
    .local v0, "found":Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    array-length v6, p1

    if-ge v2, v6, :cond_3

    .line 106
    add-int v6, v1, v2

    aget-byte v6, p0, v6

    aget-byte v7, p1, v2

    if-eq v6, v7, :cond_5

    .line 107
    const/4 v0, 0x0

    .line 111
    :cond_3
    if-nez v0, :cond_1

    .line 103
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 100
    .end local v0    # "found":Z
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "loopEndIdx":I
    :cond_4
    move v3, v4

    .restart local v3    # "loopEndIdx":I
    goto :goto_1

    .line 105
    .restart local v0    # "found":Z
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .end local v0    # "found":Z
    .end local v2    # "j":I
    :cond_6
    move v1, v5

    .line 115
    goto :goto_0
.end method

.method public static isClientiOS()Z
    .locals 3

    .prologue
    .line 133
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 134
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v2, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v1, v2, :cond_0

    .line 135
    const/4 v1, 0x1

    .line 137
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMainThread()Z
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 60
    const/4 v0, 0x1

    .line 62
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static normalizeToFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    .end local p0    # "str":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "str":Ljava/lang/String;
    :cond_0
    const-string v0, "[^a-zA-Z0-9\\._]+"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static removePunctuation(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    .end local p0    # "str":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "str":Ljava/lang/String;
    :cond_0
    const-string v0, "\\p{Punct}+"

    const-string v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static sleep(I)V
    .locals 2
    .param p0, "millis"    # I

    .prologue
    .line 82
    int-to-long v0, p0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0
.end method
