.class public Lcom/navdy/hud/app/util/ViewUtil;
.super Ljava/lang/Object;
.source "ViewUtil.java"


# static fields
.field private static final LINE_SPACING_INDEX:I = 0x2

.field private static final MAX_LINE_INDEX:I = 0x0

.field private static final MULTI_LINE_ATTRS:[I

.field private static final SINGLE_LINE_INDEX:I = 0x1

.field static TEXT_SIZES:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[F>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/hud/app/util/ViewUtil;->MULTI_LINE_ATTRS:[I

    .line 36
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/util/ViewUtil;->TEXT_SIZES:Landroid/util/SparseArray;

    return-void

    .line 26
    :array_0
    .array-data 4
        0x1010153
        0x101015d
        0x1010218
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static adjustPadding(Landroid/view/View;IIII)V
    .locals 4
    .param p0, "v"    # Landroid/view/View;
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 43
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, p2

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, p3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    add-int/2addr v3, p4

    .line 43
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 45
    return-void
.end method

.method public static applyTextAndStyle(Landroid/widget/TextView;Ljava/lang/CharSequence;II)I
    .locals 7
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "maxLines"    # I
    .param p3, "style"    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 132
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 133
    .local v0, "context":Landroid/content/Context;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 134
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    :goto_0
    return v5

    .line 137
    :cond_0
    invoke-virtual {p0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    if-ne p2, v4, :cond_2

    move v2, v4

    .line 139
    .local v2, "singleLine":Z
    :goto_1
    const/high16 v1, 0x3f800000    # 1.0f

    .line 140
    .local v1, "lineSpacing":F
    const/4 v6, -0x1

    if-eq p3, v6, :cond_1

    .line 142
    sget-object v6, Lcom/navdy/hud/app/util/ViewUtil;->MULTI_LINE_ATTRS:[I

    invoke-virtual {v0, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 143
    .local v3, "ta":Landroid/content/res/TypedArray;
    invoke-virtual {v3, v5, p2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 144
    invoke-virtual {v3, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 145
    invoke-virtual {v3, v4, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 150
    :goto_2
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 151
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 153
    invoke-virtual {p0, v0, p3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 155
    .end local v3    # "ta":Landroid/content/res/TypedArray;
    :cond_1
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 157
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 158
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setMaxLines(I)V

    move v5, p2

    .line 159
    goto :goto_0

    .end local v1    # "lineSpacing":F
    .end local v2    # "singleLine":Z
    :cond_2
    move v2, v5

    .line 138
    goto :goto_1

    .line 148
    .restart local v1    # "lineSpacing":F
    .restart local v2    # "singleLine":Z
    .restart local v3    # "ta":Landroid/content/res/TypedArray;
    :cond_3
    if-ne p2, v4, :cond_4

    move v2, v4

    :goto_3
    goto :goto_2

    :cond_4
    move v2, v5

    goto :goto_3
.end method

.method public static autosize(Landroid/widget/TextView;II[F)F
    .locals 3
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "maxLines"    # I
    .param p2, "maxWidth"    # I
    .param p3, "textSizes"    # [F

    .prologue
    .line 53
    array-length v2, p3

    new-array v1, v2, [I

    .line 54
    .local v1, "maxLinesArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_0

    .line 55
    aput p1, v1, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    const/4 v2, 0x0

    invoke-static {p0, v1, p2, p3, v2}, Lcom/navdy/hud/app/util/ViewUtil;->autosize(Landroid/widget/TextView;[II[F[I)F

    move-result v2

    return v2
.end method

.method public static autosize(Landroid/widget/TextView;[II[F[I)F
    .locals 17
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "maxLinesArray"    # [I
    .param p2, "maxWidth"    # I
    .param p3, "textSizes"    # [F
    .param p4, "indexHolder"    # [I

    .prologue
    .line 71
    move-object/from16 v0, p1

    array-length v4, v0

    move-object/from16 v0, p3

    array-length v5, v0

    if-eq v4, v5, :cond_0

    .line 72
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "maxLinesArray length must match textSizes length"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 75
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 77
    .local v2, "sequence":Ljava/lang/CharSequence;
    invoke-virtual/range {p0 .. p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 80
    .local v3, "p":Landroid/text/TextPaint;
    const/4 v15, -0x1

    .line 81
    .local v15, "minLines":I
    move-object/from16 v0, p3

    array-length v4, v0

    add-int/lit8 v10, v4, -0x1

    .line 82
    .local v10, "index":I
    aget v14, p3, v10

    .line 84
    .local v14, "maxSize":F
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p3

    array-length v4, v0

    if-ge v9, v4, :cond_3

    .line 85
    aget v13, p1, v9

    .line 86
    .local v13, "maxLines":I
    const/4 v4, 0x1

    if-ne v13, v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 88
    move v11, v13

    .line 89
    .local v11, "limit":I
    aget v16, p3, v9

    .line 90
    .local v16, "textSize":F
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 91
    new-instance v1, Landroid/text/StaticLayout;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    move/from16 v4, p2

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 92
    .local v1, "layout":Landroid/text/StaticLayout;
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v12

    .line 93
    .local v12, "lines":I
    if-gt v12, v11, :cond_2

    const/4 v4, -0x1

    if-eq v15, v4, :cond_1

    if-ge v12, v15, :cond_2

    .line 94
    :cond_1
    move v15, v12

    .line 95
    move/from16 v14, v16

    .line 96
    move v10, v9

    .line 98
    :cond_2
    const/4 v4, 0x1

    if-ne v12, v4, :cond_6

    .line 103
    .end local v1    # "layout":Landroid/text/StaticLayout;
    .end local v11    # "limit":I
    .end local v12    # "lines":I
    .end local v13    # "maxLines":I
    .end local v16    # "textSize":F
    :cond_3
    if-eqz p4, :cond_4

    .line 104
    const/4 v4, 0x0

    aput v10, p4, v4

    .line 105
    const/4 v4, 0x1

    const/4 v5, -0x1

    if-eq v15, v5, :cond_7

    .end local v15    # "minLines":I
    :goto_2
    aput v15, p4, v4

    .line 108
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setTextSize(F)V

    .line 109
    return v14

    .line 86
    .restart local v13    # "maxLines":I
    .restart local v15    # "minLines":I
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 84
    .restart local v1    # "layout":Landroid/text/StaticLayout;
    .restart local v11    # "limit":I
    .restart local v12    # "lines":I
    .restart local v16    # "textSize":F
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 105
    .end local v1    # "layout":Landroid/text/StaticLayout;
    .end local v11    # "limit":I
    .end local v12    # "lines":I
    .end local v13    # "maxLines":I
    .end local v16    # "textSize":F
    :cond_7
    aget v15, p1, v10

    goto :goto_2
.end method

.method public static autosize(Landroid/widget/TextView;III)V
    .locals 1
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "maxLines"    # I
    .param p2, "maxWidth"    # I
    .param p3, "arrayId"    # I
        .annotation build Landroid/support/annotation/ArrayRes;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/navdy/hud/app/util/ViewUtil;->lookupSizes(Landroid/content/res/Resources;I)[F

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lcom/navdy/hud/app/util/ViewUtil;->autosize(Landroid/widget/TextView;II[F)F

    .line 40
    return-void
.end method

.method private static lookupSizes(Landroid/content/res/Resources;I)[F
    .locals 5
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "arrayId"    # I
        .annotation build Landroid/support/annotation/ArrayRes;
        .end annotation
    .end param

    .prologue
    .line 116
    sget-object v4, Lcom/navdy/hud/app/util/ViewUtil;->TEXT_SIZES:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    .line 117
    .local v1, "cache":[F
    if-nez v1, :cond_1

    .line 118
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 119
    .local v0, "array":[I
    array-length v4, v0

    new-array v2, v4, [F

    .line 120
    .local v2, "copy":[F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_0

    .line 121
    aget v4, v0, v3

    int-to-float v4, v4

    aput v4, v2, v3

    .line 120
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 123
    :cond_0
    move-object v1, v2

    .line 124
    sget-object v4, Lcom/navdy/hud/app/util/ViewUtil;->TEXT_SIZES:Landroid/util/SparseArray;

    invoke-virtual {v4, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 126
    .end local v0    # "array":[I
    .end local v2    # "copy":[F
    .end local v3    # "i":I
    :cond_1
    return-object v1
.end method

.method public static setBottomPadding(Landroid/view/View;I)V
    .locals 3
    .param p0, "v"    # Landroid/view/View;
    .param p1, "bottom"    # I

    .prologue
    .line 48
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 48
    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 50
    return-void
.end method
