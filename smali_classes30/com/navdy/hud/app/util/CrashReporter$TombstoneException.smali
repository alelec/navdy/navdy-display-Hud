.class public Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;
.super Lcom/navdy/hud/app/util/CrashReporter$CrashException;
.source "CrashReporter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/CrashReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TombstoneException"
.end annotation


# static fields
.field private static final stackMatcher:Ljava/util/regex/Pattern;


# instance fields
.field public final description:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 156
    const-string v0, "Abort message: (\'[^\']+\'\n)|#00 pc \\w+\\s+([^\n]+\n)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;->stackMatcher:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 163
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;-><init>(Ljava/lang/String;)V

    .line 165
    iput-object p1, p0, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;->description:Ljava/lang/String;

    .line 166
    return-void
.end method


# virtual methods
.method public getCrashLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;->description:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;->extractCrashLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocationPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;->stackMatcher:Ljava/util/regex/Pattern;

    return-object v0
.end method
