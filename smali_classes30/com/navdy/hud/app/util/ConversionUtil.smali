.class public Lcom/navdy/hud/app/util/ConversionUtil;
.super Ljava/lang/Object;
.source "ConversionUtil.java"


# static fields
.field public static final MAX_KMPL:I = 0x2d

.field public static final MAX_MPG:I = 0x64


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertLpHundredKmToKMPL(D)D
    .locals 4
    .param p0, "value"    # D

    .prologue
    const-wide/16 v0, 0x0

    .line 28
    cmpl-double v2, p0, v0

    if-lez v2, :cond_0

    .line 29
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    div-double/2addr v0, p0

    const-wide v2, 0x4046800000000000L    # 45.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    .line 31
    :cond_0
    return-wide v0
.end method

.method public static convertLpHundredKmToMPG(D)D
    .locals 4
    .param p0, "value"    # D

    .prologue
    const-wide/16 v0, 0x0

    .line 16
    cmpl-double v2, p0, v0

    if-lez v2, :cond_0

    .line 17
    const-wide v0, 0x406d66d916872b02L    # 235.214

    div-double/2addr v0, p0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    .line 19
    :cond_0
    return-wide v0
.end method
