.class Lcom/navdy/hud/app/util/FeatureUtil$1;
.super Landroid/content/BroadcastReceiver;
.source "FeatureUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/FeatureUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/FeatureUtil;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/FeatureUtil;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/FeatureUtil;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/navdy/hud/app/util/FeatureUtil$1;->this$0:Lcom/navdy/hud/app/util/FeatureUtil;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 55
    :try_start_0
    const-string v2, "com.navdy.hud.app.feature"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 57
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v2

    if-eq v2, v4, :cond_1

    .line 99
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 60
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/util/FeatureUtil$1$1;

    invoke-direct {v3, p0, v0}, Lcom/navdy/hud/app/util/FeatureUtil$1$1;-><init>(Lcom/navdy/hud/app/util/FeatureUtil$1;Landroid/os/Bundle;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 97
    .local v1, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/FeatureUtil;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
