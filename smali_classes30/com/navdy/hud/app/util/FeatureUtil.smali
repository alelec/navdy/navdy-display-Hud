.class public final Lcom/navdy/hud/app/util/FeatureUtil;
.super Ljava/lang/Object;
.source "FeatureUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    }
.end annotation


# static fields
.field public static final FEATURE_ACTION:Ljava/lang/String; = "com.navdy.hud.app.feature"

.field public static final FEATURE_FUEL_ROUTING:Ljava/lang/String; = "com.navdy.hud.app.feature.fuelRouting"

.field public static final FEATURE_GESTURE_COLLECTOR:Ljava/lang/String; = "com.navdy.hud.app.feature.gestureCollector"

.field public static final FEATURE_GESTURE_ENGINE:Ljava/lang/String; = "com.navdy.hud.app.feature.gestureEngine"

.field public static final FEATURE_VOICE_SEARCH_ADDITIONAL_RESULTS_PROPERTY:Ljava/lang/String; = "persist.voice.search.additional.results"

.field public static final FEATURE_VOICE_SEARCH_LIST_PROPERTY:Ljava/lang/String; = "persist.sys.voicesearch.list"

.field private static final GESTURE_CALIBRATED_PROPERTY:Ljava/lang/String; = "persist.sys.swiped.calib"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private featuresEnabledMap:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/navdy/hud/app/util/FeatureUtil$Feature;",
            ">;"
        }
    .end annotation
.end field

.field private receiver:Landroid/content/BroadcastReceiver;

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/FeatureUtil;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 10
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    iput-object v8, p0, Lcom/navdy/hud/app/util/FeatureUtil;->featuresEnabledMap:Ljava/util/HashSet;

    .line 51
    new-instance v8, Lcom/navdy/hud/app/util/FeatureUtil$1;

    invoke-direct {v8, p0}, Lcom/navdy/hud/app/util/FeatureUtil$1;-><init>(Lcom/navdy/hud/app/util/FeatureUtil;)V

    iput-object v8, p0, Lcom/navdy/hud/app/util/FeatureUtil;->receiver:Landroid/content/BroadcastReceiver;

    .line 104
    iput-object p1, p0, Lcom/navdy/hud/app/util/FeatureUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 105
    const-string v8, "persist.sys.swiped.calib"

    const-string v9, ""

    invoke-static {v8, v9}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    move v0, v6

    .line 106
    .local v0, "calibrated":Z
    :goto_0
    const-string v8, "gesture.engine"

    invoke-interface {p1, v8, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz v0, :cond_3

    :cond_0
    move v2, v6

    .line 109
    .local v2, "enabled":Z
    :goto_1
    const-string v8, "persist.sys.voicesearch.list"

    invoke-static {v8, v7}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 110
    .local v5, "voiceSearchListResultsEnabled":Z
    const-string v7, "persist.voice.search.additional.results"

    invoke-static {v7, v6}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 111
    .local v4, "voiceSearchListAdditionalResultsEnabled":Z
    if-eqz v2, :cond_4

    .line 113
    iget-object v6, p0, Lcom/navdy/hud/app/util/FeatureUtil;->featuresEnabledMap:Ljava/util/HashSet;

    sget-object v7, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_ENGINE:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 114
    sget-object v6, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "enabled gesture engine"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 117
    invoke-direct {p0}, Lcom/navdy/hud/app/util/FeatureUtil;->isGestureCollectorInstalled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 118
    iget-object v6, p0, Lcom/navdy/hud/app/util/FeatureUtil;->featuresEnabledMap:Ljava/util/HashSet;

    sget-object v7, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_COLLECTOR:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 119
    sget-object v6, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "enabled gesture collector"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 126
    :cond_1
    :goto_2
    iget-object v6, p0, Lcom/navdy/hud/app/util/FeatureUtil;->featuresEnabledMap:Ljava/util/HashSet;

    sget-object v7, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v3, Landroid/content/IntentFilter;

    const-string v6, "com.navdy.hud.app.feature"

    invoke-direct {v3, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 130
    .local v3, "filter":Landroid/content/IntentFilter;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 131
    .local v1, "context":Landroid/content/Context;
    iget-object v6, p0, Lcom/navdy/hud/app/util/FeatureUtil;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v6, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 132
    return-void

    .end local v0    # "calibrated":Z
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "enabled":Z
    .end local v3    # "filter":Landroid/content/IntentFilter;
    .end local v4    # "voiceSearchListAdditionalResultsEnabled":Z
    .end local v5    # "voiceSearchListResultsEnabled":Z
    :cond_2
    move v0, v7

    .line 105
    goto :goto_0

    .restart local v0    # "calibrated":Z
    :cond_3
    move v2, v7

    .line 106
    goto :goto_1

    .line 122
    .restart local v2    # "enabled":Z
    .restart local v4    # "voiceSearchListAdditionalResultsEnabled":Z
    .restart local v5    # "voiceSearchListResultsEnabled":Z
    :cond_4
    sget-object v6, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "not enabled gesture engine"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/util/FeatureUtil;Ljava/lang/String;)Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/FeatureUtil;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/FeatureUtil;->getFeatureFromName(Ljava/lang/String;)Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/util/FeatureUtil;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/FeatureUtil;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/util/FeatureUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/util/FeatureUtil;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/FeatureUtil;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/util/FeatureUtil;->isGestureCollectorInstalled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/util/FeatureUtil;Lcom/navdy/hud/app/util/FeatureUtil$Feature;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/FeatureUtil;
    .param p1, "x1"    # Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .param p2, "x2"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/util/FeatureUtil;->setFeature(Lcom/navdy/hud/app/util/FeatureUtil$Feature;Z)V

    return-void
.end method

.method private getFeatureFromName(Ljava/lang/String;)Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 149
    if-nez p1, :cond_0

    .line 163
    :goto_0
    return-object v0

    .line 152
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 154
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_COLLECTOR:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    goto :goto_0

    .line 152
    :sswitch_0
    const-string v2, "com.navdy.hud.app.feature.gestureCollector"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "com.navdy.hud.app.feature.gestureEngine"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "com.navdy.hud.app.feature.fuelRouting"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    .line 157
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_ENGINE:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    goto :goto_0

    .line 160
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    goto :goto_0

    .line 152
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4ae1f92f -> :sswitch_2
        -0x4488babd -> :sswitch_0
        -0x167d2b4 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private isGestureCollectorInstalled()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 169
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 170
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const-string v3, "com.navdy.collector"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v2

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/Throwable;
    instance-of v2, v0, Landroid/content/pm/PackageManager$NameNotFoundException;

    if-nez v2, :cond_0

    .line 174
    sget-object v2, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "gesture collector not enabled"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 176
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "gesture collector not found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private declared-synchronized setFeature(Lcom/navdy/hud/app/util/FeatureUtil$Feature;Z)V
    .locals 3
    .param p1, "feature"    # Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .param p2, "enable"    # Z

    .prologue
    .line 139
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/util/FeatureUtil;->featuresEnabledMap:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v0, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enabled featured:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :goto_0
    monitor-exit p0

    return-void

    .line 143
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/util/FeatureUtil;->featuresEnabledMap:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 144
    sget-object v0, Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disabled featured:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized isFeatureEnabled(Lcom/navdy/hud/app/util/FeatureUtil$Feature;)Z
    .locals 1
    .param p1, "feature"    # Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/util/FeatureUtil;->featuresEnabledMap:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
