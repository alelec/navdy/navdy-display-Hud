.class public Lcom/navdy/hud/app/util/OTAUpdateService;
.super Landroid/app/Service;
.source "OTAUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;,
        Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;,
        Lcom/navdy/hud/app/util/OTAUpdateService$InstallingUpdate;,
        Lcom/navdy/hud/app/util/OTAUpdateService$UpdateVerified;,
        Lcom/navdy/hud/app/util/OTAUpdateService$IncrementalOTAFailureDetected;
    }
.end annotation


# static fields
.field public static final CAPTURE_GROUP_FROM_VERSION:I = 0x2

.field public static final CAPTURE_GROUP_TARGET_VERSION:I = 0x3

.field public static final COMMAND_CHECK_UPDATE_ON_REBOOT:Ljava/lang/String; = "CHECK_UPDATE"

.field public static final COMMAND_DO_NOT_PROMPT:Ljava/lang/String; = "DO_NOT_PROMPT"

.field public static final COMMAND_INSTALL_UPDATE:Ljava/lang/String; = "INSTALL_UPDATE"

.field public static final COMMAND_INSTALL_UPDATE_REBOOT_QUIET:Ljava/lang/String; = "INSTALL_UPDATE_REBOOT_QUIET"

.field public static final COMMAND_INSTALL_UPDATE_SHUTDOWN:Ljava/lang/String; = "INSTALL_UPDATE_SHUTDOWN"

.field public static final COMMAND_VERIFY_UPDATE:Ljava/lang/String; = "VERIFY_UPDATE"

.field public static final DO_NOT_PROMPT:Ljava/lang/String; = "do_not_prompt"

.field public static final EXTRA_COMMAND:Ljava/lang/String; = "COMMAND"

.field private static final FAILED_OTA_FILE:Ljava/lang/String; = "failed_ota_file"

.field private static final MAX_FILE_SIZE:J = 0x80000000L

.field public static final MAX_RETRY_COUNT:I = 0x3

.field private static final MAX_ZIP_ENTRIES:I = 0x3e8

.field public static final RECOVERY_LAST_INSTALL_FILE_PATH:Ljava/lang/String; = "/cache/recovery/last_install"

.field private static final RECOVERY_LAST_LOG_FILE_PATH:Ljava/lang/String; = "/cache/recovery/last_log"

.field public static final RETRY_COUNT:Ljava/lang/String; = "retry_count"

.field public static final UPDATE_FILE:Ljava/lang/String; = "last_update_file_received"

.field private static final UPDATE_FROM_VERSION:Ljava/lang/String; = "update_from_version"

.field public static final VERIFIED:Ljava/lang/String; = "verified"

.field public static final VERSION_PREFIX:Ljava/lang/String; = "1.0."

.field private static final sFileNamePattern:Ljava/util/regex/Pattern;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static swVersion:Ljava/lang/String;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private installing:Z

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 66
    const-string v0, "^navdy_ota_(([0-9]+)_to_)?([0-9]+)\\.zip"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/OTAUpdateService;->sFileNamePattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/content/SharedPreferences;

    .prologue
    .line 54
    invoke-static {p0, p1}, Lcom/navdy/hud/app/util/OTAUpdateService;->checkUpdateFile(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/util/OTAUpdateService;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/OTAUpdateService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/io/File;
    .param p3, "x3"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/util/OTAUpdateService;->installPackage(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/util/OTAUpdateService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/OTAUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService;->installing:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/util/OTAUpdateService;Ljava/io/File;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/OTAUpdateService;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/OTAUpdateService;->checkIfIncrementalUpdatesFailed(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/hud/app/util/OTAUpdateService;->swVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 54
    sput-object p0, Lcom/navdy/hud/app/util/OTAUpdateService;->swVersion:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/SharedPreferences;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/navdy/hud/app/util/OTAUpdateService;->getUpdateVersion(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/navdy/hud/app/util/OTAUpdateService;->extractIncrementalVersion(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private checkIfIncrementalUpdatesFailed(Ljava/io/File;)Z
    .locals 18
    .param p1, "currentUpdate"    # Ljava/io/File;

    .prologue
    .line 300
    new-instance v7, Ljava/io/File;

    const-string v15, "/cache/recovery/last_install"

    invoke-direct {v7, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 301
    .local v7, "lastInstallFile":Ljava/io/File;
    const/4 v1, 0x1

    .line 302
    .local v1, "currentUpdateValid":Z
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 304
    :try_start_0
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 305
    .local v8, "lastInstallInfo":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 306
    const-string v15, "\n"

    invoke-virtual {v8, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 307
    .local v10, "lines":[Ljava/lang/String;
    array-length v15, v10

    const/16 v16, 0x2

    move/from16 v0, v16

    if-lt v15, v0, :cond_2

    .line 308
    const/4 v15, 0x0

    aget-object v3, v10, v15

    .line 309
    .local v3, "fileName":Ljava/lang/String;
    sget-object v15, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Last installed update was "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 310
    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 311
    .local v14, "updateFile":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    .line 312
    .local v13, "updateBaseName":Ljava/lang/String;
    const/4 v15, 0x1

    aget-object v15, v10, v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_3

    const/4 v9, 0x1

    .line 313
    .local v9, "lastInstallSucceeded":Z
    :goto_0
    if-nez v9, :cond_0

    .line 314
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/navdy/hud/app/util/OTAUpdateService;->reportOTAFailure(Ljava/lang/String;)V

    .line 316
    :cond_0
    sget-object v16, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Last install succeeded: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    if-eqz v9, :cond_4

    const-string v15, "1"

    :goto_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 317
    invoke-static {v13}, Lcom/navdy/hud/app/util/OTAUpdateService;->extractFromVersion(Ljava/lang/String;)I

    move-result v4

    .line 318
    .local v4, "fromVersion":I
    const/4 v15, -0x1

    if-eq v4, v15, :cond_5

    const/4 v6, 0x1

    .line 319
    .local v6, "isIncremental":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v16, "update_from_version"

    const-string v17, ""

    invoke-interface/range {v15 .. v17}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 320
    .local v11, "priorVersion":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 321
    invoke-static {v6, v11, v9}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordOTAInstallResult(ZLjava/lang/String;Z)V

    .line 322
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v15}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v15

    const-string v16, "update_from_version"

    invoke-interface/range {v15 .. v16}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v15

    invoke-interface {v15}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 324
    :cond_1
    if-eqz v6, :cond_2

    .line 325
    sget-object v15, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "Last install was an incremental update"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 327
    const/4 v5, 0x0

    .line 329
    .local v5, "incrementalVersion":I
    :try_start_1
    sget-object v15, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 333
    :goto_3
    if-ne v5, v4, :cond_2

    .line 334
    :try_start_2
    sget-object v15, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "The last installed incremental from version is same as current version :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 335
    if-nez v9, :cond_2

    .line 336
    sget-object v15, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "Last incremental update has failed"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 337
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/util/OTAUpdateService;->bus:Lcom/squareup/otto/Bus;

    new-instance v16, Lcom/navdy/hud/app/util/OTAUpdateService$IncrementalOTAFailureDetected;

    invoke-direct/range {v16 .. v16}, Lcom/navdy/hud/app/util/OTAUpdateService$IncrementalOTAFailureDetected;-><init>()V

    invoke-virtual/range {v15 .. v16}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 338
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 339
    sget-object v15, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "Current update is the same as the one failed during last install"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 340
    const/4 v1, 0x0

    .line 351
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v4    # "fromVersion":I
    .end local v5    # "incrementalVersion":I
    .end local v6    # "isIncremental":Z
    .end local v8    # "lastInstallInfo":Ljava/lang/String;
    .end local v9    # "lastInstallSucceeded":Z
    .end local v10    # "lines":[Ljava/lang/String;
    .end local v11    # "priorVersion":Ljava/lang/String;
    .end local v13    # "updateBaseName":Ljava/lang/String;
    .end local v14    # "updateFile":Ljava/io/File;
    :cond_2
    :goto_4
    return v1

    .line 312
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v8    # "lastInstallInfo":Ljava/lang/String;
    .restart local v10    # "lines":[Ljava/lang/String;
    .restart local v13    # "updateBaseName":Ljava/lang/String;
    .restart local v14    # "updateFile":Ljava/io/File;
    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 316
    .restart local v9    # "lastInstallSucceeded":Z
    :cond_4
    const-string v15, "0"

    goto/16 :goto_1

    .line 318
    .restart local v4    # "fromVersion":I
    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 330
    .restart local v5    # "incrementalVersion":I
    .restart local v6    # "isIncremental":Z
    .restart local v11    # "priorVersion":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 331
    .local v12, "t":Ljava/lang/NumberFormatException;
    sget-object v15, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Cannot parse the incremental version of the build :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 347
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v4    # "fromVersion":I
    .end local v5    # "incrementalVersion":I
    .end local v6    # "isIncremental":Z
    .end local v8    # "lastInstallInfo":Ljava/lang/String;
    .end local v9    # "lastInstallSucceeded":Z
    .end local v10    # "lines":[Ljava/lang/String;
    .end local v11    # "priorVersion":Ljava/lang/String;
    .end local v12    # "t":Ljava/lang/NumberFormatException;
    .end local v13    # "updateBaseName":Ljava/lang/String;
    .end local v14    # "updateFile":Ljava/io/File;
    :catch_1
    move-exception v2

    .line 348
    .local v2, "e":Ljava/io/IOException;
    sget-object v15, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Error reading the file :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method private static checkUpdateFile(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/io/File;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v7, 0x0

    .line 471
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Checking for update file"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 472
    sput-object v7, Lcom/navdy/hud/app/util/OTAUpdateService;->swVersion:Ljava/lang/String;

    .line 473
    const-string v8, "last_update_file_received"

    invoke-interface {p1, v8, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 474
    .local v1, "fileName":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 475
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "No update available"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move-object v0, v7

    .line 510
    :cond_0
    :goto_0
    return-object v0

    .line 478
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 479
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v8

    if-nez v8, :cond_3

    .line 480
    :cond_2
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Invalid update file"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move-object v0, v7

    .line 481
    goto :goto_0

    .line 483
    :cond_3
    const/4 v3, 0x0

    .line 485
    .local v3, "incrementalVersion":I
    :try_start_0
    sget-object v8, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 489
    :goto_1
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Incremental version of the build on the device:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 490
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/hud/app/util/OTAUpdateService;->extractIncrementalVersion(Ljava/lang/String;)I

    move-result v6

    .line 491
    .local v6, "updateVersion":I
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/hud/app/util/OTAUpdateService;->extractFromVersion(Ljava/lang/String;)I

    move-result v2

    .line 492
    .local v2, "fromVersion":I
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Update from version "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 494
    const/4 v8, -0x1

    if-eq v2, v8, :cond_4

    if-eqz v3, :cond_4

    if-eq v2, v3, :cond_4

    .line 495
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Update from version mismatch "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 496
    invoke-static {v0, p0, p1}, Lcom/navdy/hud/app/util/OTAUpdateService;->clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    move-object v0, v7

    .line 497
    goto/16 :goto_0

    .line 486
    .end local v2    # "fromVersion":I
    .end local v6    # "updateVersion":I
    :catch_0
    move-exception v5

    .line 487
    .local v5, "t":Ljava/lang/NumberFormatException;
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot parse the incremental version of the build :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 499
    .end local v5    # "t":Ljava/lang/NumberFormatException;
    .restart local v2    # "fromVersion":I
    .restart local v6    # "updateVersion":I
    :cond_4
    if-gt v6, v3, :cond_5

    .line 500
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Already up to date :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 501
    invoke-static {v0, p0, p1}, Lcom/navdy/hud/app/util/OTAUpdateService;->clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    move-object v0, v7

    .line 502
    goto/16 :goto_0

    .line 504
    :cond_5
    const-string v8, "retry_count"

    const/4 v9, 0x0

    invoke-interface {p1, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 505
    .local v4, "retryCount":I
    const/4 v8, 0x3

    if-lt v4, v8, :cond_0

    .line 506
    sget-object v8, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Maximum retries. Deleting the update file."

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 507
    invoke-static {v0, p0, p1}, Lcom/navdy/hud/app/util/OTAUpdateService;->clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    move-object v0, v7

    .line 508
    goto/16 :goto_0
.end method

.method private cleanupOldFiles()V
    .locals 3

    .prologue
    .line 355
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/util/OTAUpdateService$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/util/OTAUpdateService$4;-><init>(Lcom/navdy/hud/app/util/OTAUpdateService;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 389
    return-void
.end method

.method public static clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 2
    .param p0, "file"    # Ljava/io/File;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 565
    if-eqz p0, :cond_0

    .line 566
    sget-object v0, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Clearing the update"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 567
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 569
    :cond_0
    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_update_file_received"

    .line 570
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "retry_count"

    .line 571
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "do_not_prompt"

    .line 572
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "verified"

    .line 573
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 574
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 575
    return-void
.end method

.method private static extractFromVersion(Ljava/lang/String;)I
    .locals 7
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 423
    sget-object v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sFileNamePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 424
    .local v1, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 425
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 426
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 427
    .local v3, "versionString":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 439
    .end local v3    # "versionString":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 431
    .restart local v3    # "versionString":Ljava/lang/String;
    :cond_1
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 432
    .local v2, "version":I
    goto :goto_0

    .line 433
    .end local v2    # "version":I
    :catch_0
    move-exception v0

    .line 434
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot parse the file to retrieve the from version"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 435
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method private static extractIncrementalVersion(Ljava/lang/String;)I
    .locals 7
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 399
    sget-object v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sFileNamePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 400
    .local v1, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 401
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 402
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 404
    .local v3, "versionString":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 412
    .end local v3    # "versionString":Ljava/lang/String;
    :goto_0
    return v2

    .line 406
    .restart local v3    # "versionString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot parse the file to retrieve the target version"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 408
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 412
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v3    # "versionString":Ljava/lang/String;
    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getCurrentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 561
    const-string v0, "1.3.3052-ce5463a"

    invoke-static {v0}, Lcom/navdy/hud/app/util/OTAUpdateService;->shortVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getIncrementalUpdateVersion(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 5
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v3, 0x0

    .line 514
    const-string v4, "last_update_file_received"

    invoke-interface {p0, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 515
    .local v1, "fileName":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 523
    :cond_0
    :goto_0
    return-object v3

    .line 518
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 519
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/hud/app/util/OTAUpdateService;->extractIncrementalVersion(Ljava/lang/String;)I

    move-result v2

    .line 520
    .local v2, "updateVersion":I
    if-lez v2, :cond_0

    .line 523
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getSWUpdateVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 725
    sget-object v0, Lcom/navdy/hud/app/util/OTAUpdateService;->swVersion:Ljava/lang/String;

    return-object v0
.end method

.method private static getUpdateVersion(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 11
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v6, 0x0

    .line 527
    const-string v9, "last_update_file_received"

    invoke-interface {p0, v9, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 528
    .local v3, "fileName":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 552
    :cond_0
    :goto_0
    return-object v6

    .line 531
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 532
    .local v2, "file":Ljava/io/File;
    const/4 v7, 0x0

    .line 533
    .local v7, "zipFile":Ljava/util/zip/ZipFile;
    const/4 v4, 0x0

    .line 534
    .local v4, "inputStream":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 536
    .local v6, "versionName":Ljava/lang/String;
    :try_start_0
    new-instance v8, Ljava/util/zip/ZipFile;

    invoke-direct {v8, v2}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    .end local v7    # "zipFile":Ljava/util/zip/ZipFile;
    .local v8, "zipFile":Ljava/util/zip/ZipFile;
    :try_start_1
    const-string v9, "META-INF/com/android/metadata"

    invoke-virtual {v8, v9}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v1

    .line 538
    .local v1, "entry":Ljava/util/zip/ZipEntry;
    new-instance v5, Ljava/util/Properties;

    invoke-direct {v5}, Ljava/util/Properties;-><init>()V

    .line 539
    .local v5, "properties":Ljava/util/Properties;
    invoke-virtual {v8, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v4

    .line 540
    invoke-virtual {v5, v4}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 541
    const-string v9, "version-name"

    invoke-virtual {v5, v9}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 542
    invoke-static {v6}, Lcom/navdy/hud/app/util/OTAUpdateService;->shortVersion(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 546
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 547
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object v7, v8

    .line 549
    .end local v1    # "entry":Ljava/util/zip/ZipEntry;
    .end local v5    # "properties":Ljava/util/Properties;
    .end local v8    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v7    # "zipFile":Ljava/util/zip/ZipFile;
    :goto_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 550
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "1.0."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p0}, Lcom/navdy/hud/app/util/OTAUpdateService;->getIncrementalUpdateVersion(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 543
    :catch_0
    move-exception v0

    .line 544
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_2
    sget-object v9, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Error extracting version-name from ota metadata "

    invoke-virtual {v9, v10, v0}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 546
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 547
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto :goto_1

    .line 546
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_3
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 547
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v9

    .line 546
    .end local v7    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v8    # "zipFile":Ljava/util/zip/ZipFile;
    :catchall_1
    move-exception v9

    move-object v7, v8

    .end local v8    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v7    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_3

    .line 543
    .end local v7    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v8    # "zipFile":Ljava/util/zip/ZipFile;
    :catch_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v7    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_2
.end method

.method private installPackage(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "postUpdateCommand"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 156
    sget-object v4, Lcom/navdy/hud/app/event/Shutdown$Reason;->OTA:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordShutdown(Lcom/navdy/hud/app/event/Shutdown$Reason;Z)V

    .line 159
    const-class v3, Landroid/os/RecoverySystem;

    .line 161
    .local v3, "recoverySystemClass":Ljava/lang/Class;
    :try_start_0
    const-string v4, "installPackage"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/Context;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/io/File;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 162
    .local v2, "installPackageMethod":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    const/4 v6, 0x2

    aput-object p3, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 175
    .end local v2    # "installPackageMethod":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v1

    .line 165
    .local v1, "ie":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 166
    .local v0, "e":Ljava/lang/Throwable;
    instance-of v4, v0, Ljava/io/IOException;

    if-eqz v4, :cond_0

    .line 167
    check-cast v0, Ljava/io/IOException;

    .end local v0    # "e":Ljava/lang/Throwable;
    throw v0

    .line 169
    .restart local v0    # "e":Ljava/lang/Throwable;
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected exception from RecoverySystem.installPackage()"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 170
    .end local v0    # "e":Ljava/lang/Throwable;
    .end local v1    # "ie":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "error invoking Recovery.installPackage(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 173
    invoke-static {p1, p2}, Landroid/os/RecoverySystem;->installPackage(Landroid/content/Context;Ljava/io/File;)V

    goto :goto_0
.end method

.method public static isOTAUpdateVerified(Landroid/content/SharedPreferences;)Z
    .locals 2
    .param p0, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 608
    const-string v0, "verified"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isOtaFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 449
    sget-object v1, Lcom/navdy/hud/app/util/OTAUpdateService;->sFileNamePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 450
    .local v0, "match":Ljava/util/regex/Matcher;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUpdateAvailable()Z
    .locals 1

    .prologue
    .line 579
    sget-object v0, Lcom/navdy/hud/app/util/OTAUpdateService;->swVersion:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reportOTAFailure(Ljava/lang/String;)V
    .locals 8
    .param p1, "updateFileName"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v5, p0, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v6, "failed_ota_file"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "lastFailedUpdate":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 270
    sget-object v5, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update failure already reported for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 297
    :goto_0
    return-void

    .line 273
    :cond_0
    const-string v3, ""

    .line 274
    .local v3, "last_install":Ljava/lang/String;
    const-string v4, ""

    .line 276
    .local v4, "last_log":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n========== /cache/recovery/last_install\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/cache/recovery/last_install"

    .line 277
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 283
    :goto_1
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n========== /cache/recovery/last_log\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/cache/recovery/last_log"

    .line 284
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 289
    :goto_2
    new-instance v1, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    .local v1, "exception":Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v5

    new-instance v6, Lcom/navdy/hud/app/util/OTAUpdateService$3;

    invoke-direct {v6, p0, v1}, Lcom/navdy/hud/app/util/OTAUpdateService$3;-><init>(Lcom/navdy/hud/app/util/OTAUpdateService;Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;)V

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 296
    iget-object v5, p0, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "failed_ota_file"

    invoke-interface {v5, v6, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 278
    .end local v1    # "exception":Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "\n*** failed to read /cache/recovery/last_install"

    .line 280
    sget-object v5, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "exception reading /cache/recovery/last_install"

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 285
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 286
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "\n*** failed to read /cache/recovery/last_log"

    .line 287
    sget-object v5, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "exception reading /cache/recovery/last_log"

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static shortVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "versionName"    # Ljava/lang/String;

    .prologue
    .line 557
    if-eqz p0, :cond_0

    const-string v0, "-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static startServiceToVerifyUpdate()V
    .locals 4

    .prologue
    .line 670
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 671
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 672
    .local v1, "updateServiceIntent":Landroid/content/Intent;
    const-string v2, "COMMAND"

    const-string v3, "VERIFY_UPDATE"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 673
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 674
    return-void
.end method

.method private update(Ljava/lang/String;)V
    .locals 3
    .param p1, "postUpdateCommand"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 178
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/OTAUpdateService;->installing:Z

    if-eqz v0, :cond_0

    .line 225
    :goto_0
    return-void

    .line 182
    :cond_0
    iput-boolean v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService;->installing:Z

    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/util/OTAUpdateService;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/util/OTAUpdateService$InstallingUpdate;

    invoke-direct {v1}, Lcom/navdy/hud/app/util/OTAUpdateService$InstallingUpdate;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 185
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/util/OTAUpdateService$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/util/OTAUpdateService$1;-><init>(Lcom/navdy/hud/app/util/OTAUpdateService;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static verifyOTAZipFile(Ljava/io/File;IJ)Z
    .locals 22
    .param p0, "file"    # Ljava/io/File;
    .param p1, "maxEntries"    # I
    .param p2, "maxSize"    # J

    .prologue
    .line 622
    const/16 v16, 0x0

    .line 624
    .local v16, "zipFile":Ljava/util/zip/ZipFile;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    .line 625
    .local v5, "destinationDirectory":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    .line 626
    .local v4, "destinationCanonicalPath":Ljava/lang/String;
    new-instance v17, Ljava/util/zip/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 627
    .end local v16    # "zipFile":Ljava/util/zip/ZipFile;
    .local v17, "zipFile":Ljava/util/zip/ZipFile;
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v7

    .line 628
    .local v7, "entries":Ljava/util/Enumeration;
    const/4 v9, 0x0

    .line 629
    .local v9, "entryCount":I
    const-wide/16 v12, 0x0

    .line 630
    .local v12, "uncompressedSize":J
    :cond_0
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 631
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/zip/ZipEntry;

    .line 632
    .local v8, "entry":Ljava/util/zip/ZipEntry;
    add-int/lit8 v9, v9, 0x1

    .line 633
    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v18

    if-nez v18, :cond_1

    .line 634
    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v10

    .line 635
    .local v10, "size":J
    const-wide/16 v18, 0x0

    cmp-long v18, v10, v18

    if-lez v18, :cond_1

    .line 636
    add-long/2addr v12, v10

    .line 640
    .end local v10    # "size":J
    :cond_1
    move/from16 v0, p1

    if-le v9, v0, :cond_2

    .line 641
    sget-object v18, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "OTA zip file failed verification : Too many entries "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 642
    const/16 v18, 0x0

    .line 664
    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object/from16 v16, v17

    .line 666
    .end local v4    # "destinationCanonicalPath":Ljava/lang/String;
    .end local v5    # "destinationDirectory":Ljava/io/File;
    .end local v7    # "entries":Ljava/util/Enumeration;
    .end local v8    # "entry":Ljava/util/zip/ZipEntry;
    .end local v9    # "entryCount":I
    .end local v12    # "uncompressedSize":J
    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v16    # "zipFile":Ljava/util/zip/ZipFile;
    :goto_0
    return v18

    .line 646
    .end local v16    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v4    # "destinationCanonicalPath":Ljava/lang/String;
    .restart local v5    # "destinationDirectory":Ljava/io/File;
    .restart local v7    # "entries":Ljava/util/Enumeration;
    .restart local v8    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v9    # "entryCount":I
    .restart local v12    # "uncompressedSize":J
    .restart local v17    # "zipFile":Ljava/util/zip/ZipFile;
    :cond_2
    cmp-long v18, v12, p2

    if-lez v18, :cond_3

    .line 647
    :try_start_2
    sget-object v18, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "OTA zip file failed verification : Exceeded max uncompressed size "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 648
    const/16 v18, 0x0

    .line 664
    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object/from16 v16, v17

    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v16    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_0

    .line 652
    .end local v16    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v17    # "zipFile":Ljava/util/zip/ZipFile;
    :cond_3
    :try_start_3
    new-instance v15, Ljava/io/File;

    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v15, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 653
    .local v15, "zipEntryFile":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v14

    .line 654
    .local v14, "zipEntryCanonicalPath":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_4

    invoke-virtual {v14, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 655
    :cond_4
    sget-object v18, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "OTA zip failed verification : Zip entry tried to write outside destination "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " , Canonical path "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 656
    const/16 v18, 0x0

    .line 664
    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object/from16 v16, v17

    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v16    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_0

    .line 659
    .end local v8    # "entry":Ljava/util/zip/ZipEntry;
    .end local v14    # "zipEntryCanonicalPath":Ljava/lang/String;
    .end local v15    # "zipEntryFile":Ljava/io/File;
    .end local v16    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v17    # "zipFile":Ljava/util/zip/ZipFile;
    :cond_5
    const/16 v18, 0x1

    .line 664
    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object/from16 v16, v17

    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v16    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_0

    .line 660
    .end local v4    # "destinationCanonicalPath":Ljava/lang/String;
    .end local v5    # "destinationDirectory":Ljava/io/File;
    .end local v7    # "entries":Ljava/util/Enumeration;
    .end local v9    # "entryCount":I
    .end local v12    # "uncompressedSize":J
    :catch_0
    move-exception v6

    .line 661
    .local v6, "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    sget-object v18, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Exception while verifying the OTA package "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 662
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 664
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 666
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 664
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    :goto_2
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v18

    .end local v16    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v4    # "destinationCanonicalPath":Ljava/lang/String;
    .restart local v5    # "destinationDirectory":Ljava/io/File;
    .restart local v17    # "zipFile":Ljava/util/zip/ZipFile;
    :catchall_1
    move-exception v18

    move-object/from16 v16, v17

    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v16    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_2

    .line 660
    .end local v16    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v17    # "zipFile":Ljava/util/zip/ZipFile;
    :catch_1
    move-exception v6

    move-object/from16 v16, v17

    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v16    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_1
.end method


# virtual methods
.method public bVerifyUpdate(Ljava/io/File;Landroid/content/SharedPreferences;)Z
    .locals 4
    .param p1, "file"    # Ljava/io/File;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 584
    :try_start_0
    new-instance v1, Lcom/navdy/hud/app/util/OTAUpdateService$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/util/OTAUpdateService$5;-><init>(Lcom/navdy/hud/app/util/OTAUpdateService;)V

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Landroid/os/RecoverySystem;->verifyPackage(Ljava/io/File;Landroid/os/RecoverySystem$ProgressListener;Ljava/io/File;)V

    .line 592
    const/16 v1, 0x3e8

    const-wide v2, 0x80000000L

    invoke-static {p1, v1, v2, v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->verifyOTAZipFile(Ljava/io/File;IJ)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 593
    const/4 v1, 0x1

    .line 596
    :goto_0
    return v1

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    sget-object v1, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while verifying the update package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 596
    const/4 v1, 0x0

    goto :goto_0

    .line 594
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public checkForUpdate()V
    .locals 3

    .prologue
    .line 232
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/util/OTAUpdateService$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/util/OTAUpdateService$2;-><init>(Lcom/navdy/hud/app/util/OTAUpdateService;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 265
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 462
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 119
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 120
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 124
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 125
    const-string v1, "COMMAND"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "command":Ljava/lang/String;
    sget-object v1, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OTA Update service started command["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 127
    const-string v1, "COMMAND"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 148
    sget-object v1, Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStartCommand() invalid command: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 151
    :cond_1
    :goto_1
    return v3

    .line 128
    :sswitch_0
    const-string v4, "INSTALL_UPDATE_REBOOT_QUIET"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v4, "INSTALL_UPDATE_SHUTDOWN"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "INSTALL_UPDATE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_3
    const-string v4, "DO_NOT_PROMPT"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v4, "VERIFY_UPDATE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 130
    :pswitch_0
    const-string v1, "--reboot_quiet_after"

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/util/OTAUpdateService;->update(Ljava/lang/String;)V

    goto :goto_1

    .line 134
    :pswitch_1
    const-string v1, "--shutdown_after"

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/util/OTAUpdateService;->update(Ljava/lang/String;)V

    goto :goto_1

    .line 137
    :pswitch_2
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/util/OTAUpdateService;->update(Ljava/lang/String;)V

    goto :goto_1

    .line 140
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "do_not_prompt"

    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 143
    :pswitch_4
    invoke-virtual {p0}, Lcom/navdy/hud/app/util/OTAUpdateService;->checkForUpdate()V

    .line 145
    invoke-direct {p0}, Lcom/navdy/hud/app/util/OTAUpdateService;->cleanupOldFiles()V

    goto :goto_1

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        -0x795f17f3 -> :sswitch_2
        -0x63aad414 -> :sswitch_0
        0xd91fe48 -> :sswitch_1
        0x6c1a634f -> :sswitch_4
        0x6f7bd4a4 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
