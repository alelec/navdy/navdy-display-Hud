.class public Lcom/navdy/hud/app/util/os/CpuProfiler;
.super Ljava/lang/Object;
.source "CpuProfiler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;,
        Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;
    }
.end annotation


# static fields
.field private static final HIGH_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

.field private static final HIGH_CPU_THREAD_NAME_PATTERN:[Ljava/lang/String;

.field private static final HIGH_CPU_USAGE_THRESHOLD:I = 0x55

.field private static final INITIAL_PERIODIC_INTERVAL:I

.field private static final LOW_NICE_PRIORITY:I = 0x13

.field private static final NAVDY_PROCESS_NAME:Ljava/lang/String; = "com.navdy.hud.app"

.field private static final NORMAL_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

.field private static final NORMAL_CPU_USAGE_THRESHOLD:I = 0x46

.field private static final PERIODIC_INTERVAL:I

.field private static final PROCESS_CPU_USAGE_THRESHOLD:I = 0x14

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/util/os/CpuProfiler;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private handler:Landroid/os/Handler;

.field private highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

.field private highCpuThreadOrigPrio:I

.field private highCpuUsage:Z

.field private periodicRunnable:Ljava/lang/Runnable;

.field private periodicRunnableBk:Ljava/lang/Runnable;

.field private running:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/os/CpuProfiler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 46
    new-instance v0, Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->HIGH:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;-><init>(Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;)V

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->HIGH_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    .line 47
    new-instance v0, Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->NORMAL:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;-><init>(Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;)V

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->NORMAL_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    .line 59
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2d

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->INITIAL_PERIODIC_INTERVAL:I

    .line 60
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->PERIODIC_INTERVAL:I

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NAVDY-HOGGER-"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Thread-"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->HIGH_CPU_THREAD_NAME_PATTERN:[Ljava/lang/String;

    .line 69
    new-instance v0, Lcom/navdy/hud/app/util/os/CpuProfiler;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->singleton:Lcom/navdy/hud/app/util/os/CpuProfiler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;

    .line 82
    new-instance v0, Lcom/navdy/hud/app/util/os/CpuProfiler$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/util/os/CpuProfiler$1;-><init>(Lcom/navdy/hud/app/util/os/CpuProfiler;)V

    iput-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;

    .line 89
    new-instance v0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/util/os/CpuProfiler$2;-><init>(Lcom/navdy/hud/app/util/os/CpuProfiler;)V

    iput-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnableBk:Ljava/lang/Runnable;

    .line 143
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->bus:Lcom/squareup/otto/Bus;

    .line 144
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/util/os/CpuProfiler;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnableBk:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/util/os/CpuProfiler;)Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->PERIODIC_INTERVAL:I

    return v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/util/os/CpuProfiler;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuUsage:Z

    return v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/util/os/CpuProfiler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuUsage:Z

    return p1
.end method

.method static synthetic access$300()Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->HIGH_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/util/os/CpuProfiler;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/util/os/CpuProfiler;Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;
    .param p1, "x1"    # Lcom/navdy/service/library/util/SystemUtils$CpuInfo;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->takeCorrectiveAction(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/util/os/CpuProfiler;Ljava/lang/String;IIILjava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # Ljava/util/ArrayList;

    .prologue
    .line 28
    invoke-direct/range {p0 .. p5}, Lcom/navdy/hud/app/util/os/CpuProfiler;->printCpuInfo(Ljava/lang/String;IIILjava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$700()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800()Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->NORMAL_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/util/os/CpuProfiler;Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;
    .param p1, "x1"    # Lcom/navdy/service/library/util/SystemUtils$CpuInfo;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->revertCorrectiveAction(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V

    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/util/os/CpuProfiler;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->singleton:Lcom/navdy/hud/app/util/os/CpuProfiler;

    return-object v0
.end method

.method private getKnownHighCpuThread(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .locals 7
    .param p1, "cpuInfo"    # Lcom/navdy/service/library/util/SystemUtils$CpuInfo;

    .prologue
    .line 243
    invoke-virtual {p1}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->getList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    .line 244
    .local v1, "info":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getProcessName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.navdy.hud.app"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 245
    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getThreadName()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "threadName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 249
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->HIGH_CPU_THREAD_NAME_PATTERN:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 250
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->HIGH_CPU_THREAD_NAME_PATTERN:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 251
    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getCpu()I

    move-result v4

    const/16 v5, 0x14

    if-lt v4, v5, :cond_1

    .line 252
    sget-object v3, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "high cpu thread found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getThreadName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getCpu()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 262
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .end local v2    # "threadName":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 255
    .restart local v0    # "i":I
    .restart local v1    # "info":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .restart local v2    # "threadName":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Thread found but cpu usage < threshold"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getThreadName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getCpu()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 249
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .end local v2    # "threadName":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private printCpuInfo(Ljava/lang/String;IIILjava/util/ArrayList;)V
    .locals 5
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "total"    # I
    .param p3, "user"    # I
    .param p4, "system"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "III",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;>;"
    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[CPU] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cpu="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "% user="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "% system="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 175
    invoke-virtual {p5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    .line 176
    .local v0, "info":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    const-string v2, "top"

    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getProcessName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 179
    sget-object v2, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[CPU] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getCpu()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "% process="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 180
    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getProcessName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " thread="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getThreadName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 181
    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getTid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 179
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 183
    .end local v0    # "info":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    :cond_1
    return-void
.end method

.method private revertCorrectiveAction(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    .locals 4
    .param p1, "cpuInfo"    # Lcom/navdy/service/library/util/SystemUtils$CpuInfo;

    .prologue
    .line 222
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "revertCorrectiveAction"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    .line 226
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    invoke-virtual {v1}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getTid()I

    move-result v1

    iget v2, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadOrigPrio:I

    invoke-static {v1, v2}, Landroid/os/Process;->setThreadPriority(II)V

    .line 227
    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "revertCorrectiveAction high cpu thread priority reduced from 19 to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadOrigPrio:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 232
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    .line 233
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadOrigPrio:I

    .line 240
    :goto_1
    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 237
    .end local v0    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v0

    .line 238
    .restart local v0    # "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 235
    .end local v0    # "t":Ljava/lang/Throwable;
    :cond_0
    :try_start_3
    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "revertCorrectiveAction high cpu thread was not detected previously"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1
.end method

.method private takeCorrectiveAction(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    .locals 7
    .param p1, "cpuInfo"    # Lcom/navdy/service/library/util/SystemUtils$CpuInfo;

    .prologue
    .line 187
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "takeCorrectiveAction"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 188
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->getKnownHighCpuThread(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    move-result-object v0

    .line 189
    .local v0, "highCpuThread":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    if-eqz v0, :cond_2

    .line 190
    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getTid()I

    move-result v3

    .line 191
    .local v3, "tid":I
    if-eqz v3, :cond_1

    .line 192
    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getTid()I

    move-result v4

    invoke-static {v4}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v1

    .line 193
    .local v1, "prio":I
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "takeCorrectiveAction: high cpu thread, priority["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 194
    iget-object v4, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    if-nez v4, :cond_0

    .line 196
    iput-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    .line 197
    iput v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadOrigPrio:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 199
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    invoke-virtual {v4}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getTid()I

    move-result v4

    const/16 v5, 0x13

    invoke-static {v4, v5}, Landroid/os/Process;->setThreadPriority(II)V

    .line 200
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "takeCorrectiveAction high cpu thread, priority changed from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->getTid()I

    move-result v6

    invoke-static {v6}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 218
    .end local v0    # "highCpuThread":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .end local v1    # "prio":I
    .end local v3    # "tid":I
    :goto_0
    return-void

    .line 201
    .restart local v0    # "highCpuThread":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .restart local v1    # "prio":I
    .restart local v3    # "tid":I
    :catch_0
    move-exception v2

    .line 202
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 203
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    .line 204
    const/4 v4, 0x0

    iput v4, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadOrigPrio:I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 215
    .end local v0    # "highCpuThread":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .end local v1    # "prio":I
    .end local v2    # "t":Ljava/lang/Throwable;
    .end local v3    # "tid":I
    :catch_1
    move-exception v2

    .line 216
    .restart local v2    # "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 207
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v0    # "highCpuThread":Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    .restart local v1    # "prio":I
    .restart local v3    # "tid":I
    :cond_0
    :try_start_3
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "takeCorrectiveAction high cpu thread, priority already reduced"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 210
    .end local v1    # "prio":I
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "takeCorrectiveAction high cpu thread tid is not valid"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 213
    .end local v3    # "tid":I
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "takeCorrectiveAction high cpu thread not found"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized start()V
    .locals 4

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z

    if-eqz v0, :cond_0

    .line 148
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :goto_0
    monitor-exit p0

    return-void

    .line 152
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 153
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/navdy/hud/app/util/os/CpuProfiler;->INITIAL_PERIODIC_INTERVAL:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z

    .line 155
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 2

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z

    if-nez v0, :cond_0

    .line 160
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "not running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    :goto_0
    monitor-exit p0

    return-void

    .line 164
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z

    .line 166
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopped"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
