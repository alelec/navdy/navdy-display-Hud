.class Lcom/navdy/hud/app/util/os/CpuProfiler$2;
.super Ljava/lang/Object;
.source "CpuProfiler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/os/CpuProfiler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/os/CpuProfiler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/os/CpuProfiler;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1300(Lcom/navdy/hud/app/util/os/CpuProfiler;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Ljava/lang/Runnable;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->PERIODIC_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1200()I

    move-result v5

    int-to-long v8, v5

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/navdy/service/library/util/SystemUtils;->getCpuUsage()Lcom/navdy/service/library/util/SystemUtils$CpuInfo;

    move-result-object v6

    .line 98
    .local v6, "cpuInfo":Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
    invoke-virtual {v6}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->getCpuUser()I

    move-result v3

    .line 99
    .local v3, "user":I
    invoke-virtual {v6}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->getCpuSystem()I

    move-result v4

    .line 100
    .local v4, "system":I
    add-int v2, v3, v4

    .line 102
    .local v2, "total":I
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuUsage:Z
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$200(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 103
    const/16 v0, 0x55

    if-le v2, v0, :cond_3

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuUsage:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$202(Lcom/navdy/hud/app/util/os/CpuProfiler;Z)Z

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$400(Lcom/navdy/hud/app/util/os/CpuProfiler;)Lcom/squareup/otto/Bus;

    move-result-object v0

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->HIGH_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$300()Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # invokes: Lcom/navdy/hud/app/util/os/CpuProfiler;->takeCorrectiveAction(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    invoke-static {v0, v6}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$500(Lcom/navdy/hud/app/util/os/CpuProfiler;Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    const-string v1, "HIGH"

    invoke-virtual {v6}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->getList()Ljava/util/ArrayList;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/util/os/CpuProfiler;->printCpuInfo(Ljava/lang/String;IIILjava/util/ArrayList;)V
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$600(Lcom/navdy/hud/app/util/os/CpuProfiler;Ljava/lang/String;IIILjava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1300(Lcom/navdy/hud/app/util/os/CpuProfiler;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Ljava/lang/Runnable;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->PERIODIC_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1200()I

    move-result v5

    int-to-long v8, v5

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 110
    :cond_3
    :try_start_2
    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    const-string v1, "NORMAL"

    invoke-virtual {v6}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->getList()Ljava/util/ArrayList;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/util/os/CpuProfiler;->printCpuInfo(Ljava/lang/String;IIILjava/util/ArrayList;)V
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$600(Lcom/navdy/hud/app/util/os/CpuProfiler;Ljava/lang/String;IIILjava/util/ArrayList;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 129
    .end local v2    # "total":I
    .end local v3    # "user":I
    .end local v4    # "system":I
    .end local v6    # "cpuInfo":Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
    :catch_0
    move-exception v7

    .line 130
    .local v7, "t":Ljava/lang/Throwable;
    :try_start_3
    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1300(Lcom/navdy/hud/app/util/os/CpuProfiler;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Ljava/lang/Runnable;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->PERIODIC_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1200()I

    move-result v5

    int-to-long v8, v5

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 116
    .end local v7    # "t":Ljava/lang/Throwable;
    .restart local v2    # "total":I
    .restart local v3    # "user":I
    .restart local v4    # "system":I
    .restart local v6    # "cpuInfo":Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
    :cond_4
    const/16 v0, 0x46

    if-ge v2, v0, :cond_6

    .line 117
    :try_start_4
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuUsage:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$202(Lcom/navdy/hud/app/util/os/CpuProfiler;Z)Z

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$400(Lcom/navdy/hud/app/util/os/CpuProfiler;)Lcom/squareup/otto/Bus;

    move-result-object v0

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->NORMAL_CPU:Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$800()Lcom/navdy/hud/app/util/os/CpuProfiler$CpuUsage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # invokes: Lcom/navdy/hud/app/util/os/CpuProfiler;->revertCorrectiveAction(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    invoke-static {v0, v6}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$900(Lcom/navdy/hud/app/util/os/CpuProfiler;Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    const-string v1, "NOW NORMAL"

    invoke-virtual {v6}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->getList()Ljava/util/ArrayList;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/util/os/CpuProfiler;->printCpuInfo(Ljava/lang/String;IIILjava/util/ArrayList;)V
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$600(Lcom/navdy/hud/app/util/os/CpuProfiler;Ljava/lang/String;IIILjava/util/ArrayList;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 132
    .end local v2    # "total":I
    .end local v3    # "user":I
    .end local v4    # "system":I
    .end local v6    # "cpuInfo":Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->running:Z
    invoke-static {v1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 133
    iget-object v1, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1300(Lcom/navdy/hud/app/util/os/CpuProfiler;)Landroid/os/Handler;

    move-result-object v1

    iget-object v5, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1100(Lcom/navdy/hud/app/util/os/CpuProfiler;)Ljava/lang/Runnable;

    move-result-object v5

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->PERIODIC_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1200()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v1, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_5
    throw v0

    .line 122
    .restart local v2    # "total":I
    .restart local v3    # "user":I
    .restart local v4    # "system":I
    .restart local v6    # "cpuInfo":Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
    :cond_6
    :try_start_5
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    const-string v1, "STILL HIGH"

    invoke-virtual {v6}, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->getList()Ljava/util/ArrayList;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/util/os/CpuProfiler;->printCpuInfo(Ljava/lang/String;IIILjava/util/ArrayList;)V
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$600(Lcom/navdy/hud/app/util/os/CpuProfiler;Ljava/lang/String;IIILjava/util/ArrayList;)V

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # getter for: Lcom/navdy/hud/app/util/os/CpuProfiler;->highCpuThreadInfo:Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$1000(Lcom/navdy/hud/app/util/os/CpuProfiler;)Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;

    move-result-object v0

    if-nez v0, :cond_2

    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/util/os/CpuProfiler$2;->this$0:Lcom/navdy/hud/app/util/os/CpuProfiler;

    # invokes: Lcom/navdy/hud/app/util/os/CpuProfiler;->takeCorrectiveAction(Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    invoke-static {v0, v6}, Lcom/navdy/hud/app/util/os/CpuProfiler;->access$500(Lcom/navdy/hud/app/util/os/CpuProfiler;Lcom/navdy/service/library/util/SystemUtils$CpuInfo;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1
.end method
