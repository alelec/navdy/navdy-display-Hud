.class public final enum Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;
.super Ljava/lang/Enum;
.source "CpuProfiler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/os/CpuProfiler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Usage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

.field public static final enum HIGH:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

.field public static final enum NORMAL:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->HIGH:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    .line 33
    new-instance v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->NORMAL:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->HIGH:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->NORMAL:Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->$VALUES:[Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->$VALUES:[Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/util/os/CpuProfiler$Usage;

    return-object v0
.end method
