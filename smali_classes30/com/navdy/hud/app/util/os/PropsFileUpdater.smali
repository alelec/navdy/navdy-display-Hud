.class public Lcom/navdy/hud/app/util/os/PropsFileUpdater;
.super Ljava/lang/Object;
.source "PropsFileUpdater.java"


# static fields
.field private static final CHAR_SET:Ljava/lang/String; = "UTF-8"

.field private static final COMMAND_GETPROP:Ljava/lang/String; = "getprop"

.field private static final PROPS_FILE_INTENT:I = 0x2

.field private static final PROPS_FILE_NAME:Ljava/lang/String; = "system_info.json"

.field private static final PROPS_FILE_PATH_PROP_NAME:Ljava/lang/String; = "ro.maps_partition"

.field private static final PROPS_KEY_VALUE_SEPARATOR:Ljava/util/regex/Pattern;

.field private static final PROPS_READOUT_DELAY:I = 0x3a98

.field private static final PROPS_TEMP_FILE_SUFFIX:Ljava/lang/String; = "~"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/os/PropsFileUpdater;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 34
    const-string v0, "\\]: \\["

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->PROPS_KEY_VALUE_SEPARATOR:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-static {p0}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->updatePropsFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private static propsToJSON(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 12
    .param p0, "props"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 98
    const-string v7, "line.separator"

    invoke-static {v7}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 100
    .local v4, "propLines":[Ljava/lang/String;
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 101
    .local v1, "jsonObject":Lorg/json/JSONObject;
    array-length v7, v4

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v3, v4, v6

    .line 102
    .local v3, "propLine":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 106
    .local v5, "trimmedLine":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v5, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 107
    sget-object v8, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->PROPS_KEY_VALUE_SEPARATOR:Ljava/util/regex/Pattern;

    const/4 v9, 0x2

    invoke-virtual {v8, v5, v9}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;I)[Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "keyValuePair":[Ljava/lang/String;
    const/4 v8, 0x0

    :try_start_0
    aget-object v8, v2, v8

    const/4 v9, 0x1

    aget-object v9, v2, v9

    invoke-virtual {v1, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot create json for prop: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " - skipping"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 114
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v2    # "keyValuePair":[Ljava/lang/String;
    .end local v3    # "propLine":Ljava/lang/String;
    .end local v5    # "trimmedLine":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method public static readProps()Ljava/lang/String;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 119
    const/4 v2, 0x0

    .line 120
    .local v2, "readerProcess":Ljava/lang/Process;
    const/4 v1, 0x0

    .line 122
    .local v1, "props":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/lang/ProcessBuilder;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "getprop"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/ProcessBuilder;->command([Ljava/lang/String;)Ljava/lang/ProcessBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I

    .line 124
    sget-object v3, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Reading props process ended with exit value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Process;->exitValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 126
    invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    const-string v4, "UTF-8"

    .line 125
    invoke-static {v3, v4}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 130
    if-eqz v2, :cond_0

    .line 131
    invoke-virtual {v2}, Ljava/lang/Process;->destroy()V

    .line 135
    :cond_0
    :goto_0
    return-object v1

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Cannot execute getprop command"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    if-eqz v2, :cond_0

    .line 131
    invoke-virtual {v2}, Ljava/lang/Process;->destroy()V

    goto :goto_0

    .line 130
    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_1

    .line 131
    invoke-virtual {v2}, Ljava/lang/Process;->destroy()V

    :cond_1
    throw v3
.end method

.method public static run()V
    .locals 4

    .prologue
    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/navdy/hud/app/util/os/PropsFileUpdater$1;

    invoke-direct {v1}, Lcom/navdy/hud/app/util/os/PropsFileUpdater$1;-><init>()V

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 57
    return-void
.end method

.method private static updatePropsFile(Ljava/lang/String;)V
    .locals 4
    .param p0, "props"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 60
    if-nez p0, :cond_0

    .line 61
    sget-object v2, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Cannot update file with empty props string"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 73
    :goto_0
    return-void

    .line 66
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->propsToJSON(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 67
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "ro.maps_partition"

    .line 68
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 67
    invoke-static {v2, v3}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->updatePropsFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Cannot build JSON string with props or read path for output file"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static updatePropsFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "destinationFileFullPath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "json"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 77
    new-instance v3, Ljava/io/File;

    const-string v4, "system_info.json"

    invoke-direct {v3, p0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "propsFileFullPath":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "~"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "propsTempFileFullPath":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/io/InputStream;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :try_start_1
    invoke-static {v2, v1}, Landroid/system/Os;->rename(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-object v3, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Props file updated successfully"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/system/ErrnoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 94
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception while writing into file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 91
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 92
    .local v0, "e":Landroid/system/ErrnoException;
    sget-object v3, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Cannot overwrite props file with the new one"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
