.class Lcom/navdy/hud/app/util/os/PropsFileUpdater$1$UpdatePropsFileRunnable;
.super Ljava/lang/Object;
.source "PropsFileUpdater.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/os/PropsFileUpdater$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdatePropsFileRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/os/PropsFileUpdater$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/os/PropsFileUpdater$1;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/os/PropsFileUpdater$1;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/navdy/hud/app/util/os/PropsFileUpdater$1$UpdatePropsFileRunnable;->this$0:Lcom/navdy/hud/app/util/os/PropsFileUpdater$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 44
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->readProps()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/navdy/hud/app/util/os/PropsFileUpdater;->updatePropsFile(Ljava/lang/String;)V
    invoke-static {v1}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->access$000(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/util/os/PropsFileUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Error while updating props file"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
