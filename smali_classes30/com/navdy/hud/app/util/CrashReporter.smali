.class public final Lcom/navdy/hud/app/util/CrashReporter;
.super Ljava/lang/Object;
.source "CrashReporter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;,
        Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;,
        Lcom/navdy/hud/app/util/CrashReporter$AnrException;,
        Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;,
        Lcom/navdy/hud/app/util/CrashReporter$CrashException;
    }
.end annotation


# static fields
.field private static final ANR_LOG_LIMIT:I = 0x8000

.field private static final ANR_PATH:Ljava/lang/String; = "/data/anr/traces.txt"

.field public static final HOCKEY_APP_ID:Ljava/lang/String; = "20625a4f769df641ecf46825a71c1911"

.field public static final HOCKEY_APP_STABLE_ID:Ljava/lang/String; = "4dd8d996ee304ef2994965aa7f6d4f15"

.field public static final HOCKEY_APP_UNSTABLE_ID:Ljava/lang/String; = "ad4b9dbcbb3b4083a49af48878026e5a"

.field private static final KERNEL_CRASH_INFO_LIMIT:I = 0x8000

.field private static final LOG_ANR_MARKER:Ljava/lang/String; = "Wrote stack traces to \'/data/anr/traces.txt\'"

.field private static final LOG_BEGIN_MARKER:Ljava/lang/String; = "--------- beginning of main"

.field private static final PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL:I

.field private static final PERIOD_DELIVERY_CHECK_INTERVAL:I

.field private static final START_OF_OOPS:Ljava/util/regex/Pattern;

.field private static final SYSTEM_LOG_LIMIT:I = 0x8000

.field private static final TOMBSTONE_FILE_PATTERN:Ljava/lang/String; = "tombstone_light_"

.field private static final TOMBSTONE_PATH:Ljava/lang/String; = "/data/tombstones"

.field public static final USER_REBOOT:Ljava/util/regex/Pattern;

.field private static driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field private static final sInstance:Lcom/navdy/hud/app/util/CrashReporter;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static volatile stopCrashReporting:Z

.field private static userId:Ljava/lang/String;


# instance fields
.field private crashManagerListener:Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;

.field private deliveryRunnable:Ljava/lang/Runnable;

.field private handler:Landroid/os/Handler;

.field private installed:Z

.field private otaFailure:Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

.field private periodicRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/CrashReporter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 52
    const-string v0, "(Unable to handle)|(Internal error:)"

    .line 53
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter;->START_OF_OOPS:Ljava/util/regex/Pattern;

    .line 54
    const-string v0, "(reboot: Restarting system with command)|(do_powerctl:)"

    .line 55
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter;->USER_REBOOT:Ljava/util/regex/Pattern;

    .line 57
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/util/CrashReporter;->PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL:I

    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/util/CrashReporter;->PERIOD_DELIVERY_CHECK_INTERVAL:I

    .line 69
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter;->userId:Ljava/lang/String;

    .line 225
    new-instance v0, Lcom/navdy/hud/app/util/CrashReporter;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/CrashReporter;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter;->sInstance:Lcom/navdy/hud/app/util/CrashReporter;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    new-instance v0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->crashManagerListener:Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;

    .line 238
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->handler:Landroid/os/Handler;

    .line 240
    new-instance v0, Lcom/navdy/hud/app/util/CrashReporter$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/util/CrashReporter$1;-><init>(Lcom/navdy/hud/app/util/CrashReporter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->periodicRunnable:Ljava/lang/Runnable;

    .line 247
    new-instance v0, Lcom/navdy/hud/app/util/CrashReporter$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/util/CrashReporter$2;-><init>(Lcom/navdy/hud/app/util/CrashReporter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->deliveryRunnable:Ljava/lang/Runnable;

    .line 519
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->otaFailure:Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    .line 232
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/util/CrashReporter;->periodicRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/navdy/hud/app/util/CrashReporter;->PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 233
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 234
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/util/CrashReporter;->userId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/hud/app/profile/DriverProfileManager;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/util/CrashReporter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/util/CrashReporter;[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/CrashReporter;->cleanupKernelCrashes([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/util/CrashReporter;)Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->crashManagerListener:Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/util/CrashReporter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->deliveryRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/util/CrashReporter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/navdy/hud/app/util/CrashReporter;->checkForCrashes()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/util/CrashReporter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->periodicRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lcom/navdy/hud/app/util/CrashReporter;->PERIOD_DELIVERY_CHECK_INTERVAL:I

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/util/CrashReporter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/util/CrashReporter;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;
    .param p1, "x1"    # Ljava/lang/Thread;
    .param p2, "x2"    # Ljava/lang/Throwable;
    .param p3, "x3"    # Ljava/lang/Thread$UncaughtExceptionHandler;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/util/CrashReporter;->handleUncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/util/CrashReporter;Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/CrashReporter;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/CrashReporter;->filterKernelCrash(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkForCrashes()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 484
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HOCKEY:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isNetworkAccessAllowed(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 495
    :goto_0
    return v3

    .line 490
    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter;->crashManagerListener:Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;

    invoke-static {v1, v2}, Lnet/hockeyapp/android/CrashManager;->submitStackTraces(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/CrashManagerListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 491
    :catch_0
    move-exception v0

    .line 492
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private cleanupAnrFiles()V
    .locals 5

    .prologue
    .line 542
    new-instance v0, Ljava/io/File;

    const-string v2, "/data/anr/traces.txt"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 543
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 544
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 545
    .local v1, "ret":Z
    sget-object v2, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delete /data/anr/traces.txt :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 547
    .end local v1    # "ret":Z
    :cond_0
    return-void
.end method

.method private cleanupKernelCrashes([Ljava/lang/String;)V
    .locals 8
    .param p1, "kernelCrashFiles"    # [Ljava/lang/String;

    .prologue
    .line 415
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, p1, v3

    .line 416
    .local v0, "crashFile":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 417
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 418
    sget-object v5, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Trying to delete "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 419
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    .line 420
    .local v2, "worked":Z
    if-nez v2, :cond_0

    .line 421
    sget-object v5, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to delete kernel crash file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 415
    .end local v2    # "worked":Z
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 425
    .end local v0    # "crashFile":Ljava/lang/String;
    .end local v1    # "f":Ljava/io/File;
    :cond_1
    return-void
.end method

.method private filterKernelCrash(Ljava/io/File;)Ljava/lang/String;
    .locals 8
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 386
    const/4 v4, 0x0

    .line 388
    .local v4, "str":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 389
    if-eqz v4, :cond_1

    .line 391
    sget-object v6, Lcom/navdy/hud/app/util/CrashReporter;->USER_REBOOT:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 392
    .local v2, "rebootMatch":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-nez v6, :cond_2

    .line 393
    sget-object v6, Lcom/navdy/hud/app/util/CrashReporter;->START_OF_OOPS:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 395
    .local v1, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit16 v3, v6, -0x8000

    .line 396
    .local v3, "start":I
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v6

    if-ge v6, v3, :cond_0

    .line 398
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    .line 400
    :cond_0
    const/4 v6, 0x0

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 401
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v3

    const v7, 0x8000

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 402
    .local v0, "len":I
    invoke-virtual {v4, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 411
    .end local v0    # "len":I
    .end local v1    # "match":Ljava/util/regex/Matcher;
    .end local v2    # "rebootMatch":Ljava/util/regex/Matcher;
    .end local v3    # "start":I
    :cond_1
    :goto_0
    return-object v4

    .line 404
    .restart local v2    # "rebootMatch":Ljava/util/regex/Matcher;
    :cond_2
    sget-object v6, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Ignoring user reboot log"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    const/4 v4, 0x0

    goto :goto_0

    .line 408
    .end local v2    # "rebootMatch":Ljava/util/regex/Matcher;
    :catch_0
    move-exception v5

    .line 409
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/navdy/hud/app/util/CrashReporter;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lcom/navdy/hud/app/util/CrashReporter;->sInstance:Lcom/navdy/hud/app/util/CrashReporter;

    return-object v0
.end method

.method private handleUncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 6
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "exception"    # Ljava/lang/Throwable;
    .param p3, "exceptionHandler"    # Ljava/lang/Thread$UncaughtExceptionHandler;

    .prologue
    .line 328
    sget-boolean v2, Lcom/navdy/hud/app/util/CrashReporter;->stopCrashReporting:Z

    if-eqz v2, :cond_1

    .line 329
    sget-object v2, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "FATAL-reporting-turned-off"

    invoke-virtual {v2, v3, p2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    const/4 v2, 0x1

    sput-boolean v2, Lcom/navdy/hud/app/util/CrashReporter;->stopCrashReporting:Z

    .line 334
    const-string v1, "FATAL-CRASH"

    .line 335
    .local v1, "tag":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Uncaught exception - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ui thread id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 338
    sget-object v2, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "closing logger"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 339
    invoke-static {}, Lcom/navdy/service/library/log/Logger;->close()V

    .line 341
    if-eqz p3, :cond_0

    .line 342
    invoke-interface {p3, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static isEnabled()Z
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->isDeveloperBuild()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private saveHockeyAppException(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 551
    :try_start_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/util/CrashReporter$6;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/util/CrashReporter$6;-><init>(Lcom/navdy/hud/app/util/CrashReporter;Ljava/lang/Throwable;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 564
    :goto_0
    return-void

    .line 561
    :catch_0
    move-exception v0

    .line 562
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static setCrashReporting(Z)V
    .locals 0
    .param p0, "b"    # Z

    .prologue
    .line 479
    sput-boolean p0, Lcom/navdy/hud/app/util/CrashReporter;->stopCrashReporting:Z

    .line 480
    return-void
.end method

.method private setHockeyAppCrashUploading(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x1

    .line 572
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setHockeyAppCrashUploading: try to enabled:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 573
    const-class v3, Lnet/hockeyapp/android/CrashManager;

    const-string v4, "submitting"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 574
    .local v0, "f":Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 575
    const/4 v3, 0x0

    if-nez p1, :cond_0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 576
    sget-object v2, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setHockeyAppCrashUploading: enabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 580
    .end local v0    # "f":Ljava/lang/reflect/Field;
    :goto_1
    return-void

    .line 575
    .restart local v0    # "f":Ljava/lang/reflect/Field;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 577
    .end local v0    # "f":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 578
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "setHockeyAppCrashUploading"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public checkForAnr()V
    .locals 7

    .prologue
    .line 499
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 501
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "checking for anr\'s"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 502
    new-instance v1, Ljava/io/File;

    const-string v4, "/data/anr/traces.txt"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 503
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 504
    const-string v4, "/data/anr/traces.txt"

    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 505
    .local v2, "str":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 506
    new-instance v0, Lcom/navdy/hud/app/util/CrashReporter$AnrException;

    invoke-direct {v0, v2}, Lcom/navdy/hud/app/util/CrashReporter$AnrException;-><init>(Ljava/lang/String;)V

    .line 507
    .local v0, "anrException":Lcom/navdy/hud/app/util/CrashReporter$AnrException;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    .line 508
    sget-object v4, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "anr crash created size["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 510
    .end local v0    # "anrException":Lcom/navdy/hud/app/util/CrashReporter$AnrException;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/util/CrashReporter;->cleanupAnrFiles()V

    .line 517
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 512
    .restart local v1    # "f":Ljava/io/File;
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "no anr files"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 514
    .end local v1    # "f":Ljava/io/File;
    :catch_0
    move-exception v3

    .line 515
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public checkForKernelCrashes([Ljava/lang/String;)V
    .locals 3
    .param p1, "kernelCrashFiles"    # [Ljava/lang/String;

    .prologue
    .line 347
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/util/CrashReporter$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/util/CrashReporter$4;-><init>(Lcom/navdy/hud/app/util/CrashReporter;[Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public declared-synchronized checkForOTAFailure()V
    .locals 1

    .prologue
    .line 535
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->otaFailure:Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->otaFailure:Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    .line 537
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->otaFailure:Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 539
    :cond_0
    monitor-exit p0

    return-void

    .line 535
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public checkForTombstones()V
    .locals 12

    .prologue
    .line 428
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 430
    :try_start_0
    sget-object v9, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "checking for tombstones"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 431
    new-instance v0, Ljava/io/File;

    const-string v9, "/data/tombstones"

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 432
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 433
    new-instance v9, Lcom/navdy/hud/app/util/CrashReporter$5;

    invoke-direct {v9, p0}, Lcom/navdy/hud/app/util/CrashReporter$5;-><init>(Lcom/navdy/hud/app/util/CrashReporter;)V

    invoke-virtual {v0, v9}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v3

    .line 444
    .local v3, "list":[Ljava/lang/String;
    if-eqz v3, :cond_0

    array-length v9, v3

    if-nez v9, :cond_2

    .line 445
    :cond_0
    sget-object v9, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "no tombstones"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 471
    .end local v0    # "f":Ljava/io/File;
    .end local v3    # "list":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 449
    .restart local v0    # "f":Ljava/io/File;
    .restart local v3    # "list":[Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v9, v3

    if-ge v2, v9, :cond_1

    .line 450
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/data/tombstones"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v3, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 451
    .local v8, "tombstoneFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 452
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 453
    .local v1, "fileName":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 454
    .local v5, "str":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 455
    new-instance v7, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;

    invoke-direct {v7, v5}, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;-><init>(Ljava/lang/String;)V

    .line 456
    .local v7, "tombstoneException":Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;
    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    .line 457
    sget-object v9, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "tombstone exception logged file["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] size["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 461
    .end local v7    # "tombstoneException":Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;
    :cond_3
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v4

    .line 462
    .local v4, "ret":Z
    sget-object v9, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "tombstone deleted: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 449
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v4    # "ret":Z
    .end local v5    # "str":Ljava/lang/String;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 466
    .end local v2    # "i":I
    .end local v3    # "list":[Ljava/lang/String;
    .end local v8    # "tombstoneFile":Ljava/io/File;
    :cond_5
    sget-object v9, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "no tombstones"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 468
    .end local v0    # "f":Ljava/io/File;
    :catch_0
    move-exception v6

    .line 469
    .local v6, "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v9, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public declared-synchronized installCrashHandler(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/util/CrashReporter;->installed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 307
    :goto_0
    monitor-exit p0

    return-void

    .line 268
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v3

    if-nez v3, :cond_1

    .line 269
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-eng"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/util/CrashReporter;->userId:Ljava/lang/String;

    .line 273
    :cond_1
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    .line 274
    .local v1, "defaultUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    if-eqz v1, :cond_2

    .line 275
    sget-object v3, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "default uncaught handler:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 282
    :goto_1
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/util/CrashReporter;->setHockeyAppCrashUploading(Z)V

    .line 284
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v0, "20625a4f769df641ecf46825a71c1911"

    .line 287
    .local v0, "appId":Ljava/lang/String;
    :goto_2
    iget-object v3, p0, Lcom/navdy/hud/app/util/CrashReporter;->crashManagerListener:Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;

    invoke-static {p1, v0, v3}, Lnet/hockeyapp/android/CrashManager;->register(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 288
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getApplication()Lcom/navdy/hud/app/HudApplication;

    move-result-object v3

    invoke-static {p1, v3, v0}, Lnet/hockeyapp/android/metrics/MetricsManager;->register(Landroid/content/Context;Landroid/app/Application;Ljava/lang/String;)V

    .line 289
    sget-object v3, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "enabled metrics manager"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 291
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/util/CrashReporter;->setHockeyAppCrashUploading(Z)V

    .line 293
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    .line 294
    .local v2, "hockeyUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    if-eqz v2, :cond_4

    .line 295
    sget-object v3, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hockey uncaught handler:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 300
    :goto_3
    new-instance v3, Lcom/navdy/hud/app/util/CrashReporter$3;

    invoke-direct {v3, p0, v2}, Lcom/navdy/hud/app/util/CrashReporter$3;-><init>(Lcom/navdy/hud/app/util/CrashReporter;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v3}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 306
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/app/util/CrashReporter;->installed:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 264
    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "defaultUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    .end local v2    # "hockeyUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 277
    .restart local v1    # "defaultUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    :cond_2
    :try_start_2
    sget-object v3, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "default uncaught handler is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 284
    :cond_3
    const-string v0, "4dd8d996ee304ef2994965aa7f6d4f15"

    goto :goto_2

    .line 297
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v2    # "hockeyUncaughtHandler":Ljava/lang/Thread$UncaughtExceptionHandler;
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "default hockey handler is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method public isInstalled()Z
    .locals 1

    .prologue
    .line 567
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->installed:Z

    return v0
.end method

.method public log(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 323
    return-void
.end method

.method public reportNonFatalException(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 312
    iget-boolean v1, p0, Lcom/navdy/hud/app/util/CrashReporter;->installed:Z

    if-eqz v1, :cond_0

    .line 314
    :try_start_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/CrashReporter;->saveHockeyAppException(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 315
    :catch_0
    move-exception v0

    .line 316
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public declared-synchronized reportOTAFailure(Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;)V
    .locals 2
    .param p1, "failure"    # Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    .prologue
    .line 522
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->installed:Z

    if-eqz v0, :cond_0

    .line 524
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    :goto_0
    monitor-exit p0

    return-void

    .line 527
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter;->otaFailure:Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    if-eqz v0, :cond_1

    .line 528
    sget-object v0, Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "multiple OTAFailedException reports"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 530
    :cond_1
    iput-object p1, p0, Lcom/navdy/hud/app/util/CrashReporter;->otaFailure:Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stopCrashReporting(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 475
    invoke-static {p1}, Lcom/navdy/hud/app/util/CrashReporter;->setCrashReporting(Z)V

    .line 476
    return-void
.end method
