.class Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;
.super Ljava/lang/Object;
.source "OTAUpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

    .prologue
    .line 686
    iput-object p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

    iput-object p2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 689
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->val$path:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 690
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->isOtaFile(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 691
    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->sharedPreferences:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->access$800(Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "last_update_file_received"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 692
    .local v2, "oldUpdateFile":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 693
    .local v1, "newFileName":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->sharedPreferences:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->access$800(Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "last_update_file_received"

    .line 694
    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "do_not_prompt"

    .line 695
    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "retry_count"

    .line 696
    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "verified"

    .line 697
    invoke-interface {v3, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 698
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 699
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 700
    if-eqz v2, :cond_0

    .line 701
    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->val$context:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 702
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete the old update file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", new update :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 705
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->sharedPreferences:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->access$800(Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordDownloadOTAUpdate(Landroid/content/SharedPreferences;)V

    .line 708
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->startServiceToVerifyUpdate()V

    .line 710
    .end local v1    # "newFileName":Ljava/lang/String;
    .end local v2    # "oldUpdateFile":Ljava/lang/String;
    :cond_2
    return-void
.end method
