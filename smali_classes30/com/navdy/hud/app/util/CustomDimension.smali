.class public Lcom/navdy/hud/app/util/CustomDimension;
.super Ljava/lang/Object;
.source "CustomDimension.java"


# instance fields
.field private mAttribute:Landroid/util/TypedValue;

.field private mValue:F


# direct methods
.method public constructor <init>(F)V
    .locals 0
    .param p1, "fixedValue"    # F

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput p1, p0, Lcom/navdy/hud/app/util/CustomDimension;->mValue:F

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/util/TypedValue;)V
    .locals 0
    .param p1, "attribute"    # Landroid/util/TypedValue;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/navdy/hud/app/util/CustomDimension;->mAttribute:Landroid/util/TypedValue;

    .line 73
    return-void
.end method

.method public static getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/hud/app/util/CustomDimension;
    .locals 4
    .param p0, "v"    # Landroid/view/View;
    .param p1, "a"    # Landroid/content/res/TypedArray;
    .param p2, "index"    # I
    .param p3, "defValue"    # F

    .prologue
    .line 23
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 27
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "value":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 29
    new-instance v2, Lcom/navdy/hud/app/util/CustomDimension;

    invoke-direct {v2, p3}, Lcom/navdy/hud/app/util/CustomDimension;-><init>(F)V

    .line 49
    .end local v1    # "value":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 31
    .restart local v1    # "value":Ljava/lang/String;
    :cond_0
    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "%p"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 33
    :cond_1
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 34
    .local v0, "attributeValue":Landroid/util/TypedValue;
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 35
    new-instance v2, Lcom/navdy/hud/app/util/CustomDimension;

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/util/CustomDimension;-><init>(Landroid/util/TypedValue;)V

    goto :goto_0

    .line 38
    .end local v0    # "attributeValue":Landroid/util/TypedValue;
    :cond_2
    new-instance v2, Lcom/navdy/hud/app/util/CustomDimension;

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/util/CustomDimension;-><init>(F)V

    goto :goto_0

    .line 41
    .end local v1    # "value":Ljava/lang/String;
    :cond_3
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 42
    .restart local v0    # "attributeValue":Landroid/util/TypedValue;
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 43
    iget v2, v0, Landroid/util/TypedValue;->type:I

    if-nez v2, :cond_4

    .line 44
    new-instance v2, Lcom/navdy/hud/app/util/CustomDimension;

    invoke-direct {v2, p3}, Lcom/navdy/hud/app/util/CustomDimension;-><init>(F)V

    goto :goto_0

    .line 45
    :cond_4
    iget v2, v0, Landroid/util/TypedValue;->type:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_5

    .line 47
    new-instance v2, Lcom/navdy/hud/app/util/CustomDimension;

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/util/CustomDimension;-><init>(F)V

    goto :goto_0

    .line 49
    :cond_5
    new-instance v2, Lcom/navdy/hud/app/util/CustomDimension;

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/util/CustomDimension;-><init>(Landroid/util/TypedValue;)V

    goto :goto_0
.end method

.method public static hasDimension(Landroid/view/View;Landroid/content/res/TypedArray;I)Z
    .locals 5
    .param p0, "v"    # Landroid/view/View;
    .param p1, "a"    # Landroid/content/res/TypedArray;
    .param p2, "index"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 56
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 57
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 62
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .restart local v1    # "value":Ljava/lang/String;
    :cond_1
    move v2, v3

    .line 58
    goto :goto_0

    .line 60
    .end local v1    # "value":Ljava/lang/String;
    :cond_2
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 61
    .local v0, "attributeValue":Landroid/util/TypedValue;
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 62
    iget v4, v0, Landroid/util/TypedValue;->type:I

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method public getSize(Landroid/view/View;FF)F
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "base"    # F
    .param p3, "pBase"    # F

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/util/CustomDimension;->mAttribute:Landroid/util/TypedValue;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/util/CustomDimension;->mAttribute:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->type:I

    packed-switch v0, :pswitch_data_0

    .line 83
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attribute must have type fraction or float - it is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/util/CustomDimension;->mAttribute:Landroid/util/TypedValue;

    iget v2, v2, Landroid/util/TypedValue;->type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/util/CustomDimension;->mAttribute:Landroid/util/TypedValue;

    invoke-virtual {v0, p2, p3}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    .line 87
    :goto_0
    return v0

    .line 81
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/util/CustomDimension;->mAttribute:Landroid/util/TypedValue;

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    mul-float/2addr v0, p2

    goto :goto_0

    .line 87
    :cond_0
    iget v0, p0, Lcom/navdy/hud/app/util/CustomDimension;->mValue:F

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
