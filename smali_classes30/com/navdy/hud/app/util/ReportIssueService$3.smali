.class Lcom/navdy/hud/app/util/ReportIssueService$3;
.super Ljava/lang/Object;
.source "ReportIssueService.java"

# interfaces
.implements Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/ReportIssueService;->sync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/ReportIssueService;

.field final synthetic val$fileToSync:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/ReportIssueService;Ljava/io/File;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/ReportIssueService;

    .prologue
    .line 614
    iput-object p1, p0, Lcom/navdy/hud/app/util/ReportIssueService$3;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    iput-object p2, p0, Lcom/navdy/hud/app/util/ReportIssueService$3;->val$fileToSync:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 626
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error during sync "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$3;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    # invokes: Lcom/navdy/hud/app/util/ReportIssueService;->syncLater()V
    invoke-static {v0}, Lcom/navdy/hud/app/util/ReportIssueService;->access$200(Lcom/navdy/hud/app/util/ReportIssueService;)V

    .line 628
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 617
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 618
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Issue reported "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 620
    :cond_0
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Sync succeeded"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 621
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$3;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    iget-object v1, p0, Lcom/navdy/hud/app/util/ReportIssueService$3;->val$fileToSync:Ljava/io/File;

    # invokes: Lcom/navdy/hud/app/util/ReportIssueService;->onSyncComplete(Ljava/io/File;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/ReportIssueService;->access$100(Lcom/navdy/hud/app/util/ReportIssueService;Ljava/io/File;)V

    .line 622
    return-void
.end method
