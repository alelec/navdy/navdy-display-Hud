.class Lcom/navdy/hud/app/common/MainThreadBus$1;
.super Ljava/lang/Object;
.source "MainThreadBus.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/common/MainThreadBus;->post(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/common/MainThreadBus;

.field final synthetic val$event:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/common/MainThreadBus;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->this$0:Lcom/navdy/hud/app/common/MainThreadBus;

    iput-object p2, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->val$event:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 39
    const-wide/16 v2, 0x0

    .line 40
    .local v2, "l1":J
    iget-object v4, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->this$0:Lcom/navdy/hud/app/common/MainThreadBus;

    iget v4, v4, Lcom/navdy/hud/app/common/MainThreadBus;->threshold:I

    if-lez v4, :cond_0

    .line 41
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 43
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->this$0:Lcom/navdy/hud/app/common/MainThreadBus;

    iget-object v5, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->val$event:Ljava/lang/Object;

    # invokes: Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    invoke-static {v4, v5}, Lcom/navdy/hud/app/common/MainThreadBus;->access$001(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V

    .line 44
    iget-object v4, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->this$0:Lcom/navdy/hud/app/common/MainThreadBus;

    iget v4, v4, Lcom/navdy/hud/app/common/MainThreadBus;->threshold:I

    if-lez v4, :cond_1

    .line 45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v0, v4, v2

    .line 46
    .local v0, "diff":J
    iget-object v4, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->this$0:Lcom/navdy/hud/app/common/MainThreadBus;

    iget v4, v4, Lcom/navdy/hud/app/common/MainThreadBus;->threshold:I

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-ltz v4, :cond_1

    .line 47
    iget-object v4, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->this$0:Lcom/navdy/hud/app/common/MainThreadBus;

    # getter for: Lcom/navdy/hud/app/common/MainThreadBus;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/common/MainThreadBus;->access$100(Lcom/navdy/hud/app/common/MainThreadBus;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] MainThreadBus-event ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/common/MainThreadBus$1;->val$event:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 50
    .end local v0    # "diff":J
    :cond_1
    return-void
.end method
