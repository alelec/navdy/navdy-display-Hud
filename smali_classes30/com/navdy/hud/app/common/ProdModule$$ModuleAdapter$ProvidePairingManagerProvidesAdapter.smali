.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvidePairingManagerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/manager/PairingManager;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/manager/PairingManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/hud/app/common/ProdModule;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 904
    const-string v0, "com.navdy.hud.app.manager.PairingManager"

    const-string v1, "com.navdy.hud.app.common.ProdModule"

    const-string v2, "providePairingManager"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 905
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    .line 906
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;->setLibrary(Z)V

    .line 907
    return-void
.end method


# virtual methods
.method public get()Lcom/navdy/hud/app/manager/PairingManager;
    .locals 1

    .prologue
    .line 915
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {v0}, Lcom/navdy/hud/app/common/ProdModule;->providePairingManager()Lcom/navdy/hud/app/manager/PairingManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 899
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;->get()Lcom/navdy/hud/app/manager/PairingManager;

    move-result-object v0

    return-object v0
.end method
