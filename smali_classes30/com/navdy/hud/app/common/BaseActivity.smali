.class public abstract Lcom/navdy/hud/app/common/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"


# instance fields
.field private activityScope:Lmortar/MortarActivityScope;

.field protected logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 1
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-static {p1}, Lcom/navdy/hud/app/profile/HudLocale;->onAttach(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/app/Activity;->attachBaseContext(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method protected getBlueprint()Lmortar/Blueprint;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-static {p1}, Lmortar/Mortar;->isScopeSystemService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BaseActivity must override getBlueprint to use Mortar internally"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    .line 99
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/BaseActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/common/BaseActivity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onConfigurationChanged"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 72
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 74
    invoke-static {p0}, Lcom/navdy/hud/app/profile/HudLocale;->onAttach(Landroid/content/Context;)Landroid/content/Context;

    .line 75
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    iget-object v2, p0, Lcom/navdy/hud/app/common/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "::OnCreate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/BaseActivity;->getBlueprint()Lmortar/Blueprint;

    move-result-object v0

    .line 33
    .local v0, "blueprint":Lmortar/Blueprint;
    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    invoke-static {v2}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v1

    .line 35
    .local v1, "parentScope":Lmortar/MortarScope;
    invoke-static {v1, v0}, Lmortar/Mortar;->requireActivityScope(Lmortar/MortarScope;Lmortar/Blueprint;)Lmortar/MortarActivityScope;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    .line 36
    invoke-static {p0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 37
    iget-object v2, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    invoke-interface {v2, p1}, Lmortar/MortarActivityScope;->onCreate(Landroid/os/Bundle;)V

    .line 39
    .end local v1    # "parentScope":Lmortar/MortarScope;
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 60
    iget-object v1, p0, Lcom/navdy/hud/app/common/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "::onDestroy"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 61
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 62
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/BaseActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    .line 64
    .local v0, "parentScope":Lmortar/MortarScope;
    iget-object v1, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    invoke-interface {v0, v1}, Lmortar/MortarScope;->destroyChild(Lmortar/MortarScope;)V

    .line 65
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    .line 67
    .end local v0    # "parentScope":Lmortar/MortarScope;
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 55
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 56
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 49
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 50
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onSaveInstanceState"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 80
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/common/BaseActivity;->activityScope:Lmortar/MortarActivityScope;

    invoke-interface {v0, p1}, Lmortar/MortarActivityScope;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 84
    :cond_0
    return-void
.end method
