.class Lcom/navdy/hud/app/common/ServiceReconnector$2;
.super Ljava/lang/Object;
.source "ServiceReconnector.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ServiceReconnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/common/ServiceReconnector;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/common/ServiceReconnector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$200(Lcom/navdy/hud/app/common/ServiceReconnector;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ServiceConnection established with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/common/ServiceReconnector;->onConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 111
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/common/ServiceReconnector;->onDisconnected(Landroid/content/ComponentName;)V

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-boolean v0, v0, Lcom/navdy/hud/app/common/ServiceReconnector;->shuttingDown:Z

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$200(Lcom/navdy/hud/app/common/ServiceReconnector;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Service disconnected - will try reconnecting"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$100(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/common/ServiceReconnector$2;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->connectRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$600(Lcom/navdy/hud/app/common/ServiceReconnector;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 120
    :cond_0
    return-void
.end method
