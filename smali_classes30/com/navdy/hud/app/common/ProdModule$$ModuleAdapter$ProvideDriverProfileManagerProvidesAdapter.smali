.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideDriverProfileManagerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/profile/DriverProfileManager;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/profile/DriverProfileManager;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/navdy/hud/app/common/ProdModule;

.field private pathManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/storage/PathManager;",
            ">;"
        }
    .end annotation
.end field

.field private timeHelper:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/common/TimeHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 801
    const-string v0, "com.navdy.hud.app.profile.DriverProfileManager"

    const-string v1, "com.navdy.hud.app.common.ProdModule"

    const-string v2, "provideDriverProfileManager"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 802
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    .line 803
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->setLibrary(Z)V

    .line 804
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 813
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    .line 814
    const-string v0, "com.navdy.hud.app.storage.PathManager"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->pathManager:Ldagger/internal/Binding;

    .line 815
    const-string v0, "com.navdy.hud.app.common.TimeHelper"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->timeHelper:Ldagger/internal/Binding;

    .line 816
    return-void
.end method

.method public get()Lcom/navdy/hud/app/profile/DriverProfileManager;
    .locals 4

    .prologue
    .line 835
    iget-object v3, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->pathManager:Ldagger/internal/Binding;

    invoke-virtual {v1}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/storage/PathManager;

    iget-object v2, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->timeHelper:Ldagger/internal/Binding;

    invoke-virtual {v2}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/common/TimeHelper;

    invoke-virtual {v3, v0, v1, v2}, Lcom/navdy/hud/app/common/ProdModule;->provideDriverProfileManager(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/storage/PathManager;Lcom/navdy/hud/app/common/TimeHelper;)Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 793
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->get()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 824
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 825
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->pathManager:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 826
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;->timeHelper:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 827
    return-void
.end method
