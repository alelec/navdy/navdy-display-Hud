.class public Lcom/navdy/hud/app/common/ProdModule;
.super Ljava/lang/Object;
.source "ProdModule.java"


# annotations
.annotation runtime Ldagger/Module;
    injects = {
        Lcom/navdy/hud/app/service/ConnectionServiceProxy;,
        Lcom/navdy/hud/app/maps/here/HereMapsManager;,
        Lcom/navdy/hud/app/maps/here/HereRegionManager;,
        Lcom/navdy/hud/app/obd/ObdManager;,
        Lcom/navdy/hud/app/util/OTAUpdateService;,
        Lcom/navdy/hud/app/maps/MapsEventHandler;,
        Lcom/navdy/hud/app/manager/RemoteDeviceManager;,
        Lcom/navdy/hud/app/maps/MapSchemeController;,
        Lcom/navdy/hud/app/framework/DriverProfileHelper;,
        Lcom/navdy/hud/app/framework/notifications/NotificationManager;,
        Lcom/navdy/hud/app/framework/trips/TripManager;,
        Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;,
        Lcom/navdy/hud/app/framework/DriverProfileHelper;,
        Lcom/navdy/hud/app/debug/DebugReceiver;,
        Lcom/navdy/hud/app/framework/network/NetworkStateManager;,
        Lcom/navdy/hud/app/service/ShutdownMonitor;,
        Lcom/navdy/hud/app/util/ReportIssueService;,
        Lcom/navdy/hud/app/obd/CarMDVinDecoder;,
        Lcom/navdy/hud/app/debug/DriveRecorder;,
        Lcom/navdy/hud/app/HudApplication;,
        Lcom/navdy/hud/app/manager/SpeedManager;,
        Lcom/navdy/hud/app/view/MusicWidgetPresenter;,
        Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;,
        Lcom/navdy/hud/app/manager/UpdateReminderManager;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter;,
        Lcom/navdy/hud/app/analytics/AnalyticsSupport$NotificationReceiver;,
        Lcom/navdy/hud/app/service/GestureVideosSyncService;,
        Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;,
        Lcom/navdy/hud/app/view/EngineTemperaturePresenter;
    }
    library = true
.end annotation


# static fields
.field public static final MUSIC_DATA_CACHE_SIZE:I = 0x1e8480

.field public static final MUSIC_RESPONSE_CACHE:Ljava/lang/String; = "MUSIC_RESPONSE_CACHE"

.field public static final PREF_NAME:Ljava/lang/String; = "App"


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    .line 109
    return-void
.end method


# virtual methods
.method provideAncsServiceConnector(Lcom/squareup/otto/Bus;)Lcom/navdy/hud/app/ancs/AncsServiceConnector;
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 148
    new-instance v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;-><init>(Landroid/content/Context;Lcom/squareup/otto/Bus;)V

    return-object v0
.end method

.method provideArtworkCache()Lcom/navdy/hud/app/util/MusicArtworkCache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 233
    new-instance v0, Lcom/navdy/hud/app/util/MusicArtworkCache;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/MusicArtworkCache;-><init>()V

    return-object v0
.end method

.method provideBus()Lcom/squareup/otto/Bus;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/navdy/hud/app/common/MainThreadBus;

    invoke-direct {v0}, Lcom/navdy/hud/app/common/MainThreadBus;-><init>()V

    return-object v0
.end method

.method provideCallManager(Lcom/squareup/otto/Bus;)Lcom/navdy/hud/app/framework/phonecall/CallManager;
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 162
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/framework/phonecall/CallManager;-><init>(Lcom/squareup/otto/Bus;Landroid/content/Context;)V

    return-object v0
.end method

.method provideConnectionHandler(Lcom/navdy/hud/app/service/ConnectionServiceProxy;Lcom/navdy/hud/app/device/PowerManager;Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/common/TimeHelper;)Lcom/navdy/hud/app/service/ConnectionHandler;
    .locals 7
    .param p1, "connectionServiceProxy"    # Lcom/navdy/hud/app/service/ConnectionServiceProxy;
    .param p2, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;
    .param p3, "driverProfileManager"    # Lcom/navdy/hud/app/profile/DriverProfileManager;
    .param p4, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .param p5, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 182
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionHandler;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/service/ConnectionHandler;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/service/ConnectionServiceProxy;Lcom/navdy/hud/app/device/PowerManager;Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/common/TimeHelper;)V

    return-object v0
.end method

.method provideConnectionServiceProxy(Lcom/squareup/otto/Bus;)Lcom/navdy/hud/app/service/ConnectionServiceProxy;
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 125
    new-instance v0, Lcom/navdy/hud/app/service/ConnectionServiceProxy;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/navdy/hud/app/service/ConnectionServiceProxy;-><init>(Landroid/content/Context;Lcom/squareup/otto/Bus;)V

    return-object v0
.end method

.method provideDialSimulatorMessagesHandler()Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 157
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method provideDriverProfileManager(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/storage/PathManager;Lcom/navdy/hud/app/common/TimeHelper;)Lcom/navdy/hud/app/profile/DriverProfileManager;
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "pathManager"    # Lcom/navdy/hud/app/storage/PathManager;
    .param p3, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-direct {v0, p1, p2, p3}, Lcom/navdy/hud/app/profile/DriverProfileManager;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/storage/PathManager;Lcom/navdy/hud/app/common/TimeHelper;)V

    return-object v0
.end method

.method provideFeatureUtil(Landroid/content/SharedPreferences;)Lcom/navdy/hud/app/util/FeatureUtil;
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 245
    new-instance v0, Lcom/navdy/hud/app/util/FeatureUtil;

    invoke-direct {v0, p1}, Lcom/navdy/hud/app/util/FeatureUtil;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method provideGestureServiceConnector(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/device/PowerManager;)Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 189
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    invoke-direct {v0, p1, p2}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/device/PowerManager;)V

    return-object v0
.end method

.method provideHttpManager()Lcom/navdy/service/library/network/http/IHttpManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 221
    new-instance v0, Lcom/navdy/service/library/network/http/HttpManager;

    invoke-direct {v0}, Lcom/navdy/service/library/network/http/HttpManager;-><init>()V

    return-object v0
.end method

.method provideInputManager(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/device/PowerManager;Lcom/navdy/hud/app/ui/framework/UIStateManager;)Lcom/navdy/hud/app/manager/InputManager;
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;
    .param p3, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 152
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager;

    invoke-direct {v0, p1, p2, p3}, Lcom/navdy/hud/app/manager/InputManager;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/device/PowerManager;Lcom/navdy/hud/app/ui/framework/UIStateManager;)V

    return-object v0
.end method

.method provideMusicCollectionResponseMessageCache(Lcom/navdy/hud/app/storage/PathManager;)Lcom/navdy/hud/app/storage/cache/MessageCache;
    .locals 4
    .param p1, "pathManager"    # Lcom/navdy/hud/app/storage/PathManager;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/storage/PathManager;",
            ")",
            "Lcom/navdy/hud/app/storage/cache/MessageCache",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 257
    new-instance v0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;

    const-string v1, "MUSIC_RESPONSE_CACHE"

    invoke-virtual {p1}, Lcom/navdy/hud/app/storage/PathManager;->getMusicDiskCacheFolder()Ljava/lang/String;

    move-result-object v2

    const v3, 0x1e8480

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 258
    .local v0, "diskLruCacheAdapter":Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;
    new-instance v1, Lcom/navdy/hud/app/storage/cache/MessageCache;

    const-class v2, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    invoke-direct {v1, v0, v2}, Lcom/navdy/hud/app/storage/cache/MessageCache;-><init>(Lcom/navdy/hud/app/storage/cache/Cache;Ljava/lang/Class;)V

    return-object v1
.end method

.method provideMusicManager(Lcom/navdy/hud/app/storage/cache/MessageCache;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/service/pandora/PandoraManager;Lcom/navdy/hud/app/util/MusicArtworkCache;)Lcom/navdy/hud/app/manager/MusicManager;
    .locals 7
    .param p2, "bus"    # Lcom/squareup/otto/Bus;
    .param p3, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .param p4, "pandoraManager"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;
    .param p5, "artworkCache"    # Lcom/navdy/hud/app/util/MusicArtworkCache;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/storage/cache/MessageCache",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;",
            "Lcom/squareup/otto/Bus;",
            "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
            "Lcom/navdy/hud/app/service/pandora/PandoraManager;",
            "Lcom/navdy/hud/app/util/MusicArtworkCache;",
            ")",
            "Lcom/navdy/hud/app/manager/MusicManager;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 174
    .local p1, "musicCollectionResponseMessageCache":Lcom/navdy/hud/app/storage/cache/MessageCache;, "Lcom/navdy/hud/app/storage/cache/MessageCache<Lcom/navdy/service/library/events/audio/MusicCollectionResponse;>;"
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/manager/MusicManager;-><init>(Lcom/navdy/hud/app/storage/cache/MessageCache;Lcom/squareup/otto/Bus;Landroid/content/res/Resources;Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/service/pandora/PandoraManager;Lcom/navdy/hud/app/util/MusicArtworkCache;)V

    return-object v0
.end method

.method provideObdDataReceorder(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/service/ConnectionHandler;)Lcom/navdy/hud/app/debug/DriveRecorder;
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "connectionHandler"    # Lcom/navdy/hud/app/service/ConnectionHandler;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 227
    new-instance v0, Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-direct {v0, p1, p2}, Lcom/navdy/hud/app/debug/DriveRecorder;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/service/ConnectionHandler;)V

    return-object v0
.end method

.method providePairingManager()Lcom/navdy/hud/app/manager/PairingManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 216
    new-instance v0, Lcom/navdy/hud/app/manager/PairingManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/manager/PairingManager;-><init>()V

    return-object v0
.end method

.method providePandoraManager(Lcom/squareup/otto/Bus;)Lcom/navdy/hud/app/service/pandora/PandoraManager;
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 167
    new-instance v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;-><init>(Lcom/squareup/otto/Bus;Landroid/content/res/Resources;)V

    return-object v0
.end method

.method providePathManager()Lcom/navdy/hud/app/storage/PathManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 199
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v0

    return-object v0
.end method

.method providePowerManager(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;)Lcom/navdy/hud/app/device/PowerManager;
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 137
    new-instance v0, Lcom/navdy/hud/app/device/PowerManager;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-direct {v0, p1, v1, p2}, Lcom/navdy/hud/app/device/PowerManager;-><init>(Lcom/squareup/otto/Bus;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method provideSettingsManager(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;)Lcom/navdy/hud/app/config/SettingsManager;
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 143
    new-instance v0, Lcom/navdy/hud/app/config/SettingsManager;

    invoke-direct {v0, p1, p2}, Lcom/navdy/hud/app/config/SettingsManager;-><init>(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method provideSharedPreferences()Landroid/content/SharedPreferences;
    .locals 5
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    const-string v1, "App"

    const/high16 v2, 0x7f060000

    const/4 v3, 0x1

    invoke-static {v0, v1, v4, v2, v3}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;Ljava/lang/String;IIZ)V

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    const-string v1, "App"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method provideTelemetryDataManager(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)Lcom/navdy/hud/app/analytics/TelemetryDataManager;
    .locals 6
    .param p1, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .param p2, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;
    .param p3, "bus"    # Lcom/squareup/otto/Bus;
    .param p4, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p5, "tripManager"    # Lcom/navdy/hud/app/framework/trips/TripManager;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 251
    new-instance v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;-><init>(Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/device/PowerManager;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/framework/trips/TripManager;)V

    return-object v0
.end method

.method provideTimeHelper(Lcom/squareup/otto/Bus;)Lcom/navdy/hud/app/common/TimeHelper;
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 131
    new-instance v0, Lcom/navdy/hud/app/common/TimeHelper;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/navdy/hud/app/common/TimeHelper;-><init>(Landroid/content/Context;Lcom/squareup/otto/Bus;)V

    return-object v0
.end method

.method provideTripManager(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;)Lcom/navdy/hud/app/framework/trips/TripManager;
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 211
    new-instance v0, Lcom/navdy/hud/app/framework/trips/TripManager;

    invoke-direct {v0, p1, p2}, Lcom/navdy/hud/app/framework/trips/TripManager;-><init>(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;)V

    return-object v0
.end method

.method provideUIStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 194
    new-instance v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;-><init>()V

    return-object v0
.end method

.method provideVoiceSearchHandler(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/util/FeatureUtil;)Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "featureUtil"    # Lcom/navdy/hud/app/util/FeatureUtil;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 239
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;-><init>(Landroid/content/Context;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/util/FeatureUtil;)V

    return-object v0
.end method
