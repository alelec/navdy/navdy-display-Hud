.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideMusicManagerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/manager/MusicManager;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/manager/MusicManager;",
        ">;"
    }
.end annotation


# instance fields
.field private artworkCache:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/util/MusicArtworkCache;",
            ">;"
        }
    .end annotation
.end field

.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/navdy/hud/app/common/ProdModule;

.field private musicCollectionResponseMessageCache:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/storage/cache/MessageCache",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private pandoraManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/service/pandora/PandoraManager;",
            ">;"
        }
    .end annotation
.end field

.field private uiStateManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 572
    const-string v0, "com.navdy.hud.app.manager.MusicManager"

    const-string v1, "com.navdy.hud.app.common.ProdModule"

    const-string v2, "provideMusicManager"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 573
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    .line 574
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->setLibrary(Z)V

    .line 575
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 584
    const-string v0, "com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->musicCollectionResponseMessageCache:Ldagger/internal/Binding;

    .line 585
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    .line 586
    const-string v0, "com.navdy.hud.app.ui.framework.UIStateManager"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->uiStateManager:Ldagger/internal/Binding;

    .line 587
    const-string v0, "com.navdy.hud.app.service.pandora.PandoraManager"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->pandoraManager:Ldagger/internal/Binding;

    .line 588
    const-string v0, "com.navdy.hud.app.util.MusicArtworkCache"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->artworkCache:Ldagger/internal/Binding;

    .line 589
    return-void
.end method

.method public get()Lcom/navdy/hud/app/manager/MusicManager;
    .locals 6

    .prologue
    .line 610
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->musicCollectionResponseMessageCache:Ldagger/internal/Binding;

    invoke-virtual {v1}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/storage/cache/MessageCache;

    iget-object v2, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v2}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/otto/Bus;

    iget-object v3, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-virtual {v3}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v4, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->pandoraManager:Ldagger/internal/Binding;

    invoke-virtual {v4}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/service/pandora/PandoraManager;

    iget-object v5, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->artworkCache:Ldagger/internal/Binding;

    invoke-virtual {v5}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/util/MusicArtworkCache;

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/common/ProdModule;->provideMusicManager(Lcom/navdy/hud/app/storage/cache/MessageCache;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/service/pandora/PandoraManager;Lcom/navdy/hud/app/util/MusicArtworkCache;)Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->get()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 597
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->musicCollectionResponseMessageCache:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 598
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 599
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 600
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->pandoraManager:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 601
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;->artworkCache:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 602
    return-void
.end method
