.class public Lcom/navdy/hud/app/common/TimeHelper;
.super Ljava/lang/Object;
.source "TimeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;,
        Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;
    }
.end annotation


# static fields
.field public static final CLOCK_CHANGED:Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;

.field public static final DATE_TIME_AVAILABLE_EVENT:Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private am:Ljava/lang/String;

.field private amCompact:Ljava/lang/String;

.field private bus:Lcom/squareup/otto/Bus;

.field private calendar:Ljava/util/Calendar;

.field private currentTimeFormat:Ljava/text/SimpleDateFormat;

.field private dateFormat:Ljava/text/SimpleDateFormat;

.field private dayFormat:Ljava/text/SimpleDateFormat;

.field private pm:Ljava/lang/String;

.field private pmCompact:Ljava/lang/String;

.field private timeFormat12:Ljava/text/SimpleDateFormat;

.field private timeFormat24:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/common/TimeHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/common/TimeHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;

    invoke-direct {v0}, Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/common/TimeHelper;->CLOCK_CHANGED:Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;

    .line 34
    new-instance v0, Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/common/TimeHelper;->DATE_TIME_AVAILABLE_EVENT:Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p2, p0, Lcom/navdy/hud/app/common/TimeHelper;->bus:Lcom/squareup/otto/Bus;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f09020b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/common/TimeHelper;->pmCompact:Ljava/lang/String;

    .line 56
    const v1, 0x7f09020a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/common/TimeHelper;->pm:Ljava/lang/String;

    .line 57
    const v1, 0x7f090017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/common/TimeHelper;->amCompact:Ljava/lang/String;

    .line 58
    const v1, 0x7f090016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/common/TimeHelper;->am:Ljava/lang/String;

    .line 59
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/TimeHelper;->updateLocale()V

    .line 60
    return-void
.end method


# virtual methods
.method public declared-synchronized formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 4
    .param p1, "date"    # Ljava/util/Date;
    .param p2, "amPmMarker"    # Ljava/lang/StringBuilder;

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "ret":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 86
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 87
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat12:Ljava/text/SimpleDateFormat;

    if-ne v2, v3, :cond_0

    .line 88
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 89
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->calendar:Ljava/util/Calendar;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 90
    .local v0, "ampm":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 91
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->pmCompact:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    .end local v0    # "ampm":I
    :cond_0
    monitor-exit p0

    return-object v1

    .line 84
    .end local v1    # "ret":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized formatTime12Hour(Ljava/util/Date;Ljava/lang/StringBuilder;Z)Ljava/lang/String;
    .locals 4
    .param p1, "date"    # Ljava/util/Date;
    .param p2, "amPmMarker"    # Ljava/lang/StringBuilder;
    .param p3, "compact_AM_PM"    # Z

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat12:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "ret":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 102
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 103
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 104
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->calendar:Ljava/util/Calendar;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 105
    .local v0, "ampm":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 106
    if-eqz p3, :cond_1

    .line 107
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->pmCompact:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    .end local v0    # "ampm":I
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 109
    .restart local v0    # "ampm":I
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->pm:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 100
    .end local v0    # "ampm":I
    .end local v1    # "ret":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 112
    .restart local v0    # "ampm":I
    .restart local v1    # "ret":Ljava/lang/String;
    :cond_2
    if-eqz p3, :cond_3

    .line 113
    :try_start_2
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->amCompact:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 115
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/common/TimeHelper;->am:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized getDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDay()Ljava/lang/String;
    .locals 2

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->dayFormat:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFormat()Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat12:Ljava/text/SimpleDateFormat;

    if-ne v0, v1, :cond_0

    .line 77
    sget-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 79
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    goto :goto_0
.end method

.method public declared-synchronized getTimeZone()Ljava/util/TimeZone;
    .locals 5

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "tz":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/common/TimeHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "timezone is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 166
    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 167
    .local v0, "timeZone":Ljava/util/TimeZone;
    monitor-exit p0

    return-object v0

    .line 164
    .end local v0    # "timeZone":Ljava/util/TimeZone;
    .end local v1    # "tz":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setFormat(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V
    .locals 2
    .param p1, "format"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .prologue
    .line 63
    sget-object v0, Lcom/navdy/hud/app/common/TimeHelper$1;->$SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 72
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->calendar:Ljava/util/Calendar;

    .line 73
    return-void

    .line 65
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat24:Ljava/text/SimpleDateFormat;

    iput-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;

    goto :goto_0

    .line 69
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat12:Ljava/text/SimpleDateFormat;

    iput-object v0, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized updateLocale()V
    .locals 6

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "tz":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/common/TimeHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "timezone is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 125
    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 127
    .local v0, "timeZone":Ljava/util/TimeZone;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "EEE"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->dayFormat:Ljava/text/SimpleDateFormat;

    .line 128
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->dayFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 130
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "d MMM"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 131
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->dateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 133
    const/4 v1, 0x1

    .line 134
    .local v1, "twelveHour":Z
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;

    iget-object v4, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat24:Ljava/text/SimpleDateFormat;

    if-ne v3, v4, :cond_0

    .line 135
    const/4 v1, 0x0

    .line 138
    :cond_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "H:mm"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat24:Ljava/text/SimpleDateFormat;

    .line 139
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat24:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 141
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "h:mm"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat12:Ljava/text/SimpleDateFormat;

    .line 142
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat12:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 144
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->calendar:Ljava/util/Calendar;

    .line 145
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->calendar:Ljava/util/Calendar;

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 147
    if-eqz v1, :cond_1

    .line 148
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat12:Ljava/text/SimpleDateFormat;

    iput-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;

    .line 152
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/common/TimeHelper;->CLOCK_CHANGED:Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    monitor-exit p0

    return-void

    .line 150
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->timeFormat24:Ljava/text/SimpleDateFormat;

    iput-object v3, p0, Lcom/navdy/hud/app/common/TimeHelper;->currentTimeFormat:Ljava/text/SimpleDateFormat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    .end local v0    # "timeZone":Ljava/util/TimeZone;
    .end local v1    # "twelveHour":Z
    .end local v2    # "tz":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
