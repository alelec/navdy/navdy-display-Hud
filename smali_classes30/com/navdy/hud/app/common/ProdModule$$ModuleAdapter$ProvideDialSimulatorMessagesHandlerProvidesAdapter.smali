.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideDialSimulatorMessagesHandlerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/hud/app/common/ProdModule;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 437
    const-string v0, "com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler"

    const-string v1, "com.navdy.hud.app.common.ProdModule"

    const-string v2, "provideDialSimulatorMessagesHandler"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 438
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    .line 439
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;->setLibrary(Z)V

    .line 440
    return-void
.end method


# virtual methods
.method public get()Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {v0}, Lcom/navdy/hud/app/common/ProdModule;->provideDialSimulatorMessagesHandler()Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;->get()Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    move-result-object v0

    return-object v0
.end method
