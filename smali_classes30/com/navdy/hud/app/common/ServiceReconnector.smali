.class public abstract Lcom/navdy/hud/app/common/ServiceReconnector;
.super Ljava/lang/Object;
.source "ServiceReconnector.java"


# static fields
.field private static final RECONNECT_INTERVAL_MS:I = 0x3a98

.field private static final RETRY_INTERVAL_MS:I = 0xea60


# instance fields
.field private action:Ljava/lang/String;

.field private connectRunnable:Ljava/lang/Runnable;

.field private context:Landroid/content/Context;

.field private handler:Landroid/os/Handler;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private serviceConnection:Landroid/content/ServiceConnection;

.field protected serviceIntent:Landroid/content/Intent;

.field shuttingDown:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceIntent"    # Landroid/content/Intent;
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;

    .line 72
    new-instance v0, Lcom/navdy/hud/app/common/ServiceReconnector$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/common/ServiceReconnector$1;-><init>(Lcom/navdy/hud/app/common/ServiceReconnector;)V

    iput-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->connectRunnable:Ljava/lang/Runnable;

    .line 106
    new-instance v0, Lcom/navdy/hud/app/common/ServiceReconnector$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/common/ServiceReconnector$2;-><init>(Lcom/navdy/hud/app/common/ServiceReconnector;)V

    iput-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceConnection:Landroid/content/ServiceConnection;

    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Establishing service connection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->handler:Landroid/os/Handler;

    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->context:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    .line 45
    iput-object p3, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->action:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->connectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/common/ServiceReconnector;Landroid/content/Intent;)Landroid/content/ComponentName;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/ServiceReconnector;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/common/ServiceReconnector;->getServiceComponent(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/common/ServiceReconnector;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/common/ServiceReconnector;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->action:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/common/ServiceReconnector;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->connectRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private getServiceComponent(Landroid/content/Intent;)Landroid/content/ComponentName;
    .locals 5
    .param p1, "serviceIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 63
    iget-object v2, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 64
    .local v1, "pkgAppsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    const/4 v2, 0x0

    .line 68
    :goto_0
    return-object v2

    .line 67
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 68
    .local v0, "info":Landroid/content/pm/ResolveInfo;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract onConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end method

.method protected abstract onDisconnected(Landroid/content/ComponentName;)V
.end method

.method public restart()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 60
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "shutting down service"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->shuttingDown:Z

    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->connectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 55
    return-void
.end method
