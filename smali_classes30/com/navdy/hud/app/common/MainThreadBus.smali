.class public Lcom/navdy/hud/app/common/MainThreadBus;
.super Lcom/squareup/otto/Bus;
.source "MainThreadBus.java"


# instance fields
.field private busLeakDetector:Lcom/navdy/hud/app/debug/BusLeakDetector;

.field private isEngBuild:Z

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final mHandler:Landroid/os/Handler;

.field public threshold:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/squareup/otto/Bus;-><init>()V

    .line 16
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->mHandler:Landroid/os/Handler;

    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/common/MainThreadBus;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->logger:Lcom/navdy/service/library/log/Logger;

    .line 24
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->isEngBuild:Z

    .line 26
    iget-boolean v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->isEngBuild:Z

    if-eqz v0, :cond_0

    .line 27
    invoke-static {}, Lcom/navdy/hud/app/debug/BusLeakDetector;->getInstance()Lcom/navdy/hud/app/debug/BusLeakDetector;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->busLeakDetector:Lcom/navdy/hud/app/debug/BusLeakDetector;

    .line 29
    :cond_0
    return-void

    .line 24
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$001(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/common/MainThreadBus;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/common/MainThreadBus;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/MainThreadBus;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/common/MainThreadBus;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/MainThreadBus;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->isEngBuild:Z

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/common/MainThreadBus;)Lcom/navdy/hud/app/debug/BusLeakDetector;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/common/MainThreadBus;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->busLeakDetector:Lcom/navdy/hud/app/debug/BusLeakDetector;

    return-object v0
.end method

.method static synthetic access$401(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/common/MainThreadBus;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$501(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/common/MainThreadBus;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public post(Ljava/lang/Object;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/Object;

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 52
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/common/MainThreadBus$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/common/MainThreadBus$1;-><init>(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public register(Ljava/lang/Object;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 68
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/common/MainThreadBus$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/common/MainThreadBus$2;-><init>(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public unregister(Ljava/lang/Object;)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 72
    if-nez p1, :cond_0

    .line 84
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/common/MainThreadBus;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/common/MainThreadBus$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/common/MainThreadBus$3;-><init>(Lcom/navdy/hud/app/common/MainThreadBus;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
