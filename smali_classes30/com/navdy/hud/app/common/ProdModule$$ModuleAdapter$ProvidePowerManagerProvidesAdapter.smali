.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvidePowerManagerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/device/PowerManager;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/device/PowerManager;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/navdy/hud/app/common/ProdModule;

.field private preferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 230
    const-string v0, "com.navdy.hud.app.device.PowerManager"

    const-string v1, "com.navdy.hud.app.common.ProdModule"

    const-string v2, "providePowerManager"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 231
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    .line 232
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->setLibrary(Z)V

    .line 233
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 242
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    .line 243
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->preferences:Ldagger/internal/Binding;

    .line 244
    return-void
.end method

.method public get()Lcom/navdy/hud/app/device/PowerManager;
    .locals 3

    .prologue
    .line 262
    iget-object v2, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->preferences:Ldagger/internal/Binding;

    invoke-virtual {v1}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    invoke-virtual {v2, v0, v1}, Lcom/navdy/hud/app/common/ProdModule;->providePowerManager(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;)Lcom/navdy/hud/app/device/PowerManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->get()Lcom/navdy/hud/app/device/PowerManager;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 252
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 253
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;->preferences:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 254
    return-void
.end method
