.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideUIStateManagerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/hud/app/common/ProdModule;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 741
    const-string v0, "com.navdy.hud.app.ui.framework.UIStateManager"

    const-string v1, "com.navdy.hud.app.common.ProdModule"

    const-string v2, "provideUIStateManager"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 742
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    .line 743
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;->setLibrary(Z)V

    .line 744
    return-void
.end method


# virtual methods
.method public get()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {v0}, Lcom/navdy/hud/app/common/ProdModule;->provideUIStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 736
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;->get()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    return-object v0
.end method
