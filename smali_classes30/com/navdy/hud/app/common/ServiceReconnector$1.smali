.class Lcom/navdy/hud/app/common/ServiceReconnector$1;
.super Ljava/lang/Object;
.source "ServiceReconnector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ServiceReconnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/common/ServiceReconnector;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/common/ServiceReconnector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/common/ServiceReconnector;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 76
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-object v4, v4, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 77
    .local v1, "component":Landroid/content/ComponentName;
    if-nez v1, :cond_0

    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-object v5, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-object v5, v5, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    # invokes: Lcom/navdy/hud/app/common/ServiceReconnector;->getServiceComponent(Landroid/content/Intent;)Landroid/content/ComponentName;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$000(Lcom/navdy/hud/app/common/ServiceReconnector;Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    .line 78
    :cond_0
    if-nez v1, :cond_2

    .line 80
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$100(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/os/Handler;

    move-result-object v4

    const-wide/32 v6, 0xea60

    invoke-virtual {v4, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 100
    .end local v1    # "component":Landroid/content/ComponentName;
    :cond_1
    :goto_0
    return-void

    .line 82
    .restart local v1    # "component":Landroid/content/ComponentName;
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-object v4, v4, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 83
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$200(Lcom/navdy/hud/app/common/ServiceReconnector;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Trying to start service"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 84
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->context:Landroid/content/Context;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$300(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-object v5, v5, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v4, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v3

    .line 86
    .local v3, "startedComponent":Landroid/content/ComponentName;
    if-eqz v3, :cond_3

    .line 87
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-object v4, v4, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    iget-object v5, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->action:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$400(Lcom/navdy/hud/app/common/ServiceReconnector;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->context:Landroid/content/Context;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$300(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    iget-object v5, v5, Lcom/navdy/hud/app/common/ServiceReconnector;->serviceIntent:Landroid/content/Intent;

    iget-object v6, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->serviceConnection:Landroid/content/ServiceConnection;
    invoke-static {v6}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$500(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/content/ServiceConnection;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 89
    .local v0, "bound":Z
    if-nez v0, :cond_1

    .line 90
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$200(Lcom/navdy/hud/app/common/ServiceReconnector;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Unable to bind to service - aborting"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 97
    .end local v0    # "bound":Z
    .end local v1    # "component":Landroid/content/ComponentName;
    .end local v3    # "startedComponent":Landroid/content/ComponentName;
    :catch_0
    move-exception v2

    .line 98
    .local v2, "ex":Ljava/lang/SecurityException;
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$200(Lcom/navdy/hud/app/common/ServiceReconnector;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Security exception connecting to service - aborting"

    invoke-virtual {v4, v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 93
    .end local v2    # "ex":Ljava/lang/SecurityException;
    .restart local v1    # "component":Landroid/content/ComponentName;
    .restart local v3    # "startedComponent":Landroid/content/ComponentName;
    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$200(Lcom/navdy/hud/app/common/ServiceReconnector;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Service doesn\'t exist (uninstalled?) - retrying"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 94
    iget-object v4, p0, Lcom/navdy/hud/app/common/ServiceReconnector$1;->this$0:Lcom/navdy/hud/app/common/ServiceReconnector;

    # getter for: Lcom/navdy/hud/app/common/ServiceReconnector;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/common/ServiceReconnector;->access$100(Lcom/navdy/hud/app/common/ServiceReconnector;)Landroid/os/Handler;

    move-result-object v4

    const-wide/32 v6, 0xea60

    invoke-virtual {v4, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
