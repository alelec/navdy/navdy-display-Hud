.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideMusicCollectionResponseMessageCacheProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/hud/app/storage/cache/MessageCache",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
        ">;>;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/storage/cache/MessageCache",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/hud/app/common/ProdModule;

.field private pathManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/storage/PathManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 1207
    const-string v0, "com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>"

    const-string v1, "com.navdy.hud.app.common.ProdModule"

    const-string v2, "provideMusicCollectionResponseMessageCache"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1208
    iput-object p1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    .line 1209
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;->setLibrary(Z)V

    .line 1210
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 1219
    const-string v0, "com.navdy.hud.app.storage.PathManager"

    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;->pathManager:Ldagger/internal/Binding;

    .line 1220
    return-void
.end method

.method public get()Lcom/navdy/hud/app/storage/cache/MessageCache;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/navdy/hud/app/storage/cache/MessageCache",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1237
    iget-object v1, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;->module:Lcom/navdy/hud/app/common/ProdModule;

    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;->pathManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/storage/PathManager;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/common/ProdModule;->provideMusicCollectionResponseMessageCache(Lcom/navdy/hud/app/storage/PathManager;)Lcom/navdy/hud/app/storage/cache/MessageCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1201
    invoke-virtual {p0}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;->get()Lcom/navdy/hud/app/storage/cache/MessageCache;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 1228
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;->pathManager:Ldagger/internal/Binding;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1229
    return-void
.end method
