.class public final Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;
.super Ldagger/internal/ModuleAdapter;
.source "ProdModule$$ModuleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTelemetryDataManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideFeatureUtilProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideVoiceSearchHandlerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideArtworkCacheProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideObdDataReceorderProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTripManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePathManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideGestureServiceConnectorProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideConnectionHandlerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePandoraManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideCallManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideInputManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideAncsServiceConnectorProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideSettingsManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTimeHelperProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideConnectionServiceProxyProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;,
        Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideBusProvidesAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ModuleAdapter",
        "<",
        "Lcom/navdy/hud/app/common/ProdModule;",
        ">;"
    }
.end annotation


# static fields
.field private static final INCLUDES:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final INJECTS:[Ljava/lang/String;

.field private static final STATIC_INJECTIONS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 17
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "members/com.navdy.hud.app.service.ConnectionServiceProxy"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "members/com.navdy.hud.app.maps.here.HereMapsManager"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "members/com.navdy.hud.app.maps.here.HereRegionManager"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "members/com.navdy.hud.app.obd.ObdManager"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "members/com.navdy.hud.app.util.OTAUpdateService"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "members/com.navdy.hud.app.maps.MapsEventHandler"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "members/com.navdy.hud.app.manager.RemoteDeviceManager"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "members/com.navdy.hud.app.maps.MapSchemeController"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "members/com.navdy.hud.app.framework.DriverProfileHelper"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "members/com.navdy.hud.app.framework.notifications.NotificationManager"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "members/com.navdy.hud.app.framework.trips.TripManager"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "members/com.navdy.hud.app.framework.DriverProfileHelper"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "members/com.navdy.hud.app.debug.DebugReceiver"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "members/com.navdy.hud.app.framework.network.NetworkStateManager"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "members/com.navdy.hud.app.service.ShutdownMonitor"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "members/com.navdy.hud.app.util.ReportIssueService"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "members/com.navdy.hud.app.obd.CarMDVinDecoder"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "members/com.navdy.hud.app.debug.DriveRecorder"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "members/com.navdy.hud.app.HudApplication"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "members/com.navdy.hud.app.manager.SpeedManager"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "members/com.navdy.hud.app.view.MusicWidgetPresenter"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "members/com.navdy.hud.app.manager.UpdateReminderManager"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "members/com.navdy.hud.app.service.GestureVideosSyncService"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "members/com.navdy.hud.app.service.ObdCANBusDataUploadService"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "members/com.navdy.hud.app.view.EngineTemperaturePresenter"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    .line 18
    new-array v0, v3, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    .line 19
    new-array v0, v3, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 22
    const-class v1, Lcom/navdy/hud/app/common/ProdModule;

    sget-object v2, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    sget-object v3, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    move-object v0, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Ldagger/internal/ModuleAdapter;-><init>(Ljava/lang/Class;[Ljava/lang/String;[Ljava/lang/Class;Z[Ljava/lang/Class;ZZ)V

    .line 23
    return-void
.end method


# virtual methods
.method public getBindings(Ldagger/internal/BindingsGroup;Lcom/navdy/hud/app/common/ProdModule;)V
    .locals 2
    .param p1, "bindings"    # Ldagger/internal/BindingsGroup;
    .param p2, "module"    # Lcom/navdy/hud/app/common/ProdModule;

    .prologue
    .line 31
    const-string v0, "com.squareup.otto.Bus"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideBusProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideBusProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 32
    const-string v0, "android.content.SharedPreferences"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 33
    const-string v0, "com.navdy.hud.app.service.ConnectionServiceProxy"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideConnectionServiceProxyProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideConnectionServiceProxyProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 34
    const-string v0, "com.navdy.hud.app.common.TimeHelper"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTimeHelperProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTimeHelperProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 35
    const-string v0, "com.navdy.hud.app.device.PowerManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 36
    const-string v0, "com.navdy.hud.app.config.SettingsManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideSettingsManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideSettingsManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 37
    const-string v0, "com.navdy.hud.app.ancs.AncsServiceConnector"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideAncsServiceConnectorProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideAncsServiceConnectorProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 38
    const-string v0, "com.navdy.hud.app.manager.InputManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideInputManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideInputManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 39
    const-string v0, "com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 40
    const-string v0, "com.navdy.hud.app.framework.phonecall.CallManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideCallManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideCallManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 41
    const-string v0, "com.navdy.hud.app.service.pandora.PandoraManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePandoraManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePandoraManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 42
    const-string v0, "com.navdy.hud.app.manager.MusicManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 43
    const-string v0, "com.navdy.hud.app.service.ConnectionHandler"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideConnectionHandlerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideConnectionHandlerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 44
    const-string v0, "com.navdy.hud.app.gesture.GestureServiceConnector"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideGestureServiceConnectorProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideGestureServiceConnectorProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 45
    const-string v0, "com.navdy.hud.app.ui.framework.UIStateManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 46
    const-string v0, "com.navdy.hud.app.storage.PathManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePathManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePathManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 47
    const-string v0, "com.navdy.hud.app.profile.DriverProfileManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 48
    const-string v0, "com.navdy.hud.app.framework.trips.TripManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTripManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTripManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 49
    const-string v0, "com.navdy.hud.app.manager.PairingManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 50
    const-string v0, "com.navdy.service.library.network.http.IHttpManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 51
    const-string v0, "com.navdy.hud.app.debug.DriveRecorder"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideObdDataReceorderProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideObdDataReceorderProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 52
    const-string v0, "com.navdy.hud.app.util.MusicArtworkCache"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideArtworkCacheProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideArtworkCacheProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 53
    const-string v0, "com.navdy.hud.app.framework.voice.VoiceSearchHandler"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideVoiceSearchHandlerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideVoiceSearchHandlerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 54
    const-string v0, "com.navdy.hud.app.util.FeatureUtil"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideFeatureUtilProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideFeatureUtilProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 55
    const-string v0, "com.navdy.hud.app.analytics.TelemetryDataManager"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTelemetryDataManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideTelemetryDataManagerProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 56
    const-string v0, "com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>"

    new-instance v1, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter;-><init>(Lcom/navdy/hud/app/common/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 57
    return-void
.end method

.method public bridge synthetic getBindings(Ldagger/internal/BindingsGroup;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p2, Lcom/navdy/hud/app/common/ProdModule;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/common/ProdModule$$ModuleAdapter;->getBindings(Ldagger/internal/BindingsGroup;Lcom/navdy/hud/app/common/ProdModule;)V

    return-void
.end method
