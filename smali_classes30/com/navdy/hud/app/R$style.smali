.class public final Lcom/navdy/hud/app/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AppTheme:I = 0x7f0c0007

.field public static final GaugeText:I = 0x7f0c0008

.field public static final Glances_1:I = 0x7f0c0009

.field public static final Glances_1_bold:I = 0x7f0c000a

.field public static final Glances_2:I = 0x7f0c000b

.field public static final Glances_Disabled_1:I = 0x7f0c0000

.field public static final Glances_Disabled_2:I = 0x7f0c0001

.field public static final HockeyApp_ButtonStyle:I = 0x7f0c0002

.field public static final HockeyApp_EditTextStyle:I = 0x7f0c0003

.field public static final HockeyApp_SingleLineInputStyle:I = 0x7f0c0004

.field public static final HudTheme:I = 0x7f0c000c

.field public static final NotificationText:I = 0x7f0c0005

.field public static final NotificationTitle:I = 0x7f0c0006

.field public static final Roboto:I = 0x7f0c000d

.field public static final RobotoMedium:I = 0x7f0c000f

.field public static final Roboto_Bold:I = 0x7f0c000e

.field public static final ToastMainTitle:I = 0x7f0c0010

.field public static final Toast_1:I = 0x7f0c0011

.field public static final Toast_2:I = 0x7f0c0012

.field public static final Toast_3:I = 0x7f0c0013

.field public static final destination_subtitle_two_line:I = 0x7f0c0014

.field public static final destination_title_large:I = 0x7f0c0015

.field public static final destination_title_short:I = 0x7f0c0016

.field public static final glance_message_body:I = 0x7f0c0017

.field public static final glance_message_title:I = 0x7f0c0018

.field public static final glance_title_1:I = 0x7f0c0019

.field public static final glance_title_2:I = 0x7f0c001a

.field public static final glance_title_3:I = 0x7f0c001b

.field public static final glance_title_4:I = 0x7f0c001c

.field public static final incident_1:I = 0x7f0c001d

.field public static final incident_2:I = 0x7f0c001e

.field public static final installing_update_title_1:I = 0x7f0c001f

.field public static final installing_update_title_2:I = 0x7f0c0020

.field public static final mainTitle:I = 0x7f0c0021

.field public static final route_duration_20_b:I = 0x7f0c0022

.field public static final route_duration_24:I = 0x7f0c0023

.field public static final route_duration_26:I = 0x7f0c0024

.field public static final small_glance_sign_text_1:I = 0x7f0c0025

.field public static final small_glance_sign_text_2:I = 0x7f0c0026

.field public static final small_glance_sign_text_3:I = 0x7f0c0027

.field public static final system_info_section:I = 0x7f0c0028

.field public static final system_info_section_heading:I = 0x7f0c0029

.field public static final system_info_section_title:I = 0x7f0c002a

.field public static final title1:I = 0x7f0c002b

.field public static final title2:I = 0x7f0c002c

.field public static final title3:I = 0x7f0c002d

.field public static final title3_single:I = 0x7f0c002e

.field public static final tool_tip:I = 0x7f0c002f

.field public static final vlist_picker_subtitle:I = 0x7f0c0030

.field public static final vlist_picker_title:I = 0x7f0c0031

.field public static final vlist_subtitle:I = 0x7f0c0032

.field public static final vlist_subtitle_2_line:I = 0x7f0c0033

.field public static final vlist_title:I = 0x7f0c0034


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
