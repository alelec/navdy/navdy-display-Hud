.class public Lcom/navdy/hud/app/settings/BrightnessControl;
.super Ljava/lang/Object;
.source "BrightnessControl.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field public static final AUTOBRIGHTNESSD_PROPERTY:Ljava/lang/String; = "hw.navdy.autobrightnessd"

.field public static final AUTO_BRIGHTNESS_PROPERTY:Ljava/lang/String; = "persist.sys.autobrightness"

.field public static final DEFAULT_AUTO_BRIGHTNESS:Ljava/lang/String; = "false"

.field public static final DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT:Ljava/lang/String; = "0"

.field public static final DEFAULT_BRIGHTNESS:Ljava/lang/String; = "128"

.field public static final DEFAULT_LED_BRIGHTNESS:Ljava/lang/String; = "255"

.field public static final DISABLED:Ljava/lang/String; = "disabled"

.field public static final ENABLED:Ljava/lang/String; = "enabled"

.field public static final MAX_BRIGHTNESS_ADJUSTMENT:I = 0x40

.field public static final MIN_BRIGHTNESS_ADJUSTMENT:I = -0x40

.field private static final MIN_INITIAL_BRIGHTNESS:I = 0x80

.field public static final OFF:Ljava/lang/String; = "off"

.field public static final ON:Ljava/lang/String; = "on"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private autoBrightnessAdjustmentKey:Ljava/lang/String;

.field private autoBrightnessKey:Ljava/lang/String;

.field private brightnessKey:Ljava/lang/String;

.field private bus:Lcom/squareup/otto/Bus;

.field private context:Landroid/content/Context;

.field private lastBrightness:F

.field private ledBrightnessKey:Ljava/lang/String;

.field private mainHandler:Landroid/os/Handler;

.field private preferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/navdy/hud/app/settings/BrightnessControl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/settings/BrightnessControl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;
    .param p4, "brightnessKey"    # Ljava/lang/String;
    .param p5, "autoBrightnessKey"    # Ljava/lang/String;
    .param p6, "autoBrightnessAdjustmentKey"    # Ljava/lang/String;
    .param p7, "ledBrightnessKey"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x80

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->lastBrightness:F

    .line 65
    iput-object p1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->bus:Lcom/squareup/otto/Bus;

    .line 67
    invoke-virtual {p2, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 68
    iput-object p3, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->preferences:Landroid/content/SharedPreferences;

    .line 69
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->mainHandler:Landroid/os/Handler;

    .line 70
    invoke-interface {p3, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 71
    iput-object p4, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->brightnessKey:Ljava/lang/String;

    .line 72
    iput-object p5, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->autoBrightnessKey:Ljava/lang/String;

    .line 73
    iput-object p6, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->autoBrightnessAdjustmentKey:Ljava/lang/String;

    .line 74
    iput-object p7, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->ledBrightnessKey:Ljava/lang/String;

    .line 77
    const-string v2, "hw.navdy.autobrightnessd"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/BrightnessControl;->getAutoBrightnessProperty()Z

    move-result v0

    .line 79
    .local v0, "autoBrightnessEnabled":Z
    invoke-interface {p3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    if-eqz v0, :cond_2

    const-string v2, "true"

    :goto_0
    invoke-interface {v3, p5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 82
    if-nez v0, :cond_1

    .line 83
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/BrightnessControl;->getBrightnessPreference()I

    move-result v1

    .line 84
    .local v1, "initialBrightness":I
    if-ge v1, v4, :cond_0

    .line 85
    invoke-interface {p3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 86
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 87
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 89
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->brightnessKey:Ljava/lang/String;

    invoke-virtual {p0, p3, v2}, Lcom/navdy/hud/app/settings/BrightnessControl;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 92
    .end local v1    # "initialBrightness":I
    :cond_1
    invoke-virtual {p0, p3, p5}, Lcom/navdy/hud/app/settings/BrightnessControl;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0, p3, p7}, Lcom/navdy/hud/app/settings/BrightnessControl;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 94
    return-void

    .line 79
    :cond_2
    const-string v2, "false"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/settings/BrightnessControl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/BrightnessControl;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/settings/BrightnessControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/BrightnessControl;

    .prologue
    .line 24
    iget v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->lastBrightness:F

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/settings/BrightnessControl;F)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/BrightnessControl;
    .param p1, "x1"    # F

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/settings/BrightnessControl;->updateWindowBrightness(F)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/settings/BrightnessControl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/BrightnessControl;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->mainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getAutoBrightnessAdjustmentPreference()I
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->autoBrightnessAdjustmentKey:Ljava/lang/String;

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private final getAutoBrightnessProperty()Z
    .locals 3

    .prologue
    .line 174
    const-string v1, "persist.sys.autobrightness"

    const-string v2, "on"

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "autoBrightness":Ljava/lang/String;
    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getBrightnessPreference()I
    .locals 3

    .prologue
    .line 213
    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->brightnessKey:Ljava/lang/String;

    const-string v2, "128"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private setAutoBrightnessAdjustment(I)V
    .locals 5
    .param p1, "val"    # I

    .prologue
    const/4 v4, 0x1

    .line 180
    const/16 v2, -0x40

    if-lt p1, v2, :cond_0

    const/16 v2, 0x40

    if-le p1, v2, :cond_1

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_brightness_mode"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-ne v2, v4, :cond_0

    .line 194
    const-string v0, "screen_auto_brightness_adj"

    .line 195
    .local v0, "SCREEN_AUTO_BRIGHTNESS_ADJ":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/settings/BrightnessControl$3;

    invoke-direct {v3, p0, p1}, Lcom/navdy/hud/app/settings/BrightnessControl$3;-><init>(Lcom/navdy/hud/app/settings/BrightnessControl;I)V

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 189
    .end local v0    # "SCREEN_AUTO_BRIGHTNESS_ADJ":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 190
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v2, Lcom/navdy/hud/app/settings/BrightnessControl;->TAG:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private updateWindowBrightness(F)V
    .locals 2
    .param p1, "brightness"    # F

    .prologue
    .line 207
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 208
    .local v0, "layout":Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 209
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 210
    return-void
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 161
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->brightnessKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/BrightnessControl;->getBrightnessPreference()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/settings/BrightnessControl;->setBrightnessValue(I)V

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->autoBrightnessAdjustmentKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/BrightnessControl;->getAutoBrightnessAdjustmentPreference()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/settings/BrightnessControl;->setAutoBrightnessAdjustment(I)V

    goto :goto_0

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->autoBrightnessKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    const-string v1, "false"

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 167
    .local v0, "autoBrightness":Z
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/settings/BrightnessControl;->toggleAutoBrightness(Z)V

    goto :goto_0

    .line 168
    .end local v0    # "autoBrightness":Z
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->ledBrightnessKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    const-string v1, "255"

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/settings/BrightnessControl;->setLEDValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBrightnessValue(I)V
    .locals 4
    .param p1, "val"    # I

    .prologue
    const/4 v3, 0x1

    .line 102
    if-ltz p1, :cond_0

    const/16 v1, 0xff

    if-le p1, v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    int-to-float v1, p1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->lastBrightness:F

    .line 108
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eq v1, v3, :cond_0

    .line 117
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/settings/BrightnessControl$1;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/settings/BrightnessControl$1;-><init>(Lcom/navdy/hud/app/settings/BrightnessControl;I)V

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 125
    iget v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl;->lastBrightness:F

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/settings/BrightnessControl;->updateWindowBrightness(F)V

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v1, Lcom/navdy/hud/app/settings/BrightnessControl;->TAG:Ljava/lang/String;

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setLEDValue(Ljava/lang/String;)V
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 129
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 130
    .local v0, "val":I
    if-ltz v0, :cond_0

    const/16 v1, 0xff

    if-le v0, v1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    const-string v1, "hw.navdy.led_max_brightness"

    const-string v2, "%f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    int-to-float v5, v0

    const/high16 v6, 0x437f0000    # 255.0f

    div-float/2addr v5, v6

    .line 136
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 135
    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public toggleAutoBrightness(Z)V
    .locals 3
    .param p1, "autoBrightness"    # Z

    .prologue
    .line 140
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/settings/BrightnessControl$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/settings/BrightnessControl$2;-><init>(Lcom/navdy/hud/app/settings/BrightnessControl;Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 157
    return-void
.end method
