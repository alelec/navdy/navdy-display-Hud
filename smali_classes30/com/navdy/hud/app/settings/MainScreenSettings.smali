.class public Lcom/navdy/hud/app/settings/MainScreenSettings;
.super Ljava/lang/Object;
.source "MainScreenSettings.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/hud/app/settings/MainScreenSettings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected landscapeConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

.field protected portraitConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/navdy/hud/app/settings/MainScreenSettings$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/settings/MainScreenSettings$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/settings/MainScreenSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    invoke-direct {v0}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;-><init>()V

    new-instance v1, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    invoke-direct {v1}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/settings/MainScreenSettings;-><init>(Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 97
    const-class v0, Lcom/navdy/hud/app/settings/MainScreenSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    const-class v1, Lcom/navdy/hud/app/settings/MainScreenSettings;

    .line 98
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .line 97
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/settings/MainScreenSettings;-><init>(Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;)V
    .locals 0
    .param p1, "portraitConfiguration"    # Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    .param p2, "landscapeConfiguration"    # Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->portraitConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .line 93
    iput-object p2, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->landscapeConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .line 94
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public getLandscapeConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->landscapeConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    return-object v0
.end method

.method public getPortraitConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->portraitConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    return-object v0
.end method

.method public setLandscapeConfiguration(Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;)V
    .locals 0
    .param p1, "landscapeConfiguration"    # Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->landscapeConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .line 115
    return-void
.end method

.method public setPortraitConfiguration(Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;)V
    .locals 0
    .param p1, "portraitConfiguration"    # Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->portraitConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    .line 107
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->portraitConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/settings/MainScreenSettings;->landscapeConfiguration:Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 126
    return-void
.end method
