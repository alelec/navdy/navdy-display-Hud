.class public Lcom/navdy/hud/app/settings/HUDSettings;
.super Ljava/lang/Object;
.source "HUDSettings.java"


# static fields
.field private static final ADAPTIVE_AUTOBRIGHTNESS_PROP:Ljava/lang/String; = "persist.sys.autobright_adaptive"

.field public static final AUTO_BRIGHTNESS:Ljava/lang/String; = "screen.auto_brightness"

.field public static final AUTO_BRIGHTNESS_ADJUSTMENT:Ljava/lang/String; = "screen.auto_brightness_adj"

.field public static final BRIGHTNESS:Ljava/lang/String; = "screen.brightness"

.field public static final BRIGHTNESS_SCALE:F = 255.0f

.field public static final GESTURE_ENGINE:Ljava/lang/String; = "gesture.engine"

.field public static final GESTURE_PREVIEW:Ljava/lang/String; = "gesture.preview"

.field public static final LED_BRIGHTNESS:Ljava/lang/String; = "screen.led_brightness"

.field public static final MAP_ANIMATION_MODE:Ljava/lang/String; = "map.animation.mode"

.field public static final MAP_SCHEME:Ljava/lang/String; = "map.scheme"

.field public static final MAP_TILT:Ljava/lang/String; = "map.tilt"

.field public static final MAP_ZOOM:Ljava/lang/String; = "map.zoom"

.field public static final USE_ADAPTIVE_AUTOBRIGHTNESS:Z

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private preferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-string v0, "persist.sys.autobright_adaptive"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/settings/HUDSettings;->USE_ADAPTIVE_AUTOBRIGHTNESS:Z

    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/settings/HUDSettings;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/settings/HUDSettings;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/navdy/hud/app/settings/HUDSettings;->preferences:Landroid/content/SharedPreferences;

    .line 39
    return-void
.end method

.method private get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/settings/HUDSettings;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private set(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "editor"    # Landroid/content/SharedPreferences$Editor;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/settings/HUDSettings;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 87
    .local v1, "oldValue":Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 88
    invoke-interface {p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    instance-of v2, v1, Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 90
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 91
    .local v0, "boolVal":Z
    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method


# virtual methods
.method public availableSettings()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/navdy/hud/app/settings/HUDSettings;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 44
    return-object v0
.end method

.method public readSettings(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/settings/Setting;>;"
    iget-object v5, p0, Lcom/navdy/hud/app/settings/HUDSettings;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 71
    .local v0, "allPreferences":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 72
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 73
    .local v2, "key":Ljava/lang/String;
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 74
    .local v3, "object":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 75
    new-instance v5, Lcom/navdy/service/library/events/settings/Setting;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v2, v6}, Lcom/navdy/service/library/events/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 78
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "object":Ljava/lang/Object;
    :cond_1
    return-object v4
.end method

.method public updateSettings(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "newSettings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/settings/Setting;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/settings/HUDSettings;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 49
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 50
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/settings/Setting;

    .line 51
    .local v2, "newSetting":Lcom/navdy/service/library/events/settings/Setting;
    iget-object v3, v2, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    const-string v4, "-1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52
    iget-object v4, v2, Lcom/navdy/service/library/events/settings/Setting;->key:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 49
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52
    :sswitch_0
    const-string v5, "map.tilt"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_1

    :sswitch_1
    const-string v5, "map.zoom"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    .line 54
    :pswitch_0
    iget-object v3, v2, Lcom/navdy/service/library/events/settings/Setting;->key:Ljava/lang/String;

    const/high16 v4, 0x42700000    # 60.0f

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v3, v4}, Lcom/navdy/hud/app/settings/HUDSettings;->set(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 57
    :pswitch_1
    iget-object v3, v2, Lcom/navdy/service/library/events/settings/Setting;->key:Ljava/lang/String;

    const/high16 v4, 0x41840000    # 16.5f

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v3, v4}, Lcom/navdy/hud/app/settings/HUDSettings;->set(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 61
    :cond_1
    iget-object v3, v2, Lcom/navdy/service/library/events/settings/Setting;->key:Ljava/lang/String;

    iget-object v4, v2, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v3, v4}, Lcom/navdy/hud/app/settings/HUDSettings;->set(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 64
    .end local v2    # "newSetting":Lcom/navdy/service/library/events/settings/Setting;
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 65
    return-void

    .line 52
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f9e00f -> :sswitch_0
        0x7fcb125 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
