.class Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;
.super Ljava/lang/Object;
.source "AdaptiveBrightnessControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->toggleAutoBrightness(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

.field final synthetic val$autoBrightness:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->this$0:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    iput-boolean p2, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->val$autoBrightness:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->this$0:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    # getter for: Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->access$000(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    iget-boolean v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->val$autoBrightness:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->this$0:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    # getter for: Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->mainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->access$300(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2$1;-><init>(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 140
    return-void

    .line 128
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
