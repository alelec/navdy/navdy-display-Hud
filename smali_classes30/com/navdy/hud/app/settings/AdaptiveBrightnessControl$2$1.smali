.class Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2$1;
.super Ljava/lang/Object;
.source "AdaptiveBrightnessControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;

    iget-object v1, v0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->this$0:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;

    iget-boolean v0, v0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->val$autoBrightness:Z

    if-eqz v0, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    # invokes: Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->updateWindowBrightness(F)V
    invoke-static {v1, v0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->access$200(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;F)V

    .line 138
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;

    iget-object v0, v0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;->this$0:Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    .line 137
    # getter for: Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->lastBrightness:F
    invoke-static {v0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->access$100(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;)F

    move-result v0

    goto :goto_0
.end method
