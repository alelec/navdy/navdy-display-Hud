.class Lcom/navdy/hud/app/settings/BrightnessControl$3;
.super Ljava/lang/Object;
.source "BrightnessControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/settings/BrightnessControl;->setAutoBrightnessAdjustment(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/settings/BrightnessControl;

.field final synthetic val$val:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/settings/BrightnessControl;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/settings/BrightnessControl;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/navdy/hud/app/settings/BrightnessControl$3;->this$0:Lcom/navdy/hud/app/settings/BrightnessControl;

    iput p2, p0, Lcom/navdy/hud/app/settings/BrightnessControl$3;->val$val:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 198
    iget v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl$3;->val$val:I

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float v0, v1, v2

    .line 199
    .local v0, "normalizedBrightnessAdjustment":F
    const-string v1, "TEST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Normalized : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v1, p0, Lcom/navdy/hud/app/settings/BrightnessControl$3;->this$0:Lcom/navdy/hud/app/settings/BrightnessControl;

    # getter for: Lcom/navdy/hud/app/settings/BrightnessControl;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/navdy/hud/app/settings/BrightnessControl;->access$000(Lcom/navdy/hud/app/settings/BrightnessControl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_auto_brightness_adj"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    .line 202
    return-void
.end method
