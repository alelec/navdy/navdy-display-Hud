.class public Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;
.super Ljava/lang/Object;
.source "AdaptiveBrightnessControl.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field public static final AUTOBRIGHTNESSD_PROPERTY:Ljava/lang/String; = "hw.navdy.autobrightnessd"

.field public static final AUTO_BRIGHTNESS_PROPERTY:Ljava/lang/String; = "persist.sys.autobrightness"

.field public static final DEFAULT_AUTO_BRIGHTNESS:Ljava/lang/String; = "false"

.field public static final DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT:Ljava/lang/String; = "0"

.field public static final DEFAULT_BRIGHTNESS:Ljava/lang/String; = "128"

.field public static final DEFAULT_LED_BRIGHTNESS:Ljava/lang/String; = "255"

.field public static final DISABLED:Ljava/lang/String; = "disabled"

.field public static final ENABLED:Ljava/lang/String; = "enabled"

.field private static final MIN_INITIAL_BRIGHTNESS:I = 0x80

.field public static final OFF:Ljava/lang/String; = "off"

.field public static final ON:Ljava/lang/String; = "on"

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private autoBrightnessAdjustmentKey:Ljava/lang/String;

.field private autoBrightnessKey:Ljava/lang/String;

.field private brightnessKey:Ljava/lang/String;

.field private bus:Lcom/squareup/otto/Bus;

.field private context:Landroid/content/Context;

.field private lastBrightness:F

.field private ledBrightnessKey:Ljava/lang/String;

.field private mainHandler:Landroid/os/Handler;

.field private preferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;
    .param p4, "brightnessKey"    # Ljava/lang/String;
    .param p5, "autoBrightnessKey"    # Ljava/lang/String;
    .param p6, "autoBrightnessAdjustmentKey"    # Ljava/lang/String;
    .param p7, "ledBrightnessKey"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x80

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->lastBrightness:F

    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->context:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->bus:Lcom/squareup/otto/Bus;

    .line 60
    invoke-virtual {p2, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 61
    iput-object p3, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->preferences:Landroid/content/SharedPreferences;

    .line 62
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->mainHandler:Landroid/os/Handler;

    .line 63
    invoke-interface {p3, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 64
    iput-object p4, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->brightnessKey:Ljava/lang/String;

    .line 65
    iput-object p5, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->autoBrightnessKey:Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->autoBrightnessAdjustmentKey:Ljava/lang/String;

    .line 67
    iput-object p7, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->ledBrightnessKey:Ljava/lang/String;

    .line 70
    const-string v2, "hw.navdy.autobrightnessd"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->getAutoBrightnessProperty()Z

    move-result v0

    .line 72
    .local v0, "autoBrightnessEnabled":Z
    invoke-interface {p3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    if-eqz v0, :cond_2

    const-string v2, "true"

    :goto_0
    invoke-interface {v3, p5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 75
    if-nez v0, :cond_1

    .line 76
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->getBrightnessPreference()I

    move-result v1

    .line 77
    .local v1, "initialBrightness":I
    if-ge v1, v4, :cond_0

    .line 78
    invoke-interface {p3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 79
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 80
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 82
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->brightnessKey:Ljava/lang/String;

    invoke-virtual {p0, p3, v2}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 85
    .end local v1    # "initialBrightness":I
    :cond_1
    invoke-virtual {p0, p3, p6}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0, p3, p5}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0, p3, p7}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 88
    return-void

    .line 72
    :cond_2
    const-string v2, "false"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;)F
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    .prologue
    .line 18
    iget v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->lastBrightness:F

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;F)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;
    .param p1, "x1"    # F

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->updateWindowBrightness(F)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->mainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getAutoBrightnessAdjustmentPreference()I
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->autoBrightnessAdjustmentKey:Ljava/lang/String;

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private final getAutoBrightnessProperty()Z
    .locals 3

    .prologue
    .line 159
    const-string v1, "persist.sys.autobrightness"

    const-string v2, "on"

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "autoBrightness":Ljava/lang/String;
    const-string v1, "on"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getBrightnessPreference()I
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->brightnessKey:Ljava/lang/String;

    const-string v2, "128"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private setAutoBrightnessAdjustment(I)V
    .locals 4
    .param p1, "val"    # I

    .prologue
    .line 165
    if-ltz p1, :cond_0

    int-to-float v1, p1

    const/high16 v2, 0x437f0000    # 255.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    const-string v0, "screen_auto_brightness_adj"

    .line 170
    .local v0, "SCREEN_AUTO_BRIGHTNESS_ADJ":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$3;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$3;-><init>(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;I)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private updateWindowBrightness(F)V
    .locals 2
    .param p1, "brightness"    # F

    .prologue
    .line 182
    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 183
    .local v0, "layout":Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 184
    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 185
    return-void
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 146
    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->brightnessKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->getBrightnessPreference()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->setBrightnessValue(I)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->autoBrightnessAdjustmentKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    invoke-direct {p0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->getAutoBrightnessAdjustmentPreference()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->setAutoBrightnessAdjustment(I)V

    goto :goto_0

    .line 150
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->autoBrightnessKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151
    const-string v1, "false"

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 152
    .local v0, "autoBrightness":Z
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->toggleAutoBrightness(Z)V

    goto :goto_0

    .line 153
    .end local v0    # "autoBrightness":Z
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->ledBrightnessKey:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    const-string v1, "255"

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->setLEDValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBrightnessValue(I)V
    .locals 3
    .param p1, "val"    # I

    .prologue
    .line 96
    if-ltz p1, :cond_0

    const/16 v0, 0xff

    if-le p1, v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting brightness to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 100
    int-to-float v0, p1

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->lastBrightness:F

    .line 102
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$1;-><init>(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 110
    iget v0, p0, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->lastBrightness:F

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;->updateWindowBrightness(F)V

    goto :goto_0
.end method

.method public setLEDValue(Ljava/lang/String;)V
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 115
    .local v0, "val":I
    if-ltz v0, :cond_0

    const/16 v1, 0xff

    if-le v0, v1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    const-string v1, "hw.navdy.led_max_brightness"

    const-string v2, "%f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    int-to-float v5, v0

    const/high16 v6, 0x437f0000    # 255.0f

    div-float/2addr v5, v6

    .line 121
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public toggleAutoBrightness(Z)V
    .locals 3
    .param p1, "autoBrightness"    # Z

    .prologue
    .line 125
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl$2;-><init>(Lcom/navdy/hud/app/settings/AdaptiveBrightnessControl;Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 142
    return-void
.end method
