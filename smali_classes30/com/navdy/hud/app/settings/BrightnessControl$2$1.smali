.class Lcom/navdy/hud/app/settings/BrightnessControl$2$1;
.super Ljava/lang/Object;
.source "BrightnessControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/settings/BrightnessControl$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/settings/BrightnessControl$2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/settings/BrightnessControl$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/settings/BrightnessControl$2;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/hud/app/settings/BrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/BrightnessControl$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/BrightnessControl$2;

    iget-object v1, v0, Lcom/navdy/hud/app/settings/BrightnessControl$2;->this$0:Lcom/navdy/hud/app/settings/BrightnessControl;

    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/BrightnessControl$2;

    iget-boolean v0, v0, Lcom/navdy/hud/app/settings/BrightnessControl$2;->val$autoBrightness:Z

    if-eqz v0, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    # invokes: Lcom/navdy/hud/app/settings/BrightnessControl;->updateWindowBrightness(F)V
    invoke-static {v1, v0}, Lcom/navdy/hud/app/settings/BrightnessControl;->access$200(Lcom/navdy/hud/app/settings/BrightnessControl;F)V

    .line 153
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/settings/BrightnessControl$2$1;->this$1:Lcom/navdy/hud/app/settings/BrightnessControl$2;

    iget-object v0, v0, Lcom/navdy/hud/app/settings/BrightnessControl$2;->this$0:Lcom/navdy/hud/app/settings/BrightnessControl;

    .line 152
    # getter for: Lcom/navdy/hud/app/settings/BrightnessControl;->lastBrightness:F
    invoke-static {v0}, Lcom/navdy/hud/app/settings/BrightnessControl;->access$100(Lcom/navdy/hud/app/settings/BrightnessControl;)F

    move-result v0

    goto :goto_0
.end method
