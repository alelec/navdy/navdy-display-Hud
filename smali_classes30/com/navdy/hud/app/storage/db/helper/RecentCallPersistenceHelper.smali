.class public Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;
.super Ljava/lang/Object;
.source "RecentCallPersistenceHelper.java"


# static fields
.field private static final BULK_INSERT_SQL:Ljava/lang/String; = "INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)"

.field private static final CALL_TIME_ORDINAL:I = 0x5

.field private static final CALL_TYPE_ORDINAL:I = 0x4

.field private static final CATEGORY_ORDINAL:I = 0x0

.field private static final DEFAULT_IMAGE_INDEX_ORDINAL:I = 0x6

.field private static final EMPTY_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation
.end field

.field private static final NAME_ORDINAL:I = 0x1

.field private static final NUMBER_NUMERIC_ORDINAL:I = 0x7

.field private static final NUMBER_ORDINAL:I = 0x2

.field private static final NUMBER_TYPE_ORDINAL:I = 0x3

.field private static final RECENT_CALL_ARGS:[Ljava/lang/String;

.field private static final RECENT_CALL_ORDER_BY:Ljava/lang/String; = "call_time DESC"

.field private static final RECENT_CALL_PROJECTION:[Ljava/lang/String;

.field private static final RECENT_CALL_WHERE:Ljava/lang/String; = "device_id=?"

.field private static final lockObj:Ljava/lang/Object;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sReverseComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->lockObj:Ljava/lang/Object;

    .line 38
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->EMPTY_LIST:Ljava/util/List;

    .line 40
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "category"

    aput-object v2, v0, v1

    const-string v1, "name"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "number_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "call_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "call_time"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "def_image"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "number_numeric"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->RECENT_CALL_PROJECTION:[Ljava/lang/String;

    .line 52
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->RECENT_CALL_ARGS:[Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->sReverseComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deleteRecentCalls(Ljava/lang/String;)V
    .locals 7
    .param p0, "driverId"    # Ljava/lang/String;

    .prologue
    .line 188
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 190
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 191
    new-instance v3, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v3}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v3

    .line 196
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 197
    :catch_0
    move-exception v2

    .line 198
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 200
    .end local v2    # "t":Ljava/lang/Throwable;
    :goto_0
    return-void

    .line 193
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_3
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->RECENT_CALL_ARGS:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v3, v5

    .line 194
    const-string v3, "recent_calls"

    const-string v5, "device_id=?"

    sget-object v6, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->RECENT_CALL_ARGS:[Ljava/lang/String;

    invoke-virtual {v0, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 195
    .local v1, "rows":I
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "recent-calls rows deleted:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 196
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public static getRecentsCalls(Ljava/lang/String;)Ljava/util/List;
    .locals 26
    .param p0, "driverId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 84
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    sget-object v21, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->EMPTY_LIST:Ljava/util/List;

    .line 126
    :goto_0
    return-object v21

    .line 88
    :cond_0
    sget-object v22, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v22

    .line 89
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 90
    .local v2, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_1

    .line 91
    new-instance v3, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v3}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v3

    .line 128
    .end local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    monitor-exit v22
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 93
    .restart local v2    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->RECENT_CALL_ARGS:[Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p0, v3, v6

    .line 94
    const-string v3, "recent_calls"

    sget-object v4, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->RECENT_CALL_PROJECTION:[Ljava/lang/String;

    const-string v5, "device_id=?"

    sget-object v6, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->RECENT_CALL_ARGS:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "call_time DESC"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v19

    .line 103
    .local v19, "cursor":Landroid/database/Cursor;
    if-eqz v19, :cond_2

    :try_start_2
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_3

    .line 105
    :cond_2
    sget-object v21, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->EMPTY_LIST:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 126
    :try_start_3
    invoke-static/range {v19 .. v19}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v22
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 107
    :cond_3
    :try_start_4
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v18

    .line 108
    .local v18, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v21, "recentCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 111
    .local v17, "category":I
    const/4 v3, 0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 112
    .local v5, "name":Ljava/lang/String;
    const/4 v3, 0x2

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 113
    .local v7, "number":Ljava/lang/String;
    const/4 v3, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 114
    .local v20, "numberType":I
    const/4 v3, 0x4

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 115
    .local v16, "callType":I
    const/4 v3, 0x5

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 116
    .local v14, "callTime":J
    const/4 v3, 0x6

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 117
    .local v11, "imageIndex":I
    const/4 v3, 0x7

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 118
    .local v12, "numericNumber":J
    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/framework/contacts/NumberType;->buildFromValue(I)Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v8

    .line 119
    .local v8, "callNumberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    new-instance v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    invoke-static/range {v17 .. v17}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->buildFromValue(I)Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    move-result-object v6

    new-instance v9, Ljava/util/Date;

    const-wide/16 v24, 0x3e8

    mul-long v24, v24, v14

    move-wide/from16 v0, v24

    invoke-direct {v9, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 120
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->buildFromValue(I)Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    move-result-object v10

    invoke-direct/range {v4 .. v13}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;Ljava/util/Date;Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;IJ)V

    .line 122
    .local v4, "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v3

    if-nez v3, :cond_4

    .line 126
    :try_start_5
    invoke-static/range {v19 .. v19}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v22

    goto/16 :goto_0

    .end local v4    # "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "number":Ljava/lang/String;
    .end local v8    # "callNumberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    .end local v11    # "imageIndex":I
    .end local v12    # "numericNumber":J
    .end local v14    # "callTime":J
    .end local v16    # "callType":I
    .end local v17    # "category":I
    .end local v18    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v20    # "numberType":I
    .end local v21    # "recentCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    :catchall_1
    move-exception v3

    invoke-static/range {v19 .. v19}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private static mergeRecentCalls(Ljava/util/List;Ljava/util/List;Z)Ljava/util/List;
    .locals 13
    .param p2, "fromPbap"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "existingCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .local p1, "newCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    const/16 v12, 0x1e

    .line 204
    new-instance v2, Ljava/util/HashMap;

    const/16 v6, 0x34

    invoke-direct {v2, v6}, Ljava/util/HashMap;-><init>(I)V

    .line 207
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    if-eqz p0, :cond_2

    .line 208
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 209
    .local v0, "existingCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    if-eqz p2, :cond_1

    iget-object v7, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    sget-object v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    if-eq v7, v8, :cond_0

    .line 213
    :cond_1
    iget-wide v8, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 216
    .end local v0    # "existingCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_2
    if-eqz p1, :cond_7

    .line 217
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 218
    .local v4, "newCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    iget-wide v8, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 219
    .restart local v0    # "existingCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    if-nez v0, :cond_4

    .line 221
    iget-wide v8, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 223
    :cond_4
    if-eqz p2, :cond_5

    .line 225
    iget-wide v8, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 227
    :cond_5
    iget-object v7, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    iget-object v7, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-ltz v7, :cond_3

    .line 229
    iget-object v7, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    sget-object v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->MESSAGE:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    if-ne v7, v8, :cond_6

    iget-object v7, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 231
    iget-object v7, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    iput-object v7, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    .line 233
    :cond_6
    iget-wide v8, v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 240
    .end local v0    # "existingCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v4    # "newCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_7
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 241
    .local v3, "mergedList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    sget-object v6, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->sReverseComparator:Ljava/util/Comparator;

    invoke-static {v3, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 242
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v12, :cond_9

    .line 244
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 245
    .local v5, "truncatedList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v12, :cond_8

    .line 246
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 248
    :cond_8
    move-object v3, v5

    .line 250
    .end local v1    # "i":I
    .end local v5    # "truncatedList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    :cond_9
    return-object v3
.end method

.method public static storeRecentCalls(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 18
    .param p0, "driverId"    # Ljava/lang/String;
    .param p2, "fromPbap"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "newCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 133
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    move-result-object v7

    .line 134
    .local v7, "recentCallManager":Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getRecentCalls()Ljava/util/List;

    move-result-object v3

    .line 135
    .local v3, "currentRecentCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v3, v0, v1}, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->mergeRecentCalls(Ljava/util/List;Ljava/util/List;Z)Ljava/util/List;

    move-result-object v5

    .line 136
    .local v5, "mergedCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->deleteRecentCalls(Ljava/lang/String;)V

    .line 137
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_0

    .line 138
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->setRecentCalls(Ljava/util/List;)V

    .line 184
    :goto_0
    return-void

    .line 141
    :cond_0
    if-eqz p2, :cond_1

    .line 142
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->clearAllPhotoCheckEntries()V

    .line 144
    :cond_1
    sget-object v11, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v11

    .line 145
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 146
    .local v4, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v4, :cond_2

    .line 147
    new-instance v10, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v10}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v10

    .line 183
    .end local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 149
    .restart local v4    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_2
    :try_start_1
    const-string v10, "INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)"

    invoke-virtual {v4, v10}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 150
    .local v9, "sqLiteStatement":Landroid/database/sqlite/SQLiteStatement;
    const/4 v8, 0x0

    .line 152
    .local v8, "rows":I
    :try_start_2
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v2

    .line 153
    .local v2, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 154
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 155
    .local v6, "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 156
    const/4 v12, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v9, v12, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 157
    const/4 v12, 0x2

    iget-object v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    invoke-virtual {v13}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->getValue()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v9, v12, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 158
    iget-object v12, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 159
    const/4 v12, 0x3

    iget-object v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 163
    :goto_2
    const/4 v12, 0x4

    iget-object v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 164
    const/4 v12, 0x5

    iget-object v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-virtual {v13}, Lcom/navdy/hud/app/framework/contacts/NumberType;->getValue()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v9, v12, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 165
    const/4 v12, 0x6

    iget-object v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    invoke-virtual {v13}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    invoke-virtual {v9, v12, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 166
    const/4 v12, 0x7

    iget-object v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callType:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    invoke-virtual {v13}, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->getValue()I

    move-result v13

    int-to-long v14, v13

    invoke-virtual {v9, v12, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 167
    iget-object v12, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v2, v12}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v12

    iput v12, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    .line 168
    const/16 v12, 0x8

    iget v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    int-to-long v14, v13

    invoke-virtual {v9, v12, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 169
    const/16 v12, 0x9

    iget-wide v14, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-virtual {v9, v12, v14, v15}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 170
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 171
    add-int/lit8 v8, v8, 0x1

    .line 173
    if-eqz p2, :cond_3

    .line 174
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v12

    iget-object v13, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    sget-object v14, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->NORMAL:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    sget-object v15, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v0, v6, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v12 .. v16}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->submitDownload(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_1

    .line 180
    .end local v2    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v6    # "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :catchall_1
    move-exception v10

    :try_start_3
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 161
    .restart local v2    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .restart local v6    # "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_4
    const/4 v12, 0x3

    :try_start_4
    invoke-virtual {v9, v12}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_2

    .line 177
    .end local v6    # "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_5
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 178
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    move-result-object v10

    invoke-virtual {v10, v5}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->setRecentCalls(Ljava/util/List;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 180
    :try_start_5
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 182
    sget-object v10, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "recent-calls rows added:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 183
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method
