.class public Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;
.super Ljava/lang/Object;
.source "FavoriteContactsPersistenceHelper.java"


# static fields
.field private static final BULK_INSERT_SQL:Ljava/lang/String; = "INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)"

.field private static final DEFAULT_IMAGE_INDEX_ORDINAL:I = 0x3

.field private static final EMPTY_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private static final FAV_CONTACT_ARGS:[Ljava/lang/String;

.field private static final FAV_CONTACT_ORDER_BY:Ljava/lang/String; = "rowid"

.field private static final FAV_CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final FAV_CONTACT_WHERE:Ljava/lang/String; = "device_id=?"

.field private static final NAME_ORDINAL:I = 0x0

.field private static final NUMBER_NUMERIC_ORDINAL:I = 0x4

.field private static final NUMBER_ORDINAL:I = 0x1

.field private static final NUMBER_TYPE_ORDINAL:I = 0x2

.field private static final lockObj:Ljava/lang/Object;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->lockObj:Ljava/lang/Object;

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->EMPTY_LIST:Ljava/util/List;

    .line 35
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    const-string v1, "number"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "number_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "def_image"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "number_numeric"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->FAV_CONTACT_PROJECTION:[Ljava/lang/String;

    .line 44
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->FAV_CONTACT_ARGS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deleteFavoriteContacts(Ljava/lang/String;)V
    .locals 7
    .param p0, "driverId"    # Ljava/lang/String;

    .prologue
    .line 162
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 164
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 165
    new-instance v3, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v3}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v3

    .line 170
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 171
    :catch_0
    move-exception v2

    .line 172
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 174
    .end local v2    # "t":Ljava/lang/Throwable;
    :goto_0
    return-void

    .line 167
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_3
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->FAV_CONTACT_ARGS:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v3, v5

    .line 168
    const-string v3, "fav_contacts"

    const-string v5, "device_id=?"

    sget-object v6, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->FAV_CONTACT_ARGS:[Ljava/lang/String;

    invoke-virtual {v0, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 169
    .local v1, "rows":I
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fav-contact rows deleted:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public static getFavoriteContacts(Ljava/lang/String;)Ljava/util/List;
    .locals 15
    .param p0, "driverId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 63
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 65
    sget-object v10, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->EMPTY_LIST:Ljava/util/List;

    .line 100
    :goto_0
    return-object v10

    .line 67
    :cond_0
    sget-object v13, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v13

    .line 68
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 69
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_1

    .line 70
    new-instance v12, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v12}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v12

    .line 102
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v12

    .line 72
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :try_start_1
    sget-object v12, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->FAV_CONTACT_ARGS:[Ljava/lang/String;

    const/4 v14, 0x0

    aput-object p0, v12, v14

    .line 73
    const-string v1, "fav_contacts"

    sget-object v2, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->FAV_CONTACT_PROJECTION:[Ljava/lang/String;

    const-string v3, "device_id=?"

    sget-object v4, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->FAV_CONTACT_ARGS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "rowid"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 82
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_2

    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v12

    if-nez v12, :cond_3

    .line 84
    :cond_2
    sget-object v10, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->EMPTY_LIST:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 100
    :try_start_3
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 86
    :cond_3
    :try_start_4
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v8

    .line 87
    .local v8, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v10, "favContacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    :cond_4
    const/4 v12, 0x0

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    .local v2, "name":Ljava/lang/String;
    const/4 v12, 0x1

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 91
    .local v3, "number":Ljava/lang/String;
    const/4 v12, 0x2

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 92
    .local v11, "numberType":I
    const/4 v12, 0x3

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 93
    .local v5, "imageIndex":I
    const/4 v12, 0x4

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 94
    .local v6, "numericNumber":J
    invoke-static {v11}, Lcom/navdy/hud/app/framework/contacts/NumberType;->buildFromValue(I)Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v4

    .line 95
    .local v4, "callNumberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    new-instance v1, Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct/range {v1 .. v7}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;IJ)V

    .line 96
    .local v1, "favContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v12

    if-nez v12, :cond_4

    .line 100
    :try_start_5
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v13

    goto :goto_0

    .end local v1    # "favContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "number":Ljava/lang/String;
    .end local v4    # "callNumberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    .end local v5    # "imageIndex":I
    .end local v6    # "numericNumber":J
    .end local v8    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v10    # "favContacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .end local v11    # "numberType":I
    :catchall_1
    move-exception v12

    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public static storeFavoriteContacts(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 13
    .param p0, "driverId"    # Ljava/lang/String;
    .param p2, "refreshPhotos"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "favContacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 109
    sget-object v6, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fav-contacts passed ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    move-result-object v3

    .line 111
    .local v3, "favoriteContactsManager":Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;
    invoke-static {p0}, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->deleteFavoriteContacts(Ljava/lang/String;)V

    .line 112
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_0

    .line 113
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->setFavoriteContacts(Ljava/util/List;)V

    .line 158
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->clearAllPhotoCheckEntries()V

    .line 117
    sget-object v7, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v7

    .line 118
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 119
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_1

    .line 120
    new-instance v6, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v6}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v6

    .line 157
    .end local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 122
    .restart local v1    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :try_start_1
    const-string v6, "INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)"

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 123
    .local v5, "sqLiteStatement":Landroid/database/sqlite/SQLiteStatement;
    const/4 v4, 0x0

    .line 125
    .local v4, "rows":I
    :try_start_2
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v0

    .line 126
    .local v0, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 127
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 128
    .local v2, "favContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 129
    const/4 v8, 0x1

    invoke-virtual {v5, v8, p0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 130
    iget-object v8, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 131
    const/4 v8, 0x2

    iget-object v9, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 135
    :goto_2
    const/4 v8, 0x3

    iget-object v9, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 136
    const/4 v8, 0x4

    iget-object v9, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/contacts/NumberType;->getValue()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v8, v10, v11}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 137
    iget-object v8, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v8

    iput v8, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 138
    const/4 v8, 0x5

    iget v9, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    int-to-long v10, v9

    invoke-virtual {v5, v8, v10, v11}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 139
    const/4 v8, 0x6

    iget-wide v10, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->numericNumber:J

    invoke-virtual {v5, v8, v10, v11}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 140
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 141
    add-int/lit8 v4, v4, 0x1

    .line 142
    if-eqz p2, :cond_2

    .line 144
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v8

    iget-object v9, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    sget-object v10, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->NORMAL:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    sget-object v11, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v12, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 145
    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->submitDownload(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 154
    .end local v0    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v2    # "favContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    :catchall_1
    move-exception v6

    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 133
    .restart local v0    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .restart local v2    # "favContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    :cond_3
    const/4 v8, 0x2

    :try_start_4
    invoke-virtual {v5, v8}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_2

    .line 151
    .end local v2    # "favContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    :cond_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 152
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->setFavoriteContacts(Ljava/util/List;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 154
    :try_start_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 156
    sget-object v6, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fav-contacts rows added:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 157
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method
