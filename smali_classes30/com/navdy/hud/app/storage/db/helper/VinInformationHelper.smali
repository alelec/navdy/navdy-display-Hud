.class public Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;
.super Ljava/lang/Object;
.source "VinInformationHelper.java"


# static fields
.field private static final DATASET_ARGS:[Ljava/lang/String;

.field private static final DATASET_PROJECTION:[Ljava/lang/String;

.field private static final DATASET_WHERE:Ljava/lang/String; = "vin=?"

.field private static final VIN_INFO_ORDINAL:I = 0x0

.field public static final VIN_SHARED_PREF:Ljava/lang/String; = "vin"

.field private static final VIN_SHARED_PREF_NAME:Ljava/lang/String; = "activeVin"

.field private static final lockObj:Ljava/lang/Object;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 27
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "info"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->DATASET_PROJECTION:[Ljava/lang/String;

    .line 29
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->DATASET_ARGS:[Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->lockObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteVinInfo(Ljava/lang/String;)V
    .locals 7
    .param p0, "vin"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 93
    sget-object v4, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 95
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 96
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 97
    new-instance v3, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v3}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v2

    .line 103
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 105
    .end local v2    # "t":Ljava/lang/Throwable;
    :goto_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    return-void

    .line 99
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_2
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->DATASET_ARGS:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v3, v5

    .line 100
    const-string v3, "vininfo"

    const-string v5, "vin=?"

    sget-object v6, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->DATASET_ARGS:[Ljava/lang/String;

    invoke-virtual {v0, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 101
    .local v1, "n":I
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "vin info deleted:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 105
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v1    # "n":I
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3
.end method

.method public static getVinInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "vin"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 35
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 36
    sget-object v10, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v10

    .line 37
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 38
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 39
    new-instance v1, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v1}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v1

    .line 64
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v1

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 42
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->DATASET_ARGS:[Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    .line 44
    const-string v1, "vininfo"

    sget-object v2, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->DATASET_PROJECTION:[Ljava/lang/String;

    const-string v3, "vin=?"

    sget-object v4, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->DATASET_ARGS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 55
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v1

    if-nez v1, :cond_2

    .line 62
    :cond_1
    :try_start_3
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v1, v9

    :goto_0
    return-object v1

    .line 59
    :cond_2
    const/4 v1, 0x0

    :try_start_4
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v1

    .line 62
    :try_start_5
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v10

    goto :goto_0

    :catchall_1
    move-exception v1

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public static getVinPreference()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 109
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "activeVin"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static storeVinInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "vin"    # Ljava/lang/String;
    .param p1, "info"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 69
    invoke-static {p0}, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->deleteVinInfo(Ljava/lang/String;)V

    .line 71
    sget-object v4, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->lockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 73
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 74
    .local v0, "database":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 75
    new-instance v3, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;

    invoke-direct {v3}, Lcom/navdy/hud/app/storage/db/DatabaseUtil$DatabaseNotAvailable;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v1

    .line 85
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 87
    .end local v1    # "t":Ljava/lang/Throwable;
    :goto_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    return-void

    .line 78
    .restart local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 79
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "vin"

    invoke-virtual {v2, v3, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v3, "info"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v3, "vininfo"

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 83
    sget-object v3, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "added vin info [ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 87
    .end local v0    # "database":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3
.end method
