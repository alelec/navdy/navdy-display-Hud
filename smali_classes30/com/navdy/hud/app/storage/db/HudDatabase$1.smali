.class final Lcom/navdy/hud/app/storage/db/HudDatabase$1;
.super Ljava/lang/Object;
.source "HudDatabase.java"

# interfaces
.implements Landroid/database/DatabaseErrorHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/storage/db/HudDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCorruption(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "dbObj"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 126
    # getter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "**** DB Corrupt ****"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 127
    const/4 v4, 0x1

    # setter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBError:Z
    invoke-static {v4}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$102(Z)Z

    .line 128
    const/4 v4, 0x0

    # setter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBErrorStr:Ljava/lang/Throwable;
    invoke-static {v4}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$202(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 130
    :try_start_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v4

    if-nez v4, :cond_0

    .line 131
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->deleteDatabaseFile()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    return-void

    .line 134
    :cond_0
    const/4 v0, 0x0

    .line 139
    .local v0, "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :try_start_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getAttachedDbs()Ljava/util/List;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 145
    :goto_1
    :try_start_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 152
    :goto_2
    if-eqz v0, :cond_1

    .line 153
    :try_start_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 154
    .local v2, "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    # getter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$300()Landroid/content/Context;

    move-result-object v6

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v6, v4}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 159
    .end local v0    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v2    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 160
    .local v3, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 140
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v0    # "attachedDbs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :catch_1
    move-exception v1

    .line 142
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    # getter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 152
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    move-object v5, v4

    if-eqz v0, :cond_2

    .line 153
    :try_start_5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 154
    .restart local v2    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    # getter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$300()Landroid/content/Context;

    move-result-object v7

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v7, v4}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_4

    .line 146
    .end local v2    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_2
    move-exception v1

    .line 148
    .restart local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    # getter for: Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 157
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    :try_start_7
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->deleteDatabaseFile()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->deleteDatabaseFile()V

    throw v5
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
.end method
