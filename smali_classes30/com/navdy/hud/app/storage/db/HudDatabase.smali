.class public Lcom/navdy/hud/app/storage/db/HudDatabase;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "HudDatabase.java"


# static fields
.field private static final DATABASE_FILE:Ljava/lang/String;

.field private static final DATABASE_NAME:Ljava/lang/String; = "hud.db"

.field private static final DATABASE_VERSION:I = 0x11

.field private static final UPGRADE_METHOD_PREFIX:Ljava/lang/String; = "upgradeDatabase_"

.field private static final VERSION_1:I = 0x1

.field private static final VERSION_10:I = 0xa

.field private static final VERSION_11:I = 0xb

.field private static final VERSION_12:I = 0xc

.field private static final VERSION_13:I = 0xd

.field private static final VERSION_14:I = 0xe

.field private static final VERSION_15:I = 0xf

.field private static final VERSION_16:I = 0x10

.field private static final VERSION_17:I = 0x11

.field private static final VERSION_2:I = 0x2

.field private static final VERSION_3:I = 0x3

.field private static final VERSION_4:I = 0x4

.field private static final VERSION_5:I = 0x5

.field private static final VERSION_6:I = 0x6

.field private static final VERSION_7:I = 0x7

.field private static final VERSION_8:I = 0x8

.field private static final VERSION_9:I = 0x9

.field private static final context:Landroid/content/Context;

.field private static final lockObj:Ljava/lang/Object;

.field private static volatile sDBError:Z

.field private static volatile sDBErrorStr:Ljava/lang/Throwable;

.field private static sDatabaseErrorHandler:Landroid/database/DatabaseErrorHandler;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;

.field private static volatile sSingleton:Lcom/navdy/hud/app/storage/db/HudDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/storage/db/HudDatabase;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/storage/PathManager;->getDatabaseDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hud.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->DATABASE_FILE:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->lockObj:Ljava/lang/Object;

    .line 121
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;

    .line 123
    new-instance v0, Lcom/navdy/hud/app/storage/db/HudDatabase$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/storage/db/HudDatabase$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDatabaseErrorHandler:Landroid/database/DatabaseErrorHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    .line 192
    sget-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;

    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->DATABASE_FILE:Ljava/lang/String;

    const/4 v3, 0x0

    const/16 v4, 0x11

    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDatabaseErrorHandler:Landroid/database/DatabaseErrorHandler;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILandroid/database/DatabaseErrorHandler;)V

    .line 193
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 41
    sput-boolean p0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBError:Z

    return p0
.end method

.method static synthetic access$202(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Throwable;

    .prologue
    .line 41
    sput-object p0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBErrorStr:Ljava/lang/Throwable;

    return-object p0
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;

    return-object v0
.end method

.method public static deleteDatabaseFile()V
    .locals 8

    .prologue
    .line 324
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "deleteDatabaseFile"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 326
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->DATABASE_FILE:Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 327
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 328
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 329
    .local v4, "str":Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 330
    .local v3, "ret":Z
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 333
    .end local v3    # "ret":Z
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->DATABASE_FILE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-journal"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 334
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 335
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 336
    .restart local v4    # "str":Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 337
    .restart local v3    # "ret":Z
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 339
    .end local v3    # "ret":Z
    .end local v4    # "str":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/io/File;

    .end local v2    # "file":Ljava/io/File;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->DATABASE_FILE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-shm"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 340
    .restart local v2    # "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 341
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 342
    .restart local v4    # "str":Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 343
    .restart local v3    # "ret":Z
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 345
    .end local v3    # "ret":Z
    .end local v4    # "str":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/io/File;

    .end local v2    # "file":Ljava/io/File;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->DATABASE_FILE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-wal"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 346
    .restart local v2    # "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 347
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 348
    .restart local v4    # "str":Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->context:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 349
    .restart local v3    # "ret":Z
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 351
    .end local v3    # "ret":Z
    .end local v4    # "str":Ljava/lang/String;
    :cond_3
    invoke-static {v0}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    .end local v2    # "file":Ljava/io/File;
    :goto_0
    return-void

    .line 352
    :catch_0
    move-exception v1

    .line 353
    .local v1, "e":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error while trying to delete dbase"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getErrorStr()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 362
    sget-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBErrorStr:Ljava/lang/Throwable;

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;
    .locals 5

    .prologue
    .line 166
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sSingleton:Lcom/navdy/hud/app/storage/db/HudDatabase;

    if-nez v2, :cond_2

    .line 167
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 168
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sSingleton:Lcom/navdy/hud/app/storage/db/HudDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 170
    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->DATABASE_FILE:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/hud/app/storage/db/HudDatabase;->isDatabaseStable(Ljava/lang/String;)Z

    move-result v0

    .line 171
    .local v0, "ret":Z
    if-nez v0, :cond_0

    .line 173
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "db is not stable, deleting it"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 174
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->deleteDatabaseFile()V

    .line 175
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "db delete complete"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 177
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "creating db-instance"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    new-instance v2, Lcom/navdy/hud/app/storage/db/HudDatabase;

    invoke-direct {v2}, Lcom/navdy/hud/app/storage/db/HudDatabase;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sSingleton:Lcom/navdy/hud/app/storage/db/HudDatabase;

    .line 179
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "created db-instance"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :cond_1
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 188
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sSingleton:Lcom/navdy/hud/app/storage/db/HudDatabase;

    return-object v2

    .line 180
    :catch_0
    move-exception v1

    .line 181
    .local v1, "t":Ljava/lang/Throwable;
    const/4 v2, 0x1

    :try_start_3
    sput-boolean v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBError:Z

    .line 182
    sput-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBErrorStr:Ljava/lang/Throwable;

    .line 183
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 186
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method

.method private static isDatabaseIntegrityFine(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 10
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 258
    const/4 v5, 0x0

    .line 260
    .local v5, "stmt":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 261
    .local v0, "l1":J
    const-string v6, "PRAGMA integrity_check"

    invoke-virtual {p0, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    .line 262
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForString()Ljava/lang/String;

    move-result-object v4

    .line 263
    .local v4, "result":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 264
    .local v2, "l2":J
    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "db integrity check took:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-long v8, v2, v0

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 265
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "ok"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    .line 266
    const/4 v6, 0x1

    .line 271
    if-eqz v5, :cond_0

    .line 272
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_0
    :goto_0
    return v6

    .line 268
    :cond_1
    const/4 v6, 0x0

    .line 271
    if-eqz v5, :cond_0

    .line 272
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto :goto_0

    .line 271
    .end local v0    # "l1":J
    .end local v2    # "l2":J
    .end local v4    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v6

    if-eqz v5, :cond_2

    .line 272
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_2
    throw v6
.end method

.method private static isDatabaseStable(Ljava/lang/String;)Z
    .locals 12
    .param p0, "dbFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 278
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 279
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 320
    :cond_0
    :goto_0
    return v3

    .line 285
    :cond_1
    const/4 v7, 0x0

    const/16 v8, 0x10

    :try_start_0
    invoke-static {p0, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 286
    .local v1, "dbase":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_2

    .line 287
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "dbase could not be opened"

    invoke-virtual {v3, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v6

    .line 288
    goto :goto_0

    .line 290
    .end local v1    # "dbase":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v2

    .line 291
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "dbase could not be opened"

    invoke-virtual {v3, v7, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v3, v6

    .line 292
    goto :goto_0

    .line 296
    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "dbase":Landroid/database/sqlite/SQLiteDatabase;
    :cond_2
    :try_start_1
    invoke-static {v1}, Lcom/navdy/hud/app/storage/db/HudDatabase;->isDatabaseIntegrityFine(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 298
    sget-object v7, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "data integrity is not valid"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    move v3, v6

    goto :goto_0

    .line 301
    :cond_3
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 302
    sget-object v7, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "dbase in transaction"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 303
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 305
    :cond_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isDbLockedByCurrentThread()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 306
    sget-object v7, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "dbase is currently locked"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 308
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 309
    .local v4, "l1":J
    const-string v7, "VACUUM"

    invoke-virtual {v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 310
    sget-object v7, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "dbase VACCUM complete in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 314
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 316
    .end local v4    # "l1":J
    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v7

    if-nez v7, :cond_0

    .line 319
    :cond_6
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "dbase not stable"

    invoke-virtual {v3, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move v3, v6

    .line 320
    goto/16 :goto_0

    .line 311
    :catch_1
    move-exception v2

    .line 312
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v7, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v7, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 314
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_1

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v3
.end method

.method public static isInErrorState()Z
    .locals 1

    .prologue
    .line 358
    sget-boolean v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBError:Z

    return v0
.end method

.method public static upgradeDatabase_10(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 417
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 418
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "gesture.engine"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 419
    sget-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "enabling gesture engine"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 420
    return-void
.end method

.method public static upgradeDatabase_11(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 424
    :try_start_0
    const-string v1, "persist.sys.hud_gps"

    .line 425
    .local v1, "property":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "trying to set ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 431
    sget-object v3, Lcom/navdy/hud/app/util/SerialNumber;->instance:Lcom/navdy/hud/app/util/SerialNumber;

    iget-object v3, v3, Lcom/navdy/hud/app/util/SerialNumber;->revisionCode:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 432
    .local v0, "c":C
    const/16 v3, 0x34

    if-lt v0, v3, :cond_0

    const/16 v3, 0x36

    if-gt v0, v3, :cond_0

    .line 433
    const-string v3, "-1"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setting property for ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] to [-1] digit["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 441
    .end local v0    # "c":C
    .end local v1    # "property":Ljava/lang/String;
    :goto_0
    return-void

    .line 436
    .restart local v0    # "c":C
    .restart local v1    # "property":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "not setting property ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 438
    .end local v0    # "c":C
    .end local v1    # "property":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 439
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "dbupgrade --> 11 failed"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static upgradeDatabase_12(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 445
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 446
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string v4, ".Fabric"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 447
    .local v1, "dir":Ljava/io/File;
    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 448
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "dbupgrade --> Fabric dir removed"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "dir":Ljava/io/File;
    :goto_0
    return-void

    .line 449
    :catch_0
    move-exception v2

    .line 450
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "dbupgrade --> 12 failed"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static upgradeDatabase_13(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 456
    invoke-static {p0}, Lcom/navdy/hud/app/storage/db/table/VinInformationTable;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 457
    return-void
.end method

.method public static upgradeDatabase_14(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 464
    :try_start_0
    const-string v0, "/data/anr"

    .line 465
    .local v0, "NATIVE_CRASH_PATH":Ljava/lang/String;
    const-string v1, "native_crash_"

    .line 467
    .local v1, "NATIVE_CRASH_PATTERN":Ljava/lang/String;
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "looking for native crash files"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 468
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 469
    .local v3, "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 470
    new-instance v11, Lcom/navdy/hud/app/storage/db/HudDatabase$2;

    invoke-direct {v11}, Lcom/navdy/hud/app/storage/db/HudDatabase$2;-><init>()V

    invoke-virtual {v3, v11}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v6

    .line 481
    .local v6, "list":[Ljava/lang/String;
    if-eqz v6, :cond_0

    array-length v11, v6

    if-nez v11, :cond_4

    .line 482
    :cond_0
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "no native crash files"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    .end local v0    # "NATIVE_CRASH_PATH":Ljava/lang/String;
    .end local v1    # "NATIVE_CRASH_PATTERN":Ljava/lang/String;
    .end local v3    # "f":Ljava/io/File;
    .end local v6    # "list":[Ljava/lang/String;
    :cond_1
    :goto_0
    :try_start_1
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "looking for tombstone"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 500
    const-string v2, "/data/tombstones"

    .line 501
    .local v2, "TOMBSTONE_PATH":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 502
    .restart local v3    # "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 503
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 504
    .restart local v6    # "list":[Ljava/lang/String;
    if-eqz v6, :cond_2

    array-length v11, v6

    if-nez v11, :cond_6

    .line 505
    :cond_2
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "no tombstones"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 522
    .end local v2    # "TOMBSTONE_PATH":Ljava/lang/String;
    .end local v3    # "f":Ljava/io/File;
    .end local v6    # "list":[Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 484
    .restart local v0    # "NATIVE_CRASH_PATH":Ljava/lang/String;
    .restart local v1    # "NATIVE_CRASH_PATTERN":Ljava/lang/String;
    .restart local v3    # "f":Ljava/io/File;
    .restart local v6    # "list":[Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    :try_start_2
    array-length v11, v6

    if-ge v5, v11, :cond_1

    .line 485
    new-instance v7, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v6, v5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 486
    .local v7, "nativeCrashFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 487
    .local v4, "fileName":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v8

    .line 488
    .local v8, "ret":Z
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "removing native crash file["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 484
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 492
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "list":[Ljava/lang/String;
    .end local v7    # "nativeCrashFile":Ljava/io/File;
    .end local v8    # "ret":Z
    :cond_5
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "no native crashes to delete"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 494
    .end local v0    # "NATIVE_CRASH_PATH":Ljava/lang/String;
    .end local v1    # "NATIVE_CRASH_PATTERN":Ljava/lang/String;
    .end local v3    # "f":Ljava/io/File;
    :catch_0
    move-exception v9

    .line 495
    .local v9, "t":Ljava/lang/Throwable;
    :try_start_3
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v11, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 519
    .end local v9    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v9

    .line 520
    .restart local v9    # "t":Ljava/lang/Throwable;
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v11, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 507
    .end local v9    # "t":Ljava/lang/Throwable;
    .restart local v2    # "TOMBSTONE_PATH":Ljava/lang/String;
    .restart local v3    # "f":Ljava/io/File;
    .restart local v6    # "list":[Ljava/lang/String;
    :cond_6
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    :try_start_4
    array-length v11, v6

    if-ge v5, v11, :cond_3

    .line 508
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v6, v5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 509
    .local v10, "tombstoneFile":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 512
    .restart local v4    # "fileName":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    move-result v8

    .line 513
    .restart local v8    # "ret":Z
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "removing tombstone file["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 507
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 517
    .end local v4    # "fileName":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "list":[Ljava/lang/String;
    .end local v8    # "ret":Z
    .end local v10    # "tombstoneFile":Ljava/io/File;
    :cond_7
    sget-object v11, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "no tombstones to delete"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1
.end method

.method public static upgradeDatabase_15(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 526
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 527
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_low_voltage_event"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 528
    return-void
.end method

.method public static upgradeDatabase_16(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 531
    invoke-static {p0}, Lcom/navdy/hud/app/storage/db/table/MusicArtworkCacheTable;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 532
    return-void
.end method

.method public static upgradeDatabase_17(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 536
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 537
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dial_reminder_time"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 538
    return-void
.end method

.method public static upgradeDatabase_2(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 372
    invoke-static {p0}, Lcom/navdy/hud/app/storage/db/table/RecentCallsTable;->upgradeDatabase_2(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 374
    invoke-static {p0}, Lcom/navdy/hud/app/storage/db/table/FavoriteContactsTable;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 375
    return-void
.end method

.method public static upgradeDatabase_3(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 379
    return-void
.end method

.method public static upgradeDatabase_4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 383
    return-void
.end method

.method public static upgradeDatabase_5(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 387
    return-void
.end method

.method public static upgradeDatabase_6(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 391
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 392
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "map.scheme"

    const-string v3, "carnav.day"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 393
    sget-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "map scheme changed to carnav.day"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 394
    return-void
.end method

.method public static upgradeDatabase_7(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 398
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/storage/PathManager;->getHereVoiceSkinsPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "en-US_TTS"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    .local v0, "dir":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 400
    sget-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleted old here voice skins ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 401
    return-void
.end method

.method public static upgradeDatabase_8(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 404
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 405
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "screen.led_brightness"

    const-string v3, "255"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 406
    sget-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "default max LED brightness changed to 255"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 407
    return-void
.end method

.method public static upgradeDatabase_9(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 410
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 411
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "gesture.engine"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 412
    sget-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "disabling gesture engine"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 413
    return-void
.end method


# virtual methods
.method public declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 242
    monitor-enter p0

    const/4 v0, 0x0

    .line 244
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 254
    :goto_0
    monitor-exit p0

    return-object v0

    .line 245
    :catch_0
    move-exception v1

    .line 246
    .local v1, "ex":Ljava/lang/Throwable;
    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 247
    instance-of v2, v1, Landroid/database/sqlite/SQLiteDatabaseCorruptException;

    if-eqz v2, :cond_0

    .line 248
    sget-object v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, ":: db corrupted"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    .end local v1    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 250
    .restart local v1    # "ex":Ljava/lang/Throwable;
    :cond_0
    const/4 v2, 0x1

    :try_start_2
    sput-boolean v2, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBError:Z

    .line 251
    sput-object v1, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBErrorStr:Ljava/lang/Throwable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 197
    const/4 v0, 0x0

    sput-boolean v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBError:Z

    .line 198
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sDBErrorStr:Ljava/lang/Throwable;

    .line 199
    sget-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCreate::start::"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 203
    :try_start_0
    invoke-static {p1}, Lcom/navdy/hud/app/storage/db/table/RecentCallsTable;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 204
    invoke-static {p1}, Lcom/navdy/hud/app/storage/db/table/FavoriteContactsTable;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 205
    invoke-static {p1}, Lcom/navdy/hud/app/storage/db/table/VinInformationTable;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 206
    invoke-static {p1}, Lcom/navdy/hud/app/storage/db/table/MusicArtworkCacheTable;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 207
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 211
    sget-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCreate::end::"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 212
    return-void

    .line 209
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 236
    sget-object v0, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onOpen::"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 237
    invoke-super {p0, p1}, Landroid/database/sqlite/SQLiteOpenHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 238
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 11
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 217
    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onUpgrade::start:: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ==>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 218
    new-array v1, v10, [Ljava/lang/Object;

    aput-object p1, v1, v9

    .line 219
    .local v1, "params":[Ljava/lang/Object;
    new-array v2, v10, [Ljava/lang/Class;

    const-class v6, Landroid/database/sqlite/SQLiteDatabase;

    aput-object v6, v2, v9

    .line 220
    .local v2, "reflectionParams":[Ljava/lang/Class;
    add-int/lit8 v0, p2, 0x1

    .local v0, "i":I
    :goto_0
    if-gt v0, p3, :cond_0

    .line 222
    :try_start_0
    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onUpgrade:: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 223
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upgradeDatabase_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 224
    .local v5, "upgradeMethodName":Ljava/lang/String;
    const-class v6, Lcom/navdy/hud/app/storage/db/HudDatabase;

    invoke-virtual {v6, v5, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 225
    .local v4, "upgradeMethod":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    .end local v4    # "upgradeMethod":Ljava/lang/reflect/Method;
    .end local v5    # "upgradeMethodName":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 227
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 228
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 231
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_0
    sget-object v6, Lcom/navdy/hud/app/storage/db/HudDatabase;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onUpgrade::end:: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ==>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 232
    return-void
.end method
