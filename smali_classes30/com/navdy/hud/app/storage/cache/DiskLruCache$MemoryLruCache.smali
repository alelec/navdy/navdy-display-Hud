.class Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;
.super Landroid/util/LruCache;
.source "DiskLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/storage/cache/DiskLruCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemoryLruCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private parent:Lcom/navdy/hud/app/storage/cache/DiskLruCache;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;I)V
    .locals 0
    .param p1, "parent"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    .param p2, "maxSize"    # I

    .prologue
    .line 46
    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->parent:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    .line 48
    return-void
.end method


# virtual methods
.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Ljava/lang/String;

    check-cast p3, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    check-cast p4, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->entryRemoved(ZLjava/lang/String;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    .locals 1
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "oldValue"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    .param p4, "newValue"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    .prologue
    .line 57
    if-eqz p3, :cond_0

    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->parent:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    # invokes: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->removeFile(Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    invoke-static {v0, p3}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$000(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;->removed:Z

    .line 61
    :cond_0
    return-void
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 42
    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->sizeOf(Ljava/lang/String;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/String;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    .prologue
    .line 52
    iget v0, p2, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;->size:I

    return v0
.end method
