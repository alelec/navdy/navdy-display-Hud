.class public Lcom/navdy/hud/app/storage/cache/DiskLruCache;
.super Ljava/lang/Object;
.source "DiskLruCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;,
        Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    }
.end annotation


# instance fields
.field private appContext:Landroid/content/Context;

.field private cacheName:Ljava/lang/String;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

.field private maxSize:I

.field private rootPath:Ljava/io/File;

.field private taskManager:Lcom/navdy/service/library/task/TaskManager;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "cacheName"    # Ljava/lang/String;
    .param p2, "rootPath"    # Ljava/lang/String;
    .param p3, "maxSize"    # I

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DLC-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;

    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->cacheName:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->rootPath:Ljava/io/File;

    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->rootPath:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 77
    iput p3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->maxSize:I

    .line 78
    new-instance v0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    invoke-direct {v0, p0, p3}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;-><init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    .line 79
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->taskManager:Lcom/navdy/service/library/task/TaskManager;

    .line 80
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->appContext:Landroid/content/Context;

    .line 82
    invoke-direct {p0}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->init()V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    .param p1, "x1"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->removeFile(Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method private addFile(Ljava/lang/String;[B)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "val"    # [B

    .prologue
    .line 124
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;-><init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Ljava/lang/String;[B)V

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 139
    return-void
.end method

.method private getFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->rootPath:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 11

    .prologue
    .line 86
    iget-object v8, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "init start"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 88
    iget-object v8, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->rootPath:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 89
    .local v5, "list":[Ljava/io/File;
    if-nez v5, :cond_0

    .line 121
    :goto_0
    return-void

    .line 92
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v0, "cacheFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v5

    if-ge v2, v8, :cond_2

    .line 94
    aget-object v8, v5, v2

    invoke-virtual {v8}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 95
    aget-object v8, v5, v2

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 99
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 100
    .local v6, "n":I
    iget-object v8, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "init: files in cache["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 101
    if-nez v6, :cond_3

    .line 102
    iget-object v8, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "init: cache empty"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_3
    new-instance v8, Lcom/navdy/hud/app/storage/cache/DiskLruCache$1;

    invoke-direct {v8, p0}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$1;-><init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)V

    invoke-static {v0, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 113
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 114
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/io/File;>;"
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 115
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 116
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 117
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-int v4, v8

    .line 118
    .local v4, "length":I
    iget-object v8, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    new-instance v9, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    invoke-direct {v9, v7, v4}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v8, v7, v9}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 120
    .end local v1    # "f":Ljava/io/File;
    .end local v4    # "length":I
    .end local v7    # "name":Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "init complete"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeFile(Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    .locals 3
    .param p1, "entry"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    .prologue
    .line 142
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;-><init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 151
    return-void
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    invoke-virtual {v0}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->evictAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    monitor-exit p0

    return-void

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized contains(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    .local v0, "entry":Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    monitor-exit p0

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 175
    .end local v0    # "entry":Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized get(Ljava/lang/String;)[B
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 180
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 181
    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    invoke-virtual {v3, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    .local v0, "entry":Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    if-nez v0, :cond_0

    .line 191
    :goto_0
    monitor-exit p0

    return-object v2

    .line 186
    :cond_0
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->updateFile(Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :try_start_2
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->readBinaryFile(Ljava/lang/String;)[B
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 189
    :catch_0
    move-exception v1

    .line 190
    .local v1, "ex":Ljava/lang/Throwable;
    :try_start_3
    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 180
    .end local v0    # "entry":Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    .end local v1    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized put(Ljava/lang/String;[B)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "val"    # [B

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    array-length v1, p2

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;-><init>(Ljava/lang/String;I)V

    .line 168
    .local v0, "entry":Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    iget-object v1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    invoke-virtual {v1, p1, v0}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-boolean v1, v0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;->removed:Z

    if-nez v1, :cond_0

    .line 170
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->addFile(Ljava/lang/String;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    :cond_0
    monitor-exit p0

    return-void

    .line 167
    .end local v0    # "entry":Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->lruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$MemoryLruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    monitor-exit p0

    return-void

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public updateFile(Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    .locals 3
    .param p1, "entry"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    .prologue
    .line 154
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;-><init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 164
    return-void
.end method
