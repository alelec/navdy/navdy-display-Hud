.class Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;
.super Ljava/lang/Object;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/storage/cache/DiskLruCache;->addFile(Ljava/lang/String;[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$val:[B


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    iput-object p2, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;->val$name:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;->val$val:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 127
    const/4 v0, 0x0

    .line 129
    .local v0, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    iget-object v4, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;->val$name:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->getFilePath(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$100(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    .end local v0    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v1, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;->val$val:[B

    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 131
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 135
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 137
    .end local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "fileOutputStream":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v2

    .line 133
    .local v2, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$2;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    # getter for: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v3}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$200(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 135
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    :goto_2
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .end local v0    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 132
    .end local v0    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_1
.end method
