.class Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;
.super Ljava/lang/Object;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/storage/cache/DiskLruCache;->removeFile(Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

.field final synthetic val$entry:Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    iput-object p2, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;->val$entry:Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 145
    iget-object v1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    # getter for: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->appContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$300(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;->val$entry:Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    iget-object v3, v3, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;->name:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->getFilePath(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$100(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 146
    .local v0, "ret":Z
    if-nez v0, :cond_0

    .line 147
    iget-object v1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    # getter for: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$200(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file not deleted ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$3;->val$entry:Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    iget-object v3, v3, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 149
    :cond_0
    return-void
.end method
