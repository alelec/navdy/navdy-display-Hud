.class public final Lcom/navdy/hud/app/storage/cache/MessageCache;
.super Ljava/lang/Object;
.source "MessageCache.kt"

# interfaces
.implements Lcom/navdy/hud/app/storage/cache/Cache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/wire/Message;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/navdy/hud/app/storage/cache/Cache",
        "<",
        "Ljava/lang/String;",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B\'\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004H\u0016J\u0017\u0010\u0015\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0014\u001a\u00020\u0004H\u0016\u00a2\u0006\u0002\u0010\u0016J\u001d\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0004H\u0016R\u001d\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/navdy/hud/app/storage/cache/MessageCache;",
        "T",
        "Lcom/squareup/wire/Message;",
        "Lcom/navdy/hud/app/storage/cache/Cache;",
        "",
        "dataCache",
        "",
        "type",
        "Ljava/lang/Class;",
        "(Lcom/navdy/hud/app/storage/cache/Cache;Ljava/lang/Class;)V",
        "getDataCache",
        "()Lcom/navdy/hud/app/storage/cache/Cache;",
        "getType",
        "()Ljava/lang/Class;",
        "wire",
        "Lcom/squareup/wire/Wire;",
        "clear",
        "",
        "contains",
        "",
        "key",
        "get",
        "(Ljava/lang/String;)Lcom/squareup/wire/Message;",
        "put",
        "message",
        "(Ljava/lang/String;Lcom/squareup/wire/Message;)V",
        "remove",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final dataCache:Lcom/navdy/hud/app/storage/cache/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/navdy/hud/app/storage/cache/Cache",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final type:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final wire:Lcom/squareup/wire/Wire;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/storage/cache/Cache;Ljava/lang/Class;)V
    .locals 4
    .param p1, "dataCache"    # Lcom/navdy/hud/app/storage/cache/Cache;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "type"    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/storage/cache/Cache",
            "<",
            "Ljava/lang/String;",
            "[B>;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const-string v0, "dataCache"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->dataCache:Lcom/navdy/hud/app/storage/cache/Cache;

    iput-object p2, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->type:Ljava/lang/Class;

    .line 14
    new-instance v0, Lcom/squareup/wire/Wire;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/squareup/wire/Wire;-><init>([Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->wire:Lcom/squareup/wire/Wire;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->dataCache:Lcom/navdy/hud/app/storage/cache/Cache;

    invoke-interface {v0}, Lcom/navdy/hud/app/storage/cache/Cache;->clear()V

    .line 41
    return-void
.end method

.method public bridge synthetic contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 12
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/storage/cache/MessageCache;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->dataCache:Lcom/navdy/hud/app/storage/cache/Cache;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/storage/cache/Cache;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/String;)Lcom/squareup/wire/Message;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-string v3, "key"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->dataCache:Lcom/navdy/hud/app/storage/cache/Cache;

    invoke-interface {v3, p1}, Lcom/navdy/hud/app/storage/cache/Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .local v0, "messageData":[B
    move-object v1, v2

    .line 17
    check-cast v1, Lcom/squareup/wire/Message;

    .line 18
    .local v1, "result":Lcom/squareup/wire/Message;
    if-eqz v0, :cond_0

    .line 19
    iget-object v2, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->wire:Lcom/squareup/wire/Wire;

    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->type:Ljava/lang/Class;

    invoke-virtual {v2, v0, v3}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v1

    .line 22
    invoke-static {v1}, Lcom/navdy/service/library/events/MessageStore;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v2

    :cond_0
    return-object v2
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/storage/cache/MessageCache;->get(Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v0

    return-object v0
.end method

.method public final getDataCache()Lcom/navdy/hud/app/storage/cache/Cache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/navdy/hud/app/storage/cache/Cache",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->dataCache:Lcom/navdy/hud/app/storage/cache/Cache;

    return-object v0
.end method

.method public final getType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 12
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->type:Ljava/lang/Class;

    return-object v0
.end method

.method public bridge synthetic put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/storage/cache/MessageCache;->put(Ljava/lang/String;Lcom/squareup/wire/Message;)V

    return-void
.end method

.method public put(Ljava/lang/String;Lcom/squareup/wire/Message;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "message"    # Lcom/squareup/wire/Message;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    const-string v1, "key"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "message"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p2}, Lcom/squareup/wire/Message;->toByteArray()[B

    move-result-object v0

    .line 28
    .local v0, "bytes":[B
    iget-object v1, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->dataCache:Lcom/navdy/hud/app/storage/cache/Cache;

    const-string v2, "bytes"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, p1, v0}, Lcom/navdy/hud/app/storage/cache/Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/storage/cache/MessageCache;->remove(Ljava/lang/String;)V

    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/MessageCache;->dataCache:Lcom/navdy/hud/app/storage/cache/Cache;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/storage/cache/Cache;->remove(Ljava/lang/Object;)V

    .line 33
    return-void
.end method
