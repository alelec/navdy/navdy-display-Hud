.class Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;
.super Ljava/lang/Object;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/storage/cache/DiskLruCache;->updateFile(Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

.field final synthetic val$entry:Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    iput-object p2, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;->val$entry:Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 157
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    iget-object v3, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;->val$entry:Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;

    iget-object v3, v3, Lcom/navdy/hud/app/storage/cache/DiskLruCache$FileEntry;->name:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->getFilePath(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$100(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 158
    .local v0, "f":Ljava/io/File;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    move-result v1

    .line 159
    .local v1, "ret":Z
    if-nez v1, :cond_0

    .line 160
    iget-object v2, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCache$4;->this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    # getter for: Lcom/navdy/hud/app/storage/cache/DiskLruCache;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->access$200(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file time not changed ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 162
    :cond_0
    return-void
.end method
