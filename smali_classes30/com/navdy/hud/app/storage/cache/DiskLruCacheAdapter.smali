.class public final Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;
.super Ljava/lang/Object;
.source "DiskLruCacheAdapter.kt"

# interfaces
.implements Lcom/navdy/hud/app/storage/cache/Cache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/navdy/hud/app/storage/cache/Cache",
        "<",
        "Ljava/lang/String;",
        "[B>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0012\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016J\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0011\u001a\u00020\u0002H\u0096\u0002J\u0018\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0003H\u0016J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;",
        "Lcom/navdy/hud/app/storage/cache/Cache;",
        "",
        "",
        "cacheName",
        "directory",
        "maxSize",
        "",
        "(Ljava/lang/String;Ljava/lang/String;I)V",
        "diskLruCache",
        "Lcom/navdy/hud/app/storage/cache/DiskLruCache;",
        "getDiskLruCache",
        "()Lcom/navdy/hud/app/storage/cache/DiskLruCache;",
        "clear",
        "",
        "contains",
        "",
        "key",
        "get",
        "put",
        "data",
        "remove",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "cacheName"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "directory"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "maxSize"    # I

    .prologue
    const-string v0, "cacheName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "directory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    invoke-direct {v0, p1, p2, p3}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    invoke-virtual {v0}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->clear()V

    .line 26
    return-void
.end method

.method public bridge synthetic contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->get(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;)[B
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->get(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getDiskLruCache()Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 10
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    return-object v0
.end method

.method public bridge synthetic put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Ljava/lang/String;

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->put(Ljava/lang/String;[B)V

    return-void
.end method

.method public put(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "data"    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->put(Ljava/lang/String;[B)V

    .line 18
    return-void
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->remove(Ljava/lang/String;)V

    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->remove(Ljava/lang/String;)V

    .line 22
    return-void
.end method
