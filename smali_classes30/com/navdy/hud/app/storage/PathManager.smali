.class public Lcom/navdy/hud/app/storage/PathManager;
.super Ljava/lang/Object;
.source "PathManager.java"

# interfaces
.implements Lcom/navdy/service/library/file/IFileTransferAuthority;


# static fields
.field private static final ACTIVE_ROUTE_DIR:Ljava/lang/String; = ".activeroute"

.field private static final CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR:Ljava/lang/String; = "car_md_cache"

.field private static CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String; = null

.field private static final DATA_LOGS_DIR:Ljava/lang/String; = "DataLogs"

.field private static final DRIVER_PROFILES_DIR:Ljava/lang/String; = "DriverProfiles"

.field private static final GESTURE_VIDEOS_SYNC_FOLDER:Ljava/lang/String; = "gesture_videos"

.field private static final HERE_MAPS_CONFIG_BASE_PATH:Ljava/lang/String; = "/.here-maps"

.field private static final HERE_MAPS_CONFIG_DIRS_PATTERN:Ljava/util/regex/Pattern;

.field private static final HERE_MAPS_CONFIG_DIRS_REGEX:Ljava/lang/String; = "[0-9]{10}"

.field private static final HERE_MAP_DATA_DIR:Ljava/lang/String; = ".here-maps"

.field public static final HERE_MAP_META_JSON_FILE:Ljava/lang/String; = "meta.json"

.field private static final HUD_DATABASE_DIR:Ljava/lang/String; = "/.db"

.field private static final IMAGE_DISK_CACHE_FILES_DIR:Ljava/lang/String; = "img_disk_cache"

.field private static final KERNEL_CRASH_CONSOLE_RAMOOPS:Ljava/lang/String; = "/sys/fs/pstore/console-ramoops"

.field private static final KERNEL_CRASH_CONSOLE_RAMOOPS_0:Ljava/lang/String; = "/sys/fs/pstore/console-ramoops-0"

.field private static final KERNEL_CRASH_DMESG_RAMOOPS:Ljava/lang/String; = "/sys/fs/pstore/dmesg-ramoops-0"

.field private static final KERNEL_CRASH_FILES:[Ljava/lang/String;

.field private static final LOGS_FOLDER:Ljava/lang/String; = "templogs"

.field private static final MUSIC_LIBRARY_DISK_CACHE_FILES_DIR:Ljava/lang/String; = "music_disk_cache"

.field private static final NAVIGATION_FILES_DIR:Ljava/lang/String; = "navigation_issues"

.field private static final NON_FATAL_CRASH_REPORT_DIR:Ljava/lang/String; = "/sdcard/.logs/snapshot/"

.field private static final SYSTEM_CACHE_DIR:Ljava/lang/String; = "/cache"

.field private static final TEMP_FILE_TIMESTAMP_FORMAT:Ljava/lang/String; = "\'display_log\'_yyyy_MM_dd-HH_mm_ss\'.zip\'"

.field private static final TIMESTAMP_MWCONFIG_LATEST:Ljava/lang/String;

.field private static format:Ljava/text/SimpleDateFormat;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sSingleton:Lcom/navdy/hud/app/storage/PathManager;


# instance fields
.field private activeRouteInfoDir:Ljava/lang/String;

.field private carMdResponseDiskCacheFolder:Ljava/lang/String;

.field private databaseDir:Ljava/lang/String;

.field private driverProfilesDir:Ljava/lang/String;

.field private gestureVideosSyncFolder:Ljava/lang/String;

.field private hereMapsConfigDirs:Ljava/io/File;

.field private hereMapsDataDirectory:Ljava/lang/String;

.field private hereVoiceSkinsPath:Ljava/lang/String;

.field private imageDiskCacheFolder:Ljava/lang/String;

.field private volatile initialized:Z

.field private final mapsPartitionPath:Ljava/lang/String;

.field private musicDiskCacheFolder:Ljava/lang/String;

.field private navigationIssuesFolder:Ljava/lang/String;

.field private tempLogsFolder:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/storage/PathManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/storage/PathManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 38
    const-string v0, "info.txt"

    sput-object v0, Lcom/navdy/hud/app/storage/PathManager;->CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String;

    .line 41
    const-string v0, "[0-9]{10}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/storage/PathManager;->HERE_MAPS_CONFIG_DIRS_PATTERN:Ljava/util/regex/Pattern;

    .line 44
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->getCurrentHereSdkTimestamp()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/storage/PathManager;->TIMESTAMP_MWCONFIG_LATEST:Ljava/lang/String;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/sys/fs/pstore/console-ramoops"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/sys/fs/pstore/console-ramoops-0"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "/sys/fs/pstore/dmesg-ramoops-0"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/storage/PathManager;->KERNEL_CRASH_FILES:[Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "\'display_log\'_yyyy_MM_dd-HH_mm_ss\'.zip\'"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/storage/PathManager;->format:Ljava/text/SimpleDateFormat;

    .line 69
    new-instance v0, Lcom/navdy/hud/app/storage/PathManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/storage/PathManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/storage/PathManager;->sSingleton:Lcom/navdy/hud/app/storage/PathManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "externalPath":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "internalAppPath":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "cacheDirectory":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "templogs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->tempLogsFolder:Ljava/lang/String;

    .line 106
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DriverProfiles"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->driverProfilesDir:Ljava/lang/String;

    .line 107
    const-string v4, "ro.maps_partition"

    invoke-static {v4, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->mapsPartitionPath:Ljava/lang/String;

    .line 108
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/navdy/hud/app/storage/PathManager;->mapsPartitionPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/.here-maps"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->hereMapsConfigDirs:Ljava/io/File;

    .line 109
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/.db"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->databaseDir:Ljava/lang/String;

    .line 110
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".activeroute"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->activeRouteInfoDir:Ljava/lang/String;

    .line 111
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "navigation_issues"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->navigationIssuesFolder:Ljava/lang/String;

    .line 112
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "car_md_cache"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->carMdResponseDiskCacheFolder:Ljava/lang/String;

    .line 113
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "img_disk_cache"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->imageDiskCacheFolder:Ljava/lang/String;

    .line 114
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "music_disk_cache"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->musicDiskCacheFolder:Ljava/lang/String;

    .line 115
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/storage/PathManager;->mapsPartitionPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "gesture_videos"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->gestureVideosSyncFolder:Ljava/lang/String;

    .line 118
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/storage/PathManager;->mapsPartitionPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".here-maps/voices-download"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v3, "voiceSkinsFilepath":Ljava/io/File;
    sget-object v4, Lcom/navdy/hud/app/storage/PathManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "voiceSkins path="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->hereVoiceSkinsPath:Ljava/lang/String;

    .line 123
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/storage/PathManager;->mapsPartitionPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".here-maps"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/storage/PathManager;->hereMapsDataDirectory:Ljava/lang/String;

    .line 124
    return-void
.end method

.method static synthetic access$000()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/navdy/hud/app/storage/PathManager;->HERE_MAPS_CONFIG_DIRS_PATTERN:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method private collectGpsLog(Ljava/lang/String;)V
    .locals 5
    .param p1, "stagingPath"    # Ljava/lang/String;

    .prologue
    .line 311
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "GPS_COLLECT_LOGS"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 312
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "logPath"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 314
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 315
    .local v1, "handle":Landroid/os/UserHandle;
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 317
    const/16 v4, 0xbb8

    invoke-static {v4}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 318
    :catch_0
    move-exception v3

    .line 319
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/storage/PathManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static declared-synchronized generateTempFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 271
    const-class v1, Lcom/navdy/hud/app/storage/PathManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/storage/PathManager;->format:Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/storage/PathManager;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/navdy/hud/app/storage/PathManager;->sSingleton:Lcom/navdy/hud/app/storage/PathManager;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    .line 130
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->tempLogsFolder:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 131
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->databaseDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 132
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->driverProfilesDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 133
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->activeRouteInfoDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 134
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->navigationIssuesFolder:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 135
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->carMdResponseDiskCacheFolder:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 136
    new-instance v0, Ljava/io/File;

    const-string v1, "/sdcard/.logs/snapshot/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 137
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->imageDiskCacheFolder:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 138
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->musicDiskCacheFolder:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 139
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->hereVoiceSkinsPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 140
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->gestureVideosSyncFolder:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public collectEnvironmentInfo(Ljava/lang/String;)V
    .locals 7
    .param p1, "stagingPath"    # Ljava/lang/String;

    .prologue
    .line 324
    const/4 v0, 0x0

    .line 326
    .local v0, "fileWriter":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/app/storage/PathManager;->CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 327
    .local v2, "infoTextFile":Ljava/io/File;
    new-instance v1, Ljava/io/FileWriter;

    invoke-direct {v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    .end local v0    # "fileWriter":Ljava/io/FileWriter;
    .local v1, "fileWriter":Ljava/io/FileWriter;
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->readProps()Ljava/lang/String;

    move-result-object v3

    .line 329
    .local v3, "props":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 333
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 335
    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    .end local v2    # "infoTextFile":Ljava/io/File;
    .end local v3    # "props":Ljava/lang/String;
    .restart local v0    # "fileWriter":Ljava/io/FileWriter;
    :goto_0
    return-void

    .line 330
    :catch_0
    move-exception v4

    .line 331
    .local v4, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    sget-object v5, Lcom/navdy/hud/app/storage/PathManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 333
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5

    .end local v0    # "fileWriter":Ljava/io/FileWriter;
    .restart local v1    # "fileWriter":Ljava/io/FileWriter;
    .restart local v2    # "infoTextFile":Ljava/io/File;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    .restart local v0    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_2

    .line 330
    .end local v0    # "fileWriter":Ljava/io/FileWriter;
    .restart local v1    # "fileWriter":Ljava/io/FileWriter;
    :catch_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "fileWriter":Ljava/io/FileWriter;
    .restart local v0    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method public getActiveRouteInfoDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->activeRouteInfoDir:Ljava/lang/String;

    return-object v0
.end method

.method public getCarMdResponseDiskCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->carMdResponseDiskCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getDatabaseDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->databaseDir:Ljava/lang/String;

    return-object v0
.end method

.method public getDirectoryForFileType(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;
    .locals 2
    .param p1, "fileType"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 217
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/storage/PathManager$2;->$SwitchMap$com$navdy$service$library$events$file$FileType:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/file/FileType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 223
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 219
    :pswitch_0
    const-string v0, "/cache"

    goto :goto_0

    .line 221
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->tempLogsFolder:Ljava/lang/String;

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getDriverProfilesDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->driverProfilesDir:Ljava/lang/String;

    return-object v0
.end method

.method public getFileToSend(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;
    .locals 14
    .param p1, "fileType"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 229
    sget-object v10, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    if-ne p1, v10, :cond_2

    .line 230
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/storage/PathManager;->getDirectoryForFileType(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;

    move-result-object v9

    .line 231
    .local v9, "tempLogFolder":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "stage"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 232
    .local v6, "stagingPath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 233
    .local v5, "stagingDirectory":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v5}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 234
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 236
    :try_start_0
    invoke-static {v6}, Lcom/navdy/service/library/util/LogUtils;->copyComprehensiveSystemLogs(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0, v6}, Lcom/navdy/hud/app/storage/PathManager;->collectEnvironmentInfo(Ljava/lang/String;)V

    .line 238
    invoke-direct {p0, v6}, Lcom/navdy/hud/app/storage/PathManager;->collectGpsLog(Ljava/lang/String;)V

    .line 239
    invoke-static {v6}, Lcom/navdy/hud/app/util/DeviceUtil;->copyHEREMapsDataInfo(Ljava/lang/String;)V

    .line 240
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "HUDScreenShot.png"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/navdy/hud/app/util/DeviceUtil;->takeDeviceScreenShot(Ljava/lang/String;)V

    .line 241
    const/4 v10, 0x1

    invoke-static {v10}, Lcom/navdy/hud/app/util/ReportIssueService;->getLatestDriveLogFiles(I)Ljava/util/List;

    move-result-object v2

    .line 242
    .local v2, "driveLogFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_1

    .line 244
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 245
    .local v1, "driveLogFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 256
    .end local v1    # "driveLogFile":Ljava/io/File;
    .end local v2    # "driveLogFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    :catch_0
    move-exception v7

    .line 257
    .local v7, "t":Ljava/lang/Throwable;
    const/4 v0, 0x0

    .line 260
    .end local v5    # "stagingDirectory":Ljava/io/File;
    .end local v6    # "stagingPath":Ljava/lang/String;
    .end local v7    # "t":Ljava/lang/Throwable;
    .end local v9    # "tempLogFolder":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v0

    .line 248
    .restart local v2    # "driveLogFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .restart local v5    # "stagingDirectory":Ljava/io/File;
    .restart local v6    # "stagingPath":Ljava/lang/String;
    .restart local v9    # "tempLogFolder":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 249
    .local v4, "logFiles":[Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->generateTempFileName()Ljava/lang/String;

    move-result-object v8

    .line 250
    .local v8, "tempFilName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 251
    .local v0, "absolutePath":Ljava/lang/String;
    invoke-static {v4, v0}, Lcom/navdy/hud/app/util/CrashReportService;->compressCrashReportsToZip([Ljava/io/File;Ljava/lang/String;)V

    .line 252
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .local v3, "fileToSend":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    if-nez v10, :cond_0

    .line 260
    .end local v0    # "absolutePath":Ljava/lang/String;
    .end local v2    # "driveLogFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v3    # "fileToSend":Ljava/io/File;
    .end local v4    # "logFiles":[Ljava/io/File;
    .end local v5    # "stagingDirectory":Ljava/io/File;
    .end local v6    # "stagingPath":Ljava/lang/String;
    .end local v8    # "tempFilName":Ljava/lang/String;
    .end local v9    # "tempLogFolder":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getGestureVideosSyncFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->gestureVideosSyncFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getHereMapsConfigDirs()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-boolean v3, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 146
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v2, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/storage/PathManager;->hereMapsConfigDirs:Ljava/io/File;

    new-instance v4, Lcom/navdy/hud/app/storage/PathManager$1;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/storage/PathManager$1;-><init>(Lcom/navdy/hud/app/storage/PathManager;)V

    invoke-virtual {v3, v4}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    .line 156
    .local v1, "hereDirs":[Ljava/io/File;
    if-eqz v1, :cond_1

    .line 157
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 158
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 162
    .end local v0    # "f":Ljava/io/File;
    :cond_1
    return-object v2
.end method

.method public getHereMapsDataDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->hereMapsDataDirectory:Ljava/lang/String;

    return-object v0
.end method

.method public getHereVoiceSkinsPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->hereVoiceSkinsPath:Ljava/lang/String;

    return-object v0
.end method

.method public getImageDiskCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->imageDiskCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getKernelCrashFiles()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 177
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/storage/PathManager;->KERNEL_CRASH_FILES:[Ljava/lang/String;

    return-object v0
.end method

.method public getLatestHereMapsConfigPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 167
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/hud/app/storage/PathManager;->hereMapsConfigDirs:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/storage/PathManager;->TIMESTAMP_MWCONFIG_LATEST:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMapsPartitionPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->mapsPartitionPath:Ljava/lang/String;

    return-object v0
.end method

.method public getMusicDiskCacheFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->musicDiskCacheFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getNavigationIssuesDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/storage/PathManager;->navigationIssuesFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getNonFatalCrashReportDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/navdy/hud/app/storage/PathManager;->initialized:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/storage/PathManager;->init()V

    .line 296
    :cond_0
    const-string v0, "/sdcard/.logs/snapshot/"

    return-object v0
.end method

.method public isFileTypeAllowed(Lcom/navdy/service/library/events/file/FileType;)Z
    .locals 2
    .param p1, "fileType"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 204
    sget-object v0, Lcom/navdy/hud/app/storage/PathManager$2;->$SwitchMap$com$navdy$service$library$events$file$FileType:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/file/FileType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 210
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 208
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onFileSent(Lcom/navdy/service/library/events/file/FileType;)V
    .locals 1
    .param p1, "fileType"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 265
    sget-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    if-ne p1, v0, :cond_0

    .line 266
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReportService;->clearCrashReports()V

    .line 268
    :cond_0
    return-void
.end method
