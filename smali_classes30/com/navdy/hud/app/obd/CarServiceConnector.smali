.class public Lcom/navdy/hud/app/obd/CarServiceConnector;
.super Lcom/navdy/hud/app/common/ServiceReconnector;
.source "CarServiceConnector.java"


# instance fields
.field private carApi:Lcom/navdy/obd/ICarService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.navdy.obd.action.START_AUTO_CONNECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/navdy/obd/ICarService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/hud/app/common/ServiceReconnector;-><init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method public getCarApi()Lcom/navdy/obd/ICarService;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarServiceConnector;->carApi:Lcom/navdy/obd/ICarService;

    return-object v0
.end method

.method protected onConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 24
    invoke-static {p2}, Lcom/navdy/obd/ICarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/obd/ICarService;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/CarServiceConnector;->carApi:Lcom/navdy/obd/ICarService;

    .line 25
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->serviceConnected()V

    .line 26
    return-void
.end method

.method protected onDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/CarServiceConnector;->carApi:Lcom/navdy/obd/ICarService;

    .line 31
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->serviceDisconnected()V

    .line 32
    return-void
.end method
