.class Lcom/navdy/hud/app/obd/ObdManager$7;
.super Lcom/navdy/obd/ICanBusMonitoringListener$Stub;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/obd/ObdManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/ObdManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 609
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {p0}, Lcom/navdy/obd/ICanBusMonitoringListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getGpsSpeed()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 630
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$1900(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/SpeedManager;->getGpsSpeedInMetersPerSecond()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public getLatitude()D
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 635
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 636
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 637
    .local v0, "c":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v0, :cond_0

    .line 638
    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v2

    .line 641
    .end local v0    # "c":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_0
    return-wide v2

    :cond_0
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public getLongitude()D
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 646
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 647
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 648
    .local v0, "c":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v2

    .line 652
    .end local v0    # "c":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_0
    return-wide v2

    :cond_0
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public getMake()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 662
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v1, v1, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    if-eqz v1, :cond_0

    .line 663
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v1, v1, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 664
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v0, :cond_0

    .line 665
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v1

    .line 668
    .end local v0    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getModel()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 673
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v1, v1, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    if-eqz v1, :cond_0

    .line 674
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v1, v1, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 675
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v0, :cond_0

    .line 676
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v1

    .line 679
    .end local v0    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getYear()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 684
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v1, v1, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    if-eqz v1, :cond_0

    .line 685
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v1, v1, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 686
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v0, :cond_0

    .line 687
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v1

    .line 690
    .end local v0    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public isMonitoringLimitReached()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 657
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$2300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringLimitReached()Z

    move-result v0

    return v0
.end method

.method public onCanBusMonitorSuccess(I)V
    .locals 3
    .param p1, "averageAmountOfData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 624
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCanBusMonitorSuccess , Average amount of data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 625
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$2300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->onCanBusMonitorSuccess(I)V

    .line 626
    return-void
.end method

.method public onCanBusMonitoringError(Ljava/lang/String;)V
    .locals 3
    .param p1, "errorMessage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 618
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCanBusMonitoringError, Error : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$2300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->onCanBusMonitoringFailed()V

    .line 620
    return-void
.end method

.method public onNewDataAvailable(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 612
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNewDataAvailable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$7;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$2300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->onNewDataAvailable(Ljava/lang/String;)V

    .line 614
    return-void
.end method
