.class Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;
.super Ljava/lang/Object;
.source "CarMDVinDecoder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/obd/CarMDVinDecoder;->decodeVin(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

.field final synthetic val$vinNumber:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    iput-object p2, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->val$vinNumber:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 80
    :try_start_0
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    iget-object v8, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->val$vinNumber:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->getFromCache(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$000(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "data":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 83
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Entry found in the cache for the vinNumber :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->val$vinNumber:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Data :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 84
    invoke-static {v1}, Lcom/navdy/hud/app/obd/CarDetails;->fromJson(Ljava/lang/String;)Lcom/navdy/hud/app/obd/CarDetails;

    move-result-object v2

    .line 85
    .local v2, "details":Lcom/navdy/hud/app/obd/CarDetails;
    if-eqz v2, :cond_0

    .line 86
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$200(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setDecodedCarDetails(Lcom/navdy/hud/app/obd/CarDetails;)V

    .line 87
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z

    .line 134
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "details":Lcom/navdy/hud/app/obd/CarDetails;
    :goto_0
    return-void

    .line 90
    .restart local v1    # "data":Ljava/lang/String;
    .restart local v2    # "details":Lcom/navdy/hud/app/obd/CarDetails;
    :cond_0
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "Error parsing the CarMD response to CarDetails"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 93
    .end local v2    # "details":Lcom/navdy/hud/app/obd/CarDetails;
    :cond_1
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # invokes: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->isConnected()Z
    invoke-static {v7}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$400(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 94
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 130
    .end local v1    # "data":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 131
    .local v3, "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "Exception while fetching the vin from CarMD"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 132
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # setter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z
    invoke-static {v7, v10}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z

    goto :goto_0

    .line 97
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "data":Ljava/lang/String;
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_API_URL:Lokhttp3/HttpUrl;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$500()Lokhttp3/HttpUrl;

    move-result-object v7

    invoke-virtual {v7}, Lokhttp3/HttpUrl;->newBuilder()Lokhttp3/HttpUrl$Builder;

    move-result-object v0

    .line 98
    .local v0, "builder":Lokhttp3/HttpUrl$Builder;
    const-string v7, "vin"

    iget-object v8, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->val$vinNumber:Ljava/lang/String;

    invoke-virtual {v0, v7, v8}, Lokhttp3/HttpUrl$Builder;->addQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    .line 99
    invoke-virtual {v0}, Lokhttp3/HttpUrl$Builder;->build()Lokhttp3/HttpUrl;

    move-result-object v6

    .line 100
    .local v6, "requestUrl":Lokhttp3/HttpUrl;
    new-instance v7, Lokhttp3/Request$Builder;

    invoke-direct {v7}, Lokhttp3/Request$Builder;-><init>()V

    invoke-virtual {v7, v6}, Lokhttp3/Request$Builder;->url(Lokhttp3/HttpUrl;)Lokhttp3/Request$Builder;

    move-result-object v5

    .line 101
    .local v5, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_AUTH:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$600(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 102
    const-string v7, "authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Basic "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_AUTH:Ljava/lang/String;
    invoke-static {v9}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$600(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 108
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_TOKEN:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$700(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 109
    const-string v7, "partner-token"

    iget-object v8, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_TOKEN:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$700(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 115
    invoke-virtual {v5}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v4

    .line 116
    .local v4, "request":Lokhttp3/Request;
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mHttpClient:Lokhttp3/OkHttpClient;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$900(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Lokhttp3/OkHttpClient;

    move-result-object v7

    invoke-virtual {v7, v4}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v7

    new-instance v8, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1$1;

    invoke-direct {v8, p0}, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1$1;-><init>(Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;)V

    invoke-interface {v7, v8}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V

    goto/16 :goto_0

    .line 104
    .end local v4    # "request":Lokhttp3/Request;
    :cond_3
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "Missing car md auth credential !"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 105
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z

    goto/16 :goto_0

    .line 111
    :cond_4
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "Missing car md token credential !"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 112
    iget-object v7, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
