.class public final Lcom/navdy/hud/app/obd/ObdManager;
.super Ljava/lang/Object;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/obd/ObdManager$PidCheck;,
        Lcom/navdy/hud/app/obd/ObdManager$ObdPidReadEvent;,
        Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;,
        Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;,
        Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;,
        Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;,
        Lcom/navdy/hud/app/obd/ObdManager$Firmware;
    }
.end annotation


# static fields
.field public static final BATTERY_CHARGING_VOLTAGE:F = 13.1f

.field public static final BATTERY_NO_OBD:D = -1.0

.field private static final BLACKLIST_MODIFICATION_TIME:Ljava/lang/String; = "blacklist_modification_time"

.field public static final CRITICAL_BATTERY_VOLTAGE:D

.field private static final CRITICAL_VOLTAGE_OVERRIDE_PROPERTY:Ljava/lang/String; = "persist.sys.voltage.crit"

.field private static final DEFAULT_CRITICAL_BATTERY_VOLTAGE:F = 12.0f

.field private static final DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION:J

.field private static final DEFAULT_SET:Ljava/lang/String; = "default_set"

.field private static final DISTANCE_UPDATE_INTERVAL:I = 0xea60

.field private static final ENGINE_TEMP_UPDATE_INTERVAL:I = 0x7d0

.field public static final FIRMWARE_VERSION:Ljava/lang/String; = "4.2.1"

.field public static final FIRST_ODOMETER_READING_KM:Ljava/lang/String; = "first_odometer_reading"

.field private static final FLUSH_INTERVAL_MS:I = 0x7530

.field private static final FUEL_UPDATE_INTERVAL:I = 0x3a98

.field private static final GENERIC_UPDATE_INTERVAL:I = 0x3e8

.field public static final LAST_ODOMETER_READING_KM:Ljava/lang/String; = "last_odometer_reading"

.field private static final LOW_BATTERY_COUNT_THRESHOLD:I

.field public static final LOW_BATTERY_VOLTAGE:D

.field public static final LOW_FUEL_LEVEL:I = 0xa

.field private static final LOW_VOLTAGE_DURATION_OVERRIDE_PROPERTY:Ljava/lang/String; = "persist.sys.voltage.duration"

.field private static final LOW_VOLTAGE_OVERRIDE_PROPERTY:Ljava/lang/String; = "persist.sys.voltage.low"

.field private static final LOW_VOLTAGE_SHUTOFF_DURATION:J

.field public static final MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING:J

.field private static final MPG_UPDATE_INTERVAL:I = 0x3e8

.field public static final NOT_AVAILABLE:I = -0x1

.field private static final OBD_CONNECTED:Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

.field private static final OBD_NOT_CONNECTED:Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

.field private static final RPM_UPDATE_INTERVAL:I = 0xfa

.field public static final SAFE_TO_SLEEP_BATTERY_OBSERVED_COUNT_THRESHOLD:I = 0x5

.field private static final SCAN_SETTING:Ljava/lang/String; = "scan_setting"

.field private static final SLOW_RPM_UPDATE_INTERVAL:I = 0x9c4

.field private static final SPEED_UPDATE_INTERVAL:I = 0x64

.field private static final STATE_CONNECTED:I = 0x2

.field private static final STATE_CONNECTING:I = 0x1

.field private static final STATE_DISCONNECTED:I = 0x0

.field private static final STATE_IDLE:I = 0x4

.field private static final STATE_SLEEPING:I = 0x5

.field public static final TELEMETRY_TAG:Ljava/lang/String; = "Telemetry"

.field public static final TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY_METERS:Ljava/lang/String; = "total_distance_travelled_with_navdy"

.field public static final VALID_FUEL_DATA_WAIT_TIME:I = 0xafc8

.field public static final VEHICLE_VIN:Ljava/lang/String; = "vehicle_vin"

.field public static final VOLTAGE_REPORT_INTERVAL:I = 0x7d0

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sTelemetryLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/obd/ObdManager;


# instance fields
.field private batteryVoltage:D

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

.field private carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

.field private checkVoltage:Ljava/lang/Runnable;

.field private connectionType:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

.field private driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

.field driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ecus:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;"
        }
    .end annotation
.end field

.field private firstScan:Z

.field fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

.field private fullScan:Lcom/navdy/obd/ScanSchedule;

.field private handler:Landroid/os/Handler;

.field private isCheckEngineLightOn:Z

.field private lowBatteryCount:I

.field private maxBatteryVoltage:D

.field private mileageEventLastReportTime:J

.field private minBatteryVoltage:D

.field private minimalScan:Lcom/navdy/obd/ScanSchedule;

.field private obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

.field private obdChipFirmwareVersion:Ljava/lang/String;

.field private volatile obdConnected:Z

.field private obdDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

.field private obdPidsCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private odoMeterReadingValidated:Z

.field private periodicCheckRunnable:Ljava/lang/Runnable;

.field private pidListener:Lcom/navdy/obd/IPidListener;

.field powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private protocol:Ljava/lang/String;

.field private recordingPidListener:Lcom/navdy/obd/IPidListener;

.field private safeToSleepBatteryLevelObservedCount:I

.field private schedule:Lcom/navdy/obd/ScanSchedule;

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private supportedPidSet:Lcom/navdy/obd/PidSet;

.field public telemetryDataManager:Lcom/navdy/hud/app/analytics/TelemetryDataManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private totalDistanceInCurrentSessionPersisted:J

.field tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private troubleCodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private vin:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 73
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/obd/ObdManager;->DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION:J

    .line 78
    const-string v0, "persist.sys.voltage.low"

    const v1, 0x41433333    # 12.2f

    .line 79
    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getFloat(Ljava/lang/String;F)F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/navdy/hud/app/obd/ObdManager;->LOW_BATTERY_VOLTAGE:D

    .line 80
    const-string v0, "persist.sys.voltage.crit"

    const/high16 v1, 0x41400000    # 12.0f

    .line 81
    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getFloat(Ljava/lang/String;F)F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/navdy/hud/app/obd/ObdManager;->CRITICAL_BATTERY_VOLTAGE:D

    .line 82
    const-string v0, "persist.sys.voltage.duration"

    sget-wide v2, Lcom/navdy/hud/app/obd/ObdManager;->DEFAULT_LOW_VOLTAGE_SHUTOFF_DURATION:J

    .line 83
    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/obd/ObdManager;->LOW_VOLTAGE_SHUTOFF_DURATION:J

    .line 87
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/obd/ObdManager;->MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING:J

    .line 89
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 90
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "Telemetry"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager;->sTelemetryLogger:Lcom/navdy/service/library/log/Logger;

    .line 91
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager;->OBD_CONNECTED:Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

    .line 92
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager;->OBD_NOT_CONNECTED:Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

    .line 93
    sget-wide v0, Lcom/navdy/hud/app/obd/ObdManager;->LOW_VOLTAGE_SHUTOFF_DURATION:J

    long-to-double v0, v0

    const-wide v2, 0x409f400000000000L    # 2000.0

    div-double/2addr v0, v2

    .line 94
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/obd/ObdManager;->LOW_BATTERY_COUNT_THRESHOLD:I

    .line 161
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/obd/ObdManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager;->singleton:Lcom/navdy/hud/app/obd/ObdManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 10

    .prologue
    const/16 v9, 0x2f

    const/16 v8, 0xc

    const/4 v4, 0x0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    const/16 v5, 0x3e8

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-boolean v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->odoMeterReadingValidated:Z

    .line 105
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->totalDistanceInCurrentSessionPersisted:J

    .line 106
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->mileageEventLastReportTime:J

    .line 123
    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->POWER_ONLY:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->connectionType:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    .line 176
    new-instance v1, Lcom/navdy/obd/PidSet;

    invoke-direct {v1}, Lcom/navdy/obd/PidSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    .line 179
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 184
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;

    .line 190
    iput-wide v6, p0, Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D

    .line 191
    iput-wide v6, p0, Lcom/navdy/hud/app/obd/ObdManager;->minBatteryVoltage:D

    .line 192
    iput-wide v6, p0, Lcom/navdy/hud/app/obd/ObdManager;->maxBatteryVoltage:D

    .line 194
    iput v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I

    .line 195
    iput-boolean v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->isCheckEngineLightOn:Z

    .line 376
    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/obd/ObdManager$4;-><init>(Lcom/navdy/hud/app/obd/ObdManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->periodicCheckRunnable:Ljava/lang/Runnable;

    .line 384
    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/obd/ObdManager$5;-><init>(Lcom/navdy/hud/app/obd/ObdManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->checkVoltage:Ljava/lang/Runnable;

    .line 518
    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/obd/ObdManager$6;-><init>(Lcom/navdy/hud/app/obd/ObdManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->pidListener:Lcom/navdy/obd/IPidListener;

    .line 609
    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$7;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/obd/ObdManager$7;-><init>(Lcom/navdy/hud/app/obd/ObdManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

    .line 238
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 239
    new-instance v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/obd/ObdManager;->sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    invoke-direct {v1, v2, v3, p0, v4}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/obd/ObdManager;Lcom/navdy/hud/app/framework/trips/TripManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    .line 240
    new-instance v1, Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    .line 241
    new-instance v1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;-><init>(Lcom/navdy/hud/app/obd/CarServiceConnector;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .line 242
    new-instance v1, Lcom/navdy/obd/ScanSchedule;

    invoke-direct {v1}, Lcom/navdy/obd/ScanSchedule;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->minimalScan:Lcom/navdy/obd/ScanSchedule;

    .line 243
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->minimalScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0xd

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 244
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->minimalScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x3a98

    invoke-virtual {v1, v9, v2}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 245
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->minimalScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x31

    const v3, 0xea60

    invoke-virtual {v1, v2, v3}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 248
    new-instance v1, Lcom/navdy/obd/ScanSchedule;

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->minimalScan:Lcom/navdy/obd/ScanSchedule;

    invoke-direct {v1, v2}, Lcom/navdy/obd/ScanSchedule;-><init>(Lcom/navdy/obd/ScanSchedule;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    .line 250
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0xfa

    invoke-virtual {v1, v8, v2}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 251
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v5}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 252
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x100

    invoke-virtual {v1, v2, v5}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 253
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    const/4 v2, 0x5

    const/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 255
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x102

    invoke-virtual {v1, v2, v5}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 256
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x103

    invoke-virtual {v1, v2, v5}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 257
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x104

    invoke-virtual {v1, v2, v5}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 261
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->minimalScan:Lcom/navdy/obd/ScanSchedule;

    const/16 v2, 0x9c4

    invoke-virtual {v1, v8, v2}, Lcom/navdy/obd/ScanSchedule;->addPid(II)V

    .line 263
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 264
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 265
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 267
    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager;->sTelemetryLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "SESSION started"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 268
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->handler:Landroid/os/Handler;

    .line 269
    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$1;

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->handler:Landroid/os/Handler;

    invoke-direct {v1, p0, v9, v2}, Lcom/navdy/hud/app/obd/ObdManager$1;-><init>(Lcom/navdy/hud/app/obd/ObdManager;ILandroid/os/Handler;)V

    iput-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    .line 291
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/obd/ObdManager$2;-><init>(Lcom/navdy/hud/app/obd/ObdManager;)V

    .line 304
    .local v0, "periodicFlush":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 305
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/obd/PidSet;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    return-object v0
.end method

.method static synthetic access$1000()I
    .locals 1

    .prologue
    .line 66
    sget v0, Lcom/navdy/hud/app/obd/ObdManager;->LOW_BATTERY_COUNT_THRESHOLD:I

    return v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/obd/ObdManager;Lcom/navdy/obd/PidSet;)Lcom/navdy/obd/PidSet;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->periodicCheckRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/obd/ObdManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/navdy/hud/app/obd/ObdManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/CarServiceConnector;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/obd/ObdManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/obd/ObdManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/ObdManager;->onVinRead(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1602(Lcom/navdy/hud/app/obd/ObdManager;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->ecus:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/navdy/hud/app/obd/ObdManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->protocol:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1800()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/obd/ObdManager;->sTelemetryLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/manager/SpeedManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/obd/ObdManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/obd/ObdManager;J)V
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # J

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/obd/ObdManager;->updateCumulativeVehicleDistanceTravelled(J)V

    return-void
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/obd/IPidListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->recordingPidListener:Lcom/navdy/obd/IPidListener;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/obd/ObdManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/ObdManager;->updateConnectionState(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->checkVoltage:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/obd/ObdManager;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D

    return-wide v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/obd/ObdManager;D)D
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # D

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D

    return-wide p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/obd/ObdManager;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->minBatteryVoltage:D

    return-wide v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/obd/ObdManager;D)D
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # D

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->minBatteryVoltage:D

    return-wide p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/obd/ObdManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/obd/ObdManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I

    return p1
.end method

.method static synthetic access$608(Lcom/navdy/hud/app/obd/ObdManager;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I

    return v0
.end method

.method static synthetic access$700()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/obd/ObdManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I

    return v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/obd/ObdManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I

    return p1
.end method

.method static synthetic access$808(Lcom/navdy/hud/app/obd/ObdManager;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I

    return v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/obd/ObdManager;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->maxBatteryVoltage:D

    return-wide v0
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/obd/ObdManager;D)D
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p1, "x1"    # D

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->maxBatteryVoltage:D

    return-wide p1
.end method

.method public static getInstance()Lcom/navdy/hud/app/obd/ObdManager;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/navdy/hud/app/obd/ObdManager;->singleton:Lcom/navdy/hud/app/obd/ObdManager;

    return-object v0
.end method

.method private getScanSetting(Landroid/content/SharedPreferences;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .locals 4
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 1059
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 1061
    .local v0, "defaultSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :try_start_0
    const-string v2, "scan_setting"

    .line 1062
    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->name()Ljava/lang/String;

    move-result-object v3

    .line 1061
    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1066
    .end local v0    # "defaultSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :goto_0
    return-object v0

    .line 1063
    .restart local v0    # "defaultSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :catch_0
    move-exception v1

    .line 1064
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Failed to parse saved scan setting "

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized onVinRead(Ljava/lang/String;)V
    .locals 5
    .param p1, "vin"    # Ljava/lang/String;

    .prologue
    .line 695
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 696
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "vehicle_vin"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 697
    .local v0, "savedVin":Ljava/lang/String;
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 698
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Vin changed , Old :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", New Vin :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 699
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 700
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "vehicle_vin"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 704
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdManager;->resetCumulativeMileageData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 708
    :goto_1
    monitor-exit p0

    return-void

    .line 702
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "vehicle_vin"

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 695
    .end local v0    # "savedVin":Ljava/lang/String;
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 706
    .restart local v0    # "savedVin":Ljava/lang/String;
    .restart local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_1
    :try_start_2
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Vin has not changed , Vin "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private reportMileageEvent()V
    .locals 24

    .prologue
    .line 721
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 722
    .local v10, "currentTime":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/obd/ObdManager;->mileageEventLastReportTime:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v17, v20, v22

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/obd/ObdManager;->mileageEventLastReportTime:J

    move-wide/from16 v20, v0

    sub-long v20, v10, v20

    sget-wide v22, Lcom/navdy/hud/app/obd/ObdManager;->MINIMUM_TIME_BETWEEN_MILEAGE_REPORTING:J

    cmp-long v17, v20, v22

    if-lez v17, :cond_1

    .line 724
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v16

    .line 725
    .local v16, "preferences":Landroid/content/SharedPreferences;
    const-string v17, "first_odometer_reading"

    const-wide/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-wide/from16 v2, v20

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 726
    .local v12, "firstOdoMeterReadingKms":J
    const-string v17, "last_odometer_reading"

    const-wide/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-wide/from16 v2, v20

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    .line 727
    .local v14, "lastOdoMeterReadingKms":J
    sub-long v20, v14, v12

    move-wide/from16 v0, v20

    long-to-double v4, v0

    .line 728
    .local v4, "totalDistanceVehicleTravelledInKms":D
    sget-object v17, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Initial reading (KM) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", Current reading (KM) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " , Total (KM) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 730
    const-string v17, "total_distance_travelled_with_navdy"

    const-wide/16 v20, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-wide/from16 v2, v20

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v18, v0

    .line 731
    .local v18, "totalDistanceTravelledWithNavdyInMeters":D
    sget-object v17, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Distance travelled with Navdy(Meters) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 733
    const-wide v20, 0x408f400000000000L    # 1000.0

    div-double v6, v18, v20

    .line 735
    .local v6, "totalDistanceTravelledWithNavdyKms":D
    const-wide/16 v20, 0x0

    cmpl-double v17, v4, v20

    if-lez v17, :cond_1

    const-wide/16 v20, 0x0

    cmpl-double v17, v6, v20

    if-lez v17, :cond_1

    .line 736
    div-double v8, v4, v4

    .line 737
    .local v8, "usageRate":D
    sget-object v17, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Reporting the Navdy Mileage Vehicle (KMs) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " , Navdy (KMs) : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " , Navdy Usage rate "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 738
    invoke-static/range {v4 .. v9}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNavdyMileageEvent(DDD)V

    .line 739
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/navdy/hud/app/obd/ObdManager;->mileageEventLastReportTime:J

    .line 742
    .end local v4    # "totalDistanceVehicleTravelledInKms":D
    .end local v6    # "totalDistanceTravelledWithNavdyKms":D
    .end local v8    # "usageRate":D
    .end local v12    # "firstOdoMeterReadingKms":J
    .end local v14    # "lastOdoMeterReadingKms":J
    .end local v16    # "preferences":Landroid/content/SharedPreferences;
    .end local v18    # "totalDistanceTravelledWithNavdyInMeters":D
    :cond_1
    return-void
.end method

.method private resetCumulativeMileageData()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 711
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 712
    .local v0, "preferences":Landroid/content/SharedPreferences;
    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Resetting the cumulative mileage statistics"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 713
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "first_odometer_reading"

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_odometer_reading"

    .line 714
    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "total_distance_travelled_with_navdy"

    .line 715
    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 716
    iput-wide v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->totalDistanceInCurrentSessionPersisted:J

    .line 717
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->odoMeterReadingValidated:Z

    .line 718
    return-void
.end method

.method private setConnected(Z)V
    .locals 6
    .param p1, "connected"    # Z

    .prologue
    const/4 v3, 0x0

    .line 462
    iget-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdConnected:Z

    if-eq v2, p1, :cond_1

    .line 463
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    if-eqz v2, :cond_0

    .line 464
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/debug/DriveRecorder;->setRealObdConnected(Z)V

    .line 466
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->reset()V

    .line 467
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setConnectionState(Z)V

    .line 468
    iput-boolean p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdConnected:Z

    .line 469
    if-eqz p1, :cond_2

    .line 471
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 472
    .local v0, "api":Lcom/navdy/obd/ICarService;
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getVIN()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;

    .line 473
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/obd/ObdManager;->onVinRead(Ljava/lang/String;)V

    .line 474
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getEcus()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->ecus:Ljava/util/List;

    .line 475
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getProtocol()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->protocol:Ljava/lang/String;

    .line 476
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->isCheckEngineLightOn()Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->isCheckEngineLightOn:Z

    .line 477
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getTroubleCodes()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->troubleCodes:Ljava/util/List;

    .line 478
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getObdChipFirmwareVersion()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdChipFirmwareVersion:Ljava/lang/String;

    .line 479
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Connected, Obd chip firmware version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdChipFirmwareVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    if-eqz p1, :cond_4

    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->OBD_CONNECTED:Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

    :goto_1
    invoke-virtual {v3, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 495
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->onObdConnectionStateChanged(Z)V

    .line 497
    :cond_1
    return-void

    .line 480
    :catch_0
    move-exception v1

    .line 481
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Failed to read vin/ecu/protocol after connecting"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 484
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z

    .line 485
    iput-object v3, p0, Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;

    .line 486
    iput-object v3, p0, Lcom/navdy/hud/app/obd/ObdManager;->ecus:Ljava/util/List;

    .line 487
    iput-object v3, p0, Lcom/navdy/hud/app/obd/ObdManager;->protocol:Ljava/lang/String;

    .line 488
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    const/4 v3, -0x1

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/navdy/hud/app/manager/SpeedManager;->setObdSpeed(IJ)V

    .line 489
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    if-eqz v2, :cond_3

    .line 490
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    invoke-virtual {v2}, Lcom/navdy/obd/PidSet;->clear()V

    .line 492
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    goto :goto_0

    .line 494
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->OBD_NOT_CONNECTED:Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;

    goto :goto_1
.end method

.method private setScanSchedule(Lcom/navdy/obd/ScanSchedule;)V
    .locals 1
    .param p1, "schedule"    # Lcom/navdy/obd/ScanSchedule;

    .prologue
    .line 792
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->schedule:Lcom/navdy/obd/ScanSchedule;

    if-eq p1, v0, :cond_0

    .line 793
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->schedule:Lcom/navdy/obd/ScanSchedule;

    .line 794
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager;->updateListener()V

    .line 796
    :cond_0
    return-void
.end method

.method private updateConnectionState(I)V
    .locals 3
    .param p1, "newState"    # I

    .prologue
    const/4 v2, 0x1

    .line 781
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 782
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/obd/ObdManager;->setConnected(Z)V

    .line 786
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->connectionType:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->POWER_ONLY:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    if-ne v0, v1, :cond_0

    if-le p1, v2, :cond_0

    .line 787
    sget-object v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->OBD:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->connectionType:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    .line 789
    :cond_0
    return-void

    .line 784
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->setConnected(Z)V

    goto :goto_0
.end method

.method private declared-synchronized updateCumulativeVehicleDistanceTravelled(J)V
    .locals 17
    .param p1, "odoMeterReadingInKms"    # J

    .prologue
    .line 745
    monitor-enter p0

    const-wide/16 v12, 0x0

    cmp-long v12, p1, v12

    if-lez v12, :cond_2

    .line 746
    :try_start_0
    sget-object v12, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "New odometer reading (KMs) : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-wide/from16 v0, p1

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 747
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 748
    .local v4, "preferences":Landroid/content/SharedPreferences;
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/navdy/hud/app/obd/ObdManager;->odoMeterReadingValidated:Z

    if-nez v12, :cond_1

    .line 749
    sget-object v12, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "Validating the Cumulative mileage data"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 750
    const-string v12, "last_odometer_reading"

    const-wide/16 v14, 0x0

    invoke-interface {v4, v12, v14, v15}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    long-to-double v8, v12

    .line 751
    .local v8, "savedOdoMeterReadingKms":D
    const-wide/16 v12, 0x0

    cmpl-double v12, v8, v12

    if-nez v12, :cond_3

    .line 752
    sget-object v12, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "No saved odo meter reading, starting fresh"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 753
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/obd/ObdManager;->resetCumulativeMileageData()V

    .line 754
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    const-string v13, "first_odometer_reading"

    move-wide/from16 v0, p1

    invoke-interface {v12, v13, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 760
    :cond_0
    :goto_0
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/navdy/hud/app/obd/ObdManager;->odoMeterReadingValidated:Z

    .line 763
    .end local v8    # "savedOdoMeterReadingKms":D
    :cond_1
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    const-string v13, "last_odometer_reading"

    move-wide/from16 v0, p1

    invoke-interface {v12, v13, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 766
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTripManager()Lcom/navdy/hud/app/framework/trips/TripManager;

    move-result-object v5

    .line 767
    .local v5, "tripManager":Lcom/navdy/hud/app/framework/trips/TripManager;
    const-string v12, "total_distance_travelled_with_navdy"

    const-wide/16 v14, 0x0

    invoke-interface {v4, v12, v14, v15}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 768
    .local v6, "savedDistanceTravelledWithNavdyMeters":J
    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelled()I

    move-result v12

    int-to-long v10, v12

    .line 769
    .local v10, "totalDistanceTravelledMeters":J
    sget-object v12, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Saving total distance travelled in current session, before :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", Current Trip mileage : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelled()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 770
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/navdy/hud/app/obd/ObdManager;->totalDistanceInCurrentSessionPersisted:J

    sub-long v2, v10, v12

    .line 771
    .local v2, "newDistanceAfterLastSaveMeters":J
    const-wide/16 v12, 0x0

    cmp-long v12, v2, v12

    if-lez v12, :cond_2

    .line 772
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    const-string v13, "total_distance_travelled_with_navdy"

    add-long v14, v6, v2

    invoke-interface {v12, v13, v14, v15}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 773
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/navdy/hud/app/obd/ObdManager;->totalDistanceInCurrentSessionPersisted:J

    .line 774
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/obd/ObdManager;->reportMileageEvent()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 777
    .end local v2    # "newDistanceAfterLastSaveMeters":J
    .end local v4    # "preferences":Landroid/content/SharedPreferences;
    .end local v5    # "tripManager":Lcom/navdy/hud/app/framework/trips/TripManager;
    .end local v6    # "savedDistanceTravelledWithNavdyMeters":J
    .end local v10    # "totalDistanceTravelledMeters":J
    :cond_2
    monitor-exit p0

    return-void

    .line 755
    .restart local v4    # "preferences":Landroid/content/SharedPreferences;
    .restart local v8    # "savedOdoMeterReadingKms":D
    :cond_3
    move-wide/from16 v0, p1

    long-to-double v12, v0

    cmpg-double v12, v12, v8

    if-gez v12, :cond_0

    .line 756
    :try_start_1
    sget-object v12, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Odometer reading mismatch with saved reading, resetting, Saved :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", New : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-wide/from16 v0, p1

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 757
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/obd/ObdManager;->resetCumulativeMileageData()V

    .line 758
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    const-string v13, "first_odometer_reading"

    move-wide/from16 v0, p1

    invoke-interface {v12, v13, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 745
    .end local v4    # "preferences":Landroid/content/SharedPreferences;
    .end local v8    # "savedOdoMeterReadingKms":D
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12
.end method

.method private updateObdScanSetting()V
    .locals 15

    .prologue
    .line 987
    iget-object v11, p0, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    if-eqz v11, :cond_3

    .line 988
    iget-object v11, p0, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v11}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v7

    .line 989
    .local v7, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v7, :cond_3

    .line 990
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->isAutoOnEnabled()Z

    move-result v11

    if-nez v11, :cond_4

    .line 991
    iget-object v11, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setAutoOnEnabled(Z)V

    .line 996
    :cond_0
    :goto_0
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getObdScanSetting()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-result-object v9

    .line 997
    .local v9, "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    sget-object v11, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ObdScanSetting received : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 999
    if-eqz v9, :cond_3

    .line 1000
    const/4 v10, 0x0

    .line 1001
    .local v10, "writeSetting":Z
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1002
    .local v6, "preferences":Landroid/content/SharedPreferences;
    const-string v11, "default_set"

    const/4 v12, 0x0

    invoke-interface {v6, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1003
    const/4 v10, 0x1

    .line 1005
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager;->isObdScanningEnabled()Z

    move-result v11

    if-eqz v11, :cond_6

    sget-object v8, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 1007
    .local v8, "savedSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :goto_1
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "default_set"

    .line 1008
    invoke-interface {v11, v12}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 1009
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1013
    :goto_2
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getObdBlacklistModificationTime()J

    move-result-wide v4

    .line 1014
    .local v4, "phoneModificationTime":J
    const-string v11, "blacklist_modification_time"

    const-wide/16 v12, 0x0

    invoke-interface {v6, v11, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1015
    .local v2, "localModificationTime":J
    cmp-long v11, v4, v2

    if-lez v11, :cond_8

    const/4 v1, 0x1

    .line 1016
    .local v1, "phoneNewer":Z
    :goto_3
    sget-object v11, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "saved setting:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " blacklist updated: ("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") (phone/local)"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1019
    sget-object v11, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Is Obd enabled :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager;->isObdScanningEnabled()Z

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1020
    sget-object v11, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    if-eq v8, v11, :cond_1

    sget-object v11, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    if-ne v8, v11, :cond_9

    :cond_1
    const/4 v0, 0x1

    .line 1022
    .local v0, "isDefault":Z
    :goto_4
    sget-object v11, Lcom/navdy/hud/app/obd/ObdManager$8;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting:[I

    invoke-virtual {v9}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 1045
    :cond_2
    :goto_5
    if-eqz v10, :cond_3

    .line 1046
    sget-object v11, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Writing scan setting:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " modTime:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1048
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "scan_setting"

    .line 1049
    invoke-virtual {v9}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->name()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "blacklist_modification_time"

    .line 1050
    invoke-interface {v11, v12, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 1051
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1056
    .end local v0    # "isDefault":Z
    .end local v1    # "phoneNewer":Z
    .end local v2    # "localModificationTime":J
    .end local v4    # "phoneModificationTime":J
    .end local v6    # "preferences":Landroid/content/SharedPreferences;
    .end local v7    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v8    # "savedSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .end local v9    # "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .end local v10    # "writeSetting":Z
    :cond_3
    return-void

    .line 992
    .restart local v7    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    :cond_4
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 993
    :cond_5
    iget-object v11, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->onCarDetailsAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    iget-object v11, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setCarDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1005
    .restart local v6    # "preferences":Landroid/content/SharedPreferences;
    .restart local v9    # "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .restart local v10    # "writeSetting":Z
    :cond_6
    sget-object v8, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    goto/16 :goto_1

    .line 1011
    :cond_7
    invoke-direct {p0, v6}, Lcom/navdy/hud/app/obd/ObdManager;->getScanSetting(Landroid/content/SharedPreferences;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-result-object v8

    .restart local v8    # "savedSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    goto/16 :goto_2

    .line 1015
    .restart local v2    # "localModificationTime":J
    .restart local v4    # "phoneModificationTime":J
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 1020
    .restart local v1    # "phoneNewer":Z
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 1024
    .restart local v0    # "isDefault":Z
    :pswitch_0
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1025
    sget-object v11, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "Setting default to on"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1026
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Lcom/navdy/hud/app/obd/ObdManager;->enableObdScanning(Z)V

    .line 1027
    const/4 v10, 0x1

    goto/16 :goto_5

    .line 1031
    :pswitch_1
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Lcom/navdy/hud/app/obd/ObdManager;->enableObdScanning(Z)V

    .line 1032
    const/4 v10, 0x1

    .line 1033
    goto/16 :goto_5

    .line 1035
    :pswitch_2
    const/4 v11, 0x0

    invoke-virtual {p0, v11}, Lcom/navdy/hud/app/obd/ObdManager;->enableObdScanning(Z)V

    .line 1036
    const/4 v10, 0x1

    .line 1037
    goto/16 :goto_5

    .line 1039
    :pswitch_3
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1040
    const/4 v11, 0x0

    invoke-virtual {p0, v11}, Lcom/navdy/hud/app/obd/ObdManager;->enableObdScanning(Z)V

    .line 1041
    const/4 v10, 0x1

    goto/16 :goto_5

    .line 1022
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public enableInstantaneousMode(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 814
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->onInstantaneousModeChanged(Z)V

    .line 815
    if-eqz p1, :cond_0

    .line 816
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->setScanSchedule(Lcom/navdy/obd/ScanSchedule;)V

    .line 825
    :goto_0
    return-void

    .line 818
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/debug/DriveRecorder;->isAutoRecordingEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 820
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->fullScan:Lcom/navdy/obd/ScanSchedule;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->setScanSchedule(Lcom/navdy/obd/ScanSchedule;)V

    goto :goto_0

    .line 822
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->minimalScan:Lcom/navdy/obd/ScanSchedule;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->setScanSchedule(Lcom/navdy/obd/ScanSchedule;)V

    goto :goto_0
.end method

.method public enableObdScanning(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    .line 828
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 829
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 831
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting obd scanning enabled:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 832
    invoke-interface {v0, p1}, Lcom/navdy/obd/ICarService;->setObdPidsScanningEnabled(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 837
    :cond_0
    :goto_0
    return-void

    .line 833
    :catch_0
    move-exception v1

    .line 834
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p1, :cond_1

    const-string v2, "enabling "

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Obd PIDs scanning "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    const-string v2, "disabling "

    goto :goto_1
.end method

.method public getBatteryVoltage()D
    .locals 4

    .prologue
    .line 924
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 925
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 927
    :try_start_0
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getBatteryVoltage()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 932
    :cond_0
    :goto_0
    iget-wide v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D

    return-wide v2

    .line 928
    :catch_0
    move-exception v1

    .line 929
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getCarService()Lcom/navdy/obd/ICarService;
    .locals 2

    .prologue
    .line 362
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    if-eqz v1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 366
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConnectionType()Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->connectionType:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    return-object v0
.end method

.method public getDistanceTravelled()I
    .locals 2

    .prologue
    .line 892
    const/16 v0, 0x31

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getDistanceTravelledWithNavdy()J
    .locals 4

    .prologue
    .line 896
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 897
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "total_distance_travelled_with_navdy"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public getDriveRecorder()Lcom/navdy/hud/app/debug/DriveRecorder;
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    return-object v0
.end method

.method public getEcus()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/ECU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 905
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->ecus:Ljava/util/List;

    return-object v0
.end method

.method public getEngineRpm()I
    .locals 2

    .prologue
    .line 884
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getFuelLevel()I
    .locals 2

    .prologue
    .line 877
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    iget-boolean v0, v0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->isWaitingForValidData()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 878
    :cond_0
    const/4 v0, -0x1

    .line 880
    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method

.method public getInstantFuelConsumption()D
    .locals 2

    .prologue
    .line 888
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public getMaxBatteryVoltage()D
    .locals 2

    .prologue
    .line 940
    iget-wide v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->maxBatteryVoltage:D

    return-wide v0
.end method

.method public getMinBatteryVoltage()D
    .locals 2

    .prologue
    .line 936
    iget-wide v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->minBatteryVoltage:D

    return-wide v0
.end method

.method public getObdCanBusRecordingPolicy()Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    return-object v0
.end method

.method public getObdChipFirmwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdChipFirmwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getObdDeviceConfigurationManager()Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    .locals 1

    .prologue
    .line 971
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    return-object v0
.end method

.method public getPidValue(I)D
    .locals 2
    .param p1, "pid"    # I

    .prologue
    .line 917
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 918
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 920
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedPids()Lcom/navdy/obd/PidSet;
    .locals 1

    .prologue
    .line 913
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    return-object v0
.end method

.method public getTroubleCodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1209
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->troubleCodes:Ljava/util/List;

    return-object v0
.end method

.method public getVin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;

    return-object v0
.end method

.method public injectObdData(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 950
    .local p1, "pidsread":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .local p2, "pidschanged":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->pidListener:Lcom/navdy/obd/IPidListener;

    invoke-interface {v1, p1, p2}, Lcom/navdy/obd/IPidListener;->pidsRead(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 954
    :goto_0
    return-void

    .line 951
    :catch_0
    move-exception v0

    .line 952
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Remote exception when injecting fake obd data"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isCheckEngineLightOn()Z
    .locals 1

    .prologue
    .line 1205
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->isCheckEngineLightOn:Z

    return v0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdConnected:Z

    return v0
.end method

.method public isObdScanningEnabled()Z
    .locals 4

    .prologue
    .line 1070
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 1071
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 1073
    :try_start_0
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->isObdPidsScanningEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1078
    :goto_0
    return v2

    .line 1074
    :catch_0
    move-exception v1

    .line 1075
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "RemoteException "

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1078
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isSleeping()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 508
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 509
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 511
    :try_start_0
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getConnectionState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 515
    :cond_0
    :goto_0
    return v1

    .line 512
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public isSpeedPidAvailable()Z
    .locals 2

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    if-eqz v0, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    .line 1085
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public monitorBatteryVoltage()V
    .locals 6

    .prologue
    .line 453
    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "monitorBatteryVoltage()"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 454
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->periodicCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 457
    const/16 v0, 0xfa0

    .line 458
    .local v0, "INITIAL_BATTERY_MONITOR_DELAY":I
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->periodicCheckRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 459
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 0
    .param p1, "changed"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 977
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdManager;->updateObdScanSetting()V

    .line 978
    return-void
.end method

.method public onDriverProfileUpdated(Lcom/navdy/hud/app/event/DriverProfileUpdated;)V
    .locals 0
    .param p1, "updated"    # Lcom/navdy/hud/app/event/DriverProfileUpdated;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 982
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdManager;->updateObdScanSetting()V

    .line 983
    return-void
.end method

.method public onDrivingStateChange(Lcom/navdy/hud/app/event/DrivingStateChange;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/event/DrivingStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 800
    iget-boolean v2, p1, Lcom/navdy/hud/app/event/DrivingStateChange;->driving:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 801
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 802
    .local v0, "api":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 804
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Driving started - triggering obd scan"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 805
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->rescan()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 811
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    :cond_0
    :goto_0
    return-void

    .line 806
    .restart local v0    # "api":Lcom/navdy/obd/ICarService;
    :catch_0
    move-exception v1

    .line 807
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Failed to trigger OBD rescan"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onObdStatusRequest(Lcom/navdy/service/library/events/obd/ObdStatusRequest;)V
    .locals 8
    .param p1, "request"    # Lcom/navdy/service/library/events/obd/ObdStatusRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 872
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v0

    .line 873
    .local v0, "speed":I
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v4, Lcom/navdy/service/library/events/obd/ObdStatusResponse;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    invoke-direct {v4, v5, v6, v7, v1}, Lcom/navdy/service/library/events/obd/ObdStatusResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 874
    return-void

    .line 873
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onShutdown(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 2
    .param p1, "shutdown"    # Lcom/navdy/hud/app/event/Shutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 865
    iget-object v0, p1, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->SHUTTING_DOWN:Lcom/navdy/hud/app/event/Shutdown$State;

    if-ne v0, v1, :cond_0

    .line 866
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/CarServiceConnector;->shutdown()V

    .line 868
    :cond_0
    return-void
.end method

.method public onWakeUp(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 2
    .param p1, "wakeup"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 371
    sget-object v0, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onWakeUp"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager;->updateListener()V

    .line 373
    return-void
.end method

.method serviceConnected()V
    .locals 3

    .prologue
    .line 312
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/obd/ObdManager$3;-><init>(Lcom/navdy/hud/app/obd/ObdManager;)V

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 318
    return-void
.end method

.method serviceDisconnected()V
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/obd/ObdManager;->setConnected(Z)V

    .line 501
    return-void
.end method

.method public setDriveRecorder(Lcom/navdy/hud/app/debug/DriveRecorder;)V
    .locals 0
    .param p1, "driveRecorder"    # Lcom/navdy/hud/app/debug/DriveRecorder;

    .prologue
    .line 1090
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    .line 1091
    return-void
.end method

.method public setMode(IZ)V
    .locals 5
    .param p1, "mode"    # I
    .param p2, "persistent"    # Z

    .prologue
    .line 1163
    packed-switch p1, :pswitch_data_0

    .line 1176
    :cond_0
    :goto_0
    return-void

    .line 1166
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 1167
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 1169
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/navdy/obd/ICarService;->setMode(IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1170
    :catch_0
    move-exception v1

    .line 1171
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1163
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setRecordingPidListener(Lcom/navdy/obd/IPidListener;)V
    .locals 0
    .param p1, "recordingPidListener"    # Lcom/navdy/obd/IPidListener;

    .prologue
    .line 945
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->recordingPidListener:Lcom/navdy/obd/IPidListener;

    .line 946
    return-void
.end method

.method public setSupportedPidSet(Lcom/navdy/obd/PidSet;)V
    .locals 2
    .param p1, "supportedPids"    # Lcom/navdy/obd/PidSet;

    .prologue
    .line 957
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z

    if-eqz v0, :cond_0

    .line 958
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z

    .line 959
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    .line 967
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 968
    return-void

    .line 961
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    if-eqz v0, :cond_1

    .line 962
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    invoke-virtual {v0, p1}, Lcom/navdy/obd/PidSet;->merge(Lcom/navdy/obd/PidSet;)V

    goto :goto_0

    .line 964
    :cond_1
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;

    goto :goto_0
.end method

.method public sleep(Z)V
    .locals 5
    .param p1, "deep"    # Z

    .prologue
    .line 842
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 843
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 845
    :try_start_0
    invoke-interface {v0, p1}, Lcom/navdy/obd/ICarService;->sleep(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 850
    :cond_0
    :goto_0
    return-void

    .line 846
    :catch_0
    move-exception v1

    .line 847
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error during call to sleep Remote Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateFirmware(Lcom/navdy/hud/app/obd/ObdManager$Firmware;)V
    .locals 8
    .param p1, "firmware"    # Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    .prologue
    .line 1179
    sget-object v6, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Updating the firmware of the OBD chip , Version : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v5, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->OLD_320:Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    if-ne p1, v5, :cond_1

    const-string v5, "3.2.0"

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1180
    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v1

    .line 1181
    .local v1, "carService":Lcom/navdy/obd/ICarService;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v6, p1, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->resource:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 1182
    .local v4, "is":Ljava/io/InputStream;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "update.bin"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1183
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1184
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1185
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1188
    :cond_0
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/io/InputStream;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1194
    :try_start_1
    const-string v5, "4.2.1"

    invoke-interface {v1, v5, v0}, Lcom/navdy/obd/ICarService;->updateFirmware(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1198
    :goto_1
    return-void

    .line 1179
    .end local v0    # "absolutePath":Ljava/lang/String;
    .end local v1    # "carService":Lcom/navdy/obd/ICarService;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "is":Ljava/io/InputStream;
    :cond_1
    const-string v5, "4.2.1"

    goto :goto_0

    .line 1189
    .restart local v0    # "absolutePath":Ljava/lang/String;
    .restart local v1    # "carService":Lcom/navdy/obd/ICarService;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v2

    .line 1190
    .local v2, "e":Ljava/io/IOException;
    sget-object v5, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error writing to the file "

    invoke-virtual {v5, v6, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1195
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 1196
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Remote Exception while updating firmware "

    invoke-virtual {v5, v6, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public updateListener()V
    .locals 7

    .prologue
    .line 321
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 322
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_1

    .line 324
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->canBusMonitoringListener:Lcom/navdy/obd/ICanBusMonitoringListener;

    invoke-interface {v0, v4}, Lcom/navdy/obd/ICarService;->setCANBusMonitoringListener(Lcom/navdy/obd/ICanBusMonitoringListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :goto_0
    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z

    .line 330
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getConnectionState()I

    move-result v1

    .line 331
    .local v1, "connectionState":I
    packed-switch v1, :pswitch_data_0

    .line 342
    :goto_1
    :pswitch_0
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getConnectionState()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/navdy/hud/app/obd/ObdManager;->updateConnectionState(I)V

    .line 343
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager;->monitorBatteryVoltage()V

    .line 344
    sget-object v4, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "adding listener for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager;->schedule:Lcom/navdy/obd/ScanSchedule;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 345
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->schedule:Lcom/navdy/obd/ScanSchedule;

    if-eqz v4, :cond_0

    .line 346
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->schedule:Lcom/navdy/obd/ScanSchedule;

    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdManager;->pidListener:Lcom/navdy/obd/IPidListener;

    invoke-interface {v0, v4, v5}, Lcom/navdy/obd/ICarService;->updateScan(Lcom/navdy/obd/ScanSchedule;Lcom/navdy/obd/IPidListener;)V

    .line 348
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdCanBusRecordingPolicy:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringNeeded()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 349
    sget-object v4, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Start CAN bus monitoring"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 350
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->startCanBusMonitoring()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 359
    .end local v1    # "connectionState":I
    :cond_1
    :goto_2
    return-void

    .line 325
    :catch_0
    move-exception v2

    .line 326
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "RemoteException "

    invoke-virtual {v4, v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 333
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "connectionState":I
    :pswitch_1
    :try_start_2
    sget-object v4, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "ObdService is in Sleeping state, waking it up"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 334
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->wakeup()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 355
    .end local v1    # "connectionState":I
    :catch_1
    move-exception v3

    .line 356
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 338
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v1    # "connectionState":I
    :pswitch_2
    :try_start_3
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getObdChipFirmwareVersion()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdChipFirmwareVersion:Ljava/lang/String;

    .line 339
    sget-object v4, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Obd chip firmware version "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager;->obdChipFirmwareVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Connection State :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 352
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "stop CAN bus monitoring"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 353
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->stopCanBusMonitoring()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 331
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public wakeup()V
    .locals 5

    .prologue
    .line 853
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 854
    .local v0, "carService":Lcom/navdy/obd/ICarService;
    if-eqz v0, :cond_0

    .line 856
    :try_start_0
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->wakeup()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 861
    :cond_0
    :goto_0
    return-void

    .line 857
    :catch_0
    move-exception v1

    .line 858
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error during call to sleep Remote Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
