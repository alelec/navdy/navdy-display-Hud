.class Lcom/navdy/hud/app/obd/ObdManager$5;
.super Ljava/lang/Object;
.source "ObdManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/obd/ObdManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/ObdManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 384
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/high16 v12, -0x4010000000000000L    # -1.0

    const/4 v10, 0x0

    .line 386
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v4

    .line 387
    .local v4, "prevVoltage":D
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager;->getBatteryVoltage()D

    move-result-wide v8

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6, v8, v9}, Lcom/navdy/hud/app/obd/ObdManager;->access$402(Lcom/navdy/hud/app/obd/ObdManager;D)D

    .line 388
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    cmpl-double v6, v6, v12

    if-eqz v6, :cond_0

    cmpl-double v6, v4, v12

    if-nez v6, :cond_0

    .line 389
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v8

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->minBatteryVoltage:D
    invoke-static {v6, v8, v9}, Lcom/navdy/hud/app/obd/ObdManager;->access$502(Lcom/navdy/hud/app/obd/ObdManager;D)D

    .line 390
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordStartingBatteryVoltage(D)V

    .line 393
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    cmpl-double v6, v6, v12

    if-nez v6, :cond_4

    .line 394
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$600(Lcom/navdy/hud/app/obd/ObdManager;)I

    move-result v6

    if-eqz v6, :cond_2

    .line 395
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "Battery level data is not available"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 397
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I
    invoke-static {v6, v10}, Lcom/navdy/hud/app/obd/ObdManager;->access$602(Lcom/navdy/hud/app/obd/ObdManager;I)I

    .line 398
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I
    invoke-static {v6, v10}, Lcom/navdy/hud/app/obd/ObdManager;->access$802(Lcom/navdy/hud/app/obd/ObdManager;I)I

    .line 449
    :cond_3
    :goto_0
    return-void

    .line 402
    :cond_4
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->minBatteryVoltage:D
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$500(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v8

    cmpg-double v6, v6, v8

    if-gez v6, :cond_5

    .line 403
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v8

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->minBatteryVoltage:D
    invoke-static {v6, v8, v9}, Lcom/navdy/hud/app/obd/ObdManager;->access$502(Lcom/navdy/hud/app/obd/ObdManager;D)D

    .line 405
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->maxBatteryVoltage:D
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$900(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v8

    cmpl-double v6, v6, v8

    if-lez v6, :cond_6

    .line 406
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v8

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->maxBatteryVoltage:D
    invoke-static {v6, v8, v9}, Lcom/navdy/hud/app/obd/ObdManager;->access$902(Lcom/navdy/hud/app/obd/ObdManager;D)D

    .line 409
    :cond_6
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v6, v6, Lcom/navdy/hud/app/obd/ObdManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v6

    if-eqz v6, :cond_7

    sget-wide v2, Lcom/navdy/hud/app/obd/ObdManager;->LOW_BATTERY_VOLTAGE:D

    .line 412
    .local v2, "lowBatteryThreshold":D
    :goto_1
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    const-wide v8, 0x402a333340000000L    # 13.100000381469727

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_8

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v6, v6, Lcom/navdy/hud/app/obd/ObdManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 414
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "Battery seems to be charging, waking up"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 415
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v6, v6, Lcom/navdy/hud/app/obd/ObdManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    sget-object v7, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->VOLTAGE_SPIKE:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/device/PowerManager;->wakeUp(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V

    .line 416
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I
    invoke-static {v6, v10}, Lcom/navdy/hud/app/obd/ObdManager;->access$802(Lcom/navdy/hud/app/obd/ObdManager;I)I

    goto :goto_0

    .line 409
    .end local v2    # "lowBatteryThreshold":D
    :cond_7
    sget-wide v2, Lcom/navdy/hud/app/obd/ObdManager;->CRITICAL_BATTERY_VOLTAGE:D

    goto :goto_1

    .line 417
    .restart local v2    # "lowBatteryThreshold":D
    :cond_8
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v6

    cmpg-double v6, v6, v2

    if-gez v6, :cond_a

    .line 418
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I
    invoke-static {v6, v10}, Lcom/navdy/hud/app/obd/ObdManager;->access$802(Lcom/navdy/hud/app/obd/ObdManager;I)I

    .line 420
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Battery voltage (LOW) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 421
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # operator++ for: Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$608(Lcom/navdy/hud/app/obd/ObdManager;)I

    .line 422
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$600(Lcom/navdy/hud/app/obd/ObdManager;)I

    move-result v6

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->LOW_BATTERY_COUNT_THRESHOLD:I
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$1000()I

    move-result v7

    if-le v6, v7, :cond_3

    .line 423
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "Battery Voltage is too low, shutting down"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 425
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v6, v6, Lcom/navdy/hud/app/obd/ObdManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v6

    if-eqz v6, :cond_9

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->LOW_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;

    .line 427
    .local v1, "reason":Lcom/navdy/hud/app/event/Shutdown$Reason;
    :goto_2
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/obd/ObdManager;->sleep(Z)V

    .line 428
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v6, v6, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v7, Lcom/navdy/hud/app/event/Shutdown;

    invoke-direct {v7, v1}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 430
    .end local v1    # "reason":Lcom/navdy/hud/app/event/Shutdown$Reason;
    :catch_0
    move-exception v0

    .line 431
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "error invoking ShutdownMonitor.androidShutdown(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 425
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_9
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$Reason;->CRITICAL_VOLTAGE:Lcom/navdy/hud/app/event/Shutdown$Reason;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 435
    :cond_a
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->lowBatteryCount:I
    invoke-static {v6, v10}, Lcom/navdy/hud/app/obd/ObdManager;->access$602(Lcom/navdy/hud/app/obd/ObdManager;I)I

    .line 436
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v6, v6, Lcom/navdy/hud/app/obd/ObdManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 437
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "In Quiet mode , Battery voltage : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->batteryVoltage:D
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$400(Lcom/navdy/hud/app/obd/ObdManager;)D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 438
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # operator++ for: Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$808(Lcom/navdy/hud/app/obd/ObdManager;)I

    .line 439
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$800(Lcom/navdy/hud/app/obd/ObdManager;)I

    move-result v6

    const/4 v7, 0x5

    if-lt v6, v7, :cond_3

    .line 440
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "Safe to put obd chip to sleep, invoking sleep"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 441
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->safeToSleepBatteryLevelObservedCount:I
    invoke-static {v6, v10}, Lcom/navdy/hud/app/obd/ObdManager;->access$802(Lcom/navdy/hud/app/obd/ObdManager;I)I

    .line 443
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/obd/ObdManager;->access$200(Lcom/navdy/hud/app/obd/ObdManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->periodicCheckRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$1100(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 445
    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdManager$5;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v6, v10}, Lcom/navdy/hud/app/obd/ObdManager;->sleep(Z)V

    goto/16 :goto_0
.end method
