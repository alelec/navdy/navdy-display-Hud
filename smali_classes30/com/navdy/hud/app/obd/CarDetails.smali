.class public Lcom/navdy/hud/app/obd/CarDetails;
.super Ljava/lang/Object;
.source "CarDetails.java"


# static fields
.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field aaia:Ljava/lang/String;

.field engine:Ljava/lang/String;

.field engineType:Ljava/lang/String;

.field make:Ljava/lang/String;

.field model:Ljava/lang/String;

.field vin:Ljava/lang/String;

.field year:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/obd/CarDetails;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/obd/CarDetails;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lcom/navdy/hud/app/obd/CarDetails;
    .locals 5
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 29
    :try_start_0
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 30
    .local v1, "gson":Lcom/google/gson/Gson;
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/navdy/hud/app/obd/CarDetails;

    invoke-virtual {v1, v3, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/obd/CarDetails;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .end local v1    # "gson":Lcom/google/gson/Gson;
    :goto_0
    return-object v0

    .line 32
    :catch_0
    move-exception v2

    .line 33
    .local v2, "se":Lcom/google/gson/JsonSyntaxException;
    sget-object v3, Lcom/navdy/hud/app/obd/CarDetails;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while parsing the car details response from CarMD "

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static matches(Lcom/navdy/hud/app/obd/CarDetails;Ljava/lang/String;)Z
    .locals 1
    .param p0, "details"    # Lcom/navdy/hud/app/obd/CarDetails;
    .param p1, "vinNumber"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-static {p1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->isValidVin(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarDetails;->vin:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
