.class Lcom/navdy/hud/app/obd/ObdManager$PidCheck;
.super Ljava/lang/Object;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PidCheck"
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field volatile hasIncorrectData:Z

.field private invalidPidRunnable:Ljava/lang/Runnable;

.field pid:I

.field final synthetic this$0:Lcom/navdy/hud/app/obd/ObdManager;

.field volatile waitingForValidData:Z


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/obd/ObdManager;ILandroid/os/Handler;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p2, "pid"    # I
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v0, 0x0

    .line 1104
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1098
    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData:Z

    .line 1099
    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->waitingForValidData:Z

    .line 1105
    iput p2, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->pid:I

    .line 1106
    iput-object p3, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->handler:Landroid/os/Handler;

    .line 1107
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;-><init>(Lcom/navdy/hud/app/obd/ObdManager$PidCheck;Lcom/navdy/hud/app/obd/ObdManager;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->invalidPidRunnable:Ljava/lang/Runnable;

    .line 1117
    return-void
.end method


# virtual methods
.method public checkPid(D)V
    .locals 5
    .param p1, "value"    # D

    .prologue
    .line 1145
    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->isPidValueValid(D)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1146
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData:Z

    if-nez v0, :cond_0

    .line 1147
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->waitingForValidData:Z

    if-nez v0, :cond_1

    .line 1148
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->invalidPidRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1149
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->invalidPidRunnable:Ljava/lang/Runnable;

    .line 1150
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->getWaitForValidDataTimeout()J

    move-result-wide v2

    .line 1149
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->waitingForValidData:Z

    .line 1159
    :cond_0
    :goto_0
    return-void

    .line 1153
    :cond_1
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already waiting for valid PID data, PID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->pid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,  Value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 1157
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->reset()V

    goto :goto_0
.end method

.method public getWaitForValidDataTimeout()J
    .locals 2

    .prologue
    .line 1135
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public hasIncorrectData()Z
    .locals 1

    .prologue
    .line 1127
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData:Z

    return v0
.end method

.method public invalidatePid(I)V
    .locals 0
    .param p1, "pid"    # I

    .prologue
    .line 1124
    return-void
.end method

.method public isPidValueValid(D)Z
    .locals 1
    .param p1, "value"    # D

    .prologue
    .line 1120
    const/4 v0, 0x1

    return v0
.end method

.method public isWaitingForValidData()Z
    .locals 1

    .prologue
    .line 1131
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->waitingForValidData:Z

    return v0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1139
    iput-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData:Z

    .line 1140
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->invalidPidRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1141
    iput-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->waitingForValidData:Z

    .line 1142
    return-void
.end method
