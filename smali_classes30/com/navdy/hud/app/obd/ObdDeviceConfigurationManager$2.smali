.class Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;
.super Ljava/lang/Object;
.source "ObdDeviceConfigurationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setCarDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

.field final synthetic val$make:Ljava/lang/String;

.field final synthetic val$model:Ljava/lang/String;

.field final synthetic val$year:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$make:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$model:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$year:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 217
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z
    invoke-static {v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$100(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 218
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "setCarDetails: Auto on is de selected by the user , so skipping the overwriting of configuration"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsVinRecognized:Z
    invoke-static {v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$300(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 222
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "setCarDetails: Vin is recognized and it is used as the preferred source"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$make:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "*"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$model:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "*"

    :goto_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$year:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "*"

    :goto_3
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 226
    .local v1, "name":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Vehicle name :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 227
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$400()[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v2

    if-nez v2, :cond_3

    .line 228
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    const-string v3, "configuration_mapping.csv"

    # invokes: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->loadConfigurationMappingList(Ljava/lang/String;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$500(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v2

    # setter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$402([Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    .line 230
    :cond_3
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$400()[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->pickConfiguration([Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v0

    .line 231
    .local v0, "matchingConfiguration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    if-eqz v0, :cond_0

    .line 232
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    iget-object v3, v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;->configurationName:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->bSetConfigurationFile(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$000(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 225
    .end local v0    # "matchingConfiguration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .end local v1    # "name":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$make:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$model:Ljava/lang/String;

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;->val$year:Ljava/lang/String;

    goto :goto_3
.end method
