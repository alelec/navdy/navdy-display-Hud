.class public final enum Lcom/navdy/hud/app/obd/ObdManager$Firmware;
.super Ljava/lang/Enum;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Firmware"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/obd/ObdManager$Firmware;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/obd/ObdManager$Firmware;

.field public static final enum NEW_421:Lcom/navdy/hud/app/obd/ObdManager$Firmware;

.field public static final enum OLD_320:Lcom/navdy/hud/app/obd/ObdManager$Firmware;


# instance fields
.field public resource:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    const-string v1, "OLD_320"

    const v2, 0x7f07000e

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/hud/app/obd/ObdManager$Firmware;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->OLD_320:Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    .line 110
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    const-string v1, "NEW_421"

    const v2, 0x7f07000f

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/hud/app/obd/ObdManager$Firmware;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->NEW_421:Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    .line 108
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->OLD_320:Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->NEW_421:Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->$VALUES:[Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 114
    iput p3, p0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->resource:I

    .line 115
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdManager$Firmware;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    const-class v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/obd/ObdManager$Firmware;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/navdy/hud/app/obd/ObdManager$Firmware;->$VALUES:[Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/obd/ObdManager$Firmware;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/obd/ObdManager$Firmware;

    return-object v0
.end method
