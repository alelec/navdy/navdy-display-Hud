.class Lcom/navdy/hud/app/obd/CarMDVinDecoder$1$1;
.super Ljava/lang/Object;
.source "CarMDVinDecoder.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1$1;->this$1:Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 2
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 119
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onFailure : Decoding failed "

    invoke-virtual {v0, v1, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1$1;->this$1:Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;

    iget-object v0, v0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z

    .line 121
    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 2
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    # getter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onResponse : Decoding response received"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1$1;->this$1:Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;

    iget-object v0, v0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v1

    # invokes: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->parseResponse(Lokhttp3/ResponseBody;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$800(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Lokhttp3/ResponseBody;)V

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1$1;->this$1:Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;

    iget-object v0, v0, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;->this$0:Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z

    .line 128
    return-void
.end method
