.class public Lcom/navdy/hud/app/obd/ObdManager$ObdPidReadEvent;
.super Ljava/lang/Object;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ObdPidReadEvent"
.end annotation


# instance fields
.field public readPids:Lcom/navdy/obd/PidSet;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "readPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    new-instance v0, Lcom/navdy/obd/PidSet;

    invoke-direct {v0, p1}, Lcom/navdy/obd/PidSet;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$ObdPidReadEvent;->readPids:Lcom/navdy/obd/PidSet;

    .line 152
    return-void
.end method
