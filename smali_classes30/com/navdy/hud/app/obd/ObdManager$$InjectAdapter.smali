.class public final Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "ObdManager$$InjectAdapter.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/obd/ObdManager;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/obd/ObdManager;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private driverProfileManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/profile/DriverProfileManager;",
            ">;"
        }
    .end annotation
.end field

.field private powerManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/device/PowerManager;",
            ">;"
        }
    .end annotation
.end field

.field private sharedPreferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private telemetryDataManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/analytics/TelemetryDataManager;",
            ">;"
        }
    .end annotation
.end field

.field private tripManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/framework/trips/TripManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 32
    const/4 v0, 0x0

    const-string v1, "members/com.navdy.hud.app.obd.ObdManager"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 33
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 42
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    .line 43
    const-string v0, "com.navdy.hud.app.profile.DriverProfileManager"

    const-class v1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->driverProfileManager:Ldagger/internal/Binding;

    .line 44
    const-string v0, "com.navdy.hud.app.device.PowerManager"

    const-class v1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    .line 45
    const-string v0, "com.navdy.hud.app.framework.trips.TripManager"

    const-class v1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    .line 46
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    .line 47
    const-string v0, "com.navdy.hud.app.analytics.TelemetryDataManager"

    const-class v1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->telemetryDataManager:Ldagger/internal/Binding;

    .line 48
    return-void
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->driverProfileManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->telemetryDataManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/obd/ObdManager;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->driverProfileManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/PowerManager;

    iput-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/trips/TripManager;

    iput-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->telemetryDataManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    iput-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager;->telemetryDataManager:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    .line 76
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/obd/ObdManager$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/obd/ObdManager;)V

    return-void
.end method
