.class Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;
.super Ljava/lang/Object;
.source "ObdDeviceConfigurationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->decodeVin()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 284
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mLastKnowVinNumber:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$600(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->isValidVin(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z
    invoke-static {v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$100(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 285
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->VIN_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$700()[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v1

    if-nez v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    const-string v2, "vin_configuration_mapping.csv"

    # invokes: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->loadConfigurationMappingList(Ljava/lang/String;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$500(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v1

    # setter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->VIN_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$702([Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    .line 288
    :cond_0
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->VIN_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$700()[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mLastKnowVinNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$600(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->pickConfiguration([Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v0

    .line 289
    .local v0, "matchingConfiguration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    if-eqz v0, :cond_2

    .line 290
    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decodeVin : got the matching configuration :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;->configurationName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Vin :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mLastKnowVinNumber:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$600(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 291
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    const/4 v2, 0x1

    # setter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsVinRecognized:Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$302(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Z)Z

    .line 292
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    iget-object v2, v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;->configurationName:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->bSetConfigurationFile(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$000(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;)V

    .line 297
    .end local v0    # "matchingConfiguration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    :cond_1
    :goto_0
    return-void

    .line 294
    .restart local v0    # "matchingConfiguration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;->this$0:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsVinRecognized:Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->access$302(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Z)Z

    goto :goto_0
.end method
