.class public final enum Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;
.super Ljava/lang/Enum;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

.field public static final enum OBD:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

.field public static final enum POWER_ONLY:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 119
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    const-string v1, "POWER_ONLY"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->POWER_ONLY:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    .line 120
    new-instance v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    const-string v1, "OBD"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->OBD:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    .line 118
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->POWER_ONLY:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->OBD:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->$VALUES:[Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 118
    const-class v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->$VALUES:[Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    return-object v0
.end method
