.class public Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
.super Ljava/lang/Object;
.source "ObdDeviceConfigurationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;,
        Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    }
.end annotation


# static fields
.field public static final ASSETS_FOLDER_PREFIX:Ljava/lang/String; = "stn_obd"

.field public static final CAR_CONFIGURATION_MAPPING_FILE:Ljava/lang/String; = "configuration_mapping.csv"

.field private static CAR_DETAILS_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration; = null

.field public static final CONFIGURATION_FILE_EXTENSION:Ljava/lang/String; = "conf"

.field public static final CONFIGURATION_NAME_PREFIX:Ljava/lang/String; = "Navdy OBD-II "

.field public static final DEBUG_CONFIGURATION:Ljava/lang/String; = "debug"

.field public static final DEFAULT_OFF_CONFIGURATION:Ljava/lang/String; = "no_trigger"

.field public static final DEFAULT_ON_CONFIGURATION:Ljava/lang/String; = "default_on"

.field public static final STSATI:Ljava/lang/String; = "STSATI"

.field private static VIN_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration; = null

.field public static final VIN_CONFIGURATION_MAPPING_FILE:Ljava/lang/String; = "vin_configuration_mapping.csv"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mCarServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

.field private volatile mConnected:Z

.field private mExpectedConfigurationFileName:Ljava/lang/String;

.field private volatile mIsAutoOnEnabled:Z

.field private volatile mIsVinRecognized:Z

.field private volatile mLastKnowVinNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/obd/CarServiceConnector;)V
    .locals 1
    .param p1, "mCarServiceConnector"    # Lcom/navdy/hud/app/obd/CarServiceConnector;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mExpectedConfigurationFileName:Ljava/lang/String;

    .line 52
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mCarServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->bSetConfigurationFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z

    return v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsVinRecognized:Z

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsVinRecognized:Z

    return p1
.end method

.method static synthetic access$400()[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    return-object v0
.end method

.method static synthetic access$402([Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .locals 0
    .param p0, "x0"    # [Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    .prologue
    .line 20
    sput-object p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->CAR_DETAILS_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    return-object p0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->loadConfigurationMappingList(Ljava/lang/String;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mLastKnowVinNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700()[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->VIN_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    return-object v0
.end method

.method static synthetic access$702([Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .locals 0
    .param p0, "x0"    # [Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    .prologue
    .line 20
    sput-object p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->VIN_CONFIGURATION_MAPPING:[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    return-object p0
.end method

.method private applyConfiguration(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;)V
    .locals 7
    .param p1, "configuration"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;

    .prologue
    .line 100
    if-eqz p1, :cond_0

    iget-boolean v4, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mConnected:Z

    if-eqz v4, :cond_0

    .line 101
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mCarServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 103
    .local v0, "api":Lcom/navdy/obd/ICarService;
    iget-object v4, p1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;->configurationData:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;->configurationIdentifier:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 104
    sget-object v4, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Writing new configuration :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 106
    :try_start_0
    iget-object v4, p1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;->configurationData:Ljava/lang/String;

    invoke-interface {v0, v4}, Lcom/navdy/obd/ICarService;->applyConfiguration(Ljava/lang/String;)Z

    move-result v1

    .line 107
    .local v1, "applied":Z
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getCurrentConfigurationName()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "newConfigurationName":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "New Configuration applied : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", configuration : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    .end local v1    # "applied":Z
    .end local v2    # "newConfigurationName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 109
    .restart local v0    # "api":Lcom/navdy/obd/ICarService;
    :catch_0
    move-exception v3

    .line 110
    .local v3, "re":Landroid/os/RemoteException;
    sget-object v4, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Failed to apply the configuration :"

    invoke-virtual {v3}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private bSetConfigurationFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "configurationName"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->validateConfigurationName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    sget-object v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received an invalid configuration name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Configuration does not exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mExpectedConfigurationFileName:Ljava/lang/String;

    .line 169
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->syncConfiguration()V

    .line 170
    return-void
.end method

.method private decodeVin()V
    .locals 3

    .prologue
    .line 281
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$3;-><init>(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)V

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 299
    return-void
.end method

.method static extractOBDConfiguration(Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;
    .locals 9
    .param p0, "fileContent"    # Ljava/lang/String;

    .prologue
    .line 136
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .local v4, "machineFriendlyConfiguration":Ljava/lang/StringBuilder;
    const-string v5, "\n"

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 138
    .local v3, "lines":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 139
    .local v1, "configurationIdentifier":Ljava/lang/String;
    array-length v6, v3

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v6, :cond_2

    aget-object v2, v3, v5

    .line 140
    .local v2, "line":Ljava/lang/String;
    const-string v7, "ST"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "AT"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 141
    :cond_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string v7, "STSATI"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 143
    const-string v7, "STSATI"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 139
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 147
    .end local v2    # "line":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "configurationData":Ljava/lang/String;
    new-instance v5, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;

    invoke-direct {v5, v0, v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5
.end method

.method public static isValidVin(Ljava/lang/String;)Z
    .locals 2
    .param p0, "vinNumber"    # Ljava/lang/String;

    .prologue
    .line 315
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadConfigurationMappingList(Ljava/lang/String;)[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .locals 13
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 239
    const/4 v3, 0x0

    .line 241
    .local v3, "configurationsList":[Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->readObdAssetFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "configurationMapping":Ljava/lang/String;
    const-string v10, "\n"

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 243
    .local v2, "configurationMappingEntries":[Ljava/lang/String;
    array-length v10, v2

    new-array v3, v10, [Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    .line 244
    const/4 v6, 0x0

    .line 245
    .local v6, "i":I
    array-length v10, v2

    move v7, v6

    .end local v6    # "i":I
    .local v7, "i":I
    :goto_0
    if-ge v9, v10, :cond_0

    aget-object v5, v2, v9

    .line 246
    .local v5, "entry":Ljava/lang/String;
    const-string v11, ","

    invoke-virtual {v5, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 247
    .local v8, "parts":[Ljava/lang/String;
    new-instance v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;

    invoke-direct {v0}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;-><init>()V

    .line 248
    .local v0, "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    const/4 v11, 0x0

    aget-object v11, v8, v11

    const/4 v12, 0x2

    invoke-static {v11, v12}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v11

    iput-object v11, v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;->pattern:Ljava/util/regex/Pattern;

    .line 249
    const/4 v11, 0x1

    aget-object v11, v8, v11

    iput-object v11, v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;->configurationName:Ljava/lang/String;

    .line 250
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    aput-object v0, v3, v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    add-int/lit8 v9, v9, 0x1

    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_0

    .line 252
    .end local v0    # "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .end local v1    # "configurationMapping":Ljava/lang/String;
    .end local v2    # "configurationMappingEntries":[Ljava/lang/String;
    .end local v5    # "entry":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v8    # "parts":[Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 253
    .local v4, "e":Ljava/io/IOException;
    sget-object v9, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Failed to load the configuration mapping file"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 255
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    return-object v3
.end method

.method public static pickConfiguration([Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .locals 5
    .param p0, "configurationMapping"    # [Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    .param p1, "expression"    # Ljava/lang/String;

    .prologue
    .line 266
    const/4 v1, 0x0

    .line 267
    .local v1, "matchingConfiguration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, p0, v2

    .line 268
    .local v0, "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    iget-object v4, v0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 269
    move-object v1, v0

    .line 273
    .end local v0    # "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    :cond_0
    return-object v1

    .line 267
    .restart local v0    # "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$Configuration;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private readConfiguration(Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 127
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "conf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->readObdAssetFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "configurationFileContent":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->extractOBDConfiguration(Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 132
    .end local v0    # "configurationFileContent":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 129
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/io/IOException;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error reading the configuration file :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 132
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private readObdAssetFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "assetFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stn_obd/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 153
    .local v2, "is":Ljava/io/InputStream;
    const-string v3, "UTF-8"

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "content":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    return-object v0

    .line 156
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Error closing the stream after reading the asset file"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncConfiguration()V
    .locals 8

    .prologue
    .line 77
    iget-boolean v5, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mConnected:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mExpectedConfigurationFileName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 78
    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mCarServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 79
    .local v0, "api":Lcom/navdy/obd/ICarService;
    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mExpectedConfigurationFileName:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->readConfiguration(Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;

    move-result-object v1

    .line 81
    .local v1, "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;
    :try_start_0
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getCurrentConfigurationName()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "configurationName":Ljava/lang/String;
    iget-object v3, v1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;->configurationIdentifier:Ljava/lang/String;

    .line 83
    .local v3, "expectedConfigurationName":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 84
    sget-object v5, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "The configuration on the obd chip is as expected"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 96
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    .end local v1    # "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;
    .end local v2    # "configurationName":Ljava/lang/String;
    .end local v3    # "expectedConfigurationName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 86
    .restart local v0    # "api":Lcom/navdy/obd/ICarService;
    .restart local v1    # "configuration":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;
    .restart local v2    # "configurationName":Ljava/lang/String;
    .restart local v3    # "expectedConfigurationName":Ljava/lang/String;
    :cond_1
    sget-object v5, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "The configuration on the obd chip is different"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 87
    sget-object v5, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Expected : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 88
    sget-object v5, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Actual : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 89
    sget-object v5, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Syncing configuration..."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 90
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->applyConfiguration(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$OBDConfiguration;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    .end local v2    # "configurationName":Ljava/lang/String;
    .end local v3    # "expectedConfigurationName":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 93
    .local v4, "re":Landroid/os/RemoteException;
    sget-object v5, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error while applying configuration"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static turnOffOBDSleep()V
    .locals 2

    .prologue
    .line 323
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getObdDeviceConfigurationManager()Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    move-result-object v0

    const-string v1, "debug"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setConfigurationFile(Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method private validateConfigurationName(Ljava/lang/String;)Z
    .locals 6
    .param p1, "configurationName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 202
    :cond_0
    :goto_0
    return v2

    .line 191
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stn_obd/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "conf"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 192
    .local v1, "is":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 196
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 200
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 201
    .end local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0

    .line 197
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method public isAutoOnEnabled()Z
    .locals 1

    .prologue
    .line 311
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z

    return v0
.end method

.method public setAutoOnEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 304
    iput-boolean p1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z

    .line 305
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z

    if-nez v0, :cond_0

    .line 306
    const-string v0, "no_trigger"

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setConfigurationFile(Ljava/lang/String;)V

    .line 308
    :cond_0
    return-void
.end method

.method public setCarDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "model"    # Ljava/lang/String;
    .param p3, "year"    # Ljava/lang/String;

    .prologue
    .line 214
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$2;-><init>(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 236
    return-void
.end method

.method public setConfigurationFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "configurationFileName"    # Ljava/lang/String;

    .prologue
    .line 178
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager$1;-><init>(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;Ljava/lang/String;)V

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 184
    return-void
.end method

.method public setConnectionState(Z)V
    .locals 5
    .param p1, "connected"    # Z

    .prologue
    .line 56
    iget-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mConnected:Z

    if-eq p1, v2, :cond_1

    .line 57
    iput-boolean p1, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mConnected:Z

    .line 58
    sget-object v2, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "OBD Connection state changed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 59
    iget-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mConnected:Z

    if-eqz v2, :cond_0

    .line 60
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mCarServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 62
    .local v0, "api":Lcom/navdy/obd/ICarService;
    :try_start_0
    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getVIN()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mLastKnowVinNumber:Ljava/lang/String;

    .line 63
    sget-object v2, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Vin number read :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mLastKnowVinNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->decodeVin()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    :cond_0
    :goto_0
    return-void

    .line 65
    .restart local v0    # "api":Lcom/navdy/obd/ICarService;
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Remote exception while getting the VIN number "

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mLastKnowVinNumber:Ljava/lang/String;

    .line 71
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsAutoOnEnabled:Z

    .line 72
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->mIsVinRecognized:Z

    goto :goto_0
.end method

.method public setDecodedCarDetails(Lcom/navdy/hud/app/obd/CarDetails;)V
    .locals 0
    .param p1, "decodedCarDetails"    # Lcom/navdy/hud/app/obd/CarDetails;

    .prologue
    .line 278
    return-void
.end method
