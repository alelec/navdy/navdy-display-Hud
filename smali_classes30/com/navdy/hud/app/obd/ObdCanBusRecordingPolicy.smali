.class public Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
.super Ljava/lang/Object;
.source "ObdCanBusRecordingPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;
    }
.end annotation


# static fields
.field private static final CAN_BUS_MONITORING_DISTANCE_LIMIT_METERS:J = 0x9d2aL

.field public static final CAN_PROTOCOL_PREFIX:Ljava/lang/String; = "ISO 15765-4"

.field public static final FILES_TO_UPLOAD_DIRECTORY:Ljava/lang/String; = "/sdcard/.canBusLogs/upload"

.field public static final MAX_META_DATA_FILES:I = 0xa

.field public static final MAX_UPLOAD_FILES:I = 0x5

.field public static final META_DATA_FILE:Ljava/lang/String; = "/sdcard/.canBusLogs/upload/meta_data_"

.field public static final MINIMUM_TIME_FOR_MOTION_DETECTION:I = 0x2710

.field public static final MINIMUM_TIME_FOR_STOP_DETECTION:I = 0x2710

.field private static final MOVING_SPEED_METERS_PER_SECOND:F = 1.34112f

.field public static final PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED:Ljava/lang/String; = "preference_navdy_miles_when_listening_started"

.field public static final PREF_CAN_BUS_DATA_RECORDED_AND_SENT:Ljava/lang/String; = "canBusDataRecordedAndSent"

.field public static final PREF_CAN_BUS_DATA_SENT_VERSION:Ljava/lang/String; = "canBusDataSentVersion"

.field public static final PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION:Ljava/lang/String; = "canBusDataStatusReportedVersion"

.field private static final UPLOAD_DIRECTORY_FILE:Ljava/io/File;

.field private static final dateFormat:Ljava/text/SimpleDateFormat;

.field private static metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private canBusMonitoringDataSize:I

.field private canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

.field private context:Landroid/content/Context;

.field private currentMetaDataFile:Ljava/io/File;

.field private firstTimeMovementDetected:J

.field private firstTimeStoppingDetected:J

.field private handler:Landroid/os/Handler;

.field private isCanBusMonitoringLimitReached:Z

.field private isCanProtocol:Z

.field private isEngineeringBuild:Z

.field private isInstantaneousModeOn:Z

.field private isObdConnected:Z

.field lastSpeed:I

.field private make:Ljava/lang/String;

.field private model:Ljava/lang/String;

.field private motionDetected:Z

.field private obdManager:Lcom/navdy/hud/app/obd/ObdManager;

.field private requiredObdDataVersion:I

.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

.field private vin:Ljava/lang/String;

.field private year:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 49
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 70
    new-instance v0, Ljava/io/File;

    const-string v1, "/sdcard/.canBusLogs/upload"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->UPLOAD_DIRECTORY_FILE:Ljava/io/File;

    .line 79
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy_MM_dd_HH_mm_ss_SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 84
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v1, Lcom/navdy/hud/app/util/CrashReportService$FilesModifiedTimeComparator;

    invoke-direct {v1}, Lcom/navdy/hud/app/util/CrashReportService$FilesModifiedTimeComparator;-><init>()V

    invoke-direct {v0, v2, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 85
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v1, Lcom/navdy/hud/app/util/CrashReportService$FilesModifiedTimeComparator;

    invoke-direct {v1}, Lcom/navdy/hud/app/util/CrashReportService$FilesModifiedTimeComparator;-><init>()V

    invoke-direct {v0, v2, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/obd/ObdManager;Lcom/navdy/hud/app/framework/trips/TripManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p3, "obdManager"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p4, "tripManager"    # Lcom/navdy/hud/app/framework/trips/TripManager;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isEngineeringBuild:Z

    .line 57
    iput-wide v2, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->firstTimeMovementDetected:J

    .line 58
    iput-wide v2, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->firstTimeStoppingDetected:J

    .line 71
    iput-boolean v1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isObdConnected:Z

    .line 72
    iput-boolean v1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanProtocol:Z

    .line 73
    iput-boolean v1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isInstantaneousModeOn:Z

    .line 78
    iput-boolean v1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->motionDetected:Z

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->lastSpeed:I

    .line 87
    iput-boolean v1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringLimitReached:Z

    .line 95
    sget-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->UNKNOWN:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    .line 97
    iput v1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringDataSize:I

    .line 100
    iput-object p3, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    .line 101
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->context:Landroid/content/Context;

    .line 102
    iput-object p4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    .line 103
    iput-object p2, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    .line 105
    sget-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->UPLOAD_DIRECTORY_FILE:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    sget-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->UPLOAD_DIRECTORY_FILE:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 108
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->populateFilesQueue()V

    .line 109
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->handler:Landroid/os/Handler;

    .line 111
    return-void

    :cond_1
    move v0, v1

    .line 56
    goto :goto_0
.end method

.method private getMetaDataFile()Ljava/io/File;
    .locals 9

    .prologue
    .line 379
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 380
    .local v4, "time":J
    sget-object v6, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 381
    .local v0, "dateString":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/sdcard/.canBusLogs/upload/meta_data_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".txt"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 382
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 383
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 384
    sget-object v6, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Upload directory not found, creating"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 385
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 387
    :cond_0
    sget-object v6, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Creating Meta data file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 388
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 390
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :cond_1
    :goto_0
    return-object v3

    .line 391
    :catch_0
    move-exception v2

    .line 392
    .local v2, "e":Ljava/io/IOException;
    sget-object v6, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "IOException while creating file "

    invoke-virtual {v6, v7, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private populateFilesQueue()V
    .locals 8

    .prologue
    .line 320
    sget-object v3, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    .line 321
    sget-object v3, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    .line 322
    sget-object v3, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->UPLOAD_DIRECTORY_FILE:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 323
    .local v1, "childrenFiles":[Ljava/io/File;
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v0, v1, v3

    .line 324
    .local v0, "childFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "meta_data"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 325
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 326
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v5

    const/16 v6, 0xa

    if-le v5, v6, :cond_0

    .line 327
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 328
    .local v2, "oldestFile":Ljava/io/File;
    if-eqz v2, :cond_0

    .line 329
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Deleting meta data file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", As its old"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 330
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 323
    .end local v2    # "oldestFile":Ljava/io/File;
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 333
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "CAN_BUS_DATA_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 334
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 335
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v5

    const/4 v6, 0x5

    if-le v5, v6, :cond_0

    .line 336
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 337
    .restart local v2    # "oldestFile":Ljava/io/File;
    if-eqz v2, :cond_0

    .line 338
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Deleting upload file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", As its old"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 339
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    .line 344
    .end local v0    # "childFile":Ljava/io/File;
    .end local v2    # "oldestFile":Ljava/io/File;
    :cond_2
    return-void
.end method

.method private reportBusMonitoringState()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 415
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "canBusDataStatusReportedVersion"

    const/4 v6, -0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 416
    .local v0, "canBusReadStatusReportedVersion":I
    iget v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    if-ne v0, v4, :cond_2

    move v1, v2

    .line 417
    .local v1, "isMonitoringStateReported":Z
    :goto_0
    sget-object v4, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reportBusMonitoringState, Already reported ? : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 418
    if-nez v1, :cond_1

    .line 419
    sget-object v4, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Reporting, Make "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Model : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Year : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", State : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 420
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    .line 421
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    .line 422
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->UNKNOWN:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    if-eq v4, v5, :cond_1

    .line 423
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->vin:Ljava/lang/String;

    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    sget-object v6, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->SUCCESS:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    if-ne v5, v6, :cond_3

    :goto_1
    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    sget-object v6, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->SUCCESS:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    if-ne v5, v6, :cond_0

    iget v3, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringDataSize:I

    :cond_0
    iget v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    invoke-static {v4, v2, v3, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordObdCanBusMonitoringState(Ljava/lang/String;ZII)V

    .line 424
    iget-object v2, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "canBusDataStatusReportedVersion"

    iget v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 427
    :cond_1
    return-void

    .end local v1    # "isMonitoringStateReported":Z
    :cond_2
    move v1, v3

    .line 416
    goto/16 :goto_0

    .restart local v1    # "isMonitoringStateReported":Z
    :cond_3
    move v2, v3

    .line 423
    goto :goto_1
.end method


# virtual methods
.method public isCanBusMonitoringLimitReached()Z
    .locals 1

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringLimitReached:Z

    return v0
.end method

.method public isCanBusMonitoringNeeded()Z
    .locals 14

    .prologue
    const-wide/16 v12, -0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 239
    iget-object v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v10, "canBusDataRecordedAndSent"

    invoke-interface {v9, v10, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 240
    .local v0, "dataRecordedAndSent":Z
    iget-object v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v10, "canBusDataSentVersion"

    const/4 v11, -0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 241
    .local v6, "sentDataVersion":I
    sget-object v9, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IsCanBusMonitoringNeeded : ? Is Engineering Build "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isEngineeringBuild:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Data record sent : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", Sent Data Version : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", isMoving : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->motionDetected:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", InstantaneousMode : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isInstantaneousModeOn:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", Obd Connected : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isObdConnected:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", Is CAN protocol :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanProtocol:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 242
    iget-boolean v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isEngineeringBuild:Z

    if-eqz v9, :cond_2

    if-eqz v0, :cond_0

    iget v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    if-eq v6, v9, :cond_2

    :cond_0
    iget-boolean v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->motionDetected:Z

    if-eqz v9, :cond_2

    iget-boolean v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isInstantaneousModeOn:Z

    if-nez v9, :cond_2

    iget-boolean v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isObdConnected:Z

    if-eqz v9, :cond_2

    iget-boolean v9, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanProtocol:Z

    if-eqz v9, :cond_2

    move v1, v7

    .line 243
    .local v1, "monitoringNeeded":Z
    :goto_0
    if-eqz v1, :cond_4

    .line 244
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "preference_navdy_miles_when_listening_started"

    invoke-interface {v8, v9, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 245
    .local v4, "distanceTravelledWhenMonitoringStarted":J
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    invoke-virtual {v8}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelledWithNavdy()J

    move-result-wide v2

    .line 246
    .local v2, "distanceDrivenSoFar":J
    cmp-long v8, v4, v12

    if-nez v8, :cond_3

    .line 247
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "preference_navdy_miles_when_listening_started"

    invoke-interface {v7, v8, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 255
    .end local v2    # "distanceDrivenSoFar":J
    .end local v4    # "distanceTravelledWhenMonitoringStarted":J
    :cond_1
    :goto_1
    return v1

    .end local v1    # "monitoringNeeded":Z
    :cond_2
    move v1, v8

    .line 242
    goto :goto_0

    .line 248
    .restart local v1    # "monitoringNeeded":Z
    .restart local v2    # "distanceDrivenSoFar":J
    .restart local v4    # "distanceTravelledWhenMonitoringStarted":J
    :cond_3
    sub-long v8, v2, v4

    const-wide/32 v10, 0x9d2a

    cmp-long v8, v8, v10

    if-lez v8, :cond_1

    .line 249
    sget-object v8, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CAN bus monitoring limit reached, Distance recorded when listening started "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Current distance travelled "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 250
    iput-boolean v7, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringLimitReached:Z

    goto :goto_1

    .line 253
    .end local v2    # "distanceDrivenSoFar":J
    .end local v4    # "distanceTravelledWhenMonitoringStarted":J
    :cond_4
    iput-boolean v8, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringLimitReached:Z

    goto :goto_1
.end method

.method public onCanBusMonitorSuccess(I)V
    .locals 1
    .param p1, "approxDataSize"    # I

    .prologue
    .line 399
    sget-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->SUCCESS:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    iput-object v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    .line 400
    iput p1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringDataSize:I

    .line 401
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->reportBusMonitoringState()V

    .line 402
    return-void
.end method

.method public onCanBusMonitoringFailed()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 405
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v4

    int-to-double v2, v4

    .line 406
    .local v2, "speed":D
    iget-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/ObdManager;->getEngineRpm()I

    move-result v4

    int-to-double v0, v4

    .line 407
    .local v0, "rpm":D
    sget-object v4, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCanBusMonitoringFailed , RawSpeed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", RPM : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 408
    cmpl-double v4, v2, v8

    if-gtz v4, :cond_0

    cmpl-double v4, v0, v8

    if-lez v4, :cond_1

    .line 409
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->FAILURE:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    iput-object v4, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    .line 410
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->reportBusMonitoringState()V

    .line 412
    :cond_1
    return-void
.end method

.method public declared-synchronized onCarDetailsAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "model"    # Ljava/lang/String;
    .param p3, "year"    # Ljava/lang/String;

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCarDetailsAvailable Make : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , Model : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Year : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 204
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->reportBusMonitoringState()V

    .line 205
    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    invoke-static {v5, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    invoke-static {v5, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 206
    :cond_0
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Car details changed"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 207
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    .line 208
    iput-object p2, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    .line 209
    iput-object p3, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    .line 210
    iget-boolean v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isObdConnected:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->currentMetaDataFile:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_4

    .line 211
    const/4 v3, 0x0

    .line 212
    .local v3, "fw":Ljava/io/FileWriter;
    const/4 v0, 0x0

    .line 214
    .local v0, "bw":Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v4, Ljava/io/FileWriter;

    iget-object v5, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->currentMetaDataFile:Ljava/io/File;

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 215
    .end local v3    # "fw":Ljava/io/FileWriter;
    .local v4, "fw":Ljava/io/FileWriter;
    :try_start_2
    new-instance v1, Ljava/io/BufferedWriter;

    invoke-direct {v1, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 216
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .local v1, "bw":Ljava/io/BufferedWriter;
    :try_start_3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 217
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Make : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 219
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 220
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Model : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 222
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 223
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Year : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 225
    :cond_3
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 229
    :try_start_4
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    move-object v3, v4

    .line 236
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v4    # "fw":Ljava/io/FileWriter;
    :cond_4
    :goto_0
    monitor-exit p0

    return-void

    .line 226
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v3    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v2

    .line 227
    .local v2, "e":Ljava/io/IOException;
    :goto_1
    :try_start_5
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error writing to the metadata file"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 229
    :try_start_6
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 203
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fw":Ljava/io/FileWriter;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 229
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v3    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v5

    :goto_2
    :try_start_7
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 230
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5

    .line 234
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v3    # "fw":Ljava/io/FileWriter;
    :cond_5
    sget-object v5, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Car details same"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 229
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    :catchall_2
    move-exception v5

    move-object v3, v4

    .end local v4    # "fw":Ljava/io/FileWriter;
    .restart local v3    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v3    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    :catchall_3
    move-exception v5

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    move-object v3, v4

    .end local v4    # "fw":Ljava/io/FileWriter;
    .restart local v3    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 226
    .end local v3    # "fw":Ljava/io/FileWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "fw":Ljava/io/FileWriter;
    .restart local v3    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v3    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v4    # "fw":Ljava/io/FileWriter;
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    move-object v3, v4

    .end local v4    # "fw":Ljava/io/FileWriter;
    .restart local v3    # "fw":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method public onDrivingStateChanged(Lcom/navdy/hud/app/event/DrivingStateChange;)V
    .locals 5
    .param p1, "drivingStateChange"    # Lcom/navdy/hud/app/event/DrivingStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 364
    iget-boolean v2, p1, Lcom/navdy/hud/app/event/DrivingStateChange;->driving:Z

    .line 365
    .local v2, "motionDetected":Z
    iget-boolean v3, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->motionDetected:Z

    if-eq v3, v2, :cond_0

    .line 366
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringNeeded()Z

    move-result v1

    .line 367
    .local v1, "isCanBusMonitoringNeededPrev":Z
    iput-boolean v2, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->motionDetected:Z

    .line 368
    invoke-virtual {p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanBusMonitoringNeeded()Z

    move-result v0

    .line 369
    .local v0, "isCanBusMonitoringNeededCurrent":Z
    if-eq v1, v0, :cond_1

    .line 370
    sget-object v3, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Can BUS monitoring need changed, update listener"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 371
    iget-object v3, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/obd/ObdManager;->updateListener()V

    .line 376
    .end local v0    # "isCanBusMonitoringNeededCurrent":Z
    .end local v1    # "isCanBusMonitoringNeededPrev":Z
    :cond_0
    :goto_0
    return-void

    .line 373
    .restart local v0    # "isCanBusMonitoringNeededCurrent":Z
    .restart local v1    # "isCanBusMonitoringNeededPrev":Z
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Can BUS monitoring need is not changed due to driving state change"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFileUploaded(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 347
    sget-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File successfully uploaded "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "canBusDataRecordedAndSent"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 349
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "canBusDataSentVersion"

    iget v2, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 350
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "preference_navdy_miles_when_listening_started"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 351
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->populateFilesQueue()V

    .line 352
    iget v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordObdCanBusDataSent(Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->updateListener()V

    .line 355
    return-void
.end method

.method public onInstantaneousModeChanged(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 358
    iput-boolean p1, p0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isInstantaneousModeOn:Z

    .line 359
    return-void
.end method

.method public onNewDataAvailable(Ljava/lang/String;)V
    .locals 19
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 259
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onNewDataAvailable , File path : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 260
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 261
    .local v4, "dataFile":Ljava/io/File;
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v15, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    invoke-direct/range {v15 .. v17}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v14, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 262
    .local v5, "dateString":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 263
    new-instance v9, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/sdcard/.canBusLogs/upload"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".log"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 264
    .local v9, "newFile":Ljava/io/File;
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Moving From : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", TO : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 265
    invoke-virtual {v4, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 267
    .end local v9    # "newFile":Ljava/io/File;
    :cond_0
    new-instance v12, Ljava/io/File;

    const-string v14, "/sdcard/.canBusLogs/upload"

    invoke-direct {v12, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 268
    .local v12, "uploadDirectory":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 269
    .local v6, "files":[Ljava/io/File;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 270
    .local v7, "filesToBeBundled":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    if-eqz v6, :cond_a

    .line 271
    array-length v15, v6

    const/4 v14, 0x0

    :goto_0
    if-ge v14, v15, :cond_2

    aget-object v3, v6, v14

    .line 272
    .local v3, "childrenFile":Ljava/io/File;
    sget-object v16, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Child "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 273
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v16

    if-eqz v16, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    const-string v17, ".zip"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 274
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 277
    .end local v3    # "childrenFile":Ljava/io/File;
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    if-lez v14, :cond_9

    .line 278
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    new-array v8, v14, [Ljava/io/File;

    .line 279
    .local v8, "filesToBeBundledArray":[Ljava/io/File;
    invoke-interface {v7, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 280
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 281
    .local v2, "builder":Ljava/lang/StringBuilder;
    const-string v14, "CAN_BUS_DATA_"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 283
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "_"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_4

    .line 286
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "_"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_5

    .line 289
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "_"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->vin:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 292
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->vin:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "_"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_6
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "V("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->requiredObdDataVersion:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".zip"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/sdcard/.canBusLogs/upload"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 297
    .local v13, "uploadFile":Ljava/io/File;
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Compressing the files to "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 298
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v8, v15}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 299
    array-length v15, v8

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v15, :cond_7

    aget-object v11, v8, v14

    .line 300
    .local v11, "temp":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v16

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 299
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 302
    .end local v11    # "temp":Ljava/io/File;
    :cond_7
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v14, v13}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 303
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v14}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v14

    const/4 v15, 0x5

    if-le v14, v15, :cond_8

    .line 304
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->uploadFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v14}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/io/File;

    .line 305
    .local v10, "oldestFile":Ljava/io/File;
    if-eqz v10, :cond_8

    .line 306
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Deleting upload file "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", As its old"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 307
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 310
    .end local v10    # "oldestFile":Ljava/io/File;
    :cond_8
    invoke-static {v13}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->addObdDataFileToQueue(Ljava/io/File;)V

    .line 311
    invoke-static {}, Lcom/navdy/hud/app/service/ObdCANBusDataUploadService;->syncNow()V

    .line 316
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "filesToBeBundledArray":[Ljava/io/File;
    .end local v13    # "uploadFile":Ljava/io/File;
    :cond_9
    :goto_2
    return-void

    .line 314
    :cond_a
    sget-object v14, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "No files found in the upload directory"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public declared-synchronized onObdConnectionStateChanged(Z)V
    .locals 27
    .param p1, "connected"    # Z

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "onObdConnectionStateChanged , Connected : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 115
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isObdConnected:Z

    .line 116
    if-eqz p1, :cond_6

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/obd/ObdManager;->getProtocol()Ljava/lang/String;

    move-result-object v21

    .line 118
    .local v21, "protocol":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_5

    const-string v24, "ISO 15765-4"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 119
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanProtocol:Z

    .line 126
    .end local v21    # "protocol":Ljava/lang/String;
    :goto_0
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->UNKNOWN:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    .line 127
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->canBusMonitoringDataSize:I

    .line 128
    if-eqz p1, :cond_9

    .line 129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 130
    .local v22, "time":J
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual/range {v24 .. v24}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v24

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_0

    .line 131
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual/range {v24 .. v24}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/io/File;

    .line 132
    .local v17, "oldestFile":Ljava/io/File;
    if-eqz v17, :cond_0

    .line 133
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Deleting file "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", As its old"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 134
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v24

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 137
    .end local v17    # "oldestFile":Ljava/io/File;
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->getMetaDataFile()Ljava/io/File;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->currentMetaDataFile:Ljava/io/File;

    .line 138
    const/4 v12, 0x0

    .line 139
    .local v12, "fw":Ljava/io/FileWriter;
    const/4 v4, 0x0

    .line 140
    .local v4, "bw":Ljava/io/BufferedWriter;
    new-instance v16, Lorg/json/JSONObject;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    .local v16, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v13, Ljava/io/FileWriter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->currentMetaDataFile:Ljava/io/File;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v13, v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 143
    .end local v12    # "fw":Ljava/io/FileWriter;
    .local v13, "fw":Ljava/io/FileWriter;
    :try_start_2
    new-instance v5, Ljava/io/BufferedWriter;

    invoke-direct {v5, v13}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 144
    .end local v4    # "bw":Ljava/io/BufferedWriter;
    .local v5, "bw":Ljava/io/BufferedWriter;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/obd/ObdManager;->getCarService()Lcom/navdy/obd/ICarService;

    move-result-object v6

    .line 145
    .local v6, "carService":Lcom/navdy/obd/ICarService;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->vin:Ljava/lang/String;

    .line 146
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v25, Ljava/util/Date;

    move-object/from16 v0, v25

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v24 .. v25}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v7

    .line 148
    .local v7, "dateString":Ljava/lang/String;
    :try_start_4
    const-string v24, "time"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v7}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 149
    const-string v24, "vin"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->vin:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 151
    if-eqz v6, :cond_1

    .line 153
    :try_start_5
    const-string v24, "protocol"

    invoke-interface {v6}, Lcom/navdy/obd/ICarService;->getProtocol()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 154
    invoke-interface {v6}, Lcom/navdy/obd/ICarService;->getEcus()Ljava/util/List;

    move-result-object v11

    .line 155
    .local v11, "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    new-instance v15, Lorg/json/JSONArray;

    invoke-direct {v15}, Lorg/json/JSONArray;-><init>()V

    .line 156
    .local v15, "jsonArray":Lorg/json/JSONArray;
    if-eqz v11, :cond_1

    .line 157
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v14, v0, :cond_8

    .line 158
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 159
    .local v10, "ecuObject":Lorg/json/JSONObject;
    invoke-interface {v11, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/navdy/obd/ECU;

    .line 160
    .local v9, "ecu":Lcom/navdy/obd/ECU;
    const-string v24, "Address "

    iget v0, v9, Lcom/navdy/obd/ECU;->address:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 161
    iget-object v0, v9, Lcom/navdy/obd/ECU;->supportedPids:Lcom/navdy/obd/PidSet;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/obd/PidSet;->asList()Ljava/util/List;

    move-result-object v19

    .line 162
    .local v19, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    new-instance v20, Lorg/json/JSONArray;

    invoke-direct/range {v20 .. v20}, Lorg/json/JSONArray;-><init>()V

    .line 163
    .local v20, "pidsArray":Lorg/json/JSONArray;
    if-eqz v19, :cond_7

    .line 164
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_2
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_7

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/navdy/obd/Pid;

    .line 165
    .local v18, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/obd/Pid;->getId()I

    move-result v25

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_2

    .line 173
    .end local v9    # "ecu":Lcom/navdy/obd/ECU;
    .end local v10    # "ecuObject":Lorg/json/JSONObject;
    .end local v11    # "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .end local v14    # "i":I
    .end local v15    # "jsonArray":Lorg/json/JSONArray;
    .end local v18    # "pid":Lcom/navdy/obd/Pid;
    .end local v19    # "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .end local v20    # "pidsArray":Lorg/json/JSONArray;
    :catch_0
    move-exception v8

    .line 174
    .local v8, "e":Landroid/os/RemoteException;
    :try_start_6
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "Error get the data from obd service"

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 177
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_2

    .line 178
    const-string v24, "Make"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->make:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_3

    .line 181
    const-string v24, "Model"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->model:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 183
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_4

    .line 184
    const-string v24, "Year"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->year:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 189
    :cond_4
    :goto_4
    :try_start_7
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 194
    :try_start_8
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 195
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-object v4, v5

    .end local v5    # "bw":Ljava/io/BufferedWriter;
    .restart local v4    # "bw":Ljava/io/BufferedWriter;
    move-object v12, v13

    .line 200
    .end local v4    # "bw":Ljava/io/BufferedWriter;
    .end local v6    # "carService":Lcom/navdy/obd/ICarService;
    .end local v7    # "dateString":Ljava/lang/String;
    .end local v13    # "fw":Ljava/io/FileWriter;
    .end local v16    # "jsonObject":Lorg/json/JSONObject;
    .end local v22    # "time":J
    :goto_5
    monitor-exit p0

    return-void

    .line 121
    .restart local v21    # "protocol":Ljava/lang/String;
    :cond_5
    const/16 v24, 0x0

    :try_start_9
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanProtocol:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 114
    .end local v21    # "protocol":Ljava/lang/String;
    :catchall_0
    move-exception v24

    monitor-exit p0

    throw v24

    .line 124
    :cond_6
    const/16 v24, 0x0

    :try_start_a
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->isCanProtocol:Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 168
    .restart local v5    # "bw":Ljava/io/BufferedWriter;
    .restart local v6    # "carService":Lcom/navdy/obd/ICarService;
    .restart local v7    # "dateString":Ljava/lang/String;
    .restart local v9    # "ecu":Lcom/navdy/obd/ECU;
    .restart local v10    # "ecuObject":Lorg/json/JSONObject;
    .restart local v11    # "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .restart local v13    # "fw":Ljava/io/FileWriter;
    .restart local v14    # "i":I
    .restart local v15    # "jsonArray":Lorg/json/JSONArray;
    .restart local v16    # "jsonObject":Lorg/json/JSONObject;
    .restart local v19    # "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .restart local v20    # "pidsArray":Lorg/json/JSONArray;
    .restart local v22    # "time":J
    :cond_7
    :try_start_b
    const-string v24, "PIDs"

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 169
    invoke-virtual {v15, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 157
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    .line 171
    .end local v9    # "ecu":Lcom/navdy/obd/ECU;
    .end local v10    # "ecuObject":Lorg/json/JSONObject;
    .end local v19    # "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .end local v20    # "pidsArray":Lorg/json/JSONArray;
    :cond_8
    const-string v24, "ecus"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v15}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto/16 :goto_3

    .line 186
    .end local v11    # "ecus":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/ECU;>;"
    .end local v14    # "i":I
    .end local v15    # "jsonArray":Lorg/json/JSONArray;
    :catch_1
    move-exception v8

    .line 187
    .local v8, "e":Lorg/json/JSONException;
    :try_start_c
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "Error writing meta data JSON "

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto :goto_4

    .line 191
    .end local v6    # "carService":Lcom/navdy/obd/ICarService;
    .end local v7    # "dateString":Ljava/lang/String;
    .end local v8    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v8

    move-object v4, v5

    .end local v5    # "bw":Ljava/io/BufferedWriter;
    .restart local v4    # "bw":Ljava/io/BufferedWriter;
    move-object v12, v13

    .line 192
    .end local v13    # "fw":Ljava/io/FileWriter;
    .local v8, "e":Ljava/io/IOException;
    .restart local v12    # "fw":Ljava/io/FileWriter;
    :goto_6
    :try_start_d
    sget-object v24, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "Error writing to the metadata file"

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 194
    :try_start_e
    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 195
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_5

    .line 194
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v24

    :goto_7
    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 195
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v24

    .line 198
    .end local v4    # "bw":Ljava/io/BufferedWriter;
    .end local v12    # "fw":Ljava/io/FileWriter;
    .end local v16    # "jsonObject":Lorg/json/JSONObject;
    .end local v22    # "time":J
    :cond_9
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;->currentMetaDataFile:Ljava/io/File;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_5

    .line 194
    .restart local v4    # "bw":Ljava/io/BufferedWriter;
    .restart local v13    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "jsonObject":Lorg/json/JSONObject;
    .restart local v22    # "time":J
    :catchall_2
    move-exception v24

    move-object v12, v13

    .end local v13    # "fw":Ljava/io/FileWriter;
    .restart local v12    # "fw":Ljava/io/FileWriter;
    goto :goto_7

    .end local v4    # "bw":Ljava/io/BufferedWriter;
    .end local v12    # "fw":Ljava/io/FileWriter;
    .restart local v5    # "bw":Ljava/io/BufferedWriter;
    .restart local v13    # "fw":Ljava/io/FileWriter;
    :catchall_3
    move-exception v24

    move-object v4, v5

    .end local v5    # "bw":Ljava/io/BufferedWriter;
    .restart local v4    # "bw":Ljava/io/BufferedWriter;
    move-object v12, v13

    .end local v13    # "fw":Ljava/io/FileWriter;
    .restart local v12    # "fw":Ljava/io/FileWriter;
    goto :goto_7

    .line 191
    :catch_3
    move-exception v8

    goto :goto_6

    .end local v12    # "fw":Ljava/io/FileWriter;
    .restart local v13    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v8

    move-object v12, v13

    .end local v13    # "fw":Ljava/io/FileWriter;
    .restart local v12    # "fw":Ljava/io/FileWriter;
    goto :goto_6
.end method
