.class public Lcom/navdy/hud/app/obd/CarMDVinDecoder;
.super Ljava/lang/Object;
.source "CarMDVinDecoder.java"


# static fields
.field public static final APP_VERSION:I = 0x1

.field private static final CAR_MD_API_URL:Lokhttp3/HttpUrl;

.field private static final DATA:Ljava/lang/String; = "data"

.field public static final MAX_SIZE:J = 0x1388L

.field private static final META_CAR_MD_AUTH:Ljava/lang/String; = "CAR_MD_AUTH"

.field private static final META_CAR_MD_TOKEN:Ljava/lang/String; = "CAR_MD_TOKEN"

.field public static final VIN:Ljava/lang/String; = "vin"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final CAR_MD_AUTH:Ljava/lang/String;

.field private final CAR_MD_TOKEN:Ljava/lang/String;

.field private volatile mDecoding:Z

.field private mDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

.field private mDiskLruCache:Lokhttp3/internal/DiskLruCache;

.field private mHttpClient:Lokhttp3/OkHttpClient;

.field mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-string v0, "https://api2.carmd.com/v2.0/decode"

    invoke-static {v0}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_API_URL:Lokhttp3/HttpUrl;

    .line 60
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;)V
    .locals 8
    .param p1, "obdDeviceConfigurationManager"    # Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .prologue
    const-wide/16 v6, 0x78

    const/4 v2, 0x1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z

    .line 65
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 66
    sget-object v0, Lokhttp3/internal/io/FileSystem;->SYSTEM:Lokhttp3/internal/io/FileSystem;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/PathManager;->getCarMdResponseDiskCacheFolder()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x1388

    move v3, v2

    invoke-static/range {v0 .. v5}, Lokhttp3/internal/DiskLruCache;->create(Lokhttp3/internal/io/FileSystem;Ljava/io/File;IIJ)Lokhttp3/internal/DiskLruCache;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDiskLruCache:Lokhttp3/internal/DiskLruCache;

    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    invoke-interface {v0}, Lcom/navdy/service/library/network/http/IHttpManager;->getClientCopy()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v1}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mHttpClient:Lokhttp3/OkHttpClient;

    .line 69
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "CAR_MD_AUTH"

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_AUTH:Ljava/lang/String;

    .line 70
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "CAR_MD_TOKEN"

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_TOKEN:Ljava/lang/String;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->getFromCache(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->isConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500()Lokhttp3/HttpUrl;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_API_URL:Lokhttp3/HttpUrl;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_AUTH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->CAR_MD_TOKEN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Lokhttp3/ResponseBody;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;
    .param p1, "x1"    # Lokhttp3/ResponseBody;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->parseResponse(Lokhttp3/ResponseBody;)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/obd/CarMDVinDecoder;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/obd/CarMDVinDecoder;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mHttpClient:Lokhttp3/OkHttpClient;

    return-object v0
.end method

.method private getFromCache(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "vinNumber"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 165
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDiskLruCache:Lokhttp3/internal/DiskLruCache;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lokhttp3/internal/DiskLruCache;->get(Ljava/lang/String;)Lokhttp3/internal/DiskLruCache$Snapshot;

    move-result-object v3

    .line 166
    .local v3, "snapShot":Lokhttp3/internal/DiskLruCache$Snapshot;
    if-nez v3, :cond_0

    move-object v1, v5

    .line 177
    .end local v3    # "snapShot":Lokhttp3/internal/DiskLruCache$Snapshot;
    :goto_0
    return-object v1

    .line 169
    .restart local v3    # "snapShot":Lokhttp3/internal/DiskLruCache$Snapshot;
    :cond_0
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lokhttp3/internal/DiskLruCache$Snapshot;->getSource(I)Lokio/Source;

    move-result-object v4

    .line 170
    .local v4, "source":Lokio/Source;
    invoke-static {v4}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    move-result-object v0

    .line 171
    .local v0, "bufferedSource":Lokio/BufferedSource;
    invoke-interface {v0}, Lokio/BufferedSource;->readUtf8()Ljava/lang/String;

    move-result-object v1

    .line 172
    .local v1, "dataRead":Ljava/lang/String;
    invoke-interface {v0}, Lokio/BufferedSource;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 174
    .end local v0    # "bufferedSource":Lokio/BufferedSource;
    .end local v1    # "dataRead":Ljava/lang/String;
    .end local v3    # "snapShot":Lokhttp3/internal/DiskLruCache$Snapshot;
    .end local v4    # "source":Lokio/Source;
    :catch_0
    move-exception v2

    .line 175
    .local v2, "e":Ljava/io/IOException;
    sget-object v6, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Exception while retrieving response from cache"

    invoke-virtual {v6, v7, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v5

    .line 177
    goto :goto_0
.end method

.method private isConnected()Z
    .locals 4

    .prologue
    .line 195
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    .line 196
    .local v2, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v0

    .line 197
    .local v0, "deviceConnected":Z
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    .line 198
    .local v1, "internetConnected":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private parseResponse(Lokhttp3/ResponseBody;)V
    .locals 8
    .param p1, "responseBody"    # Lokhttp3/ResponseBody;

    .prologue
    .line 140
    if-eqz p1, :cond_0

    .line 142
    :try_start_0
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v4

    .line 143
    .local v4, "jsonDataString":Ljava/lang/String;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 144
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "data"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 145
    .local v0, "dataObject":Lorg/json/JSONObject;
    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "dataString":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/obd/CarDetails;->fromJson(Ljava/lang/String;)Lcom/navdy/hud/app/obd/CarDetails;

    move-result-object v2

    .line 148
    .local v2, "details":Lcom/navdy/hud/app/obd/CarDetails;
    if-eqz v2, :cond_1

    .line 149
    iget-object v6, v2, Lcom/navdy/hud/app/obd/CarDetails;->vin:Ljava/lang/String;

    invoke-direct {p0, v6, v1}, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->putToCache(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v6, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDeviceConfigurationManager:Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    invoke-virtual {v6, v2}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->setDecodedCarDetails(Lcom/navdy/hud/app/obd/CarDetails;)V

    .line 161
    .end local v0    # "dataObject":Lorg/json/JSONObject;
    .end local v1    # "dataString":Ljava/lang/String;
    .end local v2    # "details":Lcom/navdy/hud/app/obd/CarDetails;
    .end local v4    # "jsonDataString":Ljava/lang/String;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 152
    .restart local v0    # "dataObject":Lorg/json/JSONObject;
    .restart local v1    # "dataString":Ljava/lang/String;
    .restart local v2    # "details":Lcom/navdy/hud/app/obd/CarDetails;
    .restart local v4    # "jsonDataString":Ljava/lang/String;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    sget-object v6, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Error parsing the CarMD response to car details"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 155
    .end local v0    # "dataObject":Lorg/json/JSONObject;
    .end local v1    # "dataString":Ljava/lang/String;
    .end local v2    # "details":Lcom/navdy/hud/app/obd/CarDetails;
    .end local v4    # "jsonDataString":Ljava/lang/String;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 156
    .local v3, "e":Lorg/json/JSONException;
    sget-object v6, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Error parsing the response body "

    invoke-virtual {v6, v7, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 157
    .end local v3    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v3

    .line 158
    .local v3, "e":Ljava/io/IOException;
    sget-object v6, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "IO Error parsing the response body "

    invoke-virtual {v6, v7, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private putToCache(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "vinNumber"    # Ljava/lang/String;
    .param p2, "dataString"    # Ljava/lang/String;

    .prologue
    .line 183
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDiskLruCache:Lokhttp3/internal/DiskLruCache;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lokhttp3/internal/DiskLruCache;->edit(Ljava/lang/String;)Lokhttp3/internal/DiskLruCache$Editor;

    move-result-object v2

    .line 184
    .local v2, "editor":Lokhttp3/internal/DiskLruCache$Editor;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lokhttp3/internal/DiskLruCache$Editor;->newSink(I)Lokio/Sink;

    move-result-object v3

    .line 185
    .local v3, "sink":Lokio/Sink;
    invoke-static {v3}, Lokio/Okio;->buffer(Lokio/Sink;)Lokio/BufferedSink;

    move-result-object v0

    .line 186
    .local v0, "bufferedSink":Lokio/BufferedSink;
    invoke-interface {v0, p2}, Lokio/BufferedSink;->writeUtf8(Ljava/lang/String;)Lokio/BufferedSink;

    .line 187
    invoke-interface {v0}, Lokio/BufferedSink;->close()V

    .line 188
    invoke-virtual {v2}, Lokhttp3/internal/DiskLruCache$Editor;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    .end local v0    # "bufferedSink":Lokio/BufferedSink;
    .end local v2    # "editor":Lokhttp3/internal/DiskLruCache$Editor;
    .end local v3    # "sink":Lokio/Sink;
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v1

    .line 190
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Exception while putting into cache "

    invoke-virtual {v4, v5, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized decodeVin(Ljava/lang/String;)V
    .locals 3
    .param p1, "vinNumber"    # Ljava/lang/String;

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/obd/CarMDVinDecoder;->mDecoding:Z

    .line 76
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/obd/CarMDVinDecoder$1;-><init>(Lcom/navdy/hud/app/obd/CarMDVinDecoder;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_0
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
