.class Lcom/navdy/hud/app/obd/ObdManager$1;
.super Lcom/navdy/hud/app/obd/ObdManager$PidCheck;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/obd/ObdManager;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/obd/ObdManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/ObdManager;ILandroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/ObdManager;
    .param p2, "pid"    # I
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager$1;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;-><init>(Lcom/navdy/hud/app/obd/ObdManager;ILandroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public getWaitForValidDataTimeout()J
    .locals 2

    .prologue
    .line 287
    const-wide/32 v0, 0xafc8

    return-wide v0
.end method

.method public invalidatePid(I)V
    .locals 3
    .param p1, "pid"    # I

    .prologue
    const/16 v2, 0x2f

    .line 277
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$1;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$000(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$1;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$100(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/obd/PidSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$1;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;
    invoke-static {v0}, Lcom/navdy/hud/app/obd/ObdManager;->access$100(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/obd/PidSet;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/navdy/obd/PidSet;->remove(I)V

    .line 280
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$1;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v0, v0, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 282
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordIncorrectFuelData()V

    .line 283
    return-void
.end method

.method public isPidValueValid(D)Z
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 272
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
