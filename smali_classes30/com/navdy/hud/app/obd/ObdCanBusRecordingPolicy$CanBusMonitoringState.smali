.class final enum Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;
.super Ljava/lang/Enum;
.source "ObdCanBusRecordingPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CanBusMonitoringState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

.field public static final enum FAILURE:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

.field public static final enum SUCCESS:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

.field public static final enum UNKNOWN:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 91
    new-instance v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->UNKNOWN:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    .line 92
    new-instance v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->SUCCESS:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    .line 93
    new-instance v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->FAILURE:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    .line 90
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    sget-object v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->UNKNOWN:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->SUCCESS:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->FAILURE:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->$VALUES:[Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 90
    const-class v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->$VALUES:[Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;

    return-object v0
.end method
