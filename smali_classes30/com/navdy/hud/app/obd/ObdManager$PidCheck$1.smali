.class Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;
.super Ljava/lang/Object;
.source "ObdManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/obd/ObdManager$PidCheck;-><init>(Lcom/navdy/hud/app/obd/ObdManager;ILandroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

.field final synthetic val$pid:I

.field final synthetic val$this$0:Lcom/navdy/hud/app/obd/ObdManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/ObdManager$PidCheck;Lcom/navdy/hud/app/obd/ObdManager;I)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    .prologue
    .line 1107
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->this$1:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    iput-object p2, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->val$this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iput p3, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->val$pid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->this$1:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    iget-boolean v0, v0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData:Z

    if-nez v0, :cond_0

    .line 1111
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->this$1:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->waitingForValidData:Z

    .line 1112
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->this$1:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData:Z

    .line 1113
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->this$1:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    iget v1, p0, Lcom/navdy/hud/app/obd/ObdManager$PidCheck$1;->val$pid:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->invalidatePid(I)V

    .line 1115
    :cond_0
    return-void
.end method
