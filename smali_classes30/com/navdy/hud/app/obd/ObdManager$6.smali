.class Lcom/navdy/hud/app/obd/ObdManager$6;
.super Lcom/navdy/obd/IPidListener$Stub;
.source "ObdManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/obd/ObdManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/obd/ObdManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/obd/ObdManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/obd/ObdManager;

    .prologue
    .line 518
    iput-object p1, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-direct {p0}, Lcom/navdy/obd/IPidListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionStateChange(I)V
    .locals 1
    .param p1, "newState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 605
    iget-object v0, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # invokes: Lcom/navdy/hud/app/obd/ObdManager;->updateConnectionState(I)V
    invoke-static {v0, p1}, Lcom/navdy/hud/app/obd/ObdManager;->access$2200(Lcom/navdy/hud/app/obd/ObdManager;I)V

    .line 606
    return-void
.end method

.method public pidsChanged(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 601
    .local p1, "pids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    return-void
.end method

.method public declared-synchronized pidsRead(Ljava/util/List;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/obd/Pid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "pidsRead":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    .local p2, "pidsChanged":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$1200(Lcom/navdy/hud/app/obd/ObdManager;)Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz p2, :cond_0

    .line 523
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->firstScan:Z
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$1202(Lcom/navdy/hud/app/obd/ObdManager;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    :try_start_1
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$1300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/CarServiceConnector;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v0

    .line 526
    .local v0, "api":Lcom/navdy/obd/ICarService;
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getVIN()Ljava/lang/String;

    move-result-object v8

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$1402(Lcom/navdy/hud/app/obd/ObdManager;Ljava/lang/String;)Ljava/lang/String;

    .line 527
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$1400(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/navdy/hud/app/obd/ObdManager;->onVinRead(Ljava/lang/String;)V
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$1500(Lcom/navdy/hud/app/obd/ObdManager;Ljava/lang/String;)V

    .line 528
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getEcus()Ljava/util/List;

    move-result-object v8

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->ecus:Ljava/util/List;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$1602(Lcom/navdy/hud/app/obd/ObdManager;Ljava/util/List;)Ljava/util/List;

    .line 529
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-interface {v0}, Lcom/navdy/obd/ICarService;->getProtocol()Ljava/lang/String;

    move-result-object v8

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->protocol:Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$1702(Lcom/navdy/hud/app/obd/ObdManager;Ljava/lang/String;)Ljava/lang/String;

    .line 530
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->carServiceConnector:Lcom/navdy/hud/app/obd/CarServiceConnector;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$1300(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/obd/CarServiceConnector;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/CarServiceConnector;->getCarApi()Lcom/navdy/obd/ICarService;

    move-result-object v7

    invoke-interface {v7}, Lcom/navdy/obd/ICarService;->getSupportedPids()Ljava/util/List;

    move-result-object v4

    .line 531
    .local v4, "supportedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "First scan, got the supported PIDS"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 532
    if-eqz v4, :cond_1

    .line 533
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Supported PIDS size :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 537
    :goto_0
    if-eqz v4, :cond_0

    .line 538
    new-instance v3, Lcom/navdy/obd/PidSet;

    invoke-direct {v3, v4}, Lcom/navdy/obd/PidSet;-><init>(Ljava/util/List;)V

    .line 539
    .local v3, "pidSet":Lcom/navdy/obd/PidSet;
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$100(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/obd/PidSet;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/navdy/obd/PidSet;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 540
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sTelemetryLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$1800()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SUPPORTED VIN: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$1400(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->vin:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$1400(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/lang/String;

    move-result-object v7

    :goto_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "FUEL: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v9, 0x2f

    .line 541
    invoke-virtual {v3, v9}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "SPEED: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v9, 0xd

    .line 542
    invoke-virtual {v3, v9}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "RPM: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v9, 0xc

    .line 543
    invoke-virtual {v3, v9}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "DIST: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v9, 0x31

    .line 544
    invoke-virtual {v3, v9}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "MAF: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v9, 0x10

    .line 545
    invoke-virtual {v3, v9}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "IFC(LPKKM): "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v9, 0x100

    .line 546
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 540
    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 547
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # setter for: Lcom/navdy/hud/app/obd/ObdManager;->supportedPidSet:Lcom/navdy/obd/PidSet;
    invoke-static {v7, v3}, Lcom/navdy/hud/app/obd/ObdManager;->access$102(Lcom/navdy/hud/app/obd/ObdManager;Lcom/navdy/obd/PidSet;)Lcom/navdy/obd/PidSet;

    .line 548
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, v7, Lcom/navdy/hud/app/obd/ObdManager;->fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->reset()V

    .line 549
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, v7, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;

    invoke-direct {v8}, Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;-><init>()V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 550
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "pid-changed event sent"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 560
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    .end local v3    # "pidSet":Lcom/navdy/obd/PidSet;
    .end local v4    # "supportedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_0
    :goto_2
    if-nez p2, :cond_4

    .line 561
    :try_start_2
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, v7, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/obd/ObdManager$ObdPidReadEvent;

    invoke-direct {v8, p1}, Lcom/navdy/hud/app/obd/ObdManager$ObdPidReadEvent;-><init>(Ljava/util/List;)V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 596
    :goto_3
    monitor-exit p0

    return-void

    .line 535
    .restart local v0    # "api":Lcom/navdy/obd/ICarService;
    .restart local v4    # "supportedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_1
    :try_start_3
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "First scan, got the supported PIDS , list null"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 555
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    .end local v4    # "supportedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :catch_0
    move-exception v5

    .line 556
    .local v5, "t":Ljava/lang/Throwable;
    :try_start_4
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 522
    .end local v5    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 540
    .restart local v0    # "api":Lcom/navdy/obd/ICarService;
    .restart local v3    # "pidSet":Lcom/navdy/obd/PidSet;
    .restart local v4    # "supportedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_2
    :try_start_5
    const-string v7, ""

    goto/16 :goto_1

    .line 552
    :cond_3
    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "pid-changed event not sent"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 565
    .end local v0    # "api":Lcom/navdy/obd/ICarService;
    .end local v3    # "pidSet":Lcom/navdy/obd/PidSet;
    .end local v4    # "supportedPids":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/obd/Pid;>;"
    :cond_4
    :try_start_6
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/obd/Pid;

    .line 566
    .local v2, "pid":Lcom/navdy/obd/Pid;
    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getId()I

    move-result v1

    .line 567
    .local v1, "id":I
    sparse-switch v1, :sswitch_data_0

    .line 585
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$000(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 588
    :goto_5
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v8, v8, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v9, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;

    invoke-direct {v9, v2}, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;-><init>(Lcom/navdy/obd/Pid;)V

    invoke-virtual {v8, v9}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_4

    .line 569
    :sswitch_0
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$1900(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v8

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v10

    double-to-int v9, v10

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getTimeStamp()J

    move-result-wide v10

    invoke-virtual {v8, v9, v10, v11}, Lcom/navdy/hud/app/manager/SpeedManager;->setObdSpeed(IJ)V

    goto :goto_5

    .line 572
    :sswitch_1
    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v8

    double-to-int v6, v8

    .line 573
    .local v6, "value":I
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v8, v8, Lcom/navdy/hud/app/obd/ObdManager;->fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    int-to-double v10, v6

    invoke-virtual {v8, v10, v11}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->checkPid(D)V

    .line 574
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v8, v8, Lcom/navdy/hud/app/obd/ObdManager;->fuelPidCheck:Lcom/navdy/hud/app/obd/ObdManager$PidCheck;

    invoke-virtual {v8}, Lcom/navdy/hud/app/obd/ObdManager$PidCheck;->hasIncorrectData()Z

    move-result v8

    if-nez v8, :cond_5

    .line 575
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$000(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 577
    :cond_5
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$000(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/util/HashMap;

    move-result-object v8

    const/16 v9, 0x2f

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 581
    .end local v6    # "value":I
    :sswitch_2
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->obdPidsCache:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/navdy/hud/app/obd/ObdManager;->access$000(Lcom/navdy/hud/app/obd/ObdManager;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    iget-object v8, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v10

    double-to-long v10, v10

    # invokes: Lcom/navdy/hud/app/obd/ObdManager;->updateCumulativeVehicleDistanceTravelled(J)V
    invoke-static {v8, v10, v11}, Lcom/navdy/hud/app/obd/ObdManager;->access$2000(Lcom/navdy/hud/app/obd/ObdManager;J)V

    goto/16 :goto_5

    .line 590
    .end local v1    # "id":I
    .end local v2    # "pid":Lcom/navdy/obd/Pid;
    :cond_6
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->recordingPidListener:Lcom/navdy/obd/IPidListener;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$2100(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/obd/IPidListener;

    move-result-object v7

    if-eqz v7, :cond_7

    if-eqz p2, :cond_7

    .line 591
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    # getter for: Lcom/navdy/hud/app/obd/ObdManager;->recordingPidListener:Lcom/navdy/obd/IPidListener;
    invoke-static {v7}, Lcom/navdy/hud/app/obd/ObdManager;->access$2100(Lcom/navdy/hud/app/obd/ObdManager;)Lcom/navdy/obd/IPidListener;

    move-result-object v7

    invoke-interface {v7, p1, p2}, Lcom/navdy/obd/IPidListener;->pidsRead(Ljava/util/List;Ljava/util/List;)V

    .line 595
    :cond_7
    iget-object v7, p0, Lcom/navdy/hud/app/obd/ObdManager$6;->this$0:Lcom/navdy/hud/app/obd/ObdManager;

    iget-object v7, v7, Lcom/navdy/hud/app/obd/ObdManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/obd/ObdManager$ObdPidReadEvent;

    invoke-direct {v8, p1}, Lcom/navdy/hud/app/obd/ObdManager$ObdPidReadEvent;-><init>(Ljava/util/List;)V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    .line 567
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x2f -> :sswitch_1
        0x31 -> :sswitch_2
    .end sparse-switch
.end method
