.class Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$3;
.super Ljava/lang/Object;
.source "ShrinkingBorderView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$3;->this$0:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 66
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$3;->this$0:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 67
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 69
    .local v1, "val":I
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    if-ne v2, v1, :cond_0

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 74
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$3;->this$0:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->requestLayout()V

    goto :goto_0
.end method
