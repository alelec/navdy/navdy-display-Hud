.class Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "ChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 293
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1300(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 294
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1400(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    if-ne v2, v3, :cond_4

    .line 295
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1300(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textDeselectionColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1500()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 301
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 303
    .local v0, "pos":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 305
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 306
    .local v1, "tag":Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 307
    check-cast v0, Ljava/lang/Integer;

    .line 309
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1400(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    if-ne v2, v3, :cond_5

    .line 310
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1000()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 316
    .end local v1    # "tag":Ljava/lang/Object;
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$602(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 318
    if-eqz v0, :cond_3

    .line 319
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    # invokes: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->callSelectListener(I)V
    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1600(Lcom/navdy/hud/app/ui/component/ChoiceLayout;I)V

    .line 322
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # invokes: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runQueuedOperation()V
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$000(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    .line 323
    return-void

    .line 297
    .end local v0    # "pos":Ljava/lang/Integer;
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1300(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textDeselectionColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1500()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_0

    .line 312
    .restart local v0    # "pos":Ljava/lang/Integer;
    .restart local v1    # "tag":Ljava/lang/Object;
    :cond_5
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1000()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1
.end method
