.class Lcom/navdy/hud/app/ui/component/CarouselTextView$2;
.super Ljava/lang/Object;
.source "CarouselTextView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/CarouselTextView;->rotateWithAnimation(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

.field final synthetic val$text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/CarouselTextView;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/CarouselTextView;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$2;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$2;->val$text:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$2;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$2;->val$text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$2;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 72
    return-void
.end method
