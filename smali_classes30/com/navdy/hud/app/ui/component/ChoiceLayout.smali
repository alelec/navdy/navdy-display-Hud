.class public Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.super Landroid/widget/FrameLayout;
.source "ChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;,
        Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;,
        Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;,
        Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;,
        Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
    }
.end annotation


# static fields
.field private static final ANIMATION_EXECUTE_DURATION:I = 0xc8

.field private static final ANIMATION_SLIDE_DURATION:I = 0x64

.field private static final ANIMATION_TAG:Ljava/lang/Object;

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final KEY_UP_DOWN_DURATION:I = 0x32

.field private static MIN_HIGHLIGHT_VIEW_WIDTH:I

.field private static choiceHighlightColor:I

.field private static choiceHighlightMargin:I

.field private static choiceTextPaddingBottomDefault:I

.field private static choiceTextPaddingDefault:I

.field private static choiceTextSizeDefault:I

.field private static textDeselectionColor:I

.field private static textSelectionColor:I


# instance fields
.field private animationLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private animatorSet:Landroid/animation/AnimatorSet;

.field protected choiceContainer:Landroid/widget/LinearLayout;

.field private choiceTextPaddingBottom:I

.field private choiceTextPaddingLeft:I

.field private choiceTextPaddingRight:I

.field private choiceTextPaddingTop:I

.field private choiceTextSize:I

.field protected choices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

.field private currentSelectionView:Landroid/view/View;

.field private executeAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private executeUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private executeView:Landroid/view/View;

.field private executing:Z

.field private handler:Landroid/os/Handler;

.field private highlightPersistent:Z

.field protected highlightView:Landroid/view/View;

.field private highlightViewListener:Landroid/animation/Animator$AnimatorListener;

.field private highlightVisibility:I

.field private initialized:Z

.field private listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field private oldSelectionView:Landroid/view/View;

.field private operationQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;",
            ">;"
        }
    .end annotation
.end field

.field private operationRunning:Z

.field private pressedDown:Z

.field private selectedItem:I

.field private upDownAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private upDownUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->ANIMATION_TAG:Ljava/lang/Object;

    .line 51
    const/4 v0, -0x1

    sput v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 328
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 329
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 332
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 334
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    .line 337
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightPersistent:Z

    .line 156
    iput v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    .line 160
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 171
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    .line 183
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->handler:Landroid/os/Handler;

    .line 185
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationQueue:Ljava/util/Queue;

    .line 188
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$1;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->upDownAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 200
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$2;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->upDownUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 211
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 264
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 278
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animationLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 290
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$6;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightViewListener:Landroid/animation/Animator$AnimatorListener;

    .line 338
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->init()V

    .line 339
    sget-object v1, Lcom/navdy/hud/app/R$styleable;->ChoiceLayout:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 340
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 341
    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingDefault:I

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingLeft:I

    .line 342
    const/4 v1, 0x1

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingDefault:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingRight:I

    .line 343
    const/4 v1, 0x3

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingBottomDefault:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingBottom:I

    .line 344
    const/4 v1, 0x2

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingDefault:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingTop:I

    .line 345
    const/4 v1, 0x4

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextSizeDefault:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextSize:I

    .line 346
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 348
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runQueuedOperation()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1000()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    return v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/ui/component/ChoiceLayout;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectInitialItem(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    return-object v0
.end method

.method static synthetic access$1500()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textDeselectionColor:I

    return v0
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/ui/component/ChoiceLayout;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->callSelectListener(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;
    .param p2, "x2"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runOperation(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightColor:I

    return v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .param p1, "x1"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    return p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    return v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/ChoiceLayout;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->callListener(I)V

    return-void
.end method

.method private buildChoice(Ljava/lang/String;I)Landroid/widget/TextView;
    .locals 4
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 562
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 563
    .local v0, "textView":Landroid/widget/TextView;
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 564
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 565
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 566
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 567
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 568
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 569
    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textDeselectionColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 570
    return-object v0
.end method

.method private buildChoices()V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v9, -0x2

    .line 404
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 405
    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    .line 406
    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;

    .line 407
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 409
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    if-eqz v7, :cond_1

    .line 410
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    .line 411
    .local v3, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 412
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    .line 414
    .local v0, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    sget-object v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    if-ne v7, v8, :cond_0

    .line 415
    const-string v7, ""

    invoke-direct {p0, v7, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->buildChoice(Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v5

    .line 416
    .local v5, "textView":Landroid/widget/TextView;
    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->label:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    move-object v6, v5

    .line 425
    .end local v5    # "textView":Landroid/widget/TextView;
    .local v6, "view":Landroid/view/View;
    :goto_1
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 426
    .local v4, "lytParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingLeft:I

    iget v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingRight:I

    invoke-virtual {v4, v7, v10, v8, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 427
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v6, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 411
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 419
    .end local v4    # "lytParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "view":Landroid/view/View;
    :cond_0
    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v2, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 420
    .local v2, "imageView":Landroid/widget/ImageView;
    sget-object v7, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 421
    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    iget v7, v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdUnSelected:I

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 422
    sget v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textDeselectionColor:I

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 423
    move-object v6, v2

    .restart local v6    # "view":Landroid/view/View;
    goto :goto_1

    .line 430
    .end local v0    # "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    .end local v1    # "i":I
    .end local v2    # "imageView":Landroid/widget/ImageView;
    .end local v3    # "len":I
    .end local v6    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->invalidate()V

    .line 431
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->requestLayout()V

    .line 432
    return-void
.end method

.method private callListener(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 582
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    if-eqz v0, :cond_0

    .line 583
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_SELECT:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 584
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 585
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    invoke-interface {v1, p1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;->executeItem(II)V

    .line 588
    :cond_0
    return-void
.end method

.method private callSelectListener(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 591
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 593
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    invoke-interface {v1, p1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;->itemSelected(II)V

    .line 596
    :cond_0
    return-void
.end method

.method private executeSelectedItemInternal()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 435
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->initialized:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    .line 462
    :goto_0
    return v1

    .line 438
    :cond_1
    iget v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getItemCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 439
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    .line 440
    iget v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    if-nez v3, :cond_4

    .line 441
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    .line 442
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    if-nez v3, :cond_3

    .line 443
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->callListener(I)V

    .line 444
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    :cond_2
    :goto_1
    move v1, v2

    .line 462
    goto :goto_0

    .line 446
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    sget-object v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->ANIMATION_TAG:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 447
    const/4 v3, 0x2

    new-array v3, v3, [I

    sget v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I

    aput v4, v3, v2

    aput v2, v3, v1

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 448
    .local v0, "valueAnimator":Landroid/animation/ValueAnimator;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 449
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 450
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    .line 451
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 452
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 453
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 457
    .end local v0    # "valueAnimator":Landroid/animation/ValueAnimator;
    :cond_4
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->callListener(I)V

    .line 458
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    goto :goto_1
.end method

.method private getItemCount()I
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 577
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getXPositionForHighlight(Landroid/view/View;)F
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 646
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v1, v2, v3

    .line 647
    .local v1, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    add-int v0, v2, v3

    .line 648
    .local v0, "left":I
    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->MIN_HIGHLIGHT_VIEW_WIDTH:I

    if-lt v1, v2, :cond_0

    .line 649
    int-to-float v2, v0

    .line 651
    :goto_0
    return v2

    :cond_0
    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->MIN_HIGHLIGHT_VIEW_WIDTH:I

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    int-to-float v2, v2

    goto :goto_0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 356
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000b

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 357
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->invalidate()V

    .line 358
    const v1, 0x7f0e00c9

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    .line 359
    const v1, 0x7f0e00ca

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    .line 360
    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 361
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 362
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b00dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->MIN_HIGHLIGHT_VIEW_WIDTH:I

    .line 363
    const v1, 0x7f0d000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    .line 364
    const v1, 0x7f0d000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textDeselectionColor:I

    .line 365
    const v1, 0x7f0b0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingDefault:I

    .line 366
    const v1, 0x7f0b0057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingBottomDefault:I

    .line 367
    const v1, 0x7f0b0058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextSizeDefault:I

    .line 368
    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightColor:I

    .line 369
    const v1, 0x7f0b0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I

    .line 371
    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method

.method private keyDownSelectedItemInternal()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 486
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->initialized:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->pressedDown:Z

    if-eqz v2, :cond_1

    :cond_0
    move v2, v4

    .line 510
    :goto_0
    return v2

    .line 489
    :cond_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    const/4 v5, -0x1

    if-eq v2, v5, :cond_3

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getItemCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 490
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    sget-object v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    if-ne v2, v5, :cond_2

    .line 491
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    check-cast v2, Landroid/widget/TextView;

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightColor:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 495
    :goto_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    if-nez v2, :cond_3

    .line 496
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->pressedDown:Z

    .line 497
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 498
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 499
    const/4 v2, 0x2

    new-array v2, v2, [I

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I

    aput v5, v2, v4

    aput v4, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 500
    .local v0, "valueAnimator":Landroid/animation/ValueAnimator;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->upDownAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 501
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->upDownUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 502
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    .line 503
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x32

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 504
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 505
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    move v2, v3

    .line 506
    goto :goto_0

    .line 493
    .end local v0    # "valueAnimator":Landroid/animation/ValueAnimator;
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    check-cast v2, Landroid/widget/ImageView;

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightColor:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    :cond_3
    move v2, v3

    .line 510
    goto :goto_0
.end method

.method private keyUpSelectedItemInternal()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 514
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->initialized:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->pressedDown:Z

    if-nez v2, :cond_1

    :cond_0
    move v2, v4

    .line 538
    :goto_0
    return v2

    .line 517
    :cond_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    const/4 v5, -0x1

    if-eq v2, v5, :cond_3

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getItemCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 518
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    sget-object v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    if-ne v2, v5, :cond_2

    .line 519
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    check-cast v2, Landroid/widget/TextView;

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 523
    :goto_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    if-nez v2, :cond_3

    .line 524
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->pressedDown:Z

    .line 525
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 526
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 527
    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v4, v2, v4

    sget v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I

    aput v4, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 528
    .local v0, "valueAnimator":Landroid/animation/ValueAnimator;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->upDownAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 529
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->upDownUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 530
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    .line 531
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x32

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 532
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 533
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    move v2, v3

    .line 534
    goto :goto_0

    .line 521
    .end local v0    # "valueAnimator":Landroid/animation/ValueAnimator;
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    check-cast v2, Landroid/widget/ImageView;

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    :cond_3
    move v2, v3

    .line 538
    goto :goto_0
.end method

.method private moveSelectionLeftInternal()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 466
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->initialized:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    if-eqz v1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return v0

    .line 469
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 472
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setSelectedItem(I)Z

    move-result v0

    goto :goto_0
.end method

.method private moveSelectionRightInternal()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 476
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->initialized:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    if-eqz v1, :cond_1

    .line 482
    :cond_0
    :goto_0
    return v0

    .line 479
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 482
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setSelectedItem(I)Z

    move-result v0

    goto :goto_0
.end method

.method private runOperation(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V
    .locals 2
    .param p1, "operation"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;
    .param p2, "ignoreCurrent"    # Z

    .prologue
    .line 718
    if-nez p2, :cond_1

    .line 719
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationRunning:Z

    if-eqz v0, :cond_1

    .line 720
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 756
    :cond_0
    :goto_0
    return-void

    .line 725
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationRunning:Z

    .line 727
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$9;->$SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 729
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeftInternal()Z

    move-result v0

    if-nez v0, :cond_0

    .line 730
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runQueuedOperation()V

    goto :goto_0

    .line 735
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRightInternal()Z

    move-result v0

    if-nez v0, :cond_0

    .line 736
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runQueuedOperation()V

    goto :goto_0

    .line 741
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItemInternal()Z

    move-result v0

    if-nez v0, :cond_0

    .line 742
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runQueuedOperation()V

    goto :goto_0

    .line 746
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->keyDownSelectedItemInternal()Z

    move-result v0

    if-nez v0, :cond_0

    .line 747
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runQueuedOperation()V

    goto :goto_0

    .line 751
    :pswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->keyUpSelectedItemInternal()Z

    move-result v0

    if-nez v0, :cond_0

    .line 752
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runQueuedOperation()V

    goto :goto_0

    .line 727
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private runQueuedOperation()V
    .locals 3

    .prologue
    .line 771
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 772
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    .line 773
    .local v0, "operation":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$8;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$8;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 782
    .end local v0    # "operation":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;
    :goto_0
    return-void

    .line 780
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationRunning:Z

    goto :goto_0
.end method

.method private selectInitialItem(I)V
    .locals 7
    .param p1, "pos"    # I

    .prologue
    .line 656
    const/4 v4, -0x1

    if-ne p1, v4, :cond_0

    .line 682
    :goto_0
    return-void

    .line 659
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 660
    .local v1, "viewToHighlight":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 661
    .local v2, "width":I
    if-nez v2, :cond_1

    .line 663
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animationLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 666
    :cond_1
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getXPositionForHighlight(Landroid/view/View;)F

    move-result v4

    float-to-int v3, v4

    .line 667
    .local v3, "xPos":I
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 668
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    sub-int/2addr v4, v5

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->MIN_HIGHLIGHT_VIEW_WIDTH:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 669
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Landroid/view/View;->setX(F)V

    .line 671
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    iget v5, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 672
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    .line 673
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    .line 674
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    .line 675
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 676
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    sget-object v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    if-ne v4, v5, :cond_2

    .line 677
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    check-cast v4, Landroid/widget/TextView;

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 681
    :goto_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->initialized:Z

    goto :goto_0

    .line 679
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    check-cast v4, Landroid/widget/ImageView;

    sget v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1
.end method


# virtual methods
.method public clearOperationQueue()V
    .locals 1

    .prologue
    .line 785
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 787
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 788
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 791
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationRunning:Z

    .line 792
    return-void
.end method

.method public executeSelectedItem(Z)V
    .locals 2
    .param p1, "animated"    # Z

    .prologue
    .line 558
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;->EXECUTE:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runOperation(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V

    .line 559
    return-void
.end method

.method public getChoices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    return-object v0
.end method

.method public getSelectedItem()Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 760
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 761
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    .line 763
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedItemIndex()I
    .locals 1

    .prologue
    .line 767
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    return v0
.end method

.method public hasChoiceId(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 709
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    .line 710
    .local v0, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    iget v2, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    if-ne v2, p1, :cond_0

    .line 711
    const/4 v1, 0x1

    .line 714
    .end local v0    # "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected isHighlightPersistent()Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightPersistent:Z

    return v0
.end method

.method public keyDownSelectedItem()V
    .locals 2

    .prologue
    .line 550
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;->KEY_DOWN:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runOperation(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V

    .line 551
    return-void
.end method

.method public keyUpSelectedItem()V
    .locals 2

    .prologue
    .line 554
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;->KEY_UP:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runOperation(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V

    .line 555
    return-void
.end method

.method public moveSelectionLeft()V
    .locals 2

    .prologue
    .line 542
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;->MOVE_LEFT:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runOperation(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V

    .line 543
    return-void
.end method

.method public moveSelectionRight()V
    .locals 2

    .prologue
    .line 546
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;->MOVE_RIGHT:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->runOperation(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;Z)V

    .line 547
    return-void
.end method

.method public resetAnimationElements()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 694
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 698
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 699
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 700
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    .line 706
    :cond_1
    :goto_0
    return-void

    .line 701
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 702
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 703
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    goto :goto_0
.end method

.method public setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V
    .locals 4
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;
    .param p3, "initialSelection"    # I
    .param p4, "listener"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;I",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    const/4 v3, 0x0

    .line 374
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentMode:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    .line 375
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choices:Ljava/util/List;

    .line 376
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 377
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->initialized:Z

    .line 378
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    .line 379
    .local v1, "pendingOp":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;

    .line 380
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->clearOperationQueue()V

    .line 381
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z

    .line 382
    const/4 v2, -0x1

    iput v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    .line 383
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 384
    if-ltz p3, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p3, v2, :cond_2

    .line 385
    iput p3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    .line 391
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->buildChoices()V

    .line 392
    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectInitialItem(I)V

    .line 393
    sget-object v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;->KEY_UP:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Operation;

    if-ne v1, v2, :cond_1

    .line 394
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 395
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 397
    .end local v0    # "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    return-void

    .line 387
    :cond_2
    iput v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    goto :goto_0
.end method

.method public setHighlightPersistent(Z)V
    .locals 0
    .param p1, "highlightPersistent"    # Z

    .prologue
    .line 261
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightPersistent:Z

    .line 262
    return-void
.end method

.method public setHighlightVisibility(I)V
    .locals 0
    .param p1, "highlightVisibility"    # I

    .prologue
    .line 351
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    .line 352
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->invalidate()V

    .line 353
    return-void
.end method

.method public setItemPadding(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 689
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingLeft:I

    .line 690
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextPaddingRight:I

    .line 691
    return-void
.end method

.method public setLabelTextSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 685
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceTextSize:I

    .line 686
    return-void
.end method

.method protected setSelectedItem(I)Z
    .locals 13
    .param p1, "pos"    # I

    .prologue
    const/4 v12, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 599
    iget v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    if-ne v8, p1, :cond_1

    .line 642
    :cond_0
    :goto_0
    return v6

    .line 602
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getItemCount()I

    move-result v1

    .line 603
    .local v1, "itemCount":I
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    .line 606
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I

    .line 607
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v8, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 608
    .local v4, "view":Landroid/view/View;
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;

    .line 609
    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    .line 610
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 611
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sget v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->MIN_HIGHLIGHT_VIEW_WIDTH:I

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 612
    .local v5, "width":I
    if-eqz v5, :cond_0

    .line 615
    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    add-int/2addr v8, v9

    sub-int/2addr v5, v8

    .line 617
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;

    if-eqz v8, :cond_2

    .line 618
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->oldSelectionView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getPaddingRight()I

    move-result v10

    add-int/2addr v9, v10

    sub-int/2addr v8, v9

    sget v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->MIN_HIGHLIGHT_VIEW_WIDTH:I

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 622
    .local v2, "oldWidth":I
    :goto_1
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    .line 623
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    const-string v9, "x"

    new-array v10, v7, [F

    invoke-direct {p0, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->getXPositionForHighlight(Landroid/view/View;)F

    move-result v11

    aput v11, v10, v6

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 624
    .local v0, "a1":Landroid/animation/ObjectAnimator;
    new-array v8, v12, [I

    aput v2, v8, v6

    aput v5, v8, v7

    invoke-static {v8}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 625
    .local v3, "v1":Landroid/animation/ValueAnimator;
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout$7;

    invoke-direct {v8, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$7;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V

    invoke-virtual {v3, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 634
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    iget v9, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightVisibility:I

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 635
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->invalidate()V

    .line 636
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->requestLayout()V

    .line 637
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    new-array v9, v12, [Landroid/animation/Animator;

    aput-object v0, v9, v6

    aput-object v3, v9, v7

    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 638
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v8, 0x64

    invoke-virtual {v6, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 639
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightViewListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v6, v8}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 640
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    .line 641
    sget-object v6, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v6}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    move v6, v7

    .line 642
    goto/16 :goto_0

    .line 620
    .end local v0    # "a1":Landroid/animation/ObjectAnimator;
    .end local v2    # "oldWidth":I
    .end local v3    # "v1":Landroid/animation/ValueAnimator;
    :cond_2
    move v2, v5

    .restart local v2    # "oldWidth":I
    goto :goto_1
.end method
