.class Lcom/navdy/hud/app/ui/component/HaloView$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "HaloView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/HaloView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/HaloView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/HaloView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/HaloView;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/HaloView$1;->this$0:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView$1;->this$0:Lcom/navdy/hud/app/ui/component/HaloView;

    # getter for: Lcom/navdy/hud/app/ui/component/HaloView;->started:Z
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/HaloView;->access$000(Lcom/navdy/hud/app/ui/component/HaloView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView$1;->this$0:Lcom/navdy/hud/app/ui/component/HaloView;

    # getter for: Lcom/navdy/hud/app/ui/component/HaloView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/HaloView;->access$200(Lcom/navdy/hud/app/ui/component/HaloView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView$1;->this$0:Lcom/navdy/hud/app/ui/component/HaloView;

    # getter for: Lcom/navdy/hud/app/ui/component/HaloView;->startRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/HaloView;->access$100(Lcom/navdy/hud/app/ui/component/HaloView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView$1;->this$0:Lcom/navdy/hud/app/ui/component/HaloView;

    # getter for: Lcom/navdy/hud/app/ui/component/HaloView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/HaloView;->access$200(Lcom/navdy/hud/app/ui/component/HaloView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView$1;->this$0:Lcom/navdy/hud/app/ui/component/HaloView;

    # getter for: Lcom/navdy/hud/app/ui/component/HaloView;->startRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/HaloView;->access$100(Lcom/navdy/hud/app/ui/component/HaloView;)Ljava/lang/Runnable;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView$1;->this$0:Lcom/navdy/hud/app/ui/component/HaloView;

    # getter for: Lcom/navdy/hud/app/ui/component/HaloView;->animationDelay:I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/HaloView;->access$300(Lcom/navdy/hud/app/ui/component/HaloView;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
