.class public final enum Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
.super Ljava/lang/Enum;
.source "SystemTrayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/SystemTrayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/SystemTrayView$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum LOCATION_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum LOCATION_LOST:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum NO_PHONE_NETWORK:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field public static final enum PHONE_NETWORK_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 68
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 69
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "LOW_BATTERY"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 70
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "EXTREMELY_LOW_BATTERY"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 71
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "OK_BATTERY"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 72
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "NO_PHONE_NETWORK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->NO_PHONE_NETWORK:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 73
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "PHONE_NETWORK_AVAILABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->PHONE_NETWORK_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 74
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "LOCATION_LOST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOCATION_LOST:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 75
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const-string v1, "LOCATION_AVAILABLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOCATION_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 66
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->NO_PHONE_NETWORK:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->PHONE_NETWORK_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOCATION_LOST:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOCATION_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->$VALUES:[Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->$VALUES:[Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    return-object v0
.end method
