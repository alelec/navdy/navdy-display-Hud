.class public final enum Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;
.super Ljava/lang/Enum;
.source "ChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

.field public static final enum ICON:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

.field public static final enum LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 142
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    const-string v1, "LABEL"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    .line 143
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    const-string v1, "ICON"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->ICON:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    .line 141
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->ICON:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->$VALUES:[Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 141
    const-class v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->$VALUES:[Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    return-object v0
.end method
