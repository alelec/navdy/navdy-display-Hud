.class Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "ChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 9
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 214
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 219
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 220
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, Landroid/widget/TextView;

    if-eqz v3, :cond_3

    .line 221
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$200()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 225
    :cond_2
    :goto_1
    new-array v3, v7, [I

    aput v6, v3, v6

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$300()I

    move-result v4

    aput v4, v3, v8

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 226
    .local v2, "valueAnimator":Landroid/animation/ValueAnimator;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$400(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 227
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$500(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 228
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    const-string v4, "alpha"

    new-array v5, v7, [F

    fill-array-data v5, :array_0

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 229
    .local v0, "alphaAnimator":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    # setter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$602(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 230
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$600(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/animation/AnimatorSet;

    move-result-object v3

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 231
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$600(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/animation/AnimatorSet;

    move-result-object v3

    new-array v4, v7, [Landroid/animation/Animator;

    aput-object v2, v4, v6

    aput-object v0, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 232
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$600(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/animation/AnimatorSet;

    move-result-object v3

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    .line 222
    .end local v0    # "alphaAnimator":Landroid/animation/ObjectAnimator;
    .end local v2    # "valueAnimator":Landroid/animation/ValueAnimator;
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    .line 223
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$200()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    .line 235
    :cond_4
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->isHighlightPersistent()Z

    move-result v3

    if-nez v3, :cond_5

    .line 236
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 238
    :cond_5
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 239
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 240
    .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceHighlightMargin:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$300()I

    move-result v3

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 241
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # setter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executing:Z
    invoke-static {v3, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$702(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Z)Z

    .line 242
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # setter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->animatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v3, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$602(Lcom/navdy/hud/app/ui/component/ChoiceLayout;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 243
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->clearOperationQueue()V

    .line 244
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    .line 245
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)I

    move-result v4

    # invokes: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->callListener(I)V
    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$900(Lcom/navdy/hud/app/ui/component/ChoiceLayout;I)V

    .line 247
    :cond_6
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, Landroid/widget/TextView;

    if-eqz v3, :cond_7

    .line 248
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1000()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 249
    :cond_7
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 250
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->currentSelectionView:Landroid/view/View;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->textSelectionColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1000()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto/16 :goto_0

    .line 228
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data
.end method
