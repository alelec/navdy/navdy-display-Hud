.class public Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;
.super Landroid/widget/LinearLayout;
.source "GpsSignalView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;
    }
.end annotation


# instance fields
.field private logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->setOrientation(I)V

    .line 55
    return-void
.end method


# virtual methods
.method public setSatelliteData([Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;)V
    .locals 16
    .param p1, "satelliteData"    # [Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;

    .prologue
    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->removeAllViews()V

    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 61
    .local v2, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 62
    .local v7, "resources":Landroid/content/res/Resources;
    const v13, 0x7f0b0122

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 63
    .local v8, "separator":I
    const v13, 0x7f0b0121

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 65
    .local v12, "width":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v1, v13, :cond_1

    .line 66
    const v13, 0x7f030058

    const/4 v14, 0x0

    invoke-virtual {v2, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 67
    .local v3, "layout":Landroid/view/ViewGroup;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v13, -0x1

    invoke-direct {v5, v12, v13}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 68
    .local v5, "lytParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    const v13, 0x7f0e01b5

    invoke-virtual {v3, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 71
    .local v9, "textView":Landroid/widget/TextView;
    aget-object v13, p1, v1

    iget v13, v13, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->cNo:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    const v13, 0x7f0e01b4

    invoke-virtual {v3, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .end local v9    # "textView":Landroid/widget/TextView;
    check-cast v9, Landroid/widget/TextView;

    .line 74
    .restart local v9    # "textView":Landroid/widget/TextView;
    aget-object v13, p1, v1

    iget-object v13, v13, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->provider:Ljava/lang/String;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    const v13, 0x7f0e01b3

    invoke-virtual {v3, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .end local v9    # "textView":Landroid/widget/TextView;
    check-cast v9, Landroid/widget/TextView;

    .line 77
    .restart local v9    # "textView":Landroid/widget/TextView;
    aget-object v13, p1, v1

    iget v13, v13, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->satelliteId:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    const v13, 0x7f0e01b6

    invoke-virtual {v3, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 80
    .local v11, "view":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 81
    .local v6, "params":Landroid/widget/RelativeLayout$LayoutParams;
    aget-object v13, p1, v1

    iget v13, v13, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->cNo:I

    mul-int/lit8 v13, v13, 0x2

    iput v13, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 83
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->addView(Landroid/view/View;)V

    .line 85
    move-object/from16 v0, p1

    array-length v13, v0

    add-int/lit8 v13, v13, -0x1

    if-ge v1, v13, :cond_0

    .line 86
    new-instance v10, Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-direct {v10, v13}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 87
    .local v10, "v":Landroid/view/View;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 88
    .local v4, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v10, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->addView(Landroid/view/View;)V

    .line 65
    .end local v4    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v10    # "v":Landroid/view/View;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 93
    .end local v3    # "layout":Landroid/view/ViewGroup;
    .end local v5    # "lytParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9    # "textView":Landroid/widget/TextView;
    .end local v11    # "view":Landroid/view/View;
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "set satellite ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    array-length v15, v0

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 94
    return-void
.end method
