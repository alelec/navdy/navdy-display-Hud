.class public Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;
.super Ljava/lang/Object;
.source "GpsSignalView.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SatelliteData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;",
        ">;"
    }
.end annotation


# instance fields
.field cNo:I

.field provider:Ljava/lang/String;

.field satelliteId:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1, "satelliteId"    # I
    .param p2, "cNo"    # I
    .param p3, "provider"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->satelliteId:I

    .line 30
    iput p2, p0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->cNo:I

    .line 31
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->provider:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;)I
    .locals 2
    .param p1, "another"    # Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;

    .prologue
    .line 40
    iget v0, p1, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->cNo:I

    iget v1, p0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->cNo:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;->compareTo(Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;)I

    move-result v0

    return v0
.end method
