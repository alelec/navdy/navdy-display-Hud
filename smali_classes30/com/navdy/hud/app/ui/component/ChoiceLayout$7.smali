.class Lcom/navdy/hud/app/ui/component/ChoiceLayout$7;
.super Ljava/lang/Object;
.source "ChoiceLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setSelectedItem(I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 625
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$7;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "valueAnimator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 628
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$7;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 629
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 630
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$7;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 631
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$7;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 632
    return-void
.end method
