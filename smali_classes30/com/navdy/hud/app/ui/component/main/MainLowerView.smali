.class public Lcom/navdy/hud/app/ui/component/main/MainLowerView;
.super Landroid/widget/FrameLayout;
.source "MainLowerView.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/main/MainLowerView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method private initView()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    if-nez v0, :cond_0

    .line 82
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-nez v0, :cond_1

    .line 85
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-nez v0, :cond_3

    .line 88
    :cond_2
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public ejectView()V
    .locals 3

    .prologue
    .line 60
    sget-object v1, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ejectView"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->initView()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 68
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->setVisibility(I)V

    .line 69
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->setAlpha(F)V

    .line 70
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->removeAllViews()V

    .line 71
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v0

    .line 72
    .local v0, "navController":Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavController;->getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    if-eq v1, v2, :cond_2

    .line 73
    sget-object v1, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ejectView: reset to open mode"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setMode(Lcom/navdy/hud/app/maps/NavigationMode;)V

    goto :goto_0

    .line 76
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ejectView: nav active already"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public injectView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "injectView"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->initView()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->removeAllViews()V

    .line 46
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->addView(Landroid/view/View;)V

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->setAlpha(F)V

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 50
    sget-object v0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "injectView: set to nav mode"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    sget-object v1, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setMode(Lcom/navdy/hud/app/maps/NavigationMode;)V

    .line 56
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 53
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/main/MainLowerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "injectView: nav active already"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method
