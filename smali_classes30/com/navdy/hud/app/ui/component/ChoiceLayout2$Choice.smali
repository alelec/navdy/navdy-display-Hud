.class public Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
.super Ljava/lang/Object;
.source "ChoiceLayout2.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Choice"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final fluctuatorColor:I

.field private final id:I

.field private final label:Ljava/lang/String;

.field private final resIdSelected:I

.field private final resIdUnselected:I

.field private final selectedColor:I

.field private final unselectedColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIIILjava/lang/String;I)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "resIdSelected"    # I
    .param p3, "selectedColor"    # I
    .param p4, "resIdUnselected"    # I
    .param p5, "unselectedColor"    # I
    .param p6, "label"    # Ljava/lang/String;
    .param p7, "fluctuatorColor"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I

    .line 70
    iput p2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdSelected:I

    .line 71
    iput p3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->selectedColor:I

    .line 72
    iput p4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdUnselected:I

    .line 73
    iput p5, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->unselectedColor:I

    .line 74
    iput-object p6, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->label:Ljava/lang/String;

    .line 75
    iput p7, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->fluctuatorColor:I

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdSelected:I

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->selectedColor:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdUnselected:I

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->unselectedColor:I

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "str":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->label:Ljava/lang/String;

    .line 90
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->fluctuatorColor:I

    .line 91
    return-void

    .line 88
    :cond_0
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->label:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdSelected:I

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->selectedColor:I

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdUnselected:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->unselectedColor:I

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->fluctuatorColor:I

    return v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I

    return v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->label:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 112
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdSelected:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->selectedColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdUnselected:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->unselectedColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->label:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->label:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 118
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->fluctuatorColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    return-void

    .line 117
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
