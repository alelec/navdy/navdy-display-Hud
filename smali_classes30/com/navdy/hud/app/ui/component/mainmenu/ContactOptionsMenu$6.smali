.class Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;
.super Ljava/lang/Object;
.source "ContactOptionsMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->launchShareTripMenu(Lcom/navdy/hud/app/framework/contacts/Contact;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

.field final synthetic val$args:Landroid/os/Bundle;

.field final synthetic val$contact:Lcom/navdy/hud/app/framework/contacts/Contact;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    .prologue
    .line 431
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;->val$args:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;->val$contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 434
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->access$400(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;->val$args:Landroid/os/Bundle;

    new-instance v4, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;->val$contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->access$300(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;-><init>(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 435
    return-void
.end method
