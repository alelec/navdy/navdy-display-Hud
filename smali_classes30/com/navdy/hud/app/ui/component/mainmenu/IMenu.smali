.class public interface abstract Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
.super Ljava/lang/Object;
.source "IMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;,
        Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    }
.end annotation


# virtual methods
.method public abstract getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
.end method

.method public abstract getInitialSelection()I
.end method

.method public abstract getItems()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
.end method

.method public abstract getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
.end method

.method public abstract getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
.end method

.method public abstract isBindCallsEnabled()Z
.end method

.method public abstract isFirstItemEmpty()Z
.end method

.method public abstract isItemClickable(II)Z
.end method

.method public abstract onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
.end method

.method public abstract onFastScrollEnd()V
.end method

.method public abstract onFastScrollStart()V
.end method

.method public abstract onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
.end method

.method public abstract onScrollIdle()V
.end method

.method public abstract onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
.end method

.method public abstract selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
.end method

.method public abstract setBackSelectionId(I)V
.end method

.method public abstract setBackSelectionPos(I)V
.end method

.method public abstract setSelectedIcon()V
.end method

.method public abstract showToolTip()V
.end method
