.class Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;
.super Ljava/lang/Object;
.source "SystemInfoMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 239
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->access$000(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateTemperature()V
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->access$100(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V

    .line 244
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateVoltage()V
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V

    .line 245
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->setFix()V
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->access$300(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V

    .line 247
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->access$400(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->access$400(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
