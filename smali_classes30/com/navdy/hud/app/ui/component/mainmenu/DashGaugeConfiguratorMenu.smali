.class public final Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;
.super Ljava/lang/Object;
.source "DashGaugeConfiguratorMenu.kt"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;,
        Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u00d2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000 r2\u00020\u0001:\u0002rsB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u000bJ\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<H\u0007J\"\u0010=\u001a\u0004\u0018\u00010\u00012\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020?H\u0016J\u0008\u0010A\u001a\u00020\rH\u0016J\u0010\u0010B\u001a\n\u0012\u0004\u0012\u00020D\u0018\u00010CH\u0016J\u0012\u0010E\u001a\u0004\u0018\u00010D2\u0006\u0010F\u001a\u00020\rH\u0016J\n\u0010G\u001a\u0004\u0018\u00010HH\u0016J\n\u0010I\u001a\u0004\u0018\u00010JH\u0016J\u0008\u0010K\u001a\u00020\'H\u0016J\u0008\u0010L\u001a\u00020\'H\u0016J\u0018\u0010M\u001a\u00020\'2\u0006\u0010N\u001a\u00020\r2\u0006\u0010F\u001a\u00020\rH\u0016J(\u0010O\u001a\u00020:2\u0006\u0010P\u001a\u00020D2\u0006\u0010Q\u001a\u00020R2\u0006\u0010F\u001a\u00020\r2\u0006\u0010S\u001a\u00020TH\u0016J\u0010\u0010U\u001a\u00020:2\u0006\u0010V\u001a\u00020WH\u0007J\u0010\u0010X\u001a\u00020:2\u0006\u0010Y\u001a\u00020ZH\u0007J\u0008\u0010[\u001a\u00020:H\u0016J\u0008\u0010\\\u001a\u00020:H\u0016J\u0010\u0010]\u001a\u00020:2\u0006\u0010^\u001a\u00020_H\u0007J\u0010\u0010`\u001a\u00020:2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010c\u001a\u00020:2\u0006\u0010;\u001a\u00020dH\u0007J\u0010\u0010e\u001a\u00020:2\u0006\u0010;\u001a\u00020fH\u0007J\u0008\u0010g\u001a\u00020:H\u0016J\u0010\u0010h\u001a\u00020:2\u0006\u0010i\u001a\u00020jH\u0016J\u0010\u0010k\u001a\u00020\'2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010l\u001a\u00020:2\u0006\u0010N\u001a\u00020\rH\u0016J\u0010\u0010m\u001a\u00020:2\u0006\u0010n\u001a\u00020\rH\u0016J\u0008\u0010o\u001a\u00020:H\u0016J\u000e\u0010p\u001a\u00020:2\u0006\u0010F\u001a\u00020\rJ\u0008\u0010q\u001a\u00020:H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\"\u0010#\"\u0004\u0008$\u0010%R\u000e\u0010\n\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008(\u0010)\"\u0004\u0008*\u0010+R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010,\u001a\u0008\u0018\u00010-R\u00020.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008/\u00100\"\u0004\u00081\u00102R\u0011\u00103\u001a\u00020.\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00084\u00105R\u001a\u00106\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00087\u0010)\"\u0004\u00088\u0010+R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006t"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
        "bus",
        "Lcom/squareup/otto/Bus;",
        "sharedPreferences",
        "Landroid/content/SharedPreferences;",
        "vscrollComponent",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
        "presenter",
        "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
        "parent",
        "(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V",
        "backSelection",
        "",
        "backSelectionId",
        "value",
        "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;",
        "gaugeType",
        "getGaugeType",
        "()Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;",
        "setGaugeType",
        "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V",
        "gaugeView",
        "Lcom/navdy/hud/app/view/DashboardWidgetView;",
        "getGaugeView",
        "()Lcom/navdy/hud/app/view/DashboardWidgetView;",
        "setGaugeView",
        "(Lcom/navdy/hud/app/view/DashboardWidgetView;)V",
        "gaugeViewContainer",
        "Landroid/widget/FrameLayout;",
        "getGaugeViewContainer",
        "()Landroid/widget/FrameLayout;",
        "headingDataUtil",
        "Lcom/navdy/hud/app/util/HeadingDataUtil;",
        "getHeadingDataUtil",
        "()Lcom/navdy/hud/app/util/HeadingDataUtil;",
        "setHeadingDataUtil",
        "(Lcom/navdy/hud/app/util/HeadingDataUtil;)V",
        "registered",
        "",
        "getRegistered",
        "()Z",
        "setRegistered",
        "(Z)V",
        "smartDashWidgetCache",
        "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;",
        "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;",
        "getSmartDashWidgetCache",
        "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;",
        "setSmartDashWidgetCache",
        "(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;)V",
        "smartDashWidgetManager",
        "getSmartDashWidgetManager",
        "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;",
        "userPreferenceChanged",
        "getUserPreferenceChanged",
        "setUserPreferenceChanged",
        "ObdPidChangeEvent",
        "",
        "event",
        "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;",
        "getChildMenu",
        "args",
        "",
        "path",
        "getInitialSelection",
        "getItems",
        "",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "getModelfromPos",
        "pos",
        "getScrollIndex",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;",
        "getType",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;",
        "isBindCallsEnabled",
        "isFirstItemEmpty",
        "isItemClickable",
        "id",
        "onBindToView",
        "model",
        "view",
        "Landroid/view/View;",
        "state",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;",
        "onDriveScoreUpdated",
        "driveScoreUpdated",
        "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;",
        "onDriverProfileChanged",
        "profileChanged",
        "Lcom/navdy/hud/app/event/DriverProfileChanged;",
        "onFastScrollEnd",
        "onFastScrollStart",
        "onGpsLocationChanged",
        "location",
        "Landroid/location/Location;",
        "onItemSelected",
        "selection",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;",
        "onLocationFixChangeEvent",
        "Lcom/navdy/hud/app/maps/MapEvents$LocationFix;",
        "onMapEvent",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "onScrollIdle",
        "onUnload",
        "level",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;",
        "selectItem",
        "setBackSelectionId",
        "setBackSelectionPos",
        "n",
        "setSelectedIcon",
        "showGauge",
        "showToolTip",
        "Companion",
        "GaugeType",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final backColor:I

.field private static final bkColorUnselected:I

.field private static final centerGaugeBackgroundColor:I

.field private static final centerGaugeOptionsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private static final centerGaugeTitle:Ljava/lang/String;

.field private static final offLabel:Ljava/lang/String;

.field private static final onLabel:Ljava/lang/String;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sideGaugesOptionsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private static final sideGaugesTitle:Ljava/lang/String;

.field private static final speedoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final tachoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

.field private static final unavailableLabel:Ljava/lang/String;


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private final bus:Lcom/squareup/otto/Bus;

.field private gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final gaugeViewContainer:Landroid/widget/FrameLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private final presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private registered:Z

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private userPreferenceChanged:Z

.field private final vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/high16 v9, -0x1000000

    const/4 v6, 0x0

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    invoke-direct {v0, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeOptionsList:Ljava/util/ArrayList;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sideGaugesOptionsList:Ljava/util/ArrayList;

    .line 100
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 111
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 112
    .local v15, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0d0054

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->backColor:I

    .line 113
    const v0, 0x7f0d003e

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bkColorUnselected:I

    .line 114
    const v0, 0x7f090078

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeTitle:Ljava/lang/String;

    .line 115
    const v0, 0x7f090079

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sideGaugesTitle:Ljava/lang/String;

    .line 116
    const v0, 0x7f0901eb

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->onLabel:Ljava/lang/String;

    .line 117
    const v0, 0x7f0901e8

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->offLabel:Ljava/lang/String;

    .line 118
    const v0, 0x7f0902ca

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->unavailableLabel:Ljava/lang/String;

    .line 119
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v14

    .line 120
    .local v14, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v14}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    const-string v1, "remoteDeviceManager.uiStateManager"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 121
    const v0, 0x7f0d0066

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeBackgroundColor:I

    .line 124
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getBackColor()I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getBackColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)I

    move-result v2

    .line 125
    .local v2, "fluctuatorColor":I
    const v0, 0x7f09002d

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 126
    .local v5, "title":Ljava/lang/String;
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 129
    const v0, 0x7f090287

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 130
    const v0, 0x7f0d0069

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 131
    const v7, 0x7f0e0037

    const v8, 0x7f02019d

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v11, v2

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->tachoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 134
    const v0, 0x7f090277

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 135
    const v0, 0x7f0d0068

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 136
    const v7, 0x7f0e0036

    const v8, 0x7f02019c

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v11, v2

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->speedoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 138
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getTachoMeter()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getTachoMeter$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSpeedoMeter()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSpeedoMeter$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 3
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "sharedPreferences"    # Landroid/content/SharedPreferences;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    const-string v0, "bus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sharedPreferences"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "vscrollComponent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bus:Lcom/squareup/otto/Bus;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sharedPreferences:Landroid/content/SharedPreferences;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 48
    new-instance v0, Lcom/navdy/hud/app/util/HeadingDataUtil;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/HeadingDataUtil;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    const-string v1, "vscrollComponent.selectedCustomView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeViewContainer:Landroid/widget/FrameLayout;

    .line 53
    new-instance v0, Lcom/navdy/hud/app/view/DashboardWidgetView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeViewContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 56
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeViewContainer:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 57
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;-><init>(Landroid/content/SharedPreferences;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->onResume()V

    .line 60
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;->INSTANCE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->setFilter(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;)V

    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->reLoadAvailableWidgets(Z)V

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->registerForChanges(Ljava/lang/Object;)V

    .line 145
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    return-void
.end method

.method public static final synthetic access$getBack$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getBackColor$cp()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->backColor:I

    return v0
.end method

.method public static final synthetic access$getBkColorUnselected$cp()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bkColorUnselected:I

    return v0
.end method

.method public static final synthetic access$getCenterGaugeBackgroundColor$cp()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeBackgroundColor:I

    return v0
.end method

.method public static final synthetic access$getCenterGaugeOptionsList$cp()Ljava/util/ArrayList;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeOptionsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static final synthetic access$getCenterGaugeTitle$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeTitle:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getOffLabel$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->offLabel:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getOnLabel$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->onLabel:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getPresenter$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method public static final synthetic access$getSLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getSideGaugesOptionsList$cp()Ljava/util/ArrayList;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sideGaugesOptionsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static final synthetic access$getSideGaugesTitle$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sideGaugesTitle:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getSpeedoMeter$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->speedoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getTachoMeter$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->tachoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getUiStateManager$cp()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-object v0
.end method

.method public static final synthetic access$getUnavailableLabel$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->unavailableLabel:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 371
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 375
    :goto_0
    return-void

    .line 372
    :sswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "FUEL_GAUGE_ID"

    iget-object v2, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 373
    :sswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "MPG_AVG_WIDGET"

    iget-object v2, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 374
    :sswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "ENGINE_TEMPERATURE_GAUGE_ID"

    iget-object v2, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 371
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_2
        0x2f -> :sswitch_0
        0x100 -> :sswitch_1
    .end sparse-switch
.end method

.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "args"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "path"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "args"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "path"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getGaugeType()Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    return-object v0
.end method

.method public final getGaugeView()Lcom/navdy/hud/app/view/DashboardWidgetView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    return-object v0
.end method

.method public final getGaugeViewContainer()Landroid/widget/FrameLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeViewContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final getHeadingDataUtil()Lcom/navdy/hud/app/util/HeadingDataUtil;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 197
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 167
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 165
    :goto_0
    return-object v0

    .line 170
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->buildSmartDashWidgetCache(I)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    .line 171
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSideGaugesOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 172
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSideGaugesOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    if-eqz v0, :cond_b

    .line 174
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    if-nez v7, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 175
    .local v7, "$receiver":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 176
    .local v13, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0d0067

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 177
    .local v2, "fluctuatorColor":I
    const/4 v1, 0x0

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetsCount()I

    move-result v0

    add-int/lit8 v14, v0, -0x1

    if-gt v1, v14, :cond_a

    .line 178
    :goto_1
    invoke-virtual {v7, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 179
    .local v3, "title":Ljava/lang/String;
    :goto_2
    invoke-virtual {v7, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetIdentifier()Ljava/lang/String;

    move-result-object v10

    .line 180
    .local v10, "identifier":Ljava/lang/String;
    :goto_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v0, v10}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->isGaugeOn(Ljava/lang/String;)Z

    move-result v5

    .line 181
    .local v5, "isGaugeOn":Z
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v11

    .line 182
    .local v11, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v11}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v12

    .line 183
    .local v12, "pidSet":Lcom/navdy/obd/PidSet;
    if-nez v10, :cond_4

    .line 188
    :cond_1
    :goto_4
    const/4 v0, 0x1

    move v6, v0

    .line 191
    .local v6, "isGaugeEnabled":Z
    :goto_5
    if-eqz v6, :cond_9

    if-eqz v5, :cond_8

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getOnLabel()Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getOnLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    move-object v4, v0

    .line 192
    .local v4, "subtitle":Ljava/lang/String;
    :goto_7
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->Companion:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;->buildModel(IILjava/lang/String;Ljava/lang/String;ZZ)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v8

    .line 193
    .local v8, "gaugeVerticalListModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSideGaugesOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    if-eq v1, v14, :cond_a

    add-int/lit8 v9, v1, 0x1

    .local v9, "i":I
    move v1, v9

    goto :goto_1

    .line 178
    .end local v3    # "title":Ljava/lang/String;
    .end local v4    # "subtitle":Ljava/lang/String;
    .end local v5    # "isGaugeOn":Z
    .end local v6    # "isGaugeEnabled":Z
    .end local v8    # "gaugeVerticalListModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v9    # "i":I
    .end local v10    # "identifier":Ljava/lang/String;
    .end local v11    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .end local v12    # "pidSet":Lcom/navdy/obd/PidSet;
    :cond_2
    const-string v3, ""

    goto :goto_2

    .line 179
    .restart local v3    # "title":Ljava/lang/String;
    :cond_3
    const/4 v10, 0x0

    goto :goto_3

    .line 183
    .restart local v5    # "isGaugeOn":Z
    .restart local v10    # "identifier":Ljava/lang/String;
    .restart local v11    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .restart local v12    # "pidSet":Lcom/navdy/obd/PidSet;
    :cond_4
    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_4

    :sswitch_0
    const-string v0, "MPG_AVG_WIDGET"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    if-eqz v12, :cond_7

    const/16 v0, 0x100

    invoke-virtual {v12, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_8
    move v6, v0

    goto :goto_5

    .line 183
    :sswitch_1
    const-string v0, "ENGINE_TEMPERATURE_GAUGE_ID"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    if-eqz v12, :cond_6

    const/4 v0, 0x5

    invoke-virtual {v12, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_9
    move v6, v0

    goto :goto_5

    .line 183
    :sswitch_2
    const-string v0, "FUEL_GAUGE_ID"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    if-eqz v12, :cond_5

    const/16 v0, 0x2f

    invoke-virtual {v12, v0}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_a
    move v6, v0

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    goto :goto_a

    .line 185
    :cond_6
    const/4 v0, 0x0

    goto :goto_9

    .line 186
    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    .line 191
    .restart local v6    # "isGaugeEnabled":Z
    :cond_8
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getOffLabel()Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getOffLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_9
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getUnavailableLabel()Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getUnavailableLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;

    move-result-object v4

    goto :goto_7

    .line 194
    .end local v3    # "title":Ljava/lang/String;
    .end local v5    # "isGaugeOn":Z
    .end local v6    # "isGaugeEnabled":Z
    .end local v10    # "identifier":Ljava/lang/String;
    .end local v11    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .end local v12    # "pidSet":Lcom/navdy/obd/PidSet;
    :cond_a
    nop

    .line 174
    nop

    .line 197
    .end local v2    # "fluctuatorColor":I
    .end local v7    # "$receiver":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    .end local v13    # "resources":Landroid/content/res/Resources;
    :cond_b
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSideGaugesOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto/16 :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 183
    :sswitch_data_0
    .sparse-switch
        -0x45145f74 -> :sswitch_0
        -0x7dd2557 -> :sswitch_1
        0x45a45eaa -> :sswitch_2
    .end sparse-switch
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p1, "pos"    # I
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 300
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 297
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 295
    :goto_0
    return-object v0

    .line 300
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesOptionsList()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSideGaugesOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    goto :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getRegistered()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    return v0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 208
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSmartDashWidgetCache()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    return-object v0
.end method

.method public final getSmartDashWidgetManager()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 287
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public final getUserPreferenceChanged()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->userPreferenceChanged:Z

    return v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 422
    const/4 v0, 0x0

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 291
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 1
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "view"    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    return-void
.end method

.method public final onDriveScoreUpdated(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V
    .locals 2
    .param p1, "driveScoreUpdated"    # Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "driveScoreUpdated"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 401
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "DRIVE_SCORE_GAUGE_ID"

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    .line 402
    return-void
.end method

.method public final onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 2
    .param p1, "profileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "profileChanged"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 395
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onDriverProfileChanged, reloading the widgets"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->reLoadAvailableWidgets(Z)V

    .line 397
    :cond_0
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 415
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public final onGpsLocationChanged(Landroid/location/Location;)V
    .locals 6
    .param p1, "location"    # Landroid/location/Location;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v2, "location"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 380
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-double v0, v2

    .line 381
    .local v0, "heading":D
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    invoke-virtual {v2, v0, v1}, Lcom/navdy/hud/app/util/HeadingDataUtil;->setHeading(D)V

    .line 382
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v3, "COMPASS_WIDGET"

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    invoke-virtual {v4}, Lcom/navdy/hud/app/util/HeadingDataUtil;->getHeading()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    .line 383
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 3
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v1, "selection"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    .line 334
    .local v0, "pos":I
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->showGauge(I)V

    .line 336
    .end local v0    # "pos":I
    :cond_0
    return-void
.end method

.method public final onLocationFixChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$LocationFix;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 387
    iget-boolean v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->locationAvailable:Z

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    invoke-virtual {v0}, Lcom/navdy/hud/app/util/HeadingDataUtil;->reset()V

    .line 389
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "COMPASS_WIDGET"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    .line 391
    :cond_0
    return-void
.end method

.method public final onMapEvent(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v2, "event"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v1

    .line 365
    .local v1, "unit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    iget v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentSpeedLimit:F

    float-to-double v2, v2

    sget-object v4, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v2, v3, v4, v1}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v2

    int-to-float v0, v2

    .line 366
    .local v0, "speedLimitNativeUnit":F
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v3, "SPEED_LIMIT_SIGN_GAUGE_ID"

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    .line 367
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 407
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 3
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const-string v0, "level"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->getTag()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    instance-of v2, v0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    check-cast v0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 319
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->userPreferenceChanged:Z

    if-eqz v0, :cond_4

    .line 320
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User preference has changed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->userPreferenceChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$UserPreferenceChanged;

    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$UserPreferenceChanged;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 324
    :goto_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    if-eqz v0, :cond_2

    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 327
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    .line 329
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    .line 318
    goto :goto_0

    .line 323
    :cond_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User preference has not been changed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->userPreferenceChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 9
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string v4, "selection"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "select id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pos:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 238
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    .line 279
    :cond_0
    :goto_0
    return v8

    .line 240
    :pswitch_0
    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 250
    :sswitch_0
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close()V

    .line 251
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getUiStateManager$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getSmartDashView()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    move-result-object v2

    .line 252
    .local v2, "smartDashView":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->onSpeedoMeterSelected()V

    goto :goto_0

    .line 242
    .end local v2    # "smartDashView":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    :sswitch_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->backSelection:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 245
    :sswitch_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close()V

    .line 246
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getUiStateManager$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getSmartDashView()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    move-result-object v2

    .line 247
    .restart local v2    # "smartDashView":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->onTachoMeterSelected()V

    goto :goto_0

    .line 257
    .end local v2    # "smartDashView":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    :pswitch_1
    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v4, :pswitch_data_1

    .line 262
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 263
    .local v1, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    .line 264
    .local v0, "id":I
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    if-eqz v4, :cond_1

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v3

    .line 265
    .local v3, "widgetPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :goto_1
    iget-boolean v4, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    if-eqz v4, :cond_3

    .line 267
    iput-boolean v8, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    .line 268
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getOffLabel()Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getOffLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 269
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetIdentifier()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    :goto_2
    invoke-virtual {v5, v4, v8}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->setGaugeOn(Ljava/lang/String;Z)V

    .line 275
    :goto_3
    iput-boolean v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->userPreferenceChanged:Z

    .line 277
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(I)V

    goto :goto_0

    .line 259
    .end local v0    # "id":I
    .end local v1    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v3    # "widgetPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->backSelection:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 264
    .restart local v0    # "id":I
    .restart local v1    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 269
    .restart local v3    # "widgetPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :cond_2
    const-string v4, ""

    goto :goto_2

    .line 272
    :cond_3
    iput-boolean v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    .line 273
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getOnLabel()Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getOnLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 274
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetIdentifier()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    :goto_4
    invoke-virtual {v5, v4, v7}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->setGaugeOn(Ljava/lang/String;Z)V

    goto :goto_3

    :cond_4
    const-string v4, ""

    goto :goto_4

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 240
    :sswitch_data_0
    .sparse-switch
        0x7f0e0036 -> :sswitch_0
        0x7f0e0037 -> :sswitch_2
        0x7f0e0047 -> :sswitch_1
    .end sparse-switch

    .line 257
    :pswitch_data_1
    .packed-switch 0x7f0e0047
        :pswitch_2
    .end packed-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 216
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->backSelectionId:I

    .line 217
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 212
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->backSelection:I

    .line 213
    return-void
.end method

.method public final setGaugeType(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V
    .locals 1
    .param p1, "value"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    .line 148
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    goto :goto_0
.end method

.method public final setGaugeView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    return-void
.end method

.method public final setHeadingDataUtil(Lcom/navdy/hud/app/util/HeadingDataUtil;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/util/HeadingDataUtil;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    return-void
.end method

.method public final setRegistered(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->registered:Z

    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 220
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeType:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 233
    :goto_0
    return-void

    .line 222
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f0200f5

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeBackgroundColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeBackgroundColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)I

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 223
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeTitle()Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeTitle$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 226
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->showSelectedCustomView()V

    .line 227
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    if-nez v0, :cond_0

    .line 229
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->getItems()Ljava/util/List;

    .line 231
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->showGauge(I)V

    goto :goto_0

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final setSmartDashWidgetCache(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;)V
    .locals 0
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    return-void
.end method

.method public final setUserPreferenceChanged(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->userPreferenceChanged:Z

    return-void
.end method

.method public final showGauge(I)V
    .locals 8
    .param p1, "pos"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 339
    if-lez p1, :cond_4

    .line 340
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->showSelectedCustomView()V

    .line 341
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/DashboardWidgetView;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v4, v2, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    if-nez v4, :cond_0

    move-object v2, v3

    :cond_0
    check-cast v2, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    .line 343
    .local v2, "oldPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->smartDashWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    if-eqz v4, :cond_3

    add-int/lit8 v5, p1, -0x1

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v1

    .line 345
    .local v1, "newPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :goto_0
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    if-eq v2, v4, :cond_1

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetView()Lcom/navdy/hud/app/view/DashboardWidgetView;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    if-ne v4, v5, :cond_1

    .line 346
    invoke-virtual {v2, v3, v3}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 348
    :cond_1
    if-eqz v1, :cond_2

    .line 349
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 350
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "EXTRA_GRAVITY"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 351
    const-string v3, "EXTRA_IS_ACTIVE"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 352
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v1, v3, v0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 353
    invoke-virtual {v1, v6}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setWidgetVisibleToUser(Z)V

    .line 355
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->gaugeView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setTag(Ljava/lang/Object;)V

    .line 359
    .end local v1    # "newPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    .end local v2    # "oldPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :goto_1
    return-void

    .restart local v2    # "oldPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :cond_3
    move-object v1, v3

    .line 343
    goto :goto_0

    .line 357
    .end local v2    # "oldPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :cond_4
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v5, 0x7f0201e2

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeBackgroundColor()I
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getCenterGaugeBackgroundColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)I

    move-result v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5, v6, v3, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 358
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesTitle()Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->access$getSideGaugesTitle$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 419
    return-void
.end method
