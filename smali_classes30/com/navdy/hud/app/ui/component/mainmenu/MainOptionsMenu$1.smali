.class Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$1;
.super Ljava/lang/Object;
.source "MainOptionsMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 292
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->access$000(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close()V

    .line 293
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->access$100(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v0

    .line 294
    .local v0, "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    const-string v1, "Manual_Zoom"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordOptionSelection(Ljava/lang/String;)V

    .line 295
    new-instance v1, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    invoke-direct {v1, v0}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    const/4 v2, 0x1

    .line 296
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoom(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    move-result-object v1

    const/high16 v2, -0x40800000    # -1.0f

    .line 297
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoomLevel(Ljava/lang/Float;)Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    move-result-object v1

    .line 298
    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v0

    .line 299
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->access$100(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->updateLocalPreferences(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    .line 300
    return-void
.end method
