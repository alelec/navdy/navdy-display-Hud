.class public Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
.super Landroid/widget/RelativeLayout;
.source "MainMenuView2.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

.field public confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0174
    .end annotation
.end field

.field public presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field rightBackground:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0173
    .end annotation
.end field

.field public scrimCover:Landroid/view/View;

.field vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 130
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 133
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    .line 134
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 137
    :cond_0
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 191
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 155
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->takeView(Ljava/lang/Object;)V

    .line 158
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 162
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clear()V

    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->sendCloseEvent()V

    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->cancelLoadingAnimation(I)V

    .line 167
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->dropView(Landroid/view/View;)V

    .line 169
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 141
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 142
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 143
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 144
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 147
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03005f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->scrimCover:Landroid/view/View;

    .line 148
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->scrimCover:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 149
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->scrimCover:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 150
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 176
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handleGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    .line 185
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method performSelectionAnimation(Ljava/lang/Runnable;I)V
    .locals 1
    .param p1, "endAction"    # Ljava/lang/Runnable;
    .param p2, "delay"    # I

    .prologue
    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 196
    return-void
.end method
