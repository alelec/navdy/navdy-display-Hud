.class public Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;
.super Ljava/lang/Object;
.source "RecentContactsMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final backColor:I

.field private static final contactColor:I

.field private static final contactStr:Ljava/lang/String;

.field private static final noContactColor:I

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private backSelection:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private returnToCacheList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 42
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->resources:Landroid/content/res/Resources;

    .line 43
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->backColor:I

    .line 44
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->contactColor:I

    .line 45
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->noContactColor:I

    .line 46
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->contactStr:Ljava/lang/String;

    .line 49
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 50
    .local v5, "title":Ljava/lang/String;
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->backColor:I

    .line 51
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/4 v6, 0x0

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 0
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->bus:Lcom/squareup/otto/Bus;

    .line 67
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 68
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 69
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 70
    return-void
.end method

.method private buildModel(Lcom/navdy/hud/app/framework/recentcall/RecentCall;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 8
    .param p1, "call"    # Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .param p2, "id"    # I

    .prologue
    .line 229
    iget-object v2, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    .line 230
    .local v2, "contactName":Ljava/lang/String;
    iget-object v3, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->formattedNumber:Ljava/lang/String;

    .line 231
    .local v3, "formattedNumber":Ljava/lang/String;
    iget-object v4, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberTypeStr:Ljava/lang/String;

    .line 232
    .local v4, "numberTypeStr":Ljava/lang/String;
    iget v5, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    .line 233
    .local v5, "defaultImageIndex":I
    iget-object v6, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 234
    .local v6, "numberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    iget-object v7, p1, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->initials:Ljava/lang/String;

    .line 236
    .local v7, "initials":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    move v1, p2

    invoke-virtual/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->buildModel(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/navdy/hud/app/framework/contacts/NumberType;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 106
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v0, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 109
    :cond_1
    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    if-eqz v7, :cond_0

    .line 76
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    .line 101
    :goto_0
    return-object v3

    .line 79
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->returnToCacheList:Ljava/util/List;

    .line 81
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    const/4 v0, 0x0

    .line 86
    .local v0, "counter":I
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getRecentCalls()Ljava/util/List;

    move-result-object v6

    .line 87
    .local v6, "recentCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    if-eqz v6, :cond_1

    .line 88
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "recent contacts:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 89
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 90
    .local v5, "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "counter":I
    .local v1, "counter":I
    move v2, v0

    .line 91
    .local v2, "id":I
    invoke-direct {p0, v5, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->buildModel(Lcom/navdy/hud/app/framework/recentcall/RecentCall;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v4

    .line 92
    .local v4, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iput-object v5, v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 93
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->returnToCacheList:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 95
    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    goto :goto_1

    .line 97
    .end local v2    # "id":I
    .end local v4    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v5    # "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_1
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "no recent contacts"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 100
    :cond_2
    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    goto :goto_0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 134
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 135
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 137
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->RECENT_CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x1

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 225
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 1
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 149
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 170
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 2
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 158
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 160
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->returnToCacheList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 161
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "rcm:unload add to cache"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->returnToCacheList:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->addToCache(Ljava/util/List;)V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->returnToCacheList:Ljava/util/List;

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 11
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 193
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "select id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pos:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 195
    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v4, :pswitch_data_0

    .line 203
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->cachedList:Ljava/util/List;

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 204
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 206
    .local v8, "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    new-instance v1, Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v2, v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    iget-object v3, v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    iget-object v4, v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    iget v5, v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->defaultImageIndex:I

    iget-wide v6, v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numericNumber:J

    invoke-direct/range {v1 .. v7}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;IJ)V

    .line 208
    .local v1, "recentContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 209
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->bus:Lcom/squareup/otto/Bus;

    move-object v6, p0

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;-><init>(Ljava/util/List;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/squareup/otto/Bus;)V

    .line 211
    .local v2, "contactOptionsMenu":Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v4, v2, v5, v6, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 215
    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v1    # "recentContact":Lcom/navdy/hud/app/framework/contacts/Contact;
    .end local v2    # "contactOptionsMenu":Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .end local v8    # "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :goto_0
    return v10

    .line 197
    :pswitch_0
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "back"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 198
    const-string v4, "back"

    invoke-static {v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 199
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->backSelection:I

    invoke-virtual {v4, v5, v6, v7, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 195
    :pswitch_data_0
    .packed-switch 0x7f0e0047
        :pswitch_0
    .end packed-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 124
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->backSelection:I

    .line 120
    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f0201ba

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->contactColor:I

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->contactStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method
