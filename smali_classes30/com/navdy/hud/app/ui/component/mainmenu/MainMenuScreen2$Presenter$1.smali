.class Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "MainMenuScreen2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field final synthetic val$endAction:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;->val$endAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 283
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "post back"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;->val$endAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;->val$endAction:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 288
    :cond_0
    return-void
.end method
