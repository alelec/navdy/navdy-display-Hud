.class Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;
.super Ljava/lang/Object;
.source "MainMenuScreen2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field final synthetic val$hasScrollModel:Z

.field final synthetic val$itemSelection:I

.field final synthetic val$menu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field final synthetic val$parentList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;IZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    .line 386
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$menu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$parentList:Ljava/util/List;

    iput p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$itemSelection:I

    iput-boolean p5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$hasScrollModel:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 389
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$300(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 390
    .local v6, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v6, :cond_1

    .line 391
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading menu:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$menu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 392
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$menu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    # setter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$202(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 396
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->setSelectedIcon()V

    .line 397
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performBackAnimation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v2

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 398
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->unlock(Z)V

    .line 399
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v1

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isBindCallsEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setBindCallbacks(Z)V

    .line 400
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$parentList:Ljava/util/List;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$itemSelection:I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v3

    invoke-interface {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isFirstItemEmpty()Z

    move-result v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->val$hasScrollModel:Z

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v5

    invoke-interface {v5}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V

    .line 402
    :cond_1
    return-void
.end method
