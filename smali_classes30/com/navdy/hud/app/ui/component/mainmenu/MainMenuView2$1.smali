.class Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;
.super Ljava/lang/Object;
.source "MainMenuView2.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close()V

    .line 116
    return-void
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z

    move-result v0

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 2
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 67
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 68
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    .line 69
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 71
    :cond_0
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 100
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    .line 101
    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onFastScrollEnd()V

    .line 103
    :cond_0
    return-void
.end method

.method public onFastScrollStart()V
    .locals 2

    .prologue
    .line 91
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 92
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    .line 93
    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onFastScrollStart()V

    .line 95
    :cond_0
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 2
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 76
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    .line 77
    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 79
    :cond_0
    return-void
.end method

.method public onLoad()V
    .locals 2

    .prologue
    .line 55
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onLoad"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->resetSelectedItem()V

    .line 57
    return-void
.end method

.method public onScrollIdle()V
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 84
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    .line 85
    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onScrollIdle()V

    .line 87
    :cond_0
    return-void
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z

    .line 51
    return-void
.end method

.method public showToolTip()V
    .locals 2

    .prologue
    .line 107
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 108
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    .line 109
    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->showToolTip()V

    .line 111
    :cond_0
    return-void
.end method
