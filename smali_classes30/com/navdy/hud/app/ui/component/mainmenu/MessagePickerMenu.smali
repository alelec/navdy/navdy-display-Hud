.class public final Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;
.super Ljava/lang/Object;
.source "MessagePickerMenu.kt"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000 <2\u00020\u0001:\u0001<B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\rJ(\u0010\u0014\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0015\u001a\u0004\u0018\u00010\t2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\tH\u0016J\u0008\u0010\u0017\u001a\u00020\u000fH\u0016J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J\n\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020 H\u0016J\u0008\u0010!\u001a\u00020 H\u0016J\u0018\u0010\"\u001a\u00020 2\u0006\u0010#\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u000fH\u0016J.\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\u00132\u0008\u0010\'\u001a\u0004\u0018\u00010(2\u0006\u0010\u001a\u001a\u00020\u000f2\u0008\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u0008\u0010+\u001a\u00020%H\u0016J\u0008\u0010,\u001a\u00020%H\u0016J\u0012\u0010-\u001a\u00020%2\u0008\u0010.\u001a\u0004\u0018\u00010/H\u0016J\u0008\u00100\u001a\u00020%H\u0016J\u0012\u00101\u001a\u00020%2\u0008\u00102\u001a\u0004\u0018\u000103H\u0016J\u0010\u00104\u001a\u00020 2\u0006\u0010.\u001a\u00020/H\u0016J\u0010\u00105\u001a\u00020%2\u0006\u00106\u001a\u00020\tH\u0002J\u0010\u00107\u001a\u00020%2\u0006\u0010#\u001a\u00020\u000fH\u0016J\u0010\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020\u000fH\u0016J\u0008\u0010:\u001a\u00020%H\u0016J\u0008\u0010;\u001a\u00020%H\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006="
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
        "vscrollComponent",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
        "presenter",
        "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
        "parent",
        "messages",
        "",
        "",
        "contact",
        "Lcom/navdy/hud/app/framework/contacts/Contact;",
        "notificationId",
        "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V",
        "backSelection",
        "",
        "backSelectionId",
        "cachedList",
        "",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "getChildMenu",
        "args",
        "path",
        "getInitialSelection",
        "getItems",
        "getModelfromPos",
        "pos",
        "getScrollIndex",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;",
        "getType",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;",
        "isBindCallsEnabled",
        "",
        "isFirstItemEmpty",
        "isItemClickable",
        "id",
        "onBindToView",
        "",
        "model",
        "view",
        "Landroid/view/View;",
        "state",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;",
        "onFastScrollEnd",
        "onFastScrollStart",
        "onItemSelected",
        "selection",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;",
        "onScrollIdle",
        "onUnload",
        "level",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;",
        "selectItem",
        "sendMessage",
        "message",
        "setBackSelectionId",
        "setBackSelectionPos",
        "n",
        "setSelectedIcon",
        "showToolTip",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

# The value of this static final field might be set in the static constructor
.field private static final SELECTION_ANIMATION_DELAY:I = 0x3e8

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

# The value of this static final field might be set in the static constructor
.field private static final baseMessageId:I = 0x64

.field private static final context:Landroid/content/Context;

.field private static final fluctuatorColor:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final messageColor:I

.field private static final resources:Landroid/content/res/Resources;


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private final contact:Lcom/navdy/hud/app/framework/contacts/Contact;

.field private final messages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationId:Ljava/lang/String;

.field private final parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private final presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private final vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    invoke-direct {v0, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "MessagePickerMenu"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->logger:Lcom/navdy/service/library/log/Logger;

    .line 29
    const/16 v0, 0x3e8

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->SELECTION_ANIMATION_DELAY:I

    .line 30
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->context:Landroid/content/Context;

    .line 31
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->resources:Landroid/content/res/Resources;

    .line 32
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getContext$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0054

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->fluctuatorColor:I

    .line 33
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getContext$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d00a0

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->messageColor:I

    .line 34
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getFluctuatorColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getFluctuatorColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I

    move-result v2

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getFluctuatorColor()I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getFluctuatorColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I

    move-result v4

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f09002d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 35
    const/16 v0, 0x64

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->baseMessageId:I

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V
    .locals 1
    .param p1, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4, "messages"    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6, "notificationId"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "vscrollComponent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messages"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contact"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->messages:Ljava/util/List;

    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iput-object p6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->notificationId:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getBack$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getBaseMessageId$cp()I
    .locals 1

    .prologue
    .line 21
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->baseMessageId:I

    return v0
.end method

.method public static final synthetic access$getContext$cp()Landroid/content/Context;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->context:Landroid/content/Context;

    return-object v0
.end method

.method public static final synthetic access$getFluctuatorColor$cp()I
    .locals 1

    .prologue
    .line 21
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->fluctuatorColor:I

    return v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getMessageColor$cp()I
    .locals 1

    .prologue
    .line 21
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->messageColor:I

    return v0
.end method

.method public static final synthetic access$getMessages$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;)Ljava/util/List;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->messages:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getPresenter$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method public static final synthetic access$getResources$cp()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->resources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static final synthetic access$getSELECTION_ANIMATION_DELAY$cp()I
    .locals 1

    .prologue
    .line 21
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->SELECTION_ANIMATION_DELAY:I

    return v0
.end method

.method public static final synthetic access$sendMessage(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;Ljava/lang/String;)V
    .locals 0
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->sendMessage(Ljava/lang/String;)V

    return-void
.end method

.method private final sendMessage(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 122
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    move-result-object v0

    .line 124
    .local v0, "glanceHandler":Lcom/navdy/hud/app/framework/glance/GlanceHandler;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->notificationId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 125
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->notificationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsSuccessNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsFailedNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2, "args"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3, "path"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->cachedList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v0}, Lkotlin/jvm/internal/TypeIntrinsics;->asMutableList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    .line 56
    :goto_0
    return-object v9

    .line 46
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v9, "list":Ljava/util/ArrayList;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    const/4 v7, 0x0

    .line 51
    .local v7, "counter":I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->messages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 52
    .local v5, "msg":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getBaseMessageId()I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getBaseMessageId$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I

    move-result v0

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "counter":I
    .local v8, "counter":I
    add-int/2addr v0, v7

    const v1, 0x7f02016c

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getMessageColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getMessageColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I

    move-result v2

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getMessageColor()I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getMessageColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I

    move-result v4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v10

    const-string v0, "IconBkColorViewHolder.bu\u2026 messageColor, msg, null)"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .local v10, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v7, v8

    .line 51
    .end local v8    # "counter":I
    .restart local v7    # "counter":I
    goto :goto_1

    .end local v5    # "msg":Ljava/lang/String;
    .end local v10    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_2
    move-object v0, v9

    .line 55
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->cachedList:Ljava/util/List;

    .line 56
    check-cast v9, Ljava/util/List;

    goto :goto_0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p1, "pos"    # I
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->cachedList:Ljava/util/List;

    .line 96
    .local v0, "list":Ljava/util/List;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 97
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 99
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MESSAGE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2, "view"    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 111
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 0
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 5
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v0, :pswitch_data_0

    .line 83
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$selectItem$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$selectItem$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    check-cast v0, Ljava/lang/Runnable;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getSELECTION_ANIMATION_DELAY()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getSELECTION_ANIMATION_DELAY$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 85
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 76
    :pswitch_0
    const-string v0, "back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->backSelection:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->backSelectionId:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->backSelectionId:I

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x7f0e0047
        :pswitch_0
    .end packed-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->backSelectionId:I

    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->backSelection:I

    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f02016c

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getMessageColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getMessageColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I

    move-result v2

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0901a8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 119
    return-void
.end method
