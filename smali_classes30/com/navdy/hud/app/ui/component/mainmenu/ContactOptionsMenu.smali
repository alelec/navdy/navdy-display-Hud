.class Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;
.super Ljava/lang/Object;
.source "ContactOptionsMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final backColor:I

.field private static final callColor:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final messageColor:I

.field private static final resources:Landroid/content/res/Resources;

.field private static final shareLocationColor:I

.field private static final shareTripLocationColor:I


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private final bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private final contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private hasScrollModel:Z

.field private messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

.field private final notifId:Ljava/lang/String;

.field private numberPicked:Lcom/navdy/hud/app/framework/contacts/Contact;

.field private final parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private final presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private final vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const v3, 0x7f0d00a0

    .line 55
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    .line 67
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    .line 71
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->backColor:I

    .line 72
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->callColor:I

    .line 73
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareTripLocationColor:I

    .line 74
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareLocationColor:I

    .line 75
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->messageColor:I

    .line 78
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 79
    .local v5, "title":Ljava/lang/String;
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->backColor:I

    .line 80
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/4 v6, 0x0

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 82
    return-void
.end method

.method constructor <init>(Ljava/util/List;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/squareup/otto/Bus;)V
    .locals 7
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p5, "bus"    # Lcom/squareup/otto/Bus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
            "Lcom/squareup/otto/Bus;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/squareup/otto/Bus;)V

    .line 105
    return-void
.end method

.method constructor <init>(Ljava/util/List;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/squareup/otto/Bus;)V
    .locals 0
    .param p2, "notifId"    # Ljava/lang/String;
    .param p3, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p4, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p5, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p6, "bus"    # Lcom/squareup/otto/Bus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
            "Lcom/squareup/otto/Bus;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    .line 114
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    .line 115
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 116
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 117
    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 118
    iput-object p6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->bus:Lcom/squareup/otto/Bus;

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->makeCall(Lcom/navdy/hud/app/framework/contacts/Contact;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->launchShareTripMenu(Lcom/navdy/hud/app/framework/contacts/Contact;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method private getContactName(Lcom/navdy/hud/app/framework/contacts/Contact;)Ljava/lang/String;
    .locals 1
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 462
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 463
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 467
    :goto_0
    return-object v0

    .line 464
    :cond_0
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 465
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    goto :goto_0

    .line 467
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private getDestinationParcelable(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .locals 22
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 440
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const v16, 0x7f02016c

    const/16 v17, 0x0

    sget v18, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareTripLocationColor:I

    sget v19, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget-object v20, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->NONE:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    const/16 v21, 0x0

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v21}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;DDDDIIIILcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;Lcom/navdy/service/library/events/places/PlaceType;)V

    return-object v0
.end method

.method private launchShareTripMenu(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 14
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 394
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 395
    .local v0, "args":Landroid/os/Bundle;
    const-string v9, "PICKER_DESTINATION_ICON"

    const v10, 0x7f0201a5

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 396
    const-string v9, "PICKER_LEFT_ICON"

    const v10, 0x7f02016c

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 397
    const-string v9, "PICKER_LEFT_ICON_BKCOLOR"

    sget-object v10, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v11, 0x7f0d00a2

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 399
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    .line 400
    .local v4, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    const/4 v2, 0x0

    .line 401
    .local v2, "destination":Lcom/navdy/service/library/events/location/Coordinate;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationRouteRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v7

    .line 402
    .local v7, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v7, :cond_0

    .line 403
    iget-object v2, v7, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 406
    :cond_0
    const-string v9, "PICKER_SHOW_ROUTE_MAP"

    invoke-virtual {v0, v9, v12}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 407
    if-eqz v4, :cond_1

    .line 408
    const-string v9, "PICKER_MAP_START_LAT"

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v10

    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 409
    const-string v9, "PICKER_MAP_START_LNG"

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v10

    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 412
    :cond_1
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->getContactName(Lcom/navdy/hud/app/framework/contacts/Contact;)Ljava/lang/String;

    move-result-object v8

    .line 413
    .local v8, "title":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 414
    const-string v9, "PICKER_MAP_END_LAT"

    iget-object v10, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 415
    const-string v9, "PICKER_MAP_END_LNG"

    iget-object v10, v2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 416
    const-string v9, "PICKER_TITLE"

    sget-object v10, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v11, 0x7f09025e

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v8, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getInstance()Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getMessages()[Ljava/lang/String;

    move-result-object v6

    .line 422
    .local v6, "messages":[Ljava/lang/String;
    array-length v9, v6

    new-array v3, v9, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .line 423
    .local v3, "destinationParcelables":[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, "currentRouteId":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v9, v6

    if-ge v5, v9, :cond_4

    .line 425
    aget-object v9, v6, v5

    invoke-direct {p0, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->getDestinationParcelable(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    move-result-object v9

    aput-object v9, v3, v5

    .line 426
    if-eqz v1, :cond_2

    .line 427
    aget-object v9, v3, v5

    invoke-virtual {v9, v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setRouteId(Ljava/lang/String;)V

    .line 424
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 418
    .end local v1    # "currentRouteId":Ljava/lang/String;
    .end local v3    # "destinationParcelables":[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v5    # "i":I
    .end local v6    # "messages":[Ljava/lang/String;
    :cond_3
    const-string v9, "PICKER_TITLE"

    sget-object v10, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v11, 0x7f09025d

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v8, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 430
    .restart local v1    # "currentRouteId":Ljava/lang/String;
    .restart local v3    # "destinationParcelables":[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .restart local v5    # "i":I
    .restart local v6    # "messages":[Ljava/lang/String;
    :cond_4
    const-string v9, "PICKER_DESTINATIONS"

    invoke-virtual {v0, v9, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 431
    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v10, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;

    invoke-direct {v10, p0, v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$6;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/contacts/Contact;)V

    invoke-virtual {v9, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close(Ljava/lang/Runnable;)V

    .line 437
    return-void
.end method

.method private makeCall(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 4
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 478
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->removeNotification()V

    .line 479
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getCallManager()Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    .line 480
    .local v0, "callManager":Lcom/navdy/hud/app/framework/phonecall/CallManager;
    iget-object v1, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->dial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    return-void
.end method

.method private removeNotification()V
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 473
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 475
    :cond_0
    return-void
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 367
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 195
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItems()Ljava/util/List;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    move-object/from16 v20, v0

    .line 190
    :goto_0
    return-object v20

    .line 127
    :cond_0
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v20, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 129
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :goto_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09003a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 135
    .local v7, "callTitle":Ljava/lang/String;
    const/4 v8, 0x0

    .line 137
    .local v8, "callSubtitle":Ljava/lang/String;
    const v2, 0x7f0e0048

    const v3, 0x7f020172

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->callColor:I

    sget v5, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v6, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->callColor:I

    invoke-static/range {v2 .. v8}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v17

    .line 140
    .local v17, "call":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getInstance()Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->isSynced()Z

    move-result v19

    .line 143
    .local v19, "glympseManagerSynced":Z
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "glympse synced:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 145
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v19, :cond_1

    .line 146
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 147
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09025c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 148
    .local v14, "title":Ljava/lang/String;
    const v9, 0x7f0e004f

    const v10, 0x7f0200d8

    sget v11, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareTripLocationColor:I

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v13, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareTripLocationColor:I

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v22

    .line 152
    .local v22, "shareTrip":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    .end local v14    # "title":Ljava/lang/String;
    .end local v22    # "shareTrip":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    :goto_2
    const/16 v16, 0x0

    .line 165
    .local v16, "addMessage":Z
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v18

    .line 166
    .local v18, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v18, :cond_2

    .line 167
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$7;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 180
    :cond_2
    :goto_3
    if-eqz v16, :cond_3

    .line 181
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 182
    .restart local v14    # "title":Ljava/lang/String;
    const v9, 0x7f0e004a

    const v10, 0x7f02016c

    sget v11, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->messageColor:I

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v13, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->messageColor:I

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v21

    .line 186
    .local v21, "shareLocation":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-interface/range {v20 .. v21}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    .end local v14    # "title":Ljava/lang/String;
    .end local v21    # "shareLocation":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_3
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    goto/16 :goto_0

    .line 131
    .end local v7    # "callTitle":Ljava/lang/String;
    .end local v8    # "callSubtitle":Ljava/lang/String;
    .end local v16    # "addMessage":Z
    .end local v17    # "call":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v18    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v19    # "glympseManagerSynced":Z
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090201

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleViewHolder;->buildModel(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 154
    .restart local v7    # "callTitle":Ljava/lang/String;
    .restart local v8    # "callSubtitle":Ljava/lang/String;
    .restart local v17    # "call":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .restart local v19    # "glympseManagerSynced":Z
    :cond_5
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09025b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 155
    .restart local v14    # "title":Ljava/lang/String;
    const v9, 0x7f0e004e

    const v10, 0x7f0201df

    sget v11, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareLocationColor:I

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v13, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareLocationColor:I

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v21

    .line 159
    .restart local v21    # "shareLocation":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-interface/range {v20 .. v21}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 169
    .end local v14    # "title":Ljava/lang/String;
    .end local v21    # "shareLocation":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .restart local v16    # "addMessage":Z
    .restart local v18    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :pswitch_0
    const/16 v16, 0x1

    .line 170
    goto :goto_3

    .line 173
    :pswitch_1
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->supportsIosSms()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 174
    const/16 v16, 0x1

    goto :goto_3

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 350
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 351
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 353
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 340
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACT_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 345
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 363
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 380
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 374
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 0
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 371
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 14
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v6, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 248
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pos:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 249
    iget v11, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    .line 250
    .local v11, "selectionPos":I
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 251
    .local v10, "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v1, :pswitch_data_0

    .line 335
    :goto_0
    :pswitch_0
    return v13

    .line 253
    :pswitch_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "back"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 254
    const-string v1, "back"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->backSelection:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->backSelectionId:I

    iget-boolean v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->hasScrollModel:Z

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    .line 256
    iput v12, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->backSelectionId:I

    goto :goto_0

    .line 259
    :pswitch_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "call contact"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 260
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v13, :cond_0

    .line 261
    const-string v1, "call_contact"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$1;

    invoke-direct {v2, p0, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;Lcom/navdy/hud/app/framework/contacts/Contact;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 274
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;->CALL:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    const v7, 0x7f020172

    sget v8, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->callColor:I

    new-instance v9, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$2;

    invoke-direct {v9, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)V

    move-object v3, p0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;Ljava/util/List;Ljava/lang/String;IILcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;)V

    .line 281
    .local v0, "numberPickerMenu":Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v0, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 285
    .end local v0    # "numberPickerMenu":Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    :pswitch_3
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "share trip"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 286
    const-string v1, "share_trip"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 287
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v13, :cond_1

    .line 288
    invoke-direct {p0, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->launchShareTripMenu(Lcom/navdy/hud/app/framework/contacts/Contact;)V

    goto :goto_0

    .line 290
    :cond_1
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;->SHARE_TRIP:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    const v7, 0x7f0200d8

    sget v8, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareTripLocationColor:I

    new-instance v9, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$3;

    invoke-direct {v9, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$3;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)V

    move-object v3, p0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;Ljava/util/List;Ljava/lang/String;IILcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;)V

    .line 297
    .restart local v0    # "numberPickerMenu":Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v0, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 301
    .end local v0    # "numberPickerMenu":Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    :pswitch_4
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "share location"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 302
    const-string v1, "share_location"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 303
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v13, :cond_2

    .line 304
    invoke-direct {p0, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->launchShareTripMenu(Lcom/navdy/hud/app/framework/contacts/Contact;)V

    goto/16 :goto_0

    .line 306
    :cond_2
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;->SHARE_TRIP:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    const v7, 0x7f0201df

    sget v8, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->shareLocationColor:I

    new-instance v9, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$4;

    invoke-direct {v9, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$4;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)V

    move-object v3, p0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;Ljava/util/List;Ljava/lang/String;IILcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;)V

    .line 313
    .restart local v0    # "numberPickerMenu":Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v0, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 317
    .end local v0    # "numberPickerMenu":Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    :pswitch_5
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "send message"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 318
    const-string v1, "send_message"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 319
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v13, :cond_4

    .line 320
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    if-nez v1, :cond_3

    .line 321
    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->getCannedMessages()Ljava/util/List;

    move-result-object v5

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    move-object v4, p0

    move-object v6, v10

    invoke-direct/range {v1 .. v7}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    .line 323
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2, v3, v4, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 325
    :cond_4
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;->MESSAGE:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->notifId:Ljava/lang/String;

    const v7, 0x7f02016c

    sget v8, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->messageColor:I

    new-instance v9, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$5;

    invoke-direct {v9, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu$5;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;)V

    move-object v3, p0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;Ljava/util/List;Ljava/lang/String;IILcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;)V

    .line 330
    .restart local v0    # "numberPickerMenu":Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v0, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 251
    :pswitch_data_0
    .packed-switch 0x7f0e0047
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->backSelectionId:I

    .line 211
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 205
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->backSelection:I

    .line 206
    return-void
.end method

.method public setScrollModel(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 484
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->hasScrollModel:Z

    .line 485
    return-void
.end method

.method public setSelectedIcon()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 215
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 216
    .local v1, "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v9, :cond_2

    .line 217
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v5

    iget-object v6, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    sget-object v7, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v5, v6, v7}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getImagePath(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v4

    .line 218
    .local v4, "imagePath":Ljava/io/File;
    invoke-static {v4}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 219
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 220
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v5, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(Landroid/graphics/Bitmap;)V

    .line 235
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "imagePath":Ljava/io/File;
    :goto_0
    iget-object v5, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 236
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v5, v5, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    iget-object v6, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    :goto_1
    return-void

    .line 222
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v4    # "imagePath":Ljava/io/File;
    :cond_0
    iget-object v5, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 223
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v6, 0x7f020217

    const/4 v7, 0x0

    sget-object v8, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_0

    .line 225
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v2

    .line 226
    .local v2, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget v6, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v6

    iget-object v7, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->initials:Ljava/lang/String;

    sget-object v8, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_0

    .line 230
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v4    # "imagePath":Ljava/io/File;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v2

    .line 231
    .restart local v2    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    iget-object v5, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v3

    .line 232
    .local v3, "defaultImageIndex":I
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v6

    iget-object v7, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->initials:Ljava/lang/String;

    sget-object v8, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_0

    .line 238
    .end local v2    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v3    # "defaultImageIndex":I
    :cond_3
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->contacts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ne v5, v9, :cond_4

    iget-object v5, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 239
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v5, v5, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 241
    :cond_4
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v5, v5, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    iget-object v6, v1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 386
    return-void
.end method
