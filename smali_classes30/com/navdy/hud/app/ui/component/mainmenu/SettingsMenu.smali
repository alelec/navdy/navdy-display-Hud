.class public Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;
.super Ljava/lang/Object;
.source "SettingsMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final brightness:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final connectPhone:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final dialFwUpdate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final displayFwUpdate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final factoryReset:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final learningGestures:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final setting:Ljava/lang/String;

.field private static final settingColor:I

.field private static final shutdown:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final systemInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private systemInfoMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v6, 0x0

    .line 38
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 56
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    .line 57
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d007c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->settingColor:I

    .line 58
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->setting:Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 65
    .local v5, "title":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 66
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 69
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090098

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 70
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 71
    const v0, 0x7f0e007b

    const v1, 0x7f0201d4

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->brightness:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 74
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 75
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 76
    const v0, 0x7f0e007c

    const v1, 0x7f0201d5

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->connectPhone:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 79
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 80
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 81
    const v0, 0x7f0e0081

    const v1, 0x7f0201d9

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->learningGestures:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 84
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 85
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 86
    const v0, 0x7f0e0083

    const v1, 0x7f0201ed

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->displayFwUpdate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 89
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09009e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 90
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0083

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 91
    const v0, 0x7f0e007e

    const v1, 0x7f020104

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->dialFwUpdate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 94
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 95
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 96
    const v0, 0x7f0e0082

    const v1, 0x7f0201de

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->shutdown:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 99
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09007e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 100
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 101
    const v7, 0x7f0e0043

    const v8, 0x7f0201dc

    const v9, 0x7f0201dd

    const/4 v11, -0x1

    move v10, v2

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->systemInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 104
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 105
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 106
    const v0, 0x7f0e0080

    const v1, 0x7f0201d7

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->factoryReset:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 107
    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 0
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    .line 125
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 126
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 127
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 128
    return-void
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 212
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->cachedList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 133
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->cachedList:Ljava/util/List;

    .line 159
    :goto_0
    return-object v1

    .line 136
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->brightness:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->connectPhone:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->learningGestures:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->isUpdateAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 145
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->displayFwUpdate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    .line 150
    .local v0, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->isUpdateAvailable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 151
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->dialFwUpdate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->shutdown:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->factoryReset:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->systemInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->cachedList:Ljava/util/List;

    goto :goto_0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 198
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 330
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SETTINGS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 190
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 208
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 219
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 0
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 216
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 8
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 242
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 245
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 325
    :goto_0
    return v4

    .line 247
    :sswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "back"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 248
    const-string v0, "back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->backSelection:I

    iget v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->backSelectionId:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 250
    iput v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->backSelectionId:I

    goto :goto_0

    .line 254
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "brightness"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 255
    const-string v0, "brightness"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 260
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "auto brightness"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 261
    const-string v0, "auto_brightness"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 266
    :sswitch_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "connect phone"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 267
    const-string v0, "connect_phone"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 268
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 269
    .local v6, "args":Landroid/os/Bundle;
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_SWITCH_PHONE:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v6, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 274
    .end local v6    # "args":Landroid/os/Bundle;
    :sswitch_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "software update"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 275
    const-string v0, "software_update"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 276
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 277
    .restart local v6    # "args":Landroid/os/Bundle;
    const-string v0, "UPDATE_REMINDER"

    invoke-virtual {v6, v0, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 278
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v6, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 282
    .end local v6    # "args":Landroid/os/Bundle;
    :sswitch_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "shutdown"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 283
    const-string v0, "shutdown"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 284
    sget-object v0, Lcom/navdy/hud/app/event/Shutdown$Reason;->MENU:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v0}, Lcom/navdy/hud/app/event/Shutdown$Reason;->asBundle()Landroid/os/Bundle;

    move-result-object v6

    .line 285
    .restart local v6    # "args":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v6, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 289
    .end local v6    # "args":Landroid/os/Bundle;
    :sswitch_6
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "factory reset"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 290
    const-string v0, "factory_reset"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FACTORY_RESET:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 295
    :sswitch_7
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Dial pairing"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 296
    const-string v0, "dial_pairing"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 302
    :sswitch_8
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dial update"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 303
    const-string v0, "dial_update"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 304
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 305
    .restart local v6    # "args":Landroid/os/Bundle;
    const-string v0, "UPDATE_REMINDER"

    invoke-virtual {v6, v0, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 306
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v6, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 310
    .end local v6    # "args":Landroid/os/Bundle;
    :sswitch_9
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Learning gestures"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 311
    const-string v0, "learning_gestures"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 316
    :sswitch_a
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "system info"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 317
    const-string v0, "system_info"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->systemInfoMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    if-nez v0, :cond_0

    .line 319
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->systemInfoMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->systemInfoMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    goto/16 :goto_0

    .line 245
    :sswitch_data_0
    .sparse-switch
        0x7f0e0043 -> :sswitch_a
        0x7f0e0047 -> :sswitch_0
        0x7f0e0079 -> :sswitch_2
        0x7f0e007b -> :sswitch_1
        0x7f0e007c -> :sswitch_3
        0x7f0e007d -> :sswitch_7
        0x7f0e007e -> :sswitch_8
        0x7f0e0080 -> :sswitch_6
        0x7f0e0081 -> :sswitch_9
        0x7f0e0082 -> :sswitch_5
        0x7f0e0083 -> :sswitch_4
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 179
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->backSelectionId:I

    .line 180
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 174
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->backSelection:I

    .line 175
    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 184
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f020187

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->settingColor:I

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->setting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 233
    return-void
.end method
