.class public Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;
.super Ljava/lang/Object;
.source "ActiveTripMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final ARRIVAL_EXPAND:I = 0x64

.field private static final MIN_MANEUVER_DISTANCE:I = 0x64

.field private static final TITLE_UPDATE_INTERVAL:I = 0x3e8

.field private static final TOWARDS_PATTERN:Ljava/lang/String;

.field private static final activeTripTitle:Ljava/lang/String;

.field private static final arriveTitle:Ljava/lang/String;

.field private static final arrivedDistance:Ljava/lang/String;

.field private static final arrivedTitle:Ljava/lang/String;

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final endTrip:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final feetLabel:Ljava/lang/String;

.field private static final handler:Landroid/os/Handler;

.field private static final infoModel:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final kiloMetersLabel:Ljava/lang/String;

.field public static final maneuverIconSize:I

.field private static final metersLabel:Ljava/lang/String;

.field private static final milesLabel:Ljava/lang/String;

.field private static final muteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final reportIssue:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final size_18:I

.field private static final size_22:I

.field private static final timeBuilder:Ljava/lang/StringBuilder;

.field private static final timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

.field private static final tripManeuvers:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final tripOptionsColor:I

.field private static final turnNotShown:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final unmuteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;


# instance fields
.field private activeTripDistance:Landroid/widget/TextView;

.field private activeTripDuration:Landroid/widget/TextView;

.field private activeTripEta:Landroid/widget/TextView;

.field private activeTripProgress:Landroid/view/View;

.field private activeTripSubTitleView:Landroid/widget/TextView;

.field private activeTripTitleView:Landroid/widget/TextView;

.field private arrived:Z

.field private backSelection:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private contactList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private contactOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

.field private currentSelection:I

.field private etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

.field private getItemsCalled:Z

.field private label:Ljava/lang/String;

.field private maneuverSelected:Z

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private phoneNumber:Lcom/navdy/service/library/events/contacts/PhoneNumber;

.field private positionOnFirstManeuver:Z

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private registered:Z

.field private reportIssueMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v6, 0x0

    .line 78
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->timeBuilder:Ljava/lang/StringBuilder;

    .line 109
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->turnNotShown:Ljava/util/HashSet;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->TOWARDS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->TOWARDS_PATTERN:Ljava/lang/String;

    .line 115
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->handler:Landroid/os/Handler;

    .line 121
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    .line 122
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 123
    .local v2, "backColor":I
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->tripOptionsColor:I

    .line 125
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->size_22:I

    .line 126
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0b0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->size_18:I

    .line 127
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0b001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->maneuverIconSize:I

    .line 132
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitle:Ljava/lang/String;

    .line 133
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrivedTitle:Ljava/lang/String;

    .line 134
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arriveTitle:Ljava/lang/String;

    .line 135
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->metersLabel:Ljava/lang/String;

    .line 136
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->kiloMetersLabel:Ljava/lang/String;

    .line 137
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->feetLabel:Ljava/lang/String;

    .line 138
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->milesLabel:Ljava/lang/String;

    .line 139
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrivedDistance:Ljava/lang/String;

    .line 142
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 143
    .local v5, "title":Ljava/lang/String;
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 146
    const/high16 v0, 0x7f030000

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ScrollableViewHolder;->buildModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->infoModel:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 149
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 150
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 151
    .local v9, "fluctuatorColor":I
    const v7, 0x7f0e002a

    const v8, 0x7f0200c8

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v11, v9

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->endTrip:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 154
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 155
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 156
    const v7, 0x7f0e002c

    const v8, 0x7f0200c9

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v11, v9

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->muteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 159
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 160
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 161
    const v7, 0x7f0e0038

    const v8, 0x7f0200ca

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v11, v9

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->unmuteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 164
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090227

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 165
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 166
    const v7, 0x7f0e0030

    const v8, 0x7f0201a0

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v11, v9

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->reportIssue:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 169
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleViewHolder;->buildModel(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->tripManeuvers:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 170
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->tripManeuvers:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    const v1, 0x7f0e0002

    iput v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 172
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 174
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->turnNotShown:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->START_TURN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 175
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->turnNotShown:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ARRIVED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 176
    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    .line 212
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->bus:Lcom/squareup/otto/Bus;

    .line 213
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 214
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 215
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 216
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getItemsCalled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)Lcom/navdy/service/library/events/contacts/PhoneNumber;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->phoneNumber:Lcom/navdy/service/library/events/contacts/PhoneNumber;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->label:Ljava/lang/String;

    return-object v0
.end method

.method private buildArrivalBoundingBox(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/here/android/mpa/common/GeoBoundingBox;
    .locals 8
    .param p1, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    const-wide/16 v6, 0x0

    .line 967
    const/4 v1, 0x0

    .line 968
    .local v1, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :try_start_0
    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_1

    .line 969
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v6, v6, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .end local v1    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .local v2, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object v1, v2

    .line 973
    .end local v2    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v1    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    .line 974
    new-instance v0, Lcom/here/android/mpa/common/GeoBoundingBox;

    invoke-direct {v0, v1, v1}, Lcom/here/android/mpa/common/GeoBoundingBox;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 975
    .local v0, "bbox":Lcom/here/android/mpa/common/GeoBoundingBox;
    const/high16 v4, 0x42c80000    # 100.0f

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-virtual {v0, v4, v5}, Lcom/here/android/mpa/common/GeoBoundingBox;->expand(FF)V

    .line 981
    .end local v0    # "bbox":Lcom/here/android/mpa/common/GeoBoundingBox;
    :goto_1
    return-object v0

    .line 970
    :cond_1
    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_0

    .line 971
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v6, v6, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v2    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object v1, v2

    .end local v2    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v1    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    goto :goto_0

    .line 978
    :catch_0
    move-exception v3

    .line 979
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 981
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private buildManeuvers(Ljava/util/List;)V
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 821
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    const/4 v13, 0x0

    .line 822
    .local v13, "fluctuatorColor":I
    const/4 v14, 0x0

    .line 824
    .local v14, "unselectedColor":I
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v26

    .line 825
    .local v26, "l1":J
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v23

    .line 826
    .local v23, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v32

    .line 827
    .local v32, "route":Lcom/here/android/mpa/routing/Route;
    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationLabel()Ljava/lang/String;

    move-result-object v28

    .line 828
    .local v28, "label":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getFullStreetAddress()Ljava/lang/String;

    move-result-object v6

    .line 829
    .local v6, "address":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 830
    move-object/from16 v28, v6

    .line 831
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 832
    const/16 v28, 0x0

    .line 837
    :cond_0
    :goto_0
    if-eqz v32, :cond_12

    .line 838
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 839
    .local v30, "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v17

    .line 840
    .local v17, "allManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v29

    .line 841
    .local v29, "len":I
    const/4 v7, 0x0

    .line 842
    .local v7, "previous":Lcom/here/android/mpa/routing/Maneuver;
    const/4 v2, 0x0

    .line 843
    .local v2, "current":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v18

    .line 844
    .local v18, "currentNavigationManeuver":Lcom/here/android/mpa/routing/Maneuver;
    if-nez v18, :cond_1

    .line 845
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "current maneuver not found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 847
    :cond_1
    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationRouteRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v8

    .line 848
    .local v8, "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    const/16 v22, 0x0

    .line 849
    .local v22, "foundCurrent":Z
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->tripManeuvers:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 851
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_1
    move/from16 v0, v24

    move/from16 v1, v29

    if-ge v0, v1, :cond_7

    .line 852
    move-object v7, v2

    .line 853
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    .line 854
    .restart local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isStartManeuver(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 851
    :cond_2
    :goto_2
    add-int/lit8 v24, v24, 0x1

    goto :goto_1

    .line 834
    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .end local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .end local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v17    # "allManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v18    # "currentNavigationManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v22    # "foundCurrent":Z
    .end local v24    # "i":I
    .end local v29    # "len":I
    .end local v30    # "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    :cond_3
    const/4 v6, 0x0

    goto :goto_0

    .line 858
    .restart local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .restart local v17    # "allManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .restart local v18    # "currentNavigationManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v22    # "foundCurrent":Z
    .restart local v24    # "i":I
    .restart local v29    # "len":I
    .restart local v30    # "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    :cond_4
    if-eqz v22, :cond_5

    .line 859
    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 944
    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .end local v6    # "address":Ljava/lang/String;
    .end local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .end local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v17    # "allManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v18    # "currentNavigationManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v22    # "foundCurrent":Z
    .end local v23    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v24    # "i":I
    .end local v26    # "l1":J
    .end local v28    # "label":Ljava/lang/String;
    .end local v29    # "len":I
    .end local v30    # "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v32    # "route":Lcom/here/android/mpa/routing/Route;
    :catch_0
    move-exception v33

    .line 945
    .local v33, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "addManeuversToList"

    move-object/from16 v0, v33

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 947
    .end local v33    # "t":Ljava/lang/Throwable;
    :goto_3
    return-void

    .line 862
    .restart local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v6    # "address":Ljava/lang/String;
    .restart local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .restart local v17    # "allManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .restart local v18    # "currentNavigationManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v22    # "foundCurrent":Z
    .restart local v23    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .restart local v24    # "i":I
    .restart local v26    # "l1":J
    .restart local v28    # "label":Ljava/lang/String;
    .restart local v29    # "len":I
    .restart local v30    # "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .restart local v32    # "route":Lcom/here/android/mpa/routing/Route;
    :cond_5
    if-nez v18, :cond_6

    .line 863
    :try_start_1
    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 864
    :cond_6
    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 865
    move-object/from16 v0, v30

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 866
    const/16 v22, 0x1

    goto :goto_2

    .line 870
    :cond_7
    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v29

    .line 871
    const/4 v2, 0x0

    .line 872
    const/16 v24, 0x0

    :goto_4
    move/from16 v0, v24

    move/from16 v1, v29

    if-ge v0, v1, :cond_12

    .line 873
    move-object v7, v2

    .line 874
    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    .line 875
    .restart local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    const/4 v9, 0x0

    .line 876
    .local v9, "after":Lcom/here/android/mpa/routing/Maneuver;
    add-int/lit8 v3, v29, -0x1

    move/from16 v0, v24

    if-ge v0, v3, :cond_8

    .line 877
    add-int/lit8 v3, v24, 0x1

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v9, Lcom/here/android/mpa/routing/Maneuver;

    .line 879
    .restart local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    :cond_8
    if-nez v9, :cond_9

    const/4 v3, 0x1

    .line 882
    :goto_5
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v4

    int-to-long v4, v4

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 879
    invoke-static/range {v2 .. v12}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;ZJLjava/lang/String;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/routing/Maneuver;ZZLcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v19

    .line 893
    .local v19, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->shouldShowTurnText(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->turnNotShown:Ljava/util/HashSet;

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    .line 894
    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 895
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 904
    .local v15, "roadInfo":Ljava/lang/String;
    :goto_6
    const v10, 0x7f0e0001

    move-object/from16 v0, v19

    iget v11, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconNowId:I

    move-object/from16 v0, v19

    iget v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconSoonId:I

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v10 .. v16}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v31

    .line 907
    .local v31, "maneuverModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    add-int/lit8 v3, v29, -0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_c

    .line 909
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->buildArrivalBoundingBox(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v3

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 920
    :goto_7
    const/4 v3, 0x1

    move-object/from16 v0, v31

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noTextAnimation:Z

    .line 921
    const/4 v3, 0x1

    move-object/from16 v0, v31

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noImageScaleAnimation:Z

    .line 922
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    sget-object v4, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    if-ne v3, v4, :cond_11

    .line 923
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arriveTitle:Ljava/lang/String;

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 924
    if-eqz v28, :cond_e

    if-eqz v6, :cond_e

    .line 925
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arriveTitle:Ljava/lang/String;

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 926
    move-object/from16 v0, v28

    move-object/from16 v1, v31

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 927
    move-object/from16 v0, v31

    iput-object v6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    .line 940
    :goto_8
    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 872
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_4

    .line 879
    .end local v15    # "roadInfo":Ljava/lang/String;
    .end local v19    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .end local v31    # "maneuverModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 897
    .restart local v19    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_a
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->TOWARDS_PATTERN:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    .line 898
    .local v25, "index":I
    if-nez v25, :cond_b

    .line 899
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->TOWARDS_PATTERN:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "roadInfo":Ljava/lang/String;
    goto :goto_6

    .line 901
    .end local v15    # "roadInfo":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, v19

    iget-object v15, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    .restart local v15    # "roadInfo":Ljava/lang/String;
    goto :goto_6

    .line 912
    .end local v25    # "index":I
    .restart local v31    # "maneuverModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_c
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v20

    .line 913
    .local v20, "distance":I
    const/16 v3, 0x64

    move/from16 v0, v20

    if-ge v0, v3, :cond_d

    .line 914
    rsub-int/lit8 v21, v20, 0x64

    .line 915
    .local v21, "expandBy":I
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v3, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->expandBoundingBox(Lcom/here/android/mpa/common/GeoBoundingBox;I)Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v3

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    goto :goto_7

    .line 917
    .end local v21    # "expandBy":I
    :cond_d
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v3

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    goto :goto_7

    .line 928
    .end local v20    # "distance":I
    :cond_e
    if-eqz v28, :cond_f

    .line 929
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arriveTitle:Ljava/lang/String;

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 930
    move-object/from16 v0, v28

    move-object/from16 v1, v31

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    goto :goto_8

    .line 931
    :cond_f
    if-eqz v6, :cond_10

    .line 932
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arriveTitle:Ljava/lang/String;

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 933
    move-object/from16 v0, v31

    iput-object v6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    goto :goto_8

    .line 935
    :cond_10
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arriveTitle:Ljava/lang/String;

    move-object/from16 v0, v31

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    goto :goto_8

    .line 938
    :cond_11
    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->maneuverIconSize:I

    move-object/from16 v0, v31

    iput v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSize:I

    goto :goto_8

    .line 943
    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .end local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .end local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    .end local v15    # "roadInfo":Ljava/lang/String;
    .end local v17    # "allManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v18    # "currentNavigationManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v19    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .end local v22    # "foundCurrent":Z
    .end local v24    # "i":I
    .end local v29    # "len":I
    .end local v30    # "maneuverList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v31    # "maneuverModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_12
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "time to build maneuvers:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    sub-long v10, v10, v26

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method

.method private expandBoundingBox(Lcom/here/android/mpa/common/GeoBoundingBox;I)Lcom/here/android/mpa/common/GeoBoundingBox;
    .locals 3
    .param p1, "bbox"    # Lcom/here/android/mpa/common/GeoBoundingBox;
    .param p2, "distance"    # I

    .prologue
    .line 986
    int-to-float v1, p2

    int-to-float v2, p2

    :try_start_0
    invoke-virtual {p1, v1, v2}, Lcom/here/android/mpa/common/GeoBoundingBox;->expand(FF)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 990
    :goto_0
    return-object p1

    .line 988
    :catch_0
    move-exception v0

    .line 989
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private fillDurationVia(Ljava/util/Date;)V
    .locals 10
    .param p1, "etaDate"    # Ljava/util/Date;

    .prologue
    const/4 v7, 0x1

    const/16 v9, 0x22

    const/4 v8, 0x0

    .line 675
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->convertDateToEta(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 676
    .local v0, "duration":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentVia()Ljava/lang/String;

    move-result-object v4

    .line 677
    .local v4, "via":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 678
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 679
    .local v1, "durationVia":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 680
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 681
    .local v2, "len":I
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v5, v8, v2, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 682
    new-instance v5, Landroid/text/style/AbsoluteSizeSpan;

    sget v6, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->size_22:I

    invoke-direct {v5, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v1, v5, v8, v2, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 683
    if-eqz v4, :cond_0

    .line 684
    const-string v5, " "

    invoke-virtual {v1, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 685
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    .line 686
    .local v3, "start":I
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v6, 0x7f0902e9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 687
    new-instance v5, Landroid/text/style/AbsoluteSizeSpan;

    sget v6, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->size_18:I

    invoke-direct {v5, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v1, v5, v3, v6, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 689
    .end local v3    # "start":I
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDuration:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 693
    .end local v1    # "durationVia":Landroid/text/SpannableStringBuilder;
    .end local v2    # "len":I
    :goto_0
    return-void

    .line 691
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDuration:Landroid/widget/TextView;

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private getFirstManeuverIndex()I
    .locals 3

    .prologue
    .line 950
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->reportIssue:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 951
    .local v0, "index":I
    if-ltz v0, :cond_0

    add-int/lit8 v1, v0, 0x2

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 952
    add-int/lit8 v1, v0, 0x2

    .line 962
    :goto_0
    return v1

    .line 954
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->unmuteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 955
    if-ltz v0, :cond_1

    add-int/lit8 v1, v0, 0x2

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 956
    add-int/lit8 v1, v0, 0x2

    goto :goto_0

    .line 958
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->muteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 959
    if-ltz v0, :cond_2

    add-int/lit8 v1, v0, 0x2

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 960
    add-int/lit8 v1, v0, 0x2

    goto :goto_0

    .line 962
    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private isMenuLoaded()Z
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSpokenTurnByTurn(Z)V
    .locals 7
    .param p1, "enabled"    # Z

    .prologue
    .line 483
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v1

    .line 484
    .local v1, "prefs":Lcom/navdy/hud/app/maps/NavSessionPreferences;
    iput-boolean p1, v1, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    .line 485
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    .line 486
    .local v4, "userProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v5

    if-nez v5, :cond_0

    .line 487
    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    .line 488
    .local v0, "oldPreferences":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    new-instance v5, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;

    invoke-direct {v5, v0}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    .line 489
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenTurnByTurn(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;

    move-result-object v5

    .line 490
    invoke-virtual {v5}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v3

    .line 491
    .local v3, "updatedPreferences":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    new-instance v5, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;-><init>()V

    sget-object v6, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    .line 492
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;

    move-result-object v5

    iget-object v6, v3, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    .line 493
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;

    move-result-object v5

    .line 494
    invoke-virtual {v5, v3}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->preferences(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;

    move-result-object v5

    .line 495
    invoke-virtual {v5}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    move-result-object v2

    .line 497
    .local v2, "update":Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v5, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 499
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v6, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 501
    .end local v0    # "oldPreferences":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .end local v2    # "update":Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
    .end local v3    # "updatedPreferences":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    :cond_0
    return-void
.end method

.method private switchToArriveState()V
    .locals 6

    .prologue
    const/16 v5, 0x22

    const/4 v4, 0x0

    .line 806
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDuration:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrivedTitle:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 807
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 808
    .local v0, "durationVia":Landroid/text/SpannableStringBuilder;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrivedTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 809
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 810
    .local v1, "len":I
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 811
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->size_22:I

    invoke-direct {v2, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 812
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDuration:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 813
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDistance:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrivedDistance:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 814
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripEta:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 815
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    const/high16 v3, 0x42c80000    # 100.0f

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->setGaugeValue(F)V

    .line 817
    .end local v0    # "durationVia":Landroid/text/SpannableStringBuilder;
    .end local v1    # "len":I
    :cond_0
    return-void
.end method

.method private update()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 633
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->isMenuLoaded()Z

    move-result v3

    if-nez v3, :cond_1

    .line 668
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    .line 637
    .local v1, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationLabel()Ljava/lang/String;

    move-result-object v2

    .line 638
    .local v2, "label":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getFullStreetAddress()Ljava/lang/String;

    move-result-object v0

    .line 640
    .local v0, "address":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 641
    move-object v2, v0

    .line 642
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 643
    const/4 v2, 0x0

    .line 649
    :cond_2
    :goto_1
    if-eqz v2, :cond_4

    .line 650
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 651
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 656
    :goto_2
    if-eqz v0, :cond_5

    .line 657
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripSubTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 658
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripSubTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 659
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripSubTitleView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->requestLayout()V

    .line 664
    :goto_3
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 666
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->switchToArriveState()V

    goto :goto_0

    .line 645
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 653
    :cond_4
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 661
    :cond_5
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripSubTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method


# virtual methods
.method public arrivalEvent(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 776
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrived:Z

    if-eqz v0, :cond_0

    .line 777
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "arrival event"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 784
    :goto_0
    return-void

    .line 780
    :cond_0
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrived:Z

    .line 781
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    .line 782
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->positionOnFirstManeuver:Z

    .line 783
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    goto :goto_0
.end method

.method buildContactModel(Lcom/navdy/hud/app/framework/contacts/Contact;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 13
    .param p1, "c"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "size"    # I

    .prologue
    const/high16 v0, 0x7f0e0000

    const/4 v10, 0x1

    const/4 v4, -0x1

    .line 996
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v7

    .line 997
    .local v7, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    iget-object v2, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 999
    const v1, 0x7f020217

    const v2, 0x7f02021e

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0d0043

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iget-object v5, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v8

    .line 1014
    .local v8, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    .line 1015
    iget-object v0, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    const-string v2, "INITIAL"

    iget-object v4, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->initials:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1016
    return-object v8

    .line 1002
    .end local v8    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    iget v2, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    invoke-virtual {v7, v2}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v1

    .line 1003
    .local v1, "largeImageRes":I
    iget v2, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    invoke-virtual {v7, v2}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceColor(I)I

    move-result v3

    .line 1004
    .local v3, "fluctuator":I
    const/4 v6, 0x0

    .line 1005
    .local v6, "subTitle":Ljava/lang/String;
    if-ne p2, v10, :cond_1

    .line 1006
    iget-object v2, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    sget-object v5, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    if-eq v2, v5, :cond_2

    .line 1007
    iget-object v6, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 1012
    :cond_1
    :goto_1
    const v2, 0x7f02021c

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0900ae

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v5, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v8

    .restart local v8    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    goto :goto_0

    .line 1009
    .end local v8    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_2
    iget-object v6, p1, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    goto :goto_1
.end method

.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 567
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 319
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->positionOnFirstManeuver:Z

    if-eqz v2, :cond_0

    .line 320
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->positionOnFirstManeuver:Z

    .line 321
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 322
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getFirstManeuverIndex()I

    move-result v0

    .line 323
    .local v0, "index":I
    if-lez v0, :cond_0

    .line 324
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initialSelection-m:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 335
    .end local v0    # "index":I
    :goto_0
    return v0

    .line 329
    :cond_0
    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    if-eq v2, v5, :cond_1

    .line 330
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initialSelection:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 331
    iget v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    .line 332
    .local v1, "ret":I
    iput v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    move v0, v1

    .line 333
    goto :goto_0

    .line 335
    .end local v1    # "ret":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItems()Ljava/util/List;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getItemsCalled:Z

    .line 221
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->phoneNumber:Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .line 222
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactList:Ljava/util/List;

    .line 223
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->label:Ljava/lang/String;

    .line 224
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v14, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->infoModel:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v13

    .line 229
    .local v13, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v13}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v15

    .line 230
    .local v15, "navigationOn":Z
    if-eqz v15, :cond_1

    .line 232
    invoke-virtual {v13}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationRouteRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v18

    .line 233
    .local v18, "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v18, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v2, :cond_0

    .line 234
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v0, v2, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    move-object/from16 v16, v0

    .line 235
    .local v16, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    if-eqz v16, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 237
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .line 238
    .local v12, "firstNumber":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0071

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 239
    .local v4, "callColor":I
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 240
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->label:Ljava/lang/String;

    .line 246
    :goto_0
    const v2, 0x7f0e0048

    const v3, 0x7f020172

    sget v5, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f090048

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->label:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v8, v19

    .line 248
    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iget-object v6, v12, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    invoke-static {v6}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move v6, v4

    .line 246
    invoke-static/range {v2 .. v8}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v10

    .line 249
    .local v10, "call":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-interface {v14, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->phoneNumber:Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .line 262
    .end local v4    # "callColor":I
    .end local v10    # "call":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v12    # "firstNumber":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    .end local v16    # "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    :cond_0
    :goto_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->endTrip:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v2

    if-nez v2, :cond_1

    .line 267
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v17

    .line 268
    .local v17, "prefs":Lcom/navdy/hud/app/maps/NavSessionPreferences;
    move-object/from16 v0, v17

    iget-boolean v2, v0, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    if-eqz v2, :cond_8

    .line 269
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->muteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0901cc

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 270
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->muteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    .end local v17    # "prefs":Lcom/navdy/hud/app/maps/NavSessionPreferences;
    .end local v18    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_1
    :goto_2
    if-eqz v15, :cond_2

    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->canReportIssue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 280
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->reportIssue:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    :cond_2
    if-eqz v15, :cond_3

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v2

    if-nez v2, :cond_3

    .line 285
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->buildManeuvers(Ljava/util/List;)V

    .line 288
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showScrimCover()V

    .line 289
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    .line 290
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getItems:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v2

    if-nez v2, :cond_9

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showMap()V

    .line 295
    const/16 v11, 0x3e8

    .line 301
    .local v11, "delay":I
    :goto_3
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)V

    int-to-long v6, v11

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 314
    return-object v14

    .line 241
    .end local v11    # "delay":I
    .restart local v4    # "callColor":I
    .restart local v12    # "firstNumber":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    .restart local v16    # "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    .restart local v18    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_4
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 242
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->label:Ljava/lang/String;

    goto/16 :goto_0

    .line 244
    :cond_5
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->label:Ljava/lang/String;

    goto/16 :goto_0

    .line 251
    .end local v4    # "callColor":I
    .end local v12    # "firstNumber":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    :cond_6
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v2, v2, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    if-eqz v2, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v2, v2, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 253
    new-instance v2, Ljava/util/ArrayList;

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v3, v3, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactList:Ljava/util/List;

    .line 254
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v2, v2, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/navdy/service/library/events/contacts/Contact;

    .line 255
    .local v9, "c":Lcom/navdy/service/library/events/contacts/Contact;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactList:Ljava/util/List;

    new-instance v5, Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {v5, v9}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Lcom/navdy/service/library/events/contacts/Contact;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 257
    .end local v9    # "c":Lcom/navdy/service/library/events/contacts/Contact;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactList:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/contacts/Contact;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->buildContactModel(Lcom/navdy/hud/app/framework/contacts/Contact;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 272
    .end local v16    # "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    .restart local v17    # "prefs":Lcom/navdy/hud/app/maps/NavSessionPreferences;
    :cond_8
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->unmuteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0902db

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 273
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->unmuteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 297
    .end local v17    # "prefs":Lcom/navdy/hud/app/maps/NavSessionPreferences;
    .end local v18    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->enableMapViews()V

    .line 298
    const/16 v11, 0x64

    .restart local v11    # "delay":I
    goto/16 :goto_3
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 527
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 528
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 530
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 505
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ACTIVE_TRIP:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 536
    const/4 v0, 0x1

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 629
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 4
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 510
    if-ne p2, v2, :cond_0

    .line 521
    :goto_0
    :pswitch_0
    return v1

    .line 513
    :cond_0
    invoke-virtual {p0, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 514
    .local v0, "m":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-eqz v0, :cond_1

    .line 515
    iget v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    packed-switch v3, :pswitch_data_0

    :cond_1
    move v1, v2

    .line 521
    goto :goto_0

    .line 515
    :pswitch_data_0
    .packed-switch 0x7f0e0001
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 3
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 541
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SCROLL_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    if-ne v1, v2, :cond_1

    .line 542
    check-cast p2, Landroid/view/ViewGroup;

    .end local p2    # "view":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 543
    .local v0, "viewGroup":Landroid/view/ViewGroup;
    const v1, 0x7f0e00b3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitleView:Landroid/widget/TextView;

    .line 544
    const v1, 0x7f0e00b4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripSubTitleView:Landroid/widget/TextView;

    .line 545
    const v1, 0x7f0e00b6

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDuration:Landroid/widget/TextView;

    .line 546
    const v1, 0x7f0e00b8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDistance:Landroid/widget/TextView;

    .line 547
    const v1, 0x7f0e00b9

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripEta:Landroid/widget/TextView;

    .line 548
    const v1, 0x7f0e00b7

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripProgress:Landroid/view/View;

    .line 549
    new-instance v1, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripProgress:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    .line 550
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->setMinValue(F)V

    .line 551
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->setMaxGaugeValue(F)V

    .line 552
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripProgress:Landroid/view/View;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 553
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->update()V

    .line 555
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->registered:Z

    if-nez v1, :cond_0

    .line 556
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 557
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->registered:Z

    .line 558
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "registered bus"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 561
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postManeuverDisplay()V

    .line 563
    .end local v0    # "viewGroup":Landroid/view/ViewGroup;
    :cond_1
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 622
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 619
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 3
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const v2, 0x7f0e0001

    .line 595
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    if-ne v1, v2, :cond_2

    .line 597
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->maneuverSelected:Z

    .line 598
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 599
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-eqz v0, :cond_1

    iget v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    if-ne v1, v2, :cond_1

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    instance-of v1, v1, Lcom/here/android/mpa/common/GeoBoundingBox;

    if-eqz v1, :cond_1

    .line 603
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v1, Lcom/here/android/mpa/common/GeoBoundingBox;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showBoundingBox(Lcom/here/android/mpa/common/GeoBoundingBox;)V

    .line 613
    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    :goto_0
    return-void

    .line 605
    .restart local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "leg - no bbox"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 607
    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_2
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->maneuverSelected:Z

    if-eqz v1, :cond_0

    .line 609
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->maneuverSelected:Z

    .line 610
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentRouteBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showBoundingBox(Lcom/here/android/mpa/common/GeoBoundingBox;)V

    .line 611
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "route bbox"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onManeuverChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 788
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onManeuverChangeEvent:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 789
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$6;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$ManeuverEvent$Type:[I

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 803
    :goto_0
    return-void

    .line 792
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentSelection()I

    move-result v6

    .line 793
    .local v6, "curPos":I
    if-le v6, v5, :cond_0

    .line 794
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getFirstManeuverIndex()I

    move-result v7

    .line 795
    .local v7, "maneuverPos":I
    if-lt v6, v7, :cond_0

    .line 796
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->positionOnFirstManeuver:Z

    .line 799
    .end local v7    # "maneuverPos":I
    :cond_0
    iput v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    .line 800
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    goto :goto_0

    .line 789
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onManeuverDisplayEvent(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 13
    .param p1, "maneuverDisplay"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 697
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->isMenuLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->isNavigating()Z

    move-result v0

    if-nez v0, :cond_1

    .line 754
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 701
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrived:Z

    if-eqz v0, :cond_2

    .line 702
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already arrived"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 705
    :cond_2
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->arrived:Z

    .line 706
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    .line 707
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    goto :goto_0

    .line 709
    :cond_3
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->fillDurationVia(Ljava/util/Date;)V

    .line 714
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-virtual {v0}, Lcom/navdy/hud/app/common/TimeHelper;->getFormat()Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    if-ne v0, v1, :cond_5

    .line 715
    iget-object v8, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    .line 721
    .local v8, "eta":Ljava/lang/String;
    :goto_1
    if-eqz v8, :cond_6

    .line 722
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripEta:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    :goto_2
    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistance:F

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToMeters(FLcom/navdy/service/library/events/navigation/DistanceUnit;)F

    move-result v11

    .line 728
    .local v11, "totalDistanceInMeters":F
    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemaining:F

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemainingUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToMeters(FLcom/navdy/service/library/events/navigation/DistanceUnit;)F

    move-result v10

    .line 729
    .local v10, "remainingDistanceInMeters":F
    sub-float v6, v11, v10

    .line 730
    .local v6, "coveredDistanceInMeters":F
    const/4 v0, 0x0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_7

    cmpl-float v0, v11, v6

    if-lez v0, :cond_7

    div-float v0, v6, v11

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v9, v0

    .line 731
    .local v9, "percentage":I
    :goto_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    if-eqz v0, :cond_4

    .line 732
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->etaProgressDrawable:Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;

    int-to-float v1, v9

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/drawable/ETAProgressDrawable;->setGaugeValue(F)V

    .line 733
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripProgress:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 735
    :cond_4
    const-string v12, ""

    .line 736
    .local v12, "unit":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$6;->$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit:[I

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemainingUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/navigation/DistanceUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 751
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemaining:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 752
    .local v7, "distance":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDistance:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 717
    .end local v6    # "coveredDistanceInMeters":F
    .end local v7    # "distance":Ljava/lang/String;
    .end local v8    # "eta":Ljava/lang/String;
    .end local v9    # "percentage":I
    .end local v10    # "remainingDistanceInMeters":F
    .end local v11    # "totalDistanceInMeters":F
    .end local v12    # "unit":Ljava/lang/String;
    :cond_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->timeBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 718
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->timeBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime12Hour(Ljava/util/Date;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v8

    .line 719
    .restart local v8    # "eta":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->timeBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 724
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripEta:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .restart local v6    # "coveredDistanceInMeters":F
    .restart local v10    # "remainingDistanceInMeters":F
    .restart local v11    # "totalDistanceInMeters":F
    :cond_7
    move v9, v3

    .line 730
    goto :goto_3

    .line 738
    .restart local v9    # "percentage":I
    .restart local v12    # "unit":Ljava/lang/String;
    :pswitch_0
    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->metersLabel:Ljava/lang/String;

    .line 739
    goto :goto_4

    .line 741
    :pswitch_1
    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->kiloMetersLabel:Ljava/lang/String;

    .line 742
    goto :goto_4

    .line 744
    :pswitch_2
    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->milesLabel:Ljava/lang/String;

    .line 745
    goto :goto_4

    .line 747
    :pswitch_3
    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->feetLabel:Ljava/lang/String;

    goto :goto_4

    .line 736
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onRouteChangeEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 759
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentSelection()I

    move-result v6

    .line 760
    .local v6, "curPos":I
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRouteChangeEvent reason="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " curPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 761
    if-le v6, v5, :cond_0

    .line 762
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getFirstManeuverIndex()I

    move-result v7

    .line 763
    .local v7, "maneuverPos":I
    if-lt v6, v7, :cond_1

    .line 764
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->positionOnFirstManeuver:Z

    .line 765
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRouteChangeEvent showing maneuver curPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maneuverPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 770
    .end local v7    # "maneuverPos":I
    :cond_0
    :goto_0
    iput v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->currentSelection:I

    .line 771
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    .line 772
    return-void

    .line 767
    .restart local v7    # "maneuverPos":I
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRouteChangeEvent not showing maneuver curPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maneuverPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 616
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 4
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 572
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->getItemsCalled:Z

    .line 573
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->hideScrimCover()V

    .line 574
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setViewBackgroundColor(I)V

    .line 576
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->disableMapViews()V

    .line 579
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->registered:Z

    if-eqz v0, :cond_1

    .line 580
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->registered:Z

    .line 581
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 582
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unregistered bus"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 585
    :cond_1
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitleView:Landroid/widget/TextView;

    .line 586
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripSubTitleView:Landroid/widget/TextView;

    .line 587
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDuration:Landroid/widget/TextView;

    .line 588
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripDistance:Landroid/widget/TextView;

    .line 589
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripEta:Landroid/widget/TextView;

    .line 590
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripProgress:Landroid/view/View;

    .line 591
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 8
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 362
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 363
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 479
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 365
    :sswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "back"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setViewBackgroundColor(I)V

    .line 369
    :cond_0
    const-string v0, "back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 370
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getActivityTraySelection()I

    move-result v7

    .line 371
    .local v7, "index":I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->getOffSetFromPos(I)I

    move-result v6

    .line 372
    .local v6, "offset":I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->backSelection:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZI)V

    goto :goto_0

    .line 376
    .end local v6    # "offset":I
    .end local v7    # "index":I
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "end trip"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setNavEnded()V

    .line 379
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setViewBackgroundColor(I)V

    .line 381
    :cond_1
    const-string v0, "End_Trip"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 382
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 397
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "mute tbt"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 399
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setViewBackgroundColor(I)V

    .line 401
    :cond_2
    const-string v0, "mute_Turn_By_Turn"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordOptionSelection(Ljava/lang/String;)V

    .line 402
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$3;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)V

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 409
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->muteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0901d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 410
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock(Z)V

    .line 411
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(IZ)V

    .line 412
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lock()V

    .line 413
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->setSpokenTurnByTurn(Z)V

    goto/16 :goto_0

    .line 417
    :sswitch_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unmute tbt"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 418
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 419
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setViewBackgroundColor(I)V

    .line 421
    :cond_3
    const-string v0, "unmute_Turn_By_Turn"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordOptionSelection(Ljava/lang/String;)V

    .line 422
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$4;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)V

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 430
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->unmuteTbtAudio:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->resources:Landroid/content/res/Resources;

    const v2, 0x7f0901d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 431
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock(Z)V

    .line 432
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(IZ)V

    .line 433
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lock()V

    .line 434
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->setSpokenTurnByTurn(Z)V

    goto/16 :goto_0

    .line 438
    :sswitch_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "report-issue"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 439
    const-string v0, "report-issue"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 441
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->reportIssueMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

    if-nez v0, :cond_4

    .line 442
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->reportIssueMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

    .line 444
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->reportIssueMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 448
    :sswitch_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "call place"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 450
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setViewBackgroundColor(I)V

    .line 452
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu$5;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 469
    :sswitch_6
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "contact"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    if-nez v0, :cond_6

    .line 472
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactList:Ljava/util/List;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->bus:Lcom/squareup/otto/Bus;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;-><init>(Ljava/util/List;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    .line 473
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;->setScrollModel(Z)V

    .line 475
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->contactOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 363
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e0000 -> :sswitch_6
        0x7f0e002a -> :sswitch_1
        0x7f0e002c -> :sswitch_2
        0x7f0e0030 -> :sswitch_4
        0x7f0e0038 -> :sswitch_3
        0x7f0e0047 -> :sswitch_0
        0x7f0e0048 -> :sswitch_5
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 349
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 345
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->backSelection:I

    .line 346
    return-void
.end method

.method public setSelectedIcon()V
    .locals 6

    .prologue
    .line 353
    const v1, 0x7f0200d8

    .line 354
    .local v1, "icon":I
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->tripOptionsColor:I

    .line 355
    .local v0, "color":I
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->activeTripTitle:Ljava/lang/String;

    .line 356
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v3, v1, v0, v4, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 357
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 625
    return-void
.end method
