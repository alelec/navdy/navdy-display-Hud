.class Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;
.super Ljava/lang/Object;
.source "MainOptionsMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .prologue
    .line 321
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 324
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->access$000(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close()V

    .line 325
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/debug/DriveRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/DriveRecorder;->isDemoPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/debug/DriveRecorder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->RESUME:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V

    .line 330
    :goto_0
    return-void

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/debug/DriveRecorder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/debug/DriveRecorder$Action;->PLAY:Lcom/navdy/hud/app/debug/DriveRecorder$Action;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/debug/DriveRecorder;->performDemoPlaybackAction(Lcom/navdy/hud/app/debug/DriveRecorder$Action;Z)V

    goto :goto_0
.end method
