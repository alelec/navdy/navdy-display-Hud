.class public Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "MainMenuScreen2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private animateIn:Z

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private closed:Z

.field private currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private firstLoad:Z

.field private handledSelection:Z

.field private leftView:[Landroid/view/View;

.field private mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

.field private mapShown:Z

.field private menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

.field private menuPath:Ljava/lang/String;

.field private navEnded:Z

.field private registered:Z

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

.field tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private findEntry(Ljava/util/List;I)I
    .locals 3
    .param p2, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 542
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 543
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 544
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    if-ne v2, p2, :cond_0

    .line 548
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 543
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 548
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method buildMenuPath(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 6
    .param p1, "menu"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    .param p2, "menuPath"    # Ljava/lang/String;

    .prologue
    .line 516
    :try_start_0
    const-string v4, "/"

    invoke-virtual {p2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 517
    .local v2, "index":I
    if-eqz v2, :cond_1

    .line 518
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "menu_path does not start with slash"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 537
    .end local v2    # "index":I
    .end local p1    # "menu":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    :cond_0
    :goto_0
    return-object p1

    .line 521
    .restart local v2    # "index":I
    .restart local p1    # "menu":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 522
    .local v1, "element":Ljava/lang/String;
    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 523
    if-ltz v2, :cond_2

    .line 524
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 525
    const/4 v4, 0x0

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 529
    :goto_1
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v1, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 530
    .local v0, "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    move-object p1, v0

    .line 533
    goto :goto_0

    .line 527
    .end local v0    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    :cond_2
    const/4 p2, 0x0

    goto :goto_1

    .line 535
    .end local v1    # "element":Ljava/lang/String;
    .end local v2    # "index":I
    :catch_0
    move-exception v3

    .line 536
    .local v3, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method cancelLoadingAnimation(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 553
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 554
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->cancelLoadingAnimation(I)V

    .line 556
    :cond_0
    return-void
.end method

.method public cleanMapFluctuator()V
    .locals 3

    .prologue
    .line 831
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "cleanMapFluctuator"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 832
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v0

    .line 833
    .local v0, "navigationView":Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    if-eqz v0, :cond_0

    .line 834
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupFluctuator()V

    .line 836
    :cond_0
    return-void
.end method

.method close()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SEARCH:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_0

    .line 262
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchClosed()V

    .line 264
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close(Ljava/lang/Runnable;)V

    .line 265
    return-void
.end method

.method close(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 268
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->closed:Z

    if-eqz v1, :cond_1

    .line 269
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "already closed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->closed:Z

    .line 274
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "close"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 275
    const-string v1, "exit"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 278
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 279
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 280
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateOut(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public disableMapViews()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, -0x1000000

    .line 867
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 868
    .local v1, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-nez v1, :cond_0

    .line 884
    :goto_0
    return-void

    .line 871
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "disableMapViews"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 872
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->setBackgroundColor(I)V

    .line 873
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 874
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 875
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->leftView:[Landroid/view/View;

    if-eqz v2, :cond_2

    .line 876
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->leftView:[Landroid/view/View;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 877
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->leftView:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 876
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 879
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->leftView:[Landroid/view/View;

    .line 881
    .end local v0    # "i":I
    :cond_2
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 882
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 883
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->rightBackground:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public enableMapViews()V
    .locals 9

    .prologue
    const/high16 v8, -0x1000000

    const/4 v7, 0x0

    .line 847
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 848
    .local v3, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-nez v3, :cond_0

    .line 864
    :goto_0
    return-void

    .line 851
    :cond_0
    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 852
    .local v2, "nChilds":I
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "enableMapViews:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 853
    new-array v4, v2, [Landroid/view/View;

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->leftView:[Landroid/view/View;

    .line 854
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 855
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->leftView:[Landroid/view/View;

    iget-object v5, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v5, v5, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    aput-object v5, v4, v0

    .line 854
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 857
    :cond_1
    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 858
    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 859
    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030001

    invoke-virtual {v4, v5, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 860
    .local v1, "maskView":Landroid/view/View;
    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 861
    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 862
    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 863
    iget-object v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->rightBackground:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method getConfirmationLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .locals 2

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 477
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 478
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .line 480
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    return-object v0
.end method

.method public getCurrentRouteBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;
    .locals 3

    .prologue
    .line 782
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v0

    .line 783
    .local v0, "routeId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 784
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v1

    .line 785
    .local v1, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v1, :cond_0

    .line 786
    iget-object v2, v1, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Route;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v2

    .line 789
    .end local v1    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method getCurrentSelection()I
    .locals 2

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 501
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 502
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v1

    .line 504
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentSubSelectionId()I
    .locals 3

    .prologue
    .line 686
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 687
    .local v1, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v1, :cond_0

    .line 688
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentViewHolder()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    move-result-object v0

    .line 689
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    instance-of v2, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    if-eqz v2, :cond_0

    .line 690
    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->getCurrentSelectionId()I

    move-result v2

    .line 693
    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getCurrentSubSelectionPos()I
    .locals 3

    .prologue
    .line 697
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 698
    .local v1, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v1, :cond_0

    .line 699
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentViewHolder()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    move-result-object v0

    .line 700
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    instance-of v2, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    if-eqz v2, :cond_0

    .line 701
    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->getCurrentSelection()I

    move-result v2

    .line 704
    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public hideMap()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 797
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mapShown:Z

    if-nez v6, :cond_1

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mapShown:Z

    .line 801
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 802
    .local v3, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v3, :cond_0

    .line 805
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "hideMap"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 806
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->disableMapViews()V

    .line 809
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-eqz v6, :cond_2

    .line 810
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "hideMap switchbackmode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 811
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v1

    .line 812
    .local v1, "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1, v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    .line 816
    .end local v1    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v2

    .line 817
    .local v2, "navigationView":Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->navEnded:Z

    if-nez v6, :cond_3

    .line 818
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    .line 819
    .local v0, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 820
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 821
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->navEnded:Z

    .line 827
    .end local v0    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_3
    :goto_1
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->navEnded:Z

    if-nez v6, :cond_5

    :goto_2
    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchBackfromRouteSearchMode(Z)V

    goto :goto_0

    .line 824
    .restart local v0    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_4
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->navEnded:Z

    goto :goto_1

    .end local v0    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_5
    move v4, v5

    .line 827
    goto :goto_2
.end method

.method public hideScrimCover()V
    .locals 3

    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 665
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 666
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "scrim-hide"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 667
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->scrimCover:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 669
    :cond_0
    return-void
.end method

.method public hideToolTip()V
    .locals 2

    .prologue
    .line 679
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 680
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 681
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->hideToolTip()V

    .line 683
    :cond_0
    return-void
.end method

.method init(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;Landroid/os/Bundle;)V
    .locals 11
    .param p1, "view"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 204
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode:[I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 233
    :goto_0
    return-void

    .line 207
    :pswitch_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->bus:Lcom/squareup/otto/Bus;

    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-direct {v0, v3, v4, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .line 208
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuPath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    invoke-virtual {p0, v0, v5, v10, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuPath:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->buildMenuPath(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v8

    .line 213
    .local v8, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-virtual {p0, v8, v5, v10, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 219
    .end local v8    # "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    :pswitch_1
    const-string v0, "CONTACTS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 220
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    const-string v0, "NOTIF_ID"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "notifId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Parcelable;

    .line 223
    .local v9, "p":Landroid/os/Parcelable;
    check-cast v9, Lcom/navdy/hud/app/framework/contacts/Contact;

    .end local v9    # "p":Landroid/os/Parcelable;
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 225
    :cond_1
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->bus:Lcom/squareup/otto/Bus;

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 226
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-virtual {p0, v0, v5, v10, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 229
    .end local v1    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .end local v2    # "notifId":Ljava/lang/String;
    .end local v7    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    :pswitch_2
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;->SNAP_SHOT:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

    invoke-direct {v0, v3, p0, v5, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-virtual {p0, v0, v5, v10, v10}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method isClosed()Z
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->closed:Z

    return v0
.end method

.method isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 3
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-interface {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isItemClickable(II)Z

    move-result v0

    .line 301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMapShown()Z
    .locals 1

    .prologue
    .line 708
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mapShown:Z

    return v0
.end method

.method loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V
    .locals 6
    .param p1, "menu"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;
    .param p3, "backSelection"    # I
    .param p4, "backSelectionId"    # I

    .prologue
    .line 313
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    .line 314
    return-void
.end method

.method loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V
    .locals 7
    .param p1, "menu"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;
    .param p3, "backSelection"    # I
    .param p4, "backSelectionId"    # I
    .param p5, "hasScrollModel"    # Z

    .prologue
    .line 321
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZI)V

    .line 322
    return-void
.end method

.method loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZI)V
    .locals 18
    .param p1, "menu"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;
    .param p3, "backSelection"    # I
    .param p4, "backSelectionId"    # I
    .param p5, "hasScrollModel"    # Z
    .param p6, "xOffset"    # I

    .prologue
    .line 330
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->hideToolTip()V

    .line 331
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ROOT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->isMapShown()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->disableMapViews()V

    .line 334
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 335
    .local v17, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v17, :cond_1

    .line 336
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->firstLoad:Z

    if-eqz v2, :cond_2

    .line 337
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loading menu:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 338
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->setSelectedIcon()V

    .line 340
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->firstLoad:Z

    .line 341
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isBindCallsEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setBindCallbacks(Z)V

    .line 342
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getItems()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getInitialSelection()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v7}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isFirstItemEmpty()Z

    move-result v7

    invoke-virtual {v2, v3, v4, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZ)V

    .line 424
    :cond_1
    :goto_0
    return-void

    .line 344
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    invoke-virtual/range {p2 .. p2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 346
    :pswitch_0
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loading menu:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move/from16 v0, p3

    invoke-interface {v2, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v14

    .line 348
    .local v14, "item":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v2, :cond_3

    .line 349
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    .line 351
    :cond_3
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 352
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move/from16 v0, p3

    invoke-interface {v2, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->setBackSelectionPos(I)V

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->setSelectedIcon()V

    .line 354
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-direct {v3, v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Z)V

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v14}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performEnterAnimation(Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_0

    .line 369
    .end local v14    # "item":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :pswitch_1
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backSelectionId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 370
    invoke-interface/range {p1 .. p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getItems()Ljava/util/List;

    move-result-object v5

    .line 371
    .local v5, "parentList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    move/from16 v16, p3

    .line 372
    .local v16, "selection":I
    if-eqz p4, :cond_5

    .line 373
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v5, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->findEntry(Ljava/util/List;I)I

    move-result v13

    .line 374
    .local v13, "index":I
    const/4 v2, -0x1

    if-eq v13, v2, :cond_4

    .line 375
    move/from16 v16, v13

    .line 377
    :cond_4
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backSelectionId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " index="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 379
    .end local v13    # "index":I
    :cond_5
    const/4 v14, 0x0

    .line 380
    .restart local v14    # "item":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_6

    .line 381
    move/from16 v0, v16

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "item":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    check-cast v14, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 383
    .restart local v14    # "item":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_6
    move-object v15, v14

    .line 384
    .local v15, "parentItem":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move/from16 v6, v16

    .line 385
    .local v6, "itemSelection":I
    move-object/from16 v0, v17

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$3;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;IZ)V

    const/4 v3, 0x0

    move/from16 v0, p6

    invoke-virtual {v8, v2, v3, v15, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performBackAnimation(Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V

    goto/16 :goto_0

    .line 407
    .end local v5    # "parentList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    .end local v6    # "itemSelection":I
    .end local v14    # "item":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v15    # "parentItem":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v16    # "selection":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-object/from16 v0, p1

    if-ne v2, v0, :cond_7

    .line 408
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "refresh current menu:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 409
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getItems()Ljava/util/List;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getInitialSelection()I

    move-result v9

    const/4 v10, 0x1

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->getFastScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v12

    move/from16 v11, p5

    invoke-virtual/range {v7 .. v12}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V

    goto/16 :goto_0

    .line 411
    :cond_7
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "refresh current menu different cur="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " passed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 416
    :pswitch_3
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "refresh root menu:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 417
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->setSelectedIcon()V

    .line 419
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-interface/range {p1 .. p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getInitialSelection()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v7}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isFirstItemEmpty()Z

    move-result v7

    invoke-virtual {v2, v3, v4, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZ)V

    goto/16 :goto_0

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 567
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-nez v0, :cond_0

    .line 597
    :goto_0
    :pswitch_0
    return-void

    .line 571
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 576
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 578
    :pswitch_2
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disconnected:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 579
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    if-eqz v0, :cond_1

    .line 580
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->clearState()V

    .line 582
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_2

    .line 583
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "disconnected: refresh main menu"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 584
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {p0, v0, v1, v3, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 586
    :cond_2
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "disconnected: refresh menu,back to root"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 587
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ROOT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {p0, v0, v1, v3, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 592
    :pswitch_3
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close()V

    goto :goto_0

    .line 571
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 576
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onDeviceInfoAvailable(Lcom/navdy/hud/app/event/DeviceInfoAvailable;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/event/DeviceInfoAvailable;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 601
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    if-eq v0, v1, :cond_1

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeviceInfoAvailable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 607
    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getInitialSelection()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    const v1, 0x7f0e003d

    if-ne v0, v1, :cond_0

    .line 609
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "connected: refresh main menu"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 610
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {p0, v0, v1, v3, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 154
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->reset()V

    .line 155
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 156
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 157
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->registered:Z

    .line 158
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->firstLoad:Z

    .line 159
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuPath:Ljava/lang/String;

    .line 160
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    .line 161
    if-eqz p1, :cond_0

    .line 162
    const-string v1, "MENU_PATH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuPath:Ljava/lang/String;

    .line 163
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "menu_path:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 165
    const-string v1, "MENU_MODE"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 166
    .local v0, "modeOrdinal":I
    if-eq v0, v4, :cond_0

    .line 167
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    move-result-object v1

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    .line 168
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "menu_mode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 171
    .end local v0    # "modeOrdinal":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->updateView(Landroid/os/Bundle;)V

    .line 172
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 173
    return-void
.end method

.method public onMapEngineReady(Lcom/navdy/hud/app/maps/MapEvents$MapEngineReady;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$MapEngineReady;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 633
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    if-eq v0, v1, :cond_1

    .line 642
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onMapEngineReady"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 637
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 639
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 637
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onNavigationModeChanged(Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 616
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->menuMode:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    if-eq v0, v1, :cond_1

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNavigationModeChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;->navigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 620
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 622
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {p0, v0, v1, v3, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 626
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ROOT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {p0, v0, v1, v3, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 620
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUnload()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->registered:Z

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->registered:Z

    .line 179
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->reset()V

    .line 182
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .line 183
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 184
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 185
    return-void
.end method

.method performSelectionAnimation(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 465
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 466
    return-void
.end method

.method performSelectionAnimation(Ljava/lang/Runnable;I)V
    .locals 1
    .param p1, "endAction"    # Ljava/lang/Runnable;
    .param p2, "delay"    # I

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 470
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 473
    :cond_0
    return-void
.end method

.method refreshData(I[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "models"    # [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p3, "menu"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 449
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-nez v0, :cond_0

    .line 462
    :goto_0
    return-void

    .line 452
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->closed:Z

    if-eqz v1, :cond_1

    .line 453
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "refreshData(s) already closed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 456
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-ne v1, p3, :cond_2

    .line 457
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshData(s):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 458
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(I[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_0

    .line 460
    :cond_2
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshData(s) update menu:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " different from current:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method refreshDataforPos(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 489
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(IZ)V

    .line 490
    return-void
.end method

.method refreshDataforPos(IZ)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "startFluctuator"    # Z

    .prologue
    .line 493
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 494
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 495
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v3, 0x0

    if-nez p2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, p1, v3, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    .line 497
    :cond_0
    return-void

    .line 495
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 188
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->closed:Z

    .line 189
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->handledSelection:Z

    .line 190
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->animateIn:Z

    .line 191
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->firstLoad:Z

    .line 192
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mapShown:Z

    .line 193
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->navEnded:Z

    .line 194
    return-void
.end method

.method resetSelectedItem()V
    .locals 3

    .prologue
    .line 249
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "resetSelectedItem"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 250
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->handledSelection:Z

    .line 251
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->animateIn:Z

    if-nez v1, :cond_0

    .line 252
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->animateIn:Z

    .line 253
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 254
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 255
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateIn(Landroid/animation/Animator$AnimatorListener;)V

    .line 258
    .end local v0    # "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    :cond_0
    return-void
.end method

.method selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 4
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v0, 0x1

    .line 236
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-nez v1, :cond_0

    .line 245
    :goto_0
    return v0

    .line 240
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->handledSelection:Z

    if-eqz v1, :cond_1

    .line 241
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "already handled ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->handledSelection:Z

    .line 245
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->handledSelection:Z

    goto :goto_0
.end method

.method sendCloseEvent()V
    .locals 2

    .prologue
    .line 645
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->hideMap()V

    .line 647
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    if-eq v0, v1, :cond_0

    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    .line 650
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    if-eqz v0, :cond_1

    .line 651
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mainMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    .line 653
    :cond_1
    return-void
.end method

.method setInitialItemState(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 508
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 509
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 510
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->setInitialState(Z)V

    .line 512
    :cond_0
    return-void
.end method

.method public setNavEnded()V
    .locals 1

    .prologue
    .line 793
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->navEnded:Z

    .line 794
    return-void
.end method

.method setScrollIdleEvents(Z)V
    .locals 2
    .param p1, "send"    # Z

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 560
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 561
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setScrollIdleEvent(Z)V

    .line 563
    :cond_0
    return-void
.end method

.method public setViewBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 839
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 840
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-nez v0, :cond_0

    .line 844
    :goto_0
    return-void

    .line 843
    :cond_0
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public showBoundingBox(Lcom/here/android/mpa/common/GeoBoundingBox;)V
    .locals 4
    .param p1, "boundingBox"    # Lcom/here/android/mpa/common/GeoBoundingBox;

    .prologue
    const/4 v3, 0x0

    .line 770
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 771
    .local v1, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-nez v1, :cond_1

    .line 779
    :cond_0
    :goto_0
    return-void

    .line 774
    :cond_1
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mapShown:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 777
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v0

    .line 778
    .local v0, "navigationView":Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v3, v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->zoomToBoundBox(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZZ)V

    goto :goto_0
.end method

.method public showMap()V
    .locals 20

    .prologue
    .line 712
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mapShown:Z

    if-eqz v7, :cond_1

    .line 713
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "showMap: already shown"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 716
    :cond_1
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->mapShown:Z

    .line 718
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 719
    .local v17, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v17, :cond_0

    .line 722
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "showMap"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 723
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v13

    .line 724
    .local v13, "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v2

    .line 727
    .local v2, "navigationView":Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    invoke-virtual {v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .line 728
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v8, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v7, v8, :cond_3

    .line 729
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const-string v8, "showMap: switchbackmode null"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 730
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .line 737
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v12

    .line 738
    .local v12, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v3

    .line 739
    .local v3, "start":Lcom/here/android/mpa/common/GeoCoordinate;
    const/4 v4, 0x0

    .line 740
    .local v4, "end":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v12}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationRouteRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v11

    .line 741
    .local v11, "current":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v11, :cond_2

    .line 742
    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/16 v18, 0x0

    cmpl-double v7, v8, v18

    if-eqz v7, :cond_4

    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/16 v18, 0x0

    cmpl-double v7, v8, v18

    if-eqz v7, :cond_4

    .line 743
    new-instance v4, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v4    # "end":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-direct {v4, v8, v9, v0, v1}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 748
    .restart local v4    # "end":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_2
    :goto_2
    invoke-virtual {v12}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v14

    .line 749
    .local v14, "routeId":Ljava/lang/String;
    const/4 v5, 0x0

    .line 750
    .local v5, "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    const/4 v6, 0x0

    .line 751
    .local v6, "route":Lcom/here/android/mpa/routing/Route;
    if-eqz v14, :cond_6

    .line 752
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v7

    invoke-virtual {v7, v14}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v15

    .line 753
    .local v15, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v15, :cond_6

    .line 754
    iget-object v6, v15, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    .line 755
    iget-object v7, v15, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v7}, Lcom/here/android/mpa/routing/Route;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v5

    move-object v10, v5

    .line 758
    .end local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v15    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .local v10, "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    :goto_3
    if-nez v10, :cond_5

    if-eqz v3, :cond_5

    .line 760
    :try_start_0
    new-instance v5, Lcom/here/android/mpa/common/GeoBoundingBox;

    const v7, 0x459c4000    # 5000.0f

    const v8, 0x459c4000    # 5000.0f

    invoke-direct {v5, v3, v7, v8}, Lcom/here/android/mpa/common/GeoBoundingBox;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;FF)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 765
    .end local v10    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    :goto_4
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    invoke-virtual/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchToRouteSearchMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZLjava/util/List;I)V

    .line 766
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->enableMapViews()V

    goto/16 :goto_0

    .line 732
    .end local v3    # "start":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v4    # "end":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v6    # "route":Lcom/here/android/mpa/routing/Route;
    .end local v11    # "current":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v12    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v14    # "routeId":Ljava/lang/String;
    :cond_3
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " showMap switchbackmode: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 733
    sget-object v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v13, v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    goto/16 :goto_1

    .line 744
    .restart local v3    # "start":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v4    # "end":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v11    # "current":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .restart local v12    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_4
    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v7, :cond_2

    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/16 v18, 0x0

    cmpl-double v7, v8, v18

    if-eqz v7, :cond_2

    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/16 v18, 0x0

    cmpl-double v7, v8, v18

    if-eqz v7, :cond_2

    .line 745
    new-instance v4, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v4    # "end":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iget-object v7, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-direct {v4, v8, v9, v0, v1}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .restart local v4    # "end":Lcom/here/android/mpa/common/GeoCoordinate;
    goto/16 :goto_2

    .line 761
    .restart local v6    # "route":Lcom/here/android/mpa/routing/Route;
    .restart local v10    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v14    # "routeId":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 762
    .local v16, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_5
    move-object v5, v10

    .end local v10    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    goto :goto_4

    :cond_6
    move-object v10, v5

    .end local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v10    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    goto/16 :goto_3
.end method

.method public showScrimCover()V
    .locals 3

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 657
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 658
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "scrim-show"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 659
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->scrimCover:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 661
    :cond_0
    return-void
.end method

.method public showToolTip(ILjava/lang/String;)V
    .locals 2
    .param p1, "posIndex"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 672
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 673
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 674
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v1, p1, p2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->showToolTip(ILjava/lang/String;)V

    .line 676
    :cond_0
    return-void
.end method

.method updateCurrentMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 7
    .param p1, "menu"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    const/4 v4, 0x0

    .line 429
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 430
    .local v6, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-nez v6, :cond_0

    .line 445
    :goto_0
    return-void

    .line 433
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->closed:Z

    if-eqz v0, :cond_1

    .line 434
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "updateCurrentMenu already closed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 437
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-ne v0, p1, :cond_2

    .line 438
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCurrentMenu:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 439
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->unlock(Z)V

    .line 440
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isBindCallsEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setBindCallbacks(Z)V

    .line 441
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getItems()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getInitialSelection()I

    move-result v2

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v5}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V

    goto :goto_0

    .line 443
    :cond_2
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCurrentMenu update menu:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " different from current:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected updateView(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 198
    .local v0, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p0, v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->init(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;Landroid/os/Bundle;)V

    .line 201
    :cond_0
    return-void
.end method
