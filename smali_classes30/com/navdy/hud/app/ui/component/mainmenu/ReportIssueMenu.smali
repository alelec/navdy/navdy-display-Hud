.class public final Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;
.super Ljava/lang/Object;
.source "ReportIssueMenu.kt"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;,
        Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u0000\u0018\u0000 =2\u00020\u0001:\u0002=>B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0007B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\"\u0010\u0011\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0016J\u0008\u0010\u0015\u001a\u00020\u000cH\u0016J\u0010\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fH\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0018\u001a\u00020\u000cH\u0016J\n\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u001eH\u0016J\u0018\u0010 \u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\u000c2\u0006\u0010\u0018\u001a\u00020\u000cH\u0016J(\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\'\u001a\u00020(H\u0016J\u0008\u0010)\u001a\u00020#H\u0016J\u0008\u0010*\u001a\u00020#H\u0016J\u0010\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0016J\u0008\u0010.\u001a\u00020#H\u0016J\u0010\u0010/\u001a\u00020#2\u0006\u00100\u001a\u000201H\u0016J\u0010\u00102\u001a\u00020#2\u0006\u00103\u001a\u000204H\u0002J\u0010\u00105\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u00106\u001a\u00020#2\u0006\u0010!\u001a\u00020\u000cH\u0016J\u0010\u00107\u001a\u00020#2\u0006\u00108\u001a\u00020\u000cH\u0016J\u0008\u00109\u001a\u00020#H\u0016J\u0008\u0010:\u001a\u00020#H\u0002J\u0008\u0010;\u001a\u00020#H\u0016J\u0010\u0010<\u001a\u00020#2\u0006\u0010!\u001a\u00020\u000cH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
        "vscrollComponent",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
        "presenter",
        "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
        "parent",
        "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V",
        "type",
        "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;",
        "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V",
        "backSelection",
        "",
        "backSelectionId",
        "currentItems",
        "",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "getChildMenu",
        "args",
        "",
        "path",
        "getInitialSelection",
        "getItems",
        "getModelfromPos",
        "pos",
        "getScrollIndex",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;",
        "getType",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;",
        "isBindCallsEnabled",
        "",
        "isFirstItemEmpty",
        "isItemClickable",
        "id",
        "onBindToView",
        "",
        "model",
        "view",
        "Landroid/view/View;",
        "state",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;",
        "onFastScrollEnd",
        "onFastScrollStart",
        "onItemSelected",
        "selection",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;",
        "onScrollIdle",
        "onUnload",
        "level",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;",
        "reportIssue",
        "issueType",
        "Lcom/navdy/hud/app/util/ReportIssueService$IssueType;",
        "selectItem",
        "setBackSelectionId",
        "setBackSelectionPos",
        "n",
        "setSelectedIcon",
        "showSentToast",
        "showToolTip",
        "takeSnapshot",
        "Companion",
        "ReportIssueMenuType",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

# The value of this static final field might be set in the static constructor
.field private static final NAV_ISSUE_SENT_TOAST_ID:Ljava/lang/String; = "nav-issue-sent"

# The value of this static final field might be set in the static constructor
.field private static final TOAST_TIMEOUT:I = 0x3e8

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final driveScore:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final etaInaccurate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final idToTitleMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final maps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final navigation:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final notFastestRoute:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final permanentClosure:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final reportIssue:Ljava/lang/String;

.field private static final reportIssueColor:I

.field private static final reportIssueItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private static final roadBlocked:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final smartDash:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final takeSnapShotItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private static final takeSnapshot:Ljava/lang/String;

.field private static final takeSnapshotColor:I

.field private static final toastSentSuccessfully:Ljava/lang/String;

.field private static final wrongDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final wrongRoadName:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private currentItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private final parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private final presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private final type:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

.field private final vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    .line 43
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->logger:Lcom/navdy/service/library/log/Logger;

    .line 44
    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/Pair;

    const/4 v1, 0x0

    const v2, 0x7f0e0085

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "Maps"

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0e0086

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "Navigation"

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0e0087

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "Smart_Dash"

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0e0084

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "Drive_Score"

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/collections/MapsKt;->hashMapOf([Lkotlin/Pair;)Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->idToTitleMap:Ljava/util/HashMap;

    .line 46
    const/16 v0, 0x3e8

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->TOAST_TIMEOUT:I

    .line 47
    const-string v0, "nav-issue-sent"

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->NAV_ISSUE_SENT_TOAST_ID:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssueItems:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapShotItems:Ljava/util/ArrayList;

    .line 73
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 75
    .local v8, "resources":Landroid/content/res/Resources;
    const v0, 0x7f090191

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026.issue_successfully_sent)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->toastSentSuccessfully:Ljava/lang/String;

    .line 76
    const v0, 0x7f090227

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026.report_navigation_issue)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue:Ljava/lang/String;

    .line 77
    const v0, 0x7f090288

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.string.take_snapshot)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapshot:Ljava/lang/String;

    .line 78
    const v0, 0x7f0d0065

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssueColor:I

    .line 79
    const v0, 0x7f0d008f

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapshotColor:I

    .line 80
    const v0, 0x7f0d003e

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 82
    .local v7, "bkColorUnselected":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    const v5, 0x7f09002d

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026back), null\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 91
    const v0, 0x7f0e0069

    const v1, 0x7f02010b

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_ETA_TRAFFIC:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026urce), null\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->etaInaccurate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 100
    const v0, 0x7f0e006a

    const v1, 0x7f0200d7

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_SELECTED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026urce), null\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->notFastestRoute:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 109
    const v0, 0x7f0e006c

    const v1, 0x7f0201d1

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getMessageStringResource()I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026ngResource)\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->roadBlocked:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 119
    const v0, 0x7f0e006d

    const v1, 0x7f0200d6

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->WRONG_DIRECTION:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026urce), null\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->wrongDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 128
    const v0, 0x7f0e006e

    const v1, 0x7f020228

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_NAME:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026urce), null\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->wrongRoadName:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 137
    const v0, 0x7f0e006b

    const v1, 0x7f0201d0

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED_PERMANENT:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026urce), null\n            )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->permanentClosure:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 145
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getEtaInaccurate()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getEtaInaccurate$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getNotFastestRoute()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getNotFastestRoute$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getRoadBlocked()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getRoadBlocked$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getWrongDirection()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getWrongDirection$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getWrongRoadName()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getWrongRoadName$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getPermanentClosure()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getPermanentClosure$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    const v0, 0x7f0e0085

    const v1, 0x7f0201a0

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    const v3, 0x7f09026f

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026ing.snapshot_maps), null)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->maps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 160
    const v0, 0x7f0e0086

    const v1, 0x7f0201a0

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    const v3, 0x7f090270

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026apshot_navigation), null)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->navigation:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 167
    const v0, 0x7f0e0087

    const v1, 0x7f0201a0

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    const v3, 0x7f090271

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026apshot_smart_dash), null)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->driveScore:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 174
    const v0, 0x7f0e0084

    const v1, 0x7f0201a0

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v4

    const v3, 0x7f09026e

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move v3, v7

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    const-string v1, "IconBkColorViewHolder.bu\u2026pshot_drive_score), null)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->smartDash:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 181
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapShotItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapShotItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getMaps()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getMaps$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapShotItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapShotItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getNavigation()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getNavigation$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapShotItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapShotItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getDriveScore()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getDriveScore$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapShotItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapShotItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getSmartDash()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getSmartDash$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 1
    .param p1, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "vscrollComponent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;->NAVIGATION_ISSUES:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

    .line 31
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V
    .locals 1
    .param p1, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4, "type"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "vscrollComponent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->type:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    .prologue
    and-int/lit8 v0, p5, 0x8

    if-eqz v0, :cond_0

    .line 28
    sget-object p4, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;->NAVIGATION_ISSUES:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V

    return-void
.end method

.method public static final synthetic access$getBack$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getDriveScore$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->driveScore:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getEtaInaccurate$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->etaInaccurate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getIdToTitleMap$cp()Ljava/util/HashMap;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->idToTitleMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getMaps$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->maps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getNAV_ISSUE_SENT_TOAST_ID$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->NAV_ISSUE_SENT_TOAST_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getNavigation$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->navigation:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getNotFastestRoute$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->notFastestRoute:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getPermanentClosure$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->permanentClosure:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getPresenter$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method public static final synthetic access$getReportIssue$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getReportIssueColor$cp()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssueColor:I

    return v0
.end method

.method public static final synthetic access$getReportIssueItems$cp()Ljava/util/ArrayList;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssueItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static final synthetic access$getRoadBlocked$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->roadBlocked:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getSmartDash$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->smartDash:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getTOAST_TIMEOUT$cp()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->TOAST_TIMEOUT:I

    return v0
.end method

.method public static final synthetic access$getTakeSnapShotItems$cp()Ljava/util/ArrayList;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapShotItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static final synthetic access$getTakeSnapshot$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapshot:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getTakeSnapshotColor$cp()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapshotColor:I

    return v0
.end method

.method public static final synthetic access$getToastSentSuccessfully$cp()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->toastSentSuccessfully:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getWrongDirection$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->wrongDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getWrongRoadName$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->wrongRoadName:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$showSentToast(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;)V
    .locals 0
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->showSentToast()V

    return-void
.end method

.method private final reportIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V
    .locals 2
    .param p1, "issueType"    # Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .prologue
    .line 279
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$reportIssue$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$reportIssue$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    .line 285
    return-void
.end method

.method private final showSentToast()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 296
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 297
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTOAST_TIMEOUT()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTOAST_TIMEOUT$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 298
    const-string v0, "8"

    const v1, 0x7f0201ce

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 300
    const-string v0, "4"

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getToastSentSuccessfully()Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getToastSentSuccessfully$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 303
    const-string v0, "17"

    sget-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_NAV_STOPPED:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v0, "21"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 306
    const-string v0, "18"

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getNAV_ISSUE_SENT_TOAST_ID()Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getNAV_ISSUE_SENT_TOAST_ID$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 309
    return-void
.end method

.method private final takeSnapshot(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 288
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$takeSnapshot$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$takeSnapshot$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;I)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    .line 293
    return-void
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "args"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "path"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "args"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "path"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 334
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->type:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 200
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 199
    :pswitch_0
    const/4 v0, 0x1

    .line 198
    :goto_0
    return v0

    .line 200
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->type:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 192
    const/4 v0, 0x0

    .line 193
    :goto_0
    return-object v0

    .line 190
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->currentItems:Ljava/util/List;

    .line 193
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->currentItems:Ljava/util/List;

    goto :goto_0

    .line 191
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapShotItems()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapShotItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->currentItems:Ljava/util/List;

    goto :goto_1

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->currentItems:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->currentItems:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_2

    .line 321
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->currentItems:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 323
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 205
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 312
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->REPORT_ISSUE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 316
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 1
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "view"    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 331
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 341
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 1
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "level"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 337
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 6
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 232
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 274
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 234
    :sswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "back"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 235
    const-string v0, "report-issue-back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->backSelection:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->backSelectionId:I

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    goto :goto_0

    .line 239
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "ETA inaccurate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 240
    const-string v0, "report-issue-eta-inaccurate"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 241
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_ETA_TRAFFIC:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    goto :goto_0

    .line 244
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Not the fastest route"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 245
    const-string v0, "report-issue-not-fastest-route"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 246
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_SELECTED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    goto :goto_0

    .line 249
    :sswitch_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Road blocked"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 250
    const-string v0, "report-issue-road-blocked"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 251
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    goto :goto_0

    .line 254
    :sswitch_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Wrong direction"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 255
    const-string v0, "report-issue-wrong-direction"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 256
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->WRONG_DIRECTION:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    goto :goto_0

    .line 259
    :sswitch_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Wrong road name"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 260
    const-string v0, "report-issue-wrong-road-name"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 261
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_NAME:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    goto/16 :goto_0

    .line 264
    :sswitch_6
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Permanent closure"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 265
    const-string v0, "report-issue-permanent-closure"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 266
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED_PERMANENT:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    goto/16 :goto_0

    .line 272
    :sswitch_7
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapshot(I)V

    goto/16 :goto_0

    .line 232
    :sswitch_data_0
    .sparse-switch
        0x7f0e0047 -> :sswitch_0
        0x7f0e0069 -> :sswitch_1
        0x7f0e006a -> :sswitch_2
        0x7f0e006b -> :sswitch_6
        0x7f0e006c -> :sswitch_3
        0x7f0e006d -> :sswitch_4
        0x7f0e006e -> :sswitch_5
        0x7f0e0084 -> :sswitch_7
        0x7f0e0085 -> :sswitch_7
        0x7f0e0086 -> :sswitch_7
        0x7f0e0087 -> :sswitch_7
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 213
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->backSelectionId:I

    .line 214
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 209
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->backSelection:I

    .line 210
    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0201a0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 217
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->type:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 226
    :goto_0
    return-void

    .line 219
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v1

    invoke-virtual {v0, v3, v1, v4, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 220
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssue()Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getReportIssue$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 223
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I

    move-result v1

    invoke-virtual {v0, v3, v1, v4, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 224
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshot()Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->access$getTakeSnapshot$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 349
    return-void
.end method
