.class public Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;
.super Ljava/lang/Object;
.source "PlacesMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field public static final CANCEL_POSITION:I = 0x1

.field private static CONFIRMATION_CHOICES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NAVIGATE_POSITION:I = 0x0

.field private static final SELECTION_ANIMATION_DELAY:I = 0x3e8

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final backColor:I

.field public static final bkColorUnselected:I

.field private static final contactColor:I

.field private static final favColor:I

.field private static final gasColor:I

.field private static final homeColor:I

.field public static final places:Ljava/lang/String;

.field private static final placesColor:I

.field private static final recentColor:I

.field private static final recentPlaces:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final search:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field public static final suggested:Ljava/lang/String;

.field public static final suggestedColor:I

.field private static final workColor:I


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private latlngSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private nearbyPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private recentPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

.field private returnToCacheList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f0d0072

    const/4 v6, 0x0

    .line 44
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->CONFIRMATION_CHOICES:Ljava/util/List;

    .line 75
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    .line 76
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->bkColorUnselected:I

    .line 81
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentColor:I

    .line 82
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->favColor:I

    .line 83
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d006e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->homeColor:I

    .line 84
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->workColor:I

    .line 85
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->contactColor:I

    .line 86
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->backColor:I

    .line 87
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->suggestedColor:I

    .line 88
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->placesColor:I

    .line 89
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d006d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->gasColor:I

    .line 91
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09027e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->suggested:Ljava/lang/String;

    .line 92
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->places:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 96
    .local v5, "title":Ljava/lang/String;
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->backColor:I

    .line 97
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 100
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 101
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 102
    const v0, 0x7f0e0063

    const v1, 0x7f0201ba

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlaces:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 105
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 106
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 107
    const v0, 0x7f0e003b

    const v1, 0x7f020186

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->search:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 109
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->CONFIRMATION_CHOICES:Ljava/util/List;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0900b4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->CONFIRMATION_CHOICES:Ljava/util/List;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0900b3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->latlngSet:Ljava/util/HashSet;

    .line 130
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->bus:Lcom/squareup/otto/Bus;

    .line 131
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 132
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 133
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    return-object v0
.end method

.method public static buildModel(Lcom/navdy/hud/app/framework/destinations/Destination;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 4
    .param p0, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;
    .param p1, "id"    # I

    .prologue
    .line 378
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    .line 380
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->recentTimeLabel:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 381
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->recentTimeLabel:Ljava/lang/String;

    .line 385
    .local v1, "subTitle":Ljava/lang/String;
    :goto_0
    invoke-static {p0, p1, v2, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->getPlaceModel(Lcom/navdy/hud/app/framework/destinations/Destination;ILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 386
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    return-object v0

    .line 383
    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v1    # "subTitle":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationSubtitle:Ljava/lang/String;

    .restart local v1    # "subTitle":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getPlaceModel(Lcom/navdy/hud/app/framework/destinations/Destination;ILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 8
    .param p0, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subTitle"    # Ljava/lang/String;

    .prologue
    const v1, 0x7f0201ba

    const v5, 0x7f0201b5

    .line 395
    if-nez p0, :cond_0

    .line 396
    const v1, 0x7f020184

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->placesColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->placesColor:I

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    .line 433
    :goto_0
    return-object v7

    .line 399
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$4;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType:[I

    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 413
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->SUGGESTED_RECENT:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    if-ne v0, v2, :cond_1

    .line 415
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentColor:I

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto :goto_0

    .line 401
    :pswitch_0
    const v1, 0x7f0201b7

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->homeColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->homeColor:I

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto :goto_0

    .line 404
    :pswitch_1
    const v1, 0x7f0201bd

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->workColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->workColor:I

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto :goto_0

    .line 407
    :pswitch_2
    const v1, 0x7f020214

    const/4 v2, 0x0

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->contactColor:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    .line 408
    .local v7, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    .line 409
    iget-object v0, v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    const-string v1, "INITIAL"

    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->initials:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 418
    .end local v7    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$4;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType:[I

    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 427
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->FIND_GAS:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    if-ne v0, v2, :cond_2

    .line 428
    const v1, 0x7f0201bf

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->gasColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->gasColor:I

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto/16 :goto_0

    .line 420
    :pswitch_3
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->favColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->favColor:I

    move v0, p1

    move v1, v5

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto/16 :goto_0

    .line 423
    :pswitch_4
    const v1, 0x7f0201b2

    const v2, 0x7f0201b3

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->homeColor:I

    const/4 v4, -0x1

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto/16 :goto_0

    .line 430
    :cond_2
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->recommendation:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->RECENT:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    if-ne v0, v2, :cond_3

    .line 431
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentColor:I

    move v0, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto/16 :goto_0

    .line 433
    :cond_3
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->favColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->favColor:I

    move v0, p1

    move v1, v5

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v7

    goto/16 :goto_0

    .line 399
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 418
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 286
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 4

    .prologue
    .line 231
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    if-nez v2, :cond_1

    .line 232
    const/4 v0, 0x0

    .line 242
    :cond_0
    :goto_0
    return v0

    .line 234
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlaces:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 235
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 238
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 239
    .local v1, "n":I
    const/4 v2, 0x3

    if-lt v1, v2, :cond_2

    .line 240
    const/4 v0, 0x2

    goto :goto_0

    .line 242
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItems()Ljava/util/List;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 139
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    .line 226
    :goto_0
    return-object v15

    .line 142
    :cond_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v15, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->returnToCacheList:Ljava/util/List;

    .line 144
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, v19

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v17

    .line 147
    .local v17, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v8

    .line 148
    .local v8, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v19

    if-nez v19, :cond_3

    const/4 v13, 0x1

    .line 149
    .local v13, "isConnected":Z
    :goto_1
    if-eqz v13, :cond_1

    if-nez v8, :cond_1

    .line 150
    const/4 v13, 0x0

    .line 152
    :cond_1
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "isConnected:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 154
    if-eqz v13, :cond_2

    .line 156
    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->supportsPlaceSearch()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 157
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->search:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, v19

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    :cond_2
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlaces:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, v19

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v10

    .line 163
    .local v10, "firstIndex":I
    const/4 v4, 0x0

    .line 164
    .local v4, "counter":I
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v7

    .line 166
    .local v7, "destinationsManager":Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->latlngSet:Ljava/util/HashSet;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashSet;->clear()V

    .line 169
    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getSuggestedDestinations()Ljava/util/List;

    move-result-object v18

    .line 170
    .local v18, "suggestedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    if-eqz v18, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v19

    if-lez v19, :cond_4

    .line 171
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "suggested destinations:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 172
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_5

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/framework/destinations/Destination;

    .line 173
    .local v6, "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "counter":I
    .local v5, "counter":I
    move v12, v4

    .line 174
    .local v12, "id":I
    invoke-static {v6, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->buildModel(Lcom/navdy/hud/app/framework/destinations/Destination;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v16

    .line 175
    .local v16, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v20, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->suggested:Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 176
    move-object/from16 v0, v16

    iput-object v6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 177
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    .line 178
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    move-object/from16 v20, v0

    const-string v21, "SUBTITLE_COLOR"

    sget v22, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->suggestedColor:I

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->returnToCacheList:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 182
    .local v14, "latlngStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->latlngSet:Ljava/util/HashSet;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v4, v5

    .line 183
    .end local v5    # "counter":I
    .restart local v4    # "counter":I
    goto/16 :goto_2

    .line 148
    .end local v4    # "counter":I
    .end local v6    # "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    .end local v7    # "destinationsManager":Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
    .end local v10    # "firstIndex":I
    .end local v12    # "id":I
    .end local v13    # "isConnected":Z
    .end local v14    # "latlngStr":Ljava/lang/String;
    .end local v16    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v18    # "suggestedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    :cond_3
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 185
    .restart local v4    # "counter":I
    .restart local v7    # "destinationsManager":Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
    .restart local v10    # "firstIndex":I
    .restart local v13    # "isConnected":Z
    .restart local v18    # "suggestedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    :cond_4
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v20, "no suggested destinations"

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 189
    :cond_5
    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getFavoriteDestinations()Ljava/util/List;

    move-result-object v9

    .line 190
    .local v9, "favoriteDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    if-eqz v9, :cond_7

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v19

    if-lez v19, :cond_7

    .line 191
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "favorite destinations:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/framework/destinations/Destination;

    .line 193
    .restart local v6    # "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    iget-wide v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 194
    .restart local v14    # "latlngStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->latlngSet:Ljava/util/HashSet;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 195
    sget-object v20, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "latlng already seen in suggested:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v6, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 198
    :cond_6
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "counter":I
    .restart local v5    # "counter":I
    move v12, v4

    .line 199
    .restart local v12    # "id":I
    invoke-static {v6, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->buildModel(Lcom/navdy/hud/app/framework/destinations/Destination;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v16

    .line 200
    .restart local v16    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v16

    iput-object v6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 201
    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->returnToCacheList:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v5

    .line 203
    .end local v5    # "counter":I
    .restart local v4    # "counter":I
    goto/16 :goto_3

    .line 205
    .end local v6    # "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    .end local v12    # "id":I
    .end local v14    # "latlngStr":Ljava/lang/String;
    .end local v16    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_7
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v20, "no favorite destinations"

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 209
    :cond_8
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getGasDestination()Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;

    move-result-object v11

    .line 210
    .local v11, "gasDestination":Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;
    if-eqz v11, :cond_9

    .line 211
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "counter":I
    .restart local v5    # "counter":I
    move v12, v4

    .line 212
    .restart local v12    # "id":I
    iget-object v0, v11, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;->destination:Lcom/navdy/hud/app/framework/destinations/Destination;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->buildModel(Lcom/navdy/hud/app/framework/destinations/Destination;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v16

    .line 213
    .restart local v16    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v19, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->suggested:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 214
    iget-object v0, v11, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;->destination:Lcom/navdy/hud/app/framework/destinations/Destination;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 215
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    .line 216
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    move-object/from16 v19, v0

    const-string v20, "SUBTITLE_COLOR"

    sget v21, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->suggestedColor:I

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-boolean v0, v11, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;->showFirst:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    .line 218
    move-object/from16 v0, v16

    invoke-interface {v15, v10, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 222
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->returnToCacheList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v5

    .line 225
    .end local v5    # "counter":I
    .end local v12    # "id":I
    .end local v16    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .restart local v4    # "counter":I
    :cond_9
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    goto/16 :goto_0

    .line 220
    .end local v4    # "counter":I
    .restart local v5    # "counter":I
    .restart local v12    # "id":I
    .restart local v16    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_a
    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 270
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 272
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 369
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 374
    const/4 v0, 0x1

    return v0
.end method

.method launchDestination(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 7
    .param p1, "m"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    const/4 v6, 0x0

    .line 442
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_0

    .line 443
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Here maps engine not initialized, exit"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 444
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v5, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$1;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;)V

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    .line 521
    :goto_0
    return-void

    .line 456
    :cond_0
    new-instance v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-direct {v3, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    .line 459
    .local v3, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    .line 460
    .local v2, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 464
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "called requestNavigation"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 465
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v5, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$2;

    invoke-direct {v5, p0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    const/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 479
    :cond_2
    iget-object v1, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v1, Lcom/navdy/hud/app/framework/destinations/Destination;

    .line 480
    .local v1, "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getConfirmationLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    .line 481
    .local v0, "confirmationLayout":Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    if-nez v0, :cond_3

    .line 482
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "confirmation layout not found"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 486
    :cond_3
    invoke-static {v0, v3, v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->configure(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/framework/destinations/Destination;)V

    .line 488
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->CONFIRMATION_CHOICES:Ljava/util/List;

    new-instance v5, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;

    invoke-direct {v5, p0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;Lcom/navdy/hud/app/framework/destinations/Destination;)V

    invoke-virtual {v0, v4, v6, v5}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 520
    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 282
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 317
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 306
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 309
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 2
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 291
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$4;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 293
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->returnToCacheList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 294
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "pm:unload add to cache"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->returnToCacheList:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->addToCache(Ljava/util/List;)V

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->returnToCacheList:Ljava/util/List;

    .line 298
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    goto :goto_0

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 7
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v6, 0x0

    .line 329
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pos:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 331
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v1, :sswitch_data_0

    .line 359
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->cachedList:Ljava/util/List;

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 360
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->launchDestination(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    .line 364
    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 333
    :sswitch_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "back"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 334
    const-string v1, "back"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 335
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->backSelection:I

    iget v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->backSelectionId:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 336
    iput v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->backSelectionId:I

    goto :goto_0

    .line 340
    :sswitch_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "recent places"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 341
    const-string v1, "recent_places"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 342
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

    if-nez v1, :cond_0

    .line 343
    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

    .line 345
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->recentPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 349
    :sswitch_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "search"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 350
    const-string v1, "search"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 351
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->nearbyPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    if-nez v1, :cond_1

    .line 352
    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->nearbyPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->nearbyPlacesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 331
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e003b -> :sswitch_2
        0x7f0e0047 -> :sswitch_0
        0x7f0e0063 -> :sswitch_1
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->backSelectionId:I

    .line 259
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 253
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->backSelection:I

    .line 254
    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 263
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f020184

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->placesColor:I

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 264
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->places:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 320
    return-void
.end method
