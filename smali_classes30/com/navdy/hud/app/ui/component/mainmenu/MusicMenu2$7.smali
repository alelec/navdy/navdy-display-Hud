.class Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;
.super Ljava/lang/Object;
.source "MusicMenu2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handleArtwork([BLjava/lang/Integer;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

.field final synthetic val$artwork:Landroid/graphics/Bitmap;

.field final synthetic val$byteArray:[B

.field final synthetic val$idString:Ljava/lang/String;

.field final synthetic val$pos:Ljava/lang/Integer;

.field final synthetic val$saveInDiskCache:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Integer;Z[B)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 1293
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$idString:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$artwork:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    iput-boolean p5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$saveInDiskCache:Z

    iput-object p6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$byteArray:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1296
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$idString:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$artwork:Landroid/graphics/Bitmap;

    invoke-static {v3, v4}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->setBitmapInCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1298
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    if-eqz v3, :cond_5

    .line 1299
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$1000(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    if-ne v3, v4, :cond_0

    .line 1300
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Refreshing artwork for position: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1301
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$1000(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(I)V

    .line 1303
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$1400(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1304
    .local v1, "size":I
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ltz v3, :cond_1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v3, v1, :cond_3

    .line 1305
    :cond_1
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Refreshing artwork invalid index: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1324
    .end local v1    # "size":I
    :cond_2
    :goto_0
    return-void

    .line 1308
    .restart local v1    # "size":I
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$1400(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$pos:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v2, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 1309
    .local v2, "state":Ljava/lang/Object;
    instance-of v3, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-eqz v3, :cond_4

    move-object v0, v2

    .line 1310
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 1320
    .end local v1    # "size":I
    .end local v2    # "state":Ljava/lang/Object;
    .local v0, "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    :goto_1
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$saveInDiskCache:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v3, :cond_2

    .line 1321
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting bitmap in cache for collection: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1322
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicArtworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$byteArray:[B

    invoke-virtual {v3, v0, v4}, Lcom/navdy/hud/app/util/MusicArtworkCache;->putArtwork(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;[B)V

    goto :goto_0

    .line 1312
    .end local v0    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .restart local v1    # "size":I
    .restart local v2    # "state":Ljava/lang/Object;
    :cond_4
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Something has gone terribly wrong."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1317
    .end local v1    # "size":I
    .end local v2    # "state":Ljava/lang/Object;
    :cond_5
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$1100(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->val$artwork:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(Landroid/graphics/Bitmap;)V

    .line 1318
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$600(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    move-result-object v3

    iget-object v0, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .restart local v0    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    goto :goto_1
.end method
