.class Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;
.super Ljava/lang/Object;
.source "MusicMenu2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onMusicArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

.field final synthetic val$artworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

.field final synthetic val$idString:Ljava/lang/String;

.field final synthetic val$pos:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicArtworkResponse;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 1250
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->val$artworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->val$pos:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->val$idString:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1253
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->val$artworkResponse:Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    invoke-virtual {v1}, Lokio/ByteString;->toByteArray()[B

    move-result-object v0

    .line 1255
    .local v0, "byteArray":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 1256
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Received photo has null or empty byte array"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1261
    :goto_0
    return-void

    .line 1260
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->val$pos:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;->val$idString:Ljava/lang/String;

    const/4 v4, 0x1

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handleArtwork([BLjava/lang/Integer;Ljava/lang/String;Z)V
    invoke-static {v1, v0, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$800(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;[BLjava/lang/Integer;Ljava/lang/String;Z)V

    goto :goto_0
.end method
