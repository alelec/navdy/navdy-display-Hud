.class public final Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
.super Ljava/lang/Object;
.source "NumberPickerMenu.kt"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;,
        Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;,
        Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000 B2\u00020\u0001:\u0003ABCBU\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J(\u0010\u001b\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u001c\u001a\u0004\u0018\u00010\r2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\rH\u0016J\u0008\u0010\u001e\u001a\u00020\u000fH\u0016J\u0010\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017H\u0016J\u0012\u0010 \u001a\u0004\u0018\u00010\u00182\u0006\u0010!\u001a\u00020\u000fH\u0016J\n\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u0008\u0010$\u001a\u00020%H\u0016J\u0008\u0010&\u001a\u00020\'H\u0016J\u0008\u0010(\u001a\u00020\'H\u0016J\u0018\u0010)\u001a\u00020\'2\u0006\u0010*\u001a\u00020\u000f2\u0006\u0010!\u001a\u00020\u000fH\u0016J.\u0010+\u001a\u00020,2\u0008\u0010-\u001a\u0004\u0018\u00010\u00182\u0008\u0010.\u001a\u0004\u0018\u00010/2\u0006\u0010!\u001a\u00020\u000f2\u0008\u00100\u001a\u0004\u0018\u000101H\u0016J\u0008\u00102\u001a\u00020,H\u0016J\u0008\u00103\u001a\u00020,H\u0016J\u0012\u00104\u001a\u00020,2\u0008\u00105\u001a\u0004\u0018\u000106H\u0016J\u0008\u00107\u001a\u00020,H\u0016J\u0012\u00108\u001a\u00020,2\u0008\u00109\u001a\u0004\u0018\u00010:H\u0016J\u0010\u0010;\u001a\u00020\'2\u0006\u00105\u001a\u000206H\u0016J\u0010\u0010<\u001a\u00020,2\u0006\u0010*\u001a\u00020\u000fH\u0016J\u0010\u0010=\u001a\u00020,2\u0006\u0010>\u001a\u00020\u000fH\u0016J\u0008\u0010?\u001a\u00020,H\u0016J\u0008\u0010@\u001a\u00020,H\u0016R\u000e\u0010\u0014\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006D"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
        "vscrollComponent",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
        "presenter",
        "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
        "parent",
        "mode",
        "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;",
        "contacts",
        "",
        "Lcom/navdy/hud/app/framework/contacts/Contact;",
        "notificationId",
        "",
        "iconResource",
        "",
        "iconColor",
        "callback",
        "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;",
        "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;Ljava/util/List;Ljava/lang/String;IILcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;)V",
        "backSelection",
        "backSelectionId",
        "cachedList",
        "",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "messagePickerMenu",
        "Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;",
        "getChildMenu",
        "args",
        "path",
        "getInitialSelection",
        "getItems",
        "getModelfromPos",
        "pos",
        "getScrollIndex",
        "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;",
        "getType",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;",
        "isBindCallsEnabled",
        "",
        "isFirstItemEmpty",
        "isItemClickable",
        "id",
        "onBindToView",
        "",
        "model",
        "view",
        "Landroid/view/View;",
        "state",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;",
        "onFastScrollEnd",
        "onFastScrollStart",
        "onItemSelected",
        "selection",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;",
        "onScrollIdle",
        "onUnload",
        "level",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;",
        "selectItem",
        "setBackSelectionId",
        "setBackSelectionPos",
        "n",
        "setSelectedIcon",
        "showToolTip",
        "Callback",
        "Companion",
        "Mode",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

# The value of this static final field might be set in the static constructor
.field private static final SELECTION_ANIMATION_DELAY:I = 0x3e8

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

# The value of this static final field might be set in the static constructor
.field private static final baseMessageId:I = 0x64

.field private static final context:Landroid/content/Context;

.field private static final fluctuatorColor:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final resources:Landroid/content/res/Resources;


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private final callback:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;

.field private final contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final iconColor:I

.field private final iconResource:I

.field private messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

.field private final mode:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;

.field private final notificationId:Ljava/lang/String;

.field private final parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private final presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private final vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    invoke-direct {v0, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->logger:Lcom/navdy/service/library/log/Logger;

    .line 35
    const/16 v0, 0x3e8

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->SELECTION_ANIMATION_DELAY:I

    .line 36
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->context:Landroid/content/Context;

    .line 37
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getContext$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->resources:Landroid/content/res/Resources;

    .line 38
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getContext$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0054

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->fluctuatorColor:I

    .line 39
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getFluctuatorColor()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getFluctuatorColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)I

    move-result v2

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getFluctuatorColor()I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getFluctuatorColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)I

    move-result v4

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f09002d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 40
    const/16 v0, 0x64

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->baseMessageId:I

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;Ljava/util/List;Ljava/lang/String;IILcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;)V
    .locals 1
    .param p1, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4, "mode"    # Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5, "contacts"    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6, "notificationId"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p7, "iconResource"    # I
    .param p8, "iconColor"    # I
    .param p9, "callback"    # Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;",
            "Ljava/util/List",
            "<+",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;",
            "Ljava/lang/String;",
            "II",
            "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;",
            ")V"
        }
    .end annotation

    .prologue
    const-string v0, "vscrollComponent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "presenter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contacts"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->mode:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;

    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->contacts:Ljava/util/List;

    iput-object p6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->notificationId:Ljava/lang/String;

    iput p7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->iconResource:I

    iput p8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->iconColor:I

    iput-object p9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->callback:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;

    return-void
.end method

.method public static final synthetic access$getBack$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method public static final synthetic access$getBaseMessageId$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->baseMessageId:I

    return v0
.end method

.method public static final synthetic access$getCallback$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->callback:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;

    return-object v0
.end method

.method public static final synthetic access$getContacts$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;)Ljava/util/List;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->contacts:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getContext$cp()Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->context:Landroid/content/Context;

    return-object v0
.end method

.method public static final synthetic access$getFluctuatorColor$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->fluctuatorColor:I

    return v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getResources$cp()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->resources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static final synthetic access$getSELECTION_ANIMATION_DELAY$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->SELECTION_ANIMATION_DELAY:I

    return v0
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2, "args"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3, "path"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 145
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->cachedList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v0}, Lkotlin/jvm/internal/TypeIntrinsics;->asMutableList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    .line 83
    :goto_0
    return-object v11

    .line 68
    :cond_1
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v11, "list":Ljava/util/ArrayList;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    const/4 v9, 0x0

    .line 74
    .local v9, "counter":I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->contacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 75
    .local v7, "c":Lcom/navdy/hud/app/framework/contacts/Contact;
    iget-object v0, v7, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v5, v7, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 76
    .local v5, "modelTitle":Ljava/lang/String;
    :goto_2
    iget-object v0, v7, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v6, v7, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    .line 77
    .local v6, "modelSubTitle":Ljava/lang/String;
    :goto_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getBaseMessageId()I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getBaseMessageId$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)I

    move-result v0

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "counter":I
    .local v10, "counter":I
    add-int/2addr v0, v9

    iget v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->iconResource:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->iconColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->iconColor:I

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v8

    .line 80
    .local v8, "call":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v9, v10

    .line 74
    .end local v10    # "counter":I
    .restart local v9    # "counter":I
    goto :goto_1

    .line 75
    .end local v5    # "modelTitle":Ljava/lang/String;
    .end local v6    # "modelSubTitle":Ljava/lang/String;
    .end local v8    # "call":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_2
    iget-object v5, v7, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    goto :goto_2

    .line 76
    .restart local v5    # "modelTitle":Ljava/lang/String;
    :cond_3
    const/4 v6, 0x0

    goto :goto_3

    .end local v5    # "modelTitle":Ljava/lang/String;
    .end local v7    # "c":Lcom/navdy/hud/app/framework/contacts/Contact;
    :cond_4
    move-object v0, v11

    .line 82
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->cachedList:Ljava/util/List;

    .line 83
    check-cast v11, Ljava/util/List;

    goto :goto_0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p1, "pos"    # I
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->cachedList:Ljava/util/List;

    .line 134
    .local v0, "list":Ljava/util/List;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 135
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 137
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->NUMBER_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2, "view"    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 143
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 0
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 147
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 9
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    const-string v0, "selection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 101
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v0, :pswitch_data_0

    .line 109
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getBaseMessageId()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getBaseMessageId$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)I

    move-result v1

    sub-int v7, v0, v1

    .line 110
    .local v7, "contactIndex":I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->mode:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 119
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$selectItem$1;

    invoke-direct {v0, p0, v7}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$selectItem$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;I)V

    check-cast v0, Ljava/lang/Runnable;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getSELECTION_ANIMATION_DELAY()I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getSELECTION_ANIMATION_DELAY$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 121
    .end local v7    # "contactIndex":I
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 103
    :pswitch_0
    const-string v0, "back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->backSelection:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->backSelectionId:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 105
    iput v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->backSelectionId:I

    goto :goto_0

    .line 112
    .restart local v7    # "contactIndex":I
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    if-nez v0, :cond_0

    .line 113
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-object v3, p0

    check-cast v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->getCannedMessages()Ljava/util/List;

    move-result-object v4

    const-string v5, "GlanceConstants.getCannedMessages()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->contacts:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->notificationId:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/util/List;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->messagePickerMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v0, v2, v3, v8}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x7f0e0047
        :pswitch_0
    .end packed-switch

    .line 110
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->backSelectionId:I

    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->backSelection:I

    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->iconResource:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->iconColor:I

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 96
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;->Companion:Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09030c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method
