.class public Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;
.super Ljava/lang/Object;
.source "RecentPlacesMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final places:Ljava/lang/String;

.field private static final placesColor:I

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private backSelection:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private returnToCacheList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 37
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->resources:Landroid/content/res/Resources;

    .line 38
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->placesColor:I

    .line 39
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->places:Ljava/lang/String;

    .line 42
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 43
    .local v5, "title":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 44
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/4 v6, 0x0

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 0
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->bus:Lcom/squareup/otto/Bus;

    .line 60
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 61
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 62
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 63
    return-void
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 142
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 97
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v0, :cond_1

    .line 98
    :cond_0
    const/4 v0, 0x0

    .line 100
    :cond_1
    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    if-eqz v7, :cond_0

    .line 68
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    .line 92
    :goto_0
    return-object v4

    .line 71
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->returnToCacheList:Ljava/util/List;

    .line 73
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getRecentDestinations()Ljava/util/List;

    move-result-object v6

    .line 77
    .local v6, "recentDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 78
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "recent destinations:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, "counter":I
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/destinations/Destination;

    .line 81
    .local v2, "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "counter":I
    .local v1, "counter":I
    move v3, v0

    .line 82
    .local v3, "id":I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->buildModel(Lcom/navdy/hud/app/framework/destinations/Destination;I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v5

    .line 83
    .local v5, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iput-object v2, v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 84
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->returnToCacheList:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 86
    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    goto :goto_1

    .line 88
    .end local v0    # "counter":I
    .end local v2    # "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    .end local v3    # "id":I
    .end local v5    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "no recent destinations"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 91
    :cond_2
    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    goto :goto_0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 128
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->RECENT_PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 207
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 138
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 159
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 2
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 147
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 149
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->returnToCacheList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 150
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "rpm:unload add to cache"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->returnToCacheList:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->addToCache(Ljava/util/List;)V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->returnToCacheList:Ljava/util/List;

    goto :goto_0

    .line 147
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 6
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 182
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pos:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 184
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v1, :pswitch_data_0

    .line 192
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->cachedList:Ljava/util/List;

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 193
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    check-cast v1, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->launchDestination(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    .line 197
    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 186
    :pswitch_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "back"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 187
    const-string v1, "back"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 188
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->backSelection:I

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0047
        :pswitch_0
    .end packed-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 115
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 111
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->backSelection:I

    .line 112
    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f0201ba

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->placesColor:I

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/RecentPlacesMenu;->places:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method
