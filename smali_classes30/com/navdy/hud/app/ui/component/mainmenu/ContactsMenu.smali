.class Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;
.super Ljava/lang/Object;
.source "ContactsMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final CONTACT_PHOTO_OPACITY:F = 0.6f

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final backColor:I

.field private static final contactColor:I

.field private static final contactStr:Ljava/lang/String;

.field private static final handler:Landroid/os/Handler;

.field private static final noContactColor:I

.field private static final recentContactStr:Ljava/lang/String;

.field private static final recentContacts:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final resources:Landroid/content/res/Resources;

.field private static final roundTransformation:Lcom/squareup/picasso/Transformation;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private recentContactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

.field private returnToCacheList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 52
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 68
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->handler:Landroid/os/Handler;

    .line 69
    new-instance v0, Lcom/makeramen/RoundedTransformationBuilder;

    invoke-direct {v0}, Lcom/makeramen/RoundedTransformationBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/makeramen/RoundedTransformationBuilder;->oval(Z)Lcom/makeramen/RoundedTransformationBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/makeramen/RoundedTransformationBuilder;->build()Lcom/squareup/picasso/Transformation;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 72
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->resources:Landroid/content/res/Resources;

    .line 76
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->backColor:I

    .line 77
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->contactColor:I

    .line 78
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->noContactColor:I

    .line 80
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->contactStr:Ljava/lang/String;

    .line 81
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContactStr:Ljava/lang/String;

    .line 84
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 85
    .local v5, "title":Ljava/lang/String;
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->backColor:I

    .line 86
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 89
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContactStr:Ljava/lang/String;

    .line 90
    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->contactColor:I

    .line 91
    const v0, 0x7f0e0009

    const v1, 0x7f0201ba

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContacts:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 92
    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 0
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->bus:Lcom/squareup/otto/Bus;

    .line 110
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 111
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 112
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;Ljava/io/File;III)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->setImage(Ljava/io/File;III)V

    return-void
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method private getContactsNotAllowed()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 8

    .prologue
    .line 408
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 411
    .local v7, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceIos()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    const v0, 0x7f090014

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 417
    .local v5, "title":Ljava/lang/String;
    :goto_0
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->contactColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->contactColor:I

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0

    .line 414
    .end local v5    # "title":Ljava/lang/String;
    :cond_0
    const v0, 0x7f090013

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "title":Ljava/lang/String;
    goto :goto_0
.end method

.method private setImage(Ljava/io/File;III)V
    .locals 2
    .param p1, "path"    # Ljava/io/File;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "position"    # I

    .prologue
    .line 389
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getInstance()Lcom/squareup/picasso/Picasso;

    move-result-object v0

    .line 390
    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 391
    invoke-virtual {v0, p2, p3}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 392
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu$2;

    invoke-direct {v1, p0, p4}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;I)V

    .line 393
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    .line 405
    return-void
.end method


# virtual methods
.method buildModel(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/navdy/hud/app/framework/contacts/NumberType;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 10
    .param p1, "id"    # I
    .param p2, "contactName"    # Ljava/lang/String;
    .param p3, "formattedNumber"    # Ljava/lang/String;
    .param p4, "numberTypeStr"    # Ljava/lang/String;
    .param p5, "defaultImageIndex"    # I
    .param p6, "numberType"    # Lcom/navdy/hud/app/framework/contacts/NumberType;
    .param p7, "initials"    # Ljava/lang/String;

    .prologue
    .line 367
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v8

    .line 368
    .local v8, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    const v2, 0x7f020217

    const v3, 0x7f02021e

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->noContactColor:I

    const/4 v5, -0x1

    const/4 v7, 0x0

    move v1, p1

    move-object v6, p3

    invoke-static/range {v1 .. v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v9

    .line 383
    .local v9, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v9, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    .line 384
    iget-object v1, v9, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    const-string v3, "INITIAL"

    move-object/from16 v0, p7

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    return-object v9

    .line 373
    .end local v9    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    invoke-virtual {v8, p5}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v2

    .line 374
    .local v2, "largeImageRes":I
    invoke-virtual {v8, p5}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceColor(I)I

    move-result v4

    .line 376
    .local v4, "fluctuator":I
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-object/from16 v0, p6

    if-eq v0, v1, :cond_1

    .line 377
    move-object v7, p4

    .line 381
    .local v7, "subTitle":Ljava/lang/String;
    :goto_1
    const v3, 0x7f02021c

    const/4 v5, -0x1

    move v1, p1

    move-object v6, p2

    invoke-static/range {v1 .. v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v9

    .restart local v9    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    goto :goto_0

    .line 379
    .end local v7    # "subTitle":Ljava/lang/String;
    .end local v9    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    move-object v7, p3

    .restart local v7    # "subTitle":Ljava/lang/String;
    goto :goto_1
.end method

.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 272
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 2

    .prologue
    .line 166
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 167
    const/4 v1, 0x0

    .line 173
    :goto_0
    return v1

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 170
    .local v0, "n":I
    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 171
    const/4 v1, 0x2

    goto :goto_0

    .line 173
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getItems()Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 118
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    .line 161
    :goto_0
    return-object v14

    .line 120
    :cond_0
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v14, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->returnToCacheList:Ljava/util/List;

    .line 122
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    move-result-object v13

    .line 124
    .local v13, "favoriteContactsManager":Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;
    invoke-virtual {v13}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->isAllowedToReceiveContacts()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 125
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContacts:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const/4 v10, 0x0

    .line 130
    .local v10, "counter":I
    invoke-virtual {v13}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getFavoriteContacts()Ljava/util/List;

    move-result-object v12

    .line 131
    .local v12, "favContacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    if-eqz v12, :cond_1

    .line 132
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "favorite contacts:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 133
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 134
    .local v9, "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "counter":I
    .local v11, "counter":I
    move v2, v10

    .line 135
    .local v2, "id":I
    iget-object v3, v9, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 136
    .local v3, "contactName":Ljava/lang/String;
    iget-object v4, v9, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    .line 137
    .local v4, "formattedNumber":Ljava/lang/String;
    iget-object v5, v9, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 138
    .local v5, "numberTypeStr":Ljava/lang/String;
    iget v6, v9, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 139
    .local v6, "defaultImageIndex":I
    iget-object v7, v9, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 140
    .local v7, "numberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    iget-object v8, v9, Lcom/navdy/hud/app/framework/contacts/Contact;->initials:Ljava/lang/String;

    .local v8, "initials":Ljava/lang/String;
    move-object/from16 v1, p0

    .line 141
    invoke-virtual/range {v1 .. v8}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->buildModel(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/navdy/hud/app/framework/contacts/NumberType;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v15

    .line 148
    .local v15, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iput-object v9, v15, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 149
    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->returnToCacheList:Ljava/util/List;

    invoke-interface {v1, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v10, v11

    .line 151
    .end local v11    # "counter":I
    .restart local v10    # "counter":I
    goto :goto_1

    .line 153
    .end local v2    # "id":I
    .end local v3    # "contactName":Ljava/lang/String;
    .end local v4    # "formattedNumber":Ljava/lang/String;
    .end local v5    # "numberTypeStr":Ljava/lang/String;
    .end local v6    # "defaultImageIndex":I
    .end local v7    # "numberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    .end local v8    # "initials":Ljava/lang/String;
    .end local v9    # "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    .end local v15    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "no favorite contacts"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 156
    :cond_2
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    goto/16 :goto_0

    .line 158
    .end local v10    # "counter":I
    .end local v12    # "favContacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->getContactsNotAllowed()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 199
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 202
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 351
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x1

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 356
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 11
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    const/4 v10, 0x0

    const v5, 0x3f19999a    # 0.6f

    const/4 v4, 0x0

    .line 214
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-static {p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->findImageView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 220
    .local v3, "imageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    invoke-static {p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->findSmallImageView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v9

    check-cast v9, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 221
    .local v9, "smallImageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    invoke-static {p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->findCrossFadeImageView(Landroid/view/View;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 223
    .local v7, "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 226
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    instance-of v0, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    iget-object v8, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    .line 231
    .local v8, "number":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v0, v8, v1}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getImagePath(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v2

    .line 232
    .local v2, "imagePath":Ljava/io/File;
    invoke-static {v2}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 233
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_2

    .line 234
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 235
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v3, v4, v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 236
    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 238
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v9, v4, v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 239
    invoke-virtual {v9, v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 240
    invoke-virtual {v9, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setAlpha(F)V

    .line 241
    invoke-virtual {v7, v5}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setSmallAlpha(F)V

    .line 244
    iput-boolean v10, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    .line 245
    iput-boolean v10, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    goto :goto_0

    .line 229
    .end local v2    # "imagePath":Ljava/io/File;
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "number":Ljava/lang/String;
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v8, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    .restart local v8    # "number":Ljava/lang/String;
    goto :goto_1

    .line 249
    .restart local v2    # "imagePath":Ljava/io/File;
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 250
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v10

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu$1;

    move-object v1, p0

    move-object v4, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;Ljava/io/File;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V

    const/4 v1, 0x1

    invoke-virtual {v10, v0, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 298
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 292
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 295
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 2
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 277
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu$3;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 279
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->returnToCacheList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 280
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cm:unload add to cache"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->returnToCacheList:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->addToCache(Ljava/util/List;)V

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->returnToCacheList:Ljava/util/List;

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    goto :goto_0

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 11
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 315
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pos:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 317
    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v2, :sswitch_data_0

    .line 335
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "contact options"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 336
    const-string v2, "contact_options"

    invoke-static {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 337
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->cachedList:Ljava/util/List;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 338
    .local v7, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v6, v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v6, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 339
    .local v6, "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 340
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->bus:Lcom/squareup/otto/Bus;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;-><init>(Ljava/util/List;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/squareup/otto/Bus;)V

    .line 342
    .local v0, "contactOptionsMenu":Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v0, v3, v4, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 346
    .end local v0    # "contactOptionsMenu":Lcom/navdy/hud/app/ui/component/mainmenu/ContactOptionsMenu;
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .end local v6    # "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    .end local v7    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_0
    return v10

    .line 319
    :sswitch_0
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "back"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 320
    const-string v2, "back"

    invoke-static {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 321
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->backSelection:I

    iget v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->backSelectionId:I

    invoke-virtual {v2, v3, v4, v5, v8}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 322
    iput v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->backSelectionId:I

    goto :goto_0

    .line 326
    :sswitch_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "recent contacts"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    const-string v2, "recent_contacts"

    invoke-static {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 328
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

    if-nez v2, :cond_0

    .line 329
    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v2, v3, v4, v5, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

    .line 331
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->recentContactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/RecentContactsMenu;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3, v4, v5, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 317
    :sswitch_data_0
    .sparse-switch
        0x7f0e0009 -> :sswitch_1
        0x7f0e0047 -> :sswitch_0
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 188
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->backSelectionId:I

    .line 189
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 183
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->backSelection:I

    .line 184
    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 193
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f020172

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->contactColor:I

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 194
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->contactStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method
