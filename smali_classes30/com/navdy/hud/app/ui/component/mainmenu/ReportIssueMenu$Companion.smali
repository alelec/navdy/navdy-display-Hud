.class public final Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
.super Ljava/lang/Object;
.source "ReportIssueMenu.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0002\u0008\u0013\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000eR\u0014\u0010\u0011\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000eR-\u0010\u0013\u001a\u001e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00040\u0014j\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u0004`\u0015\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u000eR\u0014\u0010\u001e\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u000eR\u0014\u0010 \u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u000eR\u0014\u0010\"\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\u000eR\u0014\u0010$\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u0006R\u0014\u0010&\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010\nR\u001a\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u000c0)X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+R\u0014\u0010,\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010\u000eR\u0014\u0010.\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u0010\u000eR\u001a\u00100\u001a\u0008\u0012\u0004\u0012\u00020\u000c0)X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u0010+R\u0014\u00102\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00083\u0010\u0006R\u0014\u00104\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u0010\nR\u0014\u00106\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00087\u0010\u0006R\u0014\u00108\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00089\u0010\u000eR\u0014\u0010:\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008;\u0010\u000e\u00a8\u0006<"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;",
        "",
        "()V",
        "NAV_ISSUE_SENT_TOAST_ID",
        "",
        "getNAV_ISSUE_SENT_TOAST_ID",
        "()Ljava/lang/String;",
        "TOAST_TIMEOUT",
        "",
        "getTOAST_TIMEOUT",
        "()I",
        "back",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "getBack",
        "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "driveScore",
        "getDriveScore",
        "etaInaccurate",
        "getEtaInaccurate",
        "idToTitleMap",
        "Ljava/util/HashMap;",
        "Lkotlin/collections/HashMap;",
        "getIdToTitleMap",
        "()Ljava/util/HashMap;",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "getLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "maps",
        "getMaps",
        "navigation",
        "getNavigation",
        "notFastestRoute",
        "getNotFastestRoute",
        "permanentClosure",
        "getPermanentClosure",
        "reportIssue",
        "getReportIssue",
        "reportIssueColor",
        "getReportIssueColor",
        "reportIssueItems",
        "Ljava/util/ArrayList;",
        "getReportIssueItems",
        "()Ljava/util/ArrayList;",
        "roadBlocked",
        "getRoadBlocked",
        "smartDash",
        "getSmartDash",
        "takeSnapShotItems",
        "getTakeSnapShotItems",
        "takeSnapshot",
        "getTakeSnapshot",
        "takeSnapshotColor",
        "getTakeSnapshotColor",
        "toastSentSuccessfully",
        "getToastSentSuccessfully",
        "wrongDirection",
        "getWrongDirection",
        "wrongRoadName",
        "getWrongRoadName",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getDriveScore$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getDriveScore()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getEtaInaccurate$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getEtaInaccurate()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getMaps$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getMaps()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getNAV_ISSUE_SENT_TOAST_ID$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getNAV_ISSUE_SENT_TOAST_ID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getNavigation$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getNavigation()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getNotFastestRoute$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getNotFastestRoute()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getPermanentClosure$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getPermanentClosure()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getReportIssue$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getReportIssueColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueColor()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getReportIssueItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getReportIssueItems()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getRoadBlocked$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getRoadBlocked()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSmartDash$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getSmartDash()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getTOAST_TIMEOUT$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTOAST_TIMEOUT()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getTakeSnapShotItems$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapShotItems()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getTakeSnapshot$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshot()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getTakeSnapshotColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getTakeSnapshotColor()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getToastSentSuccessfully$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getToastSentSuccessfully()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getWrongDirection$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getWrongDirection()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getWrongRoadName$p(Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;->getWrongRoadName()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 49
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getBack$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getDriveScore()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 60
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->driveScore:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getDriveScore$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getEtaInaccurate()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 51
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->etaInaccurate:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getEtaInaccurate$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 43
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getMaps()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 58
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->maps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getMaps$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getNAV_ISSUE_SENT_TOAST_ID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->NAV_ISSUE_SENT_TOAST_ID:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getNAV_ISSUE_SENT_TOAST_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getNavigation()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 59
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->navigation:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getNavigation$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getNotFastestRoute()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 52
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->notFastestRoute:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getNotFastestRoute$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getPermanentClosure()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 56
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->permanentClosure:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getPermanentClosure$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getReportIssue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssue:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getReportIssue$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getReportIssueColor()I
    .locals 1

    .prologue
    .line 66
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssueColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getReportIssueColor$cp()I

    move-result v0

    return v0
.end method

.method private final getReportIssueItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->reportIssueItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getReportIssueItems$cp()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private final getRoadBlocked()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 53
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->roadBlocked:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getRoadBlocked$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getSmartDash()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 61
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->smartDash:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getSmartDash$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getTOAST_TIMEOUT()I
    .locals 1

    .prologue
    .line 46
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->TOAST_TIMEOUT:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getTOAST_TIMEOUT$cp()I

    move-result v0

    return v0
.end method

.method private final getTakeSnapShotItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapShotItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getTakeSnapShotItems$cp()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private final getTakeSnapshot()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapshot:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getTakeSnapshot$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getTakeSnapshotColor()I
    .locals 1

    .prologue
    .line 68
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->takeSnapshotColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getTakeSnapshotColor$cp()I

    move-result v0

    return v0
.end method

.method private final getToastSentSuccessfully()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->toastSentSuccessfully:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getToastSentSuccessfully$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getWrongDirection()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 54
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->wrongDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getWrongDirection$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getWrongRoadName()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 55
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->wrongRoadName:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getWrongRoadName$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getIdToTitleMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 44
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->idToTitleMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;->access$getIdToTitleMap$cp()Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method
