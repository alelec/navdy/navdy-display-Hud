.class synthetic Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;
.super Ljava/lang/Object;
.source "MainMenuScreen2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 620
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ACTIVE_TRIP:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    .line 571
    :goto_1
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    .line 344
    :goto_3
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->REFRESH_CURRENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ROOT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    .line 204
    :goto_7
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode:[I

    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->REPLY_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$1;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->SNAPSHOT_TITLE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    move-exception v0

    goto :goto_a

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_8

    .line 344
    :catch_3
    move-exception v0

    goto :goto_7

    :catch_4
    move-exception v0

    goto :goto_6

    :catch_5
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_4

    .line 571
    :catch_7
    move-exception v0

    goto :goto_3

    :catch_8
    move-exception v0

    goto :goto_2

    .line 620
    :catch_9
    move-exception v0

    goto/16 :goto_1

    :catch_a
    move-exception v0

    goto/16 :goto_0
.end method
