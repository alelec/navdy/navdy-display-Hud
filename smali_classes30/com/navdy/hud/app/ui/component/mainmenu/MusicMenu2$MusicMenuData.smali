.class Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;
.super Ljava/lang/Object;
.source "MusicMenu2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MusicMenuData"
.end annotation


# instance fields
.field musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

.field musicCollections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field musicIndex:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;"
        }
    .end annotation
.end field

.field musicTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 236
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    .line 233
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    .line 234
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicIndex:Ljava/util/List;

    .line 236
    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;Ljava/util/List;)V
    .locals 1
    .param p2, "musicCollectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "musicCollections":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    const/4 v0, 0x0

    .line 238
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    .line 233
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    .line 234
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicIndex:Ljava/util/List;

    .line 239
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 240
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    .line 241
    return-void
.end method
