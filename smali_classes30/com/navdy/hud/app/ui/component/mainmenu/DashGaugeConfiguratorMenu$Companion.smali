.class public final Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
.super Ljava/lang/Object;
.source "DashGaugeConfiguratorMenu.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\nR\u0014\u0010\r\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\nR\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0012R\u0014\u0010!\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u0016R\u0014\u0010#\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u0006R\u0014\u0010%\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u0006R\u0014\u0010\'\u001a\u00020(X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010*R\u0014\u0010+\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010\u0016\u00a8\u0006-"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;",
        "",
        "()V",
        "back",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "getBack",
        "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "backColor",
        "",
        "getBackColor",
        "()I",
        "bkColorUnselected",
        "getBkColorUnselected",
        "centerGaugeBackgroundColor",
        "getCenterGaugeBackgroundColor",
        "centerGaugeOptionsList",
        "Ljava/util/ArrayList;",
        "getCenterGaugeOptionsList",
        "()Ljava/util/ArrayList;",
        "centerGaugeTitle",
        "",
        "getCenterGaugeTitle",
        "()Ljava/lang/String;",
        "offLabel",
        "getOffLabel",
        "onLabel",
        "getOnLabel",
        "sLogger",
        "Lcom/navdy/service/library/log/Logger;",
        "getSLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "sideGaugesOptionsList",
        "getSideGaugesOptionsList",
        "sideGaugesTitle",
        "getSideGaugesTitle",
        "speedoMeter",
        "getSpeedoMeter",
        "tachoMeter",
        "getTachoMeter",
        "uiStateManager",
        "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
        "getUiStateManager",
        "()Lcom/navdy/hud/app/ui/framework/UIStateManager;",
        "unavailableLabel",
        "getUnavailableLabel",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getBackColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getBackColor()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getBkColorUnselected$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getBkColorUnselected()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getCenterGaugeBackgroundColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeBackgroundColor()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getCenterGaugeOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeOptionsList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getCenterGaugeTitle$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getCenterGaugeTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getOffLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getOffLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getOnLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getOnLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSLogger()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSideGaugesOptionsList$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesOptionsList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSideGaugesTitle$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSideGaugesTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSpeedoMeter$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getSpeedoMeter()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getTachoMeter$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getTachoMeter()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getUiStateManager$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getUnavailableLabel$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;)Ljava/lang/String;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;->getUnavailableLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 93
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getBack$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getBackColor()I
    .locals 1

    .prologue
    .line 96
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->backColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getBackColor$cp()I

    move-result v0

    return v0
.end method

.method private final getBkColorUnselected()I
    .locals 1

    .prologue
    .line 97
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->bkColorUnselected:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getBkColorUnselected$cp()I

    move-result v0

    return v0
.end method

.method private final getCenterGaugeBackgroundColor()I
    .locals 1

    .prologue
    .line 104
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeBackgroundColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getCenterGaugeBackgroundColor$cp()I

    move-result v0

    return v0
.end method

.method private final getCenterGaugeOptionsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeOptionsList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getCenterGaugeOptionsList$cp()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private final getCenterGaugeTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->centerGaugeTitle:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getCenterGaugeTitle$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getOffLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->offLabel:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getOffLabel$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getOnLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->onLabel:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getOnLabel$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getSLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 100
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getSLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getSideGaugesOptionsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sideGaugesOptionsList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getSideGaugesOptionsList$cp()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private final getSideGaugesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->sideGaugesTitle:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getSideGaugesTitle$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getSpeedoMeter()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 95
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->speedoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getSpeedoMeter$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getTachoMeter()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 94
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->tachoMeter:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getTachoMeter$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1

    .prologue
    .line 103
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getUiStateManager$cp()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    return-object v0
.end method

.method private final getUnavailableLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->unavailableLabel:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getUnavailableLabel$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
