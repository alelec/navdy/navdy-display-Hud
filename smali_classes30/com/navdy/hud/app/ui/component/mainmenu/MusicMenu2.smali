.class public Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
.super Ljava/lang/Object;
.source "MusicMenu2.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;,
        Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;,
        Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;
    }
.end annotation


# static fields
.field private static analyticsTypeStringMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static androidBgColor:I

.field private static appleMusicBgColor:I

.field private static applePodcastsBgColor:I

.field private static final artworkRequestLock:Ljava/lang/Object;

.field private static artworkRequestQueue:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicArtworkRequest;",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private static artworkSize:I

.field private static back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static backColor:I

.field private static bgColorUnselected:I

.field private static busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

.field private static infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

.field private static lastPlayedMusicMenuData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;",
            ">;"
        }
    .end annotation
.end field

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static menuSourceIconMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionSource;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static menuSourceStringMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionSource;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static menuTypeIconMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static menuTypeStringMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mmMusicColor:I

.field private static resources:Landroid/content/res/Resources;

.field private static shuffle:Ljava/lang/String;

.field private static shuffleOff:Ljava/lang/String;

.field private static shuffleOn:Ljava/lang/String;

.field private static shufflePlay:Ljava/lang/String;

.field private static sourceBgColor:I

.field private static transparentColor:I


# instance fields
.field private anyPlayersPermitted:Z

.field private artWorkRequestTime:J

.field private backSelection:I

.field private backSelectionId:I

.field private bgColorSelected:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private currentMusicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

.field private currentMusicCollectionRequestTime:J

.field private fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

.field private handler:Landroid/os/Handler;

.field private highlightedItem:I

.field private initialized:Z

.field private isRequestingMusicCollection:Z

.field private isShuffling:Z

.field private final loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

.field private menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

.field private musicAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field musicArtworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field musicCollectionResponseMessageCache:Lcom/navdy/hud/app/storage/cache/MessageCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/navdy/hud/app/storage/cache/MessageCache",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private musicCollectionSyncComplete:Z

.field private final musicManager:Lcom/navdy/hud/app/manager/MusicManager;

.field private nextRequestLimit:I

.field private nextRequestOffset:I

.field private nowPlayingTrackPosition:I

.field private parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private partialRefresh:Z

.field private path:Ljava/lang/String;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private requestMusicOffset:I

.field private requestedArtworkModelPositions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private returnToCacheList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private trackIdToPositionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 80
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->lastPlayedMusicMenuData:Ljava/util/Map;

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceStringMap:Ljava/util/Map;

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceIconMap:Ljava/util/Map;

    .line 131
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    .line 148
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestQueue:Ljava/util/Stack;

    .line 150
    sput-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 151
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestLock:Ljava/lang/Object;

    .line 163
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->analyticsTypeStringMap:Ljava/util/Map;

    .line 245
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    .line 247
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09019c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceIconMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    const v2, 0x7f02012f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceIconMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    const v2, 0x7f0200d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090206

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09020c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const v2, 0x7f0200cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const v2, 0x7f0200d5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const v2, 0x7f0201c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const v2, 0x7f0201c5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->mmMusicColor:I

    .line 264
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->sourceBgColor:I

    .line 265
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    .line 266
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->androidBgColor:I

    .line 267
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->appleMusicBgColor:I

    .line 268
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->applePodcastsBgColor:I

    .line 269
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->transparentColor:I

    .line 271
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0b01f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkSize:I

    .line 273
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backColor:I

    .line 274
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backColor:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backColor:I

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v7, 0x7f09002d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 276
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090261

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shuffle:Ljava/lang/String;

    .line 277
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090264

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shufflePlay:Ljava/lang/String;

    .line 278
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090263

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shuffleOn:Ljava/lang/String;

    .line 279
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090262

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shuffleOff:Ljava/lang/String;

    .line 281
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->analyticsTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "Album"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->analyticsTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "Artist"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->analyticsTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "Playlist"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->analyticsTypeStringMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "Podcast"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;)V
    .locals 4
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vmenuComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parentMenu"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p5, "menuData"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->path:Ljava/lang/String;

    .line 101
    iput v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->highlightedItem:I

    .line 102
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isRequestingMusicCollection:Z

    .line 105
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicCollectionSyncComplete:Z

    .line 106
    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    .line 107
    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestLimit:I

    .line 108
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->anyPlayersPermitted:Z

    .line 109
    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    .line 111
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    invoke-direct {v0, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    .line 119
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    .line 120
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    .line 125
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handler:Landroid/os/Handler;

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestedArtworkModelPositions:Ljava/util/Map;

    .line 149
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artWorkRequestTime:J

    .line 158
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->trackIdToPositionMap:Ljava/util/Map;

    .line 166
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->initialized:Z

    .line 169
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isShuffling:Z

    .line 288
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    .line 289
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 290
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 291
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 292
    if-eqz p5, :cond_0

    .line 293
    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    .line 297
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 298
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MusicMenu "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 300
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    .line 301
    return-void

    .line 295
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onMusicCollectionResponse(Lcom/navdy/service/library/events/audio/MusicCollectionResponse;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/manager/MusicManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onMusicArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    return-void
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;[BLjava/lang/Integer;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .param p1, "x1"    # [B
    .param p2, "x2"    # Ljava/lang/Integer;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handleArtwork([BLjava/lang/Integer;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    .param p2, "x2"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->postOrQueueArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V

    return-void
.end method

.method private buildIndexFromCharacterMap(Ljava/util/List;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;)",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;"
        }
    .end annotation

    .prologue
    .line 1461
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    :try_start_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;-><init>()V

    .line 1462
    .local v0, "builder":Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicCharacterMap;

    .line 1463
    .local v1, "map":Lcom/navdy/service/library/events/audio/MusicCharacterMap;
    iget-object v4, v1, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->character:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iget-object v5, v1, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->offset:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->setEntry(CI)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1468
    .end local v0    # "builder":Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
    .end local v1    # "map":Lcom/navdy/service/library/events/audio/MusicCharacterMap;
    :catch_0
    move-exception v2

    .line 1469
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "buildIndexFromCharacterMap"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1470
    const/4 v3, 0x0

    .end local v2    # "t":Ljava/lang/Throwable;
    :goto_1
    return-object v3

    .line 1466
    .restart local v0    # "builder":Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
    :cond_0
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->positionOffset(I)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;

    .line 1467
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->build()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_1
.end method

.method private buildModelfromCollectionInfo(ILcom/navdy/service/library/events/audio/MusicCollectionInfo;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 11
    .param p1, "id"    # I
    .param p2, "collectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 1498
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->subtitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1499
    iget-object v6, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->subtitle:Ljava/lang/String;

    .line 1504
    .local v6, "subtitle":Ljava/lang/String;
    :goto_0
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v0, v1, :cond_2

    sget-object v8, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 1505
    .local v8, "iconShape":Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
    :goto_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->transparentColor:I

    iget-object v5, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    const/4 v7, 0x0

    move v0, p1

    invoke-static/range {v0 .. v8}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v10

    .line 1506
    .local v10, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iput-object p2, v10, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 1507
    return-object v10

    .line 1501
    .end local v6    # "subtitle":Ljava/lang/String;
    .end local v8    # "iconShape":Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
    .end local v10    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v0, v1, :cond_1

    const v9, 0x7f080001

    .line 1502
    .local v9, "collectionResId":I
    :goto_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    aput-object v4, v2, v3

    invoke-virtual {v0, v9, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "subtitle":Ljava/lang/String;
    goto :goto_0

    .line 1501
    .end local v6    # "subtitle":Ljava/lang/String;
    .end local v9    # "collectionResId":I
    :cond_1
    const v9, 0x7f080002

    goto :goto_2

    .line 1504
    .restart local v6    # "subtitle":Ljava/lang/String;
    :cond_2
    sget-object v8, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->SQUARE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    goto :goto_1
.end method

.method private calculateOffset(IIZ)V
    .locals 5
    .param p1, "currentPos"    # I
    .param p2, "pageLimit"    # I
    .param p3, "hasCollectionInfo"    # Z

    .prologue
    .line 1487
    div-int v3, p1, p2

    mul-int v2, v3, p2

    .line 1488
    .local v2, "top":I
    add-int v0, v2, p2

    .line 1489
    .local v0, "bottom":I
    if-eqz p3, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1490
    .local v1, "size":I
    :goto_0
    add-int/lit8 v3, v1, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1491
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;->clear()V

    .line 1492
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    iput v2, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;->offset:I

    .line 1493
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    sub-int v4, v0, v2

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;->limit:I

    .line 1494
    return-void

    .line 1489
    .end local v1    # "size":I
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0
.end method

.method private checkCharacterMap(Ljava/util/List;IIZ)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 6
    .param p2, "total"    # I
    .param p3, "current"    # I
    .param p4, "check"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;IIZ)",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;"
        }
    .end annotation

    .prologue
    .line 1438
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    .line 1439
    .local v0, "hasCharacterMap":Z
    :goto_0
    const/4 v2, -0x1

    .line 1440
    .local v2, "size":I
    if-eqz v0, :cond_0

    .line 1441
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 1443
    :cond_0
    if-eqz v0, :cond_5

    .line 1444
    if-eqz p4, :cond_1

    const/16 v3, 0x28

    if-le p2, v3, :cond_4

    const/4 v3, 0x2

    if-lt v2, v3, :cond_4

    .line 1445
    :cond_1
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->buildIndexFromCharacterMap(Ljava/util/List;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v1

    .line 1446
    .local v1, "scrollIndex":Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    if-eqz v1, :cond_3

    .line 1447
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkCharacterMap index-yes len["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] map["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] index["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getEntryCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1456
    .end local v1    # "scrollIndex":Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    :goto_1
    return-object v1

    .line 1438
    .end local v0    # "hasCharacterMap":Z
    .end local v2    # "size":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1449
    .restart local v0    # "hasCharacterMap":Z
    .restart local v1    # "scrollIndex":Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .restart local v2    # "size":I
    :cond_3
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkCharacterMap index-error len["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] map["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 1453
    .end local v1    # "scrollIndex":Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkCharacterMap index-no created len["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] index-len["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1456
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static clearMenuData()V
    .locals 1

    .prologue
    .line 1394
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->lastPlayedMusicMenuData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1395
    return-void
.end method

.method private collectionIdString(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)Ljava/lang/String;
    .locals 3
    .param p1, "artworkResponse"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .prologue
    .line 830
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceStringMap:Ljava/util/Map;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 831
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;
    .locals 4
    .param p1, "info"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 820
    if-nez p1, :cond_0

    .line 821
    const/4 v1, 0x0

    .line 825
    :goto_0
    return-object v1

    .line 823
    :cond_0
    invoke-static {p1}, Lcom/navdy/service/library/events/MessageStore;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 824
    .local v0, "defaults":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceStringMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 825
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getMusicPlayerMenu(II)Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .locals 12
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 1330
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMusicPlayerMenu "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1332
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getSubPath(II)Ljava/lang/String;

    move-result-object v11

    .line 1333
    .local v11, "subPath":Ljava/lang/String;
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->lastPlayedMusicMenuData:Ljava/util/Map;

    invoke-interface {v1, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->lastPlayedMusicMenuData:Ljava/util/Map;

    invoke-interface {v1, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    .line 1372
    .local v5, "musicMenuData":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;
    :goto_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;)V

    .line 1373
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    invoke-virtual {v0, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->setBackSelectionPos(I)V

    .line 1374
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->setBackSelectionId(I)V

    .line 1375
    return-object v0

    .line 1336
    .end local v0    # "menu":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    .end local v5    # "musicMenuData":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;
    :cond_0
    const/4 v10, 0x0

    .line 1337
    .local v10, "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    const/4 v9, 0x0

    .line 1338
    .local v9, "collections":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-nez v1, :cond_3

    .line 1340
    const v1, 0x7f0e0008

    if-ne p1, v1, :cond_2

    .line 1341
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 1342
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 1343
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v1

    .line 1344
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v10

    .line 1368
    :cond_1
    :goto_1
    new-instance v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    invoke-direct {v5, p0, v10, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;Ljava/util/List;)V

    .restart local v5    # "musicMenuData":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;
    goto :goto_0

    .line 1346
    .end local v5    # "musicMenuData":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 1347
    .local v6, "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    iget-object v2, v6, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 1348
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v1

    .line 1349
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v10

    .line 1350
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    iget-object v2, v6, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/manager/MusicManager;->getCollectionTypesForSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Ljava/util/List;

    move-result-object v8

    .line 1351
    .local v8, "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "collections":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1352
    .restart local v9    # "collections":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 1353
    .local v7, "collectionType":Lcom/navdy/service/library/events/audio/MusicCollectionType;
    new-instance v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    iget-object v3, v6, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 1354
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v2

    .line 1355
    invoke-virtual {v2, v7}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v2

    .line 1356
    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v2

    .line 1353
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1359
    .end local v6    # "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .end local v7    # "collectionType":Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .end local v8    # "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-nez v1, :cond_4

    .line 1360
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 1361
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v2

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    .line 1362
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v1

    .line 1363
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v10

    goto/16 :goto_1

    .line 1364
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1365
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    check-cast v10, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 1366
    .restart local v10    # "info":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fetching collection "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v10, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private getShuffleModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 7

    .prologue
    .line 468
    const/4 v6, 0x0

    .line 469
    .local v6, "shuffleSubtitle":Ljava/lang/String;
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isThisQueuePlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 470
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shuffle:Ljava/lang/String;

    .line 471
    .local v5, "shuffleTitle":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->isShuffling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shuffleOn:Ljava/lang/String;

    .line 473
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isShuffling:Z

    .line 480
    :goto_0
    const v0, 0x7f0e0056

    const v1, 0x7f0201e1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0

    .line 475
    :cond_0
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shuffleOff:Ljava/lang/String;

    goto :goto_0

    .line 478
    .end local v5    # "shuffleTitle":Ljava/lang/String;
    :cond_1
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->shufflePlay:Ljava/lang/String;

    .restart local v5    # "shuffleTitle":Ljava/lang/String;
    goto :goto_0
.end method

.method private getSubPath(II)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 1390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleArtwork([BLjava/lang/Integer;Ljava/lang/String;Z)V
    .locals 11
    .param p1, "byteArray"    # [B
    .param p2, "pos"    # Ljava/lang/Integer;
    .param p3, "idString"    # Ljava/lang/String;
    .param p4, "saveInDiskCache"    # Z

    .prologue
    .line 1283
    sget v9, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkSize:I

    .line 1284
    .local v9, "size":I
    sget-object v0, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    invoke-static {p1, v9, v9, v0}, Lcom/navdy/service/library/util/ScalingUtilities;->decodeByteArray([BIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1286
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v0, v1, :cond_0

    .line 1287
    new-instance v0, Lcom/makeramen/RoundedTransformationBuilder;

    invoke-direct {v0}, Lcom/makeramen/RoundedTransformationBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/makeramen/RoundedTransformationBuilder;->oval(Z)Lcom/makeramen/RoundedTransformationBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/makeramen/RoundedTransformationBuilder;->build()Lcom/squareup/picasso/Transformation;

    move-result-object v8

    .line 1288
    .local v8, "roundTransformation":Lcom/squareup/picasso/Transformation;
    invoke-interface {v8, v7}, Lcom/squareup/picasso/Transformation;->transform(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1293
    .end local v8    # "roundTransformation":Lcom/squareup/picasso/Transformation;
    .local v3, "artwork":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p2

    move v5, p4

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$7;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/Integer;Z[B)V

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1326
    return-void

    .line 1290
    .end local v3    # "artwork":Landroid/graphics/Bitmap;
    :cond_0
    move-object v3, v7

    .restart local v3    # "artwork":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method private hasShuffleEntry()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1511
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    const v2, 0x7f0e0056

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 1514
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isIndexSupported(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Z
    .locals 2
    .param p1, "collectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 1475
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v0, :cond_0

    .line 1476
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$8;->$SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1483
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1480
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1476
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isThisQueuePlaying()Z
    .locals 2

    .prologue
    .line 1434
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager;->getMusicMenuPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private onMusicArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
    .locals 10
    .param p1, "artworkResponse"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .prologue
    .line 1239
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "REQUESTQUEUE onMusicArtworkResponse "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1240
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)Ljava/lang/String;

    move-result-object v4

    .line 1241
    .local v4, "idString":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestedArtworkModelPositions:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 1242
    .local v5, "pos":Ljava/lang/Integer;
    if-nez v5, :cond_0

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-direct {p0, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1243
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Wrong menu"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1280
    :goto_0
    return-void

    .line 1246
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artWorkRequestTime:J

    sub-long v0, v6, v8

    .line 1248
    .local v0, "artWorkResponseTime":J
    iget-object v6, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    if-eqz v6, :cond_1

    .line 1249
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Timing] MusicArtworkRequest, response received in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " MS, Size : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    invoke-virtual {v8}, Lokio/ByteString;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1250
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;

    invoke-direct {v7, p0, p1, v5, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$6;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicArtworkResponse;Ljava/lang/Integer;Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1266
    :goto_1
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestLock:Ljava/lang/Object;

    monitor-enter v7

    .line 1267
    :try_start_0
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestQueue:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1268
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestQueue:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 1269
    .local v3, "artworkRequestPair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .line 1270
    .local v2, "artworkRequest":Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "REQUESTQUEUE onMusicArtworkResponse posting: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1271
    iget-object v6, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    sput-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 1272
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artWorkRequestTime:J

    .line 1273
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[Timing] Making MusicArtWorkRequest "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1274
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v8, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v6, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1279
    .end local v2    # "artworkRequest":Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    .end local v3    # "artworkRequestPair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    :goto_2
    monitor-exit v7

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 1264
    :cond_1
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "No photo in artwork response"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 1276
    :cond_2
    :try_start_1
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "REQUESTQUEUE onMusicArtworkResponse queue empty"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1277
    const/4 v6, 0x0

    sput-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method private onMusicCollectionResponse(Lcom/navdy/service/library/events/audio/MusicCollectionResponse;)V
    .locals 30
    .param p1, "musicCollectionResponse"    # Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    .prologue
    .line 1075
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v5, v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1076
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "wrong collection"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1236
    :cond_0
    :goto_0
    return-void

    .line 1080
    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 1081
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "no outstanding request"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 1085
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->currentMusicCollectionRequestTime:J

    sub-long v22, v4, v6

    .line 1086
    .local v22, "musicCollectionRequestResponseTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1088
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->currentMusicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    invoke-static {v4}, Lcom/navdy/hud/app/ExtensionsKt;->cacheKey(Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)Ljava/lang/String;

    move-result-object v13

    .line 1089
    .local v13, "cacheKey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicCollectionResponseMessageCache:Lcom/navdy/hud/app/storage/cache/MessageCache;

    move-object/from16 v0, p1

    invoke-virtual {v4, v13, v0}, Lcom/navdy/hud/app/storage/cache/MessageCache;->put(Ljava/lang/String;Lcom/squareup/wire/Message;)V

    .line 1092
    .end local v13    # "cacheKey":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-nez v4, :cond_5

    .line 1093
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iput-object v5, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 1107
    :goto_1
    const/16 v16, 0x1

    .line 1108
    .local v16, "firstResponse":Z
    const/16 v29, 0x0

    .line 1110
    .local v29, "updatedModels":[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    if-eqz v4, :cond_f

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_f

    .line 1111
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    if-nez v4, :cond_8

    .line 1112
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    .line 1116
    :goto_2
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Timing] MusicCollectionRequest , received response  in : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " MS, Collections : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1117
    if-eqz v16, :cond_e

    .line 1119
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1120
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 1121
    .local v27, "total":I
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v15

    .line 1122
    .local v15, "current":I
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v4, v1, v15, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->checkCharacterMap(Ljava/util/List;IIZ)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    .line 1123
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    if-eqz v4, :cond_4

    .line 1124
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    iput-object v5, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicIndex:Ljava/util/List;

    .line 1126
    :cond_4
    move/from16 v0, v27

    if-eq v0, v15, :cond_a

    .line 1128
    sub-int v25, v27, v15

    .line 1129
    .local v25, "remaining":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_3
    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_9

    .line 1130
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1129
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 1095
    .end local v15    # "current":I
    .end local v16    # "firstResponse":Z
    .end local v17    # "i":I
    .end local v25    # "remaining":I
    .end local v27    # "total":I
    .end local v29    # "updatedModels":[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v11, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 1097
    .local v11, "backupMusicCollectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    new-instance v12, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-direct {v12, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V

    .line 1098
    .local v12, "builder":Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1099
    iget-object v4, v11, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    invoke-virtual {v12, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    .line 1101
    :cond_6
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->subtitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1102
    iget-object v4, v11, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->subtitle:Ljava/lang/String;

    invoke-virtual {v12, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    .line 1104
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v5

    iput-object v5, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    goto/16 :goto_1

    .line 1114
    .end local v11    # "backupMusicCollectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .end local v12    # "builder":Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;
    .restart local v16    # "firstResponse":Z
    .restart local v29    # "updatedModels":[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_8
    const/16 v16, 0x0

    goto/16 :goto_2

    .line 1132
    .restart local v15    # "current":I
    .restart local v17    # "i":I
    .restart local v25    # "remaining":I
    .restart local v27    # "total":I
    :cond_9
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1-resp added["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1189
    .end local v15    # "current":I
    .end local v17    # "i":I
    .end local v25    # "remaining":I
    .end local v27    # "total":I
    :cond_a
    :goto_4
    if-nez v16, :cond_b

    if-nez v29, :cond_15

    .line 1190
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->cancelLoadingAnimation(I)V

    .line 1191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->highlightedItem:I

    .line 1192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->updateCurrentMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    .line 1215
    :goto_5
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    .line 1216
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isRequestingMusicCollection:Z

    .line 1218
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 1219
    const/16 v26, 0x0

    .line 1220
    .local v26, "request":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    if-eqz v4, :cond_19

    .line 1221
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_c

    .line 1222
    const/16 v26, 0x1

    .line 1229
    :cond_c
    :goto_6
    if-eqz v26, :cond_d

    .line 1230
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestLimit:I

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicCollection(IIZ)V

    .line 1231
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pending request:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestLimit:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " limit="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestLimit:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1233
    :cond_d
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    .line 1234
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestLimit:I

    goto/16 :goto_0

    .line 1136
    .end local v26    # "request":Z
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 1137
    .restart local v27    # "total":I
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v18

    .line 1138
    .local v18, "items":I
    move/from16 v0, v18

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v29, v0

    .line 1139
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "2-resp offset["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] total["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] got["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1140
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_7
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_a

    .line 1141
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 1142
    .local v14, "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    add-int v5, v5, v17

    invoke-interface {v4, v5, v14}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1143
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    add-int v4, v4, v17

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->buildModelfromCollectionInfo(ILcom/navdy/service/library/events/audio/MusicCollectionInfo;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v4

    aput-object v4, v29, v17

    .line 1140
    add-int/lit8 v17, v17, 0x1

    goto :goto_7

    .line 1146
    .end local v14    # "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .end local v17    # "i":I
    .end local v18    # "items":I
    .end local v27    # "total":I
    :cond_f
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    if-eqz v4, :cond_14

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_14

    .line 1147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    if-nez v4, :cond_11

    .line 1148
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    .line 1152
    :goto_8
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Timing] MusicCollectionRequest , received response  in : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " MS, Tracks : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1153
    if-eqz v16, :cond_13

    .line 1155
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1156
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 1157
    .restart local v27    # "total":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v15

    .line 1158
    .restart local v15    # "current":I
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v4, v1, v15, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->checkCharacterMap(Ljava/util/List;IIZ)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    .line 1159
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    if-eqz v4, :cond_10

    .line 1160
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    iput-object v5, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicIndex:Ljava/util/List;

    .line 1162
    :cond_10
    move/from16 v0, v27

    if-eq v0, v15, :cond_a

    .line 1164
    sub-int v25, v27, v15

    .line 1165
    .restart local v25    # "remaining":I
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_9
    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_12

    .line 1166
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165
    add-int/lit8 v17, v17, 0x1

    goto :goto_9

    .line 1150
    .end local v15    # "current":I
    .end local v17    # "i":I
    .end local v25    # "remaining":I
    .end local v27    # "total":I
    :cond_11
    const/16 v16, 0x0

    goto/16 :goto_8

    .line 1168
    .restart local v15    # "current":I
    .restart local v17    # "i":I
    .restart local v25    # "remaining":I
    .restart local v27    # "total":I
    :cond_12
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1-resp-track added["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1172
    .end local v15    # "current":I
    .end local v17    # "i":I
    .end local v25    # "remaining":I
    .end local v27    # "total":I
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 1173
    .restart local v27    # "total":I
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v18

    .line 1174
    .restart local v18    # "items":I
    move/from16 v0, v18

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v29, v0

    .line 1175
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "2-resp-track offset["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] total["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]  got["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1176
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_a
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_a

    .line 1177
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 1178
    .local v28, "trackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    add-int v5, v5, v17

    move-object/from16 v0, v28

    invoke-interface {v4, v5, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1179
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    add-int v4, v4, v17

    const v5, 0x7f0201ee

    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    move-object/from16 v0, v28

    iget-object v9, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v28

    iget-object v10, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-static/range {v4 .. v10}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v20

    .line 1180
    .local v20, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v28

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 1181
    aput-object v20, v29, v17

    .line 1176
    add-int/lit8 v17, v17, 0x1

    goto :goto_a

    .line 1185
    .end local v17    # "i":I
    .end local v18    # "items":I
    .end local v20    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v27    # "total":I
    .end local v28    # "trackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_14
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "No collections or tracks in this collection..."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1186
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicCollectionSyncComplete:Z

    goto/16 :goto_4

    .line 1194
    :cond_15
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    add-int/lit8 v24, v4, 0x1

    .line 1195
    .local v24, "pos":I
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->hasShuffleEntry()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 1196
    add-int/lit8 v24, v24, 0x1

    .line 1198
    :cond_16
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updatemodels pos="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " len="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1199
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->partialRefresh:Z

    .line 1200
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    if-eqz v4, :cond_18

    .line 1201
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v19

    .line 1202
    .local v19, "len":I
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_b
    move-object/from16 v0, v29

    array-length v4, v0

    move/from16 v0, v17

    if-ge v0, v4, :cond_18

    .line 1203
    add-int v21, v24, v17

    .line 1204
    .local v21, "newPos":I
    move/from16 v0, v21

    move/from16 v1, v19

    if-ge v0, v1, :cond_17

    .line 1205
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    aget-object v5, v29, v17

    move/from16 v0, v21

    invoke-interface {v4, v0, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1202
    add-int/lit8 v17, v17, 0x1

    goto :goto_b

    .line 1207
    :cond_17
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid index:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " size:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1212
    .end local v17    # "i":I
    .end local v19    # "len":I
    .end local v21    # "newPos":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshData(I[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    goto/16 :goto_5

    .line 1224
    .end local v24    # "pos":I
    .restart local v26    # "request":Z
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    if-eqz v4, :cond_c

    .line 1225
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_c

    .line 1226
    const/16 v26, 0x1

    goto/16 :goto_6
.end method

.method private onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 6
    .param p1, "musicTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isThisQueuePlaying()Z

    move-result v3

    if-nez v3, :cond_1

    .line 208
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "This queue isn\'t playing"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    .line 212
    .local v2, "trackId":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->trackIdToPositionMap:Ljava/util/Map;

    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->trackIdToPositionMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 213
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->trackIdToPositionMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 214
    .local v0, "pos":Ljava/lang/Integer;
    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nowPlayingTrackPosition:I

    if-lez v3, :cond_2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nowPlayingTrackPosition:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v3, v4, :cond_2

    .line 215
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->stopAudioAnimation()V

    .line 217
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nowPlayingTrackPosition:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(I)V

    .line 219
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(I)V

    .line 220
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nowPlayingTrackPosition:I

    .line 222
    .end local v0    # "pos":Ljava/lang/Integer;
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/MusicManager;->isShuffling()Z

    move-result v1

    .line 223
    .local v1, "shuffle":Z
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isShuffling:Z

    if-eq v3, v1, :cond_0

    .line 225
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isShuffling:Z

    .line 226
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getShuffleModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_0
.end method

.method private postOrQueueArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V
    .locals 4
    .param p1, "artworkRequest"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    .param p2, "collectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 604
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestLock:Ljava/lang/Object;

    monitor-enter v1

    .line 605
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestQueue:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-nez v0, :cond_0

    .line 606
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REQUESTQUEUE postOrQueueArtworkRequest posting: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " title:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 607
    sput-object p2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 608
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artWorkRequestTime:J

    .line 609
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Timing] Making MusicArtWorkRequest "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 610
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v2, p1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 615
    :goto_0
    monitor-exit v1

    .line 616
    return-void

    .line 612
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REQUESTQUEUE postOrQueueArtworkRequest queueing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " title:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 613
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestQueue:Ljava/util/Stack;

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 615
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private purgeArtworkQueue()V
    .locals 5

    .prologue
    .line 619
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestLock:Ljava/lang/Object;

    monitor-enter v2

    .line 620
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestQueue:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v0

    .line 621
    .local v0, "size":I
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->artworkRequestQueue:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->clear()V

    .line 622
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "REQUESTQUEUE purged: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 623
    monitor-exit v2

    .line 624
    return-void

    .line 623
    .end local v0    # "size":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private requestMusicCollection(IIZ)V
    .locals 7
    .param p1, "offset"    # I
    .param p2, "limit"    # I
    .param p3, "askForIndex"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    .line 538
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestMusicCollection Offset : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Limit : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", AFI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 539
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicCollectionSyncComplete:Z

    if-eqz v2, :cond_0

    .line 540
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "sync is complete"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 601
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isRequestingMusicCollection:Z

    if-eqz v2, :cond_1

    .line 544
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Request already in process next="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " limit:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 545
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    .line 546
    iput p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestLimit:I

    goto :goto_0

    .line 550
    :cond_1
    iput v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestOffset:I

    .line 551
    iput v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->nextRequestLimit:I

    .line 553
    iput-boolean v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isRequestingMusicCollection:Z

    .line 554
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicOffset:I

    .line 555
    if-ne p2, v5, :cond_2

    .line 556
    const/16 p2, 0x19

    .line 558
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "request music ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " limit:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 559
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    if-nez v2, :cond_3

    .line 561
    const/16 p2, 0x32

    .line 564
    :cond_3
    new-instance v2, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 565
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 566
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    .line 567
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    move-result-object v2

    .line 568
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->offset(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    move-result-object v2

    .line 569
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    move-result-object v0

    .line 571
    .local v0, "builder":Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v2, v3, :cond_4

    .line 572
    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->groupBy(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    .line 575
    :cond_4
    if-eqz p3, :cond_5

    .line 576
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->includeCharacterMap(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;

    .line 578
    :cond_5
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-result-object v1

    .line 579
    .local v1, "musicCollectionRequest":Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->currentMusicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    .line 580
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->currentMusicCollectionRequestTime:J

    .line 581
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got-asked offset="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " limit="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 582
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Timing] MusicCollectionRequest "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 583
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 584
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;

    invoke-direct {v3, p0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)V

    invoke-virtual {v2, v3, v6}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 599
    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v3, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private setupIconBgColor()V
    .locals 2

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-eqz v0, :cond_0

    .line 1406
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-ne v0, v1, :cond_1

    .line 1407
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->androidBgColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    .line 1414
    :cond_0
    :goto_0
    return-void

    .line 1408
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v0, v1, :cond_2

    .line 1409
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->applePodcastsBgColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    goto :goto_0

    .line 1411
    :cond_2
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->appleMusicBgColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    goto :goto_0
.end method

.method private setupMenu()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 484
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    if-nez v6, :cond_0

    .line 485
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "bus-register ctor"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 486
    new-instance v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v6, v7}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)V

    sput-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    .line 487
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 488
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v6, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setScrollIdleEvents(Z)V

    .line 490
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/manager/MusicManager;->addMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 492
    :cond_0
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->initialized:Z

    if-eqz v6, :cond_1

    .line 535
    :goto_0
    return-void

    .line 495
    :cond_1
    iput-boolean v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->initialized:Z

    .line 497
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/MusicManager;->hasMusicCapabilities()Z

    move-result v6

    if-nez v6, :cond_2

    .line 498
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Navigated to Music menu with no music capabilities"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 501
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-nez v6, :cond_6

    .line 502
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/MusicManager;->getMusicCapabilities()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    move-result-object v6

    iget-object v0, v6, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;->capabilities:Ljava/util/List;

    .line 503
    .local v0, "capabilities":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCapability;>;"
    iput-boolean v10, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->anyPlayersPermitted:Z

    .line 504
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 505
    .local v5, "musicCollectionSources":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionSource;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/audio/MusicCapability;

    .line 506
    .local v4, "musicCapability":Lcom/navdy/service/library/events/audio/MusicCapability;
    iget-object v7, v4, Lcom/navdy/service/library/events/audio/MusicCapability;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    sget-object v8, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_AUTHORIZED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    if-ne v7, v8, :cond_3

    .line 507
    iput-boolean v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->anyPlayersPermitted:Z

    .line 508
    iget-object v7, v4, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 511
    .end local v4    # "musicCapability":Lcom/navdy/service/library/events/audio/MusicCapability;
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v9, :cond_5

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/service/library/events/audio/MusicCapability;

    iget-object v6, v6, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sget-object v7, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-ne v6, v7, :cond_5

    .line 512
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    new-instance v7, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    sget-object v8, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 513
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v7

    .line 514
    invoke-virtual {v7}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v7

    iput-object v7, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 515
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    sget-object v7, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/manager/MusicManager;->getCollectionTypesForSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Ljava/util/List;

    move-result-object v3

    .line 516
    .local v3, "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v7, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    .line 517
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 518
    .local v2, "collectionType":Lcom/navdy/service/library/events/audio/MusicCollectionType;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v7, v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    new-instance v8, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v8}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    sget-object v9, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 519
    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v8

    .line 520
    invoke-virtual {v8, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v8

    .line 521
    invoke-virtual {v8}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v8

    .line 518
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 524
    .end local v2    # "collectionType":Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .end local v3    # "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    new-instance v7, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    .line 525
    invoke-virtual {v7}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v7

    iput-object v7, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 526
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v7, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    .line 527
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 528
    .local v1, "collectionSource":Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v7, v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    new-instance v8, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    invoke-direct {v8}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;-><init>()V

    .line 529
    invoke-virtual {v8, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;

    move-result-object v8

    .line 530
    invoke-virtual {v8}, Lcom/navdy/service/library/events/audio/MusicCollectionInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-result-object v8

    .line 528
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 534
    .end local v0    # "capabilities":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCapability;>;"
    .end local v1    # "collectionSource":Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .end local v5    # "musicCollectionSources":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionSource;>;"
    :cond_6
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->setupIconBgColor()V

    goto/16 :goto_0
.end method

.method private showMusicPlayer()V
    .locals 5

    .prologue
    .line 765
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "showMusicPlayer"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 767
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 768
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    .line 769
    .local v2, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$4;

    invoke-direct {v1, p0, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$4;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/hud/app/ui/framework/UIStateManager;)V

    .line 787
    .local v1, "screenAnimationListener":Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;
    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addScreenAnimationListener(Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;)V

    .line 789
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close()V

    .line 790
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->clearMenuData()V

    .line 791
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->storeMenuData()V

    .line 792
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/manager/MusicManager;->setMusicMenuPath(Ljava/lang/String;)V

    .line 793
    return-void
.end method

.method private startAudioAnimation(Lcom/navdy/hud/app/ui/component/image/IconColorImageView;)V
    .locals 3
    .param p1, "imageView"    # Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .prologue
    .line 1417
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1418
    const v1, 0x7f02000e

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setImageResource(I)V

    .line 1419
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 1420
    .local v0, "frameAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 1421
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 1422
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startAudioAnimation"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1423
    return-void
.end method

.method private stopAudioAnimation()V
    .locals 2

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1427
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 1428
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopAudioAnimation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1430
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 1431
    return-void
.end method

.method private storeMenuData()V
    .locals 3

    .prologue
    .line 1398
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_0

    .line 1399
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->storeMenuData()V

    .line 1401
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->lastPlayedMusicMenuData:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1402
    return-void
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 11
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 918
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getChildMenu:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 920
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/MusicManager;->hasMusicCapabilities()Z

    move-result v6

    if-nez v6, :cond_1

    .line 921
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "No music capabilities..."

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 922
    const/4 v4, 0x0

    .line 949
    :cond_0
    :goto_0
    return-object v4

    .line 924
    :cond_1
    const-string v6, ":"

    invoke-virtual {p2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 925
    .local v0, "backData":[Ljava/lang/String;
    aget-object v6, v0, v9

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 926
    .local v2, "id":I
    aget-object v6, v0, v10

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 927
    .local v5, "pos":I
    invoke-direct {p0, v2, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getMusicPlayerMenu(II)Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    move-result-object v4

    .line 930
    .local v4, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    const/4 v1, 0x0

    .line 931
    .local v1, "element":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 932
    const-string v6, "/"

    invoke-virtual {p3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 933
    .local v3, "index":I
    if-nez v3, :cond_4

    .line 934
    invoke-virtual {p3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 935
    const-string v6, "/"

    invoke-virtual {v1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 936
    if-ltz v3, :cond_3

    .line 937
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 938
    invoke-virtual {v1, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 946
    .end local v3    # "index":I
    :cond_2
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 947
    invoke-virtual {v4, p0, v1, p3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v4

    goto :goto_0

    .line 940
    .restart local v3    # "index":I
    :cond_3
    const/4 p3, 0x0

    goto :goto_1

    .line 943
    :cond_4
    const/4 p3, 0x0

    goto :goto_1
.end method

.method public getInitialSelection()I
    .locals 3

    .prologue
    .line 628
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getInitialSelection "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->highlightedItem:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 629
    iget v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->highlightedItem:I

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getItems "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 306
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->setupMenu()V

    .line 308
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 309
    .local v24, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/MusicManager;->hasMusicCapabilities()Z

    move-result v2

    if-nez v2, :cond_0

    .line 313
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Navigated to Music menu with no music capabilities"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 314
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 463
    :goto_0
    return-object v24

    .line 318
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->anyPlayersPermitted:Z

    if-nez v2, :cond_2

    .line 321
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v2, v3, :cond_1

    .line 322
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 323
    .local v7, "title":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 328
    .local v8, "subtitle":Ljava/lang/String;
    :goto_1
    const v2, 0x7f0e0047

    const v3, 0x7f02016e

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backColor:I

    sget v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    sget v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backColor:I

    invoke-static/range {v2 .. v8}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v23

    .line 329
    .local v23, "emptyBack":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v23

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 325
    .end local v7    # "title":Ljava/lang/String;
    .end local v8    # "subtitle":Ljava/lang/String;
    .end local v23    # "emptyBack":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 326
    .restart local v7    # "title":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "subtitle":Ljava/lang/String;
    goto :goto_1

    .line 334
    .end local v7    # "title":Ljava/lang/String;
    .end local v8    # "subtitle":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_3

    .line 335
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0900e9

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v11, v11, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v11, v11, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v6, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 336
    .restart local v8    # "subtitle":Ljava/lang/String;
    const v9, 0x7f0e0047

    const v10, 0x7f02016e

    sget v11, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backColor:I

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    sget v13, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backColor:I

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09002d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object v15, v8

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v23

    .line 337
    .restart local v23    # "emptyBack":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v23

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 343
    .end local v8    # "subtitle":Ljava/lang/String;
    .end local v23    # "emptyBack":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-nez v2, :cond_6

    .line 345
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v2, v3, :cond_4

    .line 346
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "bus-register getItems"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 347
    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)V

    sput-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 349
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setScrollIdleEvents(Z)V

    .line 353
    :cond_4
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_9

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 355
    .local v17, "collection":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceIconMap:Ljava/util/Map;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    sget v11, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->sourceBgColor:I

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    sget v13, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->sourceBgColor:I

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceStringMap:Ljava/util/Map;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v25

    .line 356
    .local v25, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v17

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 357
    invoke-interface/range {v24 .. v25}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v25

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/manager/MusicManager;->getCollectionTypesForSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Ljava/util/List;

    move-result-object v21

    .line 362
    .local v21, "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-ne v2, v3, :cond_5

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 364
    const v10, 0x7f0e0008

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->applePodcastsBgColor:I

    sget v13, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    sget v14, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->applePodcastsBgColor:I

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-static/range {v10 .. v16}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v27

    .line 365
    .local v27, "podcastsModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 366
    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v27

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    .end local v27    # "podcastsModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 371
    .end local v9    # "i":I
    .end local v17    # "collection":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .end local v21    # "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    .end local v25    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-nez v2, :cond_8

    .line 373
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_9

    .line 374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v0, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v20, v0

    .line 375
    .local v20, "collectionType":Lcom/navdy/service/library/events/audio/MusicCollectionType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-ne v2, v3, :cond_7

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    move-object/from16 v0, v20

    if-ne v0, v2, :cond_7

    .line 373
    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 379
    :cond_7
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v25

    .line 380
    .restart local v25    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 381
    invoke-interface/range {v24 .. v25}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v25

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 385
    .end local v9    # "i":I
    .end local v20    # "collectionType":Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .end local v25    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    if-nez v2, :cond_a

    .line 386
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->buildModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    const/4 v2, 0x0

    const/4 v3, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isIndexSupported(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Z

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicCollection(IIZ)V

    .line 462
    :cond_9
    :goto_5
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    goto/16 :goto_0

    .line 388
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_10

    .line 389
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loading music collections for type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelection:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 390
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicIndex:Ljava/util/List;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    if-nez v2, :cond_b

    .line 391
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicIndex:Ljava/util/List;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->checkCharacterMap(Ljava/util/List;IIZ)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    .line 393
    :cond_b
    const/16 v18, 0x0

    .line 394
    .local v18, "collectionIcon":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v2, :cond_c

    .line 395
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "collectionIcon":Ljava/lang/Integer;
    check-cast v18, Ljava/lang/Integer;

    .line 397
    .restart local v18    # "collectionIcon":Ljava/lang/Integer;
    :cond_c
    if-nez v18, :cond_d

    .line 398
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    .line 400
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v2, v3, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->canShuffle:Ljava/lang/Boolean;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 401
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getShuffleModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v28

    .line 402
    .local v28, "shuffleModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 403
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->highlightedItem:I

    .line 405
    .end local v28    # "shuffleModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_e
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_9

    .line 406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 407
    .local v19, "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    if-nez v19, :cond_f

    .line 409
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->buildModel(III)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v22

    .line 410
    .local v22, "contentPlaceHolder":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 411
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v22

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    .end local v22    # "contentPlaceHolder":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_7
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 414
    :cond_f
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v9, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->buildModelfromCollectionInfo(ILcom/navdy/service/library/events/audio/MusicCollectionInfo;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v25

    .line 415
    .restart local v25    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-interface/range {v24 .. v25}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v25

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 418
    .end local v9    # "i":I
    .end local v18    # "collectionIcon":Ljava/lang/Integer;
    .end local v19    # "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .end local v25    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_16

    .line 419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->trackIdToPositionMap:Ljava/util/Map;

    if-nez v2, :cond_11

    .line 420
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->trackIdToPositionMap:Ljava/util/Map;

    .line 424
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v2, v3, :cond_13

    .line 425
    const v10, 0x7f0201c2

    .line 438
    .local v10, "icon":I
    :cond_12
    :goto_8
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_15

    .line 439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 440
    .local v26, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    if-nez v26, :cond_14

    .line 442
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    invoke-static {v10, v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->buildModel(III)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v22

    .line 443
    .restart local v22    # "contentPlaceHolder":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v22

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 438
    .end local v22    # "contentPlaceHolder":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_a
    add-int/lit8 v9, v9, 0x1

    goto :goto_9

    .line 427
    .end local v9    # "i":I
    .end local v10    # "icon":I
    .end local v26    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_13
    const v10, 0x7f0201ee

    .line 428
    .restart local v10    # "icon":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_12

    .line 429
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getShuffleModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v28

    .line 430
    .restart local v28    # "shuffleModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eq v2, v3, :cond_12

    .line 433
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->highlightedItem:I

    goto :goto_8

    .line 447
    .end local v28    # "shuffleModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .restart local v9    # "i":I
    .restart local v26    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_14
    move-object/from16 v0, p0

    iget v11, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    sget v12, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorUnselected:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    move-object/from16 v0, v26

    iget-object v14, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v26

    iget-object v15, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-static/range {v9 .. v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v25

    .line 448
    .restart local v25    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 449
    invoke-interface/range {v24 .. v25}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    move-object/from16 v0, v25

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 451
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->trackIdToPositionMap:Ljava/util/Map;

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 453
    .end local v25    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v26    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->size:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 454
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->buildModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 457
    .end local v9    # "i":I
    .end local v10    # "icon":I
    :cond_16
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "No collections or tracks in this collection..."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 807
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 808
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 810
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1379
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->path:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1380
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_1

    .line 1381
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelectionId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelection:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->path:Ljava/lang/String;

    .line 1386
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->path:Ljava/lang/String;

    return-object v0

    .line 1383
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->path:Ljava/lang/String;

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 797
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 816
    const/4 v0, 0x1

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 1071
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 802
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 11
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 837
    iget-boolean v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->partialRefresh:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v7, v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v7

    if-ne p3, v7, :cond_0

    .line 838
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->partialRefresh:Z

    .line 839
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "partial refresh:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 840
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v7, v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v9, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    const/4 v10, 0x0

    invoke-virtual {v7, p3, v8, v9, v10}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setViewHolderState(ILcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 842
    :cond_0
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onBindToView:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " current="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v9, v9, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v9}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 843
    iget-object v7, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    instance-of v7, v7, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v7, :cond_4

    .line 844
    invoke-static {p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->findImageView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 845
    .local v3, "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    invoke-static {p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->findSmallImageView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 847
    .local v6, "smallImageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v4, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 848
    .local v4, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentTrack()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v5

    .line 849
    .local v5, "nowPlayingTrack":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    if-eqz v5, :cond_3

    iget-object v7, v5, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    iget-object v8, v4, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 850
    const/4 v7, 0x0

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    .line 851
    const/4 v7, 0x0

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    .line 852
    iget-object v7, v5, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-static {v7}, Lcom/navdy/hud/app/manager/MusicManager;->tryingToPlay(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 853
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->startAudioAnimation(Lcom/navdy/hud/app/ui/component/image/IconColorImageView;)V

    .line 858
    :goto_0
    const v7, 0x7f02003d

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setImageResource(I)V

    .line 914
    .end local v3    # "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .end local v4    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .end local v5    # "nowPlayingTrack":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .end local v6    # "smallImageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    :cond_1
    :goto_1
    return-void

    .line 855
    .restart local v3    # "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .restart local v4    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .restart local v5    # "nowPlayingTrack":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .restart local v6    # "smallImageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    :cond_2
    const v7, 0x7f02003d

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setImageResource(I)V

    .line 856
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->stopAudioAnimation()V

    goto :goto_0

    .line 860
    :cond_3
    const/4 v7, 0x1

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    .line 861
    const/4 v7, 0x1

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    goto :goto_1

    .line 863
    .end local v3    # "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .end local v4    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .end local v5    # "nowPlayingTrack":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .end local v6    # "smallImageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    :cond_4
    iget-object v7, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    instance-of v7, v7, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-eqz v7, :cond_7

    .line 864
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 865
    .local v1, "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    iget-object v7, v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 869
    invoke-static {p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->findImageView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 870
    .restart local v3    # "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    invoke-static {p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->findSmallImageView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 872
    .restart local v6    # "smallImageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setTag(Ljava/lang/Object;)V

    .line 874
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;

    move-result-object v2

    .line 875
    .local v2, "idString":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 876
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_5

    .line 877
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 878
    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 880
    const/4 v7, 0x0

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    .line 881
    const/4 v7, 0x0

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    goto :goto_1

    .line 883
    :cond_5
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 884
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 885
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestedArtworkModelPositions:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 886
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Already requested artwork for collection: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 889
    :cond_6
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestedArtworkModelPositions:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 890
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Checking cache for artwork for collection: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 891
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicArtworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

    new-instance v8, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;

    invoke-direct {v8, p0, p3, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;ILcom/navdy/service/library/events/audio/MusicCollectionInfo;)V

    invoke-virtual {v7, v1, v8}, Lcom/navdy/hud/app/util/MusicArtworkCache;->getArtwork(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V

    goto/16 :goto_1

    .line 911
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    .end local v2    # "idString":Ljava/lang/String;
    .end local v3    # "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .end local v6    # "smallImageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    :cond_7
    const/4 v7, 0x1

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    .line 912
    const/4 v7, 0x1

    iput-boolean v7, p4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    goto/16 :goto_1
.end method

.method public onFastScrollEnd()V
    .locals 2

    .prologue
    .line 1062
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "REQUESTQUEUE onFastScrollEnd"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1063
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->purgeArtworkQueue()V

    .line 1064
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 1058
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 1000
    return-void
.end method

.method public onScrollIdle()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1004
    iget-boolean v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicCollectionSyncComplete:Z

    if-eqz v8, :cond_1

    .line 1055
    :cond_0
    :goto_0
    return-void

    .line 1008
    :cond_1
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_5

    move v1, v6

    .line 1009
    .local v1, "hasCollectionInfo":Z
    :goto_1
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_6

    move v2, v6

    .line 1011
    .local v2, "hasTrackInfo":Z
    :goto_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v6, v6, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    if-eqz v6, :cond_0

    if-nez v1, :cond_2

    if-eqz v2, :cond_0

    .line 1014
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v0

    .line 1016
    .local v0, "currentPos":I
    add-int/lit8 v0, v0, -0x1

    .line 1017
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->hasShuffleEntry()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1019
    add-int/lit8 v0, v0, -0x1

    .line 1021
    :cond_3
    if-gez v0, :cond_4

    .line 1022
    const/4 v0, 0x0

    .line 1024
    :cond_4
    if-eqz v1, :cond_7

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    .line 1025
    .local v5, "size":I
    :goto_3
    if-lt v0, v5, :cond_8

    .line 1026
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onScrollIdle pos("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") >= size("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v0    # "currentPos":I
    .end local v1    # "hasCollectionInfo":Z
    .end local v2    # "hasTrackInfo":Z
    .end local v5    # "size":I
    :cond_5
    move v1, v7

    .line 1008
    goto :goto_1

    .restart local v1    # "hasCollectionInfo":Z
    :cond_6
    move v2, v7

    .line 1009
    goto :goto_2

    .line 1024
    .restart local v0    # "currentPos":I
    .restart local v2    # "hasTrackInfo":Z
    :cond_7
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_3

    .line 1029
    .restart local v5    # "size":I
    :cond_8
    if-eqz v1, :cond_c

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/wire/Message;

    move-object v4, v6

    .line 1030
    .local v4, "obj":Lcom/squareup/wire/Message;
    :goto_4
    if-eqz v4, :cond_b

    .line 1031
    add-int/lit8 v3, v0, -0x1

    .line 1032
    .local v3, "newPos":I
    if-ltz v3, :cond_9

    .line 1033
    if-eqz v1, :cond_d

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/wire/Message;

    move-object v4, v6

    .line 1034
    :goto_5
    if-nez v4, :cond_9

    .line 1035
    add-int/lit8 v0, v0, -0x1

    .line 1038
    :cond_9
    if-eqz v4, :cond_b

    .line 1039
    add-int/lit8 v3, v0, 0x1

    .line 1040
    add-int/lit8 v6, v5, -0x1

    if-gt v3, v6, :cond_a

    .line 1041
    if-eqz v1, :cond_e

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollections:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/wire/Message;

    move-object v4, v6

    .line 1043
    :cond_a
    :goto_6
    if-nez v4, :cond_b

    .line 1044
    add-int/lit8 v0, v0, 0x1

    .line 1048
    .end local v3    # "newPos":I
    :cond_b
    if-nez v4, :cond_0

    .line 1050
    const/16 v6, 0x19

    invoke-direct {p0, v0, v6, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->calculateOffset(IIZ)V

    .line 1051
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onScrollIdle newOffset:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    iget v9, v9, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;->offset:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " count="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    iget v9, v9, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;->limit:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1052
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    iget v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;->offset:I

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->loadingOffset:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;

    iget v8, v8, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$IndexOffset;->limit:I

    invoke-direct {p0, v6, v8, v7}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicCollection(IIZ)V

    goto/16 :goto_0

    .line 1029
    .end local v4    # "obj":Lcom/squareup/wire/Message;
    :cond_c
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/wire/Message;

    move-object v4, v6

    goto :goto_4

    .line 1033
    .restart local v3    # "newPos":I
    .restart local v4    # "obj":Lcom/squareup/wire/Message;
    :cond_d
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/wire/Message;

    move-object v4, v6

    goto :goto_5

    .line 1041
    :cond_e
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/wire/Message;

    move-object v4, v6

    goto :goto_6
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 4
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 955
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$8;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 997
    :cond_0
    :goto_0
    return-void

    .line 957
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_1

    .line 958
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 959
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setScrollIdleEvents(Z)V

    .line 960
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->purgeArtworkQueue()V

    .line 961
    sput-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 962
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isRequestingMusicCollection:Z

    .line 963
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->removeMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 964
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->stopAudioAnimation()V

    .line 965
    sput-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    .line 966
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onUnload-back bus-unregister"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 968
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 969
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onUnload add to cache"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 970
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->addToCache(Ljava/util/List;)V

    .line 971
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    goto :goto_0

    .line 976
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    if-eqz v0, :cond_2

    .line 977
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 978
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->setScrollIdleEvents(Z)V

    .line 979
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->purgeArtworkQueue()V

    .line 980
    sput-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->infoForCurrentArtworkRequest:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 981
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isRequestingMusicCollection:Z

    .line 982
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->removeMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 983
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->stopAudioAnimation()V

    .line 984
    sput-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->busDelegate:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;

    .line 985
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onUnload-close bus-unregister"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 987
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 988
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onUnload add to cache"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 989
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->addToCache(Ljava/util/List;)V

    .line 990
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->returnToCacheList:Ljava/util/List;

    .line 992
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_0

    .line 993
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    goto/16 :goto_0

    .line 955
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 10
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v9, 0x0

    .line 698
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onItemClicked "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 699
    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    const v5, 0x7f0e0047

    if-ne v4, v5, :cond_0

    .line 700
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelection:I

    iget v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelectionId:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    .line 701
    iput v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelection:I

    .line 702
    iput v9, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelectionId:I

    .line 761
    :goto_0
    const/4 v4, 0x1

    return v4

    .line 704
    :cond_0
    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    const v5, 0x7f0e0056

    if-ne v4, v5, :cond_3

    .line 705
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->isThisQueuePlaying()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 707
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/MusicManager;->isShuffling()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 709
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 714
    .local v3, "shuffleMode":Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    :goto_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v6, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_MODE_CHANGE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 715
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    .line 716
    invoke-virtual {v6, v3}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    .line 717
    invoke-virtual {v6}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->build()Lcom/navdy/service/library/events/audio/MusicEvent;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 714
    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 719
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handler:Landroid/os/Handler;

    new-instance v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$3;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$3;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 712
    .end local v3    # "shuffleMode":Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    :cond_1
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_SONGS:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .restart local v3    # "shuffleMode":Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    goto :goto_1

    .line 727
    .end local v3    # "shuffleMode":Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 728
    .local v0, "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v6, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;-><init>()V

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 729
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 730
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    .line 731
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 732
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_SONGS:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 733
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    .line 734
    invoke-virtual {v6}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->build()Lcom/navdy/service/library/events/audio/MusicEvent;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 728
    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 736
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->showMusicPlayer()V

    goto :goto_0

    .line 738
    .end local v0    # "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicTracks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 739
    const/4 v2, 0x0

    .line 740
    .local v2, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    if-eqz v4, :cond_4

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    if-ltz v4, :cond_4

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 741
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->cachedList:Ljava/util/List;

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    check-cast v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 743
    .restart local v2    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_4
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onItemClicked track:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v2, :cond_5

    iget-object v4, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 744
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 745
    .restart local v0    # "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v6, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;-><init>()V

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 746
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 747
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    .line 748
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    iget v7, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    .line 749
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->index(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 750
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    sget-object v7, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 751
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v6

    .line 752
    invoke-virtual {v6}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->build()Lcom/navdy/service/library/events/audio/MusicEvent;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 745
    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 754
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->showMusicPlayer()V

    .line 755
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->analyticsTypeStringMap:Ljava/util/Map;

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMusicBrowsePlayAction(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 743
    .end local v0    # "collectionInfo":Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
    :cond_5
    const-string v4, "null"

    goto :goto_2

    .line 757
    .end local v2    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_6
    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->getMusicPlayerMenu(II)Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    move-result-object v1

    .line 758
    .local v1, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v4, v1, v5, v6, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0
.end method

.method public setBackSelectionId(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 644
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBackSelectionId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 645
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelectionId:I

    .line 646
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 639
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->backSelection:I

    .line 640
    return-void
.end method

.method public setSelectedIcon()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 650
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setSelectedIcon"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 651
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->setupMenu()V

    .line 653
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->parentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v0, v1, :cond_0

    .line 654
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f020180

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->mmMusicColor:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 655
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    const v1, 0x7f0901c3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 694
    :goto_0
    return-void

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-nez v0, :cond_1

    .line 658
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceIconMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->sourceBgColor:I

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 659
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuSourceStringMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 661
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 662
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 663
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeStringMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 665
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;

    move-result-object v7

    .line 666
    .local v7, "idString":Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 667
    .local v6, "artwork":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_3

    .line 668
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(Landroid/graphics/Bitmap;)V

    .line 690
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 670
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-ne v0, v1, :cond_4

    sget-object v5, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 671
    .local v5, "iconShape":Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
    :goto_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuTypeIconMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bgColorSelected:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;FLcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V

    .line 672
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicArtworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->menuData:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;->musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$2;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/util/MusicArtworkCache;->getArtwork(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V

    goto :goto_1

    .line 670
    .end local v5    # "iconShape":Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
    :cond_4
    sget-object v5, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->SQUARE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    goto :goto_2
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 1067
    return-void
.end method
