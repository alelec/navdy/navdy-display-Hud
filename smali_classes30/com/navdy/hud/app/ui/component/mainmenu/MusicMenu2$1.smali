.class Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;
.super Ljava/lang/Object;
.source "MusicMenu2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->requestMusicCollection(IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

.field final synthetic val$musicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 584
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->val$musicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 587
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->val$musicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    invoke-static {v2}, Lcom/navdy/hud/app/ExtensionsKt;->cacheKey(Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)Ljava/lang/String;

    move-result-object v0

    .line 588
    .local v0, "cacheKeyForRequest":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->musicCollectionResponseMessageCache:Lcom/navdy/hud/app/storage/cache/MessageCache;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/storage/cache/MessageCache;->get(Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    .line 589
    .local v1, "response":Lcom/navdy/service/library/events/audio/MusicCollectionResponse;
    if-eqz v1, :cond_0

    .line 590
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cache hit for MusicCollectionRequest "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->val$musicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Key :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 591
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$500(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/squareup/otto/Bus;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 596
    :goto_0
    return-void

    .line 593
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cache miss for MusicCollectionRequest "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->val$musicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Key :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 594
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$500(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;)Lcom/squareup/otto/Bus;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$1;->val$musicCollectionRequest:Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
