.class public Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;
.super Ljava/lang/Object;
.source "SystemInfoMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final UPDATE_FREQUENCY:I = 0x7d0

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final dateFormat:Ljava/text/SimpleDateFormat;

.field private static fuelUnitText:Ljava/lang/String;

.field private static final infoModel:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final infoTitle:Ljava/lang/String;

.field private static final noData:Ljava/lang/String;

.field private static final noFixStr:Ljava/lang/String;

.field private static final off:Ljava/lang/String;

.field private static final on:Ljava/lang/String;

.field private static pressureUnitText:Ljava/lang/String;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected autoOn:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01e6
    .end annotation
.end field

.field private backSelection:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field protected carModel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f9
    .end annotation
.end field

.field protected checkEngineLight:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0204
    .end annotation
.end field

.field protected deviceMileage:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f6
    .end annotation
.end field

.field protected dialBt:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e020e
    .end annotation
.end field

.field protected dialFw:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e020c
    .end annotation
.end field

.field protected displayBt:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e020f
    .end annotation
.end field

.field protected displayFw:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e020b
    .end annotation
.end field

.field protected engineOilPressure:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0208
    .end annotation
.end field

.field protected engineOilPressureLayout:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0207
    .end annotation
.end field

.field protected engineTemp:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0202
    .end annotation
.end field

.field protected engineTroubleCodes:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0205
    .end annotation
.end field

.field protected fix:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f4
    .end annotation
.end field

.field protected fuel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01fc
    .end annotation
.end field

.field protected gestures:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01e5
    .end annotation
.end field

.field protected gps:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f1
    .end annotation
.end field

.field protected gpsSpeed:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f7
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

.field protected hereMapVerified:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ed
    .end annotation
.end field

.field protected hereMapsVersion:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01eb
    .end annotation
.end field

.field protected hereOfflineMaps:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ec
    .end annotation
.end field

.field protected hereSdk:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01e9
    .end annotation
.end field

.field protected hereUpdated:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ea
    .end annotation
.end field

.field protected hereVoiceVerified:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ee
    .end annotation
.end field

.field protected maf:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0201
    .end annotation
.end field

.field protected make:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f8
    .end annotation
.end field

.field protected obdData:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01e7
    .end annotation
.end field

.field protected obdDataLayout:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01fb
    .end annotation
.end field

.field protected obdFw:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e020d
    .end annotation
.end field

.field private obdManager:Lcom/navdy/hud/app/obd/ObdManager;

.field protected odo:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0200
    .end annotation
.end field

.field protected odometerLayout:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ff
    .end annotation
.end field

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field protected registered:Z

.field protected routeCalc:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ef
    .end annotation
.end field

.field protected routePref:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f0
    .end annotation
.end field

.field protected rpm:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01fd
    .end annotation
.end field

.field satelliteView:Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f3
    .end annotation
.end field

.field protected satellites:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f2
    .end annotation
.end field

.field protected specialPidsLayout:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0206
    .end annotation
.end field

.field protected speed:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01fe
    .end annotation
.end field

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field protected temperature:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01f5
    .end annotation
.end field

.field protected tripFuel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e020a
    .end annotation
.end field

.field protected tripFuelLayout:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0209
    .end annotation
.end field

.field protected ubloxFixType:Ljava/lang/String;

.field protected uiScaling:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01e8
    .end annotation
.end field

.field private updateRunnable:Ljava/lang/Runnable;

.field protected voltage:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0203
    .end annotation
.end field

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

.field protected year:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01fa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 70
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 92
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM/dd/yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 93
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    .line 94
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090128

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noFixStr:Ljava/lang/String;

    .line 95
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09035f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    .line 96
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090366

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->on:Ljava/lang/String;

    .line 97
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090364

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->off:Ljava/lang/String;

    .line 98
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 99
    .local v2, "backColor":I
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09007e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->infoTitle:Ljava/lang/String;

    .line 101
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090339

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->pressureUnitText:Ljava/lang/String;

    .line 102
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090337

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fuelUnitText:Ljava/lang/String;

    .line 105
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 106
    .local v5, "title":Ljava/lang/String;
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/4 v6, 0x0

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 109
    const v0, 0x7f030073

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ScrollableViewHolder;->buildModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->infoModel:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 110
    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateRunnable:Ljava/lang/Runnable;

    .line 256
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->bus:Lcom/squareup/otto/Bus;

    .line 257
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 258
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 259
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 260
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->handler:Landroid/os/Handler;

    .line 261
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    .line 262
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 263
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateTemperature()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateVoltage()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->setFix()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private formatRoutePref(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 3
    .param p1, "stringBuilder"    # Ljava/lang/StringBuilder;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 893
    const/4 v0, 0x0

    .line 894
    .local v0, "uppercase":Z
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 895
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 896
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 900
    :goto_0
    if-eqz v0, :cond_0

    .line 901
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 903
    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    return-void

    .line 898
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static getFixTypeDescription(I)Ljava/lang/String;
    .locals 1
    .param p0, "fixType"    # I

    .prologue
    .line 949
    packed-switch p0, :pswitch_data_0

    .line 963
    :pswitch_0
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 951
    :pswitch_1
    const-string v0, "No Fix"

    goto :goto_0

    .line 953
    :pswitch_2
    const-string v0, "Standard 2D/3D"

    goto :goto_0

    .line 955
    :pswitch_3
    const-string v0, "Differential"

    goto :goto_0

    .line 957
    :pswitch_4
    const-string v0, "RTK Fixed"

    goto :goto_0

    .line 959
    :pswitch_5
    const-string v0, "RTK Float"

    goto :goto_0

    .line 961
    :pswitch_6
    const-string v0, "DR Fix"

    goto :goto_0

    .line 949
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private isMenuLoaded()Z
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialFw:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeBrackets(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 451
    const-string v0, "("

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 452
    const-string v0, ")"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 453
    return-object p1
.end method

.method private setFix()V
    .locals 5

    .prologue
    .line 1076
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    if-nez v2, :cond_0

    .line 1093
    :goto_0
    return-void

    .line 1079
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1080
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLocationFixEvent()Lcom/navdy/hud/app/maps/MapEvents$LocationFix;

    move-result-object v0

    .line 1081
    .local v0, "event":Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fix ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, v0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->locationAvailable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] phone["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, v0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingPhoneLocation:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] u-blox["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, v0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingLocalGpsLocation:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] type["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->ubloxFixType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1082
    iget-boolean v2, v0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->locationAvailable:Z

    if-nez v2, :cond_1

    .line 1083
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fix:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noFixStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1085
    :cond_1
    iget-boolean v2, v0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingPhoneLocation:Z

    if-eqz v2, :cond_2

    .line 1086
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fix:Landroid/widget/TextView;

    const v3, 0x7f090129

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1087
    :cond_2
    iget-boolean v2, v0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingLocalGpsLocation:Z

    if-eqz v2, :cond_4

    .line 1088
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fix:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->ubloxFixType:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->ubloxFixType:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    const v2, 0x7f09012f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1

    .line 1090
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fix:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noFixStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private update()V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateVersions()V

    .line 443
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateStats()V

    .line 444
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateHere()V

    .line 445
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateVehicleInfo()V

    .line 446
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateGenericPref()V

    .line 447
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateNavPref()V

    .line 448
    return-void
.end method

.method private updateDeviceMileage()V
    .locals 8

    .prologue
    .line 513
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    if-eqz v5, :cond_0

    .line 514
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v4, v5, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    .line 515
    .local v4, "tripManager":Lcom/navdy/hud/app/framework/trips/TripManager;
    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/trips/TripManager;->getTotalDistanceTravelledWithNavdy()J

    move-result-wide v0

    .line 516
    .local v0, "d":J
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "navdy distance travelled:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 517
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-lez v5, :cond_0

    .line 518
    new-instance v2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-direct {v2}, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;-><init>()V

    .line 519
    .local v2, "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v5

    long-to-float v6, v0

    invoke-static {v5, v6, v2}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 520
    iget v5, v2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    iget-object v6, v2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getFormattedDistance(FLcom/navdy/service/library/events/navigation/DistanceUnit;Z)Ljava/lang/String;

    move-result-object v3

    .line 521
    .local v3, "str":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->deviceMileage:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 524
    .end local v0    # "d":J
    .end local v2    # "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .end local v3    # "str":Ljava/lang/String;
    .end local v4    # "tripManager":Lcom/navdy/hud/app/framework/trips/TripManager;
    :cond_0
    return-void
.end method

.method private updateDynamicVehicleInfo()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 622
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v2

    .line 623
    .local v2, "obdConnected":Z
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager;->getConnectionType()Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    move-result-object v1

    .line 624
    .local v1, "connectionType":Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;
    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdDataLayout:Landroid/view/ViewGroup;

    sget-object v7, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->POWER_ONLY:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    if-ne v1, v7, :cond_1

    move v7, v8

    :goto_0
    invoke-virtual {v10, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 625
    if-eqz v2, :cond_7

    .line 627
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateFuel()V

    .line 630
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateRPM()V

    .line 633
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateSpeed()V

    .line 636
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateMAF()V

    .line 639
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateEngineTemperature()V

    .line 642
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateVoltage()V

    .line 646
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateOdometer()V

    .line 648
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v4

    .line 649
    .local v4, "supportedPids":Lcom/navdy/obd/PidSet;
    const/16 v7, 0x102

    invoke-virtual {v4, v7}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v7

    if-nez v7, :cond_0

    const/16 v7, 0x103

    invoke-virtual {v4, v7}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    const/4 v3, 0x1

    .line 650
    .local v3, "shouldShowSpecialPidsSection":Z
    :goto_1
    if-nez v3, :cond_3

    .line 651
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->specialPidsLayout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 658
    :goto_2
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->checkEngineLight:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager;->isCheckEngineLightOn()Z

    move-result v7

    if-eqz v7, :cond_4

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->on:Ljava/lang/String;

    :goto_3
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 659
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager;->getTroubleCodes()Ljava/util/List;

    move-result-object v6

    .line 660
    .local v6, "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_6

    .line 661
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 662
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 663
    .local v5, "troubleCode":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v3    # "shouldShowSpecialPidsSection":Z
    .end local v4    # "supportedPids":Lcom/navdy/obd/PidSet;
    .end local v5    # "troubleCode":Ljava/lang/String;
    .end local v6    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    move v7, v9

    .line 624
    goto/16 :goto_0

    .restart local v4    # "supportedPids":Lcom/navdy/obd/PidSet;
    :cond_2
    move v3, v9

    .line 649
    goto :goto_1

    .line 653
    .restart local v3    # "shouldShowSpecialPidsSection":Z
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->specialPidsLayout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 655
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateEngineFuelPressure()V

    .line 656
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateEngineTripFuel()V

    goto :goto_2

    .line 658
    :cond_4
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->off:Ljava/lang/String;

    goto :goto_3

    .line 666
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v6    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 667
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTroubleCodes:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 677
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v3    # "shouldShowSpecialPidsSection":Z
    .end local v4    # "supportedPids":Lcom/navdy/obd/PidSet;
    .end local v6    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_5
    return-void

    .line 669
    .restart local v3    # "shouldShowSpecialPidsSection":Z
    .restart local v4    # "supportedPids":Lcom/navdy/obd/PidSet;
    .restart local v6    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_6
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTroubleCodes:Landroid/widget/TextView;

    sget-object v8, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 672
    .end local v3    # "shouldShowSpecialPidsSection":Z
    .end local v4    # "supportedPids":Lcom/navdy/obd/PidSet;
    .end local v6    # "troubleCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_7
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->specialPidsLayout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 673
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odometerLayout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 674
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->checkEngineLight:Landroid/widget/TextView;

    sget-object v8, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 675
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTroubleCodes:Landroid/widget/TextView;

    sget-object v8, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method private updateEngineFuelPressure()V
    .locals 4

    .prologue
    .line 680
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    const/16 v2, 0x102

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v2

    double-to-int v0, v2

    .line 681
    .local v0, "val":I
    if-lez v0, :cond_0

    .line 682
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineOilPressure:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->pressureUnitText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 686
    :goto_0
    return-void

    .line 684
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineOilPressure:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateEngineTemperature()V
    .locals 11

    .prologue
    .line 755
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    .line 758
    .local v2, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v0

    .line 759
    .local v0, "celsius":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v0, v6

    if-lez v6, :cond_0

    .line 763
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$2;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$UnitSystem:[I

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 770
    const v5, 0x7f09034a

    .line 771
    .local v5, "unit":I
    const-wide v6, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v6, v0

    const-wide/high16 v8, 0x4040000000000000L    # 32.0

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v3, v6

    .line 774
    .local v3, "rounded":I
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xb0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 775
    .local v4, "str":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTemp:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 779
    .end local v3    # "rounded":I
    .end local v4    # "str":Ljava/lang/String;
    .end local v5    # "unit":I
    :goto_1
    return-void

    .line 765
    :pswitch_0
    const v5, 0x7f09033d

    .line 766
    .restart local v5    # "unit":I
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v3, v6

    .line 767
    .restart local v3    # "rounded":I
    goto :goto_0

    .line 777
    .end local v3    # "rounded":I
    .end local v5    # "unit":I
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTemp:Landroid/widget/TextView;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 763
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private updateEngineTripFuel()V
    .locals 4

    .prologue
    .line 689
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    const/16 v2, 0x103

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v2

    double-to-int v0, v2

    .line 690
    .local v0, "val":I
    if-lez v0, :cond_0

    .line 691
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->tripFuel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fuelUnitText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 695
    :goto_0
    return-void

    .line 693
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->tripFuel:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateFuel()V
    .locals 7

    .prologue
    .line 700
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v0

    .line 701
    .local v0, "val":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 702
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fuel:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09034f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 706
    :goto_0
    return-void

    .line 704
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fuel:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateGPSSpeed()V
    .locals 3

    .prologue
    .line 727
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getGpsSpeed()I

    move-result v0

    .line 728
    .local v0, "val":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 729
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gpsSpeed:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 733
    :goto_0
    return-void

    .line 731
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gpsSpeed:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateGenericPref()V
    .locals 9

    .prologue
    const v8, 0x7f090346

    const v7, 0x7f090343

    .line 801
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v3

    .line 803
    .local v3, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getInputPreferences()Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v2

    .line 804
    .local v2, "inputPref":Lcom/navdy/service/library/events/preferences/InputPreferences;
    if-eqz v2, :cond_0

    .line 805
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v6, v2, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    invoke-virtual {v5, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 806
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gestures:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 812
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/obd/ObdManager;->getObdDeviceConfigurationManager()Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;

    move-result-object v0

    .line 813
    .local v0, "configurationManager":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    if-eqz v0, :cond_1

    .line 814
    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->isAutoOnEnabled()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 815
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->autoOn:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 821
    :cond_1
    :goto_1
    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getObdScanSetting()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-result-object v4

    .line 822
    .local v4, "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    if-eqz v4, :cond_2

    .line 823
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$2;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting:[I

    invoke-virtual {v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 830
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdData:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f090364

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 835
    :cond_2
    :goto_2
    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getDisplayFormat()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    move-result-object v1

    .line 836
    .local v1, "displayFormat":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    if-eqz v1, :cond_3

    .line 837
    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$2;->$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$DisplayFormat:[I

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 847
    :cond_3
    :goto_3
    return-void

    .line 808
    .end local v0    # "configurationManager":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    .end local v1    # "displayFormat":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .end local v4    # "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :cond_4
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gestures:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 817
    .restart local v0    # "configurationManager":Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;
    :cond_5
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->autoOn:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 826
    .restart local v4    # "scanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    :pswitch_0
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdData:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f090366

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 839
    .restart local v1    # "displayFormat":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    :pswitch_1
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->uiScaling:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f09033f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 843
    :pswitch_2
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->uiScaling:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f090360

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 823
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 837
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateHere()V
    .locals 15

    .prologue
    const v14, 0x7f09037a

    const v13, 0x7f09035e

    .line 557
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v4

    .line 558
    .local v4, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitializing()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 563
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereSdk:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getVersion()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isVoiceSkinsLoaded()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 571
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereVoiceVerified:Landroid/widget/TextView;

    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v12, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 576
    :goto_1
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isMapDataVerified()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 577
    sget-object v11, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "map package installed count:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getMapPackageCount()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 578
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapVerified:Landroid/widget/TextView;

    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v12, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 583
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getOfflineMapsVersion()Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    move-result-object v9

    .line 584
    .local v9, "offlineMapsVersion":Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    if-eqz v9, :cond_0

    .line 588
    const/4 v7, 0x0

    .line 589
    .local v7, "mapPackages":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->getPackages()Ljava/util/List;

    move-result-object v10

    .line 590
    .local v10, "offlinePackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v10, :cond_7

    .line 591
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v6

    .line 592
    .local v6, "len":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 593
    .local v1, "builder":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    if-ge v5, v6, :cond_6

    .line 594
    if-eqz v5, :cond_2

    .line 595
    const-string v11, ","

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    const-string v11, " "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    :cond_2
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 565
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "i":I
    .end local v6    # "len":I
    .end local v7    # "mapPackages":Ljava/lang/String;
    .end local v9    # "offlineMapsVersion":Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    .end local v10    # "offlinePackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getError()Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v3

    .line 566
    .local v3, "error":Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereSdk:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->name()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 573
    .end local v3    # "error":Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    :cond_4
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereVoiceVerified:Landroid/widget/TextView;

    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 580
    :cond_5
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapVerified:Landroid/widget/TextView;

    sget-object v12, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 600
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    .restart local v5    # "i":I
    .restart local v6    # "len":I
    .restart local v7    # "mapPackages":Ljava/lang/String;
    .restart local v9    # "offlineMapsVersion":Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    .restart local v10    # "offlinePackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 603
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v5    # "i":I
    .end local v6    # "len":I
    :cond_7
    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->getDate()Ljava/util/Date;

    move-result-object v0

    .line 604
    .local v0, "buildDate":Ljava/util/Date;
    if-eqz v0, :cond_a

    sget-object v11, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->getDate()Ljava/util/Date;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 605
    .local v2, "dateUpdated":Ljava/lang/String;
    :goto_4
    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->getVersion()Ljava/lang/String;

    move-result-object v8

    .line 607
    .local v8, "mapVersion":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 608
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereUpdated:Landroid/widget/TextView;

    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 611
    :cond_8
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 612
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereOfflineMaps:Landroid/widget/TextView;

    invoke-virtual {v11, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    :cond_9
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 616
    iget-object v11, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapsVersion:Landroid/widget/TextView;

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 604
    .end local v2    # "dateUpdated":Ljava/lang/String;
    .end local v8    # "mapVersion":Ljava/lang/String;
    :cond_a
    const/4 v2, 0x0

    goto :goto_4
.end method

.method private updateMAF()V
    .locals 4

    .prologue
    .line 736
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getInstantFuelConsumption()D

    move-result-wide v2

    double-to-int v0, v2

    .line 737
    .local v0, "val":I
    if-lez v0, :cond_0

    .line 738
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->maf:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 742
    :goto_0
    return-void

    .line 740
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->maf:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateNavPref()V
    .locals 5

    .prologue
    .line 850
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    .line 851
    .local v0, "navPref":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    if-eqz v0, :cond_6

    .line 852
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$2;->$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType:[I

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 862
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 863
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 864
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090322

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    :cond_0
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 868
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090370

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->formatRoutePref(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 871
    :cond_1
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 872
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09034c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->formatRoutePref(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 875
    :cond_2
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 876
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090371

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->formatRoutePref(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 879
    :cond_3
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 880
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090373

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->formatRoutePref(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 883
    :cond_4
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 884
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09033c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->formatRoutePref(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 887
    :cond_5
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 888
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routePref:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 890
    .end local v1    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_6
    return-void

    .line 854
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routeCalc:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v4, 0x7f09034b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 858
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routeCalc:Landroid/widget/TextView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v4, 0x7f09036c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 852
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateOdometer()V
    .locals 7

    .prologue
    const/16 v5, 0x104

    const/4 v6, 0x0

    .line 782
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v2

    .line 783
    .local v2, "supportedPids":Lcom/navdy/obd/PidSet;
    invoke-virtual {v2, v5}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 784
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odometerLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 789
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v4

    double-to-int v3, v4

    .line 790
    .local v3, "val":I
    if-lez v3, :cond_1

    .line 791
    new-instance v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;-><init>()V

    .line 792
    .local v0, "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v4

    int-to-float v5, v3

    invoke-static {v4, v5, v0}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 793
    iget v4, v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    iget-object v5, v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-static {v4, v5, v6}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getFormattedDistance(FLcom/navdy/service/library/events/navigation/DistanceUnit;Z)Ljava/lang/String;

    move-result-object v1

    .line 794
    .local v1, "str":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odo:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 798
    .end local v0    # "distance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .end local v1    # "str":Ljava/lang/String;
    .end local v3    # "val":I
    :goto_0
    return-void

    .line 786
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odometerLayout:Landroid/view/ViewGroup;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 796
    .restart local v3    # "val":I
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odo:Landroid/widget/TextView;

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateRPM()V
    .locals 3

    .prologue
    .line 709
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getEngineRpm()I

    move-result v0

    .line 710
    .local v0, "val":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 711
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->rpm:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 715
    :goto_0
    return-void

    .line 713
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->rpm:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateSpeed()V
    .locals 3

    .prologue
    .line 718
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v0

    .line 719
    .local v0, "val":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 720
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speed:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 724
    :goto_0
    return-void

    .line 722
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speed:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateStats()V
    .locals 2

    .prologue
    .line 501
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateTemperature()V

    .line 502
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateDeviceMileage()V

    .line 503
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateGPSSpeed()V

    .line 504
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    .line 505
    .local v0, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 506
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .line 509
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->setFix()V

    .line 510
    return-void
.end method

.method private updateTemperature()V
    .locals 5

    .prologue
    .line 527
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->getCurrentTemperature()I

    move-result v1

    .line 528
    .local v1, "temp":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 529
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xb0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v4, 0x7f09033d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 530
    .local v0, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->temperature:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private updateVehicleInfo()V
    .locals 3

    .prologue
    .line 535
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 538
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarMake()Ljava/lang/String;

    move-result-object v1

    .line 539
    .local v1, "str":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 540
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->make:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 543
    :cond_0
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarModel()Ljava/lang/String;

    move-result-object v1

    .line 544
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 545
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->carModel:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getCarYear()Ljava/lang/String;

    move-result-object v1

    .line 549
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 550
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->year:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 553
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateDynamicVehicleInfo()V

    .line 554
    return-void
.end method

.method private updateVersions()V
    .locals 8

    .prologue
    .line 459
    const-string v5, "1.3.3052-ce5463a"

    .line 460
    .local v5, "versionName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 461
    invoke-static {v5}, Lcom/navdy/hud/app/util/OTAUpdateService;->shortVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 463
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayFw:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v1

    .line 466
    .local v1, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 468
    :try_start_0
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v0

    .line 469
    .local v0, "dialFirmwareUpdater":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    const/4 v3, 0x0

    .line 470
    .local v3, "str":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 471
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v6

    iget-object v6, v6, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v2, v6, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    .line 472
    .local v2, "incremental":I
    const/4 v6, -0x1

    if-eq v2, v6, :cond_1

    .line 473
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 475
    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 476
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialFw:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 483
    .end local v0    # "dialFirmwareUpdater":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    .end local v2    # "incremental":I
    .end local v3    # "str":Ljava/lang/String;
    :cond_2
    :goto_0
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialName()Ljava/lang/String;

    move-result-object v3

    .line 484
    .restart local v3    # "str":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 485
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialBt:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->removeBrackets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 489
    .end local v3    # "str":Ljava/lang/String;
    :cond_3
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v3

    .line 490
    .restart local v3    # "str":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 491
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayBt:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->removeBrackets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    :cond_4
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/obd/ObdManager;->getObdChipFirmwareVersion()Ljava/lang/String;

    move-result-object v3

    .line 495
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 496
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdFw:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    :cond_5
    return-void

    .line 479
    .end local v3    # "str":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 480
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateVoltage()V
    .locals 8

    .prologue
    .line 745
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/obd/ObdManager;->getBatteryVoltage()D

    move-result-wide v0

    .line 746
    .local v0, "dbl":D
    const-wide/16 v4, 0x0

    cmpl-double v3, v0, v4

    if-lez v3, :cond_0

    .line 747
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%.1f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v5, 0x7f090377

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 748
    .local v2, "str":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->voltage:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 752
    .end local v2    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 750
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->voltage:Landroid/widget/TextView;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public GPSSpeedChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1055
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1060
    :goto_0
    return-void

    .line 1058
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateSpeed()V

    .line 1059
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateGPSSpeed()V

    goto :goto_0
.end method

.method public ObdStateChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1036
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1041
    :goto_0
    return-void

    .line 1039
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateSpeed()V

    .line 1040
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateDynamicVehicleInfo()V

    goto :goto_0
.end method

.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 364
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x1

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->infoModel:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->cachedList:Ljava/util/List;

    .line 271
    return-object v0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 329
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 330
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 332
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 315
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SYSTEM_INFO:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x1

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 438
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 320
    if-nez p2, :cond_0

    .line 321
    const/4 v0, 0x1

    .line 323
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 6
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 343
    if-ne p3, v4, :cond_1

    move-object v1, p2

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    move-object v1, p2

    .line 344
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 345
    .local v0, "viewGroup":Landroid/view/ViewGroup;
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 346
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satelliteView:Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->advancedGpsStatsEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->setVisibility(I)V

    .line 347
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->update()V

    .line 349
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->registered:Z

    if-nez v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 351
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->registered:Z

    .line 352
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/obd/ObdManager;->enableInstantaneousMode(Z)V

    .line 353
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "registered, enabled instantaneous mode"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 356
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 357
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 360
    .end local v0    # "viewGroup":Landroid/view/ViewGroup;
    :cond_1
    return-void

    .line 346
    .restart local v0    # "viewGroup":Landroid/view/ViewGroup;
    :cond_2
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public onDriverProfilePrefChangeEvent(Lcom/navdy/hud/app/event/DriverProfileUpdated;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileUpdated;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 978
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 984
    :cond_0
    :goto_0
    return-void

    .line 981
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/app/event/DriverProfileUpdated;->state:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    sget-object v1, Lcom/navdy/hud/app/event/DriverProfileUpdated$State;->UPDATED:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    if-ne v0, v1, :cond_0

    .line 982
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateGenericPref()V

    goto :goto_0
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 431
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 426
    return-void
.end method

.method public onGpsSatelliteData(Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;)V
    .locals 16
    .param p1, "satelliteData"    # Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 910
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v10

    if-nez v10, :cond_0

    .line 946
    :goto_0
    return-void

    .line 913
    :cond_0
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    const-string v11, "SAT_SEEN"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 914
    .local v7, "seen":I
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    const-string v11, "SAT_USED"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 915
    .local v9, "used":I
    if-gtz v7, :cond_1

    if-lez v9, :cond_2

    .line 916
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satellites:Landroid/widget/TextView;

    sget-object v11, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v12, 0x7f09036b

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 921
    :goto_1
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    const-string v11, "SAT_FIX_TYPE"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    invoke-static {v10}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->getFixTypeDescription(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->ubloxFixType:Ljava/lang/String;

    .line 923
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    const-string v11, "SAT_MAX_DB"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 924
    .local v5, "max":I
    if-lez v5, :cond_3

    .line 925
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gps:Landroid/widget/TextView;

    sget-object v11, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->resources:Landroid/content/res/Resources;

    const v12, 0x7f090353

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 930
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->advancedGpsStatsEnabled()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 931
    new-array v1, v7, [Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;

    .line 932
    .local v1, "data":[Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    if-ge v3, v7, :cond_4

    .line 933
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SAT_PROVIDER_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v12, v3, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 934
    .local v6, "provider":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SAT_ID_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v12, v3, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 935
    .local v4, "id":I
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SAT_DB_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v12, v3, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 936
    .local v2, "db":I
    new-instance v10, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;

    invoke-direct {v10, v4, v2, v6}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;-><init>(IILjava/lang/String;)V

    aput-object v10, v1, v3

    .line 932
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 918
    .end local v1    # "data":[Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;
    .end local v2    # "db":I
    .end local v3    # "i":I
    .end local v4    # "id":I
    .end local v5    # "max":I
    .end local v6    # "provider":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satellites:Landroid/widget/TextView;

    sget-object v11, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 943
    .end local v7    # "seen":I
    .end local v9    # "used":I
    :catch_0
    move-exception v8

    .line 944
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v10, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 927
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v5    # "max":I
    .restart local v7    # "seen":I
    .restart local v9    # "used":I
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gps:Landroid/widget/TextView;

    sget-object v11, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->noData:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 938
    .restart local v1    # "data":[Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;
    .restart local v3    # "i":I
    :cond_4
    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 939
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satelliteView:Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    invoke-virtual {v10, v1}, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;->setSatelliteData([Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;)V

    .line 942
    .end local v1    # "data":[Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView$SatelliteData;
    .end local v3    # "i":I
    :cond_5
    sget-object v10, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "satellite data { seen="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " used="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " fixType="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->ubloxFixType:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " maxDB="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 420
    return-void
.end method

.method public onNavigationPrefChangeEvent(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V
    .locals 1
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 970
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 974
    :goto_0
    return-void

    .line 973
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateNavPref()V

    goto :goto_0
.end method

.method public onPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 998
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1032
    :goto_0
    return-void

    .line 1001
    :cond_0
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1019
    :sswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateEngineTemperature()V

    goto :goto_0

    .line 1003
    :sswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateFuel()V

    goto :goto_0

    .line 1007
    :sswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateRPM()V

    goto :goto_0

    .line 1011
    :sswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateMAF()V

    goto :goto_0

    .line 1015
    :sswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateOdometer()V

    goto :goto_0

    .line 1023
    :sswitch_5
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateSpeed()V

    goto :goto_0

    .line 1026
    :sswitch_6
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateEngineFuelPressure()V

    goto :goto_0

    .line 1029
    :sswitch_7
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateEngineTripFuel()V

    goto :goto_0

    .line 1001
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xc -> :sswitch_2
        0xd -> :sswitch_5
        0x2f -> :sswitch_1
        0x100 -> :sswitch_3
        0x102 -> :sswitch_6
        0x103 -> :sswitch_7
        0x104 -> :sswitch_4
    .end sparse-switch
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 423
    return-void
.end method

.method public onSpeedDataExpired(Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;)V
    .locals 1
    .param p1, "speedDataExpired"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1064
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1069
    :goto_0
    return-void

    .line 1067
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateSpeed()V

    .line 1068
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateGPSSpeed()V

    goto :goto_0
.end method

.method public onSpeedUnitChanged(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1046
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1051
    :goto_0
    return-void

    .line 1049
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateSpeed()V

    .line 1050
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateGPSSpeed()V

    goto :goto_0
.end method

.method public onSupportedPidsChanged(Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;)V
    .locals 1
    .param p1, "supportedPidsChangedEvent"    # Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 988
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->isMenuLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 994
    :goto_0
    return-void

    .line 991
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateSpeed()V

    .line 992
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateGPSSpeed()V

    .line 993
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->updateDynamicVehicleInfo()V

    goto :goto_0
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 4
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 369
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->registered:Z

    if-eqz v0, :cond_0

    .line 370
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-eq v0, v1, :cond_1

    .line 371
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onUnload: disable instantaneous mode"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 372
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/obd/ObdManager;->enableInstantaneousMode(Z)V

    .line 376
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 377
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->registered:Z

    .line 378
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unregistered"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 380
    :cond_0
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayFw:Landroid/widget/TextView;

    .line 381
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialFw:Landroid/widget/TextView;

    .line 382
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdFw:Landroid/widget/TextView;

    .line 383
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialBt:Landroid/widget/TextView;

    .line 384
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayBt:Landroid/widget/TextView;

    .line 386
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gps:Landroid/widget/TextView;

    .line 387
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satellites:Landroid/widget/TextView;

    .line 388
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fix:Landroid/widget/TextView;

    .line 389
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->temperature:Landroid/widget/TextView;

    .line 390
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->deviceMileage:Landroid/widget/TextView;

    .line 392
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereSdk:Landroid/widget/TextView;

    .line 393
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereUpdated:Landroid/widget/TextView;

    .line 394
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapsVersion:Landroid/widget/TextView;

    .line 395
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereOfflineMaps:Landroid/widget/TextView;

    .line 396
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapVerified:Landroid/widget/TextView;

    .line 397
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereVoiceVerified:Landroid/widget/TextView;

    .line 399
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->make:Landroid/widget/TextView;

    .line 400
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->carModel:Landroid/widget/TextView;

    .line 401
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->year:Landroid/widget/TextView;

    .line 402
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fuel:Landroid/widget/TextView;

    .line 403
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->rpm:Landroid/widget/TextView;

    .line 404
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speed:Landroid/widget/TextView;

    .line 405
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odo:Landroid/widget/TextView;

    .line 406
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->maf:Landroid/widget/TextView;

    .line 407
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTemp:Landroid/widget/TextView;

    .line 408
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->voltage:Landroid/widget/TextView;

    .line 410
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gestures:Landroid/widget/TextView;

    .line 411
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->autoOn:Landroid/widget/TextView;

    .line 412
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdData:Landroid/widget/TextView;

    .line 413
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->uiScaling:Landroid/widget/TextView;

    .line 415
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routeCalc:Landroid/widget/TextView;

    .line 416
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routePref:Landroid/widget/TextView;

    .line 417
    return-void

    .line 374
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onUnload: instantaneous mode still enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 5
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 300
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 302
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v0, :pswitch_data_0

    .line 310
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 304
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "back"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 305
    const-string v0, "back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->backSelection:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 302
    :pswitch_data_0
    .packed-switch 0x7f0e0047
        :pswitch_0
    .end packed-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 290
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 286
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->backSelection:I

    .line 287
    return-void
.end method

.method public setSelectedIcon()V
    .locals 4

    .prologue
    .line 294
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f0201dc

    const/4 v2, 0x0

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->infoTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 434
    return-void
.end method
