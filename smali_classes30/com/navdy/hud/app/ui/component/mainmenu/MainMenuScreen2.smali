.class public Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "MainMenuScreen2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;,
        Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Module;,
        Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f03005e
.end annotation


# static fields
.field public static final ARG_CONTACTS_LIST:Ljava/lang/String; = "CONTACTS"

.field public static final ARG_MENU_MODE:Ljava/lang/String; = "MENU_MODE"

.field public static final ARG_MENU_PATH:Ljava/lang/String; = "MENU_PATH"

.field public static final ARG_NOTIFICATION_ID:Ljava/lang/String; = "NOTIF_ID"

.field static final SLASH:Ljava/lang/String; = "/"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 110
    const/4 v0, -0x1

    return v0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 115
    const/4 v0, -0x1

    return v0
.end method

.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Module;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
