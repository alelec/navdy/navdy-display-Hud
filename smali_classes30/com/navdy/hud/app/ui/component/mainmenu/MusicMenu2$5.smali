.class Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;
.super Ljava/lang/Object;
.source "MusicMenu2.java"

# interfaces
.implements Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

.field final synthetic val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

.field final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;ILcom/navdy/service/library/events/audio/MusicCollectionInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .prologue
    .line 891
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iput p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$pos:I

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHit([B)V
    .locals 4
    .param p1, "data"    # [B
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 894
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$pos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->collectionIdString(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$700(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->handleArtwork([BLjava/lang/Integer;Ljava/lang/String;Z)V
    invoke-static {v0, p1, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$800(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;[BLjava/lang/Integer;Ljava/lang/String;Z)V

    .line 895
    return-void
.end method

.method public onMiss()V
    .locals 4

    .prologue
    .line 899
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requesting artwork for collection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 900
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 901
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 902
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionId:Ljava/lang/String;

    .line 903
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    const/16 v2, 0xc8

    .line 904
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    .line 905
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->build()Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    move-result-object v0

    .line 906
    .local v0, "artworkRequest":Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$5;->val$collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->postOrQueueArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V
    invoke-static {v1, v0, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$900(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicArtworkRequest;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V

    .line 907
    return-void
.end method
