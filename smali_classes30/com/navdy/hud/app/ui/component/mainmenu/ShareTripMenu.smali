.class Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;
.super Ljava/lang/Object;
.source "ShareTripMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;


# static fields
.field private static final SEND_WITHOUT_MESSAGE_POS:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final contact:Lcom/navdy/hud/app/framework/contacts/Contact;

.field private failedDestinationLabel:Ljava/lang/String;

.field private failedLatitude:D

.field private failedLongitude:D

.field private failedMessage:Ljava/lang/String;

.field private final glympseManager:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

.field private hasFailed:Z

.field private itemSelected:Z

.field private final notificationId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V
    .locals 2
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "notificationId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->itemSelected:Z

    .line 46
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getInstance()Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->glympseManager:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 48
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->notificationId:Ljava/lang/String;

    .line 49
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->hasFailed:Z

    .line 50
    return-void
.end method

.method private addGlympseOfflineGlance(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;DD)V
    .locals 10
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p3, "destinationLabel"    # Ljava/lang/String;
    .param p4, "latitude"    # D
    .param p6, "longitude"    # D

    .prologue
    .line 89
    new-instance v1, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;

    sget-object v3, Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;->OFFLINE:Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;

    move-object v2, p2

    move-object v4, p1

    move-object v5, p3

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/navdy/hud/app/framework/glympse/GlympseNotification;-><init>(Lcom/navdy/hud/app/framework/contacts/Contact;Lcom/navdy/hud/app/framework/glympse/GlympseNotification$Type;Ljava/lang/String;Ljava/lang/String;DD)V

    .line 90
    .local v1, "glympseOfflineNotification":Lcom/navdy/hud/app/framework/glympse/GlympseNotification;
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 91
    return-void
.end method

.method private sendShare(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .locals 1
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->sendShare(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method private sendShare(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V
    .locals 20
    .param p1, "contact"    # Lcom/navdy/hud/app/framework/contacts/Contact;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v14

    .line 100
    .local v14, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    const/4 v5, 0x0

    .line 101
    .local v5, "destinationLabel":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 102
    .local v6, "latitude":D
    const-wide/16 v8, 0x0

    .line 104
    .local v8, "longitude":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->notificationId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 105
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->notificationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 107
    :cond_0
    const/4 v13, 0x0

    .line 109
    .local v13, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 110
    const-string v16, "Trip"

    .line 111
    .local v16, "shareType":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationLabel()Ljava/lang/String;

    move-result-object v5

    .line 112
    invoke-virtual {v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationMarker()Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v11

    .line 113
    .local v11, "destinationMarker":Lcom/here/android/mpa/mapping/MapMarker;
    if-eqz v11, :cond_4

    .line 114
    invoke-virtual {v11}, Lcom/here/android/mpa/mapping/MapMarker;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v13

    .line 126
    .end local v11    # "destinationMarker":Lcom/here/android/mpa/mapping/MapMarker;
    :cond_1
    :goto_0
    if-eqz v13, :cond_2

    .line 127
    invoke-virtual {v13}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v6

    .line 128
    invoke-virtual {v13}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v8

    .line 131
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .local v10, "builder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->glympseManager:Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v10}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->addMessage(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;Ljava/lang/String;DDLjava/lang/StringBuilder;)Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;

    move-result-object v12

    .line 133
    .local v12, "error":Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;
    sget-object v2, Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;->NONE:Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;

    if-eq v12, v2, :cond_6

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->hasFailed:Z

    .line 134
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->hasFailed:Z

    if-eqz v2, :cond_3

    .line 135
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedMessage:Ljava/lang/String;

    .line 136
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedDestinationLabel:Ljava/lang/String;

    .line 137
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedLatitude:D

    .line 138
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedLongitude:D

    .line 140
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->hasFailed:Z

    if-nez v2, :cond_7

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-static {v2, v0, v1, v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordGlympseSent(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Glympse: success= "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->hasFailed:Z

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "share="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 142
    return-void

    .line 116
    .end local v10    # "builder":Ljava/lang/StringBuilder;
    .end local v12    # "error":Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;
    .restart local v11    # "destinationMarker":Lcom/here/android/mpa/mapping/MapMarker;
    :cond_4
    invoke-virtual {v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationRouteRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v15

    .line 118
    .local v15, "navigationRouteRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v15, :cond_1

    iget-object v2, v15, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v2, :cond_1

    .line 119
    new-instance v13, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v13    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v2, v15, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, v15, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-direct {v13, v2, v3, v0, v1}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .restart local v13    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    goto/16 :goto_0

    .line 123
    .end local v11    # "destinationMarker":Lcom/here/android/mpa/mapping/MapMarker;
    .end local v15    # "navigationRouteRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v16    # "shareType":Ljava/lang/String;
    :cond_5
    const-string v16, "Location"

    .restart local v16    # "shareType":Ljava/lang/String;
    goto/16 :goto_0

    .line 133
    .restart local v10    # "builder":Ljava/lang/StringBuilder;
    .restart local v12    # "error":Lcom/navdy/hud/app/framework/glympse/GlympseManager$Error;
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 140
    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 141
    :cond_8
    const/4 v2, 0x0

    goto :goto_3
.end method


# virtual methods
.method public onDestinationPickerClosed()V
    .locals 8

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->itemSelected:Z

    if-nez v0, :cond_0

    .line 80
    const-string v0, "back"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 83
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->hasFailed:Z

    if-eqz v0, :cond_1

    .line 84
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedDestinationLabel:Ljava/lang/String;

    iget-wide v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedLatitude:D

    iget-wide v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->failedLongitude:D

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->addGlympseOfflineGlance(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;DD)V

    .line 86
    :cond_1
    return-void
.end method

.method public onItemClicked(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 6
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    const/4 v5, 0x1

    .line 56
    invoke-static {}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getInstance()Lcom/navdy/hud/app/framework/glympse/GlympseManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->getMessages()[Ljava/lang/String;

    move-result-object v1

    .line 57
    .local v1, "messages":[Ljava/lang/String;
    aget-object v0, v1, p2

    .line 59
    .local v0, "message":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selected option: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 60
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "glympse_menu_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 62
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->itemSelected:Z

    .line 64
    if-nez p2, :cond_0

    .line 65
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->sendShare(Lcom/navdy/hud/app/framework/contacts/Contact;)V

    .line 69
    :goto_0
    return v5

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->contact:Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {p0, v2, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/ShareTripMenu;->sendShare(Lcom/navdy/hud/app/framework/contacts/Contact;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onItemSelected(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method
