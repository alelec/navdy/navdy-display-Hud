.class public final enum Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;
.super Ljava/lang/Enum;
.source "DashGaugeConfiguratorMenu.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GaugeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0004\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;",
        "",
        "(Ljava/lang/String;I)V",
        "CENTER",
        "SIDE",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

.field public static final enum CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

.field public static final enum SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    const-string v2, "CENTER"

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    const-string v2, "SIDE"

    invoke-direct {v1, v2, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->$VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "$enum_name_or_ordinal$0"    # Ljava/lang/String;
    .param p2, "$enum_name_or_ordinal$1"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;
    .locals 1

    const-class v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;
    .locals 1

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->$VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    return-object v0
.end method
