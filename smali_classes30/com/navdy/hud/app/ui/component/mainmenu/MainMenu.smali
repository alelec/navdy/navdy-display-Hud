.class public Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
.super Ljava/lang/Object;
.source "MainMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final ACTIVITY_TRAY_UPDATE:I

.field private static final ETA_UPDATE_INTERVAL:I

.field private static final activeTripTitle:Ljava/lang/String;

.field private static final activityTrayIcon:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final activityTrayIconId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final activityTraySelectedColor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final activityTrayUnSelectedColor:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final bkColorUnselected:I

.field private static final callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

.field static final childMenus:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;",
            ">;"
        }
    .end annotation
.end field

.field private static final connectPhone:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final contacts:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final dashTitle:Ljava/lang/String;

.field public static final defaultFontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

.field private static final glancesColor:I

.field private static final handler:Landroid/os/Handler;

.field private static final mapTitle:Ljava/lang/String;

.field private static final maps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final musicControl:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static musicManager:Lcom/navdy/hud/app/manager/MusicManager;

.field private static final nowPlayingColor:I

.field private static final places:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final settings:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final smartDash:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

.field private static final voiceAssistanceGoogle:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final voiceAssistanceSiri:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final voiceSearch:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;


# instance fields
.field private activeTripMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

.field private activityTrayRunnable:Ljava/lang/Runnable;

.field private activityTrayRunning:Z

.field private activityTraySelection:I

.field private activityTraySelectionId:I

.field private addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

.field private bus:Lcom/squareup/otto/Bus;

.field private busRegistered:Z

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

.field private curToolTipId:I

.field private curToolTipPos:I

.field private currentEta:Ljava/lang/String;

.field private etaRunnable:Ljava/lang/Runnable;

.field private mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

.field private musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private musicUpdateListener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

.field private placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private registeredMusicCallback:Z

.field private selectedIndex:I

.field private settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

.field private swVersion:Ljava/lang/String;

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const v14, 0x7f0e0045

    const/4 v13, 0x0

    .line 77
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 97
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1e

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->ETA_UPDATE_INTERVAL:I

    .line 98
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->ACTIVITY_TRAY_UPDATE:I

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->childMenus:Ljava/util/HashMap;

    .line 104
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    .line 119
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->childMenus:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SETTINGS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SETTINGS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->childMenus:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->childMenus:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->childMenus:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    .line 125
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    .line 126
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getCallManager()Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .line 127
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 129
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    .line 130
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activeTripTitle:Ljava/lang/String;

    .line 136
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getFontInfo(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->defaultFontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .line 139
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->nowPlayingColor:I

    .line 141
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->glancesColor:I

    .line 144
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 145
    .local v5, "title":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 146
    .local v6, "subTitle":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0088

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 147
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0046

    const v1, 0x7f02018d

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->voiceSearch:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 150
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 151
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 152
    const v7, 0x7f0e003f

    const v8, 0x7f020174

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->smartDash:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 155
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 156
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 157
    const v7, 0x7f0e0020

    const v8, 0x7f02017e

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->maps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 160
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 161
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 162
    const v7, 0x7f0e0039

    const v8, 0x7f020184

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->places:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 165
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 166
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 167
    const v7, 0x7f0e0022

    const v8, 0x7f020180

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicControl:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 170
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 171
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 172
    const v8, 0x7f020188

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v7, v14

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->voiceAssistanceSiri:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 175
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 176
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 177
    const v8, 0x7f02017b

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v7, v14

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->voiceAssistanceGoogle:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 180
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 181
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 182
    const v7, 0x7f0e0014

    const v8, 0x7f020172

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contacts:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 185
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090074

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 186
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d007c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 187
    const v7, 0x7f0e003c

    const v8, 0x7f020187

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settings:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 190
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 191
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 192
    const v7, 0x7f0e003d

    const v8, 0x7f0201d5

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v9, v2

    move v11, v2

    move-object v12, v5

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->connectPhone:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 194
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->dashTitle:Ljava/lang/String;

    .line 195
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mapTitle:Ljava/lang/String;

    .line 197
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    .line 198
    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    const/4 v1, -0x1

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    iput v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    .line 222
    iput v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectionId:I

    .line 224
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->etaRunnable:Ljava/lang/Runnable;

    .line 234
    iput v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I

    .line 235
    iput v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I

    .line 237
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunnable:Ljava/lang/Runnable;

    .line 255
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicUpdateListener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    .line 286
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    .line 287
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 288
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 289
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->fillEta()V

    return-void
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->ETA_UPDATE_INTERVAL:I

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->canShowTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    iget v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I

    return v0
.end method

.method static synthetic access$1200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getMusicTrackInfo()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$1500()Lcom/navdy/hud/app/manager/MusicManager;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    return-object v0
.end method

.method static synthetic access$200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunning:Z

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunning:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    iget v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    .param p1, "x1"    # I

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getToolTipString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->ACTIVITY_TRAY_UPDATE:I

    return v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    return-object v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    return-object p1
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Lcom/navdy/service/library/events/audio/MusicTrackInfo;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .param p2, "x2"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->isSameTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v0

    return v0
.end method

.method private addLongPressAction(Lcom/navdy/service/library/events/DeviceInfo;Ljava/util/List;ZZZ)V
    .locals 2
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;
    .param p3, "isDeviceCapableOfVoiceSearch"    # Z
    .param p4, "navPossible"    # Z
    .param p5, "start"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/DeviceInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;ZZZ)V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    const/4 v1, -0x1

    .line 408
    if-eqz p5, :cond_4

    .line 409
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->isLongPressActionPlaceSearch()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411
    iget v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    if-ne v0, v1, :cond_0

    .line 412
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    .line 414
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addVoiceAssistance(Lcom/navdy/service/library/events/DeviceInfo;Ljava/util/List;)V

    .line 431
    :cond_1
    :goto_0
    return-void

    .line 415
    :cond_2
    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    .line 417
    iget v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    if-ne v0, v1, :cond_3

    .line 418
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    .line 420
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->voiceSearch:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 423
    :cond_4
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->isLongPressActionPlaceSearch()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 424
    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    .line 425
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->voiceSearch:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 428
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addVoiceAssistance(Lcom/navdy/service/library/events/DeviceInfo;Ljava/util/List;)V

    goto :goto_0
.end method

.method private addVoiceAssistance(Lcom/navdy/service/library/events/DeviceInfo;Ljava/util/List;)V
    .locals 2
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/DeviceInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 394
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v0, v1, :cond_0

    .line 396
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->voiceAssistanceSiri:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 401
    :goto_0
    return-void

    .line 399
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->voiceAssistanceGoogle:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private canShowTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    .locals 2
    .param p1, "musicTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 1071
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 1072
    invoke-static {v0}, Lcom/navdy/hud/app/manager/MusicManager;->tryingToPlay(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 1073
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1074
    const/4 v0, 0x1

    .line 1076
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearNowPlayingState()V
    .locals 1

    .prologue
    .line 1081
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 1082
    return-void
.end method

.method private createChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 9
    .param p1, "menu"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 820
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createChildMenu:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 821
    const/4 v7, 0x0

    .line 822
    .local v7, "element":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 823
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 824
    .local v8, "index":I
    if-nez v8, :cond_3

    .line 825
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 826
    const-string v0, "/"

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 827
    if-ltz v8, :cond_2

    .line 828
    add-int/lit8 v0, v8, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 829
    const/4 v0, 0x0

    invoke-virtual {v7, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 837
    .end local v8    # "index":I
    :cond_0
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$6;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v6, v5

    .line 892
    :cond_1
    :goto_1
    return-object v6

    .line 831
    .restart local v8    # "index":I
    :cond_2
    const/4 p2, 0x0

    goto :goto_0

    .line 834
    :cond_3
    const/4 p2, 0x0

    goto :goto_0

    .line 839
    .end local v8    # "index":I
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    if-nez v0, :cond_4

    .line 840
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    .line 842
    :cond_4
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 843
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    invoke-virtual {v0, p0, v7, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v6

    .line 844
    .local v6, "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-nez v6, :cond_1

    .line 848
    .end local v6    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    const v1, 0x7f0e0014

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->setBackSelectionId(I)V

    .line 849
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    goto :goto_1

    .line 852
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    if-nez v0, :cond_6

    .line 853
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    .line 855
    :cond_6
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 856
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    invoke-virtual {v0, p0, v7, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v6

    .line 857
    .restart local v6    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-nez v6, :cond_1

    .line 861
    .end local v6    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    const v1, 0x7f0e0039

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->setBackSelectionId(I)V

    .line 862
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    goto :goto_1

    .line 865
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    if-nez v0, :cond_8

    .line 866
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    .line 868
    :cond_8
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 869
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    invoke-virtual {v0, p0, v7, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v6

    .line 870
    .restart local v6    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-nez v6, :cond_1

    .line 875
    .end local v6    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    :cond_9
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    const v1, 0x7f0e003c

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;->setBackSelectionId(I)V

    .line 876
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    goto/16 :goto_1

    .line 879
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-nez v0, :cond_a

    .line 880
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$MusicMenuData;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 882
    :cond_a
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    const v1, 0x7f0e0022

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->setBackSelectionId(I)V

    .line 883
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 884
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-interface {v0, p0, v7, p2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v6

    .line 885
    .restart local v6    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-nez v6, :cond_1

    .line 889
    .end local v6    # "child":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    :cond_b
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    goto/16 :goto_1

    .line 837
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private fillEta()V
    .locals 3

    .prologue
    .line 897
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentEtaStringWithDestination()Ljava/lang/String;

    move-result-object v0

    .line 898
    .local v0, "etaString":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 900
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->currentEta:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 905
    .end local v0    # "etaString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 902
    :catch_0
    move-exception v1

    .line 903
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getActivityTrayModel(Z)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 24
    .param p1, "isConnected"    # Z

    .prologue
    .line 924
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 925
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 926
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 927
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 930
    const/4 v10, 0x0

    .line 931
    .local v10, "callAdded":Z
    if-eqz p1, :cond_0

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isCallInProgress()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 932
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f020172

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 933
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e0018

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 934
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0d0055

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 935
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 936
    const/4 v10, 0x1

    .line 940
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->etaRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 941
    const/16 v21, 0x0

    .line 942
    .local v21, "tripOptionSlot":Z
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 943
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v17

    .line 944
    .local v17, "navigationOn":Z
    if-eqz v17, :cond_1

    .line 945
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "nav on adding trip menu"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 946
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f0200d8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 947
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e0015

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 948
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0d0053

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 949
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 950
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->fillEta()V

    .line 951
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->etaRunnable:Ljava/lang/Runnable;

    sget v9, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->ETA_UPDATE_INTERVAL:I

    int-to-long v0, v9

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    invoke-virtual {v2, v7, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 952
    const/16 v21, 0x1

    .line 957
    .end local v17    # "navigationOn":Z
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 958
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f02017a

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 959
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e001e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 960
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->glancesColor:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 961
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_2
    if-eqz p1, :cond_3

    if-nez v10, :cond_3

    .line 967
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentTrack()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v20

    .line 968
    .local v20, "trackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->canShowTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 969
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f020180

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 970
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e0028

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 971
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->nowPlayingColor:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 972
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 973
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 974
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "added now playing"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 980
    .end local v20    # "trackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_3
    :goto_0
    if-nez v21, :cond_4

    .line 982
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->isUpdateAvailable()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 983
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0901bc

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->getSWUpdateVersion()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v9, v22

    invoke-virtual {v2, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->swVersion:Ljava/lang/String;

    .line 984
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "add navdy s/w"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 985
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f0201ed

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 986
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e0083

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 987
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0d0084

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 988
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 989
    const/16 v21, 0x1

    .line 993
    :cond_4
    if-nez v21, :cond_5

    .line 995
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v13

    .line 996
    .local v13, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    invoke-virtual {v13}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v13}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->isUpdateAvailable()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 997
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "add dial s/w"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 998
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f020104

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e007e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0d0083

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1001
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1002
    const/16 v21, 0x1

    .line 1007
    .end local v13    # "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    :cond_5
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getDefaultMainActiveScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v18

    .line 1008
    .local v18, "screen":Lcom/navdy/service/library/events/ui/Screen;
    const/4 v12, 0x0

    .line 1009
    .local v12, "dashModeOn":Z
    const/16 v16, 0x0

    .line 1010
    .local v16, "mapModeOn":Z
    if-eqz v18, :cond_6

    .line 1011
    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    move-object/from16 v0, v18

    if-ne v0, v2, :cond_6

    .line 1012
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v14

    .line 1013
    .local v14, "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v14, :cond_6

    .line 1014
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$6;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    invoke-virtual {v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v7

    aget v2, v2, v7

    packed-switch v2, :pswitch_data_0

    .line 1027
    .end local v14    # "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_6
    :goto_1
    if-eqz v12, :cond_9

    .line 1028
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f020168

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1029
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e0040

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1030
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0d0058

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1031
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1039
    :cond_7
    :goto_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 1040
    .local v19, "size":I
    move/from16 v0, v19

    new-array v3, v0, [I

    .line 1041
    .local v3, "icons":[I
    move/from16 v0, v19

    new-array v4, v0, [I

    .line 1042
    .local v4, "iconIds":[I
    move/from16 v0, v19

    new-array v5, v0, [I

    .line 1043
    .local v5, "iconSelectedColor":[I
    move/from16 v0, v19

    new-array v6, v0, [I

    .line 1045
    .local v6, "iconUnselectedColor":[I
    const/4 v11, 0x0

    .line 1046
    .local v11, "counter":I
    add-int/lit8 v15, v19, -0x1

    .local v15, "i":I
    :goto_3
    if-ltz v15, :cond_a

    .line 1047
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v3, v11

    .line 1048
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v4, v11

    .line 1049
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v5, v11

    .line 1050
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v6, v11

    .line 1051
    add-int/lit8 v11, v11, 0x1

    .line 1046
    add-int/lit8 v15, v15, -0x1

    goto :goto_3

    .line 976
    .end local v3    # "icons":[I
    .end local v4    # "iconIds":[I
    .end local v5    # "iconSelectedColor":[I
    .end local v6    # "iconUnselectedColor":[I
    .end local v11    # "counter":I
    .end local v12    # "dashModeOn":Z
    .end local v15    # "i":I
    .end local v16    # "mapModeOn":Z
    .end local v18    # "screen":Lcom/navdy/service/library/events/ui/Screen;
    .end local v19    # "size":I
    .restart local v20    # "trackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_8
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "not added now playing"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1016
    .end local v20    # "trackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .restart local v12    # "dashModeOn":Z
    .restart local v14    # "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .restart local v16    # "mapModeOn":Z
    .restart local v18    # "screen":Lcom/navdy/service/library/events/ui/Screen;
    :pswitch_0
    const/4 v12, 0x1

    .line 1017
    goto/16 :goto_1

    .line 1020
    :pswitch_1
    const/16 v16, 0x1

    goto/16 :goto_1

    .line 1032
    .end local v14    # "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_9
    if-eqz v16, :cond_7

    .line 1033
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIcon:Ljava/util/ArrayList;

    const v7, 0x7f020169

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1034
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayIconId:Ljava/util/ArrayList;

    const v7, 0x7f0e0021

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1035
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectedColor:Ljava/util/ArrayList;

    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0d005d

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1036
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayUnSelectedColor:Ljava/util/ArrayList;

    sget v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1054
    .restart local v3    # "icons":[I
    .restart local v4    # "iconIds":[I
    .restart local v5    # "iconSelectedColor":[I
    .restart local v6    # "iconUnselectedColor":[I
    .restart local v11    # "counter":I
    .restart local v15    # "i":I
    .restart local v19    # "size":I
    :cond_a
    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    .line 1055
    .local v8, "selection":I
    const/4 v2, -0x1

    if-ne v8, v2, :cond_b

    .line 1056
    add-int/lit8 v8, v19, -0x1

    .line 1058
    :cond_b
    const v2, 0x7f0e0016

    const/4 v9, 0x1

    move-object v7, v5

    invoke-static/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->buildModel(I[I[I[I[I[IIZ)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    return-object v2

    .line 1014
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getDurationStr(J)Ljava/lang/String;
    .locals 9
    .param p1, "time"    # J

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1184
    const-wide/16 v4, 0xe10

    cmp-long v3, p1, v4

    if-gez v3, :cond_0

    .line 1186
    long-to-int v3, p1

    div-int/lit8 v1, v3, 0x3c

    .line 1187
    .local v1, "minutes":I
    mul-int/lit8 v3, v1, 0x3c

    int-to-long v4, v3

    sub-long v4, p1, v4

    long-to-int v2, v4

    .line 1188
    .local v2, "secs":I
    const-string v3, "%02d:%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1194
    :goto_0
    return-object v3

    .line 1190
    .end local v1    # "minutes":I
    .end local v2    # "secs":I
    :cond_0
    long-to-int v3, p1

    div-int/lit16 v0, v3, 0xe10

    .line 1191
    .local v0, "hours":I
    long-to-int v3, p1

    mul-int/lit8 v4, v0, 0x3c

    mul-int/lit8 v4, v4, 0x3c

    sub-int/2addr v3, v4

    int-to-long p1, v3

    .line 1192
    long-to-int v3, p1

    div-int/lit8 v1, v3, 0x3c

    .line 1193
    .restart local v1    # "minutes":I
    mul-int/lit8 v3, v1, 0x3c

    int-to-long v4, v3

    sub-long v4, p1, v4

    long-to-int v2, v4

    .line 1194
    .restart local v2    # "secs":I
    const-string v3, "%02d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getMusicTrackInfo()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1199
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-nez v6, :cond_1

    .line 1200
    const/4 v3, 0x0

    .line 1211
    :cond_0
    :goto_0
    return-object v3

    .line 1202
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v3, v6, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 1203
    .local v3, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v6, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 1204
    .local v0, "author":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    move v1, v4

    .line 1205
    .local v1, "isValidAuthor":Z
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    move v2, v4

    .line 1206
    .local v2, "isValidName":Z
    :goto_2
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    .line 1207
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .end local v1    # "isValidAuthor":Z
    .end local v2    # "isValidName":Z
    :cond_2
    move v1, v5

    .line 1204
    goto :goto_1

    .restart local v1    # "isValidAuthor":Z
    :cond_3
    move v2, v5

    .line 1205
    goto :goto_2

    .line 1208
    .restart local v2    # "isValidName":Z
    :cond_4
    if-nez v2, :cond_0

    move-object v3, v0

    .line 1211
    goto :goto_0
.end method

.method private getToolTipString(I)Ljava/lang/String;
    .locals 12
    .param p1, "id"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 1128
    sparse-switch p1, :sswitch_data_0

    .line 1179
    :cond_0
    :goto_0
    return-object v7

    .line 1130
    :sswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getMusicTrackInfo()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 1133
    :sswitch_1
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v6

    .line 1134
    .local v6, "versions":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "1.0."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v6, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v8, v8, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1135
    .local v5, "updateTargetVersion":Ljava/lang/String;
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v8, 0x7f0901b7

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v5, v9, v11

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 1138
    .end local v5    # "updateTargetVersion":Ljava/lang/String;
    .end local v6    # "versions":Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    :sswitch_2
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->swVersion:Ljava/lang/String;

    goto :goto_0

    .line 1141
    :sswitch_3
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v8

    const-string v9, "navdy#phone#call#notif"

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .line 1142
    .local v0, "callNotification":Lcom/navdy/hud/app/framework/phonecall/CallNotification;
    if-eqz v0, :cond_0

    sget-object v8, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    invoke-virtual {v8}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isCallInProgress()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1143
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getCaller()Ljava/lang/String;

    move-result-object v2

    .line 1144
    .local v2, "name":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1147
    sget-object v8, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    invoke-virtual {v8}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->getCurrentCallDuration()I

    move-result v4

    .line 1148
    .local v4, "time":I
    if-ltz v4, :cond_0

    .line 1151
    int-to-long v8, v4

    invoke-direct {p0, v8, v9}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getDurationStr(J)Ljava/lang/String;

    move-result-object v1

    .line 1152
    .local v1, "duration":Ljava/lang/String;
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v8, 0x7f0901b6

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v2, v9, v11

    aput-object v1, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 1158
    .end local v0    # "callNotification":Lcom/navdy/hud/app/framework/phonecall/CallNotification;
    .end local v1    # "duration":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "time":I
    :sswitch_4
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v3

    .line 1159
    .local v3, "notificationCount":I
    if-ne v3, v10, :cond_1

    .line 1160
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v8, 0x7f0901ba

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1162
    :cond_1
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v8, 0x7f0901bb

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1166
    .end local v3    # "notificationCount":I
    :sswitch_5
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->currentEta:Ljava/lang/String;

    if-nez v7, :cond_2

    .line 1167
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activeTripTitle:Ljava/lang/String;

    goto/16 :goto_0

    .line 1169
    :cond_2
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->currentEta:Ljava/lang/String;

    goto/16 :goto_0

    .line 1173
    :sswitch_6
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->dashTitle:Ljava/lang/String;

    goto/16 :goto_0

    .line 1176
    :sswitch_7
    sget-object v7, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mapTitle:Ljava/lang/String;

    goto/16 :goto_0

    .line 1128
    :sswitch_data_0
    .sparse-switch
        0x7f0e0015 -> :sswitch_5
        0x7f0e0018 -> :sswitch_3
        0x7f0e001e -> :sswitch_4
        0x7f0e0021 -> :sswitch_7
        0x7f0e0028 -> :sswitch_0
        0x7f0e0040 -> :sswitch_6
        0x7f0e007e -> :sswitch_1
        0x7f0e0083 -> :sswitch_2
    .end sparse-switch
.end method

.method private handleGlances()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1085
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1086
    const-string v0, "glances"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 1087
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 1088
    .local v9, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v8

    .line 1089
    .local v8, "prefs":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    iget-object v0, v8, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 1090
    .local v6, "isGlancedEnabled":Z
    :goto_0
    if-eqz v6, :cond_2

    .line 1092
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v7

    .line 1093
    .local v7, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-virtual {v7, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->makeNotificationCurrent(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1094
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1095
    const-string v0, "dial"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->setGlanceNavigationSource(Ljava/lang/String;)V

    .line 1096
    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    .line 1125
    .end local v7    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :goto_1
    return-void

    .end local v6    # "isGlancedEnabled":Z
    :cond_0
    move v6, v5

    .line 1089
    goto :goto_0

    .line 1098
    .restart local v6    # "isGlancedEnabled":Z
    .restart local v7    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "no glances found"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1099
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1100
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0x3e8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1101
    const-string v0, "8"

    const v1, 0x7f0201cb

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1102
    const-string v0, "4"

    const v1, 0x7f09011a

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1104
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v10

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "glance-none"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v10, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 1105
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1109
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v7    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances are not enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1110
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1111
    .restart local v2    # "bundle":Landroid/os/Bundle;
    const-string v0, "15"

    const v1, 0x7f0b00e3

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1112
    const-string v0, "12"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1113
    const-string v0, "13"

    const/16 v1, 0x1388

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1114
    const-string v0, "8"

    const v1, 0x7f020124

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1115
    const-string v0, "11"

    const v1, 0x7f020105

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1116
    const-string v0, "4"

    const v1, 0x7f090116

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    const-string v0, "5"

    const/high16 v1, 0x7f0c0000

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1118
    const-string v0, "6"

    const v1, 0x7f090117

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    const-string v0, "7"

    const v1, 0x7f0c0001

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1120
    const-string v0, "16"

    const/high16 v1, 0x7f0b0000

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1121
    const-string v0, "17"

    sget-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_GLANCES_DISABLED:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v10

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "glance-disabled"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v10, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 1123
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_1
.end method

.method private isLongPressActionPlaceSearch()Z
    .locals 2

    .prologue
    .line 915
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getLongPressAction()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    move-result-object v0

    .line 916
    .local v0, "action":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    if-ne v0, v1, :cond_0

    .line 917
    const/4 v1, 0x1

    .line 919
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isSameTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    .locals 2
    .param p1, "oldTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .param p2, "newTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 1062
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1063
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1064
    const/4 v0, 0x1

    .line 1067
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startActivityTrayRunnable(II)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 1216
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I

    .line 1217
    iput p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I

    .line 1218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunning:Z

    .line 1219
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->ACTIVITY_TRAY_UPDATE:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1220
    return-void
.end method

.method private stopActivityTrayRunnable()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1223
    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I

    .line 1224
    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I

    .line 1225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunning:Z

    .line 1226
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1227
    return-void
.end method


# virtual methods
.method public arrivalEvent(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 909
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "arrival event"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 910
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->etaRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 911
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->fillEta()V

    .line 912
    return-void
.end method

.method public clearState()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 811
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    .line 812
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    .line 813
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    .line 814
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .line 815
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activeTripMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    .line 816
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 817
    return-void
.end method

.method public getActivityTraySelection()I
    .locals 1

    .prologue
    .line 807
    iget v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    return v0
.end method

.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 4
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 766
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getChildMenu:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 767
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->childMenus:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 768
    .local v0, "m":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    if-nez v0, :cond_0

    .line 769
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "getChildMenu: not found"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 770
    const/4 v1, 0x0

    .line 772
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0, p3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->createChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v1

    goto :goto_0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->clearNowPlayingState()V

    .line 294
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->stopActivityTrayRunnable()V

    .line 295
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I

    .line 296
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I

    .line 299
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v3

    .line 300
    .local v3, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 301
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v2

    if-nez v2, :cond_9

    const/16 v18, 0x1

    .line 302
    .local v18, "isConnected":Z
    :goto_0
    if-eqz v18, :cond_0

    if-nez v3, :cond_0

    .line 303
    const/16 v18, 0x0

    .line 305
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isConnected:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 307
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    .line 310
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v16

    .line 311
    .local v16, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getActivityTrayModel(Z)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v14

    .line 312
    .local v14, "activityTray":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isNavigationModeOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 313
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    .line 314
    iget-object v2, v14, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    if-eqz v2, :cond_1

    .line 315
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    iget-object v2, v14, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    array-length v2, v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_1

    .line 316
    iget-object v2, v14, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    aget v2, v2, v17

    const v7, 0x7f0e0015

    if-ne v2, v7, :cond_a

    .line 317
    move/from16 v0, v17

    iput v0, v14, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->currentIconSelection:I

    .line 323
    .end local v17    # "i":I
    :cond_1
    invoke-interface {v4, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isNavigationPossible()Z

    move-result v6

    .line 327
    .local v6, "navPossible":Z
    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->supportsVoiceSearch()Z

    move-result v5

    .line 328
    .local v5, "isDeviceCapableOfVoiceSearch":Z
    if-eqz v18, :cond_b

    .line 329
    if-nez v6, :cond_2

    .line 330
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "nav not possible"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 332
    :cond_2
    const/4 v7, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addLongPressAction(Lcom/navdy/service/library/events/DeviceInfo;Ljava/util/List;ZZZ)V

    .line 338
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    const/4 v7, -0x1

    if-ne v2, v7, :cond_3

    .line 339
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    .line 343
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->maps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->smartDash:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    if-eqz v18, :cond_5

    .line 347
    if-eqz v6, :cond_4

    .line 349
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->places:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contacts:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    :cond_5
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0901b8

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 356
    .local v12, "title":Ljava/lang/String;
    const v7, 0x7f0e001e

    const v8, 0x7f02017a

    sget v9, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->glancesColor:I

    sget v10, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    sget v11, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->glancesColor:I

    const/4 v13, 0x0

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v15

    .line 357
    .local v15, "glances":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v21

    .line 358
    .local v21, "prefs":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    .line 359
    .local v19, "isGlancedEnabled":Z
    :goto_3
    if-nez v19, :cond_d

    .line 360
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0901b9

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v15, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 361
    const/4 v2, 0x0

    iput-object v2, v15, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 367
    :goto_4
    invoke-interface {v4, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    if-eqz v18, :cond_6

    .line 371
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicControl:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addLongPressAction(Lcom/navdy/service/library/events/DeviceInfo;Ljava/util/List;ZZZ)V

    .line 375
    :cond_6
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settings:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->cachedList:Ljava/util/List;

    .line 379
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->registeredMusicCallback:Z

    if-nez v2, :cond_7

    .line 380
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicUpdateListener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    invoke-virtual {v2, v7}, Lcom/navdy/hud/app/manager/MusicManager;->addMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 381
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->registeredMusicCallback:Z

    .line 382
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "register music"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 385
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->busRegistered:Z

    if-nez v2, :cond_8

    .line 386
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 387
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->busRegistered:Z

    .line 388
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "registered bus"

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 390
    :cond_8
    return-object v4

    .line 301
    .end local v5    # "isDeviceCapableOfVoiceSearch":Z
    .end local v6    # "navPossible":Z
    .end local v12    # "title":Ljava/lang/String;
    .end local v14    # "activityTray":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v15    # "glances":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v16    # "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .end local v18    # "isConnected":Z
    .end local v19    # "isGlancedEnabled":Z
    .end local v21    # "prefs":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    :cond_9
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 315
    .restart local v14    # "activityTray":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .restart local v16    # "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .restart local v17    # "i":I
    .restart local v18    # "isConnected":Z
    :cond_a
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 334
    .end local v17    # "i":I
    .restart local v5    # "isDeviceCapableOfVoiceSearch":Z
    .restart local v6    # "navPossible":Z
    :cond_b
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->selectedIndex:I

    .line 335
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->connectPhone:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 358
    .restart local v12    # "title":Ljava/lang/String;
    .restart local v15    # "glances":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .restart local v21    # "prefs":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    :cond_c
    const/16 v19, 0x0

    goto :goto_3

    .line 363
    .restart local v19    # "isGlancedEnabled":Z
    :cond_d
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v20

    .line 364
    .local v20, "notificationCount":I
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0901b8

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v15, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 365
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0901bb

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v2, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v15, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    goto/16 :goto_4
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 486
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 487
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 489
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 761
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 566
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 472
    packed-switch p1, :pswitch_data_0

    .line 480
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 477
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0024
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 499
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 544
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 541
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 3
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v2, -0x1

    .line 503
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    packed-switch v1, :pswitch_data_0

    .line 531
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->stopActivityTrayRunnable()V

    .line 532
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->hideToolTip()V

    .line 535
    :goto_0
    return-void

    .line 505
    :pswitch_0
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getToolTipString(I)Ljava/lang/String;

    move-result-object v0

    .line 506
    .local v0, "toolTipStr":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 507
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->stopActivityTrayRunnable()V

    .line 508
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    sparse-switch v1, :sswitch_data_0

    .line 519
    iput v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I

    .line 520
    iput v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I

    .line 523
    :goto_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showToolTip(ILjava/lang/String;)V

    goto :goto_0

    .line 510
    :sswitch_0
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->startActivityTrayRunnable(II)V

    goto :goto_1

    .line 514
    :sswitch_1
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    iput v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I

    .line 515
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    iput v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I

    goto :goto_1

    .line 525
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->stopActivityTrayRunnable()V

    .line 526
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->hideToolTip()V

    goto :goto_0

    .line 503
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0016
        :pswitch_0
    .end packed-switch

    .line 508
    :sswitch_data_0
    .sparse-switch
        0x7f0e0018 -> :sswitch_0
        0x7f0e0028 -> :sswitch_1
    .end sparse-switch
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 538
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 3
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    const/4 v2, 0x0

    .line 777
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->etaRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 779
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->registeredMusicCallback:Z

    if-eqz v0, :cond_0

    .line 780
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->registeredMusicCallback:Z

    .line 781
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicUpdateListener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->removeMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 782
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unregister"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 785
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->busRegistered:Z

    if-eqz v0, :cond_1

    .line 786
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 787
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->busRegistered:Z

    .line 788
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "bus unregistered"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 791
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->clearNowPlayingState()V

    .line 793
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    if-ne p1, v0, :cond_4

    .line 794
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    if-eqz v0, :cond_2

    .line 795
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    .line 797
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    if-eqz v0, :cond_3

    .line 798
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    .line 800
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    if-eqz v0, :cond_4

    .line 801
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->CLOSE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V

    .line 804
    :cond_4
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 10
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 571
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 573
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 756
    :goto_0
    return v4

    .line 575
    :sswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dash"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 576
    const-string v0, "dash"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 577
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 582
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "map"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 583
    const-string v0, "hybrid_map"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 584
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 589
    :sswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handleGlances()V

    goto :goto_0

    .line 594
    :sswitch_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "voice_search"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 595
    const-string v0, "voice_search"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$4;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 611
    :sswitch_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v8

    .line 612
    .local v8, "musicManager":Lcom/navdy/hud/app/manager/MusicManager;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->isMusicBrowsingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcom/navdy/hud/app/manager/MusicManager;->hasMusicCapabilities()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "music browse"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 614
    const-string v0, "music"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 615
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-direct {p0, v0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->createChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 616
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->musicMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 618
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "music"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 619
    const-string v0, "music"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 620
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 626
    .end local v8    # "musicManager":Lcom/navdy/hud/app/manager/MusicManager;
    :sswitch_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "voice-assist"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 627
    const-string v0, "voice_assist"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_VOICE_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 633
    :sswitch_6
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "settings"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 634
    const-string v0, "settings"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 635
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SETTINGS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-direct {p0, v0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->createChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 636
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->settingsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/SettingsMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 641
    :sswitch_7
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "connect phone"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 642
    const-string v0, "connect_phone"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 643
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 644
    .local v6, "args":Landroid/os/Bundle;
    sget-object v0, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_SWITCH_PHONE:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v6, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 650
    .end local v6    # "args":Landroid/os/Bundle;
    :sswitch_8
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "places"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 651
    const-string v0, "places"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 652
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-direct {p0, v0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->createChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 653
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->placesMenu:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 658
    :sswitch_9
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "contacts"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 659
    const-string v0, "contacts"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 660
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-direct {p0, v0, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->createChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 661
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->contactsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ContactsMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 666
    :sswitch_a
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_0

    .line 714
    :sswitch_b
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "trip options"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 715
    const-string v0, "trip-options"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 716
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activeTripMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    if-nez v0, :cond_1

    .line 717
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activeTripMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    .line 719
    :cond_1
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    .line 720
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectionId:I

    .line 721
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activeTripMenu:Lcom/navdy/hud/app/ui/component/mainmenu/ActiveTripMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZ)V

    goto/16 :goto_0

    .line 668
    :sswitch_c
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "now_playing"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 669
    const-string v0, "now_playing"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 670
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$5;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 681
    :sswitch_d
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dial update"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 682
    const-string v0, "dial_update_mm"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 683
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 684
    .restart local v6    # "args":Landroid/os/Bundle;
    const-string v0, "UPDATE_REMINDER"

    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 685
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v6, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 689
    .end local v6    # "args":Landroid/os/Bundle;
    :sswitch_e
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "software update"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 690
    const-string v0, "software_update_mm"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 691
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 692
    .restart local v6    # "args":Landroid/os/Bundle;
    const-string v0, "UPDATE_REMINDER"

    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 693
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3, v6, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 697
    .end local v6    # "args":Landroid/os/Bundle;
    :sswitch_f
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "phone call"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 698
    const-string v0, "phone_call_mm"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 699
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v9

    .line 700
    .local v9, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v0, "navdy#phone#call#notif"

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v7

    .line 701
    .local v7, "callNotif":Lcom/navdy/hud/app/framework/notifications/INotification;
    if-eqz v7, :cond_2

    invoke-virtual {v9, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->makeNotificationCurrent(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 702
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "phone call"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 703
    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    goto/16 :goto_0

    .line 705
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 710
    .end local v7    # "callNotif":Lcom/navdy/hud/app/framework/notifications/INotification;
    .end local v9    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :sswitch_10
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handleGlances()V

    goto/16 :goto_0

    .line 726
    :sswitch_11
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dash-options"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 727
    const-string v0, "dash-options"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 728
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    if-nez v0, :cond_3

    .line 729
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .line 731
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;->DASH:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->setMode(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;)V

    .line 732
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    .line 733
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectionId:I

    .line 734
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 739
    :sswitch_12
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "map options"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 740
    const-string v0, "hybrid_map-options"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 741
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    if-nez v0, :cond_4

    .line 742
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .line 744
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;->MAP:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->setMode(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;)V

    .line 745
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    .line 746
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectionId:I

    .line 747
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->mainOptionsMenu:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 573
    :sswitch_data_0
    .sparse-switch
        0x7f0e0014 -> :sswitch_9
        0x7f0e0016 -> :sswitch_a
        0x7f0e001e -> :sswitch_2
        0x7f0e0020 -> :sswitch_1
        0x7f0e0022 -> :sswitch_4
        0x7f0e0039 -> :sswitch_8
        0x7f0e003c -> :sswitch_6
        0x7f0e003d -> :sswitch_7
        0x7f0e003f -> :sswitch_0
        0x7f0e0045 -> :sswitch_5
        0x7f0e0046 -> :sswitch_3
    .end sparse-switch

    .line 666
    :sswitch_data_1
    .sparse-switch
        0x7f0e0015 -> :sswitch_b
        0x7f0e0018 -> :sswitch_f
        0x7f0e001e -> :sswitch_10
        0x7f0e0021 -> :sswitch_12
        0x7f0e0028 -> :sswitch_c
        0x7f0e0040 -> :sswitch_11
        0x7f0e007e -> :sswitch_d
        0x7f0e0083 -> :sswitch_e
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 447
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 444
    return-void
.end method

.method public setSelectedIcon()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v13, -0x1

    .line 451
    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    if-eq v2, v13, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentSelection()I

    move-result v2

    if-nez v2, :cond_0

    .line 453
    iget v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectionId:I

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getToolTipString(I)Ljava/lang/String;

    move-result-object v12

    .line 454
    .local v12, "toolTip":Ljava/lang/String;
    if-eqz v12, :cond_0

    .line 455
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    invoke-virtual {v2, v3, v12}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showToolTip(ILjava/lang/String;)V

    .line 458
    .end local v12    # "toolTip":Ljava/lang/String;
    :cond_0
    iput v13, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelection:I

    .line 459
    iput v13, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTraySelectionId:I

    .line 461
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 462
    .local v10, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0b01f4

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 463
    .local v11, "size":I
    const v2, 0x7f0d005b

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 464
    .local v9, "gradientStart":I
    const v2, 0x7f0d005a

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 465
    .local v8, "gradientEnd":I
    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v3, v11

    int-to-float v4, v11

    const/4 v2, 0x2

    new-array v5, v2, [I

    const/4 v2, 0x0

    aput v9, v5, v2

    const/4 v2, 0x1

    aput v8, v5, v2

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 466
    .local v0, "gradient":Landroid/graphics/LinearGradient;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v2, 0x7f020182

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v13, v0, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 467
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    const v2, 0x7f0901a7

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 468
    return-void
.end method

.method public showToolTip()V
    .locals 4

    .prologue
    .line 548
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentSubSelectionId()I

    move-result v0

    .line 549
    .local v0, "subId":I
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentSubSelectionPos()I

    move-result v1

    .line 550
    .local v1, "subPos":I
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getToolTipString(I)Ljava/lang/String;

    move-result-object v2

    .line 551
    .local v2, "toolTipStr":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 552
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->stopActivityTrayRunnable()V

    .line 553
    const v3, 0x7f0e0018

    if-ne v0, v3, :cond_0

    .line 554
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->startActivityTrayRunnable(II)V

    .line 556
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v3, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showToolTip(ILjava/lang/String;)V

    .line 562
    :goto_0
    return-void

    .line 559
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->stopActivityTrayRunnable()V

    .line 560
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->hideToolTip()V

    goto :goto_0
.end method
