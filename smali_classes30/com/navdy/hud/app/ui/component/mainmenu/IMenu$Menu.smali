.class public final enum Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
.super Ljava/lang/Enum;
.source "IMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Menu"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum ACTIVE_TRIP:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum CONTACT_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum MAIN_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum MESSAGE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum NUMBER_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum RECENT_CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum RECENT_PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum REPORT_ISSUE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum SEARCH:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum SETTINGS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum SYSTEM_INFO:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

.field public static final enum TEST_FAST_SCROLL_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "MAIN"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 17
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "SETTINGS"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SETTINGS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 18
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "PLACES"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 19
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "RECENT_PLACES"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->RECENT_PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 20
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "CONTACTS"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 21
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "RECENT_CONTACTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->RECENT_CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "CONTACT_OPTIONS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACT_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 23
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "MAIN_OPTIONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 24
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "MUSIC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 25
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "SEARCH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SEARCH:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "REPORT_ISSUE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->REPORT_ISSUE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 27
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "ACTIVE_TRIP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ACTIVE_TRIP:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 28
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "SYSTEM_INFO"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SYSTEM_INFO:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 29
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "TEST_FAST_SCROLL_MENU"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->TEST_FAST_SCROLL_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 30
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "NUMBER_PICKER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->NUMBER_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 31
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    const-string v1, "MESSAGE_PICKER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MESSAGE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    .line 15
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SETTINGS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->RECENT_PLACES:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->RECENT_CONTACTS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->CONTACT_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SEARCH:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->REPORT_ISSUE:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->ACTIVE_TRIP:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SYSTEM_INFO:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->TEST_FAST_SCROLL_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->NUMBER_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MESSAGE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->$VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->$VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method
