.class final Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;
.super Ljava/lang/Object;
.source "DashGaugeConfiguratorMenu.kt"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;-><init>(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "identifier",
        "",
        "kotlin.jvm.PlatformType",
        "filter"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;->INSTANCE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final filter(Ljava/lang/String;)Z
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61
    if-nez p1, :cond_2

    :cond_0
    :goto_0
    move v0, v1

    :cond_1
    :goto_1
    return v0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "EMPTY_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :sswitch_1
    const-string v2, "TRAFFIC_INCIDENT_GAUGE_ID"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_1

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x30660196 -> :sswitch_0
        0x6f9cbd2c -> :sswitch_1
    .end sparse-switch
.end method
