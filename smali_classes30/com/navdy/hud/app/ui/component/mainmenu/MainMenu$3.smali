.class Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;
.super Ljava/lang/Object;
.source "MainMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAlbumArtUpdate(Lokio/ByteString;Z)V
    .locals 0
    .param p1, "artwork"    # Lokio/ByteString;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "animate"    # Z

    .prologue
    .line 257
    return-void
.end method

.method public onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V
    .locals 2
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .param p3, "willOpenNotification"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p2, "currentControls":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/hud/app/manager/MusicManager$MediaControl;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$800(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->isSameTrack(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    invoke-static {v0, v1, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$900(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Lcom/navdy/service/library/events/audio/MusicTrackInfo;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->canShowTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    invoke-static {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$1000(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$200()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
