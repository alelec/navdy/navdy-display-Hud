.class Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;
.super Ljava/lang/Object;
.source "PlacesMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->launchDestination(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

.field final synthetic val$destination:Lcom/navdy/hud/app/framework/destinations/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;Lcom/navdy/hud/app/framework/destinations/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    .prologue
    .line 488
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;->val$destination:Lcom/navdy/hud/app/framework/destinations/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 491
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->access$000(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getConfirmationLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    .line 492
    .local v0, "confirmationLayout":Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    if-nez v0, :cond_0

    .line 493
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "confirmation layout not found"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 514
    :goto_0
    return-void

    .line 496
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 499
    :pswitch_0
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "called requestNavigation"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 500
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->access$000(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->close(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 509
    :pswitch_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 510
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->access$000(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->reset()V

    .line 511
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock()V

    goto :goto_0

    .line 496
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 517
    return-void
.end method
