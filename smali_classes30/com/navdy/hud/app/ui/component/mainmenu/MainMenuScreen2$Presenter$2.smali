.class Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;
.super Ljava/lang/Object;
.source "MainMenuScreen2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;IIZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field final synthetic val$hasScrollModel:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iput-boolean p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->val$hasScrollModel:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 357
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getView()Ljava/lang/Object;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$100(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;

    .line 358
    .local v6, "view":Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;
    if-eqz v6, :cond_0

    .line 359
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performEnterAnimation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v2

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 360
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->unlock(Z)V

    .line 361
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v1

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->isBindCallsEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setBindCallbacks(Z)V

    .line 362
    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuView2;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v1

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getItems()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v2

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getInitialSelection()I

    move-result v2

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->val$hasScrollModel:Z

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->currentMenu:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v5

    invoke-interface {v5}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V

    .line 364
    :cond_0
    return-void
.end method
