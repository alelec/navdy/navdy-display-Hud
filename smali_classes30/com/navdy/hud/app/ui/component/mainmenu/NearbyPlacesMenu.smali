.class Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;
.super Ljava/lang/Object;
.source "NearbyPlacesMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# static fields
.field private static final atm:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final coffee:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final food:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final gas:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final groceryStore:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final parking:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final resources:Landroid/content/res/Resources;

.field private static final search:Ljava/lang/String;

.field private static final searchColor:I


# instance fields
.field private backSelection:I

.field private backSelectionId:I

.field private final bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field private final parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private final presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private final vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    .line 49
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    .line 50
    .local v7, "context":Landroid/content/Context;
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    .line 52
    const v0, 0x7f0d0073

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->searchColor:I

    .line 53
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->search:Ljava/lang/String;

    .line 59
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "title":Ljava/lang/String;
    const v0, 0x7f0d0054

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 61
    .local v2, "fluctuatorColor":I
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 64
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090091

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 65
    const v0, 0x7f0d0077

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 66
    const v0, 0x7f0e0074

    const v1, 0x7f0201b6

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->gas:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 69
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090094

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 70
    const v0, 0x7f0d007a

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 71
    const v0, 0x7f0e0077

    const v1, 0x7f0201b9

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->parking:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 74
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090090

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 75
    const v0, 0x7f0d0076

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 76
    const v0, 0x7f0e0073

    const v1, 0x7f0201bb

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->food:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 79
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090092

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 80
    const v0, 0x7f0d0078

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 81
    const v0, 0x7f0e0075

    const v1, 0x7f0201bc

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->groceryStore:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 84
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09008f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 85
    const v0, 0x7f0d0075

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 86
    const v0, 0x7f0e0072

    const v1, 0x7f0201b4

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->coffee:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 89
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09008e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 90
    const v0, 0x7f0d0074

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 91
    const v0, 0x7f0e0071

    const v1, 0x7f0201b1

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->atm:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 92
    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->bus:Lcom/squareup/otto/Bus;

    .line 109
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 110
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 111
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 113
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;)Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    return-object v0
.end method

.method private launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V
    .locals 2
    .param p1, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 207
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Here maps engine not initialized, exit"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    .line 232
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;Lcom/navdy/service/library/events/places/PlaceType;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 263
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->cachedList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->cachedList:Ljava/util/List;

    .line 133
    :goto_0
    return-object v0

    .line 122
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->gas:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->parking:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->food:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->groceryStore:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->coffee:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->atm:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->cachedList:Ljava/util/List;

    goto :goto_0
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 246
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 247
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 249
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 236
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->SEARCH:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 241
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 259
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 270
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 273
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 0
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 267
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 5
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 164
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 166
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 201
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 168
    :sswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "back"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchReturnMainMenu()V

    .line 170
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->backSelection:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->backSelectionId:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 173
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "gas"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 174
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V

    goto :goto_0

    .line 177
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "parking"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V

    goto :goto_0

    .line 181
    :sswitch_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "food"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 182
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V

    goto :goto_0

    .line 185
    :sswitch_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "grocery store"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 186
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V

    goto :goto_0

    .line 189
    :sswitch_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "coffee"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 190
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V

    goto :goto_0

    .line 193
    :sswitch_6
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "atm"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 194
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V

    goto :goto_0

    .line 197
    :sswitch_7
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "hospital"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 198
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->launchSearch(Lcom/navdy/service/library/events/places/PlaceType;)V

    goto :goto_0

    .line 166
    :sswitch_data_0
    .sparse-switch
        0x7f0e0047 -> :sswitch_0
        0x7f0e0071 -> :sswitch_6
        0x7f0e0072 -> :sswitch_5
        0x7f0e0073 -> :sswitch_3
        0x7f0e0074 -> :sswitch_1
        0x7f0e0075 -> :sswitch_4
        0x7f0e0076 -> :sswitch_7
        0x7f0e0077 -> :sswitch_2
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->backSelectionId:I

    .line 154
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->backSelection:I

    .line 149
    return-void
.end method

.method public setSelectedIcon()V
    .locals 5

    .prologue
    .line 158
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v1, 0x7f020186

    sget v2, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->searchColor:I

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 159
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->search:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method
