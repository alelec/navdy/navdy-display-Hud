.class Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;
.super Ljava/lang/Object;
.source "MusicMenu2.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BusDelegate"
.end annotation


# instance fields
.field presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;)V
    .locals 0
    .param p1, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 176
    return-void
.end method


# virtual methods
.method public onAlbumArtUpdate(Lokio/ByteString;Z)V
    .locals 0
    .param p1, "artwork"    # Lokio/ByteString;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "animate"    # Z

    .prologue
    .line 195
    return-void
.end method

.method public onMusicArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
    .locals 3
    .param p1, "musicArtworkResponse"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 188
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 189
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v1, v2, :cond_0

    .line 190
    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .end local v0    # "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onMusicArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
    invoke-static {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V

    .line 192
    :cond_0
    return-void
.end method

.method public onMusicCollectionResponse(Lcom/navdy/service/library/events/audio/MusicCollectionResponse;)V
    .locals 3
    .param p1, "musicCollectionResponse"    # Lcom/navdy/service/library/events/audio/MusicCollectionResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 180
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 181
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v1, v2, :cond_0

    .line 182
    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .end local v0    # "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onMusicCollectionResponse(Lcom/navdy/service/library/events/audio/MusicCollectionResponse;)V
    invoke-static {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$100(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicCollectionResponse;)V

    .line 184
    :cond_0
    return-void
.end method

.method public onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V
    .locals 3
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .param p3, "willOpenNotification"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 199
    .local p2, "currentControls":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/hud/app/manager/MusicManager$MediaControl;>;"
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2$BusDelegate;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->getCurrentMenu()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    move-result-object v0

    .line 200
    .local v0, "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;->getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    if-ne v1, v2, :cond_0

    .line 201
    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;

    .end local v0    # "menu":Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    invoke-static {v0, p1}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->access$300(Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 203
    :cond_0
    return-void
.end method
