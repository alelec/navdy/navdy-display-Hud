.class public Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;
.super Ljava/lang/Object;
.source "MainOptionsMenu.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;
    }
.end annotation


# static fields
.field private static final autoZoom:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final dashColor:I

.field private static final dashTitle:Ljava/lang/String;

.field private static final manualZoom:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final mapColor:I

.field private static final mapTitle:Ljava/lang/String;

.field private static final pauseDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final playDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final rawGps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final resources:Landroid/content/res/Resources;

.field private static final restartDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final scrollLeftGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final scrollRightGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final selectCenterGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final sideGauges:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;


# instance fields
.field private backSelection:I

.field private bus:Lcom/squareup/otto/Bus;

.field private cachedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private dashGaugeConfiguratorMenu:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

.field private driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

.field private driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field private mode:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;

.field private parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

.field private presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

.field private vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 69
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    .line 70
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 71
    .local v2, "backColor":I
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashColor:I

    .line 72
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->mapColor:I

    .line 77
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashTitle:Ljava/lang/String;

    .line 78
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->mapTitle:Ljava/lang/String;

    .line 81
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 82
    .local v5, "title":Ljava/lang/String;
    const v0, 0x7f0e0047

    const v1, 0x7f02016e

    sget v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/4 v6, 0x0

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 85
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 86
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 87
    .local v8, "fluctuatorColor":I
    const v6, 0x7f0e002b

    const v7, 0x7f02019f

    sget v9, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/4 v12, 0x0

    move v10, v8

    move-object v11, v5

    invoke-static/range {v6 .. v12}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->manualZoom:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 89
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09033a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 90
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 91
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils;->SHOW_RAW_GPS:Lcom/navdy/hud/app/config/BooleanSetting;

    invoke-virtual {v0}, Lcom/navdy/hud/app/config/BooleanSetting;->isEnabled()Z

    move-result v11

    .line 92
    .local v11, "isOn":Z
    sget-object v6, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->Companion:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

    const v7, 0x7f0e002f

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    if-eqz v11, :cond_0

    const v0, 0x7f090346

    .line 93
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x1

    move-object v9, v5

    .line 92
    invoke-virtual/range {v6 .. v12}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;->buildModel(IILjava/lang/String;Ljava/lang/String;ZZ)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->rawGps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 96
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 97
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 98
    const v12, 0x7f0e0029

    const v13, 0x7f02019e

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->autoZoom:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 101
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090205

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 102
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 103
    const v12, 0x7f0e002e

    const v13, 0x7f020176

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->playDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 106
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 107
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 108
    const v12, 0x7f0e002d

    const v13, 0x7f020175

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->pauseDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 111
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09022a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 112
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 113
    const v12, 0x7f0e0031

    const v13, 0x7f020177

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->restartDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 116
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090252

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 117
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 118
    const v12, 0x7f0e0032

    const v13, 0x7f02019a

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->scrollLeftGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 121
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090253

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 122
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 123
    const v12, 0x7f0e0033

    const v13, 0x7f02019b

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->scrollRightGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 126
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 127
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 128
    const v12, 0x7f0e0034

    const v13, 0x7f0200f5

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->selectCenterGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 131
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 132
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 133
    const v12, 0x7f0e0035

    const v13, 0x7f0201e2

    sget v15, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->bkColorUnselected:I

    const/16 v18, 0x0

    move v14, v8

    move/from16 v16, v8

    move-object/from16 v17, v5

    invoke-static/range {v12 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->sideGauges:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 134
    return-void

    .line 92
    :cond_0
    const v0, 0x7f090343

    goto/16 :goto_0
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
    .locals 7
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "vscrollComponent"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p3, "presenter"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .param p4, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->bus:Lcom/squareup/otto/Bus;

    .line 154
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 155
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    .line 156
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    .line 157
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v6

    .line 158
    .local v6, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getDriveRecorder()Lcom/navdy/hud/app/debug/DriveRecorder;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    .line 159
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 160
    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 161
    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 162
    .local v2, "sharedPreferences":Landroid/content/SharedPreferences;
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;-><init>(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashGaugeConfiguratorMenu:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/profile/DriverProfileManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/debug/DriveRecorder;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-object v0
.end method


# virtual methods
.method public getChildMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .locals 1
    .param p1, "parent"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;
    .param p2, "args"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 411
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInitialSelection()I
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x1

    return v0
.end method

.method public getItems()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$7;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode:[I

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->mode:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 222
    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->cachedList:Ljava/util/List;

    .line 223
    return-object v2

    .line 176
    :pswitch_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 178
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v3

    .line 179
    .local v3, "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    iget-object v6, v3, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    if-eqz v6, :cond_2

    iget-object v6, v3, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 180
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->autoZoom:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v6

    if-nez v6, :cond_1

    .line 185
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->rawGps:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->isDemoAvailable()Z

    move-result v0

    .line 190
    .local v0, "isDemoAvailable":Z
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    invoke-virtual {v6}, Lcom/navdy/hud/app/debug/DriveRecorder;->isDemoPlaying()Z

    move-result v1

    .line 191
    .local v1, "isPlaying":Z
    if-eqz v0, :cond_0

    .line 192
    if-nez v1, :cond_3

    .line 193
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->playDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 182
    .end local v0    # "isDemoAvailable":Z
    .end local v1    # "isPlaying":Z
    :cond_2
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->manualZoom:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 195
    .restart local v0    # "isDemoAvailable":Z
    .restart local v1    # "isPlaying":Z
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->pauseDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->restartDemo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    .end local v0    # "isDemoAvailable":Z
    .end local v1    # "isPlaying":Z
    .end local v3    # "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    :pswitch_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getSmartDashView()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    move-result-object v5

    .line 205
    .local v5, "smartDashView":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    if-eqz v5, :cond_4

    .line 206
    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getCurrentScrollableSideOption()I

    move-result v4

    .line 207
    .local v4, "scrollOption":I
    packed-switch v4, :pswitch_data_1

    .line 217
    .end local v4    # "scrollOption":I
    :cond_4
    :goto_2
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->selectCenterGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->sideGauges:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 209
    .restart local v4    # "scrollOption":I
    :pswitch_2
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->scrollLeftGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 213
    :pswitch_3
    sget-object v6, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->scrollRightGauge:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 207
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getModelfromPos(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 394
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->cachedList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 395
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->cachedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 397
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;
    .locals 1

    .prologue
    .line 384
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MAIN_OPTIONS:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    return-object v0
.end method

.method public isBindCallsEnabled()Z
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method public isFirstItemEmpty()Z
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x1

    return v0
.end method

.method public isItemClickable(II)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I

    .prologue
    .line 389
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 407
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 424
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 418
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 421
    return-void
.end method

.method public onUnload(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;)V
    .locals 0
    .param p1, "level"    # Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    .prologue
    .line 415
    return-void
.end method

.method public selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 7
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 269
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "select id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " pos:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 271
    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v3, :sswitch_data_0

    :goto_0
    move v2, v0

    .line 379
    :goto_1
    return v2

    .line 273
    :sswitch_0
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "back"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 274
    const-string v3, "back"

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 275
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->parent:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->BACK_TO_PARENT:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v6, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->backSelection:I

    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto :goto_0

    .line 279
    :sswitch_1
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Toggling raw gps"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 280
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 281
    .local v1, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-boolean v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    if-nez v3, :cond_0

    .line 282
    .local v0, "enabled":Z
    :goto_2
    iput-boolean v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    .line 283
    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->resources:Landroid/content/res/Resources;

    if-eqz v0, :cond_1

    const v3, 0x7f090346

    :goto_3
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 284
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsUtils;->SHOW_RAW_GPS:Lcom/navdy/hud/app/config/BooleanSetting;

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/config/BooleanSetting;->setEnabled(Z)V

    .line 285
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->refreshDataforPos(I)V

    goto :goto_1

    .end local v0    # "enabled":Z
    :cond_0
    move v0, v2

    .line 281
    goto :goto_2

    .line 283
    .restart local v0    # "enabled":Z
    :cond_1
    const v3, 0x7f090343

    goto :goto_3

    .line 289
    .end local v0    # "enabled":Z
    .end local v1    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :sswitch_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$1;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 305
    :sswitch_3
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$2;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$2;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 321
    :sswitch_4
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$3;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 335
    :sswitch_5
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$4;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$4;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 345
    :sswitch_6
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$5;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$5;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 356
    :sswitch_7
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    new-instance v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$6;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$6;-><init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->performSelectionAnimation(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 369
    :sswitch_8
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashGaugeConfiguratorMenu:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->setGaugeType(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V

    .line 370
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashGaugeConfiguratorMenu:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 373
    :sswitch_9
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashGaugeConfiguratorMenu:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    sget-object v4, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->setGaugeType(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V

    .line 374
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashGaugeConfiguratorMenu:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    sget-object v5, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;->SUB_LEVEL:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->loadMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;II)V

    goto/16 :goto_0

    .line 271
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e0029 -> :sswitch_3
        0x7f0e002b -> :sswitch_2
        0x7f0e002d -> :sswitch_5
        0x7f0e002e -> :sswitch_4
        0x7f0e002f -> :sswitch_1
        0x7f0e0031 -> :sswitch_6
        0x7f0e0032 -> :sswitch_7
        0x7f0e0033 -> :sswitch_7
        0x7f0e0034 -> :sswitch_8
        0x7f0e0035 -> :sswitch_9
        0x7f0e0047 -> :sswitch_0
    .end sparse-switch
.end method

.method public setBackSelectionId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 242
    return-void
.end method

.method public setBackSelectionPos(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 238
    iput p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->backSelection:I

    .line 239
    return-void
.end method

.method setMode(Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->mode:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;

    .line 167
    return-void
.end method

.method public setSelectedIcon()V
    .locals 6

    .prologue
    .line 246
    const/4 v1, 0x0

    .line 247
    .local v1, "icon":I
    const/4 v0, 0x0

    .line 248
    .local v0, "color":I
    const/4 v2, 0x0

    .line 249
    .local v2, "title":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$7;->$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode:[I

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->mode:Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu$Mode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 263
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v3, v1, v0, v4, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 264
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->vscrollComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    return-void

    .line 251
    :pswitch_0
    const v1, 0x7f020169

    .line 252
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->mapColor:I

    .line 253
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->mapTitle:Ljava/lang/String;

    .line 254
    goto :goto_0

    .line 257
    :pswitch_1
    const v1, 0x7f020168

    .line 258
    sget v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashColor:I

    .line 259
    sget-object v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainOptionsMenu;->dashTitle:Ljava/lang/String;

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 432
    return-void
.end method
