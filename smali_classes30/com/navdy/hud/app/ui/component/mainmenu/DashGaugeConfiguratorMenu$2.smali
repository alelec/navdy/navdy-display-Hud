.class public final Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;
.super Ljava/lang/Object;
.source "DashGaugeConfiguratorMenu.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;-><init>(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "com/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2",
        "",
        "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)V",
        "onReload",
        "",
        "reload",
        "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)V
    .locals 0
    .param p1, "$outer"    # Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 71
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReload(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;)V
    .locals 3
    .param p1, "reload"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-string v0, "reload"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 82
    :goto_0
    return-void

    .line 76
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getPresenter$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->updateCurrentMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    goto :goto_0

    .line 79
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->getSmartDashWidgetManager()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->buildSmartDashWidgetCache(I)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->setSmartDashWidgetCache(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;)V

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;->access$getPresenter$p(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->updateCurrentMenu(Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
