.class public interface abstract Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;
.super Ljava/lang/Object;
.source "NumberPickerMenu.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;",
        "",
        "selected",
        "",
        "contact",
        "Lcom/navdy/hud/app/framework/contacts/Contact;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# virtual methods
.method public abstract selected(Lcom/navdy/hud/app/framework/contacts/Contact;)V
    .param p1    # Lcom/navdy/hud/app/framework/contacts/Contact;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method
