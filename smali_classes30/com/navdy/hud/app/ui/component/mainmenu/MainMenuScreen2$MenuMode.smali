.class public final enum Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;
.super Ljava/lang/Enum;
.source "MainMenuScreen2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MenuMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

.field public static final enum MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

.field public static final enum REPLY_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

.field public static final enum SNAPSHOT_TITLE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    const-string v1, "MAIN_MENU"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    .line 64
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    const-string v1, "REPLY_PICKER"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->REPLY_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    .line 65
    new-instance v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    const-string v1, "SNAPSHOT_TITLE_PICKER"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->SNAPSHOT_TITLE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->MAIN_MENU:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->REPLY_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->SNAPSHOT_TITLE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->$VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 62
    const-class v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->$VALUES:[Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    return-object v0
.end method
