.class public final Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;
.super Ljava/lang/Object;
.source "MessagePickerMenu.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\u00080\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u0006R\u001c\u0010\u000e\u001a\n \t*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0006R\u0014\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0006R\u001c\u0010\u001a\u001a\n \t*\u0004\u0018\u00010\u001b0\u001bX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;",
        "",
        "()V",
        "SELECTION_ANIMATION_DELAY",
        "",
        "getSELECTION_ANIMATION_DELAY",
        "()I",
        "back",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "kotlin.jvm.PlatformType",
        "getBack",
        "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "baseMessageId",
        "getBaseMessageId",
        "context",
        "Landroid/content/Context;",
        "getContext",
        "()Landroid/content/Context;",
        "fluctuatorColor",
        "getFluctuatorColor",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "getLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "messageColor",
        "getMessageColor",
        "resources",
        "Landroid/content/res/Resources;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getBack$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getBaseMessageId$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getBaseMessageId()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getContext$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Landroid/content/Context;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getFluctuatorColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getFluctuatorColor()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getLogger$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getMessageColor$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getMessageColor()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getResources$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSELECTION_ANIMATION_DELAY$p(Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu$Companion;->getSELECTION_ANIMATION_DELAY()I

    move-result v0

    return v0
.end method

.method private final getBack()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1

    .prologue
    .line 34
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->back:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getBack$cp()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method private final getBaseMessageId()I
    .locals 1

    .prologue
    .line 35
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->baseMessageId:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getBaseMessageId$cp()I

    move-result v0

    return v0
.end method

.method private final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getContext$cp()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private final getFluctuatorColor()I
    .locals 1

    .prologue
    .line 32
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->fluctuatorColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getFluctuatorColor$cp()I

    move-result v0

    return v0
.end method

.method private final getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 28
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getMessageColor()I
    .locals 1

    .prologue
    .line 33
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->messageColor:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getMessageColor$cp()I

    move-result v0

    return v0
.end method

.method private final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 31
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->resources:Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getResources$cp()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private final getSELECTION_ANIMATION_DELAY()I
    .locals 1

    .prologue
    .line 29
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->SELECTION_ANIMATION_DELAY:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;->access$getSELECTION_ANIMATION_DELAY$cp()I

    move-result v0

    return v0
.end method
