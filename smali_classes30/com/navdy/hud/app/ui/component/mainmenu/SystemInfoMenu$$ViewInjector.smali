.class public Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu$$ViewInjector;
.super Ljava/lang/Object;
.source "SystemInfoMenu$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e020b

    const-string v2, "field \'displayFw\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayFw:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e020c

    const-string v2, "field \'dialFw\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialFw:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e020d

    const-string v2, "field \'obdFw\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdFw:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f0e020e

    const-string v2, "field \'dialBt\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialBt:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f0e020f

    const-string v2, "field \'displayBt\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayBt:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e01f1

    const-string v2, "field \'gps\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gps:Landroid/widget/TextView;

    .line 22
    const v1, 0x7f0e01f2

    const-string v2, "field \'satellites\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satellites:Landroid/widget/TextView;

    .line 24
    const v1, 0x7f0e01f3

    const-string v2, "field \'satelliteView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satelliteView:Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    .line 26
    const v1, 0x7f0e01f4

    const-string v2, "field \'fix\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fix:Landroid/widget/TextView;

    .line 28
    const v1, 0x7f0e01f5

    const-string v2, "field \'temperature\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->temperature:Landroid/widget/TextView;

    .line 30
    const v1, 0x7f0e01f6

    const-string v2, "field \'deviceMileage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->deviceMileage:Landroid/widget/TextView;

    .line 32
    const v1, 0x7f0e01f7

    const-string v2, "field \'gpsSpeed\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gpsSpeed:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f0e01e9

    const-string v2, "field \'hereSdk\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 35
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereSdk:Landroid/widget/TextView;

    .line 36
    const v1, 0x7f0e01ea

    const-string v2, "field \'hereUpdated\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 37
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereUpdated:Landroid/widget/TextView;

    .line 38
    const v1, 0x7f0e01eb

    const-string v2, "field \'hereMapsVersion\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapsVersion:Landroid/widget/TextView;

    .line 40
    const v1, 0x7f0e01ec

    const-string v2, "field \'hereOfflineMaps\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereOfflineMaps:Landroid/widget/TextView;

    .line 42
    const v1, 0x7f0e01ed

    const-string v2, "field \'hereMapVerified\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 43
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapVerified:Landroid/widget/TextView;

    .line 44
    const v1, 0x7f0e01ee

    const-string v2, "field \'hereVoiceVerified\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 45
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereVoiceVerified:Landroid/widget/TextView;

    .line 46
    const v1, 0x7f0e01f8

    const-string v2, "field \'make\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 47
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->make:Landroid/widget/TextView;

    .line 48
    const v1, 0x7f0e01f9

    const-string v2, "field \'carModel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 49
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->carModel:Landroid/widget/TextView;

    .line 50
    const v1, 0x7f0e01fa

    const-string v2, "field \'year\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 51
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->year:Landroid/widget/TextView;

    .line 52
    const v1, 0x7f0e01fc

    const-string v2, "field \'fuel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 53
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fuel:Landroid/widget/TextView;

    .line 54
    const v1, 0x7f0e01fd

    const-string v2, "field \'rpm\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 55
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->rpm:Landroid/widget/TextView;

    .line 56
    const v1, 0x7f0e01fe

    const-string v2, "field \'speed\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 57
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speed:Landroid/widget/TextView;

    .line 58
    const v1, 0x7f0e0200

    const-string v2, "field \'odo\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 59
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odo:Landroid/widget/TextView;

    .line 60
    const v1, 0x7f0e0201

    const-string v2, "field \'maf\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 61
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->maf:Landroid/widget/TextView;

    .line 62
    const v1, 0x7f0e0202

    const-string v2, "field \'engineTemp\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 63
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTemp:Landroid/widget/TextView;

    .line 64
    const v1, 0x7f0e0203

    const-string v2, "field \'voltage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 65
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->voltage:Landroid/widget/TextView;

    .line 66
    const v1, 0x7f0e01e5

    const-string v2, "field \'gestures\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 67
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gestures:Landroid/widget/TextView;

    .line 68
    const v1, 0x7f0e01e6

    const-string v2, "field \'autoOn\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 69
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->autoOn:Landroid/widget/TextView;

    .line 70
    const v1, 0x7f0e01e7

    const-string v2, "field \'obdData\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 71
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdData:Landroid/widget/TextView;

    .line 72
    const v1, 0x7f0e01e8

    const-string v2, "field \'uiScaling\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 73
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->uiScaling:Landroid/widget/TextView;

    .line 74
    const v1, 0x7f0e01ef

    const-string v2, "field \'routeCalc\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 75
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routeCalc:Landroid/widget/TextView;

    .line 76
    const v1, 0x7f0e01f0

    const-string v2, "field \'routePref\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 77
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routePref:Landroid/widget/TextView;

    .line 78
    const v1, 0x7f0e0207

    const-string v2, "field \'engineOilPressureLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 79
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineOilPressureLayout:Landroid/view/ViewGroup;

    .line 80
    const v1, 0x7f0e0208

    const-string v2, "field \'engineOilPressure\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 81
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineOilPressure:Landroid/widget/TextView;

    .line 82
    const v1, 0x7f0e0206

    const-string v2, "field \'specialPidsLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 83
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->specialPidsLayout:Landroid/view/ViewGroup;

    .line 84
    const v1, 0x7f0e020a

    const-string v2, "field \'tripFuel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 85
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->tripFuel:Landroid/widget/TextView;

    .line 86
    const v1, 0x7f0e0209

    const-string v2, "field \'tripFuelLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 87
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->tripFuelLayout:Landroid/view/ViewGroup;

    .line 88
    const v1, 0x7f0e0204

    const-string v2, "field \'checkEngineLight\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 89
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->checkEngineLight:Landroid/widget/TextView;

    .line 90
    const v1, 0x7f0e0205

    const-string v2, "field \'engineTroubleCodes\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 91
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTroubleCodes:Landroid/widget/TextView;

    .line 92
    const v1, 0x7f0e01ff

    const-string v2, "field \'odometerLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 93
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odometerLayout:Landroid/view/ViewGroup;

    .line 94
    const v1, 0x7f0e01fb

    const-string v2, "field \'obdDataLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 95
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdDataLayout:Landroid/view/ViewGroup;

    .line 96
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;

    .prologue
    const/4 v0, 0x0

    .line 99
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayFw:Landroid/widget/TextView;

    .line 100
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialFw:Landroid/widget/TextView;

    .line 101
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdFw:Landroid/widget/TextView;

    .line 102
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->dialBt:Landroid/widget/TextView;

    .line 103
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->displayBt:Landroid/widget/TextView;

    .line 104
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gps:Landroid/widget/TextView;

    .line 105
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satellites:Landroid/widget/TextView;

    .line 106
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->satelliteView:Lcom/navdy/hud/app/ui/component/systeminfo/GpsSignalView;

    .line 107
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fix:Landroid/widget/TextView;

    .line 108
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->temperature:Landroid/widget/TextView;

    .line 109
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->deviceMileage:Landroid/widget/TextView;

    .line 110
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gpsSpeed:Landroid/widget/TextView;

    .line 111
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereSdk:Landroid/widget/TextView;

    .line 112
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereUpdated:Landroid/widget/TextView;

    .line 113
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapsVersion:Landroid/widget/TextView;

    .line 114
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereOfflineMaps:Landroid/widget/TextView;

    .line 115
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereMapVerified:Landroid/widget/TextView;

    .line 116
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->hereVoiceVerified:Landroid/widget/TextView;

    .line 117
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->make:Landroid/widget/TextView;

    .line 118
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->carModel:Landroid/widget/TextView;

    .line 119
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->year:Landroid/widget/TextView;

    .line 120
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->fuel:Landroid/widget/TextView;

    .line 121
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->rpm:Landroid/widget/TextView;

    .line 122
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->speed:Landroid/widget/TextView;

    .line 123
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odo:Landroid/widget/TextView;

    .line 124
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->maf:Landroid/widget/TextView;

    .line 125
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTemp:Landroid/widget/TextView;

    .line 126
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->voltage:Landroid/widget/TextView;

    .line 127
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->gestures:Landroid/widget/TextView;

    .line 128
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->autoOn:Landroid/widget/TextView;

    .line 129
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdData:Landroid/widget/TextView;

    .line 130
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->uiScaling:Landroid/widget/TextView;

    .line 131
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routeCalc:Landroid/widget/TextView;

    .line 132
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->routePref:Landroid/widget/TextView;

    .line 133
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineOilPressureLayout:Landroid/view/ViewGroup;

    .line 134
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineOilPressure:Landroid/widget/TextView;

    .line 135
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->specialPidsLayout:Landroid/view/ViewGroup;

    .line 136
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->tripFuel:Landroid/widget/TextView;

    .line 137
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->tripFuelLayout:Landroid/view/ViewGroup;

    .line 138
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->checkEngineLight:Landroid/widget/TextView;

    .line 139
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->engineTroubleCodes:Landroid/widget/TextView;

    .line 140
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->odometerLayout:Landroid/view/ViewGroup;

    .line 141
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu;->obdDataLayout:Landroid/view/ViewGroup;

    .line 142
    return-void
.end method
