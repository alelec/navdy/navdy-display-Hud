.class Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;
.super Ljava/lang/Object;
.source "MainMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

.field final synthetic val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 270
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    # setter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->addedTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$802(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 271
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipId:I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$1100(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)I

    move-result v1

    const v2, 0x7f0e0028

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$400(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 272
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$1200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "update music tool tip"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 273
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getMusicTrackInfo()Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$1300(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 275
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$600(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$3;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$400(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showToolTip(ILjava/lang/String;)V

    .line 278
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    return-void
.end method
