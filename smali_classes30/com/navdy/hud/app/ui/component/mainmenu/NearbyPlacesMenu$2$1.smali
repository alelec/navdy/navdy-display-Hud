.class Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2$1;
.super Ljava/lang/Object;
.source "NearbyPlacesMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 225
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "addNotification for quick search"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 226
    new-instance v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;->val$placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;-><init>(Lcom/navdy/service/library/events/places/PlaceType;)V

    .line 227
    .local v0, "nearbyPlaceSearchNotification":Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2$1;->this$1:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;->access$200(Lcom/navdy/hud/app/ui/component/mainmenu/NearbyPlacesMenu;)Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 228
    return-void
.end method
