.class public final synthetic Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->values()[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOADED:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOAD_CACHE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->values()[Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->CENTER:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->SIDE:Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
