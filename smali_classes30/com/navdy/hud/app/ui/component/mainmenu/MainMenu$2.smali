.class Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;
.super Ljava/lang/Object;
.source "MainMenu.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 240
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunning:Z
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$300(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$400(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)I

    move-result v1

    if-gez v1, :cond_2

    .line 244
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->activityTrayRunning:Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$302(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;Z)Z

    goto :goto_0

    .line 247
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    const v2, 0x7f0e0018

    # invokes: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->getToolTipString(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$500(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;I)Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "toolTip":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 249
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->presenter:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$600(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu$2;->this$0:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->curToolTipPos:I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$400(Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;->showToolTip(ILjava/lang/String;)V

    .line 250
    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$200()Landroid/os/Handler;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->ACTIVITY_TRAY_UPDATE:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenu;->access$700()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
