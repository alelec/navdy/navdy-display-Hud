.class Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;
.super Ljava/lang/Object;
.source "CircleIndicator.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->setCurrentItem(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 254
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v1, v2, :cond_0

    .line 255
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->val$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 259
    .local v0, "pos":I
    :goto_0
    if-nez v0, :cond_1

    .line 264
    :goto_1
    return-void

    .line 257
    .end local v0    # "pos":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->val$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    .restart local v0    # "pos":I
    goto :goto_0

    .line 262
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->val$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 263
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;->val$view:Landroid/view/View;

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->setViewPos(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;Landroid/view/View;)V

    goto :goto_1
.end method
