.class Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "CarouselLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setModelInternal(Ljava/util/List;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

.field final synthetic val$newModel:Ljava/util/List;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Ljava/util/List;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->val$newModel:Ljava/util/List;

    iput p3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->val$position:I

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 390
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->val$position:I

    const/4 v2, 0x0

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setCurrentItem(IZ)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$600(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;IZ)V

    .line 391
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    .line 392
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->val$newModel:Ljava/util/List;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    .line 383
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->val$position:I

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;->val$position:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    invoke-interface {v1, v2, v3, v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onCurrentItemChanging(III)V

    .line 386
    :cond_0
    return-void
.end method
