.class public Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
.super Ljava/lang/Object;
.source "FastScrollAnimator.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;


# static fields
.field public static final ANIMATION_HERO:I = 0x32

.field public static final ANIMATION_HERO_SINGLE:I = 0x85

.field public static final ANIMATION_HERO_TEXT:I = 0xa5

.field public static final ANIMATION_HERO_TRANSLATE_X:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

.field private endPending:Z

.field private hiddenView:Landroid/view/View;

.field private lastScrollAnimationFinishTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 28
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->ANIMATION_HERO_TRANSLATE_X:I

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V
    .locals 2
    .param p1, "carouselLayout"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .line 39
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    invoke-virtual {p1, v1, v1, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildView(IILcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    .line 40
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endPending:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
    .param p1, "x1"    # J

    .prologue
    .line 18
    iput-wide p1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->lastScrollAnimationFinishTime:J

    return-wide p1
.end method


# virtual methods
.method public buildLayoutAnimation(Landroid/animation/Animator$AnimatorListener;Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;II)Landroid/animation/AnimatorSet;
    .locals 20
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p3, "currentPos"    # I
    .param p4, "newPos"    # I

    .prologue
    .line 77
    new-instance v10, Landroid/animation/AnimatorSet;

    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    .line 79
    .local v10, "animatorSet":Landroid/animation/AnimatorSet;
    move/from16 v0, p4

    move/from16 v1, p3

    if-le v0, v1, :cond_3

    const/16 v18, 0x1

    .line 80
    .local v18, "next":Z
    :goto_0
    if-eqz v18, :cond_4

    sget-object v14, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 86
    .local v14, "direction":Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    :goto_1
    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne v14, v4, :cond_5

    .line 87
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getX()F

    move-result v4

    sget v5, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->ANIMATION_HERO_TRANSLATE_X:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v19, v0

    .line 88
    .local v19, "translateX":I
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getX()F

    move-result v4

    sget v5, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->ANIMATION_HERO_TRANSLATE_X:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v16, v0

    .line 94
    .local v16, "hiddenX":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    move/from16 v0, v16

    int-to-float v5, v0

    invoke-virtual {v4, v5}, Landroid/view/View;->setX(F)V

    .line 95
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    .line 97
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->X:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    move/from16 v0, v19

    int-to-float v8, v0

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v10, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v11

    .line 98
    .local v11, "builder":Landroid/animation/AnimatorSet$Builder;
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v11, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 100
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->X:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v8

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v11, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 101
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v11, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 104
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;

    sget-object v7, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v8, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v9, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    move/from16 v5, p4

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    .line 108
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-eqz v4, :cond_0

    .line 109
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    const/4 v5, -0x1

    move/from16 v0, p4

    invoke-virtual {v4, v0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemMoveAnimator(II)Landroid/animation/AnimatorSet;

    move-result-object v17

    .line 110
    .local v17, "indicatorAnimator":Landroid/animation/AnimatorSet;
    if-eqz v17, :cond_0

    .line 111
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 115
    .end local v17    # "indicatorAnimator":Landroid/animation/AnimatorSet;
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v10, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 118
    new-instance v4, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-direct {v4, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;-><init>(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Landroid/animation/Animator$AnimatorListener;I)V

    invoke-virtual {v10, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 180
    const/16 v15, 0x32

    .line 181
    .local v15, "duration":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->isAnimationPending()Z

    move-result v4

    if-nez v4, :cond_1

    .line 182
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->lastScrollAnimationFinishTime:J

    sub-long v12, v4, v6

    .line 183
    .local v12, "diff":J
    const-wide/16 v4, 0x32

    cmp-long v4, v12, v4

    if-lez v4, :cond_1

    .line 184
    const/16 v15, 0x85

    .line 188
    .end local v12    # "diff":J
    :cond_1
    int-to-long v4, v15

    invoke-virtual {v10, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 189
    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 190
    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dur="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    :cond_2
    return-object v10

    .line 79
    .end local v11    # "builder":Landroid/animation/AnimatorSet$Builder;
    .end local v14    # "direction":Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    .end local v15    # "duration":I
    .end local v16    # "hiddenX":I
    .end local v18    # "next":Z
    .end local v19    # "translateX":I
    :cond_3
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 80
    .restart local v18    # "next":Z
    :cond_4
    sget-object v14, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    goto/16 :goto_1

    .line 90
    .restart local v14    # "direction":Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    :cond_5
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getX()F

    move-result v4

    sget v5, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->ANIMATION_HERO_TRANSLATE_X:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v19, v0

    .line 91
    .restart local v19    # "translateX":I
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getX()F

    move-result v4

    sget v5, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->ANIMATION_HERO_TRANSLATE_X:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v16, v0

    .restart local v16    # "hiddenX":I
    goto/16 :goto_2
.end method

.method public createHiddenViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public createMiddleLeftViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public createMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public createNewMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method public createSideViewToMiddleAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;
    .locals 1
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method endAnimation()V
    .locals 6

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endPending:Z

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endPending:Z

    .line 243
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v5, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    .line 246
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v5, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    .line 249
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_RIGHT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v5, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    .line 252
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endAnimation(Landroid/animation/Animator$AnimatorListener;IZ)V

    .line 255
    :cond_0
    return-void
.end method

.method endAnimation(Landroid/animation/Animator$AnimatorListener;IZ)V
    .locals 10
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "newPos"    # I
    .param p3, "skipEnd"    # Z

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 196
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 197
    .local v3, "set":Landroid/animation/AnimatorSet;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    aput v9, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 198
    .local v0, "alpha1":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    aput v9, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 199
    .local v1, "alpha2":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    aput v9, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 200
    .local v2, "alpha3":Landroid/animation/ObjectAnimator;
    const-wide/16 v4, 0xa5

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 201
    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v0, v4, v7

    aput-object v1, v4, v8

    const/4 v5, 0x2

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 202
    new-instance v4, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;

    invoke-direct {v4, p0, p3, p2, p1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;-><init>(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;ZILandroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 235
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 236
    return-void
.end method

.method isEndPending()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endPending:Z

    return v0
.end method
