.class public Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;
.super Ljava/lang/Object;
.source "Carousel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/carousel/Carousel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewCacheManager"
.end annotation


# instance fields
.field private maxCacheCount:I

.field private middleLeftViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private middleRightViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private sideViewCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxCacheCount"    # I

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->sideViewCache:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleLeftViewCache:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleRightViewCache:Ljava/util/ArrayList;

    .line 72
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->maxCacheCount:I

    .line 73
    return-void
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->sideViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleLeftViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 137
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleRightViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 138
    return-void
.end method

.method public getView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;)Landroid/view/View;
    .locals 4
    .param p1, "viewType"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    .prologue
    const/4 v3, 0x0

    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "cachedView":Landroid/view/View;
    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$1;->$SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 99
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 100
    const-string v1, "CAROUSEL"

    const-string v2, "uhoh"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_1
    return-object v0

    .line 79
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->sideViewCache:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->sideViewCache:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cachedView":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .restart local v0    # "cachedView":Landroid/view/View;
    goto :goto_0

    .line 86
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleLeftViewCache:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleLeftViewCache:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cachedView":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .restart local v0    # "cachedView":Landroid/view/View;
    goto :goto_0

    .line 93
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleRightViewCache:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleRightViewCache:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cachedView":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .restart local v0    # "cachedView":Landroid/view/View;
    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public putView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V
    .locals 2
    .param p1, "viewType"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 106
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 107
    :cond_0
    const-string v0, "CAROUSEL"

    const-string v1, "uhoh"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$1;->$SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 132
    :cond_2
    :goto_0
    return-void

    .line 112
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->sideViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->maxCacheCount:I

    if-ge v0, v1, :cond_2

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->sideViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleLeftViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->maxCacheCount:I

    if-ge v0, v1, :cond_2

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleLeftViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleRightViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->maxCacheCount:I

    if-ge v0, v1, :cond_2

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->middleRightViewCache:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
