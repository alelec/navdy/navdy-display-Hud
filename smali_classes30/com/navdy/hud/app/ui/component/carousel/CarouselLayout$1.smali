.class Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$1;
.super Ljava/lang/Object;
.source "CarouselLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 159
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    # setter for: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchX:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$002(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;I)I

    .line 161
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    # setter for: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchY:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$102(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;I)I

    .line 163
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
