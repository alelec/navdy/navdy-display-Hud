.class Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "FastScrollAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->buildLayoutAnimation(Landroid/animation/Animator$AnimatorListener;Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;II)Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

.field final synthetic val$listener:Landroid/animation/Animator$AnimatorListener;

.field final synthetic val$newPos:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Landroid/animation/Animator$AnimatorListener;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$listener:Landroid/animation/Animator$AnimatorListener;

    iput p3, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$newPos:I

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 147
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    # setter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->lastScrollAnimationFinishTime:J
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$302(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;J)J

    .line 148
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->isAnimationPending()Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$listener:Landroid/animation/Animator$AnimatorListener;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$newPos:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endAnimation(Landroid/animation/Animator$AnimatorListener;IZ)V

    .line 177
    :goto_0
    return-void

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$newPos:I

    iput v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 155
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;

    move-result-object v0

    .line 156
    .local v0, "newMiddleLeft":Landroid/view/View;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    # setter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$102(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Landroid/view/View;)Landroid/view/View;

    .line 157
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 158
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 159
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    .line 161
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$listener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v1, :cond_1

    .line 162
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$listener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v1, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 165
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    iput-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    .line 167
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-eqz v1, :cond_2

    .line 168
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v1, :cond_3

    .line 171
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget v3, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .line 172
    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v4

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    .line 171
    invoke-interface {v2, v3, v1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onCurrentItemChanged(II)V

    .line 175
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    goto/16 :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endPending:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$002(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Z)Z

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$listener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$listener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->isAnimationPending()Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$newPos:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .line 135
    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v4

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v5

    iget v5, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    .line 134
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    .line 137
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$newPos:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .line 138
    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v4

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v5

    iget v5, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    .line 137
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->val$newPos:I

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .line 141
    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_RIGHT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v4

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$1;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v5

    iget v5, v5, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    .line 140
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    .line 143
    :cond_1
    return-void
.end method
