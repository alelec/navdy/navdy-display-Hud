.class public Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
.super Landroid/view/View;
.source "ProgressIndicator.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;


# instance fields
.field private animating:Z

.field private animatingColor:I

.field private animatingPos:F

.field private backgroundColor:I

.field private barParentSize:I

.field private barSize:I

.field private blackColor:I

.field private currentItem:I

.field private currentItemColor:I

.field private currentItemPaddingRadius:I

.field private defaultColor:I

.field private fullBackground:Z

.field private greyColor:I

.field private itemCount:I

.field private itemPadding:I

.field private itemRadius:I

.field private orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

.field private paint:Landroid/graphics/Paint;

.field private roundRadius:I

.field private viewPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, -0x1

    .line 49
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 35
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    .line 36
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barParentSize:I

    .line 38
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    .line 39
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemColor:I

    .line 50
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->init(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, -0x1

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    .line 36
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barParentSize:I

    .line 38
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    .line 39
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemColor:I

    .line 55
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->init(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;F)F
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    .param p1, "x1"    # F

    .prologue
    .line 23
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animatingPos:F

    return p1
.end method

.method private drawItem(Landroid/graphics/Canvas;FFFII)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "targetPos"    # F
    .param p5, "padding"    # I
    .param p6, "color"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 331
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->blackColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 332
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v0, v1, :cond_0

    .line 333
    div-float v0, p3, v3

    int-to-float v1, p5

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 339
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setColor(I)V

    .line 340
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v0, v1, :cond_1

    .line 341
    div-float v0, p3, v3

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 345
    :goto_1
    return-void

    .line 335
    :cond_0
    div-float v0, p2, v3

    int-to-float v1, p5

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, p4, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 343
    :cond_1
    div-float v0, p2, v3

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, p4, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private getItemTargetPos(FI)F
    .locals 5
    .param p1, "dimension"    # F
    .param p2, "itemPos"    # I

    .prologue
    .line 237
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->fullBackground:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    if-lez v3, :cond_0

    .line 238
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr p1, v3

    .line 240
    :cond_0
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    if-gtz v3, :cond_2

    .line 241
    const/4 v2, 0x0

    .line 262
    :cond_1
    :goto_0
    return v2

    .line 242
    :cond_2
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 243
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemPaddingRadius:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v2, p1, v3

    goto :goto_0

    .line 245
    :cond_3
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemPaddingRadius:I

    add-int/2addr v3, v4

    int-to-float v1, v3

    .line 246
    .local v1, "minPos":F
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemPaddingRadius:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v0, p1, v3

    .line 248
    .local v0, "maxPos":F
    if-nez p2, :cond_4

    .line 249
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemPaddingRadius:I

    add-int/2addr v3, v4

    int-to-float v2, v3

    .line 256
    .local v2, "targetPos":F
    :goto_1
    cmpg-float v3, v2, v1

    if-gez v3, :cond_6

    .line 257
    move v2, v1

    goto :goto_0

    .line 250
    .end local v2    # "targetPos":F
    :cond_4
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    add-int/lit8 v3, v3, -0x1

    if-ne p2, v3, :cond_5

    .line 251
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemPaddingRadius:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v2, p1, v3

    .restart local v2    # "targetPos":F
    goto :goto_1

    .line 253
    .end local v2    # "targetPos":F
    :cond_5
    int-to-float v3, p2

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    div-float v4, p1, v4

    mul-float v2, v3, v4

    .restart local v2    # "targetPos":F
    goto :goto_1

    .line 258
    :cond_6
    cmpl-float v3, v2, v0

    if-lez v3, :cond_1

    .line 259
    move v2, v0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->blackColor:I

    .line 76
    const v1, 0x7f0d002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->greyColor:I

    .line 78
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    .line 79
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 80
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 81
    return-void
.end method


# virtual methods
.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    return v0
.end method

.method public getItemMoveAnimator(II)Landroid/animation/AnimatorSet;
    .locals 11
    .param p1, "toPos"    # I
    .param p2, "color"    # I

    .prologue
    const/4 v10, 0x1

    .line 269
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getWidth()I

    move-result v8

    int-to-float v7, v8

    .line 270
    .local v7, "width":F
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getHeight()I

    move-result v8

    int-to-float v2, v8

    .line 271
    .local v2, "height":F
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v9, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v8, v9, :cond_1

    move v8, v7

    :goto_0
    iget v9, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    invoke-direct {p0, v8, v9}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getItemTargetPos(FI)F

    move-result v1

    .line 272
    .local v1, "currentPos":F
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v9, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v8, v9, :cond_2

    move v8, v7

    :goto_1
    invoke-direct {p0, v8, p1}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getItemTargetPos(FI)F

    move-result v6

    .line 273
    .local v6, "targetPos":F
    iget-boolean v8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->fullBackground:Z

    if-eqz v8, :cond_0

    .line 274
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v9, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v8, v9, :cond_3

    .end local v7    # "width":F
    :goto_2
    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    add-int/lit8 v8, v8, -0x1

    invoke-direct {p0, v7, v8}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getItemTargetPos(FI)F

    move-result v8

    float-to-int v3, v8

    .line 275
    .local v3, "lastItem":I
    float-to-int v8, v6

    iget v9, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    add-int v4, v8, v9

    .line 276
    .local v4, "newPos":I
    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    sub-int v8, v3, v8

    if-ge v4, v8, :cond_4

    .line 277
    int-to-float v6, v4

    .line 283
    .end local v3    # "lastItem":I
    .end local v4    # "newPos":I
    :cond_0
    :goto_3
    const/4 v8, 0x2

    new-array v8, v8, [F

    const/4 v9, 0x0

    aput v1, v8, v9

    aput v6, v8, v10

    invoke-static {v8}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 284
    .local v0, "animator":Landroid/animation/ValueAnimator;
    new-instance v8, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator$1;

    invoke-direct {v8, p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator$1;-><init>(Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;)V

    invoke-virtual {v0, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 291
    new-instance v8, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator$2;

    invoke-direct {v8, p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator$2;-><init>(Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;)V

    invoke-virtual {v0, v8}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 297
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 298
    .local v5, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 299
    iput v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animatingPos:F

    .line 300
    iput p2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animatingColor:I

    .line 301
    iput-boolean v10, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animating:Z

    .line 302
    return-object v5

    .end local v0    # "animator":Landroid/animation/ValueAnimator;
    .end local v1    # "currentPos":F
    .end local v5    # "set":Landroid/animation/AnimatorSet;
    .end local v6    # "targetPos":F
    .restart local v7    # "width":F
    :cond_1
    move v8, v2

    .line 271
    goto :goto_0

    .restart local v1    # "currentPos":F
    :cond_2
    move v8, v2

    .line 272
    goto :goto_1

    .restart local v6    # "targetPos":F
    :cond_3
    move v7, v2

    .line 274
    goto :goto_2

    .line 279
    .end local v7    # "width":F
    .restart local v3    # "lastItem":I
    .restart local v4    # "newPos":I
    :cond_4
    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    sub-int v8, v3, v8

    int-to-float v6, v8

    goto :goto_3
.end method

.method public getItemPos(I)Landroid/graphics/RectF;
    .locals 8
    .param p1, "n"    # I

    .prologue
    const/4 v7, 0x0

    .line 307
    if-ltz p1, :cond_2

    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    if-ge p1, v5, :cond_2

    .line 308
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getWidth()I

    move-result v5

    int-to-float v2, v5

    .line 309
    .local v2, "width":F
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getHeight()I

    move-result v5

    int-to-float v0, v5

    .line 310
    .local v0, "height":F
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v6, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v5, v6, :cond_0

    .end local v2    # "width":F
    :goto_0
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    invoke-direct {p0, v2, v5}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getItemTargetPos(FI)F

    move-result v1

    .line 312
    .local v1, "targetPos":F
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v6, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v5, v6, :cond_1

    .line 313
    move v3, v1

    .line 314
    .local v3, "x":F
    const/4 v4, 0x0

    .line 319
    .local v4, "y":F
    :goto_1
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v3, v4, v7, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 322
    .end local v0    # "height":F
    .end local v1    # "targetPos":F
    .end local v3    # "x":F
    .end local v4    # "y":F
    :goto_2
    return-object v5

    .restart local v0    # "height":F
    .restart local v2    # "width":F
    :cond_0
    move v2, v0

    .line 310
    goto :goto_0

    .line 316
    .end local v2    # "width":F
    .restart local v1    # "targetPos":F
    :cond_1
    const/4 v3, 0x0

    .line 317
    .restart local v3    # "x":F
    move v4, v1

    .restart local v4    # "y":F
    goto :goto_1

    .line 322
    .end local v0    # "height":F
    .end local v1    # "targetPos":F
    .end local v3    # "x":F
    .end local v4    # "y":F
    :cond_2
    const/4 v5, 0x0

    goto :goto_2
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 23
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 151
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 152
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getWidth()I

    move-result v3

    int-to-float v5, v3

    .line 153
    .local v5, "width":F
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getHeight()I

    move-result v3

    int-to-float v6, v3

    .line 154
    .local v6, "height":F
    const/4 v13, 0x0

    .line 155
    .local v13, "left":I
    const/16 v17, 0x0

    .line 156
    .local v17, "top":I
    move v11, v5

    .line 157
    .local v11, "barWidth":F
    move v10, v6

    .line 159
    .local v10, "barHeight":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 160
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->VERTICAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v3, v4, :cond_2

    .line 161
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    int-to-float v3, v3

    sub-float v3, v5, v3

    float-to-int v3, v3

    div-int/lit8 v13, v3, 0x2

    .line 162
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    int-to-float v11, v3

    .line 169
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->backgroundColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 170
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    if-nez v3, :cond_3

    .line 172
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v4, v13

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v13

    move/from16 v21, v0

    add-float v21, v21, v11

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v22, v0

    add-float v22, v22, v10

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v3, v4, v0, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 234
    :cond_1
    :goto_1
    return-void

    .line 164
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    int-to-float v3, v3

    sub-float v3, v5, v3

    float-to-int v3, v3

    div-int/lit8 v17, v3, 0x2

    .line 165
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    int-to-float v10, v3

    goto :goto_0

    .line 174
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 176
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v4, v13

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v13

    move/from16 v21, v0

    add-float v21, v21, v11

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v22, v0

    add-float v22, v22, v10

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v3, v4, v0, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 200
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemPaddingRadius:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    add-int v8, v3, v4

    .line 202
    .local v8, "padding":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animating:Z

    if-eqz v3, :cond_7

    .line 203
    move-object/from16 v0, p0

    iget v7, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animatingPos:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animatingColor:I

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v9}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->drawItem(Landroid/graphics/Canvas;FFFII)V

    goto :goto_1

    .line 179
    .end local v8    # "padding":I
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->fullBackground:Z

    if-eqz v3, :cond_5

    .line 180
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v4, v13

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    int-to-float v0, v13

    move/from16 v21, v0

    add-float v21, v21, v11

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v22, v0

    add-float v22, v22, v10

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v3, v4, v0, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 184
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v3, v4, :cond_6

    .line 185
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    int-to-float v0, v3

    move/from16 v18, v0

    .line 186
    .local v18, "x":F
    const/high16 v3, 0x40000000    # 2.0f

    div-float v19, v6, v3

    .line 187
    .local v19, "y":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    mul-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemPadding:I

    add-int v16, v3, v4

    .line 188
    .local v16, "start":I
    new-instance v15, Landroid/graphics/RectF;

    move/from16 v0, v16

    int-to-float v3, v0

    const/4 v4, 0x0

    invoke-direct {v15, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 195
    .local v15, "rectF":Landroid/graphics/RectF;
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 196
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->paint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v15, v3, v4, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 190
    .end local v15    # "rectF":Landroid/graphics/RectF;
    .end local v16    # "start":I
    .end local v18    # "x":F
    .end local v19    # "y":F
    :cond_6
    const/high16 v3, 0x40000000    # 2.0f

    div-float v18, v5, v3

    .line 191
    .restart local v18    # "x":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    int-to-float v0, v3

    move/from16 v19, v0

    .line 192
    .restart local v19    # "y":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    mul-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemPadding:I

    add-int v16, v3, v4

    .line 193
    .restart local v16    # "start":I
    new-instance v15, Landroid/graphics/RectF;

    const/4 v3, 0x0

    move/from16 v0, v16

    int-to-float v4, v0

    invoke-direct {v15, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v15    # "rectF":Landroid/graphics/RectF;
    goto :goto_3

    .line 207
    .end local v15    # "rectF":Landroid/graphics/RectF;
    .end local v16    # "start":I
    .end local v18    # "x":F
    .end local v19    # "y":F
    .restart local v8    # "padding":I
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 209
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v3, v4, :cond_a

    move v3, v5

    :goto_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getItemTargetPos(FI)F

    move-result v7

    .line 211
    .local v7, "targetPos":F
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->fullBackground:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    if-lez v3, :cond_8

    .line 212
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    if-nez v3, :cond_b

    .line 213
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    int-to-float v7, v3

    .line 227
    :cond_8
    :goto_5
    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->defaultColor:I

    .line 228
    .local v9, "color":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemColor:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_9

    .line 229
    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemColor:I

    :cond_9
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    .line 232
    invoke-direct/range {v3 .. v9}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->drawItem(Landroid/graphics/Canvas;FFFII)V

    goto/16 :goto_1

    .end local v7    # "targetPos":F
    .end local v9    # "color":I
    :cond_a
    move v3, v6

    .line 209
    goto :goto_4

    .line 214
    .restart local v7    # "targetPos":F
    :cond_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_c

    .line 215
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    sub-int/2addr v3, v4

    int-to-float v7, v3

    goto :goto_5

    .line 217
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    sub-int v12, v3, v4

    .line 218
    .local v12, "lastItem":I
    float-to-int v3, v7

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    add-int v14, v3, v4

    .line 219
    .local v14, "newPos":I
    if-gt v14, v12, :cond_d

    .line 220
    int-to-float v7, v14

    goto :goto_5

    .line 222
    :cond_d
    int-to-float v7, v12

    goto :goto_5
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 61
    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getMeasuredWidth()I

    move-result v1

    .line 63
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->getMeasuredHeight()I

    move-result v0

    .line 64
    .local v0, "height":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->VERTICAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v2, v3, :cond_1

    .line 65
    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barParentSize:I

    .line 69
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setMeasuredDimension(II)V

    .line 71
    .end local v0    # "height":I
    .end local v1    # "width":I
    :cond_0
    return-void

    .line 67
    .restart local v0    # "height":I
    .restart local v1    # "width":I
    :cond_1
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barParentSize:I

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 348
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->backgroundColor:I

    .line 349
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 134
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setCurrentItem(II)V

    .line 135
    return-void
.end method

.method public setCurrentItem(II)V
    .locals 1
    .param p1, "n"    # I
    .param p2, "color"    # I

    .prologue
    .line 139
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_0

    .line 140
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    .line 141
    iput p2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemColor:I

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animating:Z

    .line 143
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animatingPos:F

    .line 144
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->animatingColor:I

    .line 145
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->invalidate()V

    .line 147
    :cond_0
    return-void
.end method

.method public setItemCount(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 120
    if-gtz p1, :cond_0

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 123
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItem:I

    .line 124
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemCount:I

    .line 125
    return-void
.end method

.method public setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V
    .locals 0
    .param p1, "orientation"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .line 116
    return-void
.end method

.method public setProperties(IIIIIIZIII)V
    .locals 1
    .param p1, "roundRadius"    # I
    .param p2, "itemRadius"    # I
    .param p3, "itemPadding"    # I
    .param p4, "currentItemPaddingRadius"    # I
    .param p5, "defaultColor"    # I
    .param p6, "backgroundColor"    # I
    .param p7, "fullBackground"    # Z
    .param p8, "viewPadding"    # I
    .param p9, "barSize"    # I
    .param p10, "barParentSize"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->roundRadius:I

    .line 94
    iput p2, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemRadius:I

    .line 95
    iput p3, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->itemPadding:I

    .line 96
    iput p8, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    .line 97
    iput p4, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->currentItemPaddingRadius:I

    .line 98
    iput p5, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->defaultColor:I

    .line 99
    iput p9, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barSize:I

    .line 100
    iput p10, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->barParentSize:I

    .line 101
    const/4 v0, -0x1

    if-eq p6, v0, :cond_1

    .line 102
    iput p6, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->backgroundColor:I

    .line 107
    :goto_0
    iput-boolean p7, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->fullBackground:Z

    .line 109
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->fullBackground:Z

    if-eqz v0, :cond_0

    if-lez p8, :cond_0

    .line 110
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->viewPadding:I

    .line 112
    :cond_0
    return-void

    .line 104
    :cond_1
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->greyColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->backgroundColor:I

    goto :goto_0
.end method
