.class Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
.super Ljava/lang/Object;
.source "CarouselLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "OperationInfo"
.end annotation


# instance fields
.field animationDuration:I

.field count:I

.field runnable:Ljava/lang/Runnable;

.field type:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "type"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    .param p2, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->count:I

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->animationDuration:I

    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->type:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 59
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->runnable:Ljava/lang/Runnable;

    .line 60
    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;Ljava/lang/Runnable;I)V
    .locals 1
    .param p1, "type"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    .param p2, "runnable"    # Ljava/lang/Runnable;
    .param p3, "animationDuration"    # I

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->count:I

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->animationDuration:I

    .line 63
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->type:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 64
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->runnable:Ljava/lang/Runnable;

    .line 65
    iput p3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->animationDuration:I

    .line 66
    return-void
.end method
