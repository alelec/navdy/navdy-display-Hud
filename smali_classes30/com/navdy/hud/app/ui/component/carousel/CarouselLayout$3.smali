.class Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$3;
.super Ljava/lang/Object;
.source "CarouselLayout.java"

# interfaces
.implements Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->init(Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return-object v0
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 279
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    invoke-static {v0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$400(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onMultipleClick(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 272
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$3;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onExit()V

    .line 275
    :cond_0
    return-void
.end method
