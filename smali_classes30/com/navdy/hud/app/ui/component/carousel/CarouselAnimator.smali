.class public Lcom/navdy/hud/app/ui/component/carousel/CarouselAnimator;
.super Ljava/lang/Object;
.source "CarouselAnimator.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselAnimator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAnimator;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public buildLayoutAnimation(Landroid/animation/Animator$AnimatorListener;Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;II)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p3, "currentPos"    # I
    .param p4, "newPos"    # I

    .prologue
    .line 144
    const/4 v0, 0x0

    return-object v0
.end method

.method public createHiddenViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 8
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 111
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 114
    .local v1, "animatorSet":Landroid/animation/AnimatorSet;
    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v4, :cond_0

    .line 115
    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newLeftView:Landroid/view/View;

    .line 116
    .local v2, "srcView":Landroid/view/View;
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 121
    .local v3, "targetPosView":Landroid/view/View;
    :goto_0
    const-string v4, "x"

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v7

    aput v7, v5, v6

    invoke-static {v2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 122
    .local v0, "a1":Landroid/animation/ObjectAnimator;
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 123
    return-object v1

    .line 118
    .end local v0    # "a1":Landroid/animation/ObjectAnimator;
    .end local v2    # "srcView":Landroid/view/View;
    .end local v3    # "targetPosView":Landroid/view/View;
    :cond_0
    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newRightView:Landroid/view/View;

    .line 119
    .restart local v2    # "srcView":Landroid/view/View;
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .restart local v3    # "targetPosView":Landroid/view/View;
    goto :goto_0
.end method

.method public createMiddleLeftViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 13
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 19
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 22
    .local v4, "animatorSet":Landroid/animation/AnimatorSet;
    sget-object v8, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v8, :cond_0

    .line 23
    iget-object v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 29
    .local v7, "targetPosView":Landroid/view/View;
    :goto_0
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getScaleX()F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_1

    .line 30
    iget v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    int-to-float v8, v8

    iget v9, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    int-to-float v9, v9

    div-float v6, v8, v9

    .line 35
    .local v6, "scaleFactor":F
    :goto_1
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    const-string v9, "scaleX"

    const/4 v10, 0x1

    new-array v10, v10, [F

    const/4 v11, 0x0

    aput v6, v10, v11

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 36
    .local v0, "a1":Landroid/animation/ObjectAnimator;
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    const-string v9, "scaleY"

    const/4 v10, 0x1

    new-array v10, v10, [F

    const/4 v11, 0x0

    aput v6, v10, v11

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 38
    .local v1, "a2":Landroid/animation/ObjectAnimator;
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    const-string v9, "x"

    const/4 v10, 0x1

    new-array v10, v10, [F

    const/4 v11, 0x0

    invoke-virtual {v7}, Landroid/view/View;->getX()F

    move-result v12

    aput v12, v10, v11

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 39
    .local v2, "a3":Landroid/animation/ObjectAnimator;
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    const-string v9, "y"

    const/4 v10, 0x1

    new-array v10, v10, [F

    const/4 v11, 0x0

    invoke-virtual {v7}, Landroid/view/View;->getY()F

    move-result v12

    aput v12, v10, v11

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 40
    .local v3, "a4":Landroid/animation/ObjectAnimator;
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    check-cast v8, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v5

    .line 42
    .local v5, "fade":Landroid/animation/AnimatorSet;
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v8, v9}, Landroid/view/View;->setPivotX(F)V

    .line 43
    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v8, v9}, Landroid/view/View;->setPivotY(F)V

    .line 44
    const/4 v8, 0x5

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v9, 0x1

    aput-object v1, v8, v9

    const/4 v9, 0x2

    aput-object v2, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    const/4 v9, 0x4

    aput-object v5, v8, v9

    invoke-virtual {v4, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 45
    return-object v4

    .line 25
    .end local v0    # "a1":Landroid/animation/ObjectAnimator;
    .end local v1    # "a2":Landroid/animation/ObjectAnimator;
    .end local v2    # "a3":Landroid/animation/ObjectAnimator;
    .end local v3    # "a4":Landroid/animation/ObjectAnimator;
    .end local v5    # "fade":Landroid/animation/AnimatorSet;
    .end local v6    # "scaleFactor":F
    .end local v7    # "targetPosView":Landroid/view/View;
    :cond_0
    iget-object v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .restart local v7    # "targetPosView":Landroid/view/View;
    goto/16 :goto_0

    .line 32
    :cond_1
    const/high16 v6, 0x3f800000    # 1.0f

    .restart local v6    # "scaleFactor":F
    goto :goto_1
.end method

.method public createMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 16
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 50
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 51
    .local v6, "animatorSet":Landroid/animation/AnimatorSet;
    move-object/from16 v0, p1

    iget v9, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    int-to-float v9, v9

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    int-to-float v10, v10

    div-float v7, v9, v10

    .line 53
    .local v7, "scaleFactor":F
    sget-object v9, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    move-object/from16 v0, p2

    if-ne v0, v9, :cond_0

    .line 54
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 58
    .local v8, "targetPosView":Landroid/view/View;
    :goto_0
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    const-string v10, "scaleX"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v7, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 59
    .local v1, "a1":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    const-string v10, "scaleY"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    aput v7, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 60
    .local v2, "a2":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    const-string v10, "x"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v13

    move-object/from16 v0, p1

    iget v14, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainViewDividerPadding:I

    move-object/from16 v0, p1

    iget v15, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v14, v15

    div-int/lit8 v14, v14, 0x2

    int-to-float v14, v14

    add-float/2addr v13, v14

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 61
    .local v3, "a3":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    const-string v10, "y"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getY()F

    move-result v13

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 62
    .local v4, "a4":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    const-string v10, "alpha"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 63
    .local v5, "a5":Landroid/animation/ObjectAnimator;
    const/4 v9, 0x5

    new-array v9, v9, [Landroid/animation/Animator;

    const/4 v10, 0x0

    aput-object v1, v9, v10

    const/4 v10, 0x1

    aput-object v2, v9, v10

    const/4 v10, 0x2

    aput-object v3, v9, v10

    const/4 v10, 0x3

    aput-object v4, v9, v10

    const/4 v10, 0x4

    aput-object v5, v9, v10

    invoke-virtual {v6, v9}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 64
    return-object v6

    .line 56
    .end local v1    # "a1":Landroid/animation/ObjectAnimator;
    .end local v2    # "a2":Landroid/animation/ObjectAnimator;
    .end local v3    # "a3":Landroid/animation/ObjectAnimator;
    .end local v4    # "a4":Landroid/animation/ObjectAnimator;
    .end local v5    # "a5":Landroid/animation/ObjectAnimator;
    .end local v8    # "targetPosView":Landroid/view/View;
    :cond_0
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .restart local v8    # "targetPosView":Landroid/view/View;
    goto/16 :goto_0
.end method

.method public createNewMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 10
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 128
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 129
    .local v2, "animatorSet":Landroid/animation/AnimatorSet;
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    const-string v4, "x"

    new-array v5, v9, [F

    iget-object v6, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getX()F

    move-result v6

    iget v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainViewDividerPadding:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iget v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 130
    .local v0, "a1":Landroid/animation/ObjectAnimator;
    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v3, :cond_0

    .line 131
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    const-string v4, "alpha"

    new-array v5, v9, [F

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 132
    .local v1, "a2":Landroid/animation/ObjectAnimator;
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v8

    aput-object v1, v3, v9

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 136
    .end local v1    # "a2":Landroid/animation/ObjectAnimator;
    :goto_0
    return-object v2

    .line 134
    :cond_0
    new-array v3, v9, [Landroid/animation/Animator;

    aput-object v0, v3, v8

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_0
.end method

.method public createSideViewToMiddleAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 12
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    .line 70
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 74
    .local v4, "animatorSet":Landroid/animation/AnimatorSet;
    sget-object v8, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v8, :cond_0

    .line 75
    iget-object v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 81
    .local v7, "sideImageView":Landroid/view/View;
    :goto_0
    invoke-virtual {v7}, Landroid/view/View;->getScaleX()F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_1

    .line 82
    iget v8, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    int-to-float v8, v8

    iget v9, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    int-to-float v9, v9

    div-float v6, v8, v9

    .line 87
    .local v6, "scaleFactor":F
    :goto_1
    const-string v8, "scaleX"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    aput v6, v9, v10

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 88
    .local v0, "a1":Landroid/animation/ObjectAnimator;
    const-string v8, "scaleY"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    aput v6, v9, v10

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 89
    .local v1, "a2":Landroid/animation/ObjectAnimator;
    const-string v8, "x"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    iget-object v11, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getX()F

    move-result v11

    aput v11, v9, v10

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 90
    .local v2, "a3":Landroid/animation/ObjectAnimator;
    const-string v8, "y"

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    iget-object v11, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getY()F

    move-result v11

    aput v11, v9, v10

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .local v3, "a4":Landroid/animation/ObjectAnimator;
    move-object v8, v7

    .line 91
    check-cast v8, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v5

    .line 93
    .local v5, "fade":Landroid/animation/AnimatorSet;
    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8}, Landroid/view/View;->setPivotX(F)V

    .line 94
    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8}, Landroid/view/View;->setPivotY(F)V

    .line 95
    const/4 v8, 0x5

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v9, 0x1

    aput-object v1, v8, v9

    const/4 v9, 0x2

    aput-object v2, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    const/4 v9, 0x4

    aput-object v5, v8, v9

    invoke-virtual {v4, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 97
    return-object v4

    .line 77
    .end local v0    # "a1":Landroid/animation/ObjectAnimator;
    .end local v1    # "a2":Landroid/animation/ObjectAnimator;
    .end local v2    # "a3":Landroid/animation/ObjectAnimator;
    .end local v3    # "a4":Landroid/animation/ObjectAnimator;
    .end local v5    # "fade":Landroid/animation/AnimatorSet;
    .end local v6    # "scaleFactor":F
    .end local v7    # "sideImageView":Landroid/view/View;
    :cond_0
    iget-object v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .restart local v7    # "sideImageView":Landroid/view/View;
    goto :goto_0

    .line 84
    :cond_1
    const/high16 v6, 0x3f800000    # 1.0f

    .restart local v6    # "scaleFactor":F
    goto :goto_1
.end method

.method public createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;
    .locals 6
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 102
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v0, :cond_0

    .line 103
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    const-string v1, "x"

    new-array v2, v2, [F

    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v3

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    aput v3, v2, v5

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    const-string v1, "x"

    new-array v2, v2, [F

    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v3

    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    neg-float v3, v3

    aput v3, v2, v5

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_0
.end method
