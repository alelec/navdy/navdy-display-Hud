.class public Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;
.super Ljava/lang/Object;
.source "Carousel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/carousel/Carousel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InitParams"
.end annotation


# instance fields
.field public animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

.field public carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

.field public exitOnDoubleClick:Z

.field public fastScrollAnimation:Z

.field public imageLytResourceId:I

.field public infoLayoutResourceId:I

.field public model:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;",
            ">;"
        }
    .end annotation
.end field

.field public rootContainer:Landroid/view/View;

.field public viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
