.class public final enum Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
.super Ljava/lang/Enum;
.source "AnimationStrategy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Direction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

.field public static final enum LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

.field public static final enum RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 12
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 10
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->$VALUES:[Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->$VALUES:[Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    return-object v0
.end method
