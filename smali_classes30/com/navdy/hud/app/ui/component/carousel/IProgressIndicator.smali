.class public interface abstract Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;
.super Ljava/lang/Object;
.source "IProgressIndicator.java"


# virtual methods
.method public abstract getCurrentItem()I
.end method

.method public abstract getItemMoveAnimator(II)Landroid/animation/AnimatorSet;
.end method

.method public abstract getItemPos(I)Landroid/graphics/RectF;
.end method

.method public abstract setCurrentItem(I)V
.end method

.method public abstract setCurrentItem(II)V
.end method

.method public abstract setItemCount(I)V
.end method

.method public abstract setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V
.end method
