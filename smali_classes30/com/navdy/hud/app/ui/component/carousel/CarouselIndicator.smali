.class public Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
.super Landroid/widget/FrameLayout;
.source "CarouselIndicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;
    }
.end annotation


# static fields
.field public static final PROGRESS_INDICATOR_HORIZONTAL_THRESHOLD:I = 0xc

.field public static final PROGRESS_INDICATOR_VERTICAL_THRESHOLD:I = 0x8


# instance fields
.field private barParentSize:I

.field private barSize:I

.field private circleFocusSize:I

.field private circleMargin:I

.field private circleSize:I

.field private currentItemPaddingRadius:I

.field private fullBackground:Z

.field private itemCount:I

.field private itemPadding:I

.field private itemRadius:I

.field private orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

.field private progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

.field private roundRadius:I

.field private viewPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 54
    invoke-direct {p0, p1, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barSize:I

    .line 36
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barParentSize:I

    .line 51
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .line 55
    invoke-direct {p0, p1, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, -0x1

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barSize:I

    .line 36
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barParentSize:I

    .line 51
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v0, -0x1

    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barSize:I

    .line 36
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barParentSize:I

    .line 51
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    const/high16 v4, -0x40800000    # -1.0f

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 71
    .local v0, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0b0031

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleSize:I

    .line 72
    const v2, 0x7f0b002f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleFocusSize:I

    .line 73
    const v2, 0x7f0b0030

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleMargin:I

    .line 75
    const v2, 0x7f0b0046

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->roundRadius:I

    .line 76
    const v2, 0x7f0b0045

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemRadius:I

    .line 77
    const v2, 0x7f0b0044

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemPadding:I

    .line 78
    const v2, 0x7f0b003f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->currentItemPaddingRadius:I

    .line 80
    if-eqz p2, :cond_0

    .line 81
    sget-object v2, Lcom/navdy/hud/app/R$styleable;->CarouselIndicator:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 83
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleSize:I

    .line 84
    const/4 v2, 0x1

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleFocusSize:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleFocusSize:I

    .line 85
    const/4 v2, 0x2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleMargin:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleMargin:I

    .line 87
    const/4 v2, 0x3

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->roundRadius:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->roundRadius:I

    .line 88
    const/4 v2, 0x4

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemRadius:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemRadius:I

    .line 89
    const/4 v2, 0x5

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemPadding:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemPadding:I

    .line 90
    const/4 v2, 0x6

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->currentItemPaddingRadius:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->currentItemPaddingRadius:I

    .line 91
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->fullBackground:Z

    .line 92
    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->viewPadding:I

    .line 93
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barSize:I

    .line 94
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barParentSize:I

    .line 96
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 98
    .end local v1    # "typedArray":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->getCurrentItem()I

    move-result v0

    .line 177
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemCount:I

    return v0
.end method

.method public getItemMoveAnimator(II)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "toPos"    # I
    .param p2, "color"    # I

    .prologue
    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    invoke-interface {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->getItemMoveAnimator(II)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemPos(I)Landroid/graphics/RectF;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->getItemPos(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCurrentItem(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->setCurrentItem(I)V

    .line 165
    :cond_0
    return-void
.end method

.method public setCurrentItem(II)V
    .locals 1
    .param p1, "n"    # I
    .param p2, "color"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    invoke-interface {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->setCurrentItem(II)V

    .line 171
    :cond_0
    return-void
.end method

.method public setItemCount(I)V
    .locals 18
    .param p1, "n"    # I

    .prologue
    .line 105
    if-gez p1, :cond_0

    .line 159
    :goto_0
    return-void

    .line 108
    :cond_0
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemCount:I

    .line 110
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 111
    .local v7, "defaultColor":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v2, v3, :cond_3

    const/16 v16, 0xc

    .line 112
    .local v16, "threshold":I
    :goto_1
    move/from16 v0, p1

    move/from16 v1, v16

    if-le v0, v1, :cond_6

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    instance-of v2, v2, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    if-nez v2, :cond_2

    .line 114
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->removeAllViews()V

    .line 115
    new-instance v2, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    check-cast v2, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->roundRadius:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemRadius:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->itemPadding:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->currentItemPaddingRadius:I

    const/4 v8, -0x1

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->fullBackground:Z

    move-object/from16 v0, p0

    iget v10, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->viewPadding:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barSize:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barParentSize:I

    invoke-virtual/range {v2 .. v12}, Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;->setProperties(IIIIIIZIII)V

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V

    .line 128
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 131
    .local v15, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->barSize:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v2, v3, :cond_4

    .line 133
    const v2, 0x7f0b0040

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v13, v2

    .line 134
    .local v13, "height":I
    const v2, 0x7f0b0043

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    .line 139
    .local v17, "width":I
    :goto_2
    new-instance v14, Landroid/widget/FrameLayout$LayoutParams;

    move/from16 v0, v17

    invoke-direct {v14, v0, v13}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 143
    .end local v13    # "height":I
    .end local v17    # "width":I
    .local v14, "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    :goto_3
    const/16 v2, 0x11

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    check-cast v2, Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    .end local v14    # "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v15    # "resources":Landroid/content/res/Resources;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->setItemCount(I)V

    goto/16 :goto_0

    .line 111
    .end local v16    # "threshold":I
    :cond_3
    const/16 v16, 0x8

    goto/16 :goto_1

    .line 136
    .restart local v15    # "resources":Landroid/content/res/Resources;
    .restart local v16    # "threshold":I
    :cond_4
    const v2, 0x7f0b0042

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    .line 137
    .restart local v17    # "width":I
    const v2, 0x7f0b0041

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v13, v2

    .restart local v13    # "height":I
    goto :goto_2

    .line 141
    .end local v13    # "height":I
    .end local v17    # "width":I
    :cond_5
    new-instance v14, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-direct {v14, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .restart local v14    # "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    goto :goto_3

    .line 148
    .end local v14    # "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v15    # "resources":Landroid/content/res/Resources;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    instance-of v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    if-nez v2, :cond_8

    .line 149
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->removeAllViews()V

    .line 150
    new-instance v2, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    check-cast v2, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleSize:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleFocusSize:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->circleMargin:I

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->setProperties(IIII)V

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V

    .line 153
    new-instance v14, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v14, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 154
    .restart local v14    # "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x11

    iput v2, v14, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    check-cast v2, Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    .end local v14    # "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->progressIndicator:Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;->setItemCount(I)V

    goto/16 :goto_0
.end method

.method public setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V
    .locals 0
    .param p1, "orientation"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .line 102
    return-void
.end method
