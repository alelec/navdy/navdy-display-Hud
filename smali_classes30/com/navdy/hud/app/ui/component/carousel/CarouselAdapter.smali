.class public Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;
.super Ljava/lang/Object;
.source "CarouselAdapter.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V
    .locals 0
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .line 23
    return-void
.end method

.method private processImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;II)V
    .locals 12
    .param p1, "viewType"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;
    .param p2, "item"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p3, "imageView"    # Landroid/view/View;
    .param p4, "position"    # I
    .param p5, "size"    # I

    .prologue
    .line 75
    instance-of v0, p3, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    if-eqz v0, :cond_3

    move-object v7, p3

    .line 76
    check-cast v7, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 77
    .local v7, "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 78
    .local v6, "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v10

    .line 80
    .local v10, "small":Landroid/view/View;
    iget v0, p2, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->largeImageRes:I

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v6, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    const v1, 0x7f030010

    if-ne v0, v1, :cond_2

    move-object v9, v10

    .line 83
    check-cast v9, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 84
    .local v9, "secondImageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    iget v0, p2, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->smallImageRes:I

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v9, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 98
    .end local v6    # "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .end local v7    # "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    .end local v9    # "secondImageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .end local v10    # "small":Landroid/view/View;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    if-eqz v0, :cond_1

    .line 100
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p1, v0, :cond_5

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    move-object v1, p2

    move-object v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;->processLargeImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;III)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_1
    :goto_1
    return-void

    .restart local v6    # "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .restart local v7    # "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    .restart local v10    # "small":Landroid/view/View;
    :cond_2
    move-object v9, v10

    .line 86
    check-cast v9, Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    .line 87
    .local v9, "secondImageView":Lcom/navdy/hud/app/ui/component/image/ColorImageView;
    iget v0, p2, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->smallImageColor:I

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setColor(I)V

    goto :goto_0

    .line 89
    .end local v6    # "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .end local v7    # "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    .end local v9    # "secondImageView":Lcom/navdy/hud/app/ui/component/image/ColorImageView;
    .end local v10    # "small":Landroid/view/View;
    :cond_3
    instance-of v0, p3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    if-eqz v0, :cond_0

    move-object v8, p3

    .line 90
    check-cast v8, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 91
    .local v8, "initialsImageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p1, v0, :cond_4

    .line 92
    iget v0, p2, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->largeImageRes:I

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v8, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_0

    .line 94
    :cond_4
    iget v0, p2, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->smallImageRes:I

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v8, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_0

    .line 103
    .end local v8    # "initialsImageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    move-object v1, p2

    move-object v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;->processSmallImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;III)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 105
    :catch_0
    move-exception v11

    .line 106
    .local v11, "t":Ljava/lang/Throwable;
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private processInfoView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;I)V
    .locals 8
    .param p1, "item"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I

    .prologue
    .line 113
    iget-object v6, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    if-eqz v6, :cond_2

    move-object v5, p2

    .line 114
    check-cast v5, Landroid/view/ViewGroup;

    .line 115
    .local v5, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 116
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 117
    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 118
    .local v0, "child":Landroid/view/View;
    instance-of v6, v0, Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 119
    iget-object v6, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->infoMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 120
    .local v4, "text":Ljava/lang/String;
    if-eqz v4, :cond_1

    move-object v6, v0

    .line 121
    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 116
    .end local v4    # "text":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 124
    .restart local v4    # "text":Ljava/lang/String;
    :cond_1
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 130
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childCount":I
    .end local v2    # "i":I
    .end local v4    # "text":Ljava/lang/String;
    .end local v5    # "viewGroup":Landroid/view/ViewGroup;
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    if-eqz v6, :cond_3

    .line 132
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    invoke-interface {v6, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;->processInfoView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_3
    :goto_2
    return-void

    .line 133
    :catch_0
    move-exception v3

    .line 134
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private processView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;II)V
    .locals 1
    .param p1, "viewType"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;
    .param p2, "item"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p3, "view"    # Landroid/view/View;
    .param p4, "position"    # I
    .param p5, "size"    # I

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_RIGHT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p1, v0, :cond_0

    .line 68
    invoke-direct {p0, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->processInfoView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;I)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-direct/range {p0 .. p5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->processImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;II)V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "type"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;
    .param p4, "layoutId"    # I
    .param p5, "size"    # I

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0e0011

    .line 26
    if-eqz p2, :cond_0

    .line 27
    invoke-virtual {p2, v5, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 29
    :cond_0
    const/4 v2, 0x0

    .line 30
    .local v2, "item":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "item":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    check-cast v2, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    .line 34
    .restart local v2    # "item":Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    :cond_1
    if-nez p2, :cond_4

    .line 35
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " view"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->carousel:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->inflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 37
    .local v3, "view":Landroid/view/View;
    instance-of v0, v3, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    if-eqz v0, :cond_2

    .line 38
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p3, v0, :cond_3

    move-object v0, v3

    .line 39
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->inject(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 55
    :cond_2
    :goto_0
    if-eqz v2, :cond_6

    .line 56
    iget v0, v2, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 57
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    move-object v0, p0

    move-object v1, p3

    move v4, p1

    move v5, p5

    .line 58
    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->processView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;II)V

    .line 63
    :goto_1
    return-object v3

    .line 40
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p3, v0, :cond_2

    move-object v0, v3

    .line 41
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->inject(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    goto :goto_0

    .line 45
    .end local v3    # "view":Landroid/view/View;
    :cond_4
    move-object v3, p2

    .line 46
    .restart local v3    # "view":Landroid/view/View;
    instance-of v0, v3, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    if-eqz v0, :cond_2

    .line 47
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p3, v0, :cond_5

    move-object v0, v3

    .line 48
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    goto :goto_0

    .line 49
    :cond_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p3, v0, :cond_2

    move-object v0, v3

    .line 50
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    goto :goto_0

    .line 60
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 61
    invoke-virtual {v3, v5, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1
.end method
