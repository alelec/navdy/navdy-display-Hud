.class Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;
.super Ljava/lang/Object;
.source "CarouselLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 170
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 171
    .local v7, "hitRect":Landroid/graphics/Rect;
    const/4 v0, 0x2

    new-array v9, v0, [I

    .line 172
    .local v9, "myPos":[I
    const/4 v0, 0x2

    new-array v11, v0, [I

    .line 173
    .local v11, "parentPos":[I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v0, v9}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getLocationInWindow([I)V

    .line 175
    const/4 v0, 0x3

    new-array v14, v0, [Landroid/view/View;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    aput-object v1, v14, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    aput-object v1, v14, v0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    aput-object v1, v14, v0

    .line 177
    .local v14, "views":[Landroid/view/View;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v14

    if-ge v8, v0, :cond_0

    .line 178
    aget-object v6, v14, v8

    .line 179
    .local v6, "childView":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    .line 180
    .local v10, "parent":Landroid/view/View;
    invoke-virtual {v10, v11}, Landroid/view/View;->getLocationInWindow([I)V

    .line 181
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchX:I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$000(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)I

    move-result v0

    const/4 v1, 0x0

    aget v1, v11, v1

    const/4 v2, 0x0

    aget v2, v9, v2

    sub-int/2addr v1, v2

    sub-int v12, v0, v1

    .line 182
    .local v12, "touchX":I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchY:I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$100(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)I

    move-result v0

    const/4 v1, 0x1

    aget v1, v11, v1

    const/4 v2, 0x1

    aget v2, v9, v2

    sub-int/2addr v1, v2

    sub-int v13, v0, v1

    .line 184
    .local v13, "touchY":I
    invoke-virtual {v6, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 185
    invoke-virtual {v7, v12, v13}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    const/4 v0, 0x1

    if-ne v8, v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectItem()V

    .line 194
    .end local v6    # "childView":Landroid/view/View;
    .end local v10    # "parent":Landroid/view/View;
    .end local v12    # "touchX":I
    .end local v13    # "touchY":I
    :cond_0
    :goto_1
    return-void

    .line 189
    .restart local v6    # "childView":Landroid/view/View;
    .restart local v10    # "parent":Landroid/view/View;
    .restart local v12    # "touchX":I
    .restart local v13    # "touchY":I
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v8, v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$200(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)I

    move-result v5

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->move(Landroid/animation/Animator$AnimatorListener;ZZZI)V
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$300(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Landroid/animation/Animator$AnimatorListener;ZZZI)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 177
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method
