.class public Lcom/navdy/hud/app/ui/component/carousel/ShrinkAnimator;
.super Lcom/navdy/hud/app/ui/component/carousel/CarouselAnimator;
.source "ShrinkAnimator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAnimator;-><init>()V

    return-void
.end method


# virtual methods
.method public createHiddenViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
    .locals 13
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f000000    # 0.5f

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 40
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 43
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    sget-object v5, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v5, :cond_0

    .line 44
    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newLeftView:Landroid/view/View;

    .line 45
    .local v2, "srcView":Landroid/view/View;
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 50
    .local v3, "targetPosView":Landroid/view/View;
    :goto_0
    const/high16 v4, 0x41a00000    # 20.0f

    .line 51
    .local v4, "targetSize":F
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v1, v4, v5

    .line 52
    .local v1, "scaleFactor":F
    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleX(F)V

    .line 53
    invoke-virtual {v2, v1}, Landroid/view/View;->setScaleY(F)V

    .line 54
    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v8

    sget-object v5, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v5, :cond_1

    const/4 v5, -0x1

    :goto_1
    iget v9, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    mul-int/2addr v5, v9

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v8, v5

    sget-object v5, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v5, :cond_2

    move v5, v7

    .line 55
    :goto_2
    int-to-float v5, v5

    add-float/2addr v5, v8

    .line 54
    invoke-virtual {v2, v5}, Landroid/view/View;->setX(F)V

    .line 56
    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v5

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v5, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float v8, v4, v8

    sub-float/2addr v5, v8

    invoke-virtual {v2, v5}, Landroid/view/View;->setY(F)V

    .line 57
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/Animator;

    const-string v8, "x"

    new-array v9, v6, [F

    .line 58
    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v10

    aput v10, v9, v7

    invoke-static {v2, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v5, v7

    const-string v8, "y"

    new-array v9, v6, [F

    .line 59
    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v10

    aput v10, v9, v7

    invoke-static {v2, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v8, 0x2

    const-string v9, "scaleX"

    new-array v10, v6, [F

    aput v12, v10, v7

    .line 60
    invoke-static {v2, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v5, v8

    const/4 v8, 0x3

    const-string v9, "scaleY"

    new-array v6, v6, [F

    aput v12, v6, v7

    .line 61
    invoke-static {v2, v9, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    aput-object v6, v5, v8

    .line 57
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 63
    invoke-virtual {v2, v11}, Landroid/view/View;->setPivotX(F)V

    .line 64
    invoke-virtual {v2, v11}, Landroid/view/View;->setPivotY(F)V

    .line 66
    return-object v0

    .line 47
    .end local v1    # "scaleFactor":F
    .end local v2    # "srcView":Landroid/view/View;
    .end local v3    # "targetPosView":Landroid/view/View;
    .end local v4    # "targetSize":F
    :cond_0
    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newRightView:Landroid/view/View;

    .line 48
    .restart local v2    # "srcView":Landroid/view/View;
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .restart local v3    # "targetPosView":Landroid/view/View;
    goto/16 :goto_0

    .restart local v1    # "scaleFactor":F
    .restart local v4    # "targetSize":F
    :cond_1
    move v5, v6

    .line 54
    goto :goto_1

    .line 55
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    goto :goto_2
.end method

.method public createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;
    .locals 13
    .param p1, "carousel"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .prologue
    const/high16 v12, 0x3f000000    # 0.5f

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 15
    const/high16 v5, 0x41a00000    # 20.0f

    .line 17
    .local v5, "targetSize":F
    sget-object v6, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v6, :cond_0

    .line 18
    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .line 19
    .local v4, "target":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getX()F

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v6, v7

    iget v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    add-float/2addr v6, v7

    div-float v7, v5, v8

    sub-float v1, v6, v7

    .line 24
    .local v1, "newX":F
    :goto_0
    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    add-float/2addr v6, v7

    div-float v7, v5, v8

    sub-float v2, v6, v7

    .line 25
    .local v2, "newY":F
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 26
    .local v3, "scaleFactor":F
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 27
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v6, 0x4

    new-array v6, v6, [Landroid/animation/Animator;

    const-string v7, "x"

    new-array v8, v11, [F

    aput v1, v8, v10

    .line 28
    invoke-static {v4, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    aput-object v7, v6, v10

    const-string v7, "y"

    new-array v8, v11, [F

    aput v2, v8, v10

    .line 29
    invoke-static {v4, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    aput-object v7, v6, v11

    const/4 v7, 0x2

    const-string v8, "scaleX"

    new-array v9, v11, [F

    aput v3, v9, v10

    .line 30
    invoke-static {v4, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "scaleY"

    new-array v9, v11, [F

    aput v3, v9, v10

    .line 31
    invoke-static {v4, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v6, v7

    .line 27
    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 32
    invoke-virtual {v4, v12}, Landroid/view/View;->setPivotX(F)V

    .line 33
    invoke-virtual {v4, v12}, Landroid/view/View;->setPivotY(F)V

    .line 35
    return-object v0

    .line 21
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v1    # "newX":F
    .end local v2    # "newY":F
    .end local v3    # "scaleFactor":F
    .end local v4    # "target":Landroid/view/View;
    :cond_0
    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 22
    .restart local v4    # "target":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getX()F

    move-result v6

    iget v7, p1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    div-float v7, v5, v8

    sub-float v1, v6, v7

    .restart local v1    # "newX":F
    goto :goto_0
.end method
