.class public interface abstract Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;
.super Ljava/lang/Object;
.source "Carousel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/carousel/Carousel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewProcessor"
.end annotation


# virtual methods
.method public abstract processInfoView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;I)V
.end method

.method public abstract processLargeImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;III)V
.end method

.method public abstract processSmallImageView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Landroid/view/View;III)V
.end method
