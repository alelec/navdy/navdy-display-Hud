.class Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "FastScrollAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endAnimation(Landroid/animation/Animator$AnimatorListener;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

.field final synthetic val$listener:Landroid/animation/Animator$AnimatorListener;

.field final synthetic val$newPos:I

.field final synthetic val$skipEnd:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;ZILandroid/animation/Animator$AnimatorListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    iput-boolean p2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->val$skipEnd:Z

    iput p3, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->val$newPos:I

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->val$listener:Landroid/animation/Animator$AnimatorListener;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 205
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endPending:Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$002(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Z)Z

    .line 207
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->val$skipEnd:Z

    if-nez v1, :cond_2

    .line 208
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->val$newPos:I

    iput v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 210
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;

    move-result-object v0

    .line 211
    .local v0, "newMiddleLeft":Landroid/view/View;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    # setter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$102(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;Landroid/view/View;)Landroid/view/View;

    .line 212
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 213
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->hiddenView:Landroid/view/View;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$100(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 214
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    .line 216
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->val$listener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->val$listener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v1, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 220
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    iput-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    .line 222
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-eqz v1, :cond_1

    .line 223
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v2

    iget v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v1, :cond_2

    .line 226
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget v3, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .line 227
    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v4

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    .line 226
    invoke-interface {v2, v3, v1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onCurrentItemChanged(II)V

    .line 231
    .end local v0    # "newMiddleLeft":Landroid/view/View;
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator$2;->this$0:Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    # getter for: Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->carouselLayout:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->access$200(Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    .line 232
    return-void
.end method
