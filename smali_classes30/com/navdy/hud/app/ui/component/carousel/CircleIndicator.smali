.class Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;
.super Landroid/widget/RelativeLayout;
.source "CircleIndicator.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/carousel/IProgressIndicator;


# instance fields
.field private circleFocusSize:I

.field private circleMargin:I

.field private circleSize:I

.field private final context:Landroid/content/Context;

.field private currentItem:I

.field private currentItemColor:I

.field private defaultColor:I

.field private id:I

.field private itemCount:I

.field private moveAnimatorView:Landroid/view/View;

.field private orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, -0x1

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 30
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    .line 31
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItemColor:I

    .line 36
    const/16 v0, 0x64

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->id:I

    .line 42
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->context:Landroid/content/Context;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, -0x1

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    .line 31
    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItemColor:I

    .line 36
    const/16 v0, 0x64

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->id:I

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->context:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->setViewPos(Landroid/view/View;)V

    return-void
.end method

.method private buildItems(I)V
    .locals 10
    .param p1, "n"    # I

    .prologue
    const/4 v8, 0x0

    .line 154
    if-nez p1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    const/4 v4, 0x0

    .line 160
    .local v4, "prev":Landroid/view/View;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildCount()I

    move-result v2

    .line 161
    .local v2, "nItems":I
    if-lez v2, :cond_2

    .line 162
    add-int/lit8 v6, v2, -0x1

    invoke-virtual {p0, v6}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 163
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 164
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v7, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v6, v7, :cond_7

    .line 165
    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 171
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    :goto_1
    move v0, v2

    .local v0, "i":I
    :goto_2
    add-int v6, p1, v2

    if-ge v0, v6, :cond_c

    .line 172
    new-instance v5, Landroid/view/View;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->context:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 173
    .local v5, "view":Landroid/view/View;
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->id:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->id:I

    invoke-virtual {v5, v6}, Landroid/view/View;->setId(I)V

    .line 174
    const v6, 0x7f02008b

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 175
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleSize:I

    iget v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleSize:I

    invoke-direct {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 177
    .local v1, "lytParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v0, :cond_3

    if-nez v4, :cond_3

    .line 178
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v7, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v6, v7, :cond_8

    .line 179
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 180
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 187
    :cond_3
    :goto_3
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    add-int/lit8 v6, v6, -0x1

    if-ne v0, v6, :cond_4

    .line 188
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v7, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v6, v7, :cond_9

    .line 189
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 195
    :cond_4
    :goto_4
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v7, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v6, v7, :cond_a

    .line 196
    const/16 v6, 0xf

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 197
    if-lez v0, :cond_5

    .line 198
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 200
    :cond_5
    if-eqz v4, :cond_6

    .line 201
    const/4 v6, 0x1

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 214
    :cond_6
    :goto_5
    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v5, v6}, Landroid/view/View;->setAlpha(F)V

    .line 215
    invoke-virtual {p0, v5, v1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 216
    move-object v4, v5

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 167
    .end local v0    # "i":I
    .end local v1    # "lytParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "view":Landroid/view/View;
    .restart local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_7
    iput v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_1

    .line 182
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v0    # "i":I
    .restart local v1    # "lytParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v5    # "view":Landroid/view/View;
    :cond_8
    const/16 v6, 0xa

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 183
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_3

    .line 191
    :cond_9
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_4

    .line 204
    :cond_a
    const/16 v6, 0xe

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 205
    if-lez v0, :cond_b

    .line 206
    iget v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 209
    :cond_b
    if-eqz v4, :cond_6

    .line 210
    const/4 v6, 0x3

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_5

    .line 220
    .end local v1    # "lytParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "view":Landroid/view/View;
    :cond_c
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    if-nez v6, :cond_0

    .line 221
    new-instance v6, Landroid/view/View;

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->context:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    .line 222
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    const v7, 0x7f02008c

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 223
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleFocusSize:I

    iget v9, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleFocusSize:I

    invoke-direct {v7, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 225
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {p0, v6}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private getTargetViewPos(Landroid/view/View;)I
    .locals 2
    .param p1, "targetView"    # Landroid/view/View;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v0, v1, :cond_0

    .line 281
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleFocusSize:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 283
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleFocusSize:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private setViewPos(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getTargetViewPos(Landroid/view/View;)I

    move-result v3

    int-to-float v2, v3

    .line 289
    .local v2, "pos":F
    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItemColor:I

    .line 290
    .local v1, "itemColor":I
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 291
    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->defaultColor:I

    .line 294
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v3, v4, :cond_1

    .line 295
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setX(F)V

    .line 299
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 300
    .local v0, "background":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 301
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 302
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->invalidate()V

    .line 303
    return-void

    .line 297
    .end local v0    # "background":Landroid/graphics/drawable/GradientDrawable;
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setY(F)V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    return v0
.end method

.method public getItemMoveAnimator(II)Landroid/animation/AnimatorSet;
    .locals 9
    .param p1, "toPos"    # I
    .param p2, "color"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 63
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    if-ne v4, v5, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-object v2

    .line 68
    :cond_1
    if-ltz p1, :cond_0

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    if-ge p1, v4, :cond_0

    .line 69
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 70
    .local v1, "currentItemView":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 72
    .local v3, "targetItemView":Landroid/view/View;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 73
    .local v0, "background":Landroid/graphics/drawable/GradientDrawable;
    if-ne p2, v5, :cond_2

    .line 74
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->defaultColor:I

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 79
    :goto_1
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 80
    .local v2, "set":Landroid/animation/AnimatorSet;
    new-instance v4, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$1;

    invoke-direct {v4, p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$1;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;I)V

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 86
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v6, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v4, v6, :cond_3

    sget-object v4, Landroid/view/View;->X:Landroid/util/Property;

    :goto_2
    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    .line 89
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getTargetViewPos(Landroid/view/View;)I

    move-result v8

    int-to-float v8, v8

    aput v8, v6, v7

    .line 87
    invoke-static {v5, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 86
    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 76
    .end local v2    # "set":Landroid/animation/AnimatorSet;
    :cond_2
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_1

    .line 86
    .restart local v2    # "set":Landroid/animation/AnimatorSet;
    :cond_3
    sget-object v4, Landroid/view/View;->Y:Landroid/util/Property;

    goto :goto_2
.end method

.method public getItemPos(I)Landroid/graphics/RectF;
    .locals 7
    .param p1, "n"    # I

    .prologue
    const/4 v6, 0x0

    .line 100
    if-ltz p1, :cond_0

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    if-ge p1, v2, :cond_0

    .line 101
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 102
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 103
    .local v0, "parent":Landroid/view/View;
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v2, v3, v4, v6, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 106
    .end local v0    # "parent":Landroid/view/View;
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCurrentItem(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 276
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->setCurrentItem(II)V

    .line 277
    return-void
.end method

.method public setCurrentItem(II)V
    .locals 4
    .param p1, "n"    # I
    .param p2, "color"    # I

    .prologue
    .line 236
    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    if-ne v2, p1, :cond_0

    .line 272
    :goto_0
    return-void

    .line 239
    :cond_0
    iput p2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItemColor:I

    .line 241
    if-ltz p1, :cond_1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    if-ge p1, v2, :cond_1

    .line 242
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 244
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v2, v3, :cond_2

    .line 245
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 249
    .local v0, "pos":I
    :goto_1
    if-nez v0, :cond_3

    .line 250
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;

    invoke-direct {v3, p0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator$2;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 271
    .end local v0    # "pos":I
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    :goto_2
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    goto :goto_0

    .line 247
    .restart local v1    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    .restart local v0    # "pos":I
    goto :goto_1

    .line 267
    :cond_3
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->setViewPos(Landroid/view/View;)V

    goto :goto_2
.end method

.method public setItemCount(I)V
    .locals 7
    .param p1, "n"    # I

    .prologue
    .line 111
    if-gez p1, :cond_0

    .line 112
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v5

    .line 114
    :cond_0
    const/4 v5, -0x1

    iput v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->currentItem:I

    .line 115
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildCount()I

    move-result v5

    if-nez v5, :cond_2

    .line 116
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    .line 117
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->buildItems(I)V

    .line 151
    :cond_1
    :goto_0
    return-void

    .line 120
    :cond_2
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    if-eq v5, p1, :cond_1

    .line 124
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    sub-int v1, v5, p1

    .line 125
    .local v1, "diff":I
    if-lez v1, :cond_6

    .line 127
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    .line 128
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->removeView(Landroid/view/View;)V

    .line 129
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_3

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->removeView(Landroid/view/View;)V

    .line 129
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 132
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildCount()I

    move-result v0

    .line 133
    .local v0, "count":I
    if-lez v0, :cond_4

    .line 134
    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 135
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 136
    .local v3, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    sget-object v6, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->HORIZONTAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    if-ne v5, v6, :cond_5

    .line 137
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 142
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "view":Landroid/view/View;
    :cond_4
    :goto_2
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 139
    .restart local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v4    # "view":Landroid/view/View;
    :cond_5
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    iput v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_2

    .line 145
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "view":Landroid/view/View;
    :cond_6
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->itemCount:I

    .line 146
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->removeView(Landroid/view/View;)V

    .line 147
    neg-int v5, v1

    invoke-direct {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->buildItems(I)V

    .line 148
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->moveAnimatorView:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V
    .locals 0
    .param p1, "orientation"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->orientation:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    .line 59
    return-void
.end method

.method public setProperties(IIII)V
    .locals 0
    .param p1, "circleSize"    # I
    .param p2, "circleFocusSize"    # I
    .param p3, "circleMargin"    # I
    .param p4, "defaultColor"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleSize:I

    .line 52
    iput p2, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleFocusSize:I

    .line 53
    iput p3, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->circleMargin:I

    .line 54
    iput p4, p0, Lcom/navdy/hud/app/ui/component/carousel/CircleIndicator;->defaultColor:I

    .line 55
    return-void
.end method
