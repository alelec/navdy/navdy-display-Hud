.class public interface abstract Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;
.super Ljava/lang/Object;
.source "Carousel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/carousel/Carousel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onCurrentItemChanged(II)V
.end method

.method public abstract onCurrentItemChanging(III)V
.end method

.method public abstract onExecuteItem(II)V
.end method

.method public abstract onExit()V
.end method
