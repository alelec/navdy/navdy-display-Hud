.class public interface abstract Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;
.super Ljava/lang/Object;
.source "AnimationStrategy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    }
.end annotation


# virtual methods
.method public abstract buildLayoutAnimation(Landroid/animation/Animator$AnimatorListener;Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;II)Landroid/animation/AnimatorSet;
.end method

.method public abstract createHiddenViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
.end method

.method public abstract createMiddleLeftViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
.end method

.method public abstract createMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
.end method

.method public abstract createNewMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
.end method

.method public abstract createSideViewToMiddleAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;
.end method

.method public abstract createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;
.end method
