.class public Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
.super Landroid/widget/FrameLayout;
.source "CarouselLayout.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;,
        Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DEFAULT_ANIM_DURATION:I = 0xfa

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private animationDuration:I

.field animationRunning:Z

.field private animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

.field carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

.field carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

.field currentItem:I

.field protected detector:Lcom/navdy/hud/app/gesture/GestureDetector;

.field private exitOnDoubleClick:Z

.field private fastScrollAnimation:Z

.field private handler:Landroid/os/Handler;

.field imageLytResourceId:I

.field inflater:Landroid/view/LayoutInflater;

.field infoLayoutResourceId:I

.field interpolator:Landroid/view/animation/Interpolator;

.field itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

.field lastScrollAnimationOperation:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

.field private lastTouchX:I

.field private lastTouchY:I

.field leftView:Landroid/view/View;

.field mainImageSize:I

.field mainLeftPadding:I

.field mainRightPadding:I

.field mainViewDividerPadding:I

.field middleLeftView:Landroid/view/View;

.field middleRightView:Landroid/view/View;

.field model:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;",
            ">;"
        }
    .end annotation
.end field

.field private multipleClickGestureDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

.field newLeftView:Landroid/view/View;

.field newMiddleLeftView:Landroid/view/View;

.field newMiddleRightView:Landroid/view/View;

.field newRightView:Landroid/view/View;

.field private operationQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;",
            ">;"
        }
    .end annotation
.end field

.field rightImageStart:I

.field rightSectionHeight:I

.field rightSectionWidth:I

.field rightView:Landroid/view/View;

.field rootContainer:Landroid/view/View;

.field selectedItemView:Landroid/view/View;

.field sideImageSize:I

.field private viewCacheManager:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;

.field viewPadding:I

.field viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

.field viewsScaled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-class v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->$assertionsDisabled:Z

    .line 47
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->handler:Landroid/os/Handler;

    .line 125
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 142
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewCacheManager:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;

    .line 967
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureDetector;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/gesture/GestureDetector;-><init>(Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->detector:Lcom/navdy/hud/app/gesture/GestureDetector;

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->handler:Landroid/os/Handler;

    .line 125
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 142
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewCacheManager:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;

    .line 967
    new-instance v0, Lcom/navdy/hud/app/gesture/GestureDetector;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/gesture/GestureDetector;-><init>(Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->detector:Lcom/navdy/hud/app/gesture/GestureDetector;

    .line 150
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->inflater:Landroid/view/LayoutInflater;

    .line 152
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 153
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setWillNotDraw(Z)V

    .line 155
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$1;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 167
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$2;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 46
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchX:I

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchX:I

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 46
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchY:I

    return v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastTouchY:I

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 46
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Landroid/animation/Animator$AnimatorListener;ZZZI)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z
    .param p5, "x5"    # I

    .prologue
    .line 46
    invoke-direct/range {p0 .. p5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->move(Landroid/animation/Animator$AnimatorListener;ZZZI)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Ljava/util/List;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setModelInternal(Ljava/util/List;IZ)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setCurrentItem(IZ)V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Landroid/animation/Animator$AnimatorListener;ILcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p4, "x4"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->doInsert(Landroid/animation/Animator$AnimatorListener;ILcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->changeSelectedItem(ZZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    return-void
.end method

.method private buildLayoutAnimation(Landroid/animation/Animator$AnimatorListener;II)Landroid/animation/AnimatorSet;
    .locals 11
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "pos"    # I
    .param p3, "newPos"    # I

    .prologue
    .line 648
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 652
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    if-le p3, p2, :cond_1

    const/4 v4, 0x1

    .line 653
    .local v4, "next":Z
    :goto_0
    if-eqz v4, :cond_2

    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 657
    .local v2, "direction":Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    :goto_1
    iget v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainViewDividerPadding:I

    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int v5, v7, v8

    .line 659
    .local v5, "offset":I
    if-eqz v4, :cond_4

    .line 660
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getX()F

    move-result v7

    float-to-int v7, v7

    add-int/2addr v7, v5

    const/4 v8, 0x0

    invoke-virtual {p0, p3, v7, v8}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addMiddleRightView(IIZ)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    .line 661
    add-int/lit8 v8, p3, 0x1

    sget-object v9, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    add-int/lit8 v7, p3, 0x1

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v7, v10, :cond_3

    const/4 v7, 0x1

    :goto_2
    invoke-virtual {p0, v8, v9, v7}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addHiddenView(ILcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;Z)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newRightView:Landroid/view/View;

    .line 669
    :goto_3
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    invoke-interface {v7, p0, v2}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    .line 672
    .local v1, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    invoke-interface {v7, p0, v2}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createMiddleLeftViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;

    move-result-object v6

    .line 673
    .local v6, "subAnimatorSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v1, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 676
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    invoke-interface {v7, p0, v2}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;

    move-result-object v6

    .line 677
    invoke-virtual {v1, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 680
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    invoke-interface {v7, p0, v2}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createSideViewToMiddleAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;

    move-result-object v6

    .line 681
    invoke-virtual {v1, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 684
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    invoke-interface {v7, p0, v2}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createNewMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;

    move-result-object v6

    .line 685
    invoke-virtual {v1, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 687
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    invoke-interface {v7, p0, v2}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createHiddenViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;

    move-result-object v6

    .line 688
    invoke-virtual {v1, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 691
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-eqz v7, :cond_0

    .line 692
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    const/4 v8, -0x1

    invoke-virtual {v7, p3, v8}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemMoveAnimator(II)Landroid/animation/AnimatorSet;

    move-result-object v3

    .line 693
    .local v3, "indicatorAnimator":Landroid/animation/AnimatorSet;
    if-eqz v3, :cond_0

    .line 694
    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 698
    .end local v3    # "indicatorAnimator":Landroid/animation/AnimatorSet;
    :cond_0
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 701
    iget v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 702
    invoke-direct {p0, p1, v4, v7, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildListener(Landroid/animation/Animator$AnimatorListener;ZII)Landroid/animation/Animator$AnimatorListener;

    move-result-object v7

    .line 701
    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 706
    return-object v0

    .line 652
    .end local v1    # "builder":Landroid/animation/AnimatorSet$Builder;
    .end local v2    # "direction":Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    .end local v4    # "next":Z
    .end local v5    # "offset":I
    .end local v6    # "subAnimatorSet":Landroid/animation/AnimatorSet;
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 653
    .restart local v4    # "next":Z
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    goto/16 :goto_1

    .line 661
    .restart local v2    # "direction":Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    .restart local v5    # "offset":I
    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    .line 663
    :cond_4
    add-int/lit8 v8, p3, -0x1

    sget-object v9, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-lez p3, :cond_5

    const/4 v7, 0x1

    :goto_4
    invoke-virtual {p0, v8, v9, v7}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addHiddenView(ILcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;Z)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newLeftView:Landroid/view/View;

    .line 664
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getX()F

    move-result v7

    float-to-int v7, v7

    add-int/2addr v7, v5

    const/4 v8, 0x0

    invoke-virtual {p0, p3, v7, v8}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addMiddleRightView(IIZ)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    .line 665
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_3

    .line 663
    :cond_5
    const/4 v7, 0x0

    goto :goto_4
.end method

.method private buildListener(Landroid/animation/Animator$AnimatorListener;ZII)Landroid/animation/Animator$AnimatorListener;
    .locals 6
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "next"    # Z
    .param p3, "oldPos"    # I
    .param p4, "newPos"    # I

    .prologue
    .line 714
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move v4, p4

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Landroid/animation/Animator$AnimatorListener;IIZ)V

    return-object v0
.end method

.method private changeSelectedItem(ZZ)V
    .locals 1
    .param p1, "next"    # Z
    .param p2, "keyPress"    # Z

    .prologue
    .line 841
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 850
    :goto_0
    return-void

    .line 844
    :cond_0
    if-eqz p1, :cond_1

    .line 846
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    goto :goto_0

    .line 848
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    goto :goto_0
.end method

.method private doInsert(Landroid/animation/Animator$AnimatorListener;ILcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Z)V
    .locals 18
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "pos"    # I
    .param p3, "item"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p4, "preserveCurrentItem"    # Z

    .prologue
    .line 570
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-interface {v3, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 572
    new-instance v11, Landroid/animation/AnimatorSet;

    invoke-direct {v11}, Landroid/animation/AnimatorSet;-><init>()V

    .line 574
    .local v11, "animatorSet":Landroid/animation/AnimatorSet;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 576
    .local v10, "animatorCollection":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/animation/Animator;>;"
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    move/from16 v16, v0

    .line 577
    .local v16, "previous":I
    sget-object v12, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 579
    .local v12, "direction":Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    .line 580
    .local v13, "leftX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightImageStart:I

    add-int v17, v3, v4

    .line 581
    .local v17, "rightX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainLeftPadding:I

    add-int v14, v3, v4

    .line 582
    .local v14, "middleLeftX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainLeftPadding:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    add-int v15, v3, v4

    .line 584
    .local v15, "middleRightX":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    add-int/lit8 v3, v3, 0x1

    move/from16 v0, p2

    if-le v0, v3, :cond_1

    .line 631
    :cond_0
    :goto_0
    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne v12, v3, :cond_7

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 632
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v16

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildListener(Landroid/animation/Animator$AnimatorListener;ZII)Landroid/animation/Animator$AnimatorListener;

    move-result-object v3

    .line 631
    invoke-virtual {v11, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 637
    invoke-virtual {v11, v10}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 638
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    .line 639
    invoke-virtual {v11}, Landroid/animation/AnimatorSet;->start()V

    .line 640
    :goto_2
    return-void

    .line 586
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    add-int/lit8 v3, v3, 0x1

    move/from16 v0, p2

    if-ne v0, v3, :cond_2

    .line 587
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addSideView(IIZ)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newRightView:Landroid/view/View;

    .line 589
    sget-object v12, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 590
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v12}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;

    move-result-object v9

    .line 591
    .local v9, "a1":Landroid/animation/Animator;
    invoke-interface {v10, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 592
    .end local v9    # "a1":Landroid/animation/Animator;
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    move/from16 v0, p2

    if-ne v0, v3, :cond_5

    .line 593
    if-eqz p4, :cond_3

    .line 595
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1, v13, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addSideView(IIZ)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newLeftView:Landroid/view/View;

    .line 597
    sget-object v12, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->LEFT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 598
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v12}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;

    move-result-object v9

    .line 599
    .restart local v9    # "a1":Landroid/animation/Animator;
    invoke-interface {v10, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 600
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    goto :goto_0

    .line 603
    .end local v9    # "a1":Landroid/animation/Animator;
    :cond_3
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1, v14, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addMiddleLeftView(IIZ)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleLeftView:Landroid/view/View;

    .line 604
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1, v15, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addMiddleRightView(IIZ)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    .line 607
    sget-object v12, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    .line 609
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    if-eqz v3, :cond_4

    .line 610
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v12}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createViewOutAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/Animator;

    move-result-object v9

    .line 611
    .restart local v9    # "a1":Landroid/animation/Animator;
    invoke-interface {v10, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 614
    .end local v9    # "a1":Landroid/animation/Animator;
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v12}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createMiddleLeftViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;

    move-result-object v9

    .line 615
    .restart local v9    # "a1":Landroid/animation/Animator;
    invoke-interface {v10, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 617
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v12}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->createMiddleRightViewAnimation(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;)Landroid/animation/AnimatorSet;

    move-result-object v9

    .line 618
    invoke-interface {v10, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 620
    .end local v9    # "a1":Landroid/animation/Animator;
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, p2

    if-gt v0, v3, :cond_0

    .line 621
    if-eqz p4, :cond_6

    .line 623
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    goto/16 :goto_0

    .line 626
    :cond_6
    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->move(Landroid/animation/Animator$AnimatorListener;ZZZI)V

    goto/16 :goto_2

    .line 631
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_1
.end method

.method private handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 10
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1015
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    const/4 v4, -0x1

    if-ne v0, v4, :cond_0

    .line 1035
    :goto_0
    return v3

    .line 1019
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$10;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    move v3, v2

    .line 1035
    goto :goto_0

    .line 1021
    :pswitch_0
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I

    move-object v0, p0

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->move(Landroid/animation/Animator$AnimatorListener;ZZZI)V

    goto :goto_0

    .line 1026
    :pswitch_1
    iget v9, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I

    move-object v4, p0

    move-object v5, v1

    move v6, v3

    move v7, v3

    move v8, v2

    invoke-direct/range {v4 .. v9}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->move(Landroid/animation/Animator$AnimatorListener;ZZZI)V

    goto :goto_0

    .line 1031
    :pswitch_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectItem()V

    goto :goto_0

    .line 1019
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 200
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/R$styleable;->Carousel:[I

    invoke-virtual {v4, p2, v5, v3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 205
    .local v0, "a":Landroid/content/res/TypedArray;
    sget-boolean v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez v0, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 209
    .local v1, "resources":Landroid/content/res/Resources;
    const/4 v4, 0x0

    const/16 v5, 0xfa

    :try_start_0
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I

    .line 212
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    const v6, 0x10a0004

    .line 213
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 212
    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->interpolator:Landroid/view/animation/Interpolator;

    .line 216
    const/4 v4, 0x2

    const v5, 0x7f0b0049

    .line 217
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 216
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    .line 218
    const/4 v4, 0x3

    const v5, 0x7f0b0048

    .line 219
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 218
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    .line 220
    const/4 v4, 0x4

    const v5, 0x7f0b0035

    .line 221
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 220
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    .line 222
    const/4 v4, 0x7

    const v5, 0x7f0b0038

    .line 223
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 222
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightImageStart:I

    .line 224
    const/4 v4, 0x5

    const v5, 0x7f0b0036

    .line 225
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 224
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainLeftPadding:I

    .line 226
    const/4 v4, 0x6

    const v5, 0x7f0b0037

    .line 227
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 226
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainRightPadding:I

    .line 231
    const/16 v4, 0x9

    const v5, 0x7f0b0039

    .line 232
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 231
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightSectionWidth:I

    .line 234
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    if-eq v4, v5, :cond_1

    :goto_0
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewsScaled:Z

    .line 235
    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "view scaled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewsScaled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 239
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b006f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightSectionHeight:I

    .line 240
    return-void

    :cond_1
    move v2, v3

    .line 234
    goto :goto_0

    .line 237
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v2
.end method

.method private move(Landroid/animation/Animator$AnimatorListener;ZZZI)V
    .locals 14
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "next"    # Z
    .param p3, "keyPress"    # Z
    .param p4, "ignoreAnimationRunning"    # Z
    .param p5, "duration"    # I

    .prologue
    .line 476
    if-eqz p2, :cond_0

    .line 477
    sget-object v11, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->FORWARD:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 482
    .local v11, "operation":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    :goto_0
    if-nez p4, :cond_4

    .line 483
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    if-eqz v3, :cond_4

    .line 484
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->fastScrollAnimation:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_1

    .line 553
    :goto_1
    return-void

    .line 479
    .end local v11    # "operation":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    :cond_0
    sget-object v11, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->BACK:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .restart local v11    # "operation":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    goto :goto_0

    .line 490
    :cond_1
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->fastScrollAnimation:Z

    if-eqz v3, :cond_3

    .line 491
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 492
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;

    .line 493
    .local v9, "last":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    iget-object v3, v9, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->type:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    if-ne v3, v11, :cond_3

    .line 494
    iget v3, v9, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v9, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->count:I

    goto :goto_1

    .line 499
    .end local v9    # "last":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastScrollAnimationOperation:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    if-ne v3, v11, :cond_3

    .line 501
    const/16 p5, 0x32

    .line 506
    :cond_3
    new-instance v7, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;

    const/4 v3, 0x0

    move/from16 v0, p5

    invoke-direct {v7, v11, v3, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;Ljava/lang/Runnable;I)V

    .line 507
    .local v7, "operationInfo":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    new-instance v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$6;

    move-object v3, p0

    move-object v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$6;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Landroid/animation/Animator$AnimatorListener;ZZLcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;)V

    .line 513
    .local v2, "runnable":Ljava/lang/Runnable;
    iput-object v2, v7, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->runnable:Ljava/lang/Runnable;

    .line 514
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v3, v7}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 519
    .end local v2    # "runnable":Ljava/lang/Runnable;
    .end local v7    # "operationInfo":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    :cond_4
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    if-eqz p2, :cond_7

    const/4 v3, 0x1

    :goto_2
    add-int v10, v4, v3

    .line 521
    .local v10, "newPos":I
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    if-ltz v3, :cond_5

    if-ltz v10, :cond_5

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->getCount()I

    move-result v3

    if-lt v10, v3, :cond_9

    .line 522
    :cond_5
    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 523
    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot go "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p2, :cond_8

    const-string v3, "next"

    :goto_3
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 525
    :cond_6
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    goto :goto_1

    .line 519
    .end local v10    # "newPos":I
    :cond_7
    const/4 v3, -0x1

    goto :goto_2

    .line 523
    .restart local v10    # "newPos":I
    :cond_8
    const-string v3, "prev"

    goto :goto_3

    .line 529
    :cond_9
    iput-object v11, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastScrollAnimationOperation:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 531
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    instance-of v3, v3, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    if-eqz v3, :cond_b

    .line 532
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-interface {v3, p1, p0, v4, v10}, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;->buildLayoutAnimation(Landroid/animation/Animator$AnimatorListener;Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;II)Landroid/animation/AnimatorSet;

    move-result-object v8

    .line 533
    .local v8, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    .line 535
    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 536
    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "move:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " duration ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->getDuration()J

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 539
    :cond_a
    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->start()V

    .line 552
    :goto_4
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v3}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    goto/16 :goto_1

    .line 541
    .end local v8    # "animatorSet":Landroid/animation/AnimatorSet;
    :cond_b
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-direct {p0, p1, v3, v10}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildLayoutAnimation(Landroid/animation/Animator$AnimatorListener;II)Landroid/animation/AnimatorSet;

    move-result-object v8

    .line 542
    .restart local v8    # "animatorSet":Landroid/animation/AnimatorSet;
    move/from16 v0, p5

    int-to-long v4, v0

    invoke-virtual {v8, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 543
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    .line 545
    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 546
    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "move:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " duration ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->getDuration()J

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 549
    :cond_c
    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_4
.end method

.method private recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V
    .locals 6
    .param p1, "viewType"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 804
    if-nez p2, :cond_0

    .line 837
    :goto_0
    return-void

    .line 809
    :cond_0
    invoke-virtual {p0, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->removeView(Landroid/view/View;)V

    .line 811
    invoke-virtual {p2, v2}, Landroid/view/View;->setScaleX(F)V

    .line 812
    invoke-virtual {p2, v2}, Landroid/view/View;->setScaleY(F)V

    .line 813
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 814
    invoke-virtual {p2, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 815
    invoke-virtual {p2, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 816
    invoke-virtual {p2, v2}, Landroid/view/View;->setAlpha(F)V

    .line 817
    instance-of v2, p2, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    if-eqz v2, :cond_2

    move-object v0, p2

    .line 818
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 819
    .local v0, "crossfadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 820
    .local v1, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 821
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 822
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 823
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewsScaled:Z

    if-eqz v2, :cond_1

    instance-of v2, v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    if-eqz v2, :cond_1

    .line 824
    check-cast v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .end local v1    # "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setScaled(Z)V

    .line 827
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 828
    .restart local v1    # "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 829
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 830
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 831
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewsScaled:Z

    if-eqz v2, :cond_2

    instance-of v2, v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    if-eqz v2, :cond_2

    .line 832
    check-cast v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .end local v1    # "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setScaled(Z)V

    .line 836
    .end local v0    # "crossfadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewCacheManager:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;

    invoke-virtual {v2, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->putView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    goto :goto_0
.end method

.method private recycleViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 790
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    .line 791
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 793
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    .line 794
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .line 796
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    .line 797
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    .line 799
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_RIGHT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    .line 800
    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    .line 801
    return-void
.end method

.method private setCurrentItem(IZ)V
    .locals 6
    .param p1, "position"    # I
    .param p2, "callOnCurrentItemChanging"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 404
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 408
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleViews()V

    .line 412
    :cond_2
    add-int/lit8 v3, p1, -0x1

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    if-lez p1, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {p0, v3, v4, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addSideView(IIZ)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 415
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainLeftPadding:I

    add-int/2addr v0, v3

    invoke-virtual {p0, p1, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addMiddleLeftView(IIZ)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    .line 420
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainLeftPadding:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    add-int/2addr v0, v3

    invoke-virtual {p0, p1, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addMiddleRightView(IIZ)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    .line 425
    add-int/lit8 v0, p1, 0x1

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightImageStart:I

    add-int/2addr v3, v4

    add-int/lit8 v4, p1, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    .line 426
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_7

    .line 425
    :goto_2
    invoke-virtual {p0, v0, v3, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addSideView(IIZ)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .line 429
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v0, :cond_3

    .line 430
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    invoke-interface {v1, v2, p1, v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onCurrentItemChanging(III)V

    .line 433
    :cond_3
    iput p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 434
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-eqz v0, :cond_4

    .line 435
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 437
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v0, :cond_5

    .line 438
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    invoke-interface {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onCurrentItemChanged(II)V

    .line 440
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 412
    goto/16 :goto_1

    :cond_7
    move v1, v2

    .line 426
    goto :goto_2
.end method

.method private setModelInternal(Ljava/util/List;IZ)V
    .locals 9
    .param p2, "position"    # I
    .param p3, "animated"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .local p1, "newModel":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;>;"
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 360
    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    if-eqz p3, :cond_3

    .line 361
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 363
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 364
    .local v2, "middleAnim":Landroid/animation/Animator;
    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    .line 366
    .local v1, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 367
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 370
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 371
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 374
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    if-eqz v3, :cond_2

    .line 375
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 378
    :cond_2
    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 379
    new-instance v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;

    invoke-direct {v3, p0, p1, p2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$5;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Ljava/util/List;I)V

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 394
    iput-boolean v7, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    .line 395
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 401
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v1    # "builder":Landroid/animation/AnimatorSet$Builder;
    .end local v2    # "middleAnim":Landroid/animation/Animator;
    :goto_0
    return-void

    .line 397
    :cond_3
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    .line 398
    invoke-direct {p0, p2, v6}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setCurrentItem(IZ)V

    .line 399
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    goto :goto_0
.end method


# virtual methods
.method addHiddenView(ILcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;Z)Landroid/view/View;
    .locals 3
    .param p1, "item"    # I
    .param p2, "direction"    # Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;
    .param p3, "visible"    # Z

    .prologue
    .line 959
    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;->RIGHT:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy$Direction;

    if-ne p2, v1, :cond_0

    .line 960
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainViewDividerPadding:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightSectionWidth:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainRightPadding:I

    int-to-float v2, v2

    add-float v0, v1, v2

    .line 964
    .local v0, "x":F
    :goto_0
    float-to-int v1, v0

    invoke-virtual {p0, p1, v1, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addSideView(IIZ)Landroid/view/View;

    move-result-object v1

    return-object v1

    .line 962
    .end local v0    # "x":F
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    neg-float v0, v1

    .restart local v0    # "x":F
    goto :goto_0
.end method

.method addMiddleLeftView(IIZ)Landroid/view/View;
    .locals 1
    .param p1, "item"    # I
    .param p2, "xPos"    # I
    .param p3, "visible"    # Z

    .prologue
    .line 950
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_LEFT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildView(IILcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method addMiddleRightView(IIZ)Landroid/view/View;
    .locals 1
    .param p1, "item"    # I
    .param p2, "xPos"    # I
    .param p3, "visible"    # Z

    .prologue
    .line 954
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_RIGHT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildView(IILcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method addSideView(IIZ)Landroid/view/View;
    .locals 1
    .param p1, "item"    # I
    .param p2, "xPos"    # I
    .param p3, "visible"    # Z

    .prologue
    .line 946
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildView(IILcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method buildView(IILcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Z)Landroid/view/View;
    .locals 12
    .param p1, "item"    # I
    .param p2, "xPos"    # I
    .param p3, "viewType"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;
    .param p4, "visible"    # Z

    .prologue
    .line 893
    const/4 v5, 0x0

    .line 894
    .local v5, "w":I
    const/4 v8, 0x0

    .line 895
    .local v8, "h":I
    const/4 v4, 0x0

    .line 897
    .local v4, "layoutId":I
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$10;->$SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType:[I

    invoke-virtual {p3}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 917
    :goto_0
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v9, v5, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 918
    .local v9, "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v0, 0x10

    iput v0, v9, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 919
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewCacheManager:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;

    invoke-virtual {v0, p3}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewCacheManager;->getView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;)Landroid/view/View;

    move-result-object v2

    .line 920
    .local v2, "convertView":Landroid/view/View;
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    move v1, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;->getView(ILandroid/view/View;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;II)Landroid/view/View;

    move-result-object v11

    .line 922
    .local v11, "view":Landroid/view/View;
    if-nez v2, :cond_1

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewsScaled:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    if-ne p3, v0, :cond_1

    .line 923
    instance-of v0, v11, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    if-eqz v0, :cond_1

    move-object v7, v11

    .line 924
    check-cast v7, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 925
    .local v7, "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v6

    .line 927
    .local v6, "big":Landroid/view/View;
    instance-of v0, v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 928
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setScaled(Z)V

    .line 931
    :cond_0
    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v10

    .line 933
    .local v10, "small":Landroid/view/View;
    instance-of v0, v10, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    if-eqz v0, :cond_1

    .line 934
    check-cast v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .end local v6    # "big":Landroid/view/View;
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setScaled(Z)V

    .line 938
    .end local v7    # "crossFadeImageView":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    .end local v10    # "small":Landroid/view/View;
    :cond_1
    int-to-float v0, p2

    invoke-virtual {v11, v0}, Landroid/view/View;->setX(F)V

    .line 939
    if-eqz p4, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 940
    invoke-virtual {p0, v11, v9}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 941
    return-object v11

    .line 899
    .end local v2    # "convertView":Landroid/view/View;
    .end local v9    # "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    .end local v11    # "view":Landroid/view/View;
    :pswitch_0
    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    .line 900
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    .line 901
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    .line 902
    goto :goto_0

    .line 905
    :pswitch_1
    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    .line 906
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->mainImageSize:I

    .line 907
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    .line 908
    goto :goto_0

    .line 911
    :pswitch_2
    iget v8, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightSectionHeight:I

    .line 912
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightSectionWidth:I

    .line 913
    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->infoLayoutResourceId:I

    goto :goto_0

    .line 939
    .restart local v2    # "convertView":Landroid/view/View;
    .restart local v9    # "lytParams":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v11    # "view":Landroid/view/View;
    :cond_2
    const/4 v0, 0x4

    goto :goto_1

    .line 897
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public clearOperationQueue()V
    .locals 1

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1069
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    return v0
.end method

.method public getCurrentModel()Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    return-object v0
.end method

.method public getListener()Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    return-object v0
.end method

.method public getModel(I)Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .locals 1
    .param p1, "item"    # I

    .prologue
    .line 327
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 328
    :cond_0
    const/4 v0, 0x0

    .line 330
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    goto :goto_0
.end method

.method public init(Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;)V
    .locals 3
    .param p1, "params"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 248
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 250
    :cond_0
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->model:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->rootContainer:Landroid/view/View;

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->infoLayoutResourceId:I

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->imageLytResourceId:I

    if-nez v0, :cond_2

    .line 254
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 256
    :cond_2
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->model:Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    .line 257
    iget v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->infoLayoutResourceId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->infoLayoutResourceId:I

    .line 258
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewProcessor:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewProcessor;

    .line 259
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->rootContainer:Landroid/view/View;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rootContainer:Landroid/view/View;

    .line 260
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 261
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselAdapter:Lcom/navdy/hud/app/ui/component/carousel/CarouselAdapter;

    .line 262
    iget v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->imageLytResourceId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->imageLytResourceId:I

    .line 263
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->fastScrollAnimation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->fastScrollAnimation:Z

    .line 264
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->exitOnDoubleClick:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->exitOnDoubleClick:Z

    .line 265
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$InitParams;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    :goto_0
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    .line 268
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->exitOnDoubleClick:Z

    if-eqz v0, :cond_3

    .line 269
    new-instance v0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    const/4 v1, 0x2

    new-instance v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$3;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;-><init>(ILcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->multipleClickGestureDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    .line 293
    :cond_3
    return-void

    .line 265
    :cond_4
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->fastScrollAnimation:Z

    if-eqz v0, :cond_5

    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselAnimator;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselAnimator;-><init>()V

    goto :goto_0
.end method

.method public insert(Landroid/animation/Animator$AnimatorListener;ILcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Z)V
    .locals 6
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;
    .param p2, "pos"    # I
    .param p3, "item"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;
    .param p4, "preserveCurrentItem"    # Z

    .prologue
    .line 556
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    if-eqz v1, :cond_0

    .line 557
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$7;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$7;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Landroid/animation/Animator$AnimatorListener;ILcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Z)V

    .line 563
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    new-instance v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->OTHER:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    invoke-direct {v2, v3, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 567
    .end local v0    # "runnable":Ljava/lang/Runnable;
    :goto_0
    return-void

    .line 565
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->doInsert(Landroid/animation/Animator$AnimatorListener;ILcom/navdy/hud/app/ui/component/carousel/Carousel$Model;Z)V

    goto :goto_0
.end method

.method public isAnimationEndPending()Z
    .locals 2

    .prologue
    .line 1098
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    instance-of v1, v1, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    if-eqz v1, :cond_0

    .line 1099
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .line 1100
    .local v0, "fastScrollAnimator":Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->isEndPending()Z

    move-result v1

    .line 1102
    .end local v0    # "fastScrollAnimator":Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method isAnimationPending()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1082
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1083
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;

    .line 1084
    .local v0, "info":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->type:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastScrollAnimationOperation:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    if-ne v2, v3, :cond_0

    .line 1086
    const/4 v1, 0x1

    .line 1093
    .end local v0    # "info":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    :cond_0
    return v1
.end method

.method public moveNext(Landroid/animation/Animator$AnimatorListener;)V
    .locals 6
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    const/4 v3, 0x0

    .line 467
    const/4 v2, 0x1

    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->move(Landroid/animation/Animator$AnimatorListener;ZZZI)V

    .line 468
    return-void
.end method

.method public movePrevious(Landroid/animation/Animator$AnimatorListener;)V
    .locals 6
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    const/4 v2, 0x0

    .line 459
    iget v5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationDuration:I

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->move(Landroid/animation/Animator$AnimatorListener;ZZZI)V

    .line 460
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 1040
    const/4 v0, 0x0

    return-object v0
.end method

.method public notifyDatasetChanged(Ljava/util/List;I)V
    .locals 1
    .param p2, "selectedItem"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1072
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    .line 1073
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setCurrentItem(IZ)V

    .line 1074
    return-void
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 971
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 979
    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 980
    const/4 v1, 0x0

    .line 1002
    :goto_0
    return v1

    .line 982
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->detector:Lcom/navdy/hud/app/gesture/GestureDetector;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/gesture/GestureDetector;->onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    .line 983
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 984
    .local v0, "gesture":Lcom/navdy/service/library/events/input/Gesture;
    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$10;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    invoke-virtual {v0}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1002
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 984
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->multipleClickGestureDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->multipleClickGestureDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    .line 1010
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 975
    return-void
.end method

.method public reload()V
    .locals 2

    .prologue
    .line 1078
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setCurrentItem(IZ)V

    .line 1079
    return-void
.end method

.method runQueuedOperation()V
    .locals 4

    .prologue
    .line 1044
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 1045
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;

    .line 1046
    .local v1, "operationInfo":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    iget v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->count:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1047
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 1051
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->handler:Landroid/os/Handler;

    iget-object v3, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1065
    .end local v1    # "operationInfo":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    :goto_1
    return-void

    .line 1049
    .restart local v1    # "operationInfo":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    :cond_0
    iget v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->count:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;->count:I

    goto :goto_0

    .line 1053
    .end local v1    # "operationInfo":Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    instance-of v2, v2, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    if-eqz v2, :cond_2

    .line 1054
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animator:Lcom/navdy/hud/app/ui/component/carousel/AnimationStrategy;

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;

    .line 1055
    .local v0, "fastScrollAnimator":Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->isEndPending()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1057
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;->endAnimation()V

    goto :goto_1

    .line 1062
    .end local v0    # "fastScrollAnimator":Lcom/navdy/hud/app/ui/component/carousel/FastScrollAnimator;
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->lastScrollAnimationOperation:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 1063
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    goto :goto_1
.end method

.method public selectItem()V
    .locals 5

    .prologue
    .line 853
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    if-eqz v2, :cond_0

    .line 854
    new-instance v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$9;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$9;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;)V

    .line 861
    .local v1, "runnable":Ljava/lang/Runnable;
    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "queueing up select event"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 862
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    new-instance v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;

    sget-object v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->SELECT:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    invoke-direct {v3, v4, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;Ljava/lang/Runnable;)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 890
    .end local v1    # "runnable":Ljava/lang/Runnable;
    :goto_0
    return-void

    .line 867
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->clearOperationQueue()V

    .line 869
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    if-nez v2, :cond_1

    .line 870
    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "no item selected"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 871
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    goto :goto_0

    .line 875
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    if-ne v2, v3, :cond_3

    .line 876
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->selectedItemView:Landroid/view/View;

    const v3, 0x7f0e0011

    invoke-virtual {v2, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 877
    .local v0, "id":I
    sget-object v2, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_SELECT:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v2}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 878
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v2, :cond_2

    .line 879
    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "execute item:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 880
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-interface {v2, v0, v3}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onExecuteItem(II)V

    goto :goto_0

    .line 882
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "no carousel listener"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 883
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    goto :goto_0

    .line 887
    .end local v0    # "id":I
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "no match"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 888
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    goto :goto_0
.end method

.method public setCurrentItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 451
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setCurrentItem(IZ)V

    .line 452
    return-void
.end method

.method public setListener(Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;)V
    .locals 0
    .param p1, "carouselListener"    # Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    .prologue
    .line 300
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    .line 301
    return-void
.end method

.method public setModel(Ljava/util/List;IZ)V
    .locals 4
    .param p2, "position"    # I
    .param p3, "animated"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 342
    .local p1, "newModel":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;>;"
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->animationRunning:Z

    if-eqz v1, :cond_0

    .line 343
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->clearOperationQueue()V

    .line 344
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$4;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$4;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Ljava/util/List;IZ)V

    .line 350
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->operationQueue:Ljava/util/Queue;

    new-instance v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->OTHER:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    invoke-direct {v2, v3, v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$OperationInfo;-><init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 354
    .end local v0    # "runnable":Ljava/lang/Runnable;
    :goto_0
    return-void

    .line 352
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->setModelInternal(Ljava/util/List;IZ)V

    goto :goto_0
.end method
