.class final enum Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
.super Ljava/lang/Enum;
.source "CarouselLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Operation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

.field public static final enum BACK:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

.field public static final enum FORWARD:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

.field public static final enum OTHER:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

.field public static final enum SELECT:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    const-string v1, "FORWARD"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->FORWARD:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 51
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    const-string v1, "BACK"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->BACK:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 52
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->SELECT:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 53
    new-instance v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->OTHER:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->FORWARD:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->BACK:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->SELECT:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->OTHER:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->$VALUES:[Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->$VALUES:[Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$Operation;

    return-object v0
.end method
