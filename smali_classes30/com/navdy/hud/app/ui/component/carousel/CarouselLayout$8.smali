.class Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "CarouselLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->buildListener(Landroid/animation/Animator$AnimatorListener;ZII)Landroid/animation/Animator$AnimatorListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

.field final synthetic val$listener:Landroid/animation/Animator$AnimatorListener;

.field final synthetic val$newPos:I

.field final synthetic val$next:Z

.field final synthetic val$oldPos:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Landroid/animation/Animator$AnimatorListener;IIZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$listener:Landroid/animation/Animator$AnimatorListener;

    iput p3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$oldPos:I

    iput p4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$newPos:I

    iput-boolean p5, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$next:Z

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 732
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$newPos:I

    iput v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    .line 734
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$listener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$listener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 739
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$next:Z

    const/4 v2, 0x1

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->changeSelectedItem(ZZ)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$800(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;ZZ)V

    .line 741
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$next:Z

    if-eqz v0, :cond_7

    .line 743
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 744
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$900(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    .line 745
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 747
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 748
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    .line 749
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newRightView:Landroid/view/View;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .line 761
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 762
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->MIDDLE_RIGHT:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$900(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    .line 763
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleRightView:Landroid/view/View;

    .line 764
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    .line 767
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleLeftView:Landroid/view/View;

    .line 768
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newLeftView:Landroid/view/View;

    .line 769
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newRightView:Landroid/view/View;

    .line 771
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    if-nez v0, :cond_3

    .line 772
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$newPos:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v3, v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    invoke-virtual {v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addSideView(IIZ)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    .line 774
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    if-nez v0, :cond_4

    .line 775
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$newPos:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v3, v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->viewPadding:I

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->sideImageSize:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v4, v4, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightImageStart:I

    add-int/2addr v3, v4

    invoke-virtual {v1, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->addSideView(IIZ)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .line 778
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    if-eqz v0, :cond_5

    .line 779
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->carouselIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 781
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v0, :cond_6

    .line 782
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v2, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget v3, v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->currentItem:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    invoke-interface {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onCurrentItemChanged(II)V

    .line 784
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->runQueuedOperation()V

    .line 785
    return-void

    .line 752
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 753
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    sget-object v1, Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;->SIDE:Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    # invokes: Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->recycleView(Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->access$900(Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;Lcom/navdy/hud/app/ui/component/carousel/Carousel$ViewType;Landroid/view/View;)V

    .line 754
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .line 756
    :cond_8
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->rightView:Landroid/view/View;

    .line 757
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleLeftView:Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleLeftView:Landroid/view/View;

    :goto_1
    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->middleLeftView:Landroid/view/View;

    .line 758
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newLeftView:Landroid/view/View;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    goto/16 :goto_0

    .line 757
    :cond_9
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->leftView:Landroid/view/View;

    goto :goto_1
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 717
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$listener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$listener:Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v0, p1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 721
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->newMiddleRightView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 723
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    if-eqz v0, :cond_2

    .line 724
    iget v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$oldPos:I

    iget v1, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$newPos:I

    if-eq v0, v1, :cond_2

    .line 725
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->itemChangeListener:Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$oldPos:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$newPos:I

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->this$0:Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout;->model:Ljava/util/List;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/carousel/CarouselLayout$8;->val$newPos:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Model;->id:I

    invoke-interface {v1, v2, v3, v0}, Lcom/navdy/hud/app/ui/component/carousel/Carousel$Listener;->onCurrentItemChanging(III)V

    .line 728
    :cond_2
    return-void
.end method
