.class public final Lcom/navdy/hud/app/ui/component/SwitchHaloView;
.super Lcom/navdy/hud/app/ui/component/HaloView;
.source "SwitchHaloView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00152\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\tH\u0016R\u001a\u0010\u000b\u001a\u00020\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u000e\"\u0004\u0008\u0013\u0010\u0010\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/SwitchHaloView;",
        "Lcom/navdy/hud/app/ui/component/HaloView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "maxScalingFactorX",
        "",
        "getMaxScalingFactorX",
        "()F",
        "setMaxScalingFactorX",
        "(F)V",
        "maxScalingFactorY",
        "getMaxScalingFactorY",
        "setMaxScalingFactorY",
        "onAnimationUpdateInternal",
        "",
        "animation",
        "Landroid/animation/ValueAnimator;",
        "onDrawInternal",
        "canvas",
        "Landroid/graphics/Canvas;",
        "setStrokeColor",
        "color",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private maxScalingFactorX:F

.field private maxScalingFactorY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/HaloView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/HaloView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "defStyleAttr"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/HaloView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final getMaxScalingFactorX()F
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorX:F

    return v0
.end method

.method public final getMaxScalingFactorY()F
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorY:F

    return v0
.end method

.method public onAnimationUpdateInternal(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/ValueAnimator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const-string v2, "animation"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/HaloView;->onAnimationUpdateInternal(Landroid/animation/ValueAnimator;)V

    .line 32
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Lkotlin/TypeCastException;

    const-string v3, "null cannot be cast to non-null type kotlin.Float"

    invoke-direct {v2, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 33
    .local v1, "value":F
    iget v2, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->startRadius:F

    sub-float v2, v1, v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->endRadius:F

    iget v4, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->startRadius:F

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    const/4 v3, 0x2

    int-to-float v3, v3

    mul-float v0, v2, v3

    .line 34
    .local v0, "scaleFactor":F
    iget v2, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->endRadius:F

    iget v3, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->startRadius:F

    sub-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorX:F

    .line 35
    iget v2, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->endRadius:F

    iget v3, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->startRadius:F

    sub-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorY:F

    .line 36
    int-to-float v2, v5

    iget v3, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorX:F

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setScaleX(F)V

    .line 37
    int-to-float v2, v5

    iget v3, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorY:F

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setScaleY(F)V

    .line 38
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setPivotX(F)V

    .line 39
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setPivotY(F)V

    .line 40
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->invalidate()V

    .line 41
    return-void
.end method

.method public onDrawInternal(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    return-void
.end method

.method public final setMaxScalingFactorX(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorX:F

    return-void
.end method

.method public final setMaxScalingFactorY(F)V
    .locals 0
    .param p1, "<set-?>"    # F

    .prologue
    .line 16
    iput p1, p0, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->maxScalingFactorY:F

    return-void
.end method

.method public setStrokeColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/HaloView;->setStrokeColor(I)V

    .line 49
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.graphics.drawable.GradientDrawable"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 50
    .local v0, "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 51
    return-void
.end method
