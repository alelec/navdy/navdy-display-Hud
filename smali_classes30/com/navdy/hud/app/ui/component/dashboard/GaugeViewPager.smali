.class public Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
.super Landroid/widget/FrameLayout;
.source "GaugeViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;,
        Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;,
        Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    }
.end annotation


# static fields
.field public static final FAST_SCROLL_ANIMATION_DURATION:I = 0x64

.field public static final REGULAR_SCROLL_ANIMATION_DURATION:I = 0xc8

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private currentOperation:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

.field private volatile isAnimationRunning:Z

.field private mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

.field private mChangeListener:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;

.field private mCurrentPosition:I

.field private mNextPosition:I

.field private mPreviousPosition:I

.field private mSlider:Landroid/widget/LinearLayout;

.field private operationQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;",
            ">;"
        }
    .end annotation
.end field

.field final positionAnimator:Landroid/animation/ValueAnimator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v0, -0x1

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    .line 29
    iput v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mNextPosition:I

    .line 30
    iput v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mPreviousPosition:I

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->isAnimationRunning:Z

    .line 36
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$1;-><init>(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;-><init>(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    const/4 v0, -0x1

    .line 98
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 28
    iput v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    .line 29
    iput v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mNextPosition:I

    .line 30
    iput v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mPreviousPosition:I

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->isAnimationRunning:Z

    .line 36
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->isAnimationRunning:Z

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->currentOperation:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .prologue
    .line 23
    iget v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mNextPosition:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .prologue
    .line 23
    iget v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mPreviousPosition:I

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    .param p2, "x2"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->performOperation(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;Z)V

    return-void
.end method

.method private getMaxTopMarginValue()I
    .locals 3

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0091

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private performOperation(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;Z)V
    .locals 5
    .param p1, "operation"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    .param p2, "fastScroll"    # Z

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 204
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->currentOperation:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    .line 205
    sget-object v0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->NEXT:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    if-ne p1, v0, :cond_1

    .line 206
    iget v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mNextPosition:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->updateWidgetView(I)V

    .line 207
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    new-array v1, v1, [I

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getMaxTopMarginValue()I

    move-result v2

    neg-int v2, v2

    aput v2, v1, v3

    aput v3, v1, v4

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 212
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    if-eqz p2, :cond_2

    const-wide/16 v0, 0x64

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 213
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 214
    return-void

    .line 208
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->PREVIOUS:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    if-ne p1, v0, :cond_0

    .line 209
    iget v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mPreviousPosition:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->updateWidgetView(I)V

    .line 210
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->positionAnimator:Landroid/animation/ValueAnimator;

    new-array v1, v1, [I

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getMaxTopMarginValue()I

    move-result v2

    neg-int v2, v2

    aput v2, v1, v3

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getMaxTopMarginValue()I

    move-result v2

    neg-int v2, v2

    mul-int/lit8 v2, v2, 0x2

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    goto :goto_0

    .line 212
    :cond_2
    const-wide/16 v0, 0xc8

    goto :goto_1
.end method

.method private populateView(II)V
    .locals 6
    .param p1, "localPosition"    # I
    .param p2, "adapterPosition"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 163
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    invoke-virtual {v5, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 164
    .local v1, "localView":Landroid/view/ViewGroup;
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 165
    .local v0, "child":Landroid/view/View;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    if-ne p1, v3, :cond_1

    :goto_0
    invoke-interface {v5, p2, v0, v1, v3}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 166
    .local v2, "newView":Landroid/view/View;
    if-eq v2, v0, :cond_0

    .line 167
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 168
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 170
    :cond_0
    return-void

    .end local v2    # "newView":Landroid/view/View;
    :cond_1
    move v3, v4

    .line 165
    goto :goto_0
.end method

.method private updateWidgetView(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 221
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v1, p1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v0

    .line 222
    .local v0, "presenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    if-eqz v0, :cond_0

    .line 223
    sget-object v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "widget:: update before animation"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 224
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setWidgetVisibleToUser(Z)V

    .line 226
    :cond_0
    return-void
.end method


# virtual methods
.method public getSelectedPosition()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    return v0
.end method

.method public moveNext()V
    .locals 2

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->isAnimationRunning:Z

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->NEXT:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    if-eq v0, v1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    sget-object v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->NEXT:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 190
    :goto_0
    return-void

    .line 188
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->NEXT:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->performOperation(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;Z)V

    goto :goto_0
.end method

.method public movePrevious()V
    .locals 2

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->isAnimationRunning:Z

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->PREVIOUS:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    if-eq v0, v1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;

    sget-object v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->PREVIOUS:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->PREVIOUS:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->performOperation(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;Z)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 11

    .prologue
    const v10, 0x7f0b008d

    const v9, 0x7f0b008a

    const/4 v8, 0x1

    .line 103
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 104
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->removeAllViews()V

    .line 106
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 108
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->addView(Landroid/view/View;)V

    .line 110
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03001e

    invoke-virtual {v4, v5, p0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 111
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x3

    if-ge v1, v4, :cond_2

    .line 112
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 113
    .local v0, "frameLayout":Landroid/widget/FrameLayout;
    if-eqz v1, :cond_0

    if-ne v1, v8, :cond_1

    .line 114
    :cond_0
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0091

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v2, v4, v5}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 115
    .local v2, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    :goto_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    .end local v2    # "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v2, v4, v5}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 118
    .restart local v2    # "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 122
    .end local v0    # "frameLayout":Landroid/widget/FrameLayout;
    .end local v2    # "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 123
    .local v3, "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getMaxTopMarginValue()I

    move-result v4

    neg-int v4, v4

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 124
    return-void
.end method

.method public setAdapter(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    .line 178
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setSelectedPosition(I)V

    .line 179
    return-void
.end method

.method public setChangeListener(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;)V
    .locals 0
    .param p1, "changeListener"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mChangeListener:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;

    .line 218
    return-void
.end method

.method public setSelectedPosition(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 131
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    if-nez v4, :cond_0

    .line 132
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Set the Adapter before setting the position"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 134
    :cond_0
    iget v2, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    .line 135
    .local v2, "oldPosition":I
    if-ltz p1, :cond_3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getCount()I

    move-result v4

    if-ge p1, v4, :cond_3

    .line 136
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getCount()I

    move-result v4

    add-int/2addr v4, p1

    add-int/lit8 v4, v4, -0x1

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getCount()I

    move-result v5

    rem-int v1, v4, v5

    .line 137
    .local v1, "nextPosition":I
    add-int/lit8 v4, p1, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getCount()I

    move-result v5

    rem-int v3, v4, v5

    .line 138
    .local v3, "previousPosition":I
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getExcludedPosition()I

    move-result v4

    if-ne v1, v4, :cond_1

    .line 139
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getCount()I

    move-result v4

    add-int/2addr v4, v1

    add-int/lit8 v4, v4, -0x1

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getCount()I

    move-result v5

    rem-int v1, v4, v5

    .line 141
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v4}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getExcludedPosition()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 142
    add-int/lit8 v4, v3, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mAdapter:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;

    invoke-interface {v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;->getCount()I

    move-result v5

    rem-int v3, v4, v5

    .line 144
    :cond_2
    iput v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mNextPosition:I

    .line 145
    iput v3, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mPreviousPosition:I

    .line 146
    iput p1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    .line 147
    const/4 v4, 0x0

    iget v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mNextPosition:I

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->populateView(II)V

    .line 148
    const/4 v4, 0x1

    iget v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->populateView(II)V

    .line 149
    const/4 v4, 0x2

    iget v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mPreviousPosition:I

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->populateView(II)V

    .line 150
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 151
    .local v0, "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getMaxTopMarginValue()I

    move-result v4

    neg-int v4, v4

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 152
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mChangeListener:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    if-eq v4, v2, :cond_3

    .line 153
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mChangeListener:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;

    iget v5, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mCurrentPosition:I

    invoke-interface {v4, v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;->onGaugeChanged(I)V

    .line 156
    .end local v0    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "nextPosition":I
    .end local v3    # "previousPosition":I
    :cond_3
    return-void
.end method

.method public updateState()V
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getSelectedPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setSelectedPosition(I)V

    .line 160
    return-void
.end method
