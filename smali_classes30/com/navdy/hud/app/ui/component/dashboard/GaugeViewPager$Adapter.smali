.class public interface abstract Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;
.super Ljava/lang/Object;
.source "GaugeViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Adapter"
.end annotation


# virtual methods
.method public abstract getCount()I
.end method

.method public abstract getExcludedPosition()I
.end method

.method public abstract getPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.end method

.method public abstract getView(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
.end method
