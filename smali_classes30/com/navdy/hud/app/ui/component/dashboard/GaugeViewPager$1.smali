.class Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$1;
.super Ljava/lang/Object;
.source "GaugeViewPager.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$1;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "valueAnimator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 57
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 58
    .local v0, "margin":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$1;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$000(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 59
    .local v1, "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 60
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$1;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mSlider:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$000(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    return-void
.end method
