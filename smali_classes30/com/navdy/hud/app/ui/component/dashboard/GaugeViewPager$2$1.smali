.class Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;
.super Ljava/lang/Object;
.source "GaugeViewPager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 74
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v3, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->currentOperation:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$200(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    move-result-object v1

    sget-object v4, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;->NEXT:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mNextPosition:I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$300(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)I

    move-result v1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setSelectedPosition(I)V

    .line 75
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$500(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$500(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;

    move-object v0, v1

    .line 76
    .local v0, "nextOperationToBePerformed":Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    :goto_1
    if-eqz v0, :cond_3

    .line 77
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v3, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->operationQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$500(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    # invokes: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->performOperation(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;Z)V
    invoke-static {v3, v0, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$600(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;Z)V

    .line 81
    :goto_3
    return-void

    .line 74
    .end local v0    # "nextOperationToBePerformed":Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # getter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->mPreviousPosition:I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$400(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;)I

    move-result v1

    goto :goto_0

    .line 75
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .restart local v0    # "nextOperationToBePerformed":Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Operation;
    :cond_2
    move v1, v2

    .line 77
    goto :goto_2

    .line 79
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2$1;->this$1:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$2;->this$0:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    # setter for: Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->isAnimationRunning:Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->access$102(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;Z)Z

    goto :goto_3
.end method
