.class public interface abstract Lcom/navdy/hud/app/ui/component/dashboard/IDashboardOptionsAdapter;
.super Ljava/lang/Object;
.source "IDashboardOptionsAdapter.java"


# static fields
.field public static final LEFT:I = 0x0

.field public static final PREFERENCE_LEFT_GAUGE_ID:Ljava/lang/String; = "PREFERENCE_LEFT_GAUGE"

.field public static final PREFERENCE_MIDDLE_GAUGE:Ljava/lang/String; = "PREFERENCE_MIDDLE_GAUGE"

.field public static final PREFERENCE_RIGHT_GAUGE_ID:Ljava/lang/String; = "PREFERENCE_RIGHT_GAUGE"

.field public static final PREFERENCE_SCROLLABLE_SIDE:Ljava/lang/String; = "PREFERENCE_SCROLLABLE_SIDE"

.field public static final RIGHT:I = 0x1

.field public static final SPEEDOMETER_GAUGE:I = 0x1

.field public static final TACHOMETER_GAUGE:I


# virtual methods
.method public abstract getCurrentScrollableSideOption()I
.end method

.method public abstract getMiddleGaugeOption()I
.end method

.method public abstract onScrollableSideOptionSelected()V
.end method

.method public abstract onSpeedoMeterSelected()V
.end method

.method public abstract onTachoMeterSelected()V
.end method
