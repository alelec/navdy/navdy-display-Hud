.class public Lcom/navdy/hud/app/ui/component/HaloView;
.super Landroid/view/View;
.source "HaloView.java"


# instance fields
.field private animationDelay:I

.field private animationDuration:I

.field private animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private animatorSet:Landroid/animation/AnimatorSet;

.field private currentAnimatorSet:Landroid/animation/AnimatorSet;

.field protected currentStrokeWidth:F

.field protected endRadius:F

.field protected endStrokeWidth:F

.field private firstAnimatorSet:Landroid/animation/AnimatorSet;

.field private firstIterationDone:Z

.field private firstRadiusAnimator:Landroid/animation/ValueAnimator;

.field private handler:Landroid/os/Handler;

.field private interpolator:Landroid/view/animation/Interpolator;

.field protected middleRadius:F

.field protected middleStrokeWidth:F

.field private paint:Landroid/graphics/Paint;

.field private radiusAnimator:Landroid/animation/ValueAnimator;

.field private reverseAnimatorSet:Landroid/animation/AnimatorSet;

.field private reverseRadiusAnimator:Landroid/animation/ValueAnimator;

.field protected startRadius:F

.field private startRunnable:Ljava/lang/Runnable;

.field private started:Z

.field private strokeColor:I

.field private updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/HaloView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/HaloView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 97
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->interpolator:Landroid/view/animation/Interpolator;

    .line 58
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->handler:Landroid/os/Handler;

    .line 62
    new-instance v1, Lcom/navdy/hud/app/ui/component/HaloView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/HaloView$1;-><init>(Lcom/navdy/hud/app/ui/component/HaloView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 73
    new-instance v1, Lcom/navdy/hud/app/ui/component/HaloView$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/HaloView$2;-><init>(Lcom/navdy/hud/app/ui/component/HaloView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRunnable:Ljava/lang/Runnable;

    .line 81
    new-instance v1, Lcom/navdy/hud/app/ui/component/HaloView$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/HaloView$3;-><init>(Lcom/navdy/hud/app/ui/component/HaloView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 98
    sget-object v1, Lcom/navdy/hud/app/R$styleable;->HaloView:[I

    invoke-virtual {p1, p2, v1, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 99
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 100
    const/4 v1, -0x1

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->strokeColor:I

    .line 101
    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRadius:F

    .line 102
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endRadius:F

    .line 103
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleRadius:F

    .line 104
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDuration:I

    .line 105
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDelay:I

    .line 106
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 109
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endRadius:F

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRadius:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endStrokeWidth:F

    .line 110
    iget v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endStrokeWidth:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_1

    .line 111
    iput v3, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endStrokeWidth:F

    .line 114
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endRadius:F

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleRadius:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleStrokeWidth:F

    .line 115
    iget v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleStrokeWidth:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_2

    .line 116
    iput v3, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleStrokeWidth:F

    .line 119
    :cond_2
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->paint:Landroid/graphics/Paint;

    .line 120
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 121
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 123
    new-array v1, v6, [F

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRadius:F

    aput v2, v1, v4

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endRadius:F

    aput v2, v1, v5

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstRadiusAnimator:Landroid/animation/ValueAnimator;

    .line 124
    new-array v1, v6, [F

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleRadius:F

    aput v2, v1, v4

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endRadius:F

    aput v2, v1, v5

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->radiusAnimator:Landroid/animation/ValueAnimator;

    .line 125
    new-array v1, v6, [F

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endRadius:F

    aput v2, v1, v4

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleRadius:F

    aput v2, v1, v5

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseRadiusAnimator:Landroid/animation/ValueAnimator;

    .line 126
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstRadiusAnimator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 127
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->radiusAnimator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 128
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseRadiusAnimator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->updateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 130
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    .line 131
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 132
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 134
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    .line 135
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 136
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 138
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    .line 139
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 140
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 142
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->radiusAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 143
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseRadiusAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 145
    iget v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endStrokeWidth:F

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentStrokeWidth:F

    .line 146
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->invalidate()V

    .line 147
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/HaloView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/HaloView;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->started:Z

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/HaloView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/HaloView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/HaloView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/HaloView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/HaloView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/HaloView;

    .prologue
    .line 32
    iget v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDelay:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/HaloView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/HaloView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->toggleAnimator()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/HaloView;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/HaloView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentAnimatorSet:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method private toggleAnimator()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    if-ne v0, v1, :cond_0

    .line 206
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentAnimatorSet:Landroid/animation/AnimatorSet;

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentAnimatorSet:Landroid/animation/AnimatorSet;

    goto :goto_0
.end method


# virtual methods
.method public onAnimationUpdateInternal(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 151
    .local v0, "val":F
    iget v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRadius:F

    sub-float v1, v0, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentStrokeWidth:F

    .line 152
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->invalidate()V

    .line 153
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 158
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/HaloView;->onDrawInternal(Landroid/graphics/Canvas;)V

    .line 159
    return-void
.end method

.method public onDrawInternal(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->strokeColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 165
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRadius:F

    iget v3, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentStrokeWidth:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/HaloView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 166
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 238
    iput p1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDuration:I

    .line 239
    return-void
.end method

.method public setEndRadius(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 230
    int-to-float v0, p1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endRadius:F

    .line 231
    return-void
.end method

.method public setMiddleRadius(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 226
    int-to-float v0, p1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->middleRadius:F

    .line 227
    return-void
.end method

.method public setStartDelay(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 234
    iput p1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationDelay:I

    .line 235
    return-void
.end method

.method public setStartRadius(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 222
    int-to-float v0, p1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRadius:F

    .line 223
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 213
    iput p1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->strokeColor:I

    .line 214
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->invalidate()V

    .line 215
    return-void
.end method

.method public setStrokeWidth(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 218
    int-to-float v0, p1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentStrokeWidth:F

    .line 219
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 169
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->stop()V

    .line 170
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->started:Z

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 174
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstIterationDone:Z

    if-nez v0, :cond_0

    .line 175
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstIterationDone:Z

    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentAnimatorSet:Landroid/animation/AnimatorSet;

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 182
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentAnimatorSet:Landroid/animation/AnimatorSet;

    .line 180
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->started:Z

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/HaloView;->startRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->firstAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 193
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->reverseAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 200
    :cond_2
    iget v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->endStrokeWidth:F

    iput v0, p0, Lcom/navdy/hud/app/ui/component/HaloView;->currentStrokeWidth:F

    .line 201
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/HaloView;->requestLayout()V

    .line 202
    return-void
.end method
