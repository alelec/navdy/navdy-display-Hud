.class public Lcom/navdy/hud/app/ui/component/UISettings;
.super Ljava/lang/Object;
.source "UISettings.java"


# static fields
.field private static final ADVANCED_GPS_STATS:Ljava/lang/String; = "persist.sys.gps.stats"

.field private static final DIAL_LONG_PRESS_ACTION_PLACE_SEARCH:Ljava/lang/String; = "persist.sys.dlpress_search"

.field private static final dialLongPressPlaceSearchAction:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-string v0, "persist.sys.dlpress_search"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/ui/component/UISettings;->dialLongPressPlaceSearchAction:Z

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static advancedGpsStatsEnabled()Z
    .locals 2

    .prologue
    .line 37
    const-string v0, "persist.sys.gps.stats"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isLongPressActionPlaceSearch()Z
    .locals 1

    .prologue
    .line 32
    sget-boolean v0, Lcom/navdy/hud/app/ui/component/UISettings;->dialLongPressPlaceSearchAction:Z

    return v0
.end method

.method public static isMusicBrowsingEnabled()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    return v0
.end method

.method public static isVerticalListNoCloseTimeout()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method

.method public static supportsIosSms()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method
