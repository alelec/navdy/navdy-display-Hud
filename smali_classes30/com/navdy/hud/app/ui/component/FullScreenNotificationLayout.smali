.class public Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;
.super Landroid/widget/RelativeLayout;
.source "FullScreenNotificationLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;
    }
.end annotation


# static fields
.field private static final ANIM_DURATION:J = 0x320L


# instance fields
.field private autoPopup:Z

.field private bodyViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private directionalTranslation:F

.field private footer:Landroid/view/View;

.field private footerId:I

.field private title:Landroid/view/View;

.field private titleId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->bodyViews:Ljava/util/List;

    .line 40
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0187

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 41
    .local v2, "translationY":F
    sget-object v3, Lcom/navdy/hud/app/R$styleable;->FullScreenNotificationLayout:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 43
    .local v1, "ta":Landroid/content/res/TypedArray;
    if-eqz v1, :cond_0

    .line 45
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->titleId:I

    .line 46
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footerId:I

    .line 49
    invoke-static {}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;->values()[Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    aget-object v0, v3, v4

    .line 50
    .local v0, "animDirection":Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;
    sget-object v3, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;->UP:Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;

    if-ne v0, v3, :cond_1

    .end local v2    # "translationY":F
    :goto_0
    iput v2, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->directionalTranslation:F

    .line 52
    const/4 v3, 0x3

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->autoPopup:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    .end local v0    # "animDirection":Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;
    :cond_0
    return-void

    .line 50
    .restart local v0    # "animDirection":Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;
    .restart local v2    # "translationY":F
    :cond_1
    neg-float v2, v2

    goto :goto_0

    .line 54
    .end local v0    # "animDirection":Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$AnimationDirection;
    .end local v2    # "translationY":F
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v3
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->animateInSecondStage(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->animateOutSecondStage(Ljava/lang/Runnable;)V

    return-void
.end method

.method private animateInSecondStage(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 132
    if-eqz p1, :cond_2

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 139
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    if-nez v0, :cond_4

    .line 141
    if-eqz p1, :cond_3

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 150
    :cond_1
    :goto_1
    return-void

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 144
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 147
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method

.method private animateOutSecondStage(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 181
    const/4 v1, 0x0

    .line 183
    .local v1, "secondStageTriggered":Z
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->bodyViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 184
    .local v0, "bodyView":Landroid/view/View;
    if-nez v1, :cond_1

    .line 185
    const/4 v1, 0x1

    .line 187
    if-eqz p1, :cond_0

    .line 188
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getOutBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 190
    :cond_0
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getOutBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 193
    :cond_1
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getOutBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 196
    .end local v0    # "bodyView":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private getInBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 199
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method private getInTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 207
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method private getOutBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->directionalTranslation:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method private getOutTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 211
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method private initialize()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 75
    iget v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->titleId:I

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    .line 76
    iget v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footerId:I

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    .line 78
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setAlpha(F)V

    .line 79
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setAlpha(F)V

    .line 81
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getChildCount()I

    move-result v0

    .line 83
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 84
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 86
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->titleId:I

    if-eq v3, v4, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footerId:I

    if-eq v3, v4, :cond_0

    .line 87
    invoke-virtual {v2, v5}, Landroid/view/View;->setAlpha(F)V

    .line 88
    iget v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->directionalTranslation:F

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 89
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->bodyViews:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    .end local v2    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method


# virtual methods
.method public animateIn()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->animateIn(Ljava/lang/Runnable;)V

    .line 96
    return-void
.end method

.method public animateIn(Ljava/lang/Runnable;)V
    .locals 5
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 101
    .local v0, "secondStageTriggered":Z
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->bodyViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 102
    .local v1, "v":Landroid/view/View;
    if-nez v0, :cond_2

    .line 104
    const/4 v0, 0x1

    .line 106
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    if-nez v3, :cond_1

    .line 107
    if-eqz p1, :cond_0

    .line 108
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 110
    :cond_0
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 113
    :cond_1
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$1;

    invoke-direct {v4, p0, p1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$1;-><init>(Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;Ljava/lang/Runnable;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 121
    :cond_2
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getInBodyAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 125
    .end local v1    # "v":Landroid/view/View;
    :cond_3
    if-nez v0, :cond_4

    .line 126
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->animateInSecondStage(Ljava/lang/Runnable;)V

    .line 128
    :cond_4
    return-void
.end method

.method public animateOut(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    if-nez v0, :cond_1

    .line 154
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->animateOutSecondStage(Ljava/lang/Runnable;)V

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getOutTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$2;-><init>(Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 165
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->title:Landroid/view/View;

    if-nez v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getOutTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout$3;-><init>(Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 174
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->footer:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->getOutTitleFooterAnimation(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 69
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->autoPopup:Z

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->animateIn()V

    .line 72
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 62
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/FullScreenNotificationLayout;->initialize()V

    .line 63
    return-void
.end method
