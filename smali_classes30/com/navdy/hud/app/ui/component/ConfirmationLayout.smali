.class public Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
.super Landroid/widget/RelativeLayout;
.source "ConfirmationLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/ConfirmationLayout$Listener;
    }
.end annotation


# instance fields
.field public choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

.field private choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field private defaultChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

.field private finished:Z

.field public fluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

.field private handler:Landroid/os/Handler;

.field public leftSwipe:Landroid/widget/ImageView;

.field private listener:Lcom/navdy/hud/app/ui/component/ConfirmationLayout$Listener;

.field public mainSection:Landroid/view/ViewGroup;

.field public rightSwipe:Landroid/widget/ImageView;

.field public screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field public screenImageIconBkColor:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

.field public screenTitle:Landroid/widget/TextView;

.field public sideImage:Landroid/widget/ImageView;

.field private timeout:I

.field private timeoutRunnable:Ljava/lang/Runnable;

.field public title1:Landroid/widget/TextView;

.field public title2:Landroid/widget/TextView;

.field public title3:Landroid/widget/TextView;

.field public title4:Landroid/widget/TextView;

.field public titleContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->handler:Landroid/os/Handler;

    .line 58
    new-instance v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout$1;-><init>(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->defaultChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 71
    new-instance v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout$2;-><init>(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->timeoutRunnable:Ljava/lang/Runnable;

    .line 89
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->init()V

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->clearTimeout()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;)Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->finished:Z

    return p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;)Lcom/navdy/hud/app/ui/component/ConfirmationLayout$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->listener:Lcom/navdy/hud/app/ui/component/ConfirmationLayout$Listener;

    return-object v0
.end method

.method private clearTimeout()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 137
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 94
    const v0, 0x7f0e00cc

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenTitle:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e00cf

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 96
    const v0, 0x7f0e00d0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImageIconBkColor:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 97
    const v0, 0x7f0e00d1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    .line 98
    const v0, 0x7f0e00bf

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->titleContainer:Landroid/view/ViewGroup;

    .line 99
    const v0, 0x7f0e0088

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0089

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e008a

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e008b

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title4:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e00d2

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .line 104
    const v0, 0x7f0e00ce

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->fluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .line 105
    const v0, 0x7f0e00d3

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->leftSwipe:Landroid/widget/ImageView;

    .line 106
    const v0, 0x7f0e00d4

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->rightSwipe:Landroid/widget/ImageView;

    .line 107
    const v0, 0x7f0e00cd

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->mainSection:Landroid/view/ViewGroup;

    .line 108
    return-void
.end method

.method private resetTimeout()V
    .locals 4

    .prologue
    .line 129
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->timeout:I

    if-lez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->timeoutRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->timeout:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 133
    :cond_0
    return-void
.end method


# virtual methods
.method public executeSelectedItem(Z)V
    .locals 1
    .param p1, "animated"    # Z

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->finished:Z

    if-eqz v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->resetTimeout()V

    .line 184
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeSelectedItem(Z)V

    goto :goto_0
.end method

.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 188
    const/4 v0, 0x1

    .line 190
    .local v0, "handled":Z
    sget-object v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout$3;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 201
    const/4 v0, 0x0

    .line 204
    :goto_0
    return v0

    .line 192
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 195
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->moveSelectionRight()V

    goto :goto_0

    .line 198
    :pswitch_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public moveSelectionLeft()V
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->finished:Z

    if-eqz v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->resetTimeout()V

    .line 168
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionLeft()V

    goto :goto_0
.end method

.method public moveSelectionRight()V
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->finished:Z

    if-eqz v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->resetTimeout()V

    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->moveSelectionRight()V

    goto :goto_0
.end method

.method public setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V
    .locals 5
    .param p2, "initialSelection"    # I
    .param p3, "listener"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "choiceLabels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 140
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->finished:Z

    .line 141
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->clearTimeout()V

    .line 142
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    if-nez p3, :cond_1

    .line 143
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 145
    :cond_1
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 148
    .local v1, "str":Ljava/lang/String;
    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-direct {v3, v1, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->defaultChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    invoke-virtual {v2, v3, v0, p2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 151
    return-void
.end method

.method public setChoicesList(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V
    .locals 3
    .param p2, "initialSelection"    # I
    .param p3, "listener"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;I",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 154
    .local p1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->finished:Z

    .line 155
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->clearTimeout()V

    .line 156
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 159
    :cond_1
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->LABEL:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->defaultChoiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 161
    return-void
.end method

.method public setTimeout(ILcom/navdy/hud/app/ui/component/ConfirmationLayout$Listener;)V
    .locals 1
    .param p1, "millis"    # I
    .param p2, "listener"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout$Listener;

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->finished:Z

    if-eqz v0, :cond_0

    .line 126
    :goto_0
    return-void

    .line 119
    :cond_0
    if-eqz p2, :cond_1

    if-gtz p1, :cond_2

    .line 121
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 123
    :cond_2
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->timeout:I

    .line 124
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->listener:Lcom/navdy/hud/app/ui/component/ConfirmationLayout$Listener;

    .line 125
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->resetTimeout()V

    goto :goto_0
.end method
