.class Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "ContentLoadingViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->access$100(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 74
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 66
    return-void
.end method
