.class public final Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.source "SwitchViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u00a1\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006*\u0001\u0019\u0018\u0000 u2\u00020\u0001:\u0001uB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010P\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J\u0008\u0010V\u001a\u00020QH\u0016J0\u0010W\u001a\u00020Q2\u0006\u0010X\u001a\u00020Y2\u0006\u0010Z\u001a\u00020-2\u0006\u0010[\u001a\u00020-2\u0006\u0010\\\u001a\u00020-2\u0006\u0010]\u001a\u00020\"H\u0016J\u0016\u0010^\u001a\u00020Q2\u0006\u0010+\u001a\u00020\"2\u0006\u0010*\u001a\u00020\"J\u0008\u0010_\u001a\u00020`H\u0016J\u0008\u0010a\u001a\u00020\nH\u0002J\u0008\u0010b\u001a\u00020\nH\u0002J\u0010\u0010c\u001a\u00020\n2\u0006\u0010d\u001a\u00020eH\u0002J\u0018\u0010f\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020UH\u0016J \u0010g\u001a\u00020Q2\u0006\u0010R\u001a\u00020S2\u0006\u0010h\u001a\u00020\n2\u0006\u0010i\u001a\u00020\nH\u0016J\u0010\u0010j\u001a\u00020Q2\u0006\u0010k\u001a\u00020\nH\u0002J(\u0010l\u001a\u00020Q2\u0006\u0010m\u001a\u00020n2\u0006\u0010o\u001a\u00020p2\u0006\u0010i\u001a\u00020\n2\u0006\u0010q\u001a\u00020\"H\u0016J\u001a\u00100\u001a\u00020Q2\u0008\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u001a\u00104\u001a\u00020Q2\u0008\u0010r\u001a\u0004\u0018\u00010e2\u0006\u0010s\u001a\u00020\"H\u0002J\u0010\u0010L\u001a\u00020Q2\u0006\u0010r\u001a\u00020eH\u0002J\u0008\u0010q\u001a\u00020QH\u0016J\u0008\u0010t\u001a\u00020QH\u0002R\u0014\u0010\t\u001a\u00020\nX\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u00020\nX\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR \u0010\u000f\u001a\u0008\u0018\u00010\u0010R\u00020\u0011X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u000e\u0010!\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010%\u001a\u00020\"X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008&\u0010\'\"\u0004\u0008(\u0010)R\u000e\u0010*\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010,\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008.\u0010/\"\u0004\u00080\u00101R\u001a\u00102\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00083\u0010/\"\u0004\u00084\u00101R\u001a\u00105\u001a\u000206X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00087\u00108\"\u0004\u00089\u0010:R\u001a\u0010;\u001a\u00020\u0003X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008<\u0010=\"\u0004\u0008>\u0010?R\u001a\u0010@\u001a\u000206X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008A\u00108\"\u0004\u0008B\u0010:R\u001a\u0010C\u001a\u00020\"X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008D\u0010\'\"\u0004\u0008E\u0010)R\u001a\u0010F\u001a\u00020\u0003X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008G\u0010=\"\u0004\u0008H\u0010?R\u000e\u0010I\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010J\u001a\u00020-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008K\u0010/\"\u0004\u0008L\u00101R\u000e\u0010M\u001a\u00020NX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020NX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006v"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;",
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;",
        "layout",
        "Landroid/view/ViewGroup;",
        "vlist",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;",
        "handler",
        "Landroid/os/Handler;",
        "(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V",
        "FLUCTUATOR_OPACITY_ALPHA",
        "",
        "getFLUCTUATOR_OPACITY_ALPHA",
        "()I",
        "HALO_DELAY_START_DURATION",
        "getHALO_DELAY_START_DURATION",
        "animatorSetBuilder",
        "Landroid/animation/AnimatorSet$Builder;",
        "Landroid/animation/AnimatorSet;",
        "getAnimatorSetBuilder",
        "()Landroid/animation/AnimatorSet$Builder;",
        "setAnimatorSetBuilder",
        "(Landroid/animation/AnimatorSet$Builder;)V",
        "fluctuatorRunnable",
        "Ljava/lang/Runnable;",
        "fluctuatorStartListener",
        "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1",
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;",
        "haloView",
        "Lcom/navdy/hud/app/ui/component/SwitchHaloView;",
        "getHaloView",
        "()Lcom/navdy/hud/app/ui/component/SwitchHaloView;",
        "setHaloView",
        "(Lcom/navdy/hud/app/ui/component/SwitchHaloView;)V",
        "hasIconFluctuatorColor",
        "",
        "hasSubTitle",
        "hasSubTitle2",
        "iconScaleAnimationDisabled",
        "getIconScaleAnimationDisabled",
        "()Z",
        "setIconScaleAnimationDisabled",
        "(Z)V",
        "isEnabled",
        "isOn",
        "subTitle",
        "Landroid/widget/TextView;",
        "getSubTitle",
        "()Landroid/widget/TextView;",
        "setSubTitle",
        "(Landroid/widget/TextView;)V",
        "subTitle2",
        "getSubTitle2",
        "setSubTitle2",
        "switchBackground",
        "Landroid/view/View;",
        "getSwitchBackground",
        "()Landroid/view/View;",
        "setSwitchBackground",
        "(Landroid/view/View;)V",
        "switchContainer",
        "getSwitchContainer",
        "()Landroid/view/ViewGroup;",
        "setSwitchContainer",
        "(Landroid/view/ViewGroup;)V",
        "switchThumb",
        "getSwitchThumb",
        "setSwitchThumb",
        "textAnimationDisabled",
        "getTextAnimationDisabled",
        "setTextAnimationDisabled",
        "textContainer",
        "getTextContainer",
        "setTextContainer",
        "thumbMargin",
        "title",
        "getTitle",
        "setTitle",
        "titleSelectedTopMargin",
        "",
        "titleUnselectedScale",
        "bind",
        "",
        "model",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "modelState",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;",
        "clearAnimation",
        "copyAndPosition",
        "imageC",
        "Landroid/widget/ImageView;",
        "titleC",
        "subTitleC",
        "subTitle2C",
        "setImage",
        "drawSwitch",
        "getModelType",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;",
        "getSubTitle2Color",
        "getSubTitleColor",
        "getTextColor",
        "property",
        "",
        "preBind",
        "select",
        "pos",
        "duration",
        "setIconFluctuatorColor",
        "color",
        "setItemState",
        "state",
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;",
        "animation",
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;",
        "startFluctuator",
        "str",
        "formatted",
        "stopFluctuator",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

.field private static final logger:Lcom/navdy/service/library/log/Logger;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final FLUCTUATOR_OPACITY_ALPHA:I

.field private final HALO_DELAY_START_DURATION:I

.field private animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final fluctuatorRunnable:Ljava/lang/Runnable;

.field private final fluctuatorStartListener:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;

.field private haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private hasIconFluctuatorColor:Z

.field private hasSubTitle:Z

.field private hasSubTitle2:Z

.field private iconScaleAnimationDisabled:Z

.field private isEnabled:Z

.field private isOn:Z

.field private subTitle:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private subTitle2:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private switchBackground:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private switchContainer:Landroid/view/ViewGroup;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private switchThumb:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private textAnimationDisabled:Z

.field private textContainer:Landroid/view/ViewGroup;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private thumbMargin:I

.field private title:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private titleSelectedTopMargin:F

.field private titleUnselectedScale:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->Companion:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->Companion:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 3
    .param p1, "layout"    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "handler"    # Landroid/os/Handler;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v1, "layout"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "vlist"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "handler"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 62
    const/16 v1, 0x99

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->FLUCTUATOR_OPACITY_ALPHA:I

    .line 63
    const/16 v1, 0x64

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->HALO_DELAY_START_DURATION:I

    .line 89
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;

    invoke-direct {v1, p0, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->fluctuatorStartListener:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;

    .line 97
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorRunnable$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorRunnable$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)V

    check-cast v1, Ljava/lang/Runnable;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    .line 100
    const v1, 0x7f0e0228

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->textContainer:Landroid/view/ViewGroup;

    .line 101
    const v1, 0x7f0e00c3

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    .line 102
    const v1, 0x7f0e0111

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    .line 103
    const v1, 0x7f0e0229

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    .line 104
    const v1, 0x7f0e022c

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "layout.findViewById(R.id.toggleSwitchBackground)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    .line 105
    const v1, 0x7f0e022d

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "layout.findViewById(R.id.toggleSwitchThumb)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    .line 106
    const v1, 0x7f0e00c6

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    .line 107
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 108
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b01e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->thumbMargin:I

    .line 109
    const v1, 0x7f0e00c7

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_5

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type com.navdy.hud.app.ui.component.SwitchHaloView"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    check-cast v1, Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    .line 110
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setVisibility(I)V

    return-void
.end method

.method public static final synthetic access$getFluctuatorRunnable$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$isEnabled$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Z
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z

    return v0
.end method

.method public static final synthetic access$isOn$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Z
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z

    return v0
.end method

.method public static final synthetic access$setEnabled$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Z)V
    .locals 0
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .param p1, "<set-?>"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z

    return-void
.end method

.method public static final synthetic access$setOn$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Z)V
    .locals 0
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .param p1, "<set-?>"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z

    return-void
.end method

.method private final getSubTitle2Color()I
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->extras:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 425
    const/4 v0, 0x0

    .line 427
    :goto_0
    return v0

    :cond_0
    const-string v0, "SUBTITLE_2_COLOR"

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->getTextColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private final getSubTitleColor()I
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->extras:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 418
    const/4 v0, 0x0

    .line 420
    :goto_0
    return v0

    :cond_0
    const-string v0, "SUBTITLE_COLOR"

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->getTextColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private final getTextColor(Ljava/lang/String;)I
    .locals 4
    .param p1, "property"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 431
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->extras:Ljava/util/HashMap;

    if-nez v3, :cond_1

    .line 439
    :cond_0
    :goto_0
    return v2

    .line 434
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->extras:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 436
    .local v1, "str":Ljava/lang/String;
    nop

    .line 437
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 438
    :catch_0
    move-exception v0

    .line 439
    .local v0, "nex":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method private final setIconFluctuatorColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 314
    if-eqz p1, :cond_0

    .line 315
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->FLUCTUATOR_OPACITY_ALPHA:I

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 316
    .local v0, "fluctuatorColor":I
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasIconFluctuatorColor:Z

    .line 317
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setStrokeColor(I)V

    .line 320
    .end local v0    # "fluctuatorColor":I
    :goto_0
    return-void

    .line 319
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasIconFluctuatorColor:Z

    goto :goto_0
.end method

.method private final setSubTitle(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "formatted"    # Z

    .prologue
    .line 272
    if-nez p1, :cond_0

    .line 273
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasSubTitle:Z

    .line 288
    .end local p1    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 276
    .restart local p1    # "str":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->getSubTitleColor()I

    move-result v0

    .line 277
    .local v0, "c":I
    if-nez v0, :cond_1

    .line 278
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->subTitleColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 281
    :goto_1
    if-eqz p2, :cond_2

    .line 283
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    .end local p1    # "str":Ljava/lang/String;
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasSubTitle:Z

    goto :goto_0

    .line 280
    .restart local p1    # "str":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 285
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    .end local p1    # "str":Ljava/lang/String;
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private final setSubTitle2(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "formatted"    # Z

    .prologue
    const/4 v3, 0x0

    .line 292
    if-nez p1, :cond_0

    .line 293
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 295
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasSubTitle2:Z

    .line 310
    .end local p1    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 297
    .restart local p1    # "str":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->getSubTitle2Color()I

    move-result v0

    .line 298
    .local v0, "c":I
    if-nez v0, :cond_1

    .line 299
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->subTitle2Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 302
    :goto_1
    if-eqz p2, :cond_2

    .line 304
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    .end local p1    # "str":Ljava/lang/String;
    :goto_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasSubTitle2:Z

    goto :goto_0

    .line 301
    .restart local p1    # "str":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 306
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    .end local p1    # "str":Ljava/lang/String;
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private final setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    .end local p1    # "str":Ljava/lang/String;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    return-void
.end method

.method private final stopFluctuator()V
    .locals 2

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasIconFluctuatorColor:Z

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 333
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->stop()V

    .line 336
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 4
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modelState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 511
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z

    .line 512
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isEnabled:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z

    .line 513
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z

    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->drawSwitch(ZZ)V

    .line 515
    iget-boolean v0, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateTitle:Z

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    const-string v1, "model.title"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->setTitle(Ljava/lang/String;)V

    .line 519
    :cond_0
    iget-boolean v0, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSubTitle:Z

    if-eqz v0, :cond_1

    .line 520
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 521
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitleFormatted:Z

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->setSubTitle(Ljava/lang/String;Z)V

    .line 524
    :cond_1
    :goto_0
    iget-boolean v0, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSubTitle2:Z

    if-eqz v0, :cond_2

    .line 528
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 529
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2Formatted:Z

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->setSubTitle2(Ljava/lang/String;Z)V

    .line 532
    :cond_2
    :goto_1
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noTextAnimation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->textAnimationDisabled:Z

    .line 536
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noImageScaleAnimation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->iconScaleAnimationDisabled:Z

    .line 538
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->setIconFluctuatorColor(I)V

    .line 539
    return-void

    .line 523
    :cond_3
    invoke-direct {p0, v3, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->setSubTitle(Ljava/lang/String;Z)V

    goto :goto_0

    .line 531
    :cond_4
    invoke-direct {p0, v3, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->setSubTitle2(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public clearAnimation()V
    .locals 2

    .prologue
    .line 260
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->stopFluctuator()V

    .line 261
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->stopAnimation()V

    .line 262
    const/4 v0, 0x0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 263
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->layout:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 265
    return-void
.end method

.method public copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
    .locals 1
    .param p1, "imageC"    # Landroid/widget/ImageView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "titleC"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "subTitleC"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4, "subTitle2C"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5, "setImage"    # Z

    .prologue
    const-string v0, "imageC"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "titleC"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subTitleC"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subTitle2C"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 449
    return-void
.end method

.method public final drawSwitch(ZZ)V
    .locals 8
    .param p1, "isOn"    # Z
    .param p2, "isEnabled"    # Z

    .prologue
    const v7, 0x7f02024b

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 452
    if-eqz p2, :cond_0

    .line 453
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    const v4, 0x7f02024e

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 456
    :goto_0
    if-eqz p2, :cond_4

    if-eqz p1, :cond_4

    .line 458
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 459
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v3, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type android.graphics.drawable.LayerDrawable"

    invoke-direct {v3, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 455
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    const v4, 0x7f02024d

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 459
    :cond_1
    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 460
    .local v1, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v1, v6}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v3, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type android.graphics.drawable.ClipDrawable"

    invoke-direct {v3, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    check-cast v0, Landroid/graphics/drawable/ClipDrawable;

    .line 461
    .local v0, "backgroundClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    const/16 v3, 0x1388

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    .line 462
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v3, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v3, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 463
    .local v2, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->thumbMargin:I

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 464
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    move-object v3, v2

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 473
    :goto_1
    return-void

    .line 466
    .end local v0    # "backgroundClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    .end local v1    # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    .end local v2    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_4
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 467
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_5

    new-instance v3, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type android.graphics.drawable.LayerDrawable"

    invoke-direct {v3, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_5
    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 468
    .restart local v1    # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v1, v6}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v3, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type android.graphics.drawable.ClipDrawable"

    invoke-direct {v3, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    check-cast v0, Landroid/graphics/drawable/ClipDrawable;

    .line 469
    .restart local v0    # "backgroundClipDrawable":Landroid/graphics/drawable/ClipDrawable;
    invoke-virtual {v0, v5}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    .line 470
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-nez v2, :cond_7

    new-instance v3, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v3, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_7
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 471
    .restart local v2    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 472
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    move-object v3, v2

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method protected final getAnimatorSetBuilder()Landroid/animation/AnimatorSet$Builder;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    return-object v0
.end method

.method public final getFLUCTUATOR_OPACITY_ALPHA()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->FLUCTUATOR_OPACITY_ALPHA:I

    return v0
.end method

.method public final getHALO_DELAY_START_DURATION()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->HALO_DELAY_START_DURATION:I

    return v0
.end method

.method protected final getHaloView()Lcom/navdy/hud/app/ui/component/SwitchHaloView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    return-object v0
.end method

.method protected final getIconScaleAnimationDisabled()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->iconScaleAnimationDisabled:Z

    return v0
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 542
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SWITCH:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method protected final getSubTitle()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method protected final getSubTitle2()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    return-object v0
.end method

.method protected final getSwitchBackground()Landroid/view/View;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    return-object v0
.end method

.method protected final getSwitchContainer()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected final getSwitchThumb()Landroid/view/View;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    return-object v0
.end method

.method protected final getTextAnimationDisabled()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->textAnimationDisabled:Z

    return v0
.end method

.method protected final getTextContainer()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->textContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected final getTitle()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    return-object v0
.end method

.method public preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 4
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v1, "model"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "modelState"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 479
    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z

    .line 480
    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isEnabled:Z

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z

    .line 481
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->drawSwitch(ZZ)V

    .line 484
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 485
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 486
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 487
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->titleSelectedTopMargin:F

    .line 489
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    if-nez v0, :cond_1

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 490
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 491
    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    if-nez v1, :cond_2

    .line 492
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c0032

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 493
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 494
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    check-cast v1, Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 500
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 503
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    if-nez v0, :cond_3

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 496
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c0033

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 497
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 498
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 499
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    .line 503
    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_3
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 504
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 505
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 506
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->titleUnselectedScale:F

    .line 507
    return-void
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
    .locals 21
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "pos"    # I
    .param p3, "duration"    # I

    .prologue
    const-string v18, "model"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasIconFluctuatorColor:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setVisibility(I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->stop()V

    .line 348
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z

    move/from16 v18, v0

    if-nez v18, :cond_1

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock()V

    .line 414
    :goto_0
    return-void

    .line 353
    :cond_1
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 354
    .local v4, "animatorSet":Landroid/animation/AnimatorSet;
    sget-object v18, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v19, v0

    fill-array-data v19, :array_0

    invoke-static/range {v18 .. v19}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    .line 355
    .local v7, "clickDownXPropertyHolder":Landroid/animation/PropertyValuesHolder;
    sget-object v18, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v19, v0

    fill-array-data v19, :array_1

    invoke-static/range {v18 .. v19}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v8

    .line 356
    .local v8, "clickDownYPropertyHolder":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v7, v19, v20

    const/16 v20, 0x1

    aput-object v8, v19, v20

    invoke-static/range {v18 .. v19}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 357
    .local v6, "clickDownAnimator":Landroid/animation/ObjectAnimator;
    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v6, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 359
    sget-object v18, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v19, v0

    fill-array-data v19, :array_2

    invoke-static/range {v18 .. v19}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    .line 360
    .local v10, "clickUpXPropertyHolder":Landroid/animation/PropertyValuesHolder;
    sget-object v18, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v19, v0

    fill-array-data v19, :array_3

    invoke-static/range {v18 .. v19}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v11

    .line 361
    .local v11, "clickUpYPropertyHolder":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v10, v19, v20

    const/16 v20, 0x1

    aput-object v11, v19, v20

    invoke-static/range {v18 .. v19}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 362
    .local v9, "clickUpAnimator":Landroid/animation/ObjectAnimator;
    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v9, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 364
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 365
    .local v5, "clickAnimatorSet":Landroid/animation/AnimatorSet;
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    check-cast v6, Landroid/animation/Animator;

    .end local v6    # "clickDownAnimator":Landroid/animation/ObjectAnimator;
    aput-object v6, v18, v19

    const/16 v19, 0x1

    check-cast v9, Landroid/animation/Animator;

    .end local v9    # "clickUpAnimator":Landroid/animation/ObjectAnimator;
    aput-object v9, v18, v19

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 367
    new-instance v16, Landroid/animation/AnimatorSet;

    invoke-direct/range {v16 .. v16}, Landroid/animation/AnimatorSet;-><init>()V

    .line 368
    .local v16, "switchAnimatorSet":Landroid/animation/AnimatorSet;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    if-nez v14, :cond_2

    new-instance v18, Lkotlin/TypeCastException;

    const-string v19, "null cannot be cast to non-null type android.graphics.drawable.LayerDrawable"

    invoke-direct/range {v18 .. v19}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_2
    check-cast v14, Landroid/graphics/drawable/LayerDrawable;

    .line 369
    .local v14, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    if-nez v12, :cond_3

    new-instance v18, Lkotlin/TypeCastException;

    const-string v19, "null cannot be cast to non-null type android.graphics.drawable.ClipDrawable"

    invoke-direct/range {v18 .. v19}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_3
    check-cast v12, Landroid/graphics/drawable/ClipDrawable;

    .line 370
    .local v12, "clipDrawable":Landroid/graphics/drawable/ClipDrawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    if-nez v15, :cond_4

    new-instance v18, Lkotlin/TypeCastException;

    const-string v19, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct/range {v18 .. v19}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_4
    check-cast v15, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 371
    .local v15, "marginLayoutParamas":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v17, 0x0

    check-cast v17, Landroid/animation/ValueAnimator;

    .line 372
    .local v17, "turnOnThumbAnimator":Landroid/animation/ValueAnimator;
    const/4 v13, 0x0

    check-cast v13, Landroid/animation/ValueAnimator;

    .line 373
    .local v13, "clipLevelAnimator":Landroid/animation/ValueAnimator;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z

    move/from16 v18, v0

    if-eqz v18, :cond_7

    .line 375
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->thumbMargin:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    aput v20, v18, v19

    const/16 v19, 0x1

    const/16 v20, 0x0

    aput v20, v18, v19

    invoke-static/range {v18 .. v18}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v17

    .line 376
    const-wide/16 v18, 0x64

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 377
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    fill-array-data v18, :array_4

    invoke-static/range {v18 .. v18}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v13

    .line 378
    const-wide/16 v18, 0x64

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 385
    :goto_1
    if-eqz v13, :cond_5

    new-instance v18, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/graphics/drawable/ClipDrawable;)V

    check-cast v18, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 393
    :cond_5
    if-eqz v17, :cond_6

    new-instance v18, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v15}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/view/ViewGroup$MarginLayoutParams;)V

    check-cast v18, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual/range {v17 .. v18}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 399
    :cond_6
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    check-cast v17, Landroid/animation/Animator;

    .end local v17    # "turnOnThumbAnimator":Landroid/animation/ValueAnimator;
    aput-object v17, v18, v19

    const/16 v19, 0x1

    check-cast v13, Landroid/animation/Animator;

    .end local v13    # "clipLevelAnimator":Landroid/animation/ValueAnimator;
    aput-object v13, v18, v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 401
    check-cast v16, Landroid/animation/Animator;

    .end local v16    # "switchAnimatorSet":Landroid/animation/AnimatorSet;
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v18

    check-cast v5, Landroid/animation/Animator;

    .end local v5    # "clickAnimatorSet":Landroid/animation/AnimatorSet;
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 402
    new-instance v18, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V

    check-cast v18, Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 413
    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    .line 381
    .restart local v5    # "clickAnimatorSet":Landroid/animation/AnimatorSet;
    .restart local v13    # "clipLevelAnimator":Landroid/animation/ValueAnimator;
    .restart local v16    # "switchAnimatorSet":Landroid/animation/AnimatorSet;
    .restart local v17    # "turnOnThumbAnimator":Landroid/animation/ValueAnimator;
    :cond_7
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    aput v20, v18, v19

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->thumbMargin:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    aput v20, v18, v19

    invoke-static/range {v18 .. v18}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v17

    .line 382
    const-wide/16 v18, 0x64

    invoke-virtual/range {v17 .. v19}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 383
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    fill-array-data v18, :array_5

    invoke-static/range {v18 .. v18}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v13

    .line 384
    const-wide/16 v18, 0x64

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    goto/16 :goto_1

    .line 354
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
    .end array-data

    .line 355
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
    .end array-data

    .line 359
    :array_2
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data

    .line 360
    :array_3
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data

    .line 377
    :array_4
    .array-data 4
        0x459c4000    # 5000.0f
        0x447a0000    # 1000.0f
    .end array-data

    .line 383
    :array_5
    .array-data 4
        0x453b8000    # 3000.0f
        0x45dac000    # 7000.0f
    .end array-data
.end method

.method protected final setAnimatorSetBuilder(Landroid/animation/AnimatorSet$Builder;)V
    .locals 0
    .param p1, "<set-?>"    # Landroid/animation/AnimatorSet$Builder;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 82
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    return-void
.end method

.method protected final setHaloView(Lcom/navdy/hud/app/ui/component/SwitchHaloView;)V
    .locals 1
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/ui/component/SwitchHaloView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    return-void
.end method

.method protected final setIconScaleAnimationDisabled(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->iconScaleAnimationDisabled:Z

    return-void
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 21
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    const-string v17, "state"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v17, "animation"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    const/4 v14, 0x0

    .line 116
    .local v14, "switchScaleFactor":F
    const/4 v15, 0x0

    .line 117
    .local v15, "titleScaleFactor":F
    const/16 v16, 0x0

    .line 119
    .local v16, "titleTopMargin":F
    const/4 v10, 0x0

    .line 120
    .local v10, "subTitleAlpha":F
    const/4 v11, 0x0

    .line 122
    .local v11, "subTitleScaleFactor":F
    const/4 v8, 0x0

    .line 123
    .local v8, "subTitle2Alpha":F
    const/4 v9, 0x0

    .line 125
    .local v9, "subTitle2ScaleFactor":F
    move-object/from16 v3, p1

    .line 126
    .local v3, "itemState":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    const/16 v17, 0x0

    check-cast v17, Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 128
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->textAnimationDisabled:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    .line 129
    sget-object v17, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 131
    new-instance v17, Landroid/animation/AnimatorSet;

    invoke-direct/range {v17 .. v17}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v18, v0

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [F

    move-object/from16 v17, v0

    fill-array-data v17, :array_0

    invoke-static/range {v17 .. v17}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v17

    check-cast v17, Landroid/animation/Animator;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 140
    :cond_2
    sget-object v17, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_0

    .line 160
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasSubTitle:Z

    move/from16 v17, v0

    if-nez v17, :cond_3

    .line 163
    const/16 v16, 0x0

    .line 164
    const/4 v10, 0x0

    .line 165
    const/4 v8, 0x0

    .line 169
    :cond_3
    sget-object v17, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual/range {p2 .. p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_1

    .line 184
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setPivotX(F)V

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    sget v18, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->titleHeight:F

    const/16 v19, 0x2

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setPivotY(F)V

    .line 189
    sget-object v17, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual/range {p2 .. p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_2

    .line 204
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setPivotX(F)V

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    sget v18, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->subTitleHeight:F

    const/16 v19, 0x2

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setPivotY(F)V

    .line 209
    move-object/from16 v2, p2

    .line 210
    .local v2, "animation2":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasSubTitle:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 211
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    .line 213
    :cond_4
    sget-object v17, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_3

    .line 226
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setPivotX(F)V

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    move-object/from16 v17, v0

    sget v18, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->subTitleHeight:F

    const/16 v19, 0x2

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setPivotY(F)V

    .line 231
    move-object/from16 v2, p2

    .line 232
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasSubTitle2:Z

    move/from16 v17, v0

    if-nez v17, :cond_5

    .line 233
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    .line 235
    :cond_5
    sget-object v17, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_4

    .line 248
    :goto_5
    sget-object v17, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    if-eqz p4, :cond_0

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v17, v0

    if-eqz v17, :cond_c

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->fluctuatorStartListener:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;

    move-object/from16 v17, v0

    check-cast v17, Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/16 :goto_0

    .line 142
    .end local v2    # "animation2":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    :pswitch_0
    const/high16 v14, 0x3f800000    # 1.0f

    .line 143
    const/high16 v15, 0x3f800000    # 1.0f

    .line 144
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->titleSelectedTopMargin:F

    move/from16 v16, v0

    .line 145
    const/high16 v10, 0x3f800000    # 1.0f

    .line 146
    const/high16 v11, 0x3f800000    # 1.0f

    .line 147
    const/high16 v8, 0x3f800000    # 1.0f

    .line 148
    const/high16 v9, 0x3f800000    # 1.0f

    goto/16 :goto_1

    .line 152
    :pswitch_1
    const v14, 0x3f19999a    # 0.6f

    .line 153
    move-object/from16 v0, p0

    iget v15, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->titleUnselectedScale:F

    .line 154
    const/16 v16, 0x0

    .line 155
    const/4 v10, 0x0

    .line 156
    move-object/from16 v0, p0

    iget v11, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->titleUnselectedScale:F

    .line 157
    const/4 v8, 0x0

    .line 158
    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->titleUnselectedScale:F

    goto/16 :goto_1

    .line 171
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/view/ViewGroup;->setScaleY(F)V

    goto/16 :goto_2

    .line 176
    :pswitch_3
    new-instance v17, Landroid/animation/AnimatorSet;

    invoke-direct/range {v17 .. v17}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 178
    sget-object v17, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v14, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    .line 179
    .local v12, "switchP1":Landroid/animation/PropertyValuesHolder;
    sget-object v17, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v14, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    .line 181
    .local v13, "switchP2":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v12, v19, v20

    const/16 v20, 0x1

    aput-object v13, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v17

    check-cast v17, Landroid/animation/Animator;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v18, v0

    if-nez v18, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v12, v19, v20

    const/16 v20, 0x1

    aput-object v13, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v17

    check-cast v17, Landroid/animation/Animator;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_2

    .line 191
    .end local v12    # "switchP1":Landroid/animation/PropertyValuesHolder;
    .end local v13    # "switchP2":Landroid/animation/PropertyValuesHolder;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setScaleX(F)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setScaleY(F)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    if-nez v4, :cond_7

    new-instance v17, Lkotlin/TypeCastException;

    const-string v18, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct/range {v17 .. v18}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v17

    :cond_7
    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 194
    .local v4, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->requestLayout()V

    goto/16 :goto_3

    .line 199
    .end local v4    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :pswitch_5
    sget-object v17, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v15, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 200
    .local v5, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v17, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v15, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    .line 201
    .local v6, "p2":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v18, v0

    if-nez v18, :cond_8

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v5, v19, v20

    const/16 v20, 0x1

    aput-object v6, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v17

    check-cast v17, Landroid/animation/Animator;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v18, v0

    if-nez v18, :cond_9

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    check-cast v17, Landroid/view/View;

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->animateMargin(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_3

    .line 215
    .end local v5    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v6    # "p2":Landroid/animation/PropertyValuesHolder;
    .restart local v2    # "animation2":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setAlpha(F)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setScaleX(F)V

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setScaleY(F)V

    goto/16 :goto_4

    .line 221
    :pswitch_7
    sget-object v17, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v11, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 222
    .restart local v5    # "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v17, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v11, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    .line 223
    .restart local v6    # "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v17, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v10, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    .line 224
    .local v7, "p3":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v18, v0

    if-nez v18, :cond_a

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v5, v19, v20

    const/16 v20, 0x1

    aput-object v6, v19, v20

    const/16 v20, 0x2

    aput-object v7, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v17

    check-cast v17, Landroid/animation/Animator;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_4

    .line 237
    .end local v5    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v6    # "p2":Landroid/animation/PropertyValuesHolder;
    .end local v7    # "p3":Landroid/animation/PropertyValuesHolder;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setAlpha(F)V

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setScaleX(F)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setScaleY(F)V

    goto/16 :goto_5

    .line 243
    :pswitch_9
    sget-object v17, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v9, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 244
    .restart local v5    # "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v17, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v9, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    .line 245
    .restart local v6    # "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v17, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [F

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v10, v18, v19

    invoke-static/range {v17 .. v18}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    .line 246
    .restart local v7    # "p3":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v18, v0

    if-nez v18, :cond_b

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v5, v19, v20

    const/16 v20, 0x1

    aput-object v6, v19, v20

    const/16 v20, 0x2

    aput-object v7, v19, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v17

    check-cast v17, Landroid/animation/Animator;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_5

    .line 254
    .end local v5    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v6    # "p2":Landroid/animation/PropertyValuesHolder;
    .end local v7    # "p3":Landroid/animation/PropertyValuesHolder;
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->startFluctuator()V

    goto/16 :goto_0

    .line 132
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 140
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 169
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 189
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 213
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 235
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method protected final setSubTitle(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle:Landroid/widget/TextView;

    return-void
.end method

.method protected final setSubTitle2(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->subTitle2:Landroid/widget/TextView;

    return-void
.end method

.method protected final setSwitchBackground(Landroid/view/View;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchBackground:Landroid/view/View;

    return-void
.end method

.method protected final setSwitchContainer(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method protected final setSwitchThumb(Landroid/view/View;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->switchThumb:Landroid/view/View;

    return-void
.end method

.method protected final setTextAnimationDisabled(Z)V
    .locals 0
    .param p1, "<set-?>"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->textAnimationDisabled:Z

    return-void
.end method

.method protected final setTextContainer(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->textContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method protected final setTitle(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "<set-?>"    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->title:Landroid/widget/TextView;

    return-void
.end method

.method public startFluctuator()V
    .locals 2

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->hasIconFluctuatorColor:Z

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/SwitchHaloView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/SwitchHaloView;->start()V

    .line 328
    :cond_0
    return-void
.end method
