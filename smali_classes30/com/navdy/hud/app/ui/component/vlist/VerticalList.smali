.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.super Ljava/lang/Object;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;,
        Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
    }
.end annotation


# static fields
.field private static final BLANK_ITEM_BOTTOM:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final BLANK_ITEM_TOP:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final FONT_SIZES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final ICON_BK_COLOR_SCALE_FACTOR:F = 0.83f

.field public static final ICON_SCALE_FACTOR:F = 0.6f

.field public static final ITEM_BACK_FADE_IN_DURATION:I = 0x64

.field public static final ITEM_INIT_ANIMATION_DURATION:I = 0x64

.field public static final ITEM_MOVE_ANIMATION_DURATION:I = 0xe6

.field public static final ITEM_SCROLL_ANIMATION:I = 0x96

.field public static final ITEM_SELECT_ANIMATION_DURATION:I = 0x32

.field public static final LOADING_ADAPTER_POS:I = 0x1

.field private static final MAX_OFF_SCREEN_VIEWS:I = 0x5

.field private static final MAX_SCROLL_BY:F = 2.0f

.field private static final SCROLL_BY_INCREMENT:F = 0.5f

.field private static final SINGLE_LINE_MAX_LINES:[I

.field private static final START_SCROLL_BY:F = 1.0f

.field public static final TEXT_SCALE_FACTOR_16:F = 1.0f

.field public static final TEXT_SCALE_FACTOR_18:F = 1.0f

.field public static final TEXT_SCALE_FACTOR_22:F = 0.81f

.field public static final TEXT_SCALE_FACTOR_26:F = 0.69f

.field private static final TITLE_SIZES:[F

.field private static final TWO_LINE_MAX_LINES:[I

.field public static final fontSizeTextView:Landroid/widget/TextView;

.field private static handler:Landroid/os/Handler;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final tit_subt_map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final tit_subt_map_2_lines:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final tit_subt_subt2_map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final vlistTitleTextW:I

.field public static final vlistTitle_16:F

.field public static final vlistTitle_16_16_subtitle_top:F

.field public static final vlistTitle_16_16_title_top:F

.field public static final vlistTitle_16_top_m_2:F

.field public static final vlistTitle_16_top_m_3:F

.field public static final vlistTitle_18:F

.field public static final vlistTitle_18_16_subtitle_top:F

.field public static final vlistTitle_18_16_title_top:F

.field public static final vlistTitle_18_top_m_2:F

.field public static final vlistTitle_18_top_m_3:F

.field public static final vlistTitle_22:F

.field public static final vlistTitle_22_16_subtitle_top:F

.field public static final vlistTitle_22_16_title_top:F

.field public static final vlistTitle_22_18_subtitle_top:F

.field public static final vlistTitle_22_18_title_top:F

.field public static final vlistTitle_22_top_m_2:F

.field public static final vlistTitle_22_top_m_3:F

.field public static final vlistTitle_26:F

.field public static final vlistTitle_26_16_subtitle_top:F

.field public static final vlistTitle_26_16_title_top:F

.field public static final vlistTitle_26_18_subtitle_top:F

.field public static final vlistTitle_26_18_title_top:F

.field public static final vlistTitle_26_top_m_2:F

.field public static final vlistTitle_26_top_m_3:F

.field public static final vlistsubTitle2_16_top_m_3:F

.field public static final vlistsubTitle2_18_top_m_3:F

.field public static final vlistsubTitle2_22_top_m_3:F

.field public static final vlistsubTitle2_26_top_m_3:F

.field public static final vlistsubTitle_16_top_m_2:F

.field public static final vlistsubTitle_16_top_m_3:F

.field public static final vlistsubTitle_18_top_m_2:F

.field public static final vlistsubTitle_18_top_m_3:F

.field public static final vlistsubTitle_22_top_m_2:F

.field public static final vlistsubTitle_22_top_m_3:F

.field public static final vlistsubTitle_26_top_m_2:F

.field public static final vlistsubTitle_26_top_m_3:F


# instance fields
.field private actualScrollY:I

.field public adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

.field final animationDuration:I

.field volatile bindCallbacks:Z

.field callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

.field containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

.field private copyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private currentMiddlePosition:I

.field private volatile currentScrollBy:F

.field firstEntryBlank:Z

.field hasScrollableElement:Z

.field private indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

.field private indicatorAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private indicatorAnimatorSet:Landroid/animation/AnimatorSet;

.field private volatile initialPosChanged:Z

.field private initialPosCheckRunnable:Ljava/lang/Runnable;

.field private volatile isScrollBy:Z

.field private volatile isScrolling:Z

.field private volatile isSelectedOperationPending:Z

.field private final itemSelectionState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

.field private final keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

.field private lastScrollState:I

.field private layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

.field private volatile lockList:Z

.field private modelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field reCalcScrollPos:Z

.field private recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

.field private volatile scrollByUpReceived:I

.field scrollItemEndY:I

.field scrollItemHeight:I

.field scrollItemIndex:I

.field scrollItemStartY:I

.field private scrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

.field private volatile scrollPendingPos:I

.field public selectedList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field sendScrollIdleEvent:Z

.field private switchingScrollBoundary:Z

.field private targetFound:Z

.field public targetPos:I

.field private twoLineTitles:Z

.field public waitForTarget:Z


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const v8, 0x3f30a3d7    # 0.69f

    const v7, 0x3f4f5c29    # 0.81f

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 50
    new-instance v3, Lcom/navdy/service/library/log/Logger;

    const-class v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 52
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->handler:Landroid/os/Handler;

    .line 165
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    .line 166
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    .line 167
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    .line 172
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 173
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 174
    .local v2, "resources":Landroid/content/res/Resources;
    const v3, 0x7f0b01e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitleTextW:I

    .line 175
    const v3, 0x7f0b01e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26:F

    .line 176
    const v3, 0x7f0b01e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    .line 177
    const v3, 0x7f0b01e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    .line 178
    const v3, 0x7f0b01e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    .line 180
    const v3, 0x7f0b01b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_top_m_3:F

    .line 181
    const v3, 0x7f0b01b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_16_top_m_3:F

    .line 182
    const v3, 0x7f0b01b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_16_top_m_3:F

    .line 184
    const v3, 0x7f0b01bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_top_m_3:F

    .line 185
    const v3, 0x7f0b01bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_18_top_m_3:F

    .line 186
    const v3, 0x7f0b01bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_18_top_m_3:F

    .line 188
    const v3, 0x7f0b01c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_top_m_3:F

    .line 189
    const v3, 0x7f0b01c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_22_top_m_3:F

    .line 190
    const v3, 0x7f0b01c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_22_top_m_3:F

    .line 192
    const v3, 0x7f0b01d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_top_m_3:F

    .line 193
    const v3, 0x7f0b01cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_26_top_m_3:F

    .line 194
    const v3, 0x7f0b01cd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_26_top_m_3:F

    .line 196
    const v3, 0x7f0b01b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_top_m_2:F

    .line 197
    const v3, 0x7f0b01b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_16_top_m_2:F

    .line 199
    const v3, 0x7f0b01be

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_top_m_2:F

    .line 200
    const v3, 0x7f0b01bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_18_top_m_2:F

    .line 202
    const v3, 0x7f0b01c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_top_m_2:F

    .line 203
    const v3, 0x7f0b01c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_22_top_m_2:F

    .line 205
    const v3, 0x7f0b01d0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_top_m_2:F

    .line 206
    const v3, 0x7f0b01ce

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_26_top_m_2:F

    .line 209
    const v3, 0x7f0b01cc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_18_title_top:F

    .line 210
    const v3, 0x7f0b01cb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_18_subtitle_top:F

    .line 211
    const v3, 0x7f0b01c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_18_title_top:F

    .line 212
    const v3, 0x7f0b01c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_18_subtitle_top:F

    .line 213
    const v3, 0x7f0b01ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_16_title_top:F

    .line 214
    const v3, 0x7f0b01b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_16_subtitle_top:F

    .line 215
    const v3, 0x7f0b01b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_16_title_top:F

    .line 216
    const v3, 0x7f0b01b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_16_subtitle_top:F

    .line 217
    const v3, 0x7f0b01ca

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_16_title_top:F

    .line 218
    const v3, 0x7f0b01c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_16_subtitle_top:F

    .line 219
    const v3, 0x7f0b01c1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_16_title_top:F

    .line 220
    const v3, 0x7f0b01c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_16_subtitle_top:F

    .line 223
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 224
    .local v1, "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 225
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 226
    iput v8, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 227
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 228
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_26_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 229
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    .line 230
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_26_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    .line 231
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 234
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 235
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 236
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 237
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 238
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 239
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_22_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 240
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    .line 241
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_22_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    .line 242
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 245
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 246
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 247
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 248
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 249
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 250
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_22_top_m_3:F

    const/high16 v4, 0x40400000    # 3.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 251
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    .line 252
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_22_top_m_3:F

    const/high16 v4, 0x40400000    # 3.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    .line 253
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 256
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 257
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 258
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 259
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 260
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_18_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 261
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    .line 262
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_18_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    .line 263
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 266
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 267
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 268
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 269
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 270
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 271
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_18_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 272
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    .line 273
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_18_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    .line 274
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 277
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 278
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 279
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 280
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 281
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_16_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 282
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    .line 283
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_16_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    .line 284
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 287
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 288
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 289
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 290
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 291
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 292
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_16_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 293
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    .line 294
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle2_16_top_m_3:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    .line 295
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 299
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 300
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 301
    iput v8, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 302
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 303
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_26_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 304
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 307
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 308
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 309
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 310
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 311
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_22_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 312
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 315
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 316
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_top_m_2:F

    const/high16 v4, 0x40400000    # 3.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 317
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 318
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 319
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 320
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_22_top_m_2:F

    const/high16 v4, 0x41100000    # 9.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 321
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 324
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 325
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 326
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 327
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 328
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_18_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 329
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 332
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 333
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_top_m_2:F

    add-float/2addr v3, v5

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 334
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 335
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 336
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 337
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_18_top_m_2:F

    const/high16 v4, 0x40e00000    # 7.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 338
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 341
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 342
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 343
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 344
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 345
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_16_top_m_2:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 346
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 349
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 350
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_top_m_2:F

    add-float/2addr v3, v5

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 351
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 352
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 353
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 354
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_16_top_m_2:F

    const/high16 v4, 0x40e00000    # 7.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 355
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 359
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 360
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_18_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 361
    iput v8, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 362
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 363
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_18_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 364
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 367
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 368
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_18_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 369
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 370
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 371
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_18_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 372
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 375
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 376
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_18_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 377
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 378
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 379
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 380
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_18_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 381
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 384
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 385
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_16_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 386
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 387
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 388
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_16_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 389
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 392
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 393
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18_top_m_2:F

    add-float/2addr v3, v5

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 394
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 395
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 396
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 397
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistsubTitle_18_top_m_2:F

    const/high16 v4, 0x40e00000    # 7.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 398
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 401
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 402
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_16_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 403
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 404
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 405
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_16_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 406
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 409
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 410
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_16_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 411
    iput v5, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 412
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 413
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 414
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16_16_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 415
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 418
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 419
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_16_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 420
    iput v8, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 421
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 422
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26_16_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 423
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 426
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 427
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_16_title_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 428
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 429
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 430
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_16_subtitle_top:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 431
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .end local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;-><init>()V

    .line 434
    .restart local v1    # "fontInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    .line 435
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_top_m_2:F

    add-float/2addr v3, v5

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    .line 436
    iput v7, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    .line 437
    iput-boolean v6, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    .line 438
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    .line 439
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22_16_subtitle_top:F

    const/high16 v4, 0x40c00000    # 6.0f

    add-float/2addr v3, v4

    iput v3, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    .line 440
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->fontSizeTextView:Landroid/widget/TextView;

    .line 443
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->fontSizeTextView:Landroid/widget/TextView;

    const v4, 0x7f0c0034

    invoke-virtual {v3, v0, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 704
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;->buildModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->BLANK_ITEM_TOP:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 705
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;->buildModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->BLANK_ITEM_BOTTOM:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 2021
    new-array v3, v9, [F

    sget v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_26:F

    aput v4, v3, v6

    const/4 v4, 0x1

    sget v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_22:F

    aput v5, v3, v4

    const/4 v4, 0x2

    sget v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    aput v5, v3, v4

    const/4 v4, 0x3

    sget v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_16:F

    aput v5, v3, v4

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->TITLE_SIZES:[F

    .line 2022
    new-array v3, v9, [I

    fill-array-data v3, :array_0

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->TWO_LINE_MAX_LINES:[I

    .line 2023
    new-array v3, v9, [I

    fill-array-data v3, :array_1

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->SINGLE_LINE_MAX_LINES:[I

    .line 2025
    const/16 v3, 0x8

    new-array v3, v3, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v4, v3, v6

    const/4 v4, 0x1

    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v5, v3, v4

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v4, v3, v9

    const/4 v4, 0x5

    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v5, v3, v4

    const/4 v4, 0x7

    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v5, v3, v4

    sput-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->FONT_SIZES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    return-void

    .line 2022
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x2
        0x2
    .end array-data

    .line 2023
    :array_1
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;)V
    .locals 6
    .param p1, "recyclerView"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;
    .param p2, "indicator"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .param p3, "callback"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;
    .param p4, "containerCallback"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    .prologue
    .line 905
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;-><init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;Z)V

    .line 906
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;Z)V
    .locals 8
    .param p1, "recyclerView"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;
    .param p2, "indicator"    # Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .param p3, "callback"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;
    .param p4, "containerCallback"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;
    .param p5, "twoLineTitles"    # Z

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v5, 0xa

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 738
    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollPendingPos:I

    .line 745
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 763
    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    .line 764
    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    .line 765
    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    .line 766
    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemHeight:I

    .line 772
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;-><init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->initialPosCheckRunnable:Ljava/lang/Runnable;

    .line 810
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    invoke-direct {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    .line 811
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    invoke-direct {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->itemSelectionState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .line 813
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->copyList:Ljava/util/ArrayList;

    .line 815
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;-><init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    .line 1564
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->selectedList:Ljava/util/HashSet;

    .line 913
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 914
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 918
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "ctor"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 920
    iput-boolean p5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->twoLineTitles:Z

    .line 921
    const/16 v2, 0xe6

    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animationDuration:I

    .line 922
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 923
    .local v0, "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    .line 924
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->setItemViewCacheSize(I)V

    .line 925
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v1

    .line 926
    .local v1, "viewPool":Landroid/support/v7/widget/RecyclerView$RecycledViewPool;
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->BLANK:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v7}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 927
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v5}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 928
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v5}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 929
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v5}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 930
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 931
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TITLE_SUBTITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 932
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 933
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v4}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 934
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SCROLL_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v6}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 935
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v5}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 936
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->setOverScrollMode(I)V

    .line 937
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->setVerticalScrollBarEnabled(Z)V

    .line 938
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->setHorizontalScrollBarEnabled(Z)V

    .line 940
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 941
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    sget-object v3, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;->VERTICAL:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setOrientation(Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator$Orientation;)V

    .line 943
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    .line 944
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    .line 946
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-direct {v2, v0, p0, p3, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    .line 947
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->setOrientation(I)V

    .line 948
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 949
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 950
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollBy:Z

    return p1
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollByUpReceived:I

    return p1
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getPositionFromScrollY(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    return v0
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    return p1
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isSelectedOperationPending:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isSelectedOperationPending:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->select(Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollPendingPos:I

    return v0
.end method

.method static synthetic access$1702(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollPendingPos:I

    return p1
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isCloseMenuVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->initialPosChanged:Z

    return v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->initialPosChanged:Z

    return p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    return p1
.end method

.method static synthetic access$500()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->onItemSelected()V

    return-void
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lastScrollState:I

    return p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->calculateScrollRange()V

    return-void
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    return p1
.end method

.method private addExtraItems(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1135
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->firstEntryBlank:Z

    if-eqz v0, :cond_0

    .line 1137
    const/4 v0, 0x0

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->BLANK_ITEM_TOP:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1139
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->BLANK_ITEM_BOTTOM:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1140
    return-void
.end method

.method private animate(II)V
    .locals 6
    .param p1, "oldPos"    # I
    .param p2, "newPos"    # I

    .prologue
    const/4 v4, 0x0

    .line 1451
    const/4 v3, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(IIZZZ)V

    .line 1452
    return-void
.end method

.method private animate(IIZZZ)V
    .locals 6
    .param p1, "oldPos"    # I
    .param p2, "newPos"    # I
    .param p3, "updateIndicator"    # Z
    .param p4, "performSelection"    # Z
    .param p5, "checkAdapterCache"    # Z

    .prologue
    const/16 v5, 0xe6

    .line 1460
    const/4 v2, -0x1

    if-eq p1, v2, :cond_1

    .line 1461
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    .line 1462
    .local v1, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    if-eqz v1, :cond_3

    move-object v0, v1

    .line 1463
    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1464
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v2, v3, :cond_0

    .line 1465
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v0, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 1472
    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_0
    :goto_0
    if-eqz p4, :cond_1

    .line 1473
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    .line 1474
    if-eqz v1, :cond_4

    move-object v0, v1

    .line 1475
    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1476
    .restart local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isCloseMenuVisible()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1477
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v0, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 1501
    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .end local v1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_1
    :goto_1
    if-eqz p3, :cond_2

    .line 1502
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animationDuration:I

    invoke-virtual {p0, v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animateIndicator(II)V

    .line 1504
    :cond_2
    return-void

    .line 1469
    .restart local v1    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "viewHolder {"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "} not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1481
    :cond_4
    if-eqz p5, :cond_6

    .line 1483
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v2, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getHolderForPos(I)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    move-result-object v0

    .line 1484
    .restart local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-nez v0, :cond_5

    .line 1485
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "viewHolder-sel {"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "} not found in adapter"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1486
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v2, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->setHighlightIndex(I)V

    goto :goto_1

    .line 1489
    :cond_5
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isCloseMenuVisible()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1490
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v0, v2, v3, v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    goto :goto_1

    .line 1495
    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_6
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "viewHolder-sel {"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "} not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private calculateScrollPos(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 2099
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    add-int/lit8 v0, v0, 0x1

    if-eq p1, v0, :cond_1

    .line 2106
    :cond_0
    :goto_0
    return-void

    .line 2102
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->reCalcScrollPos:Z

    .line 2103
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    .line 2104
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    .line 2105
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollStateChanged(( calculateScrollPos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scrollIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private calculateScrollRange()V
    .locals 4

    .prologue
    .line 2061
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 2062
    .local v0, "viewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    instance-of v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ScrollableViewHolder;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 2063
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    mul-int/2addr v1, v2

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    .line 2064
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemHeight:I

    .line 2065
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemHeight:I

    add-int/2addr v1, v2

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/lit8 v2, v2, 0x3

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    .line 2067
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->reCalcScrollPos:Z

    if-eqz v1, :cond_0

    .line 2068
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calculateScrollPos: recalc scrollItemHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " scrollItemStartY="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " scrollItemEndY="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2072
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->calculateScrollPos(I)V

    .line 2077
    :cond_0
    :goto_0
    return-void

    .line 2075
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calculateScrollRange:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vh not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private canScrollDown()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 1664
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-nez v1, :cond_1

    .line 1676
    :cond_0
    :goto_0
    return v0

    .line 1668
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->calculateScrollRange()V

    .line 1670
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    if-eq v1, v0, :cond_0

    .line 1671
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    if-ge v1, v2, :cond_0

    .line 1672
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private canScrollUp()I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1682
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-nez v2, :cond_1

    .line 1695
    :cond_0
    :goto_0
    return v1

    .line 1686
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->calculateScrollRange()V

    .line 1688
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    if-eq v2, v1, :cond_0

    .line 1689
    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    mul-int v0, v2, v3

    .line 1690
    .local v0, "scrollItemY":I
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    if-le v2, v0, :cond_0

    .line 1691
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    sub-int/2addr v1, v0

    goto :goto_0
.end method

.method private cleanupView(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V
    .locals 2
    .param p1, "pool"    # Landroid/support/v7/widget/RecyclerView$RecycledViewPool;
    .param p2, "viewType"    # I

    .prologue
    .line 1893
    :goto_0
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->getRecycledView(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1894
    .local v0, "viewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_0

    .line 1895
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    .line 1896
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0

    .line 1901
    :cond_0
    return-void
.end method

.method private clearViewAnimation(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V
    .locals 2
    .param p1, "pool"    # Landroid/support/v7/widget/RecyclerView$RecycledViewPool;
    .param p2, "viewType"    # I

    .prologue
    .line 1881
    :goto_0
    invoke-virtual {p1, p2}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->getRecycledView(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1882
    .local v0, "viewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_0

    .line 1883
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    .line 1884
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->copyList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1889
    :cond_0
    return-void
.end method

.method private down(II)V
    .locals 2
    .param p1, "oldPos"    # I
    .param p2, "newPos"    # I

    .prologue
    .line 1344
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1345
    iput p2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1346
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(II)V

    .line 1347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    .line 1348
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->smoothScrollToPosition(II)V

    .line 1349
    return-void
.end method

.method public static findCrossFadeImageView(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p0, "layout"    # Landroid/view/View;

    .prologue
    .line 1798
    const v0, 0x7f0e008e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static findImageView(Landroid/view/View;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "layout"    # Landroid/view/View;

    .prologue
    .line 1790
    const v0, 0x7f0e00d6

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public static findSmallImageView(Landroid/view/View;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "layout"    # Landroid/view/View;

    .prologue
    .line 1794
    const v0, 0x7f0e00d7

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private getExtraItemCount()I
    .locals 1

    .prologue
    .line 1700
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    return v0
.end method

.method public static getFontInfo(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
    .locals 1
    .param p0, "fontSize"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .prologue
    .line 447
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    return-object v0
.end method

.method public static getInterpolator()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 1561
    new-instance v0, Lcom/navdy/hud/app/ui/interpolator/FastOutSlowInInterpolator;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/interpolator/FastOutSlowInInterpolator;-><init>()V

    return-object v0
.end method

.method private getPositionFromScrollY(I)I
    .locals 3
    .param p1, "scrollPos"    # I

    .prologue
    .line 2081
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-nez v1, :cond_0

    .line 2082
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    div-int v1, p1, v1

    add-int/lit8 v0, v1, 0x1

    .line 2095
    .local v0, "pos":I
    :goto_0
    return v0

    .line 2084
    .end local v0    # "pos":I
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 2085
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->calculateScrollRange()V

    .line 2087
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    if-ge p1, v1, :cond_2

    .line 2088
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    div-int v1, p1, v1

    add-int/lit8 v0, v1, 0x1

    .restart local v0    # "pos":I
    goto :goto_0

    .line 2089
    .end local v0    # "pos":I
    :cond_2
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    if-le p1, v1, :cond_3

    .line 2090
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    sub-int v1, p1, v1

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    add-int v0, v1, v2

    .restart local v0    # "pos":I
    goto :goto_0

    .line 2092
    .end local v0    # "pos":I
    :cond_3
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    .restart local v0    # "pos":I
    goto :goto_0
.end method

.method private isCloseMenuVisible()Z
    .locals 1

    .prologue
    .line 2054
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    if-eqz v0, :cond_0

    .line 2055
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;->isCloseMenuVisible()Z

    move-result v0

    .line 2057
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLocked()Z
    .locals 1

    .prologue
    .line 1704
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lockList:Z

    return v0
.end method

.method private isOverrideKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "keyEvent"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 1813
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1814
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1815
    const/4 v1, 0x1

    .line 1817
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isScrollItemAlignedToBottomEdge()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1640
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-nez v2, :cond_1

    .line 1644
    :cond_0
    :goto_0
    return v1

    .line 1643
    :cond_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    sget v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    add-int v0, v2, v3

    .line 1644
    .local v0, "scrollItemY":I
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    if-ne v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isScrollItemAlignedToTopEdge()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1632
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-nez v2, :cond_1

    .line 1636
    :cond_0
    :goto_0
    return v1

    .line 1635
    :cond_1
    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    mul-int v0, v2, v3

    .line 1636
    .local v0, "scrollItemY":I
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    if-ne v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isScrollItemInMiddle()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1624
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    if-gtz v2, :cond_1

    .line 1628
    :cond_0
    :goto_0
    return v1

    .line 1627
    :cond_1
    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    add-int/lit8 v3, v3, -0x1

    mul-int v0, v2, v3

    .line 1628
    .local v0, "scrollItemY":I
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    if-ne v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isScrollPosInScrollItem()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1648
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-nez v1, :cond_1

    .line 1657
    :cond_0
    :goto_0
    return v0

    .line 1652
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->calculateScrollRange()V

    .line 1654
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1655
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    if-lt v1, v2, :cond_0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onItemSelected()V
    .locals 8

    .prologue
    .line 792
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v3

    .line 793
    .local v3, "userPos":I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    .line 794
    .local v1, "current":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-eqz v1, :cond_1

    .line 795
    const/4 v4, -0x1

    .line 796
    .local v4, "subId":I
    const/4 v5, -0x1

    .line 797
    .local v5, "subPos":I
    iget-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    if-ne v0, v2, :cond_0

    .line 798
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 799
    .local v7, "iconOptionsViewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    instance-of v0, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    if-eqz v0, :cond_0

    move-object v6, v7

    .line 800
    check-cast v6, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .line 801
    .local v6, "iconOptionsVH":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->getCurrentSelectionId()I

    move-result v4

    .line 802
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->getCurrentSelection()I

    move-result v5

    .line 805
    .end local v6    # "iconOptionsVH":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;
    .end local v7    # "iconOptionsViewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->itemSelectionState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    iget v2, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V

    .line 806
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->itemSelectionState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    invoke-interface {v0, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;->onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 808
    .end local v4    # "subId":I
    .end local v5    # "subPos":I
    :cond_1
    return-void
.end method

.method private select(Z)V
    .locals 5
    .param p1, "ignoreLock"    # Z

    .prologue
    const/4 v4, 0x1

    .line 1356
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isLocked()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez p1, :cond_1

    .line 1375
    :cond_0
    :goto_0
    return-void

    .line 1360
    :cond_1
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lockList:Z

    .line 1361
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    if-eqz v3, :cond_2

    .line 1363
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isSelectedOperationPending:Z

    goto :goto_0

    .line 1367
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_SELECT:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v3}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1368
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v1

    .line 1369
    .local v1, "pos":I
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->modelList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 1371
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1372
    .local v2, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v2, :cond_0

    .line 1373
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v3

    const/16 v4, 0x32

    invoke-virtual {v2, v0, v3, v4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V

    goto :goto_0
.end method

.method public static setFontSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V
    .locals 2
    .param p0, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p1, "allowTwoLineTitles"    # Z

    .prologue
    .line 1905
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSizeCheckDone:Z

    .line 1906
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    .line 1908
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1909
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .line 1910
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1921
    :goto_0
    return-void

    .line 1914
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1916
    :cond_1
    invoke-static {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setTitleSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    goto :goto_0

    .line 1919
    :cond_2
    invoke-static {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setTitleSubTitleSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    goto :goto_0
.end method

.method private static setTitleSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V
    .locals 8
    .param p0, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p1, "allowTwoLines"    # Z

    .prologue
    .line 2036
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->fontSizeTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2038
    const/4 v4, 0x2

    new-array v2, v4, [I

    .line 2039
    .local v2, "index":[I
    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->fontSizeTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->TWO_LINE_MAX_LINES:[I

    :goto_0
    sget v6, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitleTextW:I

    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->TITLE_SIZES:[F

    invoke-static {v5, v4, v6, v7, v2}, Lcom/navdy/hud/app/util/ViewUtil;->autosize(Landroid/widget/TextView;[II[F[I)F

    .line 2040
    const/4 v4, 0x0

    aget v0, v2, v4

    .line 2041
    .local v0, "fontOffset":I
    const/4 v4, 0x1

    aget v3, v2, v4

    .line 2042
    .local v3, "lines":I
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->FONT_SIZES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    mul-int/lit8 v5, v0, 0x2

    add-int/lit8 v6, v3, -0x1

    add-int/2addr v5, v6

    aget-object v1, v4, v5

    .line 2043
    .local v1, "fontSize":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting size for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 2045
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 2046
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_subt2_map:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .line 2050
    :goto_1
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 2051
    return-void

    .line 2039
    .end local v0    # "fontOffset":I
    .end local v1    # "fontSize":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
    .end local v3    # "lines":I
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->SINGLE_LINE_MAX_LINES:[I

    goto :goto_0

    .line 2048
    .restart local v0    # "fontOffset":I
    .restart local v1    # "fontSize":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
    .restart local v3    # "lines":I
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    goto :goto_1
.end method

.method private static setTitleSubTitleSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V
    .locals 12
    .param p0, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p1, "allowTwoLineTitles"    # Z

    .prologue
    const/4 v11, 0x1

    .line 1925
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->fontSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 1926
    .local v2, "paint":Landroid/text/TextPaint;
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->fontSizeTextView:Landroid/widget/TextView;

    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitle_18:F

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1927
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    sget v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->vlistTitleTextW:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 1928
    .local v0, "layout":Landroid/text/StaticLayout;
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v9

    .line 1929
    .local v9, "subTitleLines":I
    move v10, v9

    .line 1930
    .local v10, "subTitleTotal":I
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1931
    add-int/lit8 v10, v10, 0x1

    .line 1934
    :cond_0
    if-le v10, v11, :cond_1

    .line 1936
    const/4 p1, 0x0

    .line 1938
    :cond_1
    invoke-static {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setTitleSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    .line 1941
    if-ne v9, v11, :cond_3

    .line 2019
    :cond_2
    :goto_0
    return-void

    .line 1946
    :cond_3
    iput-boolean v11, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    .line 1948
    const/4 v8, 0x0

    .line 1950
    .local v8, "newFontSize":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 1952
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize:[I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 2013
    :goto_1
    if-eqz v8, :cond_2

    .line 2014
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->tit_subt_map_2_lines:Ljava/util/HashMap;

    invoke-virtual {v1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .line 2015
    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 2016
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adjusting size for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 1954
    :pswitch_0
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1955
    goto :goto_1

    .line 1958
    :pswitch_1
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1959
    goto :goto_1

    .line 1962
    :pswitch_2
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1963
    goto :goto_1

    .line 1966
    :pswitch_3
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1967
    goto :goto_1

    .line 1970
    :pswitch_4
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1971
    goto :goto_1

    .line 1974
    :pswitch_5
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1975
    goto :goto_1

    .line 1978
    :pswitch_6
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    goto :goto_1

    .line 1983
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$FontSize:[I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    .line 1985
    :pswitch_7
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1986
    goto :goto_1

    .line 1989
    :pswitch_8
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1990
    goto :goto_1

    .line 1993
    :pswitch_9
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1994
    goto :goto_1

    .line 1997
    :pswitch_a
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 1998
    goto :goto_1

    .line 2001
    :pswitch_b
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 2002
    goto :goto_1

    .line 2005
    :pswitch_c
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 2006
    goto :goto_1

    .line 2009
    :pswitch_d
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    goto :goto_1

    .line 1952
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 1983
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private up(II)V
    .locals 8
    .param p1, "oldPos"    # I
    .param p2, "newPos"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 1221
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1222
    iput p2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1223
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    .line 1224
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1225
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-eqz v0, :cond_3

    .line 1226
    const/4 v3, 0x1

    .line 1227
    .local v3, "updateIndicator":Z
    const/4 v4, 0x0

    .line 1228
    .local v4, "highlightItem":Z
    const/4 v5, 0x0

    .line 1229
    .local v5, "checkAdapterCache":Z
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollItemInMiddle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1231
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    .line 1232
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    neg-int v1, v1

    invoke-virtual {v0, v7, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->smoothScrollBy(II)V

    .line 1233
    const/4 v4, 0x1

    :goto_0
    move-object v0, p0

    move v1, p1

    move v2, p2

    .line 1254
    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(IIZZZ)V

    .line 1259
    .end local v3    # "updateIndicator":Z
    .end local v4    # "highlightItem":Z
    .end local v5    # "checkAdapterCache":Z
    :goto_1
    return-void

    .line 1234
    .restart local v3    # "updateIndicator":Z
    .restart local v4    # "highlightItem":Z
    .restart local v5    # "checkAdapterCache":Z
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollItemAlignedToTopEdge()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1236
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollBy:Z

    .line 1237
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    .line 1238
    const/4 v3, 0x1

    .line 1239
    const/4 v4, 0x1

    .line 1240
    const/4 v5, 0x1

    .line 1241
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->smoothScrollToPosition(II)V

    goto :goto_0

    .line 1244
    :cond_1
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    if-ne v0, v1, :cond_2

    .line 1245
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v6, v0

    .line 1246
    .local v6, "distance":F
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    .line 1247
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    neg-float v1, v6

    float-to-int v1, v1

    invoke-virtual {v0, v7, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->smoothScrollBy(II)V

    .line 1248
    const/4 v3, 0x1

    .line 1249
    const/4 v4, 0x1

    .line 1250
    goto :goto_0

    .line 1251
    .end local v6    # "distance":F
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-virtual {v0, p1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->smoothScrollToPosition(II)V

    goto :goto_0

    .line 1256
    .end local v3    # "updateIndicator":Z
    .end local v4    # "highlightItem":Z
    .end local v5    # "checkAdapterCache":Z
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(II)V

    .line 1257
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-virtual {v0, p1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->smoothScrollToPosition(II)V

    goto :goto_1
.end method

.method private updateView(Ljava/util/List;IZZI)V
    .locals 10
    .param p2, "initialSelection"    # I
    .param p3, "firstEntryBlank"    # Z
    .param p4, "hasScrollableElement"    # Z
    .param p5, "scrollIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;IZZI)V"
        }
    .end annotation

    .prologue
    .line 994
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v7

    if-nez v7, :cond_0

    .line 995
    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "updateView can only be called from main thread"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 998
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isLocked()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 999
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "cannot update vlist during lock"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1102
    :goto_0
    return-void

    .line 1003
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 1004
    .local v3, "len":I
    if-nez v3, :cond_2

    .line 1005
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "empty list now allowed"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1008
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearAllAnimations()V

    .line 1009
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->stopScroll()V

    .line 1010
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateView ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] sel:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " firstEntryBlank:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " scrollContent:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " scrollIndex:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1016
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->modelList:Ljava/util/List;

    .line 1018
    const/4 v7, 0x1

    if-gt v3, v7, :cond_3

    .line 1019
    const/4 p2, 0x0

    .line 1022
    :cond_3
    move v4, p2

    .line 1024
    .local v4, "origSelection":I
    iput-boolean p3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->firstEntryBlank:Z

    .line 1025
    iput-boolean p4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    .line 1026
    iput p5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    .line 1027
    const/4 v7, -0x1

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemStartY:I

    .line 1028
    const/4 v7, -0x1

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemEndY:I

    .line 1029
    const/4 v7, -0x1

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemHeight:I

    .line 1031
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->modelList:Ljava/util/List;

    invoke-direct {p0, v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->addExtraItems(Ljava/util/List;)V

    .line 1032
    add-int/lit8 v7, p2, 0x1

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1034
    if-eqz p4, :cond_4

    if-eqz p3, :cond_4

    .line 1035
    iget v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    .line 1038
    :cond_4
    move v2, v3

    .line 1039
    .local v2, "indicatorLen":I
    if-nez p3, :cond_5

    .line 1040
    add-int/lit8 v2, v2, -0x1

    .line 1042
    :cond_5
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v7, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setItemCount(I)V

    .line 1043
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v7, v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 1045
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->clear()V

    .line 1046
    new-instance v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->modelList:Ljava/util/List;

    invoke-direct {v7, v8, p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;-><init>(Ljava/util/List;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V

    iput-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    .line 1047
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->setHasStableIds(Z)V

    .line 1048
    const/4 v1, 0x0

    .line 1049
    .local v1, "cacheIndexSize":I
    if-eqz p4, :cond_6

    .line 1051
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    if-ne v7, v8, :cond_8

    .line 1052
    const/4 v7, 0x1

    new-array v0, v7, [I

    .line 1053
    .local v0, "cacheIndex":[I
    const/4 v7, 0x0

    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    add-int/lit8 v8, v8, -0x1

    aput v8, v0, v7

    .line 1054
    const/4 v1, 0x1

    .line 1061
    :goto_1
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->setViewHolderCacheIndex([I)V

    .line 1063
    .end local v0    # "cacheIndex":[I
    :cond_6
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 1065
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->initialPosChanged:Z

    .line 1066
    iget v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v5, v7, -0x1

    .line 1067
    .local v5, "scrollPos":I
    const/4 v6, 0x0

    .line 1068
    .local v6, "specialScrollPos":Z
    if-nez p4, :cond_9

    .line 1069
    sget v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/2addr v7, p2

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    .line 1089
    :cond_7
    :goto_2
    if-eqz v6, :cond_c

    .line 1090
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "updateView special scroll"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1091
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    add-int/lit8 v8, v5, 0x1

    sget v9, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    invoke-virtual {v7, v8, v9}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->scrollToPositionWithOffset(II)V

    .line 1095
    :goto_3
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateView scroll to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " scrollPos="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " middle="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1097
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " raw="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1098
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " adapter-cache:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " scrollIndex:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1095
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1101
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->handler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->initialPosCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1056
    .end local v5    # "scrollPos":I
    .end local v6    # "specialScrollPos":Z
    :cond_8
    const/4 v7, 0x2

    new-array v0, v7, [I

    .line 1057
    .restart local v0    # "cacheIndex":[I
    const/4 v7, 0x0

    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    add-int/lit8 v8, v8, -0x1

    aput v8, v0, v7

    .line 1058
    const/4 v7, 0x1

    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    add-int/lit8 v8, v8, 0x1

    aput v8, v0, v7

    .line 1059
    const/4 v1, 0x2

    goto/16 :goto_1

    .line 1071
    .end local v0    # "cacheIndex":[I
    .restart local v5    # "scrollPos":I
    .restart local v6    # "specialScrollPos":Z
    :cond_9
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateView initial:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " scrollIndex="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1072
    if-ne p2, p5, :cond_a

    .line 1073
    iget v5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1074
    add-int/lit8 v7, p2, 0x1

    sget v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/2addr v7, v8

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    .line 1075
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateView: scroll index selected:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1076
    :cond_a
    iget v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    if-ge p2, v7, :cond_b

    .line 1078
    sget v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/2addr v7, p2

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    goto/16 :goto_2

    .line 1081
    :cond_b
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->reCalcScrollPos:Z

    .line 1082
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "reCalcScrollPos no ht"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1083
    sget v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/2addr v7, p2

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    .line 1084
    add-int/lit8 v7, p5, 0x1

    if-ne p2, v7, :cond_7

    .line 1085
    const/4 v6, 0x1

    goto/16 :goto_2

    .line 1093
    :cond_c
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    const/4 v8, 0x0

    invoke-virtual {v7, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->scrollToPositionWithOffset(II)V

    goto/16 :goto_3
.end method


# virtual methods
.method public addCurrentHighlightAnimation(Landroid/animation/AnimatorSet$Builder;)V
    .locals 7
    .param p1, "builder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1714
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1715
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_0

    .line 1716
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->layout:Landroid/view/ViewGroup;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    aput v4, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1719
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1720
    .restart local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_1

    .line 1721
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->layout:Landroid/view/ViewGroup;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    aput v4, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1723
    :cond_1
    return-void
.end method

.method public allowsTwoLineTitles()Z
    .locals 1

    .prologue
    .line 986
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->twoLineTitles:Z

    return v0
.end method

.method public animate(IZIZ)V
    .locals 6
    .param p1, "pos"    # I
    .param p2, "selected"    # Z
    .param p3, "duration"    # I
    .param p4, "notAllowStateChange"    # Z

    .prologue
    .line 1507
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(IZIZZ)V

    .line 1508
    return-void
.end method

.method public animate(IZIZZ)V
    .locals 5
    .param p1, "pos"    # I
    .param p2, "selected"    # Z
    .param p3, "duration"    # I
    .param p4, "notAllowStateChange"    # Z
    .param p5, "changeToolTips"    # Z

    .prologue
    const/4 v3, 0x2

    .line 1511
    const/4 v2, -0x1

    if-ne p3, v2, :cond_0

    .line 1512
    iget p3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animationDuration:I

    .line 1514
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    .line 1515
    .local v1, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    if-eqz v1, :cond_5

    move-object v0, v1

    .line 1516
    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1517
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz p2, :cond_3

    .line 1518
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1519
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "animate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " selected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1521
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v0, v2, v3, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 1522
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->hasToolTip()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    if-eqz v2, :cond_2

    .line 1523
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;->showToolTips()V

    .line 1539
    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_2
    :goto_0
    return-void

    .line 1526
    .restart local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1527
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "animate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " un-selected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1529
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v0, v2, v3, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 1530
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->hasToolTip()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    if-eqz v2, :cond_2

    .line 1531
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;->hideToolTips()V

    goto :goto_0

    .line 1535
    .end local v0    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_5
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1536
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "animate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cannot find viewholder"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public animateIndicator(II)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "duration"    # I

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;->isFastScrolling()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1558
    :cond_0
    :goto_0
    return-void

    .line 1546
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1547
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1548
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1549
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 1552
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getItemMoveAnimator(II)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    .line 1553
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 1554
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1555
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1556
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method public callItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "itemSelectionState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 1834
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    if-eqz v0, :cond_0

    .line 1835
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;->onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 1837
    :cond_0
    return-void
.end method

.method public cancelLoadingAnimation(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 1802
    move v0, p1

    .line 1803
    .local v0, "userpos":I
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->firstEntryBlank:Z

    if-eqz v2, :cond_0

    .line 1804
    add-int/lit8 v0, v0, 0x1

    .line 1806
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1807
    .local v1, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    if-ne v2, v3, :cond_1

    .line 1808
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    .line 1810
    :cond_1
    return-void
.end method

.method public clearAllAnimations()V
    .locals 11

    .prologue
    .line 1841
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->getChildCount()I

    move-result v1

    .line 1842
    .local v1, "count":I
    const/4 v0, 0x0

    .line 1843
    .local v0, "cleared":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 1845
    :try_start_0
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v8, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1846
    .local v2, "holder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v2, :cond_0

    .line 1847
    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1848
    add-int/lit8 v0, v0, 0x1

    .line 1843
    .end local v2    # "holder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1850
    :catch_0
    move-exception v6

    .line 1851
    .local v6, "t":Ljava/lang/Throwable;
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1854
    .end local v6    # "t":Ljava/lang/Throwable;
    :cond_1
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "clearAllAnimations current:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1857
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v5

    .line 1858
    .local v5, "pool":Landroid/support/v7/widget/RecyclerView$RecycledViewPool;
    if-eqz v5, :cond_3

    .line 1859
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->copyList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 1860
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    invoke-direct {p0, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearViewAnimation(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V

    .line 1861
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    invoke-direct {p0, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearViewAnimation(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V

    .line 1862
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    invoke-direct {p0, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearViewAnimation(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V

    .line 1863
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    invoke-direct {p0, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearViewAnimation(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V

    .line 1864
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    invoke-direct {p0, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearViewAnimation(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V

    .line 1865
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    invoke-direct {p0, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearViewAnimation(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V

    .line 1866
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SCROLL_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    invoke-direct {p0, v5, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->cleanupView(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;I)V

    .line 1868
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->copyList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1869
    .local v4, "n":I
    if-lez v4, :cond_3

    .line 1870
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->copyList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 1871
    .local v7, "vh":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {v5, v7}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->putRecycledView(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    goto :goto_2

    .line 1873
    .end local v7    # "vh":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_2
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "clearAllAnimations pool:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1874
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->copyList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 1877
    .end local v4    # "n":I
    :cond_3
    return-void
.end method

.method public copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
    .locals 6
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "title"    # Landroid/widget/TextView;
    .param p3, "subTitle"    # Landroid/widget/TextView;
    .param p4, "subTitle2"    # Landroid/widget/TextView;
    .param p5, "setImage"    # Z

    .prologue
    .line 1730
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1731
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 1732
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V

    .line 1734
    :cond_0
    return-void
.end method

.method public down()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 1262
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->clear()V

    .line 1264
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1265
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "down locked"

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1266
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    .line 1340
    :goto_0
    return-object v0

    .line 1269
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    if-eqz v0, :cond_1

    .line 1270
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1271
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto :goto_0

    .line 1274
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-eqz v0, :cond_7

    .line 1276
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v0, v0, 0x1

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollItemIndex:I

    if-ne v0, v4, :cond_2

    .line 1277
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1278
    .local v1, "oldPos":I
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1279
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1280
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    .line 1281
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    .line 1282
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v0, v4, v10}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->smoothScrollToPosition(II)V

    .line 1283
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(IIZZZ)V

    .line 1284
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    .line 1285
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1286
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto :goto_0

    .line 1287
    .end local v1    # "oldPos":I
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollPosInScrollItem()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1289
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->canScrollDown()I

    move-result v8

    .local v8, "scrollDistance":I
    if-eq v8, v10, :cond_6

    .line 1290
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1291
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    if-eqz v0, :cond_4

    .line 1292
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v0, v4

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    .line 1293
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    cmpl-float v0, v0, v9

    if-lez v0, :cond_3

    .line 1294
    iput v9, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    .line 1299
    :cond_3
    :goto_1
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->scrollDistance:I

    int-to-float v0, v0

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    mul-float v6, v0, v4

    .line 1300
    .local v6, "distance":F
    int-to-float v0, v8

    cmpl-float v0, v0, v6

    if-lez v0, :cond_5

    move v7, v6

    .line 1301
    .local v7, "scrollBy":F
    :goto_2
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    .line 1302
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    float-to-int v4, v7

    invoke-virtual {v0, v5, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->smoothScrollBy(II)V

    .line 1303
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1304
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0

    .line 1297
    .end local v6    # "distance":F
    .end local v7    # "scrollBy":F
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    goto :goto_1

    .line 1300
    .restart local v6    # "distance":F
    :cond_5
    int-to-float v7, v8

    goto :goto_2

    .line 1307
    .end local v6    # "distance":F
    :cond_6
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isBottom()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1308
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1309
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v6, v0

    .line 1310
    .restart local v6    # "distance":F
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1311
    .restart local v1    # "oldPos":I
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v2, v0, 0x1

    .line 1312
    .local v2, "newPos":I
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    .line 1313
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    .line 1314
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1315
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    float-to-int v4, v6

    invoke-virtual {v0, v5, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->smoothScrollBy(II)V

    move-object v0, p0

    move v4, v3

    move v5, v3

    .line 1316
    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(IIZZZ)V

    .line 1317
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    .line 1318
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1319
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0

    .line 1325
    .end local v1    # "oldPos":I
    .end local v2    # "newPos":I
    .end local v6    # "distance":F
    .end local v8    # "scrollDistance":I
    :cond_7
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isBottom()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1326
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot go down:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1327
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0

    .line 1330
    :cond_8
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1332
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isOverrideKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1333
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1334
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    .line 1335
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0

    .line 1338
    :cond_9
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    .line 1339
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v0, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->down(II)V

    .line 1340
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0
.end method

.method public getCurrentModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2

    .prologue
    .line 1420
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 1416
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getCurrentViewHolder()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .locals 1

    .prologue
    .line 1424
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getViewHolder(I)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public getItemSelectionState()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    .locals 1

    .prologue
    .line 1830
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->itemSelectionState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    return-object v0
.end method

.method public getRawPosition()I
    .locals 1

    .prologue
    .line 1447
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    return v0
.end method

.method public getViewHolder(I)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .locals 5
    .param p1, "pos"    # I

    .prologue
    .line 1428
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 1429
    .local v0, "currentModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_0

    .line 1430
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getViewHolder: invalid pos:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1431
    const/4 v1, 0x0

    .line 1435
    :goto_0
    return-object v1

    .line 1434
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1435
    .local v1, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    goto :goto_0
.end method

.method public hasScrollItem()Z
    .locals 1

    .prologue
    .line 1826
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    return v0
.end method

.method public isBottom()Z
    .locals 2

    .prologue
    .line 1408
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->modelList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    if-ne v0, v1, :cond_0

    .line 1409
    const/4 v0, 0x1

    .line 1411
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFirstEntryBlank()Z
    .locals 1

    .prologue
    .line 1822
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->firstEntryBlank:Z

    return v0
.end method

.method public isTop()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1400
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    if-ne v1, v0, :cond_0

    .line 1403
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lock()V
    .locals 1

    .prologue
    .line 1378
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lockList:Z

    .line 1379
    return-void
.end method

.method public performSelectAction(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 1708
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 1709
    return-void
.end method

.method public refreshData(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 1740
    const/4 v0, 0x0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    .line 1741
    return-void
.end method

.method public refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "newModel"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    .line 1744
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    .line 1745
    return-void
.end method

.method public refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "newModel"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p3, "dontStartFluctuator"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1749
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->firstEntryBlank:Z

    if-eqz v1, :cond_0

    .line 1750
    add-int/lit8 p1, p1, 0x1

    .line 1752
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 1753
    .local v0, "currentModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_1

    .line 1754
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshData: invalid model pos:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1766
    :goto_0
    return-void

    .line 1758
    :cond_1
    if-nez p2, :cond_2

    .line 1759
    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    .line 1760
    iput-boolean p3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->dontStartFluctuator:Z

    .line 1765
    :goto_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->notifyItemChanged(I)V

    goto :goto_0

    .line 1762
    :cond_2
    iput-boolean v2, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    .line 1763
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v1, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->updateModel(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_1
.end method

.method public refreshData(I[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 7
    .param p1, "pos"    # I
    .param p2, "newModels"    # [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    .line 1770
    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->firstEntryBlank:Z

    if-eqz v4, :cond_0

    .line 1771
    add-int/lit8 p1, p1, 0x1

    .line 1773
    :cond_0
    const/4 v2, 0x0

    .line 1774
    .local v2, "itemsUpdated":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-ge v1, v4, :cond_2

    .line 1775
    add-int v3, p1, v1

    .line 1776
    .local v3, "newPos":I
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v4, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 1777
    .local v0, "currentModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_1

    .line 1778
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "refreshData(s): invalid model pos:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1774
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1781
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 1782
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    aget-object v5, p2, v1

    invoke-virtual {v4, v3, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->updateModel(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_1

    .line 1784
    .end local v0    # "currentModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v3    # "newPos":I
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->setInitialState(Z)V

    .line 1785
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v4, p1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->notifyItemRangeChanged(II)V

    .line 1786
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "refreshData(s) pos="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " len="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1787
    return-void
.end method

.method public scrollToPosition(I)V
    .locals 6
    .param p1, "pos"    # I

    .prologue
    const/4 v5, 0x0

    .line 1105
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 1106
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_0

    .line 1131
    :goto_0
    return-void

    .line 1110
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isLocked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1111
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scrollToPos list locked:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1114
    :cond_1
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    if-eqz v2, :cond_2

    .line 1116
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scrollToPos wait for scroll idle:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1117
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollPendingPos:I

    goto :goto_0

    .line 1120
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scrollToPos:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1121
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1122
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v1, v2, -0x1

    .line 1123
    .local v1, "scrollPos":I
    if-gez v1, :cond_3

    .line 1124
    const/4 v1, 0x0

    .line 1126
    :cond_3
    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    mul-int/2addr v2, v1

    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    .line 1127
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->setInitialState(Z)V

    .line 1128
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-virtual {v2, v1, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->scrollToPositionWithOffset(II)V

    .line 1129
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scrollToPos scroll to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " scrollPos="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " middle="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " raw="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1130
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v2, v3, v5}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V

    goto/16 :goto_0
.end method

.method public select()V
    .locals 1

    .prologue
    .line 1352
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->select(Z)V

    .line 1353
    return-void
.end method

.method public setBindCallbacks(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 953
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->bindCallbacks:Z

    .line 954
    return-void
.end method

.method public setScrollIdleEvent(Z)V
    .locals 0
    .param p1, "send"    # Z

    .prologue
    .line 957
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sendScrollIdleEvent:Z

    .line 958
    return-void
.end method

.method public setViewHolderState(ILcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p3, "animationType"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p4, "animationDuration"    # I

    .prologue
    .line 1439
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getViewHolder(I)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    move-result-object v0

    .line 1440
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v1

    if-eq v1, p2, :cond_0

    .line 1441
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setViewHolderState:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1442
    invoke-virtual {v0, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 1444
    :cond_0
    return-void
.end method

.method public targetFound(I)V
    .locals 13
    .param p1, "dy"    # I

    .prologue
    const/16 v12, 0xe6

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1569
    iget-boolean v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-eqz v8, :cond_1

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollPosInScrollItem()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1621
    :cond_0
    :goto_0
    return-void

    .line 1572
    :cond_1
    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I

    add-int v3, v8, p1

    .line 1573
    .local v3, "target":I
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getPositionFromScrollY(I)I

    move-result v1

    .line 1574
    .local v1, "middlePos":I
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v5

    .line 1576
    .local v5, "verbose":Z
    if-nez p1, :cond_2

    .line 1577
    iget-boolean v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->waitForTarget:Z

    if-eqz v8, :cond_2

    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->targetPos:I

    if-ne v1, v8, :cond_0

    .line 1583
    :cond_2
    iput-boolean v10, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->waitForTarget:Z

    .line 1584
    iput-boolean v10, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->targetFound:Z

    .line 1585
    const/4 v8, -0x1

    iput v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->targetPos:I

    .line 1587
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v8, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1588
    .local v7, "viewHolderSel":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-nez v7, :cond_3

    .line 1590
    iput-boolean v11, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->waitForTarget:Z

    .line 1591
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->targetPos:I

    goto :goto_0

    .line 1595
    :cond_3
    iput-boolean v11, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->targetFound:Z

    .line 1596
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    .line 1598
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->selectedList:Ljava/util/HashSet;

    .line 1599
    .local v4, "temp":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->selectedList:Ljava/util/HashSet;

    .line 1600
    if-eqz v5, :cond_4

    .line 1601
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "targetFound selected list size = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1603
    :cond_4
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v8

    if-lez v8, :cond_6

    .line 1604
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1605
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_5
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1606
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1607
    .local v2, "pp":I
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v8, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1608
    .local v6, "viewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v6, :cond_5

    .line 1609
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v8

    sget-object v9, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v8, v9, :cond_5

    .line 1610
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v9, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v6, v8, v9, v12}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    goto :goto_1

    .line 1616
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v2    # "pp":I
    .end local v6    # "viewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_6
    if-eqz v7, :cond_0

    .line 1617
    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v8

    sget-object v9, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v8, v9, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isCloseMenuVisible()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1618
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v9, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v7, v8, v9, v12}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    goto/16 :goto_0
.end method

.method public unlock()V
    .locals 1

    .prologue
    .line 1382
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock(Z)V

    .line 1383
    return-void
.end method

.method public unlock(Z)V
    .locals 3
    .param p1, "startFluctuator"    # Z

    .prologue
    .line 1386
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lockList:Z

    .line 1387
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 1388
    .local v0, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v0, :cond_0

    .line 1389
    if-eqz p1, :cond_1

    .line 1391
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->startFluctuator()V

    .line 1397
    :cond_0
    :goto_0
    return-void

    .line 1394
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    goto :goto_0
.end method

.method public up()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1143
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->clear()V

    .line 1145
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isLocked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1146
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "up locked"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1147
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    .line 1217
    :goto_0
    return-object v3

    .line 1150
    :cond_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z

    if-eqz v3, :cond_1

    .line 1151
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1152
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto :goto_0

    .line 1155
    :cond_1
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->reCalcScrollPos:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    if-eqz v3, :cond_2

    .line 1156
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1157
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto :goto_0

    .line 1160
    :cond_2
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->hasScrollableElement:Z

    if-eqz v3, :cond_6

    .line 1162
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollPosInScrollItem()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1165
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->canScrollUp()I

    move-result v2

    .local v2, "scrollDistance":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_6

    .line 1166
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    if-eqz v3, :cond_4

    .line 1167
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    .line 1168
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    cmpl-float v3, v3, v7

    if-lez v3, :cond_3

    .line 1169
    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    .line 1174
    :cond_3
    :goto_1
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->scrollDistance:I

    int-to-float v3, v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    mul-float v0, v3, v4

    .line 1175
    .local v0, "distance":F
    int-to-float v3, v2

    cmpl-float v3, v3, v0

    if-lez v3, :cond_5

    move v1, v0

    .line 1176
    .local v1, "scrollBy":F
    :goto_2
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    .line 1177
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    neg-float v4, v1

    float-to-int v4, v4

    invoke-virtual {v3, v6, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->smoothScrollBy(II)V

    .line 1178
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v3}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1179
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1180
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto :goto_0

    .line 1172
    .end local v0    # "distance":F
    .end local v1    # "scrollBy":F
    :cond_4
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentScrollBy:F

    goto :goto_1

    .line 1175
    .restart local v0    # "distance":F
    :cond_5
    int-to-float v1, v2

    goto :goto_2

    .line 1185
    .end local v0    # "distance":F
    .end local v2    # "scrollDistance":I
    :cond_6
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isTop()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1187
    sget-object v3, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isOverrideKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1188
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v3}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1189
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1190
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v6, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    .line 1191
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0

    .line 1194
    :cond_7
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollBy:Z

    if-eqz v3, :cond_8

    .line 1195
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollByUpReceived:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollByUpReceived:I

    .line 1196
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1199
    :cond_8
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    if-eqz v3, :cond_9

    .line 1200
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1203
    :cond_9
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot go up:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isScrolling:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1204
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0

    .line 1207
    :cond_a
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    .line 1209
    sget-object v3, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isOverrideKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1210
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v3}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 1211
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v6, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    .line 1212
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0

    .line 1215
    :cond_b
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    iput-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    .line 1216
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v3, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->up(II)V

    .line 1217
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->keyHandlerState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    goto/16 :goto_0
.end method

.method public updateView(Ljava/util/List;IZ)V
    .locals 6
    .param p2, "initialSelection"    # I
    .param p3, "firstEntryBlank"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 963
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->updateView(Ljava/util/List;IZZI)V

    .line 964
    return-void
.end method

.method public updateViewWithScrollableContent(Ljava/util/List;IZ)V
    .locals 9
    .param p2, "initialSelection"    # I
    .param p3, "firstEntryBlank"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    const/4 v4, 0x1

    .line 969
    const/4 v8, 0x0

    .line 970
    .local v8, "scrollItemCount":I
    const/4 v5, -0x1

    .line 971
    .local v5, "index":I
    const/4 v6, 0x0

    .line 972
    .local v6, "counter":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 973
    .local v7, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v1, v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SCROLL_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    if-ne v1, v2, :cond_0

    .line 974
    add-int/lit8 v8, v8, 0x1

    .line 975
    move v5, v6

    .line 977
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 978
    goto :goto_0

    .line 979
    .end local v7    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_2

    if-eqz v8, :cond_2

    if-le v8, v4, :cond_3

    .line 980
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid scroll item model size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    .line 982
    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->updateView(Ljava/util/List;IZZI)V

    .line 983
    return-void
.end method
