.class public Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;
.source "IconBkColorViewHolder.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 0
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 77
    return-void
.end method

.method public static buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 9
    .param p0, "id"    # I
    .param p1, "icon"    # I
    .param p2, "iconSelectedColor"    # I
    .param p3, "iconDeselectedColor"    # I
    .param p4, "iconFluctuatorColor"    # I
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 30
    const/4 v7, 0x0

    sget-object v8, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    move v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v8}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 9
    .param p0, "id"    # I
    .param p1, "icon"    # I
    .param p2, "iconSelectedColor"    # I
    .param p3, "iconDeselectedColor"    # I
    .param p4, "iconFluctuatorColor"    # I
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "subTitle"    # Ljava/lang/String;
    .param p7, "subTitle2"    # Ljava/lang/String;

    .prologue
    .line 41
    sget-object v8, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    move v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p0, "id"    # I
    .param p1, "icon"    # I
    .param p2, "iconSelectedColor"    # I
    .param p3, "iconDeselectedColor"    # I
    .param p4, "iconFluctuatorColor"    # I
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "subTitle"    # Ljava/lang/String;
    .param p7, "subTitle2"    # Ljava/lang/String;
    .param p8, "iconShape"    # Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .prologue
    .line 53
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 54
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 57
    .restart local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 58
    iput p0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 59
    iput p1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 60
    iput p2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 61
    iput p3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    .line 62
    iput p4, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 63
    iput-object p5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 64
    iput-object p6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 65
    iput-object p7, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    .line 66
    iput-object p8, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 67
    return-object v0
.end method

.method public static buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;
    .locals 3
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 71
    const v1, 0x7f030087

    const v2, 0x7f03000e

    invoke-static {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->getLayout(Landroid/view/ViewGroup;II)Landroid/view/ViewGroup;

    move-result-object v0

    .line 72
    .local v0, "layout":Landroid/view/ViewGroup;
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;

    invoke-direct {v1, v0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v1
.end method

.method private setIcon(IIIZZ)V
    .locals 7
    .param p1, "icon"    # I
    .param p2, "iconSelectedColor"    # I
    .param p3, "iconDeselectedColor"    # I
    .param p4, "updateImage"    # Z
    .param p5, "updateSmallImage"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x3f547ae1    # 0.83f

    .line 136
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 137
    .local v0, "big":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    if-eqz p4, :cond_0

    .line 138
    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setDraw(Z)V

    .line 139
    invoke-virtual {v0, p1, p2, v6, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 144
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 145
    .local v1, "small":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    if-eqz p5, :cond_1

    .line 146
    invoke-virtual {v1, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setDraw(Z)V

    .line 147
    invoke-virtual {v1, p1, p3, v6, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 151
    :goto_1
    return-void

    .line 141
    .end local v1    # "small":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    :cond_0
    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setDraw(Z)V

    goto :goto_0

    .line 149
    .restart local v1    # "small":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    :cond_1
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setDraw(Z)V

    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 6
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 131
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 132
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    iget-boolean v4, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    iget-boolean v5, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->setIcon(IIIZZ)V

    .line 133
    return-void
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 2
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 124
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIconShape(Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V

    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIconShape(Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V

    .line 127
    return-void
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 9
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 86
    invoke-super {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V

    .line 88
    const/4 v0, 0x0

    .line 89
    .local v0, "iconScaleFactor":F
    const/4 v1, 0x0

    .line 91
    .local v1, "mode":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 103
    :goto_0
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 120
    :cond_0
    :goto_1
    return-void

    .line 93
    :pswitch_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 94
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .line 95
    goto :goto_0

    .line 98
    :pswitch_1
    const v0, 0x3f19999a    # 0.6f

    .line 99
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_0

    .line 106
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->imageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->imageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setScaleY(F)V

    .line 108
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    goto :goto_1

    .line 112
    :pswitch_3
    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v5, v8, [F

    aput v0, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 113
    .local v2, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v5, v8, [F

    aput v0, v5, v7

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 114
    .local v3, "p2":Landroid/animation/PropertyValuesHolder;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->imageContainer:Landroid/view/ViewGroup;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v6, v7

    aput-object v3, v6, v8

    invoke-static {v5, v6}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 115
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    move-result-object v4

    if-eq v4, v1, :cond_0

    .line 116
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 103
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
