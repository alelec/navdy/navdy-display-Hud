.class public Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;
.source "IconOneViewHolder.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 0
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 54
    return-void
.end method

.method public static buildModel(IIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 6
    .param p0, "id"    # I
    .param p1, "icon"    # I
    .param p2, "iconFluctuatorColor"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 27
    const/4 v5, 0x0

    move v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->buildModel(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static buildModel(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p0, "id"    # I
    .param p1, "icon"    # I
    .param p2, "iconFluctuatorColor"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subTitle"    # Ljava/lang/String;
    .param p5, "subTitle2"    # Ljava/lang/String;

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 37
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 38
    iput p0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 39
    iput p1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 40
    iput p2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 41
    iput-object p3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 42
    iput-object p4, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 43
    iput-object p5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    .line 44
    return-object v0
.end method

.method public static buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;
    .locals 3
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 48
    const v1, 0x7f030087

    const v2, 0x7f030010

    invoke-static {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->getLayout(Landroid/view/ViewGroup;II)Landroid/view/ViewGroup;

    move-result-object v0

    .line 49
    .local v0, "layout":Landroid/view/ViewGroup;
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;

    invoke-direct {v1, v0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v1
.end method

.method private setIcon(IZZ)V
    .locals 7
    .param p1, "icon"    # I
    .param p2, "updateImage"    # Z
    .param p3, "updateSmallImage"    # Z

    .prologue
    .line 111
    const/4 v2, 0x0

    .line 112
    .local v2, "initials":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->extras:Ljava/util/HashMap;

    if-eqz v5, :cond_0

    .line 113
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->extras:Ljava/util/HashMap;

    const-string v6, "INITIAL"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "initials":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 115
    .restart local v2    # "initials":Ljava/lang/String;
    :cond_0
    if-eqz p2, :cond_1

    .line 117
    if-eqz v2, :cond_3

    .line 118
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 122
    .local v1, "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :goto_0
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 123
    .local v0, "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    const/4 v5, 0x0

    invoke-virtual {v0, p1, v5, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 126
    .end local v0    # "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .end local v1    # "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :cond_1
    if-eqz p3, :cond_2

    .line 128
    if-eqz v2, :cond_4

    .line 129
    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->TINY:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 133
    .local v4, "smallStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :goto_1
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 134
    .local v3, "small":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setAlpha(F)V

    .line 135
    invoke-virtual {v3, p1, v2, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 137
    .end local v3    # "small":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .end local v4    # "smallStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :cond_2
    return-void

    .line 120
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .restart local v1    # "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    goto :goto_0

    .line 131
    .end local v1    # "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :cond_4
    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .restart local v4    # "smallStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 3
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 106
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 107
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    iget-boolean v1, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    iget-boolean v2, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->setIcon(IZZ)V

    .line 108
    return-void
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 11
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 63
    invoke-super {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V

    .line 65
    const/4 v1, 0x0

    .line 66
    .local v1, "iconScaleFactor":F
    const/4 v0, 0x0

    .line 67
    .local v0, "iconAlphaFactor":F
    const/4 v2, 0x0

    .line 69
    .local v2, "mode":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;
    sget-object v6, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 83
    :goto_0
    sget-object v6, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 102
    :cond_0
    :goto_1
    return-void

    .line 71
    :pswitch_0
    const/high16 v1, 0x3f800000    # 1.0f

    .line 72
    const/high16 v0, 0x3f800000    # 1.0f

    .line 73
    sget-object v2, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .line 74
    goto :goto_0

    .line 77
    :pswitch_1
    const v1, 0x3f19999a    # 0.6f

    .line 78
    const/high16 v0, 0x3f000000    # 0.5f

    .line 79
    sget-object v2, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_0

    .line 86
    :pswitch_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->imageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 87
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->imageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->setScaleY(F)V

    .line 88
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->imageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 89
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v6, v2}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    goto :goto_1

    .line 93
    :pswitch_3
    sget-object v6, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v7, v10, [F

    aput v1, v7, v9

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 94
    .local v3, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v6, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v7, v10, [F

    aput v1, v7, v9

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 95
    .local v4, "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v7, v10, [F

    aput v0, v7, v9

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 96
    .local v5, "p3":Landroid/animation/PropertyValuesHolder;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->imageContainer:Landroid/view/ViewGroup;

    const/4 v8, 0x3

    new-array v8, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v3, v8, v9

    aput-object v4, v8, v10

    const/4 v9, 0x2

    aput-object v5, v8, v9

    invoke-static {v7, v8}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 97
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    move-result-object v6

    if-eq v6, v2, :cond_0

    .line 98
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 83
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
