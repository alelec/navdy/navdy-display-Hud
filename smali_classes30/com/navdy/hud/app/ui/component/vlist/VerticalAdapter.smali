.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "VerticalAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private highlightIndex:I

.field private holderToPosMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private initialState:Z

.field private modelState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

.field private posToHolderMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private viewHolderCache:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V
    .locals 2
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    .line 38
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->modelState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->highlightIndex:I

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->viewHolderCache:Ljava/util/HashSet;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->holderToPosMap:Ljava/util/HashMap;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->posToHolderMap:Ljava/util/HashMap;

    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    .line 50
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .line 51
    return-void
.end method

.method private clearPrevPos(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)V
    .locals 4
    .param p1, "holder"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .prologue
    .line 255
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->holderToPosMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 256
    .local v0, "prevPos":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 257
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearPrevPos removed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 258
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->posToHolderMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    :cond_0
    return-void
.end method


# virtual methods
.method public getHolderForPos(I)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 263
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->posToHolderMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 202
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 207
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 208
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v1

    return v1
.end method

.method public getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 212
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 213
    :cond_0
    const/4 v0, 0x0

    .line 215
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->onBindViewHolder(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;I)V
    .locals 11
    .param p1, "holder"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 99
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 100
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onBindViewHolder: {"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "}"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 102
    :cond_0
    const/4 v2, 0x0

    .line 103
    .local v2, "highlightItem":Z
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->clearPrevPos(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)V

    .line 105
    iget v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->highlightIndex:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_2

    .line 106
    iget v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->highlightIndex:I

    if-ne v7, p2, :cond_1

    .line 107
    const/4 v2, 0x1

    .line 109
    :cond_1
    const/4 v7, -0x1

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->highlightIndex:I

    .line 112
    :cond_2
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->viewHolderCache:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 113
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->holderToPosMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, p1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->posToHolderMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v7, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 118
    .local v3, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getPos()I

    move-result v7

    if-ne p2, v7, :cond_4

    iget-boolean v7, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    if-nez v7, :cond_4

    .line 119
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "onBindViewHolder: already bind unselect it"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 120
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    const/4 v9, 0x0

    invoke-virtual {p1, v7, v8, v9}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 193
    :goto_0
    :pswitch_0
    return-void

    .line 124
    :cond_4
    const/4 v0, 0x0

    .line 126
    .local v0, "callBind":Z
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    move-result-object v4

    .line 127
    .local v4, "modelType":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType:[I

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 140
    :goto_1
    :pswitch_1
    iget-boolean v7, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSizeCheckDone:Z

    if-nez v7, :cond_5

    .line 142
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->allowsTwoLineTitles()Z

    move-result v7

    invoke-static {v3, v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setFontSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    .line 145
    :cond_5
    iget-boolean v5, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    .line 146
    .local v5, "rebind":Z
    iget-boolean v1, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->dontStartFluctuator:Z

    .line 147
    .local v1, "dontStartFluctuator":Z
    const/4 v7, 0x0

    iput-boolean v7, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    .line 148
    const/4 v7, 0x0

    iput-boolean v7, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->dontStartFluctuator:Z

    .line 150
    if-nez v5, :cond_6

    .line 153
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    .line 157
    :cond_6
    iget-object v7, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    invoke-virtual {p1, v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setExtras(Ljava/util/HashMap;)V

    .line 158
    invoke-virtual {p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setPos(I)V

    .line 160
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->modelState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->reset()V

    .line 162
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->modelState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    invoke-virtual {p1, v3, v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 164
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-boolean v7, v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->bindCallbacks:Z

    if-eqz v7, :cond_7

    if-eqz v0, :cond_7

    .line 167
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-boolean v7, v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->firstEntryBlank:Z

    if-eqz v7, :cond_8

    .line 168
    add-int/lit8 v6, p2, -0x1

    .line 172
    .local v6, "userPos":I
    :goto_2
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v7, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v9, p1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->layout:Landroid/view/ViewGroup;

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->modelState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    invoke-interface {v8, v7, v9, v6, v10}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;->onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 175
    .end local v6    # "userPos":I
    :cond_7
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->modelState:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    invoke-virtual {p1, v3, v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 177
    iget-boolean v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->initialState:Z

    if-nez v7, :cond_a

    .line 178
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v7

    if-ne v7, p2, :cond_9

    .line 179
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->initialState:Z

    .line 180
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->INIT:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    const/16 v9, 0x64

    invoke-virtual {p1, v7, v8, v9}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 181
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "initial state: position:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 135
    .end local v1    # "dontStartFluctuator":Z
    .end local v5    # "rebind":Z
    :pswitch_2
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 170
    .restart local v1    # "dontStartFluctuator":Z
    .restart local v5    # "rebind":Z
    :cond_8
    move v6, p2

    .restart local v6    # "userPos":I
    goto :goto_2

    .line 183
    .end local v6    # "userPos":I
    :cond_9
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->INIT:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    const/16 v9, 0x64

    invoke-virtual {p1, v7, v8, v9}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    goto/16 :goto_0

    .line 186
    :cond_a
    if-nez v2, :cond_b

    if-eqz v5, :cond_d

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v7

    if-ne v7, p2, :cond_d

    .line 188
    :cond_b
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v9, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    const/4 v10, 0x0

    if-nez v1, :cond_c

    const/4 v7, 0x1

    :goto_3
    invoke-virtual {p1, v8, v9, v10, v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V

    goto/16 :goto_0

    :cond_c
    const/4 v7, 0x0

    goto :goto_3

    .line 190
    :cond_d
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    const/4 v9, 0x0

    invoke-virtual {p1, v7, v8, v9}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    goto/16 :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 55
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->values()[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    move-result-object v1

    aget-object v0, v1, p2

    .line 56
    .local v0, "modelType":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateViewHolder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 59
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 93
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "implement the model:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 61
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;

    move-result-object v1

    .line 90
    :goto_0
    return-object v1

    .line 64
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleViewHolder;

    move-result-object v1

    goto :goto_0

    .line 67
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleSubtitleViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleSubtitleViewHolder;

    move-result-object v1

    goto :goto_0

    .line 70
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;

    move-result-object v1

    goto :goto_0

    .line 73
    :pswitch_4
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;

    move-result-object v1

    goto :goto_0

    .line 76
    :pswitch_5
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOneViewHolder;

    move-result-object v1

    goto :goto_0

    .line 79
    :pswitch_6
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    move-result-object v1

    goto :goto_0

    .line 82
    :pswitch_7
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    move-result-object v1

    goto :goto_0

    .line 85
    :pswitch_8
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ScrollableViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/ScrollableViewHolder;

    move-result-object v1

    goto :goto_0

    .line 88
    :pswitch_9
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    move-result-object v1

    goto :goto_0

    .line 90
    :pswitch_a
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->Companion:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->handler:Landroid/os/Handler;

    invoke-virtual {v1, p1, v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;->buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    move-result-object v1

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic onFailedToRecycleView(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->onFailedToRecycleView(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)Z

    move-result v0

    return v0
.end method

.method public onFailedToRecycleView(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)Z
    .locals 1
    .param p1, "holder"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .prologue
    .line 236
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    .line 237
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->clearPrevPos(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)V

    .line 238
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onFailedToRecycleView(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->onViewRecycled(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)V

    return-void
.end method

.method public onViewRecycled(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)V
    .locals 0
    .param p1, "holder"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .prologue
    .line 230
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    .line 231
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->clearPrevPos(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;)V

    .line 232
    return-void
.end method

.method public setHighlightIndex(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 267
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->highlightIndex:I

    .line 268
    return-void
.end method

.method public setInitialState(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 242
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->initialState:Z

    .line 243
    return-void
.end method

.method public setViewHolderCacheIndex([I)V
    .locals 3
    .param p1, "index"    # [I

    .prologue
    .line 246
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->viewHolderCache:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 247
    if-eqz p1, :cond_0

    .line 248
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->viewHolderCache:Ljava/util/HashSet;

    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 252
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public updateModel(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    .line 220
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updated model:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
