.class public final enum Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
.super Ljava/lang/Enum;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FontSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_16_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_16_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_16_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_18_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_18_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_18_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_22:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_22_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_22_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_22_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_22_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_22_2_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_26_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field public static final enum FONT_SIZE_26_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 133
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_26"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 134
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_22"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 135
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_22_2"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 136
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_18"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 137
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_18_2"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 138
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_16"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 139
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_16_2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 140
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_26_18"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 141
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_22_18"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 142
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_22_2_18"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 143
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_18_16"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 144
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_18_2_16"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 145
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_16_16"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 146
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_16_2_16"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 147
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_26_16"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 148
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_22_16"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 149
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    const-string v1, "FONT_SIZE_22_2_16"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 132
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_18:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_18_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_16_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_26_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->FONT_SIZE_22_2_16:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->$VALUES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 132
    const-class v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->$VALUES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    return-object v0
.end method
