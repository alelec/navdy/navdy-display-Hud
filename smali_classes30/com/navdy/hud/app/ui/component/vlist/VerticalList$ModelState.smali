.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;
.super Ljava/lang/Object;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ModelState"
.end annotation


# instance fields
.field public updateImage:Z

.field public updateSmallImage:Z

.field public updateSubTitle:Z

.field public updateSubTitle2:Z

.field public updateTitle:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->reset()V

    .line 524
    return-void
.end method


# virtual methods
.method reset()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 527
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    .line 528
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    .line 529
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateTitle:Z

    .line 530
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSubTitle:Z

    .line 531
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSubTitle2:Z

    .line 532
    return-void
.end method
