.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
.super Ljava/lang/Object;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Model"
.end annotation


# static fields
.field public static final INITIALS:Ljava/lang/String; = "INITIAL"

.field public static final SUBTITLE_2_COLOR:Ljava/lang/String; = "SUBTITLE_2_COLOR"

.field public static final SUBTITLE_COLOR:Ljava/lang/String; = "SUBTITLE_COLOR"


# instance fields
.field public currentIconSelection:I

.field dontStartFluctuator:Z

.field public extras:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

.field public fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

.field fontSizeCheckDone:Z

.field public icon:I

.field public iconDeselectedColor:I

.field public iconDeselectedColors:[I

.field public iconFluctuatorColor:I

.field public iconFluctuatorColors:[I

.field public iconIds:[I

.field public iconList:[I

.field public iconSelectedColor:I

.field public iconSelectedColors:[I

.field public iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

.field public iconSize:I

.field public iconSmall:I

.field public id:I

.field public isEnabled:Z

.field public isOn:Z

.field needsRebind:Z

.field public noImageScaleAnimation:Z

.field public noTextAnimation:Z

.field public scrollItemLayoutId:I

.field public state:Ljava/lang/Object;

.field public subTitle:Ljava/lang/String;

.field public subTitle2:Ljava/lang/String;

.field public subTitle2Formatted:Z

.field public subTitleFormatted:Z

.field public subTitle_2Lines:Z

.field public supportsToolTip:Z

.field public title:Ljava/lang/String;

.field public type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 586
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 587
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 588
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    .line 589
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSmall:I

    .line 590
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 599
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->scrollItemLayoutId:I

    .line 638
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSize:I

    .line 546
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 2
    .param p1, "m"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    const/4 v1, -0x1

    .line 548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 586
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 587
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 588
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    .line 589
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSmall:I

    .line 590
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 599
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->scrollItemLayoutId:I

    .line 638
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSize:I

    .line 549
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 550
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 551
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 552
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 553
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    .line 554
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSmall:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSmall:I

    .line 555
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 556
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 557
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 558
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    .line 559
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    .line 560
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitleFormatted:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitleFormatted:Z

    .line 561
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2Formatted:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2Formatted:Z

    .line 562
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->scrollItemLayoutId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->scrollItemLayoutId:I

    .line 563
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    .line 564
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    .line 565
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColors:[I

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColors:[I

    .line 566
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColors:[I

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColors:[I

    .line 567
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColors:[I

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColors:[I

    .line 568
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->currentIconSelection:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->currentIconSelection:I

    .line 569
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->supportsToolTip:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->supportsToolTip:Z

    .line 570
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    .line 571
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 572
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 573
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    .line 574
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->dontStartFluctuator:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->dontStartFluctuator:Z

    .line 575
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSizeCheckDone:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSizeCheckDone:Z

    .line 576
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .line 577
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 578
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noTextAnimation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noTextAnimation:Z

    .line 579
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noImageScaleAnimation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noImageScaleAnimation:Z

    .line 580
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSize:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSize:I

    .line 581
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 641
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 642
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 643
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 644
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    .line 645
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSmall:I

    .line 646
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 647
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 648
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 649
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    .line 650
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    .line 651
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitleFormatted:Z

    .line 652
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2Formatted:Z

    .line 653
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->scrollItemLayoutId:I

    .line 654
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    .line 655
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    .line 656
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->currentIconSelection:I

    .line 657
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->extras:Ljava/util/HashMap;

    .line 658
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 659
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 660
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->needsRebind:Z

    .line 661
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->dontStartFluctuator:Z

    .line 662
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSizeCheckDone:Z

    .line 663
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    .line 664
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontSize:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontSize;

    .line 665
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noTextAnimation:Z

    .line 666
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noImageScaleAnimation:Z

    .line 667
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSize:I

    .line 668
    return-void
.end method
