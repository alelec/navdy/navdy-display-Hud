.class public final Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "SwitchViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1",
        "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;",
        "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/os/Handler;)V",
        "onAnimationEnd",
        "",
        "animation",
        "Landroid/animation/Animator;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic $handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/os/Handler;)V
    .locals 0
    .param p1, "$outer"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .param p2, "$captured_local_variable$1"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->$handler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "animation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    const/4 v0, 0x0

    check-cast v0, Landroid/animation/AnimatorSet;

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->$handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->access$getFluctuatorRunnable$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->$handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->access$getFluctuatorRunnable$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Ljava/lang/Runnable;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$fluctuatorStartListener$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->getHALO_DELAY_START_DURATION()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 94
    return-void
.end method
