.class Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 815
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method

.method private moveItemToSelectedState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;I)V
    .locals 4
    .param p1, "vh"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .param p2, "pos"    # I

    .prologue
    const/16 v3, 0xe6

    .line 874
    if-eqz p1, :cond_1

    .line 875
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v0

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v0, v1, :cond_3

    .line 876
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I
    invoke-static {v0, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1402(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I

    .line 877
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # invokes: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isCloseMenuVisible()Z
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1800(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 878
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollStateChanged idle-select pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 879
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {p1, v0, v1, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 891
    :cond_1
    :goto_0
    return-void

    .line 881
    :cond_2
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollStateChanged idle-select-un pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 882
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {p1, v0, v1, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    goto :goto_0

    .line 884
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 885
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # invokes: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isCloseMenuVisible()Z
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1800(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v0, v1, :cond_1

    .line 886
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollStateChanged idle-select-un-2 pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 887
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {p1, v0, v1, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    goto :goto_0
.end method

.method private moveItemToUnSelectedState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;I)V
    .locals 3
    .param p1, "vh"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .param p2, "pos"    # I

    .prologue
    .line 894
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->BLANK:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v0, v1, :cond_0

    .line 895
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScrollStateChanged idle-unselect pos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 896
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    const/16 v2, 0xe6

    invoke-virtual {p1, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V

    .line 898
    :cond_0
    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 12
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "newState"    # I

    .prologue
    const/4 v11, -0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 818
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->initialPosChanged:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$202(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z

    .line 819
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lastScrollState:I
    invoke-static {v6, p2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$702(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I

    .line 820
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V

    .line 821
    if-nez p2, :cond_3

    .line 822
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-boolean v6, v6, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->reCalcScrollPos:Z

    if-eqz v6, :cond_0

    .line 823
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # invokes: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->calculateScrollRange()V
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$800(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V

    .line 825
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrolling:Z
    invoke-static {v6, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$902(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z

    .line 826
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isScrollBy:Z
    invoke-static {v6, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1002(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z

    .line 827
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->switchingScrollBoundary:Z
    invoke-static {v6, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1102(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z

    .line 828
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollByUpReceived:I
    invoke-static {v6, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1202(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I

    .line 829
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I
    invoke-static {v9}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v9

    # invokes: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getPositionFromScrollY(I)I
    invoke-static {v6, v9}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1300(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I

    move-result v2

    .line 830
    .local v2, "pos":I
    invoke-virtual {p1, v2}, Landroid/support/v7/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 831
    .local v5, "vhCurrent":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onScrollStateChanged idle pos:"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, " current="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I
    invoke-static {v10}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, " scrollY="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I
    invoke-static {v10}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v10, " vh="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v5, :cond_4

    move v6, v7

    :goto_0
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 832
    invoke-direct {p0, v5, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->moveItemToSelectedState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;I)V

    .line 833
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .line 835
    .local v3, "upPos":I
    if-ltz v3, :cond_1

    .line 836
    invoke-virtual {p1, v3}, Landroid/support/v7/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 837
    .local v4, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    invoke-direct {p0, v4, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->moveItemToUnSelectedState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;I)V

    .line 839
    .end local v4    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->currentMiddlePosition:I
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v6

    add-int/lit8 v0, v6, 0x1

    .line 840
    .local v0, "downPos":I
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    .line 841
    .restart local v4    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    invoke-direct {p0, v4, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->moveItemToUnSelectedState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;I)V

    .line 843
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # invokes: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->onItemSelected()V
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$600(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V

    .line 845
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isSelectedOperationPending:Z
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1500(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 846
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isSelectedOperationPending:Z
    invoke-static {v6, v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1502(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)Z

    .line 847
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # invokes: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->select(Z)V
    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1600(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Z)V

    .line 855
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-boolean v6, v6, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sendScrollIdleEvent:Z

    if-eqz v6, :cond_3

    .line 856
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v6, v6, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    invoke-interface {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;->onScrollIdle()V

    .line 859
    .end local v0    # "downPos":I
    .end local v2    # "pos":I
    .end local v3    # "upPos":I
    .end local v4    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    .end local v5    # "vhCurrent":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_3
    return-void

    .restart local v2    # "pos":I
    .restart local v5    # "vhCurrent":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_4
    move v6, v8

    .line 831
    goto :goto_0

    .line 848
    .restart local v0    # "downPos":I
    .restart local v3    # "upPos":I
    .restart local v4    # "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollPendingPos:I
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1700(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v6

    if-eq v6, v11, :cond_2

    .line 849
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollPendingPos:I
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1700(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v1

    .line 850
    .local v1, "p":I
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollPendingPos:I
    invoke-static {v6, v11}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$1702(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I

    .line 851
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v6, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollToPosition(I)V

    .line 852
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onScrolled idle scrollToPos:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 865
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    .line 866
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v1

    add-int/2addr v1, p3

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$402(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;I)I

    .line 867
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->waitForTarget:Z

    if-eqz v0, :cond_0

    .line 868
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->targetFound(I)V

    .line 871
    :cond_0
    return-void
.end method
