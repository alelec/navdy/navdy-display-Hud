.class Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;
.super Landroid/support/v7/widget/LinearSmoothScroller;
.source "VerticalLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VerticalScroller"
.end annotation


# instance fields
.field private interpolator:Landroid/view/animation/Interpolator;

.field private layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

.field private position:I

.field private recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

.field private snapPreference:I

.field private vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "layoutManager"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;
    .param p4, "recyclerView"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;
    .param p5, "position"    # I
    .param p6, "snapPreference"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearSmoothScroller;-><init>(Landroid/content/Context;)V

    .line 39
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .line 40
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    .line 41
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    .line 42
    iput p5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->position:I

    .line 43
    iput p6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->snapPreference:I

    .line 44
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->interpolator:Landroid/view/animation/Interpolator;

    .line 45
    return-void
.end method


# virtual methods
.method protected calculateSpeedPerPixel(Landroid/util/DisplayMetrics;)F
    .locals 1
    .param p1, "displayMetrics"    # Landroid/util/DisplayMetrics;

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearSmoothScroller;->calculateSpeedPerPixel(Landroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method protected calculateTimeForDeceleration(I)I
    .locals 1
    .param p1, "dx"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animationDuration:I

    return v0
.end method

.method protected calculateTimeForScrolling(I)I
    .locals 1
    .param p1, "dx"    # I

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearSmoothScroller;->calculateTimeForScrolling(I)I

    move-result v0

    .line 70
    .local v0, "n":I
    return v0
.end method

.method public computeScrollVectorForPosition(I)Landroid/graphics/PointF;
    .locals 1
    .param p1, "targetPosition"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->computeScrollVectorForPosition(I)Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method protected getVerticalSnapPreference()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->snapPreference:I

    return v0
.end method

.method protected onSeekTargetStep(IILandroid/support/v7/widget/RecyclerView$State;Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;)V
    .locals 0
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;
    .param p4, "action"    # Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;

    .prologue
    .line 81
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/LinearSmoothScroller;->onSeekTargetStep(IILandroid/support/v7/widget/RecyclerView$State;Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;)V

    .line 82
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Landroid/support/v7/widget/LinearSmoothScroller;->onStart()V

    .line 60
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v7/widget/LinearSmoothScroller;->onStop()V

    .line 65
    return-void
.end method

.method protected onTargetFound(Landroid/view/View;Landroid/support/v7/widget/RecyclerView$State;Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;)V
    .locals 7
    .param p1, "targetView"    # Landroid/view/View;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;
    .param p3, "action"    # Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->getHorizontalSnapPreference()I

    move-result v4

    invoke-virtual {p0, p1, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->calculateDxToMakeVisible(Landroid/view/View;I)I

    move-result v1

    .line 119
    .local v1, "dx":I
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->getVerticalSnapPreference()I

    move-result v4

    invoke-virtual {p0, p1, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->calculateDyToMakeVisible(Landroid/view/View;I)I

    move-result v2

    .line 120
    .local v2, "dy":I
    mul-int v4, v1, v1

    mul-int v5, v2, v2

    add-int/2addr v4, v5

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 121
    .local v0, "distance":I
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->calculateTimeForDeceleration(I)I

    move-result v3

    .line 122
    .local v3, "time":I
    if-lez v3, :cond_0

    .line 123
    neg-int v4, v1

    neg-int v5, v2

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {p3, v4, v5, v3, v6}, Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;->update(IIILandroid/view/animation/Interpolator;)V

    .line 125
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 126
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "target found dy="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;->getDy()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 128
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;->getDy()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->targetFound(I)V

    .line 129
    return-void
.end method

.method protected updateActionForInterimTarget(Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;)V
    .locals 0
    .param p1, "action"    # Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearSmoothScroller;->updateActionForInterimTarget(Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;)V

    .line 113
    return-void
.end method
