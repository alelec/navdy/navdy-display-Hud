.class public final Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;
.super Ljava/lang/Object;
.source "SwitchViewHolder.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2",
        "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
        "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/view/ViewGroup$MarginLayoutParams;)V",
        "onAnimationUpdate",
        "",
        "animation",
        "Landroid/animation/ValueAnimator;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic $marginLayoutParamas:Landroid/view/ViewGroup$MarginLayoutParams;

.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 0
    .param p1, "$outer"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .param p2, "$captured_local_variable$1"    # Landroid/view/ViewGroup$MarginLayoutParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup$MarginLayoutParams;",
            ")V"
        }
    .end annotation

    .prologue
    .line 393
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;->$marginLayoutParamas:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 395
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;->$marginLayoutParamas:Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Float"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 396
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->getSwitchThumb()Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$2;->$marginLayoutParamas:Landroid/view/ViewGroup$MarginLayoutParams;

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 397
    return-void
.end method
