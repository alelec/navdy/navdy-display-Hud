.class public final Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "SwitchViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3",
        "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;",
        "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V",
        "onAnimationEnd",
        "",
        "animation",
        "Landroid/animation/Animator;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic $model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field final synthetic $pos:I

.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V
    .locals 0
    .param p1, "$outer"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .param p2, "$captured_local_variable$1"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p3, "$captured_local_variable$2"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 402
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->$model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iput p3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->$pos:I

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, -0x1

    .line 404
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 405
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->access$isOn$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->access$setOn$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Z)V

    .line 406
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isOn:Z
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->access$isOn$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Z

    move-result v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->isEnabled:Z
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->access$isEnabled$p(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->drawSwitch(ZZ)V

    .line 407
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getItemSelectionState()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    move-result-object v0

    .line 408
    .local v0, "selectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->$model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->$model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->$pos:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V

    .line 409
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->performSelectAction(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 410
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock()V

    .line 411
    return-void

    .line 405
    .end local v0    # "selectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
