.class public Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.source "IconOptionsViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    }
.end annotation


# static fields
.field private static final iconMargin:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

.field private currentSelection:I

.field private fluctuatorRunnable:Ljava/lang/Runnable;

.field private fluctuatorStartListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private itemNotSelected:Z

.field private lastState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

.field private model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private viewContainers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 40
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 41
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b01d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->iconMargin:I

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 1
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    .line 89
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->fluctuatorStartListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 98
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$2;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    .line 107
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setPivotX(F)V

    .line 108
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setPivotY(F)V

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .prologue
    .line 34
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    return v0
.end method

.method public static buildModel(I[I[I[I[I[IIZ)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p0, "id"    # I
    .param p1, "icons"    # [I
    .param p2, "ids"    # [I
    .param p3, "iconSelectedColors"    # [I
    .param p4, "iconDeselectedColors"    # [I
    .param p5, "iconFluctuatorColors"    # [I
    .param p6, "currentSelection"    # I
    .param p7, "supportsToolTip"    # Z

    .prologue
    .line 52
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 53
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 54
    iput p0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 55
    iput-object p1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    .line 56
    iput-object p2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    .line 57
    iput-object p3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColors:[I

    .line 58
    iput-object p4, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColors:[I

    .line 59
    iput-object p5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColors:[I

    .line 60
    iput p6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->currentIconSelection:I

    .line 61
    iput-boolean p7, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->supportsToolTip:Z

    .line 62
    return-object v0
.end method

.method public static buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;
    .locals 4
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 67
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030086

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 68
    .local v1, "layout":Landroid/view/ViewGroup;
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    invoke-direct {v2, v1, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v2
.end method

.method public static getOffSetFromPos(I)I
    .locals 3
    .param p0, "currentSelection"    # I

    .prologue
    .line 433
    if-gtz p0, :cond_0

    .line 434
    const/4 v0, 0x0

    .line 437
    :goto_0
    return v0

    .line 436
    :cond_0
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->mainIconSize:I

    mul-int/2addr v1, p0

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->iconMargin:I

    mul-int/2addr v2, p0

    add-int v0, v1, v2

    .line 437
    .local v0, "offset":I
    goto :goto_0
.end method

.method private isItemInBounds(I)Z
    .locals 1
    .param p1, "item"    # I

    .prologue
    .line 354
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 355
    :cond_0
    const/4 v0, 0x0

    .line 357
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startFluctuator(IZ)V
    .locals 6
    .param p1, "item"    # I
    .param p2, "changeColor"    # Z

    .prologue
    .line 327
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->isItemInBounds(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 336
    :goto_0
    return-void

    .line 330
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 331
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    if-eqz p2, :cond_1

    .line 332
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    aget v2, v2, p1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColors:[I

    aget v3, v3, p1

    const/4 v4, 0x0

    const v5, 0x3f547ae1    # 0.83f

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 334
    :cond_1
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 335
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/HaloView;->start()V

    goto :goto_0
.end method

.method private stopFluctuator(IZ)V
    .locals 6
    .param p1, "item"    # I
    .param p2, "changeColor"    # Z

    .prologue
    .line 340
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->isItemInBounds(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 351
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 345
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 346
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    if-eqz p2, :cond_1

    .line 347
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    aget v2, v2, p1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColors:[I

    aget v3, v3, p1

    const/4 v4, 0x0

    const v5, 0x3f547ae1    # 0.83f

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 349
    :cond_1
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 350
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/HaloView;->stop()V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 9
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    const/4 v8, 0x0

    const v7, 0x3f547ae1    # 0.83f

    .line 264
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getModel(I)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v3

    if-eq v3, p1, :cond_0

    .line 265
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemNotSelected:Z

    .line 267
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 268
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 269
    .local v2, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v3, v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    aget v4, v4, v1

    iget-object v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColors:[I

    aget v5, v5, v1

    invoke-virtual {v3, v4, v5, v8, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 270
    iget-object v3, v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->small:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    aget v4, v4, v1

    iget-object v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColors:[I

    aget v5, v5, v1

    invoke-virtual {v3, v4, v5, v8, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 271
    const/16 v3, 0x99

    iget-object v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColors:[I

    aget v4, v4, v1

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v4

    iget-object v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColors:[I

    aget v5, v5, v1

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v5

    iget-object v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColors:[I

    aget v6, v6, v1

    invoke-static {v6}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 272
    .local v0, "fluctuatorColor":I
    iget-object v3, v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 273
    iget-object v3, v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/ui/component/HaloView;->setStrokeColor(I)V

    .line 267
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 275
    .end local v0    # "fluctuatorColor":I
    .end local v2    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    :cond_1
    return-void
.end method

.method public clearAnimation()V
    .locals 2

    .prologue
    .line 279
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->stopFluctuator(IZ)V

    .line 280
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->stopAnimation()V

    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 282
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 284
    return-void
.end method

.method public copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
    .locals 5
    .param p1, "imageC"    # Landroid/widget/ImageView;
    .param p2, "titleC"    # Landroid/widget/TextView;
    .param p3, "subTitleC"    # Landroid/widget/TextView;
    .param p4, "subTitle2C"    # Landroid/widget/TextView;
    .param p5, "setImage"    # Z

    .prologue
    .line 307
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    if-gez v3, :cond_0

    .line 318
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 311
    .local v1, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v3, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    .line 312
    .local v0, "view":Landroid/view/View;
    if-eqz p5, :cond_1

    .line 313
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    invoke-static {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->copyImage(Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 315
    :cond_1
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->selectedImageX:I

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->getOffSetFromPos(I)I

    move-result v4

    add-int v2, v3, v4

    .line 316
    .local v2, "x":I
    int-to-float v3, v2

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 317
    sget v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->selectedImageY:I

    int-to-float v3, v3

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setY(F)V

    goto :goto_0
.end method

.method public getCurrentSelection()I
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    return v0
.end method

.method public getCurrentSelectionId()I
    .locals 2

    .prologue
    .line 404
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 405
    :cond_0
    const/4 v0, -0x1

    .line 407
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 7
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 364
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->isItemInBounds(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v1

    .line 367
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$4;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 369
    :pswitch_0
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    if-eqz v2, :cond_0

    .line 373
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    .line 374
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1, v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->stopFluctuator(IZ)V

    .line 375
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-direct {p0, v1, v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->startFluctuator(IZ)V

    .line 376
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getItemSelectionState()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    move-result-object v0

    .line 377
    .local v0, "itemSelectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    iget v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    aget v4, v4, v5

    iget v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V

    .line 378
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    move v1, v6

    .line 379
    goto :goto_0

    .line 382
    .end local v0    # "itemSelectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    :pswitch_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-eq v2, v3, :cond_0

    .line 386
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    .line 387
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->stopFluctuator(IZ)V

    .line 388
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-direct {p0, v1, v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->startFluctuator(IZ)V

    .line 389
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getItemSelectionState()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    move-result-object v0

    .line 390
    .restart local v0    # "itemSelectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    iget v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    aget v4, v4, v5

    iget v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V

    .line 391
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->callItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    move v1, v6

    .line 392
    goto/16 :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hasToolTip()Z
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-boolean v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->supportsToolTip:Z

    return v0
.end method

.method public preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 10
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 226
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 227
    .local v0, "context":Landroid/content/Context;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    iget-object v8, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v8, v8

    iget-object v9, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v9, v9

    add-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    if-eq v7, v8, :cond_1

    .line 228
    iget v7, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->currentIconSelection:I

    iput v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    .line 229
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "preBind: createIcons:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 230
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 231
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 233
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 235
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v7, v7

    if-ge v1, v7, :cond_2

    .line 236
    if-eqz v1, :cond_0

    .line 237
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 238
    .local v4, "view":Landroid/view/View;
    new-instance v3, Landroid/view/ViewGroup$MarginLayoutParams;

    sget v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->iconMargin:I

    sget v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->mainIconSize:I

    invoke-direct {v3, v7, v8}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 239
    .local v3, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 243
    .end local v3    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    new-instance v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    const/4 v7, 0x0

    invoke-direct {v5, v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$1;)V

    .line 244
    .local v5, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    const v7, 0x7f030085

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v9, 0x0

    invoke-virtual {v2, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 245
    .local v6, "viewgroup":Landroid/view/ViewGroup;
    const v7, 0x7f0e00c8

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    iput-object v7, v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    .line 246
    const v7, 0x7f0e00d5

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    iput-object v7, v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 247
    iget-object v7, v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v8, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->inject(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 248
    const v7, 0x7f0e00d6

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iput-object v7, v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 249
    const v7, 0x7f0e00d7

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iput-object v7, v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->small:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 250
    const v7, 0x7f0e00c7

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/HaloView;

    iput-object v7, v5, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    .line 251
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    new-instance v3, Landroid/view/ViewGroup$MarginLayoutParams;

    sget v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->mainIconSize:I

    sget v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->mainIconSize:I

    invoke-direct {v3, v7, v8}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 254
    .restart local v3    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {v6, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 255
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v7, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 235
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 258
    .end local v1    # "i":I
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    .end local v6    # "viewgroup":Landroid/view/ViewGroup;
    :cond_1
    sget-object v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "preBind: createIcons: already created"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 260
    :cond_2
    return-void
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
    .locals 3
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "pos"    # I
    .param p3, "duration"    # I

    .prologue
    .line 289
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->isItemInBounds(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 303
    :goto_0
    return-void

    .line 292
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->stopFluctuator(IZ)V

    .line 293
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 294
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;

    invoke-direct {v2, p0, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;I)V

    invoke-static {v1, p3, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->performClick(Landroid/view/View;ILjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public selectionDown()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 411
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "selectionDown"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 412
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->isItemInBounds(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 413
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectionDown:invalid pos:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 419
    :goto_0
    return-void

    .line 416
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-direct {p0, v1, v4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->stopFluctuator(IZ)V

    .line 417
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 418
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    const/16 v2, 0x19

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->performClickDown(Landroid/view/View;ILjava/lang/Runnable;Z)V

    goto :goto_0
.end method

.method public selectionUp()V
    .locals 4

    .prologue
    .line 422
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "selectionUp"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 423
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->isItemInBounds(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 424
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectionUp:invalid pos:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 430
    :goto_0
    return-void

    .line 427
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->startFluctuator(IZ)V

    .line 428
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 429
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    const/16 v2, 0x19

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->performClickUp(Landroid/view/View;ILjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 13
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    .line 118
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->lastState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v8, p1, :cond_0

    .line 220
    :goto_0
    return-void

    .line 122
    :cond_0
    const/4 v3, 0x0

    .line 123
    .local v3, "initialMove":Z
    iget-boolean v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemNotSelected:Z

    if-eqz v8, :cond_1

    .line 124
    const/4 v3, 0x1

    .line 125
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemNotSelected:Z

    .line 127
    sget-object p2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    .line 130
    :cond_1
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 132
    const/4 v2, 0x0

    .line 133
    .local v2, "iconScaleFactor":F
    const/4 v4, 0x0

    .line 135
    .local v4, "mode":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 147
    :goto_1
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    .line 211
    :cond_2
    :goto_2
    sget-object v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne p1, v8, :cond_3

    .line 212
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v8, :cond_e

    .line 213
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->fluctuatorStartListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 219
    :cond_3
    :goto_3
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->lastState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    goto :goto_0

    .line 137
    :pswitch_0
    const/high16 v2, 0x3f800000    # 1.0f

    .line 138
    sget-object v4, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .line 139
    goto :goto_1

    .line 142
    :pswitch_1
    const v2, 0x3f19999a    # 0.6f

    .line 143
    sget-object v4, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_1

    .line 150
    :pswitch_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v8, v8

    if-ge v1, v8, :cond_2

    .line 151
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 152
    .local v7, "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 153
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->setScaleY(F)V

    .line 154
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8, v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 155
    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    if-ne v1, v8, :cond_4

    .line 156
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v9, v9, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    aget v9, v9, v1

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v10, v10, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColors:[I

    aget v10, v10, v1

    const/4 v11, 0x0

    const v12, 0x3f547ae1    # 0.83f

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 150
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 158
    :cond_4
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v9, v9, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    aget v9, v9, v1

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v10, v10, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColors:[I

    aget v10, v10, v1

    const/4 v11, 0x0

    const v12, 0x3f547ae1    # 0.83f

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    goto :goto_5

    .line 165
    .end local v1    # "i":I
    .end local v7    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    :pswitch_3
    if-eqz v3, :cond_7

    .line 166
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v8, v8

    if-ge v1, v8, :cond_6

    .line 167
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 168
    .restart local v7    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v9, v9, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    aget v9, v9, v1

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v10, v10, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColors:[I

    aget v10, v10, v1

    const/4 v11, 0x0

    const v12, 0x3f547ae1    # 0.83f

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 169
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    move-result-object v8

    if-eq v8, v4, :cond_5

    .line 170
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8, v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 166
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 173
    .end local v7    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    :cond_6
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 174
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->setScaleY(F)V

    goto/16 :goto_2

    .line 176
    .end local v1    # "i":I
    :cond_7
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 177
    const/4 v0, 0x0

    .line 178
    .local v0, "currentSelected":Z
    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_8

    .line 179
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    iget v9, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 180
    .restart local v7    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    move-result-object v8

    if-eq v8, v4, :cond_8

    .line 181
    const/4 v0, 0x1

    .line 182
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8, v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 185
    .end local v7    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_7
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconList:[I

    array-length v8, v8

    if-ge v1, v8, :cond_c

    .line 186
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->viewContainers:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;

    .line 187
    .restart local v7    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    iget v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    if-ne v1, v8, :cond_a

    if-eqz v0, :cond_a

    .line 185
    :cond_9
    :goto_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 190
    :cond_a
    iget-object v8, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    move-result-object v8

    if-eq v8, v4, :cond_9

    .line 191
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    if-nez v8, :cond_b

    .line 192
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v9, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v9}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    goto :goto_8

    .line 194
    :cond_b
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v9, v7, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v9}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_8

    .line 199
    .end local v7    # "viewContainer":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$ViewContainer;
    :cond_c
    sget-object v8, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    aput v2, v9, v10

    invoke-static {v8, v9}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 200
    .local v5, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v8, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v10, 0x0

    aput v2, v9, v10

    invoke-static {v8, v9}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    .line 201
    .local v6, "p2":Landroid/animation/PropertyValuesHolder;
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    if-nez v8, :cond_d

    .line 202
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v10, 0x2

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v11, 0x1

    aput-object v6, v10, v11

    invoke-static {v9, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_2

    .line 204
    :cond_d
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v10, 0x2

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v11, 0x1

    aput-object v6, v10, v11

    invoke-static {v9, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_2

    .line 215
    .end local v0    # "currentSelected":Z
    .end local v1    # "i":I
    .end local v5    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v6    # "p2":Landroid/animation/PropertyValuesHolder;
    :cond_e
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->startFluctuator()V

    goto/16 :goto_3

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 147
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public startFluctuator()V
    .locals 2

    .prologue
    .line 322
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->startFluctuator(IZ)V

    .line 323
    return-void
.end method
