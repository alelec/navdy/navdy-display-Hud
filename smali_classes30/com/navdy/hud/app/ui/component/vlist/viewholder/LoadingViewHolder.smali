.class public Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.source "LoadingViewHolder.java"


# static fields
.field private static final LOADING_ANIMATION_DELAY:I = 0x21

.field private static final LOADING_ANIMATION_DURATION:I = 0x1f4

.field private static final loadingImageSize:I

.field private static final loadingImageSizeUnselected:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private loadingAnimator:Landroid/animation/ObjectAnimator;

.field private loadingImage:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 35
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 36
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b01d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImageSize:I

    .line 37
    const v1, 0x7f0b01d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImageSizeUnselected:I

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 1
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 58
    const v0, 0x7f0e022b

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImage:Landroid/widget/ImageView;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static buildModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 48
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 49
    const v1, 0x7f0e008f

    iput v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 50
    return-object v0
.end method

.method public static buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;
    .locals 4
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 41
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 42
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030088

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 43
    .local v1, "layout":Landroid/view/ViewGroup;
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    invoke-direct {v2, v1, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v2
.end method

.method private startLoadingAnimation()V
    .locals 5

    .prologue
    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImage:Landroid/widget/ImageView;

    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 124
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "started loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 126
    return-void
.end method

.method private stopLoadingAnimation()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 130
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cancelled loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;

    .line 136
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 94
    return-void
.end method

.method public clearAnimation()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->stopLoadingAnimation()V

    .line 99
    return-void
.end method

.method public copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
    .locals 0
    .param p1, "imageC"    # Landroid/widget/ImageView;
    .param p2, "titleC"    # Landroid/widget/TextView;
    .param p3, "subTitleC"    # Landroid/widget/TextView;
    .param p4, "subTitle2C"    # Landroid/widget/TextView;
    .param p5, "setImage"    # Z

    .prologue
    .line 105
    return-void
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "pos"    # I
    .param p3, "duration"    # I

    .prologue
    .line 102
    return-void
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 6
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    .line 69
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne p1, v3, :cond_0

    .line 70
    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImageSize:I

    .line 74
    .local v2, "size":I
    :goto_0
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$2;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 91
    :goto_1
    return-void

    .line 72
    .end local v2    # "size":I
    :cond_0
    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImageSizeUnselected:I

    .restart local v2    # "size":I
    goto :goto_0

    .line 77
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->startLoadingAnimation()V

    .line 78
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 79
    .local v1, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 80
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto :goto_1

    .line 84
    .end local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->startLoadingAnimation()V

    .line 85
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingImage:Landroid/widget/ImageView;

    invoke-static {v3, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->animateDimension(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v0

    .line 86
    .local v0, "animator":Landroid/animation/Animator;
    int-to-long v4, p3

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 87
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 88
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_1

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
