.class public final Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;
.super Ljava/lang/Object;
.source "SwitchViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J:\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0010J\u001e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u0016\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;",
        "",
        "()V",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "getLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "buildModel",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
        "id",
        "",
        "iconFluctuatorColor",
        "title",
        "",
        "subTitle",
        "isOn",
        "",
        "isEnabled",
        "buildViewHolder",
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;",
        "parent",
        "Landroid/view/ViewGroup;",
        "vlist",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList;",
        "handler",
        "Landroid/os/Handler;",
        "getLayout",
        "lytId",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;-><init>()V

    return-void
.end method

.method public static bridge synthetic buildModel$default(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;IILjava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    const/4 v6, 0x0

    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    move v5, v6

    .line 35
    :goto_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_0

    :goto_1
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;->buildModel(IILjava/lang/String;Ljava/lang/String;ZZ)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0

    :cond_0
    move v6, p6

    goto :goto_1

    :cond_1
    move v5, p5

    goto :goto_0
.end method


# virtual methods
.method public final buildModel(IILjava/lang/String;Ljava/lang/String;ZZ)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p1, "id"    # I
    .param p2, "iconFluctuatorColor"    # I
    .param p3, "title"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4, "subTitle"    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5, "isOn"    # Z
    .param p6, "isEnabled"    # Z
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    const-string v1, "title"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "subTitle"

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SWITCH:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 37
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 40
    .restart local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SWITCH:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 41
    iput p1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 42
    iput p2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 43
    iput-object p3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 44
    iput-object p4, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 45
    iput-boolean p5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isOn:Z

    .line 46
    iput-boolean p6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->isEnabled:Z

    .line 47
    return-object v0
.end method

.method public final buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "handler"    # Landroid/os/Handler;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    const-string v1, "parent"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "vlist"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "handler"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    check-cast p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;

    .end local p0    # "this":Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;
    const v1, 0x7f03008c

    invoke-virtual {p0, p1, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$Companion;->getLayout(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v0

    .line 52
    .local v0, "layout":Landroid/view/ViewGroup;
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    invoke-direct {v1, v0, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v1
.end method

.method public final getLayout(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "lytId"    # I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    const-string v2, "parent"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 57
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Lkotlin/TypeCastException;

    const-string v3, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v2, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    check-cast v1, Landroid/view/ViewGroup;

    .line 58
    .local v1, "layout":Landroid/view/ViewGroup;
    return-object v1
.end method

.method public final getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 31
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->access$getLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method
