.class public final synthetic Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->values()[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->values()[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->INIT:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->values()[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->INIT:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->values()[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->INIT:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->values()[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->INIT:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
