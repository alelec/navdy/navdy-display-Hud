.class public final enum Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
.super Ljava/lang/Enum;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ModelType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum BLANK:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum ICON:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum SCROLL_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum SWITCH:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum TITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum TITLE_SUBTITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

.field public static final enum TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 453
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "BLANK"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->BLANK:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 456
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 459
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "TITLE_SUBTITLE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TITLE_SUBTITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 462
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "ICON_BKCOLOR"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 465
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "TWO_ICONS"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 468
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "ICON"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 471
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "LOADING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 474
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "ICON_OPTIONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 477
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "SCROLL_CONTENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SCROLL_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 480
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "LOADING_CONTENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 482
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    const-string v1, "SWITCH"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SWITCH:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 451
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->BLANK:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TITLE_SUBTITLE:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SCROLL_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->SWITCH:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->$VALUES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 451
    const-class v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 451
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->$VALUES:[Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method
