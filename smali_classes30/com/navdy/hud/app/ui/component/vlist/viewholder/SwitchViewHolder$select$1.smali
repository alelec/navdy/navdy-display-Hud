.class public final Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1;
.super Ljava/lang/Object;
.source "SwitchViewHolder.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1",
        "Landroid/animation/ValueAnimator$AnimatorUpdateListener;",
        "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/graphics/drawable/ClipDrawable;)V",
        "onAnimationUpdate",
        "",
        "animation",
        "Landroid/animation/ValueAnimator;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic $clipDrawable:Landroid/graphics/drawable/ClipDrawable;

.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/graphics/drawable/ClipDrawable;)V
    .locals 0
    .param p1, "$outer"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;
    .param p2, "$captured_local_variable$1"    # Landroid/graphics/drawable/ClipDrawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/drawable/ClipDrawable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 386
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1;->$clipDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 388
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type kotlin.Float"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-int v0, v1

    .line 389
    .local v0, "level":I
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1;->$clipDrawable:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    .line 390
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;->getSwitchBackground()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 391
    return-void
.end method
