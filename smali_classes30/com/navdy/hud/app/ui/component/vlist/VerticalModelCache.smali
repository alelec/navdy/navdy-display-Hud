.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;
.super Ljava/lang/Object;
.source "VerticalModelCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;
    }
.end annotation


# static fields
.field private static final map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x1f4

    const/16 v5, 0x64

    .line 14
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->map:Ljava/util/Map;

    .line 35
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->map:Ljava/util/Map;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;

    invoke-direct {v2, v6, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;-><init>(II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->map:Ljava/util/Map;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;

    const/16 v3, 0x46

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;-><init>(II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->map:Ljava/util/Map;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;

    invoke-direct {v2, v6, v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;-><init>(II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addToCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 3
    .param p0, "modelType"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    .line 41
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->map:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;

    .line 45
    .local v0, "entry":Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;
    if-eqz v0, :cond_0

    .line 46
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->items:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->maxItems:I

    if-ge v1, v2, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->clear()V

    .line 48
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static addToCache(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    if-nez p0, :cond_1

    .line 60
    :cond_0
    return-void

    .line 57
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 58
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-static {v2, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->addToCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_0
.end method

.method public static getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 4
    .param p0, "modelType"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .prologue
    const/4 v1, 0x0

    .line 63
    if-nez p0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-object v1

    .line 66
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->map:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;

    .line 67
    .local v0, "entry":Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 68
    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->items:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 69
    .local v1, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    goto :goto_0
.end method
