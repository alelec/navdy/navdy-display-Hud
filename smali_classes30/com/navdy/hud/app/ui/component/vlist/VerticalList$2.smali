.class Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;
.super Ljava/lang/Object;
.source "VerticalList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 772
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 775
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->initialPosChanged:Z
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$200(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 776
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->layoutManager:Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$300(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    .line 777
    .local v0, "n":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v1

    .line 778
    .local v1, "raw":I
    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->adapter:Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalAdapter;->getItemCount()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 780
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initial scroll did not work, firstV="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " raw="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " scrollY="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 784
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # invokes: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->onItemSelected()V
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$600(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V

    .line 788
    .end local v0    # "n":I
    .end local v1    # "raw":I
    :goto_1
    return-void

    .line 782
    .restart local v0    # "n":I
    .restart local v1    # "raw":I
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initial scroll worked firstV="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " raw="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " scrollY="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$2;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->actualScrollY:I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$400(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 786
    .end local v0    # "n":I
    .end local v1    # "raw":I
    :cond_1
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "initial pos changed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method
