.class Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;
.super Ljava/lang/Object;
.source "VerticalModelCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CacheEntry"
.end annotation


# instance fields
.field items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field maxItems:I


# direct methods
.method constructor <init>(II)V
    .locals 3
    .param p1, "maxItems"    # I
    .param p2, "initialItems"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->maxItems:I

    .line 22
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->items:Ljava/util/ArrayList;

    .line 23
    if-lez p2, :cond_0

    .line 24
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 25
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache$CacheEntry;->items:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-direct {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    .end local v0    # "i":I
    :cond_0
    return-void
.end method
