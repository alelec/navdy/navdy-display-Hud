.class public Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;
.source "IconsTwoViewHolder.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private imageIconSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 1
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->imageIconSize:I

    .line 62
    return-void
.end method

.method public static buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 8
    .param p0, "id"    # I
    .param p1, "icon"    # I
    .param p2, "iconSmall"    # I
    .param p3, "iconFluctuatorColor"    # I
    .param p4, "iconDeselectedColor"    # I
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 28
    const/4 v7, 0x0

    move v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    return-object v0
.end method

.method public static buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p0, "id"    # I
    .param p1, "icon"    # I
    .param p2, "iconSmall"    # I
    .param p3, "iconFluctuatorColor"    # I
    .param p4, "iconDeselectedColor"    # I
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "subTitle"    # Ljava/lang/String;
    .param p7, "subTitle2"    # Ljava/lang/String;

    .prologue
    .line 39
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 40
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 43
    .restart local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 44
    iput p0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 45
    iput p1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 46
    iput p2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSmall:I

    .line 47
    iput p3, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 48
    iput p4, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    .line 49
    iput-object p5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    .line 50
    iput-object p6, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    .line 51
    iput-object p7, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    .line 52
    return-object v0
.end method

.method public static buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;
    .locals 3
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 56
    const v1, 0x7f030087

    const v2, 0x7f030010

    invoke-static {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->getLayout(Landroid/view/ViewGroup;II)Landroid/view/ViewGroup;

    move-result-object v0

    .line 57
    .local v0, "layout":Landroid/view/ViewGroup;
    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;

    invoke-direct {v1, v0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v1
.end method

.method private setIcon(IIZZII)V
    .locals 6
    .param p1, "icon"    # I
    .param p2, "iconSmall"    # I
    .param p3, "updateImage"    # Z
    .param p4, "updateSmallImage"    # Z
    .param p5, "deselectedColor"    # I
    .param p6, "iconSize"    # I

    .prologue
    .line 128
    iput p6, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->imageIconSize:I

    .line 129
    const/4 v2, 0x0

    .line 130
    .local v2, "initials":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->extras:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 131
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->extras:Ljava/util/HashMap;

    const-string v5, "INITIAL"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "initials":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 133
    .restart local v2    # "initials":Ljava/lang/String;
    :cond_0
    if-eqz p3, :cond_1

    .line 135
    if-eqz v2, :cond_3

    .line 136
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 140
    .local v1, "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :goto_0
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 141
    .local v0, "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    invoke-virtual {v0, p1, v2, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 144
    .end local v0    # "big":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .end local v1    # "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :cond_1
    if-eqz p4, :cond_2

    .line 145
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 146
    .local v3, "small":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setAlpha(F)V

    .line 147
    if-nez p2, :cond_4

    .line 148
    invoke-virtual {v3, p5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setBkColor(I)V

    .line 152
    :goto_1
    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->TINY:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v3, p2, v2, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 154
    .end local v3    # "small":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    :cond_2
    return-void

    .line 138
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .restart local v1    # "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    goto :goto_0

    .line 150
    .end local v1    # "bigStyle":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    .restart local v3    # "small":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    :cond_4
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setBkColor(I)V

    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 7
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 118
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 119
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSmall:I

    iget-boolean v3, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateImage:Z

    iget-boolean v4, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSmallImage:Z

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSize:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->setIcon(IIZZII)V

    .line 120
    return-void
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 5
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V

    .line 74
    const/4 v0, 0x0

    .line 75
    .local v0, "iconSize":I
    const/4 v2, 0x0

    .line 77
    .local v2, "mode":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 94
    :goto_0
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->imageIconSize:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 95
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->imageIconSize:I

    .line 98
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 114
    :cond_1
    :goto_1
    return-void

    .line 79
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->selectedIconSize:I

    .line 80
    sget-object v2, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .line 81
    goto :goto_0

    .line 84
    :pswitch_1
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->iconScaleAnimationDisabled:Z

    if-eqz v3, :cond_2

    .line 85
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->selectedIconSize:I

    .line 86
    sget-object v2, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_0

    .line 88
    :cond_2
    sget v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->unselectedIconSize:I

    .line 89
    sget-object v2, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_0

    .line 101
    :pswitch_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->iconContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 102
    .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 103
    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 104
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    goto :goto_1

    .line 108
    .end local v1    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :pswitch_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->iconContainer:Landroid/view/ViewGroup;

    invoke-static {v4, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->animateDimension(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 109
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    move-result-object v3

    if-eq v3, v2, :cond_1

    .line 110
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 98
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
