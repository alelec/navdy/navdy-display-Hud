.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;
.super Ljava/lang/Object;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FontInfo"
.end annotation


# instance fields
.field public subTitle2FontSize:F

.field public subTitle2FontTopMargin:F

.field public subTitleFontSize:F

.field public subTitleFontTopMargin:F

.field public titleFontSize:F

.field public titleFontTopMargin:F

.field public titleScale:F

.field public titleSingleLine:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    return-void
.end method
