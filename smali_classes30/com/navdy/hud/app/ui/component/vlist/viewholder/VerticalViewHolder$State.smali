.class public final enum Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
.super Ljava/lang/Enum;
.source "VerticalViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

.field public static final enum SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

.field public static final enum UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    const-string v1, "SELECTED"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 45
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    const-string v1, "UNSELECTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->UNSELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->$VALUES:[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->$VALUES:[Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    return-object v0
.end method
