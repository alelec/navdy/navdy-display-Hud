.class public Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.source "BlankViewHolder.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 0
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 36
    return-void
.end method

.method public static buildModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 24
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->BLANK:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 25
    return-object v0
.end method

.method public static buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;
    .locals 4
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 29
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 30
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030083

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 31
    .local v1, "layout":Landroid/view/ViewGroup;
    new-instance v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;

    invoke-direct {v2, v1, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/BlankViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v2
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 45
    return-void
.end method

.method public clearAnimation()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
    .locals 0
    .param p1, "imageC"    # Landroid/widget/ImageView;
    .param p2, "titleC"    # Landroid/widget/TextView;
    .param p3, "subTitleC"    # Landroid/widget/TextView;
    .param p4, "subTitle2C"    # Landroid/widget/TextView;
    .param p5, "setImage"    # Z

    .prologue
    .line 54
    return-void
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->BLANK:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "pos"    # I
    .param p3, "duration"    # I

    .prologue
    .line 51
    return-void
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 0
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    .line 42
    return-void
.end method
