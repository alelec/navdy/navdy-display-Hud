.class Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "LoadingViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->startLoadingAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x21

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/LoadingViewHolder;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "abandon loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
