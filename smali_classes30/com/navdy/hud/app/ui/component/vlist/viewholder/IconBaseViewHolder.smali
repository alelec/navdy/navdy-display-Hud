.class public abstract Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.source "IconBaseViewHolder.java"


# static fields
.field public static final FLUCTUATOR_OPACITY_ALPHA:I = 0x99

.field public static final HALO_DELAY_START_DURATION:I = 0x64

.field private static final location:[I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

.field protected crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

.field private fluctuatorRunnable:Ljava/lang/Runnable;

.field private fluctuatorStartListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field protected haloView:Lcom/navdy/hud/app/ui/component/HaloView;

.field private hasIconFluctuatorColor:Z

.field private hasSubTitle:Z

.field private hasSubTitle2:Z

.field protected iconContainer:Landroid/view/ViewGroup;

.field protected iconScaleAnimationDisabled:Z

.field protected imageContainer:Landroid/view/ViewGroup;

.field protected subTitle:Landroid/widget/TextView;

.field protected subTitle2:Landroid/widget/TextView;

.field protected textAnimationDisabled:Z

.field protected title:Landroid/widget/TextView;

.field private titleSelectedTopMargin:F

.field private titleUnselectedScale:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->location:[I

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 2
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 70
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->fluctuatorStartListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 79
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$2;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    .line 88
    const v0, 0x7f0e00c6

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->imageContainer:Landroid/view/ViewGroup;

    .line 89
    const v0, 0x7f0e00c8

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->iconContainer:Landroid/view/ViewGroup;

    .line 90
    const v0, 0x7f0e00c7

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/HaloView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 92
    const v0, 0x7f0e008e

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 93
    const v0, 0x7f0e00c3

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0e0111

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e0229

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static getLayout(Landroid/view/ViewGroup;II)Landroid/view/ViewGroup;
    .locals 6
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "lytId"    # I
    .param p2, "subLytId"    # I

    .prologue
    const/4 v5, 0x0

    .line 37
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 38
    .local v2, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v2, p1, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 39
    .local v3, "layout":Landroid/view/ViewGroup;
    const v4, 0x7f0e00c8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 40
    .local v0, "iconContainer":Landroid/view/ViewGroup;
    invoke-virtual {v2, p2, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 41
    .local v1, "image":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    sget-object v4, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->inject(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 42
    const v4, 0x7f0e008e

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setId(I)V

    .line 43
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 44
    return-object v3
.end method

.method private getSubTitle2Color()I
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->extras:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 338
    const/4 v0, 0x0

    .line 340
    :goto_0
    return v0

    :cond_0
    const-string v0, "SUBTITLE_2_COLOR"

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->getTextColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private getSubTitleColor()I
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->extras:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 331
    const/4 v0, 0x0

    .line 333
    :goto_0
    return v0

    :cond_0
    const-string v0, "SUBTITLE_COLOR"

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->getTextColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private getTextColor(Ljava/lang/String;)I
    .locals 4
    .param p1, "property"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 344
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->extras:Ljava/util/HashMap;

    if-nez v3, :cond_1

    .line 355
    :cond_0
    :goto_0
    return v2

    .line 347
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->extras:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 348
    .local v1, "str":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 353
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "nex":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method private setIconFluctuatorColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 283
    if-eqz p1, :cond_0

    .line 284
    const/16 v1, 0x99

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 285
    .local v0, "fluctuatorColor":I
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasIconFluctuatorColor:Z

    .line 286
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/HaloView;->setStrokeColor(I)V

    .line 290
    .end local v0    # "fluctuatorColor":I
    :goto_0
    return-void

    .line 288
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasIconFluctuatorColor:Z

    goto :goto_0
.end method

.method private setMultiLineStyles(Landroid/widget/TextView;ZI)V
    .locals 1
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "singleLine"    # Z
    .param p3, "style"    # I

    .prologue
    .line 420
    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    .line 421
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0, p3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 423
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 424
    if-eqz p2, :cond_1

    .line 425
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 430
    :goto_0
    return-void

    .line 427
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 428
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0
.end method

.method private setSubTitle(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "formatted"    # Z

    .prologue
    .line 241
    if-nez p1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle:Z

    .line 258
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->getSubTitleColor()I

    move-result v0

    .line 246
    .local v0, "c":I
    if-nez v0, :cond_1

    .line 247
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitleColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 251
    :goto_1
    if-eqz p2, :cond_2

    .line 252
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle:Z

    goto :goto_0

    .line 249
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 254
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private setSubTitle2(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "formatted"    # Z

    .prologue
    const/4 v3, 0x0

    .line 261
    if-nez p1, :cond_0

    .line 262
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle2:Z

    .line 280
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->getSubTitle2Color()I

    move-result v0

    .line 267
    .local v0, "c":I
    if-nez v0, :cond_1

    .line 268
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 272
    :goto_1
    if-eqz p2, :cond_2

    .line 273
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    :goto_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 278
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle2:Z

    goto :goto_0

    .line 270
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 275
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 237
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    return-void
.end method

.method private stopFluctuator()V
    .locals 2

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasIconFluctuatorColor:Z

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->fluctuatorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 303
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/HaloView;->stop()V

    .line 306
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 4
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 434
    iget-boolean v0, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateTitle:Z

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setTitle(Ljava/lang/String;)V

    .line 438
    :cond_0
    iget-boolean v0, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSubTitle:Z

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 440
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitleFormatted:Z

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setSubTitle(Ljava/lang/String;Z)V

    .line 446
    :cond_1
    :goto_0
    iget-boolean v0, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;->updateSubTitle2:Z

    if-eqz v0, :cond_2

    .line 447
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 448
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2Formatted:Z

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setSubTitle2(Ljava/lang/String;Z)V

    .line 454
    :cond_2
    :goto_1
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noTextAnimation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->textAnimationDisabled:Z

    .line 455
    iget-boolean v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->noImageScaleAnimation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->iconScaleAnimationDisabled:Z

    .line 457
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setIconFluctuatorColor(I)V

    .line 458
    return-void

    .line 442
    :cond_3
    invoke-direct {p0, v3, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setSubTitle(Ljava/lang/String;Z)V

    goto :goto_0

    .line 450
    :cond_4
    invoke-direct {p0, v3, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setSubTitle2(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public clearAnimation()V
    .locals 2

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->stopFluctuator()V

    .line 230
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->stopAnimation()V

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 232
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->layout:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 234
    return-void
.end method

.method public copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
    .locals 4
    .param p1, "imageC"    # Landroid/widget/ImageView;
    .param p2, "titleC"    # Landroid/widget/TextView;
    .param p3, "subTitleC"    # Landroid/widget/TextView;
    .param p4, "subTitle2C"    # Landroid/widget/TextView;
    .param p5, "setImage"    # Z

    .prologue
    const/4 v3, 0x1

    .line 365
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    .line 366
    .local v0, "view":Landroid/view/View;
    if-eqz p5, :cond_0

    .line 367
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    invoke-static {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->copyImage(Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 369
    :cond_0
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->selectedImageX:I

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 370
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->selectedImageY:I

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 372
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->selectedTextX:I

    int-to-float v1, v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setX(F)V

    .line 374
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->location:[I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 375
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->location:[I

    aget v1, v1, v3

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->rootTopOffset:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setY(F)V

    .line 377
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle:Z

    if-eqz v1, :cond_1

    .line 378
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->selectedTextX:I

    int-to-float v1, v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setX(F)V

    .line 380
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->location:[I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 381
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->location:[I

    aget v1, v1, v3

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->rootTopOffset:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setY(F)V

    .line 386
    :goto_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle2:Z

    if-eqz v1, :cond_2

    .line 387
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->selectedTextX:I

    int-to-float v1, v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setX(F)V

    .line 389
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->location:[I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 390
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->location:[I

    aget v1, v1, v3

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->rootTopOffset:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setY(F)V

    .line 394
    :goto_1
    return-void

    .line 383
    :cond_1
    const-string v1, ""

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 392
    :cond_2
    const-string v1, ""

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 4
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 398
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 399
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 400
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 401
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget-boolean v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleSingleLine:Z

    const/4 v3, -0x1

    invoke-direct {p0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setMultiLineStyles(Landroid/widget/TextView;ZI)V

    .line 402
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    const/4 v2, 0x0

    const v3, 0x3f666666    # 0.9f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 403
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->titleSelectedTopMargin:F

    .line 405
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 406
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 407
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    iget-boolean v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iget-boolean v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle_2Lines:Z

    if-eqz v2, :cond_1

    const v2, 0x7f0c0033

    :goto_1
    invoke-direct {p0, v3, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->setMultiLineStyles(Landroid/widget/TextView;ZI)V

    .line 409
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 411
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 412
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 413
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 415
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleScale:F

    iput v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->titleUnselectedScale:F

    .line 416
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setSmallAlpha(F)V

    .line 417
    return-void

    .line 407
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const v2, 0x7f0c0032

    goto :goto_1
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
    .locals 2
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "pos"    # I
    .param p3, "duration"    # I

    .prologue
    .line 314
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasIconFluctuatorColor:Z

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/HaloView;->stop()V

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->iconContainer:Landroid/view/ViewGroup;

    new-instance v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V

    invoke-static {v0, p3, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->performClick(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 327
    return-void
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 17
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    .line 100
    const/4 v11, 0x0

    .line 101
    .local v11, "titleScaleFactor":F
    const/4 v12, 0x0

    .line 103
    .local v12, "titleTopMargin":F
    const/4 v9, 0x0

    .line 104
    .local v9, "subTitleAlpha":F
    const/4 v10, 0x0

    .line 106
    .local v10, "subTitleScaleFactor":F
    const/4 v7, 0x0

    .line 107
    .local v7, "subTitle2Alpha":F
    const/4 v8, 0x0

    .line 109
    .local v8, "subTitle2ScaleFactor":F
    move-object/from16 v2, p1

    .line 110
    .local v2, "itemState":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 112
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->textAnimationDisabled:Z

    if-eqz v13, :cond_2

    .line 113
    sget-object v13, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->MOVE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    move-object/from16 v0, p2

    if-ne v0, v13, :cond_1

    .line 115
    new-instance v13, Landroid/animation/AnimatorSet;

    invoke-direct {v13}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 116
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    const/4 v14, 0x2

    new-array v14, v14, [F

    fill-array-data v14, :array_0

    invoke-static {v14}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 124
    :cond_2
    sget-object v13, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State:[I

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 144
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle:Z

    if-nez v13, :cond_3

    .line 145
    const/4 v12, 0x0

    .line 146
    const/4 v9, 0x0

    .line 147
    const/4 v7, 0x0

    .line 151
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setPivotX(F)V

    .line 152
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    sget v14, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->titleHeight:F

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setPivotY(F)V

    .line 153
    sget-object v13, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual/range {p2 .. p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_1

    .line 173
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setPivotX(F)V

    .line 174
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    sget v14, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitleHeight:F

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setPivotY(F)V

    .line 175
    move-object/from16 v1, p2

    .line 176
    .local v1, "animation2":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle:Z

    if-nez v13, :cond_4

    .line 177
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    .line 179
    :cond_4
    sget-object v13, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_2

    .line 196
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setPivotX(F)V

    .line 197
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    sget v14, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitleHeight:F

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setPivotY(F)V

    .line 198
    move-object/from16 v1, p2

    .line 199
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasSubTitle2:Z

    if-nez v13, :cond_5

    .line 200
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->NONE:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;

    .line 202
    :cond_5
    sget-object v13, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$4;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_3

    .line 218
    :goto_4
    sget-object v13, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v2, v13, :cond_0

    if-eqz p4, :cond_0

    .line 219
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v13, :cond_6

    .line 220
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->fluctuatorStartListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v13, v14}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/16 :goto_0

    .line 126
    .end local v1    # "animation2":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    :pswitch_0
    const/high16 v11, 0x3f800000    # 1.0f

    .line 127
    move-object/from16 v0, p0

    iget v12, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->titleSelectedTopMargin:F

    .line 128
    const/high16 v9, 0x3f800000    # 1.0f

    .line 129
    const/high16 v10, 0x3f800000    # 1.0f

    .line 130
    const/high16 v7, 0x3f800000    # 1.0f

    .line 131
    const/high16 v8, 0x3f800000    # 1.0f

    .line 132
    goto/16 :goto_1

    .line 135
    :pswitch_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->titleUnselectedScale:F

    .line 136
    const/4 v12, 0x0

    .line 137
    const/4 v9, 0x0

    .line 138
    move-object/from16 v0, p0

    iget v10, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->titleUnselectedScale:F

    .line 139
    const/4 v7, 0x0

    .line 140
    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->titleUnselectedScale:F

    goto/16 :goto_1

    .line 156
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v13, v11}, Landroid/widget/TextView;->setScaleX(F)V

    .line 157
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v13, v11}, Landroid/widget/TextView;->setScaleY(F)V

    .line 158
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 159
    .local v3, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    float-to-int v13, v12

    iput v13, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 160
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->requestLayout()V

    goto/16 :goto_2

    .line 164
    .end local v3    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :pswitch_3
    new-instance v13, Landroid/animation/AnimatorSet;

    invoke-direct {v13}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 165
    sget-object v13, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v11, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 166
    .local v4, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v13, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v11, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 167
    .local v5, "p2":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    const/4 v15, 0x2

    new-array v15, v15, [Landroid/animation/PropertyValuesHolder;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    invoke-static {v14, v15}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 168
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->title:Landroid/widget/TextView;

    float-to-int v15, v12

    invoke-static {v14, v15}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->animateMargin(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_2

    .line 182
    .end local v4    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v5    # "p2":Landroid/animation/PropertyValuesHolder;
    .restart local v1    # "animation2":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v13, v9}, Landroid/widget/TextView;->setAlpha(F)V

    .line 183
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setScaleX(F)V

    .line 184
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setScaleY(F)V

    goto/16 :goto_3

    .line 188
    :pswitch_5
    sget-object v13, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v10, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 189
    .restart local v4    # "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v13, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v10, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 190
    .restart local v5    # "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v13, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v9, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    .line 191
    .local v6, "p3":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle:Landroid/widget/TextView;

    const/4 v15, 0x3

    new-array v15, v15, [Landroid/animation/PropertyValuesHolder;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    const/16 v16, 0x2

    aput-object v6, v15, v16

    invoke-static {v14, v15}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_3

    .line 205
    .end local v4    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v5    # "p2":Landroid/animation/PropertyValuesHolder;
    .end local v6    # "p3":Landroid/animation/PropertyValuesHolder;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v13, v7}, Landroid/widget/TextView;->setAlpha(F)V

    .line 206
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setScaleX(F)V

    .line 207
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setScaleY(F)V

    goto/16 :goto_4

    .line 211
    :pswitch_7
    sget-object v13, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v8, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 212
    .restart local v4    # "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v13, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v8, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 213
    .restart local v5    # "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v13, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v14, 0x1

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v9, v14, v15

    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    .line 214
    .restart local v6    # "p3":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->subTitle2:Landroid/widget/TextView;

    const/4 v15, 0x3

    new-array v15, v15, [Landroid/animation/PropertyValuesHolder;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    const/16 v16, 0x2

    aput-object v6, v15, v16

    invoke-static {v14, v15}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_4

    .line 222
    .end local v4    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v5    # "p2":Landroid/animation/PropertyValuesHolder;
    .end local v6    # "p3":Landroid/animation/PropertyValuesHolder;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->startFluctuator()V

    goto/16 :goto_0

    .line 116
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 153
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 179
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 202
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public startFluctuator()V
    .locals 2

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->hasIconFluctuatorColor:Z

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/HaloView;->start()V

    .line 298
    :cond_0
    return-void
.end method
