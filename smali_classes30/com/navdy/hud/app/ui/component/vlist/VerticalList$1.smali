.class Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .prologue
    .line 745
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 748
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;->isFastScrolling()Z

    move-result v0

    if-nez v0, :cond_1

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$000(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;)Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 751
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$1;->this$0:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->indicatorAnimatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->access$102(Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 752
    return-void
.end method
