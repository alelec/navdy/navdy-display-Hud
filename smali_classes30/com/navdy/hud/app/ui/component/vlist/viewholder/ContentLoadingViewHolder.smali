.class public Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;
.super Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.source "ContentLoadingViewHolder.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

.field private crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

.field private iconContainer:Landroid/view/ViewGroup;

.field private imageContainer:Landroid/view/ViewGroup;

.field private loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field private loopEnd:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private loopImageView:Landroid/widget/ImageView;

.field private loopStart:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private unSelImageView:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 1
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    .line 62
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$1;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopStart:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 77
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$2;-><init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopEnd:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 94
    const v0, 0x7f0e00c6

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->imageContainer:Landroid/view/ViewGroup;

    .line 95
    const v0, 0x7f0e00c8

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->iconContainer:Landroid/view/ViewGroup;

    .line 96
    const v0, 0x7f0e008e

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 97
    const v0, 0x7f0e0226

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    .line 98
    const v0, 0x7f0e0227

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    .line 99
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    return-object v0
.end method

.method public static buildModel(III)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .locals 2
    .param p0, "icon"    # I
    .param p1, "iconSelectedColor"    # I
    .param p2, "iconDeselectedColor"    # I

    .prologue
    .line 32
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalModelCache;->getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 33
    .local v0, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .end local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;-><init>()V

    .line 36
    .restart local v0    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    .line 37
    const v1, 0x7f0e008d

    iput v1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    .line 38
    iput p0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 39
    iput p1, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 40
    iput p2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    .line 41
    return-object v0
.end method

.method public static buildViewHolder(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;
    .locals 6
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 45
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 46
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030084

    const/4 v5, 0x0

    invoke-virtual {v2, v4, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 47
    .local v3, "layout":Landroid/view/ViewGroup;
    const v4, 0x7f0e00c8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 48
    .local v0, "iconContainer":Landroid/view/ViewGroup;
    const v4, 0x7f0e00d5

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 49
    .local v1, "image":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
    sget-object v4, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->inject(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 50
    const v4, 0x7f0e008e

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setId(I)V

    .line 51
    new-instance v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;

    invoke-direct {v4, v3, p1, p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V

    return-object v4
.end method

.method private setIcon(III)V
    .locals 6
    .param p1, "icon"    # I
    .param p2, "iconSelectedColor"    # I
    .param p3, "iconDeselectedColor"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const v3, 0x3f547ae1    # 0.83f

    .line 203
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 204
    .local v0, "big":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setDraw(Z)V

    .line 205
    invoke-virtual {v0, p1, p2, v5, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 207
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 208
    .local v1, "small":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setDraw(Z)V

    .line 209
    invoke-virtual {v1, p1, p3, v5, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 210
    return-void
.end method


# virtual methods
.method public bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 3
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 199
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconDeselectedColor:I

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->setIcon(III)V

    .line 200
    return-void
.end method

.method public clearAnimation()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 112
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->stopAnimation()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->layout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->layout:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 116
    return-void
.end method

.method public copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
    .locals 0
    .param p1, "imageC"    # Landroid/widget/ImageView;
    .param p2, "titleC"    # Landroid/widget/TextView;
    .param p3, "subTitleC"    # Landroid/widget/TextView;
    .param p4, "subTitle2C"    # Landroid/widget/TextView;
    .param p5, "setImage"    # Z

    .prologue
    .line 124
    return-void
.end method

.method public getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->LOADING_CONTENT:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    return-object v0
.end method

.method public preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 2
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setSmallAlpha(F)V

    .line 193
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getBig()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIconShape(Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V

    .line 194
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getSmall()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIconShape(Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V

    .line 195
    return-void
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "pos"    # I
    .param p3, "duration"    # I

    .prologue
    .line 121
    return-void
.end method

.method public setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 10
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 129
    .local v0, "iconScaleFactor":F
    const/4 v1, 0x0

    .line 130
    .local v1, "mode":Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 132
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$3;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 144
    :goto_0
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder$3;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 188
    :cond_0
    :goto_1
    return-void

    .line 134
    :pswitch_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 135
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .line 136
    goto :goto_0

    .line 139
    :pswitch_1
    const v0, 0x3f19999a    # 0.6f

    .line 140
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_0

    .line 147
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->imageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 148
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->imageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setScaleY(F)V

    .line 149
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 150
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne p1, v4, :cond_1

    .line 151
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 152
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 153
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 155
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v4

    if-nez v4, :cond_0

    .line 156
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    goto :goto_1

    .line 159
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 161
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 162
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 163
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 164
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto :goto_1

    .line 170
    :pswitch_3
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 171
    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v0, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 172
    .local v2, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v0, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 173
    .local v3, "p2":Landroid/animation/PropertyValuesHolder;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->imageContainer:Landroid/view/ViewGroup;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/animation/PropertyValuesHolder;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    invoke-static {v5, v6}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    .line 174
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    move-result-object v4

    if-eq v4, v1, :cond_2

    .line 175
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 177
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne p1, v4, :cond_3

    .line 178
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopStart:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 179
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    aput v9, v7, v8

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 180
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_1

    .line 182
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopEnd:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 183
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->unSelImageView:Landroid/widget/ImageView;

    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    aput v9, v7, v8

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 184
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->animatorSetBuilder:Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/ContentLoadingViewHolder;->loopImageView:Landroid/widget/ImageView;

    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_1

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 144
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
