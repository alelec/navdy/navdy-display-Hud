.class Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;
.super Ljava/lang/Object;
.source "IconBaseViewHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;

.field final synthetic val$model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->val$model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iput p3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->val$pos:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 322
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getItemSelectionState()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    move-result-object v0

    .line 323
    .local v0, "selectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->val$model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->val$model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->val$pos:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V

    .line 324
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBaseViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->performSelectAction(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 325
    return-void
.end method
