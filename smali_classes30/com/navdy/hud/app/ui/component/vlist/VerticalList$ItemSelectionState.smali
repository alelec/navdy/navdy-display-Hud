.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
.super Ljava/lang/Object;
.source "VerticalList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemSelectionState"
.end annotation


# instance fields
.field public id:I

.field public model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field public pos:I

.field public subId:I

.field public subPosition:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    .line 501
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    .line 502
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    .line 503
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    return-void
.end method


# virtual methods
.method public set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "id"    # I
    .param p3, "pos"    # I
    .param p4, "subId"    # I
    .param p5, "subPosition"    # I

    .prologue
    .line 506
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 507
    iput p2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    .line 508
    iput p3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    .line 509
    iput p4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    .line 510
    iput p5, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subPosition:I

    .line 511
    return-void
.end method
