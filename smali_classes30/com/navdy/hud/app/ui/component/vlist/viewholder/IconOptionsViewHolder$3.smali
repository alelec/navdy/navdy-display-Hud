.class Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;
.super Ljava/lang/Object;
.source "IconOptionsViewHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

.field final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    iput p2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->val$pos:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 297
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getItemSelectionState()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    move-result-object v0

    .line 298
    .local v0, "selectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->val$pos:I

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .line 299
    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->model:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->access$200(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconIds:[I

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->access$300(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)I

    move-result v5

    aget v4, v4, v5

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    # getter for: Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->currentSelection:I
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->access$300(Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;)I

    move-result v5

    .line 298
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V

    .line 300
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder$3;->this$0:Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->performSelectAction(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 301
    return-void
.end method
