.class public abstract Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "VerticalViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;,
        Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    }
.end annotation


# static fields
.field public static final EMPTY:Ljava/lang/String; = ""

.field public static final NO_COLOR:I

.field public static final iconMargin:I

.field public static final listItemHeight:I

.field public static final mainIconSize:I

.field public static final rootTopOffset:I

.field static final sLogger:Lcom/navdy/service/library/log/Logger;

.field public static final scrollDistance:I

.field public static final selectedIconSize:I

.field public static final selectedImageX:I

.field public static final selectedImageY:I

.field public static final selectedTextX:I

.field public static final subTitle2Color:I

.field public static final subTitleColor:I

.field public static final subTitleHeight:F

.field public static final textLeftMargin:I

.field public static final titleHeight:F

.field public static final unselectedIconSize:I


# instance fields
.field protected currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

.field protected extras:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected handler:Landroid/os/Handler;

.field protected interpolator:Landroid/view/animation/Interpolator;

.field protected itemAnimatorSet:Landroid/animation/AnimatorSet;

.field public layout:Landroid/view/ViewGroup;

.field protected pos:I

.field protected vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const v3, 0x7f0d00bd

    .line 38
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 77
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b01d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedIconSize:I

    .line 79
    const v1, 0x7f0b01dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->unselectedIconSize:I

    .line 80
    const v1, 0x7f0b01f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedImageX:I

    .line 81
    const v1, 0x7f0b01f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedImageY:I

    .line 82
    const v1, 0x7f0b01d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->mainIconSize:I

    .line 83
    const v1, 0x7f0b01df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->textLeftMargin:I

    .line 84
    const v1, 0x7f0b01d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    .line 85
    const v1, 0x7f0b01e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->titleHeight:F

    .line 86
    const v1, 0x7f0b01de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->subTitleHeight:F

    .line 87
    const v1, 0x7f0b01f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedTextX:I

    .line 88
    const v1, 0x7f0b01f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->rootTopOffset:I

    .line 89
    const v1, 0x7f0b01d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->iconMargin:I

    .line 90
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->subTitleColor:I

    .line 91
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->subTitle2Color:I

    .line 92
    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->listItemHeight:I

    sput v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->scrollDistance:I

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Landroid/os/Handler;)V
    .locals 1
    .param p1, "layout"    # Landroid/view/ViewGroup;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 109
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    .line 100
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->interpolator:Landroid/view/animation/Interpolator;

    .line 110
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->layout:Landroid/view/ViewGroup;

    .line 111
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .line 112
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->handler:Landroid/os/Handler;

    .line 113
    return-void
.end method


# virtual methods
.method public abstract bind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
.end method

.method public abstract clearAnimation()V
.end method

.method public abstract copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V
.end method

.method public abstract getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;
.end method

.method public getPos()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    return v0
.end method

.method public getState()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    return-object v0
.end method

.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public hasToolTip()Z
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    return v0
.end method

.method public preBind(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "modelState"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 197
    return-void
.end method

.method public abstract select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;II)V
.end method

.method public setExtras(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "extras":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->extras:Ljava/util/HashMap;

    .line 128
    return-void
.end method

.method public abstract setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
.end method

.method public setPos(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 120
    return-void
.end method

.method public final setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;I)V
    .locals 1
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I

    .prologue
    .line 131
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V

    .line 132
    return-void
.end method

.method public final setState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V
    .locals 4
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;
    .param p2, "animation"    # Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;
    .param p3, "duration"    # I
    .param p4, "startFluctuator"    # Z

    .prologue
    .line 135
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$1;->$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->getModelType()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 139
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "} animate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " dur="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current-raw {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "} vh-id{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne v0, p1, :cond_3

    .line 144
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne p1, v0, :cond_2

    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->selectedList:Ljava/util/HashSet;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->selectedList:Ljava/util/HashSet;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->clearAnimation()V

    .line 153
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->currentState:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    .line 154
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;->SELECTED:Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;

    if-ne p1, v0, :cond_4

    .line 155
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->selectedList:Ljava/util/HashSet;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 160
    :goto_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->setItemState(Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$State;Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder$AnimationType;IZ)V

    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->selectedList:Ljava/util/HashSet;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->pos:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public startFluctuator()V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 176
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->itemAnimatorSet:Landroid/animation/AnimatorSet;

    .line 177
    return-void
.end method
