.class public Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "VerticalLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

.field private context:Landroid/content/Context;

.field private listLoaded:Z

.field private recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

.field private vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vlist"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList;
    .param p3, "callback"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;
    .param p4, "recyclerView"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    .prologue
    .line 147
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 148
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->context:Landroid/content/Context;

    .line 149
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .line 150
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    .line 151
    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    .line 152
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->listLoaded:Z

    .line 171
    return-void
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 1
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 162
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 163
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->listLoaded:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->listLoaded:Z

    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->callback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;->onLoad()V

    .line 167
    :cond_0
    return-void
.end method

.method public smoothScrollToPosition(II)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "snapPreference"    # I

    .prologue
    .line 155
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->vlist:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    move-object v3, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/ui/component/vlist/VerticalList;Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;II)V

    .line 156
    .local v0, "verticalScroller":Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager$VerticalScroller;->setTargetPosition(I)V

    .line 157
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalLayoutManager;->startSmoothScroll(Landroid/support/v7/widget/RecyclerView$SmoothScroller;)V

    .line 158
    return-void
.end method
