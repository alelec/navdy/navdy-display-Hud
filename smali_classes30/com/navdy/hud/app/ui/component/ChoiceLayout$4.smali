.class Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;
.super Ljava/lang/Object;
.source "ChoiceLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 267
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->executeView:Landroid/view/View;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    .line 275
    :goto_0
    return-void

    .line 270
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 271
    .local v1, "margin":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 272
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 273
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 274
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$4;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    goto :goto_0
.end method
