.class public Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
.super Landroid/support/constraint/ConstraintLayout;
.source "ChoiceLayout2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;,
        Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;,
        Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;,
        Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;
    }
.end annotation


# static fields
.field private static final DEFAULT_SCALE:F = 0.5f

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static defaultHaloDuration:I

.field private static defaultHaloEndRadius:I

.field private static defaultHaloMiddleRadius:I

.field private static defaultHaloStartDelay:I

.field private static defaultHaloStartRadius:I

.field private static defaultIconHaloSize:I

.field private static defaultIconSize:I

.field private static defaultItemPadding:I

.field private static defaultLabelSize:I

.field private static defaultTopPadding:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private choices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private clickMode:Z

.field private currentSelection:I

.field private haloDuration:I

.field private haloEndRadius:I

.field private haloMiddleRadius:I

.field private haloStartDelay:I

.field private haloStartRadius:I

.field private iconHaloSize:I

.field private iconSize:I

.field private itemPadding:I

.field private itemTopPadding:I

.field private labelSize:I

.field private labelView:Landroid/widget/TextView;

.field private listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

.field private scale:F

.field private selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

.field private viewContainers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 135
    const/4 v0, -0x1

    sput v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultItemPadding:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 185
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 187
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 190
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 175
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 176
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    .line 180
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    .line 181
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    invoke-direct {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .line 191
    invoke-static {p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->initDefaults(Landroid/content/Context;)V

    .line 193
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030009

    invoke-virtual {v1, v2, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 194
    const v1, 0x7f0e00c5

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->labelView:Landroid/widget/TextView;

    .line 195
    sget-object v1, Lcom/navdy/hud/app/R$styleable;->ChoiceLayout2:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 196
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 197
    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultItemPadding:I

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->itemPadding:I

    .line 198
    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultTopPadding:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->itemTopPadding:I

    .line 199
    const/4 v1, 0x2

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultLabelSize:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->labelSize:I

    .line 200
    const/4 v1, 0x3

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultIconSize:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->iconSize:I

    .line 201
    const/4 v1, 0x4

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultIconHaloSize:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->iconHaloSize:I

    .line 202
    const/4 v1, 0x5

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloStartRadius:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloStartRadius:I

    .line 203
    const/4 v1, 0x6

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloEndRadius:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloEndRadius:I

    .line 204
    const/4 v1, 0x7

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloMiddleRadius:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloMiddleRadius:I

    .line 205
    const/16 v1, 0x8

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloStartDelay:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloStartDelay:I

    .line 206
    const/16 v1, 0x9

    sget v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloDuration:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloDuration:I

    .line 207
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 209
    :cond_0
    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/ChoiceLayout2;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .prologue
    .line 38
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/ChoiceLayout2;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->callClickListener(I)V

    return-void
.end method

.method private buildChoices()V
    .locals 19

    .prologue
    .line 252
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->removeViewContainers()V

    .line 253
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    if-nez v3, :cond_0

    .line 338
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    .line 258
    .local v13, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v16

    .line 259
    .local v16, "len":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move/from16 v0, v16

    if-ge v12, v0, :cond_2

    .line 261
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .line 262
    .local v11, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    new-instance v18, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$1;)V

    .line 263
    .local v18, "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    const v3, 0x7f03000a

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v13, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    .line 264
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setId(I)V

    .line 265
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    const v5, 0x7f0e00c8

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    .line 266
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    const v5, 0x7f0e00d5

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .line 267
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v5, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->inject(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 268
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    const v5, 0x7f0e00d6

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 269
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    const v5, 0x7f0e00d7

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->small:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 270
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    const v5, 0x7f0e00c7

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/HaloView;

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    .line 271
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    move-object/from16 v0, v18

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    new-instance v15, Landroid/support/constraint/ConstraintLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->iconHaloSize:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->iconHaloSize:I

    invoke-direct {v15, v3, v5}, Landroid/support/constraint/ConstraintLayout$LayoutParams;-><init>(II)V

    .line 273
    .local v15, "layoutParams":Landroid/support/constraint/ConstraintLayout$LayoutParams;
    if-eqz v12, :cond_1

    .line 274
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->itemPadding:I

    iput v3, v15, Landroid/support/constraint/ConstraintLayout$LayoutParams;->leftMargin:I

    .line 278
    :cond_1
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 279
    .local v17, "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->iconSize:I

    move-object/from16 v0, v17

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 280
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->iconSize:I

    move-object/from16 v0, v17

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 282
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloStartRadius:I

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/HaloView;->setStartRadius(I)V

    .line 283
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloMiddleRadius:I

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/HaloView;->setMiddleRadius(I)V

    .line 284
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloEndRadius:I

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/HaloView;->setEndRadius(I)V

    .line 285
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloDuration:I

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/HaloView;->setDuration(I)V

    .line 286
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->haloStartDelay:I

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/HaloView;->setStartDelay(I)V

    .line 289
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->big:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdSelected:I
    invoke-static {v11}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$100(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v5

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->selectedColor:I
    invoke-static {v11}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$200(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->scale:F

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 290
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->small:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->resIdUnselected:I
    invoke-static {v11}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$300(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v5

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->unselectedColor:I
    invoke-static {v11}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$400(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->scale:F

    invoke-virtual {v3, v5, v6, v7, v8}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 291
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 292
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->fluctuatorColor:I
    invoke-static {v11}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$500(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/ui/component/HaloView;->setStrokeColor(I)V

    .line 295
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v15}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 259
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 299
    .end local v11    # "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    .end local v15    # "layoutParams":Landroid/support/constraint/ConstraintLayout$LayoutParams;
    .end local v17    # "marginLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v18    # "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->labelView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->labelSize:I

    int-to-float v5, v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextSize(F)V

    .line 302
    new-instance v1, Landroid/support/constraint/ConstraintSet;

    invoke-direct {v1}, Landroid/support/constraint/ConstraintSet;-><init>()V

    .line 303
    .local v1, "constraintSet":Landroid/support/constraint/ConstraintSet;
    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/support/constraint/ConstraintSet;->clone(Landroid/support/constraint/ConstraintLayout;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v16

    .line 305
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getId()I

    move-result v4

    .line 306
    .local v4, "parentId":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->labelView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v14

    .line 307
    .local v14, "labelId":I
    const/4 v12, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v12, v0, :cond_6

    .line 308
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    .line 309
    .restart local v18    # "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v2

    .line 310
    .local v2, "viewId":I
    if-nez v12, :cond_3

    .line 312
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/support/constraint/ConstraintSet;->setHorizontalChainStyle(II)V

    .line 313
    const/4 v3, 0x6

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 315
    :cond_3
    add-int/lit8 v3, v16, -0x1

    if-ne v12, v3, :cond_5

    .line 317
    const/4 v3, 0x7

    const/4 v5, 0x7

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 318
    const/4 v3, 0x1

    move/from16 v0, v16

    if-le v0, v3, :cond_4

    .line 320
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    add-int/lit8 v5, v12, -0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v8

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v5, v1

    move v6, v2

    invoke-virtual/range {v5 .. v10}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 332
    :cond_4
    :goto_3
    const/4 v7, 0x3

    const/4 v9, 0x4

    move-object/from16 v0, p0

    iget v10, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->itemTopPadding:I

    move-object v5, v1

    move v6, v2

    move v8, v14

    invoke-virtual/range {v5 .. v10}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 307
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 324
    :cond_5
    const/4 v3, 0x1

    move/from16 v0, v16

    if-le v0, v3, :cond_4

    .line 325
    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    add-int/lit8 v5, v12, 0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v8

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v5, v1

    move v6, v2

    invoke-virtual/range {v5 .. v10}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 326
    if-eqz v12, :cond_4

    .line 328
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    add-int/lit8 v5, v12, -0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v8

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v5, v1

    move v6, v2

    invoke-virtual/range {v5 .. v10}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    goto :goto_3

    .line 334
    .end local v2    # "viewId":I
    .end local v18    # "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/support/constraint/ConstraintSet;->applyTo(Landroid/support/constraint/ConstraintLayout;)V

    .line 336
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->invalidate()V

    .line 337
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->requestLayout()V

    goto/16 :goto_0
.end method

.method private callClickListener(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 402
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    if-eqz v0, :cond_0

    .line 403
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_SELECT:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 404
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v0

    iput v0, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    .line 405
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    iput p1, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->pos:I

    .line 406
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;->executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V

    .line 408
    :cond_0
    return-void
.end method

.method private callSelectListener(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 411
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    if-eqz v0, :cond_0

    .line 412
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v0}, Lcom/navdy/hud/app/audio/SoundUtils;->playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V

    .line 413
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v0

    iput v0, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    .line 414
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    iput p1, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->pos:I

    .line 415
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;->itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V

    .line 417
    :cond_0
    return-void
.end method

.method private getItemCount()I
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 397
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initDefaults(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    sget v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultItemPadding:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 148
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 149
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b0051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultItemPadding:I

    .line 150
    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultTopPadding:I

    .line 151
    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultLabelSize:I

    .line 152
    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultIconSize:I

    .line 153
    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultIconHaloSize:I

    .line 154
    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloStartRadius:I

    .line 155
    const v1, 0x7f0b004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloEndRadius:I

    .line 156
    const v1, 0x7f0b004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloMiddleRadius:I

    .line 157
    const v1, 0x7f0f0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloStartDelay:I

    .line 158
    const v1, 0x7f0f0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->defaultHaloDuration:I

    .line 160
    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method

.method private isItemInBounds(I)Z
    .locals 1
    .param p1, "item"    # I

    .prologue
    .line 446
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 447
    :cond_0
    const/4 v0, 0x0

    .line 449
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private removeViewContainers()V
    .locals 3

    .prologue
    .line 439
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    .line 440
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->parent:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 442
    .end local v0    # "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 443
    return-void
.end method

.method private resetClickMode()V
    .locals 1

    .prologue
    .line 483
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clickMode:Z

    .line 484
    return-void
.end method

.method private startFluctuator(IZ)V
    .locals 5
    .param p1, "item"    # I
    .param p2, "changeColor"    # Z

    .prologue
    .line 454
    sget-object v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startFluctuator:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 455
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->isItemInBounds(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 466
    :goto_0
    return-void

    .line 458
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    .line 459
    .local v1, "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .line 460
    .local v0, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    if-eqz p2, :cond_1

    .line 461
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 463
    :cond_1
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 464
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/HaloView;->start()V

    .line 465
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->labelView:Landroid/widget/TextView;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->label:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$900(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private stopFluctuator(IZ)V
    .locals 5
    .param p1, "item"    # I
    .param p2, "changeColor"    # Z

    .prologue
    .line 469
    sget-object v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopFluctuator:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 470
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->isItemInBounds(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 480
    :goto_0
    return-void

    .line 473
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .line 474
    .local v0, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    .line 475
    .local v1, "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    if-eqz p2, :cond_1

    .line 476
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    .line 478
    :cond_1
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 479
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->haloView:Lcom/navdy/hud/app/ui/component/HaloView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/HaloView;->stop()V

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 487
    sget-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clear"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 488
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->stopFluctuator(IZ)V

    .line 489
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->resetClickMode()V

    .line 490
    return-void
.end method

.method public executeSelectedItem()V
    .locals 4

    .prologue
    .line 374
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clickMode:Z

    if-eqz v1, :cond_1

    .line 375
    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "select: click mode on"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->isItemInBounds(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clickMode:Z

    .line 383
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->stopFluctuator(IZ)V

    .line 384
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    .line 385
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->iconContainer:Landroid/view/ViewGroup;

    const/16 v2, 0x32

    new-instance v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$1;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout2;)V

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->performClick(Landroid/view/View;ILjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getChoices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    return-object v0
.end method

.method public getCurrentSelectedChoice()Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    .locals 3

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getCurrentSelection()Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    move-result-object v0

    .line 431
    .local v0, "selection":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;
    if-eqz v0, :cond_0

    .line 432
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    iget v2, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->pos:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    .line 434
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentSelection()Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;
    .locals 3

    .prologue
    .line 420
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->isItemInBounds(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->id:I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;->access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;)I

    move-result v0

    iput v0, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    .line 422
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    iput v1, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->pos:I

    .line 423
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->selection:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .line 425
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveSelectionLeft()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 341
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clickMode:Z

    if-eqz v2, :cond_1

    .line 342
    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "left: click mode on"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 353
    :cond_0
    :goto_0
    return v0

    .line 345
    :cond_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    if-eqz v2, :cond_0

    .line 349
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    .line 350
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->stopFluctuator(IZ)V

    .line 351
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->startFluctuator(IZ)V

    .line 352
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->callSelectListener(I)V

    move v0, v1

    .line 353
    goto :goto_0
.end method

.method public moveSelectionRight()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 357
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clickMode:Z

    if-eqz v2, :cond_1

    .line 358
    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "right: click mode on"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 370
    :cond_0
    :goto_0
    return v0

    .line 362
    :cond_1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v2, v3, :cond_0

    .line 366
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    .line 367
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->stopFluctuator(IZ)V

    .line 368
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->startFluctuator(IZ)V

    .line 369
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->callSelectListener(I)V

    move v0, v1

    .line 370
    goto :goto_0
.end method

.method public setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V
    .locals 1
    .param p2, "initialSelection"    # I
    .param p3, "listener"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;I",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 214
    .local p1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 215
    return-void
.end method

.method public setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V
    .locals 4
    .param p2, "initialSelection"    # I
    .param p3, "listener"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;
    .param p4, "scale"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;I",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->resetClickMode()V

    .line 222
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->choices:Ljava/util/List;

    .line 223
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 224
    iput p4, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->scale:F

    .line 225
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 226
    if-ltz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 227
    iput p2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    .line 231
    :goto_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setChoices:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " sel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 232
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->buildChoices()V

    .line 233
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->viewContainers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;

    .line 234
    .local v0, "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;->crossFadeImageView:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V

    goto :goto_1

    .line 229
    .end local v0    # "viewContainer":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$ViewContainer;
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    goto :goto_0

    .line 236
    :cond_1
    iget v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->startFluctuator(IZ)V

    .line 245
    :goto_2
    return-void

    .line 238
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "setChoices:clear"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 239
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->currentSelection:I

    .line 240
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->removeViewContainers()V

    .line 241
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->labelView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->invalidate()V

    .line 243
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->requestLayout()V

    goto :goto_2
.end method
