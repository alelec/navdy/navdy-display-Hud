.class Lcom/navdy/hud/app/ui/component/ChoiceLayout$2;
.super Ljava/lang/Object;
.source "ChoiceLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 204
    .local v1, "margin":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 205
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 206
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    .line 207
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$2;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->highlightView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 208
    return-void
.end method
