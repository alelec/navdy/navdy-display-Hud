.class Lcom/navdy/hud/app/ui/component/CarouselTextView$1;
.super Ljava/lang/Object;
.source "CarouselTextView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/CarouselTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/CarouselTextView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/CarouselTextView;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 33
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    # getter for: Lcom/navdy/hud/app/ui/component/CarouselTextView;->textList:Ljava/util/List;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$000(Lcom/navdy/hud/app/ui/component/CarouselTextView;)Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    # getter for: Lcom/navdy/hud/app/ui/component/CarouselTextView;->textList:Ljava/util/List;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$000(Lcom/navdy/hud/app/ui/component/CarouselTextView;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 39
    .local v0, "size":I
    if-lez v0, :cond_0

    .line 40
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    # getter for: Lcom/navdy/hud/app/ui/component/CarouselTextView;->currentTextIndex:I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$100(Lcom/navdy/hud/app/ui/component/CarouselTextView;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    rem-int/2addr v2, v0

    # setter for: Lcom/navdy/hud/app/ui/component/CarouselTextView;->currentTextIndex:I
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$102(Lcom/navdy/hud/app/ui/component/CarouselTextView;I)I

    .line 41
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    # getter for: Lcom/navdy/hud/app/ui/component/CarouselTextView;->textList:Ljava/util/List;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$000(Lcom/navdy/hud/app/ui/component/CarouselTextView;)Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    # getter for: Lcom/navdy/hud/app/ui/component/CarouselTextView;->currentTextIndex:I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$100(Lcom/navdy/hud/app/ui/component/CarouselTextView;)I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/ui/component/CarouselTextView;->rotateWithAnimation(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$200(Lcom/navdy/hud/app/ui/component/CarouselTextView;Ljava/lang/String;)V

    .line 42
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;->this$0:Lcom/navdy/hud/app/ui/component/CarouselTextView;

    # getter for: Lcom/navdy/hud/app/ui/component/CarouselTextView;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->access$300(Lcom/navdy/hud/app/ui/component/CarouselTextView;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
