.class public Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;
.super Landroid/view/View;
.source "ShrinkingBorderView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;
    }
.end annotation


# static fields
.field private static final FORCE_COLLAPSE_INTERVAL:I = 0x12c

.field private static final RESET_TIMEOUT_INITIAL_INTERVAL:I = 0x12c

.field private static final RESET_TIMEOUT_INTERVAL:I = 0x64


# instance fields
.field private animator:Landroid/animation/ValueAnimator;

.field private animatorListener:Landroid/animation/Animator$AnimatorListener;

.field private animatorResetListener:Landroid/animation/Animator$AnimatorListener;

.field private animatorResetUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private animatorUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private forceCompleteCallback:Ljava/lang/Runnable;

.field private forceCompleteListener:Landroid/animation/Animator$AnimatorListener;

.field private interpolator:Landroid/view/animation/Interpolator;

.field private listener:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;

.field private restoreInterpolator:Landroid/view/animation/Interpolator;

.field private timeoutVal:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->restoreInterpolator:Landroid/view/animation/Interpolator;

    .line 28
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->interpolator:Landroid/view/animation/Interpolator;

    .line 37
    new-instance v0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$1;-><init>(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->forceCompleteListener:Landroid/animation/Animator$AnimatorListener;

    .line 50
    new-instance v0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$2;-><init>(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorListener:Landroid/animation/Animator$AnimatorListener;

    .line 63
    new-instance v0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$3;-><init>(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 79
    new-instance v0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$4;-><init>(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorResetListener:Landroid/animation/Animator$AnimatorListener;

    .line 89
    new-instance v0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$5;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$5;-><init>(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorResetUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 109
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->forceCompleteCallback:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->forceCompleteCallback:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->listener:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;

    .prologue
    .line 18
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->timeoutVal:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->startTimeout(IZ)V

    return-void
.end method

.method private clearAnimator()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 209
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 211
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 212
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    .line 213
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->forceCompleteCallback:Ljava/lang/Runnable;

    .line 215
    :cond_0
    return-void
.end method

.method private startTimeout(IZ)V
    .locals 5
    .param p1, "timeout"    # I
    .param p2, "reset"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 120
    if-nez p1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_0

    .line 128
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->timeoutVal:I

    .line 129
    if-eqz p2, :cond_2

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 131
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v1, 0x2

    new-array v1, v1, [I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    aput v2, v1, v3

    aput v3, v1, v4

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    .line 132
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 133
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 134
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 135
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 136
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 138
    .end local v0    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 139
    .restart local v0    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 140
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->requestLayout()V

    .line 141
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->resetTimeout(Z)V

    goto :goto_0
.end method


# virtual methods
.method public isVisible()Z
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 201
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    if-nez v1, :cond_0

    .line 202
    const/4 v1, 0x0

    .line 204
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public resetTimeout()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->resetTimeout(Z)V

    .line 147
    return-void
.end method

.method public resetTimeout(Z)V
    .locals 6
    .param p1, "initial"    # Z

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->clearAnimator()V

    .line 153
    if-eqz p1, :cond_1

    .line 154
    const/16 v0, 0x12c

    .line 158
    .local v0, "interval":I
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 159
    .local v1, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    .line 160
    .local v2, "parentHeight":I
    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    aput v5, v3, v4

    const/4 v4, 0x1

    aput v2, v3, v4

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    .line 161
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorResetUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 162
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorResetListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 163
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 164
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->restoreInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 165
    if-eqz p1, :cond_0

    .line 166
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 168
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 169
    return-void

    .line 156
    .end local v0    # "interval":I
    .end local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v2    # "parentHeight":I
    :cond_1
    const/16 v0, 0x64

    .restart local v0    # "interval":I
    goto :goto_0
.end method

.method public setListener(Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->listener:Lcom/navdy/hud/app/ui/component/ShrinkingBorderView$IListener;

    .line 113
    return-void
.end method

.method public startTimeout(I)V
    .locals 1
    .param p1, "timeout"    # I

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->startTimeout(IZ)V

    .line 117
    return-void
.end method

.method public stopTimeout(ZLjava/lang/Runnable;)V
    .locals 4
    .param p1, "force"    # Z
    .param p2, "forceCompleteCallback"    # Ljava/lang/Runnable;

    .prologue
    const/4 v3, 0x0

    .line 172
    if-eqz p1, :cond_2

    .line 173
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->clearAnimator()V

    .line 174
    if-eqz p2, :cond_1

    .line 175
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->forceCompleteCallback:Ljava/lang/Runnable;

    .line 176
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 177
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v1, 0x2

    new-array v1, v1, [I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    aput v2, v1, v3

    const/4 v2, 0x1

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    .line 178
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animatorResetUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 179
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->forceCompleteListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 180
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 181
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->interpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 182
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 197
    .end local v0    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 185
    .restart local v0    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 186
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->requestLayout()V

    goto :goto_0

    .line 189
    .end local v0    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->animator:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 194
    .restart local v0    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 195
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/ShrinkingBorderView;->requestLayout()V

    goto :goto_0
.end method
