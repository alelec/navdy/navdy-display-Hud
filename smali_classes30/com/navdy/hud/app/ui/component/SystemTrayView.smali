.class public Lcom/navdy/hud/app/ui/component/SystemTrayView;
.super Landroid/widget/LinearLayout;
.source "SystemTrayView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/SystemTrayView$State;,
        Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    }
.end annotation


# static fields
.field static dialStateResMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/ui/component/SystemTrayView$State;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static phoneStateResMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/ui/component/SystemTrayView$State;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private debugGpsMarker:Landroid/widget/TextView;

.field private dialBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field private dialConnected:Z

.field dialImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0211
    .end annotation
.end field

.field locationImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0210
    .end annotation
.end field

.field private noNetwork:Z

.field private phoneBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

.field private phoneConnected:Z

.field phoneImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0214
    .end annotation
.end field

.field phoneNetworkImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0213
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/SystemTrayView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneStateResMap:Ljava/util/HashMap;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialStateResMap:Ljava/util/HashMap;

    .line 58
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialStateResMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const v2, 0x7f0200fd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialStateResMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const v2, 0x7f0200fe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialStateResMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const v2, 0x7f020100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneStateResMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const v2, 0x7f0200fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneStateResMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const v2, 0x7f0200f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneStateResMap:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    const v2, 0x7f0200fb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 44
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 88
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->init()V

    .line 89
    return-void
.end method

.method private handleDialState(Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V
    .locals 3
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .prologue
    const/4 v0, 0x0

    .line 184
    if-nez p1, :cond_1

    .line 185
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialImageView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$1;->$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 192
    :pswitch_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    .line 193
    .local v0, "connected":Z
    :cond_2
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialConnected:Z

    if-eq v1, v0, :cond_0

    .line 194
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialConnected:Z

    .line 195
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->updateViews()V

    goto :goto_0

    .line 202
    .end local v0    # "connected":Z
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    if-eq v1, p1, :cond_0

    .line 203
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 204
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->updateViews()V

    goto :goto_0

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private handleGpsState(Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V
    .locals 2
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .prologue
    .line 225
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$1;->$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    return-void

    .line 227
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->locationImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setDebugGpsMarker(Ljava/lang/String;)V

    goto :goto_0

    .line 232
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->locationImageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handlePhoneState(Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V
    .locals 5
    .param p1, "state"    # Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 144
    if-nez p1, :cond_1

    .line 145
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 146
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneNetworkImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/ui/component/SystemTrayView$1;->$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 154
    :pswitch_0
    sget-object v3, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    if-ne p1, v3, :cond_2

    .line 155
    .local v0, "connected":Z
    :goto_1
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneConnected:Z

    if-eq v3, v0, :cond_0

    .line 156
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneConnected:Z

    .line 157
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->noNetwork:Z

    .line 158
    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 159
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->updateViews()V

    goto :goto_0

    .end local v0    # "connected":Z
    :cond_2
    move v0, v2

    .line 154
    goto :goto_1

    .line 166
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    if-eq v2, p1, :cond_0

    .line 167
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 168
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->updateViews()V

    goto :goto_0

    .line 174
    :pswitch_2
    sget-object v3, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->NO_PHONE_NETWORK:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    if-ne p1, v3, :cond_3

    move v1, v0

    .line 175
    .local v1, "noNetworkAvailable":Z
    :goto_2
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->noNetwork:Z

    if-eq v2, v1, :cond_0

    .line 176
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->noNetwork:Z

    .line 177
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->updateViews()V

    goto :goto_0

    .end local v1    # "noNetworkAvailable":Z
    :cond_3
    move v1, v2

    .line 174
    goto :goto_2

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private init()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 92
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030079

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setOrientation(I)V

    .line 94
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 96
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneNetworkImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->locationImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    return-void
.end method

.method private updateDeviceStatus(Landroid/widget/ImageView;Ljava/util/HashMap;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V
    .locals 3
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p3, "batteryState"    # Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/ui/component/SystemTrayView$State;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/navdy/hud/app/ui/component/SystemTrayView$State;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "mapping":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/navdy/hud/app/ui/component/SystemTrayView$State;Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 218
    invoke-virtual {p2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 219
    .local v1, "resource":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 220
    .local v0, "resId":I
    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 221
    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 222
    return-void

    .end local v0    # "resId":I
    :cond_0
    move v0, v2

    .line 219
    goto :goto_0

    .line 221
    .restart local v0    # "resId":I
    :cond_1
    const/16 v2, 0x8

    goto :goto_1
.end method

.method private updateViews()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 211
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneNetworkImageView:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->noNetwork:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneConnected:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 212
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneStateResMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->noNetwork:Z

    if-eqz v3, :cond_0

    const v1, 0x7f0200f8

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneImageView:Landroid/widget/ImageView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneStateResMap:Ljava/util/HashMap;

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneConnected:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    :goto_1
    invoke-direct {p0, v1, v2, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->updateDeviceStatus(Landroid/widget/ImageView;Ljava/util/HashMap;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 214
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialImageView:Landroid/widget/ImageView;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialStateResMap:Ljava/util/HashMap;

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialConnected:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialBattery:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    :goto_2
    invoke-direct {p0, v1, v2, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->updateDeviceStatus(Landroid/widget/ImageView;Ljava/util/HashMap;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 215
    return-void

    .line 211
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 213
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    goto :goto_1

    .line 214
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    goto :goto_2
.end method


# virtual methods
.method public setDebugGpsMarker(Ljava/lang/String;)V
    .locals 5
    .param p1, "marker"    # Ljava/lang/String;

    .prologue
    .line 120
    if-nez p1, :cond_1

    .line 122
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 123
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->removeView(Landroid/view/View;)V

    .line 124
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    if-nez v3, :cond_2

    .line 128
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 129
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    .line 130
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    const v4, 0x7f0c001b

    invoke-virtual {v3, v0, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 131
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 133
    .local v1, "lytParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v3, 0x10

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 134
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v1}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 136
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "lytParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->debugGpsMarker:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 138
    :catch_0
    move-exception v2

    .line 139
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/ui/component/SystemTrayView;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V
    .locals 2
    .param p1, "device"    # Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    .param p2, "state"    # Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .prologue
    .line 103
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$1;->$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$Device:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    return-void

    .line 105
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->handlePhoneState(Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_0

    .line 109
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->handleDialState(Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_0

    .line 113
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->handleGpsState(Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
