.class Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;
.super Ljava/lang/Object;
.source "ChoiceLayout.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 281
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->choiceContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 282
    .local v0, "view":Landroid/view/View;
    if-nez v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 285
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 286
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$5;->this$0:Lcom/navdy/hud/app/ui/component/ChoiceLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectedItem:I
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$800(Lcom/navdy/hud/app/ui/component/ChoiceLayout;)I

    move-result v2

    # invokes: Lcom/navdy/hud/app/ui/component/ChoiceLayout;->selectInitialItem(I)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;->access$1200(Lcom/navdy/hud/app/ui/component/ChoiceLayout;I)V

    goto :goto_0
.end method
