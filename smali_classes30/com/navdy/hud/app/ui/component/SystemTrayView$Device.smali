.class public final enum Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
.super Ljava/lang/Enum;
.source "SystemTrayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/SystemTrayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Device"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

.field public static final enum Dial:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

.field public static final enum Gps:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

.field public static final enum Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    const-string v1, "Phone"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    .line 50
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    const-string v1, "Dial"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Dial:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    .line 51
    new-instance v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    const-string v1, "Gps"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Gps:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    .line 48
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Dial:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Gps:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->$VALUES:[Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->$VALUES:[Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    return-object v0
.end method
