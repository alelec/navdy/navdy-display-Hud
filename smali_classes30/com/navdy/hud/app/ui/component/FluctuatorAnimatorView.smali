.class public Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;
.super Landroid/view/View;
.source "FluctuatorAnimatorView.java"


# instance fields
.field private alphaAnimator:Landroid/animation/ValueAnimator;

.field private animationDelay:I

.field private animationDuration:I

.field private animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private animatorSet:Landroid/animation/AnimatorSet;

.field currentCircle:F

.field private endRadius:F

.field private fillColor:I

.field private fillEnabled:Z

.field private handler:Landroid/os/Handler;

.field private interpolator:Landroid/view/animation/LinearInterpolator;

.field private paint:Landroid/graphics/Paint;

.field private radiusAnimator:Landroid/animation/ValueAnimator;

.field private startRadius:F

.field public startRunnable:Ljava/lang/Runnable;

.field started:Z

.field private strokeColor:I

.field private strokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v7, -0x1

    const/4 v3, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->interpolator:Landroid/view/animation/LinearInterpolator;

    .line 39
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->handler:Landroid/os/Handler;

    .line 41
    new-instance v1, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$1;-><init>(Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 48
    new-instance v1, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$2;-><init>(Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->startRunnable:Ljava/lang/Runnable;

    .line 55
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->started:Z

    .line 67
    sget-object v1, Lcom/navdy/hud/app/R$styleable;->FluctuatorAnimatorView:[I

    invoke-virtual {p1, p2, v1, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->strokeColor:I

    .line 70
    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->fillColor:I

    .line 71
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->startRadius:F

    .line 72
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->endRadius:F

    .line 73
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->strokeWidth:F

    .line 74
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animationDuration:I

    .line 75
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animationDelay:I

    .line 76
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 78
    :cond_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    .line 79
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 80
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 82
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    .line 83
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animationDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 84
    new-array v1, v6, [F

    iget v2, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->startRadius:F

    aput v2, v1, v4

    iget v2, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->endRadius:F

    aput v2, v1, v5

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->radiusAnimator:Landroid/animation/ValueAnimator;

    .line 85
    new-array v1, v6, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->alphaAnimator:Landroid/animation/ValueAnimator;

    .line 86
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->interpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 87
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->radiusAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$3;-><init>(Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 95
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->alphaAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView$4;-><init>(Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 102
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    new-array v2, v6, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->radiusAnimator:Landroid/animation/ValueAnimator;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->alphaAnimator:Landroid/animation/ValueAnimator;

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 103
    return-void

    .line 85
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animationDelay:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    return-object v0
.end method


# virtual methods
.method public isStarted()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->started:Z

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 108
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 109
    iget v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->fillColor:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->fillEnabled:Z

    if-eqz v0, :cond_1

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->fillColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->currentCircle:F

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->strokeColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->currentCircle:F

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 117
    return-void
.end method

.method public setFillColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->fillColor:I

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->fillEnabled:Z

    .line 138
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->strokeColor:I

    .line 142
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->started:Z

    .line 124
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->startRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 132
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->started:Z

    .line 133
    return-void
.end method
