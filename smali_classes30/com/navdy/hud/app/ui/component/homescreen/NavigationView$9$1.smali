.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->onMapTransformEnd(Lcom/here/android/mpa/mapping/MapState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    .prologue
    .line 1317
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1320
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$400(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showMapIconIndicatorRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$900(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1321
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1322
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 1325
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentMapRoute()Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v1

    .line 1326
    .local v1, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$1000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isTrafficEnabled()Z

    move-result v0

    .line 1327
    .local v0, "isTrafficEnabled":Z
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "animateBackfromOverview traffic="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1338
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 1340
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->val$endAction:Ljava/lang/Runnable;

    if-eqz v2, :cond_1

    .line 1341
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->val$endAction:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 1343
    :cond_1
    return-void
.end method
