.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Lcom/here/android/mpa/mapping/Map$OnTransformListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;

    .prologue
    .line 472
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapTransformEnd(Lcom/here/android/mpa/mapping/MapState;)V
    .locals 2
    .param p1, "mapState"    # Lcom/here/android/mpa/mapping/MapState;

    .prologue
    .line 478
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "switchToOverviewMode:no route  tranform end"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 480
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$400(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$endAction:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 481
    return-void
.end method

.method public onMapTransformStart()V
    .locals 0

    .prologue
    .line 474
    return-void
.end method
