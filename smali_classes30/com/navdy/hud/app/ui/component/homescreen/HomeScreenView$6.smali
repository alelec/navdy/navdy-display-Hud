.class Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "HomeScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getExpandAnimator()Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 849
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 874
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 852
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    .line 853
    .local v0, "isNavigationActive":Z
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating:Z
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$400(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 854
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 855
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v1, v2, :cond_0

    .line 856
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 857
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 867
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v1, v2, :cond_1

    .line 868
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 869
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 871
    :cond_1
    return-void

    .line 860
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v1, v2, :cond_0

    .line 861
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 862
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    goto :goto_0
.end method
