.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 873
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 876
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "cleanupMapOverview"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 877
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 878
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 881
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$700(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 882
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$700(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 885
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverviewRoutes()V

    .line 886
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupFluctuator()V

    .line 887
    return-void
.end method
