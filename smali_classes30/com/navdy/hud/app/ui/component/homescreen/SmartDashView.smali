.class public Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
.super Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;
.source "SmartDashView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/dashboard/IDashboardOptionsAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;
    }
.end annotation


# static fields
.field public static final HEADING_EXPIRE_INTERVAL:I = 0xbb8

.field public static final MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE:J

.field public static final NAVIGATION_MODE_SCALE_FACTOR:F = 0.8f

.field private static final SHOW_ETA:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private activeModeGaugeMargin:I

.field private activeRightGaugeId:Ljava/lang/String;

.field private activeleftGaugeId:Ljava/lang/String;

.field etaAmPm:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01b2
    .end annotation
.end field

.field etaText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01b1
    .end annotation
.end field

.field private fullModeGaugeTopMargin:I

.field private globalPreferences:Landroid/content/SharedPreferences;

.field private headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

.field public isShowingDriveScoreGauge:Z

.field private lastETA:Ljava/lang/String;

.field private lastETA_AmPm:Ljava/lang/String;

.field private lastHeading:D

.field private lastHeadingSampleTime:J

.field private lastUserPreferenceRecordedTime:J

.field mEtaLayout:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01b0
    .end annotation
.end field

.field mLeftAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

.field mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ac
    .end annotation
.end field

.field mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ae
    .end annotation
.end field

.field mRightAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

.field mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01af
    .end annotation
.end field

.field private mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

.field private middleGauge:I

.field private middleGaugeShrinkEarlyTbtOffset:I

.field private middleGaugeShrinkMargin:I

.field private middleGaugeShrinkModeMargin:I

.field private paused:Z

.field private scrollableSide:I

.field speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

.field tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 58
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 62
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 114
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;-><init>(Landroid/content/Context;)V

    .line 94
    iput-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastUserPreferenceRecordedTime:J

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastHeading:D

    .line 103
    iput-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastHeadingSampleTime:J

    .line 109
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    .line 110
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    .line 111
    new-instance v0, Lcom/navdy/hud/app/util/HeadingDataUtil;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/HeadingDataUtil;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    .line 115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 118
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 94
    iput-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastUserPreferenceRecordedTime:J

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastHeading:D

    .line 103
    iput-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastHeadingSampleTime:J

    .line 109
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    .line 110
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    .line 111
    new-instance v0, Lcom/navdy/hud/app/util/HeadingDataUtil;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/HeadingDataUtil;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    .line 119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 122
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    iput-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastUserPreferenceRecordedTime:J

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastHeading:D

    .line 103
    iput-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastHeadingSampleTime:J

    .line 109
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    .line 110
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    .line 111
    new-instance v0, Lcom/navdy/hud/app/util/HeadingDataUtil;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/HeadingDataUtil;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    .line 123
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/util/HeadingDataUtil;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA_AmPm:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA_AmPm:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showViewsAccordingToUserPreferences()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeleftGaugeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeleftGaugeId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeRightGaugeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeRightGaugeId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showEta(Z)V

    return-void
.end method

.method private savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 499
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v0

    .line 500
    .local v0, "isDefaultProfile":Z
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDriverPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 501
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    if-nez v0, :cond_0

    .line 502
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 503
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, p1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 508
    :cond_0
    :goto_0
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 509
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->globalPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-interface {v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 513
    :cond_1
    :goto_1
    return-void

    .line 504
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_2
    instance-of v2, p2, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 505
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    move-object v2, p2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v3, p1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 510
    :cond_3
    instance-of v2, p2, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 511
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->globalPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method

.method private showEta(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 816
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mEtaLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mEtaLayout:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 820
    :cond_0
    return-void
.end method

.method private showMiddleGaugeAccordingToUserPreferences()V
  .registers 4
  .prologue
  .line 442
    invoke-static { }, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;
    move-result-object v0
    invoke-virtual { v0 }, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;
    move-result-object v0
    invoke-virtual { v0 }, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z
    move-result v0
    if-eqz v0, :L2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->globalPreferences:Landroid/content/SharedPreferences;
  :L0
    const-string v1, "PREFERENCE_MIDDLE_GAUGE"
    const/4 v2, 1
    invoke-interface { v0, v1, v2 }, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    move-result v0
    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I
  .line 443
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;
    new-instance v1, Ljava/lang/StringBuilder;
    invoke-direct { v1 }, Ljava/lang/StringBuilder;-><init>()V
    const-string v2, "Middle Gauge : "
    invoke-virtual { v1, v2 }, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    move-result-object v1
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I
    invoke-virtual { v1, v2 }, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    move-result-object v1
    invoke-virtual { v1 }, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    move-result-object v1
    invoke-virtual { v0, v1 }, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
  .line 444
    invoke-static { }, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;
  .line 445
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I
    packed-switch v0, :L5
  :L1
  .line 453
    return-void
  :L2
  .line 442
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual { v0 }, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDriverPreferences()Landroid/content/SharedPreferences;
    move-result-object v0
    goto :L0
  :L3
  .line 447
    invoke-direct { p0 }, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showTachometer()V
    goto :L1
  :L4
  .line 450
    invoke-direct { p0 }, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showSpeedometerGauge()V
    goto :L1
  .line 445
  :L5
  .packed-switch 0
    :L3
    :L4
  .end packed-switch
.end method


.method private showSpeedometerGauge()V
    .locals 4

    .prologue
    .line 400
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->onPause()V

    .line 401
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setWidgetVisibleToUser(Z)V

    .line 402
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 403
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->paused:Z

    if-nez v2, :cond_0

    .line 404
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->onResume()V

    .line 406
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setWidgetVisibleToUser(Z)V

    .line 407
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 408
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    .line 409
    .local v1, "speedManager":Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v0

    .line 410
    .local v0, "speed":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setSpeed(I)V

    .line 412
    return-void
.end method

.method private showTachometer()V
    .locals 6

    .prologue
    .line 379
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->onPause()V

    .line 380
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setWidgetVisibleToUser(Z)V

    .line 381
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 382
    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->paused:Z

    if-nez v4, :cond_0

    .line 383
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->onResume()V

    .line 385
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setWidgetVisibleToUser(Z)V

    .line 386
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 387
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    .line 388
    .local v1, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getEngineRpm()I

    move-result v0

    .line 389
    .local v0, "engineRpm":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v4, :cond_1

    .line 390
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setRPM(I)V

    .line 392
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v3

    .line 393
    .local v3, "speedManager":Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v2

    .line 394
    .local v2, "speed":I
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v4, :cond_2

    .line 395
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v4, v2}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setSpeed(I)V

    .line 397
    :cond_2
    return-void
.end method

.method private showViewsAccordingToUserPreferences()V
    .locals 9

    .prologue
    .line 339
    sget-object v6, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "showViewsAccordingToUserPreferences"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 340
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v0

    .line 341
    .local v0, "isDefaultProfile":Z
    if-eqz v0, :cond_3

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->globalPreferences:Landroid/content/SharedPreferences;

    .line 342
    .local v5, "sharedPreferences":Landroid/content/SharedPreferences;
    :goto_0
    const-string v6, "PREFERENCE_SCROLLABLE_SIDE"

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    .line 343
    sget-object v6, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "scrollableSide:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 344
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showMiddleGaugeAccordingToUserPreferences()V

    .line 346
    const-string v6, "PREFERENCE_LEFT_GAUGE"

    const-string v7, "ANALOG_CLOCK_WIDGET"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 347
    .local v1, "leftGaugeId":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v6, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->getIndexForWidgetIdentifier(Ljava/lang/String;)I

    move-result v2

    .line 348
    .local v2, "leftGaugeIndex":I
    sget-object v6, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Left Gauge ID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Index : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 349
    if-gez v2, :cond_0

    .line 351
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v7, "EMPTY_WIDGET"

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->getIndexForWidgetIdentifier(Ljava/lang/String;)I

    move-result v2

    .line 353
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v6, v2}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setSelectedPosition(I)V

    .line 354
    const-string v6, "PREFERENCE_RIGHT_GAUGE"

    const-string v7, "COMPASS_WIDGET"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 355
    .local v3, "rightGaugeId":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v6, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->getIndexForWidgetIdentifier(Ljava/lang/String;)I

    move-result v4

    .line 356
    .local v4, "rightGaugeIndex":I
    sget-object v6, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Right Gauge ID : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Index : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 357
    if-gez v4, :cond_1

    .line 359
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v7, "EMPTY_WIDGET"

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->getIndexForWidgetIdentifier(Ljava/lang/String;)I

    move-result v4

    .line 361
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v6, v4}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setSelectedPosition(I)V

    .line 362
    if-nez v0, :cond_2

    .line 364
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->globalPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "PREFERENCE_MIDDLE_GAUGE"

    iget v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    .line 365
    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "PREFERENCE_SCROLLABLE_SIDE"

    iget v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    .line 366
    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "PREFERENCE_LEFT_GAUGE"

    .line 367
    invoke-interface {v6, v7, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "PREFERENCE_RIGHT_GAUGE"

    .line 368
    invoke-interface {v6, v7, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 369
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 373
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    invoke-virtual {v6, v4}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->setExcludedPosition(I)V

    .line 374
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    invoke-virtual {v6, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->setExcludedPosition(I)V

    .line 375
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->recordUsersWidgetPreference()V

    .line 376
    return-void

    .line 341
    .end local v1    # "leftGaugeId":Ljava/lang/String;
    .end local v2    # "leftGaugeIndex":I
    .end local v3    # "rightGaugeId":Ljava/lang/String;
    .end local v4    # "rightGaugeIndex":I
    .end local v5    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_3
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDriverPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    goto/16 :goto_0
.end method


# virtual methods
.method public ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;

    .prologue
    .line 623
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V

    .line 624
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 643
    :cond_0
    :goto_0
    return-void

    .line 626
    :sswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    iget-object v1, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v1}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setRPM(I)V

    goto :goto_0

    .line 631
    :sswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "FUEL_GAUGE_ID"

    iget-object v2, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 634
    :sswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "MPG_AVG_WIDGET"

    iget-object v2, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 637
    :sswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "ENGINE_TEMPERATURE_GAUGE_ID"

    iget-object v2, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v2}, Lcom/navdy/obd/Pid;->getValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 624
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_3
        0xc -> :sswitch_0
        0x2f -> :sswitch_1
        0x100 -> :sswitch_2
    .end sparse-switch
.end method

.method public adjustDashHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 684
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->adjustDashHeight(I)V

    .line 685
    return-void
.end method

.method protected animateToFullMode()V
    .locals 0

    .prologue
    .line 712
    invoke-super {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->animateToFullMode()V

    .line 713
    return-void
.end method

.method protected animateToFullModeInternal(Landroid/animation/AnimatorSet$Builder;)V
    .locals 0
    .param p1, "builder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 717
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->animateToFullModeInternal(Landroid/animation/AnimatorSet$Builder;)V

    .line 718
    return-void
.end method

.method public getActiveRightGaugeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeRightGaugeId:Ljava/lang/String;

    return-object v0
.end method

.method public getActiveleftGaugeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 878
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeleftGaugeId:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentScrollableSideOption()I
    .locals 1

    .prologue
    .line 479
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 12
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 749
    sget-object v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getCustomAnimator"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 750
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 751
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 752
    .local v1, "set":Landroid/animation/AnimatorSet;
    sget-object v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 809
    :goto_0
    invoke-virtual {p2, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 810
    return-void

    .line 754
    :pswitch_0
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/view/GaugeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 755
    .local v0, "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 756
    .local v2, "srcMargin":I
    const/4 v3, 0x0

    .line 757
    .local v3, "targetMargin":I
    new-instance v4, Landroid/animation/ValueAnimator;

    invoke-direct {v4}, Landroid/animation/ValueAnimator;-><init>()V

    .line 758
    .local v4, "valueAnimator":Landroid/animation/ValueAnimator;
    new-array v5, v10, [I

    aput v2, v5, v8

    aput v3, v5, v9

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 759
    new-instance v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;

    invoke-direct {v5, p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 766
    new-instance v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 777
    new-array v5, v9, [Landroid/animation/Animator;

    aput-object v4, v5, v8

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_0

    .line 783
    .end local v0    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v2    # "srcMargin":I
    .end local v3    # "targetMargin":I
    .end local v4    # "valueAnimator":Landroid/animation/ValueAnimator;
    :pswitch_1
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v5, v11}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 784
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v5, v11}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 786
    invoke-direct {p0, v8}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showEta(Z)V

    .line 787
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/view/GaugeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 788
    .restart local v0    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 790
    .restart local v2    # "srcMargin":I
    sget-object v5, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    if-ne p1, v5, :cond_0

    .line 791
    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGaugeShrinkMargin:I

    .line 796
    .restart local v3    # "targetMargin":I
    :goto_1
    new-instance v4, Landroid/animation/ValueAnimator;

    invoke-direct {v4}, Landroid/animation/ValueAnimator;-><init>()V

    .line 797
    .restart local v4    # "valueAnimator":Landroid/animation/ValueAnimator;
    new-array v5, v10, [I

    aput v2, v5, v8

    aput v3, v5, v9

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 798
    new-instance v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$7;

    invoke-direct {v5, p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$7;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 805
    new-array v5, v9, [Landroid/animation/Animator;

    aput-object v4, v5, v8

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_0

    .line 793
    .end local v3    # "targetMargin":I
    .end local v4    # "valueAnimator":Landroid/animation/ValueAnimator;
    :cond_0
    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGaugeShrinkModeMargin:I

    .restart local v3    # "targetMargin":I
    goto :goto_1

    .line 752
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getMiddleGaugeOption()I
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 5
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 134
    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->globalPreferences:Landroid/content/SharedPreferences;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->globalPreferences:Landroid/content/SharedPreferences;

    .line 135
    new-instance v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->globalPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;-><init>(Landroid/content/SharedPreferences;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    .line 136
    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDriverPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 137
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    new-instance v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$1;

    invoke-direct {v3, p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->setFilter(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;)V

    .line 174
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v2, p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->registerForChanges(Ljava/lang/Object;)V

    .line 175
    new-instance v2, Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    .line 176
    new-instance v2, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    .line 180
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;

    invoke-direct {v3, p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 297
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 298
    .local v0, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0b00d8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGaugeShrinkMargin:I

    .line 299
    const v2, 0x7f0b00d9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGaugeShrinkModeMargin:I

    .line 300
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGaugeShrinkEarlyTbtOffset:I

    .line 301
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 302
    return-void
.end method

.method protected initializeView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 416
    invoke-super {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->initializeView()V

    .line 417
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->reLoadAvailableWidgets(Z)V

    .line 418
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    if-nez v0, :cond_0

    .line 419
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    .line 420
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setAdapter(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;)V

    .line 421
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$3;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setChangeListener(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;)V

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    if-nez v0, :cond_1

    .line 433
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-direct {v0, p0, v1, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    .line 434
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setAdapter(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;)V

    .line 435
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setChangeListener(Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;)V

    .line 446
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showViewsAccordingToUserPreferences()V

    .line 447
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 127
    invoke-super {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onFinishInflate()V

    .line 128
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->fullModeGaugeTopMargin:I

    .line 129
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeModeGaugeMargin:I

    .line 130
    return-void
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 609
    iget v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .line 610
    .local v0, "gaugeViewPager":Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
    :goto_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 618
    :goto_1
    const/4 v1, 0x0

    return v1

    .line 609
    .end local v0    # "gaugeViewPager":Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    goto :goto_0

    .line 612
    .restart local v0    # "gaugeViewPager":Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;
    :pswitch_0
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->moveNext()V

    goto :goto_1

    .line 615
    :pswitch_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->movePrevious()V

    goto :goto_1

    .line 610
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 824
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->paused:Z

    if-eqz v0, :cond_1

    .line 839
    :cond_0
    :goto_0
    return-void

    .line 827
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 828
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->paused:Z

    .line 829
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    if-eqz v0, :cond_0

    .line 830
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "widgets disabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 831
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->onPause()V

    .line 832
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v0, :cond_2

    .line 833
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->onPause()V

    .line 835
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    if-eqz v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->onPause()V

    goto :goto_0
.end method

.method public onReload(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;)V
    .locals 1
    .param p1, "reload"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 306
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOADED:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    if-ne p1, v0, :cond_0

    .line 307
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showViewsAccordingToUserPreferences()V

    .line 309
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 843
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->paused:Z

    if-nez v0, :cond_0

    .line 859
    :goto_0
    return-void

    .line 846
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 847
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->paused:Z

    .line 848
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    if-eqz v0, :cond_2

    .line 849
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "widgets enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->onResume()V

    .line 851
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->isWidgetActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 852
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->onResume()V

    .line 854
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->isWidgetActive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 855
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->onResume()V

    .line 858
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->recordUsersWidgetPreference()V

    goto :goto_0
.end method

.method public onScrollableSideOptionSelected()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 485
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    packed-switch v0, :pswitch_data_0

    .line 495
    :goto_0
    return-void

    .line 487
    :pswitch_0
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    .line 488
    const-string v0, "PREFERENCE_SCROLLABLE_SIDE"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 491
    :pswitch_1
    iput v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->scrollableSide:I

    .line 492
    const-string v0, "PREFERENCE_SCROLLABLE_SIDE"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 485
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSpeedoMeterSelected()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 470
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    if-eq v0, v1, :cond_0

    .line 471
    iput v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    .line 472
    const-string v0, "PREFERENCE_MIDDLE_GAUGE"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 473
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showMiddleGaugeAccordingToUserPreferences()V

    .line 475
    :cond_0
    return-void
.end method

.method public onTachoMeterSelected()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 461
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    if-eqz v0, :cond_0

    .line 462
    iput v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->middleGauge:I

    .line 463
    const-string v0, "PREFERENCE_MIDDLE_GAUGE"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 464
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showMiddleGaugeAccordingToUserPreferences()V

    .line 466
    :cond_0
    return-void
.end method

.method public recordUsersWidgetPreference()V
    .locals 10

    .prologue
    .line 862
    sget-object v6, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Recording the users Widget Preference"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 863
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 864
    .local v0, "currentTime":J
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v2

    .line 865
    .local v2, "isDefaultProfile":Z
    if-nez v2, :cond_0

    iget-wide v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastUserPreferenceRecordedTime:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    iget-wide v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastUserPreferenceRecordedTime:J

    sub-long v6, v0, v6

    sget-wide v8, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_1

    .line 874
    :cond_0
    :goto_0
    return-void

    .line 868
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDriverPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 869
    .local v5, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v6, "PREFERENCE_LEFT_GAUGE"

    const-string v7, "ANALOG_CLOCK_WIDGET"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 870
    .local v3, "leftGaugeId":Ljava/lang/String;
    const-string v6, "PREFERENCE_RIGHT_GAUGE"

    const-string v7, "COMPASS_WIDGET"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 871
    .local v4, "rightGaugeId":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-static {v3, v6}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordDashPreference(Ljava/lang/String;Z)V

    .line 872
    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordDashPreference(Ljava/lang/String;Z)V

    .line 873
    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastUserPreferenceRecordedTime:J

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 7
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 722
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setview: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 723
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 724
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 745
    :goto_0
    return-void

    .line 726
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 727
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 728
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v1

    .line 729
    .local v1, "navigationActive":Z
    if-eqz v1, :cond_0

    .line 730
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showEta(Z)V

    .line 732
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GaugeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 733
    .local v0, "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_0

    .line 737
    .end local v0    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "navigationActive":Z
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 738
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 739
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showEta(Z)V

    .line 740
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/GaugeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 741
    .restart local v0    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00d8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_0

    .line 724
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public shouldShowTopViews()Z
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x0

    return v0
.end method

.method public updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;Z)V
    .locals 8
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;
    .param p2, "forced"    # Z

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const v6, 0x3f4ccccd    # 0.8f

    .line 648
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v1

    .line 649
    .local v1, "navigationActive":Z
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateLayoutForMode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " forced="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " nav:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 650
    if-eqz v1, :cond_0

    .line 651
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "updateLayoutForMode: active"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 652
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 653
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeModeGaugeMargin:I

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 654
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .end local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 655
    .restart local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeModeGaugeMargin:I

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 656
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/GaugeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 657
    .local v0, "middleGaugeParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b016f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 658
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/view/GaugeView;->setScaleX(F)V

    .line 659
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/view/GaugeView;->setScaleY(F)V

    .line 660
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mEtaLayout:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 661
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->invalidate()V

    .line 662
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->requestLayout()V

    .line 677
    :goto_0
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showEta(Z)V

    .line 678
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->invalidate()V

    .line 679
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->requestLayout()V

    .line 680
    return-void

    .line 664
    .end local v0    # "middleGaugeParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "updateLayoutForMode: not active"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 665
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 666
    .restart local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->fullModeGaugeTopMargin:I

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 667
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .end local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 668
    .restart local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->fullModeGaugeTopMargin:I

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 669
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/GaugeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 670
    .restart local v0    # "middleGaugeParams":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0163

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 671
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/view/GaugeView;->setScaleX(F)V

    .line 672
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    invoke-virtual {v3, v7}, Lcom/navdy/hud/app/view/GaugeView;->setScaleY(F)V

    .line 673
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mEtaLayout:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 674
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->invalidate()V

    .line 675
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->requestLayout()V

    goto :goto_0
.end method

.method protected updateSpeed(I)V
    .locals 1
    .param p1, "speed"    # I

    .prologue
    .line 689
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(I)V

    .line 690
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setSpeed(I)V

    .line 693
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    if-eqz v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setSpeed(I)V

    .line 696
    :cond_1
    return-void
.end method

.method protected updateSpeedLimit(I)V
    .locals 3
    .param p1, "speedLimit"    # I

    .prologue
    .line 700
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeedLimit(I)V

    .line 701
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setSpeedLimit(I)V

    .line 704
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    if-eqz v0, :cond_1

    .line 705
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->speedometerGaugePresenter2:Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/SpeedometerGaugePresenter2;->setSpeedLimit(I)V

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "SPEED_LIMIT_SIGN_GAUGE_ID"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    .line 708
    return-void
.end method
