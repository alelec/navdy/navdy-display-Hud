.class Lcom/navdy/hud/app/ui/component/homescreen/TimeView$1;
.super Landroid/content/BroadcastReceiver;
.source "TimeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$1;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 77
    const-string v0, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$1;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "timezone change broadcast"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$1;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->access$100(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/common/TimeHelper;->updateLocale()V

    .line 81
    :cond_0
    return-void
.end method
