.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->positionFluctuator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 1384
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 1387
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/mapping/MapMarker;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 1388
    .local v0, "latlng":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->projectToPixel(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/Map$PixelResult;

    move-result-object v2

    .line 1389
    .local v2, "result":Lcom/here/android/mpa/mapping/Map$PixelResult;
    if-eqz v2, :cond_0

    .line 1390
    invoke-virtual {v2}, Lcom/here/android/mpa/mapping/Map$PixelResult;->getResult()Landroid/graphics/PointF;

    move-result-object v1

    .line 1391
    .local v1, "pointF":Landroid/graphics/PointF;
    if-eqz v1, :cond_0

    .line 1392
    iget v5, v1, Landroid/graphics/PointF;->x:F

    float-to-int v5, v5

    sget v6, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->startFluctuatorDimension:I

    div-int/lit8 v6, v6, 0x2

    sub-int v3, v5, v6

    .line 1393
    .local v3, "x":I
    iget v5, v1, Landroid/graphics/PointF;->y:F

    float-to-int v5, v5

    sget v6, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->startFluctuatorDimension:I

    div-int/lit8 v6, v6, 0x2

    sub-int v4, v5, v6

    .line 1394
    .local v4, "y":I
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$400(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Landroid/os/Handler;

    move-result-object v5

    new-instance v6, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;

    invoke-direct {v6, p0, v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;II)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1410
    .end local v1    # "pointF":Landroid/graphics/PointF;
    .end local v3    # "x":I
    .end local v4    # "y":I
    :cond_0
    return-void
.end method
