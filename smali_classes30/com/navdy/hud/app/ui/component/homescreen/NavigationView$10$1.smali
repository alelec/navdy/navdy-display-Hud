.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

.field final synthetic val$x:I

.field final synthetic val$y:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;II)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    .prologue
    .line 1394
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    iput p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->val$x:I

    iput p3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->val$y:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1397
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v0

    .line 1398
    .local v0, "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v0, v1, :cond_0

    .line 1399
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startDestinationFluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->val$x:I

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterMoveX:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setX(F)V

    .line 1400
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startDestinationFluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->val$y:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setY(F)V

    .line 1401
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startDestinationFluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 1402
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startDestinationFluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->start()V

    .line 1406
    :goto_0
    return-void

    .line 1404
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10$1;->this$1:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fluctuator state from route search mode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
