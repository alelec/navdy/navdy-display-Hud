.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$$ViewInjector;
.super Ljava/lang/Object;
.source "HomeScreenView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e018e

    const-string v2, "field \'mapContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .line 12
    const v1, 0x7f0e01ab

    const-string v2, "field \'smartDashContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    .line 14
    const v1, 0x7f0e019b

    const-string v2, "field \'openMapRoadInfoContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    .line 16
    const v1, 0x7f0e01b7

    const-string v2, "field \'tbtView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    .line 18
    const v1, 0x7f0e01a7

    const-string v2, "field \'recalcRouteContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    .line 20
    const v1, 0x7f0e01c7

    const-string v2, "field \'timeContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .line 22
    const v1, 0x7f0e0188

    const-string v2, "field \'activeEtaContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    .line 24
    const v1, 0x7f0e0186

    const-string v2, "field \'navigationViewsContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/RelativeLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->navigationViewsContainer:Landroid/widget/RelativeLayout;

    .line 26
    const v1, 0x7f0e018d

    const-string v2, "field \'laneGuidanceView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    .line 28
    const v1, 0x7f0e0187

    const-string v2, "field \'laneGuidanceIconIndicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceIconIndicator:Landroid/widget/ImageView;

    .line 30
    const v1, 0x7f0e0182

    const-string v2, "field \'mainscreenRightSection\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    .line 32
    const v1, 0x7f0e0183

    const-string v2, "field \'mapViewSpeedContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    .line 34
    const v1, 0x7f0e0195

    const-string v2, "field \'speedView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 35
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    .line 36
    const v1, 0x7f0e0184

    const-string v2, "field \'speedLimitSignView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 37
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/SpeedLimitSignView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    .line 38
    const v1, 0x7f0e0185

    const-string v2, "field \'dashboardWidgetView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/DashboardWidgetView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->dashboardWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    .line 40
    const v1, 0x7f0e00b5

    const-string v2, "field \'mapMask\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapMask:Landroid/widget/ImageView;

    .line 42
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .line 46
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    .line 47
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    .line 48
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    .line 49
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    .line 50
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .line 51
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    .line 52
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->navigationViewsContainer:Landroid/widget/RelativeLayout;

    .line 53
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    .line 54
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceIconIndicator:Landroid/widget/ImageView;

    .line 55
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    .line 56
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    .line 57
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    .line 58
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    .line 59
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->dashboardWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    .line 60
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapMask:Landroid/widget/ImageView;

    .line 61
    return-void
.end method
