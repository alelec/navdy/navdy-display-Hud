.class public Lcom/navdy/hud/app/ui/component/homescreen/EtaView;
.super Landroid/widget/RelativeLayout;
.source "EtaView.java"


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field etaTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e018a
    .end annotation
.end field

.field etaTimeAmPm:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e018b
    .end annotation
.end field

.field etaTimeLeft:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e018c
    .end annotation
.end field

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private lastETA:Ljava/lang/String;

.field private lastETADate:Ljava/lang/String;

.field private lastETA_AmPm:Ljava/lang/String;

.field private logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method private getEtaTimeLeft(I)Ljava/lang/String;
    .locals 8
    .param p1, "minutes"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 147
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 148
    .local v2, "resources":Landroid/content/res/Resources;
    const/16 v3, 0x3c

    if-ge p1, v3, :cond_0

    .line 150
    const v3, 0x7f0900f9

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 167
    :goto_0
    return-object v3

    .line 151
    :cond_0
    const/16 v3, 0x5a0

    if-ge p1, v3, :cond_2

    .line 153
    div-int/lit8 v1, p1, 0x3c

    .line 154
    .local v1, "hours":I
    mul-int/lit8 v3, v1, 0x3c

    sub-int/2addr p1, v3

    .line 155
    if-lez p1, :cond_1

    .line 156
    const v3, 0x7f0900f7

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 158
    :cond_1
    const v3, 0x7f0900f8

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 162
    .end local v1    # "hours":I
    :cond_2
    div-int/lit16 v0, p1, 0x5a0

    .line 163
    .local v0, "days":I
    mul-int/lit16 v3, v0, 0x5a0

    sub-int v3, p1, v3

    div-int/lit8 v1, v3, 0x3c

    .line 164
    .restart local v1    # "hours":I
    if-lez v1, :cond_3

    .line 165
    const v3, 0x7f0900f5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 167
    :cond_3
    const v3, 0x7f0900f6

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private setEtaTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeLeft:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 96
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeAmPm:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    return-void
.end method


# virtual methods
.method public clearState()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 100
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA:Ljava/lang/String;

    .line 101
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA_AmPm:Ljava/lang/String;

    .line 102
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETADate:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTextView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeLeft:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeAmPm:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setEtaTextColor(I)V

    .line 109
    return-void
.end method

.method public getLastEtaDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETADate:Ljava/lang/String;

    return-object v0
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 64
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 65
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 66
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 67
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showDriveScoreEventsOnMap()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeLeft:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 73
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->bus:Lcom/squareup/otto/Bus;

    goto :goto_0
.end method

.method public onTrafficDelayDismissEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 90
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setEtaTextColor(I)V

    .line 91
    return-void
.end method

.method public onTrafficDelayEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 84
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->badTrafficColor:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setEtaTextColor(I)V

    .line 85
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 173
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 182
    :goto_0
    return-void

    .line 175
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->etaX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setX(F)V

    goto :goto_0

    .line 179
    :pswitch_1
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->etaShrinkLeftX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setX(F)V

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateETA(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 113
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v3

    if-nez v3, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    if-nez v3, :cond_2

    .line 118
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "etaDate is not set"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_2
    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 124
    .local v0, "time":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gez v3, :cond_4

    .line 125
    const-wide/16 v0, 0x0

    .line 129
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    long-to-int v4, v0

    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/util/RouteUtils;->formatEtaMinutes(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    .line 130
    .local v2, "timeLeft":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeLeft:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 134
    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA:Ljava/lang/String;

    .line 135
    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->convertDateToEta(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETADate:Ljava/lang/String;

    .line 136
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_3
    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaAmPm:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA_AmPm:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 141
    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaAmPm:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA_AmPm:Ljava/lang/String;

    .line 142
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeAmPm:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->lastETA_AmPm:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 127
    .end local v2    # "timeLeft":Ljava/lang/String;
    :cond_4
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    goto :goto_1
.end method
