.class Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$2;
.super Ljava/lang/Object;
.source "HomeScreenView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart(Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 0
    .param p1, "in"    # Lcom/navdy/hud/app/screen/BaseScreen;
    .param p2, "out"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    .line 179
    return-void
.end method

.method public onStop(Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 4
    .param p1, "in"    # Lcom/navdy/hud/app/screen/BaseScreen;
    .param p2, "out"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    const/4 v3, 0x0

    .line 183
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showCollapsedNotif:Z
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v1, v2, :cond_0

    .line 184
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showCollapsedNotif:Z
    invoke-static {v1, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$002(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Z)Z

    .line 185
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "expanding collapse notifi"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    .line 187
    .local v0, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->makeNotificationCurrent(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->showNotification()Z

    .line 191
    .end local v0    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_0
    return-void
.end method
