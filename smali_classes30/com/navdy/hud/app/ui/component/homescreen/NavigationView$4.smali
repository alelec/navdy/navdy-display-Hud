.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->animateBackfromOverview(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field final synthetic val$endAction:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;->val$endAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    const/4 v3, 0x0

    .line 543
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    const/4 v2, 0x1

    const/4 v5, 0x0

    move v4, v3

    move-wide v8, v6

    move v10, v3

    invoke-virtual/range {v1 .. v10}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapToArMode(ZZZLcom/here/android/mpa/common/GeoPosition;DDZ)V

    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "animateBackfromOverview: add transform"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;->val$endAction:Ljava/lang/Runnable;

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->animateBackfromOverviewMap(Ljava/lang/Runnable;)Lcom/here/android/mpa/mapping/Map$OnTransformListener;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$600(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Ljava/lang/Runnable;)Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 546
    return-void
.end method
