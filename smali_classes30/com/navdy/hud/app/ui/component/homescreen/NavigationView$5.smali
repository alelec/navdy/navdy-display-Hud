.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 850
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 853
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetMapOverview: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 854
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverview()V

    .line 855
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 856
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    move-result-object v0

    .line 857
    .local v0, "hereMapCameraManager":Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isOverviewZoomLevel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 858
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v4, v4, v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showOverviewMap(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    .line 861
    .end local v0    # "hereMapCameraManager":Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    :cond_0
    return-void
.end method
