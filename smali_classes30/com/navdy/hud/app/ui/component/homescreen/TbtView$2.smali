.class Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "TbtView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 268
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->access$100(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eq v0, v1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 264
    return-void
.end method
