.class public Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;
.super Ljava/lang/Object;
.source "DashWidgetPresenterFactory.java"


# static fields
.field private static final RESOURCE_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;->RESOURCE_MAP:Ljava/util/HashMap;

    .line 32
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;->RESOURCE_MAP:Ljava/util/HashMap;

    const-string v1, "EMPTY_WIDGET"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;->RESOURCE_MAP:Ljava/util/HashMap;

    const-string v1, "MPG_GRAPH_WIDGET"

    const v2, 0x7f02000a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;->RESOURCE_MAP:Ljava/util/HashMap;

    const-string v1, "MPG_AVG_WIDGET"

    const v2, 0x7f020009

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;->RESOURCE_MAP:Ljava/util/HashMap;

    const-string v1, "WEATHER_GRAPH_WIDGET"

    const v2, 0x7f02000b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createDashWidgetPresenter(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "widgetIdentifier"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 39
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 71
    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/ImageWidgetPresenter;

    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;->RESOURCE_MAP:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, p0, v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/ImageWidgetPresenter;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    move-object v0, v1

    :goto_1
    return-object v0

    .line 39
    :sswitch_0
    const-string v2, "EMPTY_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "FUEL_GAUGE_ID"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_2
    const-string v2, "COMPASS_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "ANALOG_CLOCK_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "DIGITAL_CLOCK_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "DIGITAL_CLOCK_2_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "TRAFFIC_INCIDENT_GAUGE_ID"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "CALENDAR_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "ETA_GAUGE_ID"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v2, "MPG_AVG_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v2, "MUSIC_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xa

    goto/16 :goto_0

    :sswitch_b
    const-string v2, "SPEED_LIMIT_SIGN_GAUGE_ID"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v2, "GFORCE_WIDGET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "ENGINE_TEMPERATURE_GAUGE_ID"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "DRIVE_SCORE_GAUGE_ID"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    .line 41
    :pswitch_0
    new-instance v0, Lcom/navdy/hud/app/view/EmptyGaugePresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/EmptyGaugePresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 43
    :pswitch_1
    new-instance v0, Lcom/navdy/hud/app/view/FuelGaugePresenter2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/FuelGaugePresenter2;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 45
    :pswitch_2
    new-instance v0, Lcom/navdy/hud/app/view/CompassPresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/CompassPresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 47
    :pswitch_3
    new-instance v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    sget-object v1, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->ANALOG:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;)V

    goto/16 :goto_1

    .line 49
    :pswitch_4
    new-instance v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    sget-object v1, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->DIGITAL1:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;)V

    goto/16 :goto_1

    .line 51
    :pswitch_5
    new-instance v0, Lcom/navdy/hud/app/view/ClockWidgetPresenter;

    sget-object v1, Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;->DIGITAL2:Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/view/ClockWidgetPresenter;-><init>(Landroid/content/Context;Lcom/navdy/hud/app/view/ClockWidgetPresenter$ClockType;)V

    goto/16 :goto_1

    .line 53
    :pswitch_6
    new-instance v0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;-><init>()V

    goto/16 :goto_1

    .line 55
    :pswitch_7
    new-instance v0, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/CalendarWidgetPresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 57
    :pswitch_8
    new-instance v0, Lcom/navdy/hud/app/view/ETAGaugePresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/ETAGaugePresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 59
    :pswitch_9
    new-instance v0, Lcom/navdy/hud/app/view/MPGGaugePresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/MPGGaugePresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 61
    :pswitch_a
    new-instance v0, Lcom/navdy/hud/app/view/MusicWidgetPresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/MusicWidgetPresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 63
    :pswitch_b
    new-instance v0, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 65
    :pswitch_c
    new-instance v0, Lcom/navdy/hud/app/view/GForcePresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/GForcePresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 67
    :pswitch_d
    new-instance v0, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 69
    :pswitch_e
    new-instance v0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    const v2, 0x7f030014

    invoke-direct {v0, p0, v2, v1}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;-><init>(Landroid/content/Context;IZ)V

    goto/16 :goto_1

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e0a2d5c -> :sswitch_3
        -0x6fbd11cf -> :sswitch_2
        -0x5a558a42 -> :sswitch_a
        -0x45145f74 -> :sswitch_9
        -0x330b2192 -> :sswitch_8
        -0x201d52b7 -> :sswitch_5
        -0x7dd2557 -> :sswitch_d
        0x1b6c50e3 -> :sswitch_e
        0x263fde65 -> :sswitch_7
        0x277ba29c -> :sswitch_4
        0x30660196 -> :sswitch_0
        0x42be70c7 -> :sswitch_b
        0x45a45eaa -> :sswitch_1
        0x6f9cbd2c -> :sswitch_6
        0x7aa7dbff -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
