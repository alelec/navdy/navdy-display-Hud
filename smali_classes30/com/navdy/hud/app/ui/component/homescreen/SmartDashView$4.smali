.class Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;
.super Ljava/lang/Object;
.source "SmartDashView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$ChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->initializeView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 435
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGaugeChanged(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 438
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->getWidgetIdentifierForIndex(I)Ljava/lang/String;

    move-result-object v0

    .line 439
    .local v0, "identifier":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onGaugeChanged, (Right) , Widget ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , Position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 440
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v2, "PREFERENCE_RIGHT_GAUGE"

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V

    .line 441
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftAdapter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->setExcludedPosition(I)V

    .line 442
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->updateState()V

    .line 443
    return-void
.end method
