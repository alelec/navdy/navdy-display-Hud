.class public Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
.super Ljava/lang/Object;
.source "SmartDashWidgetManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SmartDashWidgetCache"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGaugeIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWidgetIndexMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWidgetPresentersMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;Landroid/content/Context;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mContext:Landroid/content/Context;

    .line 274
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mGaugeIds:Ljava/util/List;

    .line 275
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetPresentersMap:Ljava/util/HashMap;

    .line 276
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetIndexMap:Ljava/util/HashMap;

    .line 277
    return-void
.end method

.method private initializeWidget(Ljava/lang/String;)V
    .locals 10
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 315
    const/4 v7, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v7, :pswitch_data_0

    .line 340
    :cond_1
    :goto_1
    return-void

    .line 315
    :sswitch_0
    const-string v8, "FUEL_GAUGE_ID"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v7, 0x0

    goto :goto_0

    :sswitch_1
    const-string v8, "DRIVE_SCORE_GAUGE_ID"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :sswitch_2
    const-string v8, "ENGINE_TEMPERATURE_GAUGE_ID"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v7, 0x2

    goto :goto_0

    .line 317
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/view/FuelGaugePresenter2;

    .line 318
    .local v5, "fuelGaugePresenter2":Lcom/navdy/hud/app/view/FuelGaugePresenter2;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v6

    .line 319
    .local v6, "fuelLevel":I
    if-eqz v5, :cond_1

    .line 320
    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->setFuelLevel(I)V

    goto :goto_1

    .line 325
    .end local v5    # "fuelGaugePresenter2":Lcom/navdy/hud/app/view/FuelGaugePresenter2;
    .end local v6    # "fuelLevel":I
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    .line 326
    .local v0, "driveScoreGaugePresenter":Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;
    if-eqz v0, :cond_1

    .line 327
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTelemetryDataManager()Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/analytics/TelemetryDataManager;->getDriveScoreUpdatedEvent()Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;

    move-result-object v1

    .line 328
    .local v1, "driveScoreUpdated":Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setDriveScoreUpdated(Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;)V

    goto :goto_1

    .line 332
    .end local v0    # "driveScoreGaugePresenter":Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;
    .end local v1    # "driveScoreUpdated":Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;

    .line 333
    .local v4, "engineTemperaturePresenter":Lcom/navdy/hud/app/view/EngineTemperaturePresenter;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/obd/ObdManager;->getPidValue(I)D

    move-result-wide v2

    .line 334
    .local v2, "engineTemperature":D
    if-eqz v4, :cond_1

    .line 335
    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    cmpl-double v7, v2, v8

    if-eqz v7, :cond_2

    .end local v2    # "engineTemperature":D
    :goto_2
    invoke-virtual {v4, v2, v3}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->setEngineTemperature(D)V

    goto :goto_1

    .restart local v2    # "engineTemperature":D
    :cond_2
    sget-object v7, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->Companion:Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;

    invoke-virtual {v7}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter$Companion;->getTEMPERATURE_GAUGE_MID_POINT_CELSIUS()D

    move-result-wide v2

    goto :goto_2

    .line 315
    :sswitch_data_0
    .sparse-switch
        -0x7dd2557 -> :sswitch_2
        0x1b6c50e3 -> :sswitch_1
        0x45a45eaa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public add(Ljava/lang/String;)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 281
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetIndexMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 282
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mGaugeIds:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetIndexMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mGaugeIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/DashWidgetPresenterFactory;->createDashWidgetPresenter(Landroid/content/Context;Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v0

    .line 285
    .local v0, "dashboardWidgetPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetPresentersMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->initializeWidget(Ljava/lang/String;)V

    .line 288
    .end local v0    # "dashboardWidgetPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mGaugeIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 390
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetPresentersMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 391
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetIndexMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 392
    return-void
.end method

.method public getIndexForWidget(Ljava/lang/String;)I
    .locals 1
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetIndexMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetIndexMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 294
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 298
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mGaugeIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mGaugeIds:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 300
    .local v0, "identifier":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetPresentersMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    .line 302
    .end local v0    # "identifier":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    .locals 2
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 306
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getIndexForWidget(Ljava/lang/String;)I

    move-result v0

    .line 307
    .local v0, "index":I
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v1

    return-object v1
.end method

.method public getWidgetPresentersMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 385
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mWidgetPresentersMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getWidgetsCount()I
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->mGaugeIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public updateWidget(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 345
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 346
    const/4 v5, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v5, :pswitch_data_0

    .line 382
    .end local p2    # "data":Ljava/lang/Object;
    :cond_1
    :goto_1
    return-void

    .line 346
    .restart local p2    # "data":Ljava/lang/Object;
    :sswitch_0
    const-string v6, "FUEL_GAUGE_ID"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :sswitch_1
    const-string v6, "COMPASS_WIDGET"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :sswitch_2
    const-string v6, "MPG_AVG_WIDGET"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x2

    goto :goto_0

    :sswitch_3
    const-string v6, "SPEED_LIMIT_SIGN_GAUGE_ID"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x3

    goto :goto_0

    :sswitch_4
    const-string v6, "ENGINE_TEMPERATURE_GAUGE_ID"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x4

    goto :goto_0

    .line 348
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/FuelGaugePresenter2;

    .line 349
    .local v2, "fuelGaugePresenter2":Lcom/navdy/hud/app/view/FuelGaugePresenter2;
    if-eqz v2, :cond_1

    instance-of v5, p2, Ljava/lang/Double;

    if-eqz v5, :cond_1

    .line 350
    check-cast p2, Ljava/lang/Number;

    .end local p2    # "data":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/view/FuelGaugePresenter2;->setFuelLevel(I)V

    goto :goto_1

    .line 355
    .end local v2    # "fuelGaugePresenter2":Lcom/navdy/hud/app/view/FuelGaugePresenter2;
    .restart local p2    # "data":Ljava/lang/Object;
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/CompassPresenter;

    .line 356
    .local v0, "compassPresenter":Lcom/navdy/hud/app/view/CompassPresenter;
    if-eqz v0, :cond_1

    .line 357
    check-cast p2, Ljava/lang/Number;

    .end local p2    # "data":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/navdy/hud/app/view/CompassPresenter;->setHeadingAngle(D)V

    goto :goto_1

    .line 361
    .end local v0    # "compassPresenter":Lcom/navdy/hud/app/view/CompassPresenter;
    .restart local p2    # "data":Ljava/lang/Object;
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/view/MPGGaugePresenter;

    .line 362
    .local v3, "mpgGaugePresenter2":Lcom/navdy/hud/app/view/MPGGaugePresenter;
    if-eqz v3, :cond_1

    .line 363
    check-cast p2, Ljava/lang/Number;

    .end local p2    # "data":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/navdy/hud/app/view/MPGGaugePresenter;->setCurrentMPG(D)V

    goto :goto_1

    .line 368
    .end local v3    # "mpgGaugePresenter2":Lcom/navdy/hud/app/view/MPGGaugePresenter;
    .restart local p2    # "data":Ljava/lang/Object;
    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;

    .line 369
    .local v4, "speedLimitSignPresenter":Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;
    if-eqz v4, :cond_1

    .line 370
    check-cast p2, Ljava/lang/Number;

    .end local p2    # "data":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;->setSpeedLimit(I)V

    goto :goto_1

    .line 375
    .end local v4    # "speedLimitSignPresenter":Lcom/navdy/hud/app/view/SpeedLimitSignPresenter;
    .restart local p2    # "data":Ljava/lang/Object;
    :pswitch_4
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(Ljava/lang/String;)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;

    .line 376
    .local v1, "engineTemperaturePresenter":Lcom/navdy/hud/app/view/EngineTemperaturePresenter;
    if-eqz v1, :cond_1

    .line 377
    check-cast p2, Ljava/lang/Number;

    .end local p2    # "data":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v5

    int-to-double v6, v5

    invoke-virtual {v1, v6, v7}, Lcom/navdy/hud/app/view/EngineTemperaturePresenter;->setEngineTemperature(D)V

    goto/16 :goto_1

    .line 346
    :sswitch_data_0
    .sparse-switch
        -0x6fbd11cf -> :sswitch_1
        -0x45145f74 -> :sswitch_2
        -0x7dd2557 -> :sswitch_4
        0x42be70c7 -> :sswitch_3
        0x45a45eaa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
