.class Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;
.super Ljava/lang/Object;
.source "TbtView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->animateSetIcon(I)Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

.field final synthetic val$imageResourceId:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    .prologue
    .line 392
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iput p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;->val$imageResourceId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 405
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 398
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;->val$imageResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 399
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 400
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 401
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 402
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 408
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 394
    return-void
.end method
