.class public Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;
.super Landroid/widget/FrameLayout;
.source "RecalculatingView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private lastRerouteEvent:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private paused:Z

.field recalcAnimator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01a9
    .end annotation
.end field

.field recalcImageView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01aa
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method


# virtual methods
.method public hideRecalculating()V
    .locals 1

    .prologue
    .line 110
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->recalcAnimator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->lastRerouteEvent:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    .line 113
    return-void
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 72
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 61
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 62
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->recalcImageView:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->reCalcAnimColor:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setColor(I)V

    .line 66
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->bus:Lcom/squareup/otto/Bus;

    .line 67
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->paused:Z

    if-eqz v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->paused:Z

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause:recalc"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRerouteEvent(Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rerouteEvent:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;->routeEventType:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "rerouteEvent:navigation not active|hasArrived|routeCalcOn"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 100
    :cond_1
    :goto_0
    return-void

    .line 84
    :cond_2
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->lastRerouteEvent:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    .line 86
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;->routeEventType:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    if-ne v0, v1, :cond_3

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setRecalculating(Z)V

    .line 88
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->paused:Z

    if-nez v0, :cond_1

    .line 89
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->showRecalculating()V

    goto :goto_0

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setRecalculating(Z)V

    .line 93
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->paused:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 96
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->hideRecalculating()V

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getTbtView()Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setVisibility(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 138
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->paused:Z

    if-nez v1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->paused:Z

    .line 142
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "::onResume:recalc"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->lastRerouteEvent:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    .line 144
    .local v0, "event":Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;
    if-eqz v0, :cond_0

    .line 145
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->lastRerouteEvent:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    .line 146
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "::onResume:recalc set last reroute event"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->onRerouteEvent(Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;)V

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 116
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 118
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->recalcX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setX(F)V

    goto :goto_0

    .line 122
    :pswitch_1
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->recalcShrinkLeftX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setX(F)V

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showRecalculating()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getTbtView()Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setVisibility(I)V

    .line 105
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->recalcAnimator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->start()V

    .line 107
    return-void
.end method
