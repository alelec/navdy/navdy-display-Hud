.class public Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$$ViewInjector;
.super Ljava/lang/Object;
.source "SmartDashView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e01ac

    const-string v2, "field \'mLeftGaugeViewPager\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .line 12
    const v1, 0x7f0e01af

    const-string v2, "field \'mRightGaugeViewPager\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .line 14
    const v1, 0x7f0e01ae

    const-string v2, "field \'mMiddleGaugeView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/GaugeView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    .line 16
    const v1, 0x7f0e01b0

    const-string v2, "field \'mEtaLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mEtaLayout:Landroid/view/ViewGroup;

    .line 18
    const v1, 0x7f0e01b1

    const-string v2, "field \'etaText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->etaText:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e01b2

    const-string v2, "field \'etaAmPm\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->etaAmPm:Landroid/widget/TextView;

    .line 22
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .line 26
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    .line 27
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    .line 28
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mEtaLayout:Landroid/view/ViewGroup;

    .line 29
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->etaText:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->etaAmPm:Landroid/widget/TextView;

    .line 31
    return-void
.end method
