.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;
.super Ljava/lang/Object;
.source "HomeScreenUtils.java"


# static fields
.field private static final DENSITY:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 24
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->DENSITY:F

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "alpha"    # I

    .prologue
    .line 121
    const-string v0, "alpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    int-to-float v3, p1

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static getFadeInAndScaleUpAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;
    .locals 9
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 125
    const-string v4, "alpha"

    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 126
    .local v1, "fadeInAnim":Landroid/animation/ObjectAnimator;
    const-string v4, "scaleX"

    new-array v5, v8, [F

    aput v6, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 127
    .local v2, "scaleUpXAnim":Landroid/animation/ObjectAnimator;
    const-string v4, "scaleY"

    new-array v5, v8, [F

    aput v6, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 129
    .local v3, "scaleUpYAnim":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 130
    .local v0, "fadeInAndScaleUp":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 132
    return-object v0

    .line 125
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static getFadeOutAndScaleDownAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;
    .locals 10
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const v9, 0x3f666666    # 0.9f

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 136
    const-string v4, "alpha"

    new-array v5, v8, [F

    const/4 v6, 0x0

    aput v6, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 137
    .local v0, "fadeOutAnim":Landroid/animation/ObjectAnimator;
    const-string v4, "scaleX"

    new-array v5, v8, [F

    aput v9, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 138
    .local v2, "scaleDownXAnim":Landroid/animation/ObjectAnimator;
    const-string v4, "scaleY"

    new-array v5, v8, [F

    aput v9, v5, v7

    invoke-static {p0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 140
    .local v3, "scaleDownYAnim":Landroid/animation/ObjectAnimator;
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 141
    .local v1, "fadeOutWithScaleDown":Landroid/animation/AnimatorSet;
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 143
    return-object v1
.end method

.method public static getMarginAnimator(Landroid/view/View;II)Landroid/animation/AnimatorSet;
    .locals 9
    .param p0, "view"    # Landroid/view/View;
    .param p1, "marginLeft"    # I
    .param p2, "marginRight"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 92
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 93
    .local v0, "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    new-array v4, v8, [I

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    aput v5, v4, v6

    aput p1, v4, v7

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 94
    .local v2, "v1":Landroid/animation/ValueAnimator;
    new-instance v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$4;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$4;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 104
    new-array v4, v8, [I

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    aput v5, v4, v6

    aput p2, v4, v7

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 105
    .local v3, "v2":Landroid/animation/ValueAnimator;
    new-instance v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$5;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$5;-><init>(Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 115
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 116
    .local v1, "set":Landroid/animation/AnimatorSet;
    new-array v4, v8, [Landroid/animation/Animator;

    aput-object v2, v4, v6

    aput-object v3, v4, v7

    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 117
    return-object v1
.end method

.method public static getProgressBarAnimator(Landroid/widget/ProgressBar;I)Landroid/animation/ValueAnimator;
    .locals 4
    .param p0, "progressBar"    # Landroid/widget/ProgressBar;
    .param p1, "progress"    # I

    .prologue
    .line 60
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    aput p1, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 62
    .local v0, "progressAnimator":Landroid/animation/ValueAnimator;
    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$2;-><init>(Landroid/widget/ProgressBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 69
    return-object v0
.end method

.method public static getTranslationXPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "xPosBy"    # I

    .prologue
    .line 36
    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    int-to-float v3, p1

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static getTranslationYPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "yPosBy"    # I

    .prologue
    .line 40
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    int-to-float v3, p1

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "targetWidth"    # I

    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 45
    .local v0, "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    aput v4, v2, v3

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 46
    .local v1, "widthAnimator":Landroid/animation/ValueAnimator;
    new-instance v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$1;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 56
    return-object v1
.end method

.method public static getWidthAnimator(Landroid/view/View;II)Landroid/animation/ValueAnimator;
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "startWidth"    # I
    .param p2, "targetWidth"    # I

    .prologue
    .line 73
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 74
    .local v0, "widthAnimator":Landroid/animation/ValueAnimator;
    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$3;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 81
    return-object v0
.end method

.method public static getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "xPos"    # F

    .prologue
    .line 28
    const-string v0, "x"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static getYPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "yPos"    # F

    .prologue
    .line 32
    const-string v0, "y"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method public static setWidth(Landroid/view/View;I)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "width"    # I

    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 86
    .local v0, "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 87
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 88
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 89
    return-void
.end method
