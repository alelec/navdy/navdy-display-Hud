.class public Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;
.super Landroid/widget/RelativeLayout;
.source "TbtShrunkDistanceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;
    }
.end annotation


# static fields
.field private static final TBT_SHRUNK_MODE:Ljava/lang/String; = "persist.sys.tbtdistanceshrunk"

.field private static final TBT_SHRUNK_MODE_DISTANCE_NUMBER:Ljava/lang/String; = "distance_number"

.field private static final TBT_SHRUNK_MODE_PROGRESS_BAR:Ljava/lang/String; = "progress_bar"


# instance fields
.field private logger:Lcom/navdy/service/library/log/Logger;

.field private final mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

.field nowShrunkIcon:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c6
    .end annotation
.end field

.field progressBarShrunk:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c4
    .end annotation
.end field

.field tbtShrunkDistance:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c5
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const-string v1, "persist.sys.tbtdistanceshrunk"

    const-string v2, "progress_bar"

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "modeString":Ljava/lang/String;
    const-string v1, "distance_number"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->DISTANCE_NUMBER:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->PROGRESS_BAR:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    goto :goto_0
.end method

.method private setRegularBackground()V
    .locals 1

    .prologue
    .line 146
    const v0, 0x106000c

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->setBackgroundResource(I)V

    .line 147
    return-void
.end method

.method private setTransparentBackground()V
    .locals 1

    .prologue
    .line 150
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->setBackgroundResource(I)V

    .line 151
    return-void
.end method


# virtual methods
.method public animateProgressBar(D)V
    .locals 5
    .param p1, "progress"    # D

    .prologue
    .line 137
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->PROGRESS_BAR:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    if-eq v0, v1, :cond_0

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->progressBarShrunk:Landroid/widget/ProgressBar;

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, p1

    double-to-int v1, v2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getProgressBarAnimator(Landroid/widget/ProgressBar;I)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 141
    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public hideNowIcon()V
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->nowShrunkIcon:Landroid/view/View;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeOutAndScaleDownAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 110
    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->DISTANCE_NUMBER:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    if-ne v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->tbtShrunkDistance:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    :cond_0
    return-void
.end method

.method public hideProgressBar()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->PROGRESS_BAR:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    if-eq v0, v1, :cond_0

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->progressBarShrunk:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->setWidth(Landroid/view/View;I)V

    .line 133
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->setTransparentBackground()V

    goto :goto_0
.end method

.method public initProgressBar()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->PROGRESS_BAR:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    if-eq v0, v1, :cond_0

    .line 126
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->setRegularBackground()V

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->progressBarShrunk:Landroid/widget/ProgressBar;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkProgressBarWidth:I

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->setWidth(Landroid/view/View;I)V

    .line 125
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->animateProgressBar(D)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 64
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 65
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 66
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 69
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkDistanceY:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->setY(F)V

    .line 71
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$1;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$TbtShrunkDistanceView$Mode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->tbtShrunkDistance:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 80
    :goto_0
    return-void

    .line 73
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->progressBarShrunk:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setDistanceText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->tbtShrunkDistance:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 83
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    :goto_0
    return-void

    .line 85
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkDistanceX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->setX(F)V

    goto :goto_0

    .line 89
    :pswitch_1
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkDistanceShrinkLeftX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->setX(F)V

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showNowIcon()V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->nowShrunkIcon:Landroid/view/View;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeInAndScaleUpAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 100
    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;->DISTANCE_NUMBER:Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView$Mode;

    if-ne v0, v1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtShrunkDistanceView;->tbtShrunkDistance:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    :cond_0
    return-void
.end method
