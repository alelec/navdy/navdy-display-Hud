.class public Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
.super Landroid/widget/FrameLayout;
.source "NavigationView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;
.implements Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;


# static fields
.field private static final MAP_MASK_OFFSET:I = 0x1

.field private static final ZOOM_IN:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

.field private static final ZOOM_OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private cleanupMapOverviewRunnable:Ljava/lang/Runnable;

.field private driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field private endMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private fluctuatorPosListener:Lcom/here/android/mpa/mapping/Map$OnTransformListener;

.field private handler:Landroid/os/Handler;

.field private hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

.field private hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private initComplete:Z

.field private isRenderingEnabled:Z

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

.field mapIconIndicator:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0191
    .end annotation
.end field

.field private mapMarkerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;"
        }
    .end annotation
.end field

.field mapMask:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00b5
    .end annotation
.end field

.field mapView:Lcom/here/android/mpa/mapping/MapView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e018f
    .end annotation
.end field

.field private networkCheckRequired:Z

.field noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0192
    .end annotation
.end field

.field private overviewMapRouteObjects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;"
        }
    .end annotation
.end field

.field private paused:Z

.field private resetMapOverviewRunnable:Ljava/lang/Runnable;

.field private selectedDestinationImage:Lcom/here/android/mpa/common/Image;

.field private setInitialCenter:Z

.field private showMapIconIndicatorRunnable:Ljava/lang/Runnable;

.field startDestinationFluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0190
    .end annotation
.end field

.field private startMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

.field private unselectedDestinationImage:Lcom/here/android/mpa/common/Image;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->IN:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;-><init>(Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->ZOOM_IN:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    .line 93
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;-><init>(Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->ZOOM_OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 177
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 104
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handler:Landroid/os/Handler;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->overviewMapRouteObjects:Ljava/util/ArrayList;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    .line 146
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->fluctuatorPosListener:Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    .line 159
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$2;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showMapIconIndicatorRunnable:Ljava/lang/Runnable;

    .line 850
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$5;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->resetMapOverviewRunnable:Ljava/lang/Runnable;

    .line 873
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$6;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverviewRunnable:Ljava/lang/Runnable;

    .line 178
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 182
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->positionFluctuator()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/profile/DriverProfileManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Ljava/lang/Runnable;)Lcom/here/android/mpa/mapping/Map$OnTransformListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->animateBackfromOverviewMap(Ljava/lang/Runnable;)Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;ZZZLcom/here/android/mpa/common/GeoPosition;DFZ)V
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p5, "x5"    # D
    .param p7, "x6"    # F
    .param p8, "x7"    # Z

    .prologue
    .line 82
    invoke-direct/range {p0 .. p8}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapToArModeInternal(ZZZLcom/here/android/mpa/common/GeoPosition;DFZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showMapIconIndicatorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private animateBackfromOverviewMap(Ljava/lang/Runnable;)Lcom/here/android/mpa/mapping/Map$OnTransformListener;
    .locals 1
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 1309
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method private declared-synchronized handleEngineInit()V
    .locals 3

    .prologue
    .line 274
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isRenderingAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onMapEngineInitializationCompleted: Maps Rendering not allowed, Quiet mode On!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    :goto_0
    monitor-exit p0

    return-void

    .line 279
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "engine initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 282
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onMapEngineInitializationCompleted()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 284
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() : Cannot initialize map engine: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getError()Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->hideLocationUI()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private initViews()V
    .locals 4

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    :goto_0
    return-void

    .line 230
    :cond_0
    const/4 v0, 0x0

    .line 232
    .local v0, "hasLocationFix":Z
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    if-eqz v1, :cond_1

    .line 233
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->hasLocationFix()Z

    .line 236
    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z

    if-nez v1, :cond_3

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "first layout: location fix["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] initComplete["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->showLocationUI()V

    goto :goto_0

    .line 240
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "first layout: location fix available"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 241
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapCenter()Z

    goto :goto_0
.end method

.method private declared-synchronized onMapEngineInitializationCompleted()V
    .locals 8

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 357
    :goto_0
    monitor-exit p0

    return-void

    .line 323
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getMapController()Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    .line 324
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z

    .line 326
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onMapEngineInitializationCompleted: Maps Rendering allowed"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->enableRendering()V

    .line 332
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    sget-object v4, Lcom/here/android/mpa/common/CopyrightLogoPosition;->TOP_RIGHT:Lcom/here/android/mpa/common/CopyrightLogoPosition;

    invoke-virtual {v3, v4}, Lcom/here/android/mpa/mapping/MapView;->setCopyrightLogoPosition(Lcom/here/android/mpa/common/CopyrightLogoPosition;)V

    .line 333
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/MapView;->onResume()V

    .line 334
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    invoke-virtual {v3, v4, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapView(Lcom/here/android/mpa/mapping/MapView;Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 338
    :try_start_2
    new-instance v1, Lcom/here/android/mpa/common/Image;

    invoke-direct {v1}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 339
    .local v1, "image":Lcom/here/android/mpa/common/Image;
    const v3, 0x7f020108

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V

    .line 340
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 341
    .local v0, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_1

    .line 342
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v0    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    const-wide v4, 0x4042e6aac1094a2cL    # 37.802086

    const-wide v6, -0x3fa1652edbb59ddcL    # -122.419015

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 344
    .restart local v0    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_1
    new-instance v3, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-direct {v3, v0, v1}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 349
    .end local v0    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v1    # "image":Lcom/here/android/mpa/common/Image;
    :goto_1
    :try_start_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "view rendered"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 350
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->isAcquiringLocation()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z

    if-eqz v3, :cond_2

    .line 351
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapCenter()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 352
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->hideLocationUI()V

    .line 356
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/hud/app/maps/MapEvents$MapUIReady;

    invoke-direct {v4}, Lcom/navdy/hud/app/maps/MapEvents$MapUIReady;-><init>()V

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 318
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 345
    :catch_0
    move-exception v2

    .line 346
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_4
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private positionFluctuator()V
    .locals 4

    .prologue
    .line 1383
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v1, :cond_0

    .line 1384
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$10;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1416
    :cond_0
    :goto_0
    return-void

    .line 1413
    :catch_0
    move-exception v0

    .line 1414
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setMapCenter()Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v10, 0x0

    const/4 v0, 0x1

    .line 918
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setInitialCenter:Z

    if-eqz v3, :cond_0

    .line 954
    :goto_0
    return v0

    .line 921
    :cond_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z

    if-nez v3, :cond_1

    .line 922
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "setMapCenter:init not complete yet, deferring"

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    move v0, v1

    .line 923
    goto :goto_0

    .line 925
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    if-nez v3, :cond_2

    .line 926
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .line 928
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .line 929
    .local v2, "center":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v2, :cond_5

    .line 930
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setInitialCenter:Z

    .line 931
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    move v7, v6

    invoke-virtual/range {v1 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 932
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMapCenter:setcenterInit done "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 934
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getFeatureUtil()Lcom/navdy/hud/app/util/FeatureUtil;

    move-result-object v1

    sget-object v3, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/util/FeatureUtil;->isFeatureEnabled(Lcom/navdy/hud/app/util/FeatureUtil$Feature;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 935
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->markAvailable()V

    .line 938
    :cond_3
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->updateMapIndicator()V

    .line 941
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isNetworkStateInitialized()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 942
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "checkPrevRoute:n/w is initialized"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 943
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startPreviousRoute(Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 944
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "send map engine ready event-1"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 945
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    new-instance v9, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    sget-object v4, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object v5, v10

    move-object v6, v10

    move-object v7, v10

    move-object v8, v10

    invoke-direct/range {v3 .. v8}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    invoke-direct {v9, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v9}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 946
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/maps/MapEvents$MapEngineReady;

    invoke-direct {v3}, Lcom/navdy/hud/app/maps/MapEvents$MapEngineReady;-><init>()V

    invoke-virtual {v1, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 948
    :cond_4
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->networkCheckRequired:Z

    .line 949
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "checkPrevRoute:n/w is initialized, defer and wait"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 954
    goto/16 :goto_0
.end method

.method private setMapToArModeInternal(ZZZLcom/here/android/mpa/common/GeoPosition;DFZ)V
    .locals 9
    .param p1, "animate"    # Z
    .param p2, "changeState"    # Z
    .param p3, "useLastZoom"    # Z
    .param p4, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p5, "zoomLevel"    # D
    .param p7, "tiltLevel"    # F
    .param p8, "cleanupMapOverview"    # Z

    .prologue
    .line 1079
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1080
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMapToArModeInternal animate:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " changeState:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1082
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    move-result-object v8

    .line 1083
    .local v8, "hereMapCameraManager":Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    if-nez p4, :cond_0

    .line 1084
    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLastGeoPosition()Lcom/here/android/mpa/common/GeoPosition;

    move-result-object p4

    .line 1091
    :cond_0
    if-eqz p8, :cond_1

    .line 1092
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverview()V

    .line 1095
    :cond_1
    if-eqz p4, :cond_5

    .line 1096
    invoke-virtual {p4}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .line 1097
    .local v2, "center":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {p4}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v0

    double-to-float v6, v0

    .line 1099
    .local v6, "heading":F
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p7, v0

    if-nez v0, :cond_2

    .line 1100
    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLastTilt()F

    move-result v7

    .line 1104
    .local v7, "tilt":F
    :goto_0
    if-eqz p3, :cond_3

    .line 1105
    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLastZoom()D

    move-result-wide v4

    .line 1130
    .local v4, "zoom":D
    :goto_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 1131
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v1

    if-eqz p1, :cond_8

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->BOW:Lcom/here/android/mpa/mapping/Map$Animation;

    :goto_2
    invoke-virtual/range {v0 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenterForState(Lcom/navdy/hud/app/maps/here/HereMapController$State;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 1134
    if-eqz p2, :cond_9

    .line 1135
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 1139
    :goto_3
    return-void

    .line 1102
    .end local v4    # "zoom":D
    .end local v7    # "tilt":F
    :cond_2
    move/from16 v7, p7

    .restart local v7    # "tilt":F
    goto :goto_0

    .line 1107
    :cond_3
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, p5, v0

    if-nez v0, :cond_4

    .line 1108
    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    .restart local v4    # "zoom":D
    goto :goto_1

    .line 1110
    .end local v4    # "zoom":D
    :cond_4
    move-wide v4, p5

    .restart local v4    # "zoom":D
    goto :goto_1

    .line 1114
    .end local v2    # "center":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v4    # "zoom":D
    .end local v6    # "heading":F
    .end local v7    # "tilt":F
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->getCenter()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .line 1115
    .restart local v2    # "center":Lcom/here/android/mpa/common/GeoCoordinate;
    const/high16 v6, -0x40800000    # -1.0f

    .line 1117
    .restart local v6    # "heading":F
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p7, v0

    if-nez v0, :cond_6

    .line 1118
    const/high16 v7, 0x42700000    # 60.0f

    .line 1123
    .restart local v7    # "tilt":F
    :goto_4
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, p5, v0

    if-nez v0, :cond_7

    .line 1124
    const-wide v4, 0x4030800000000000L    # 16.5

    .restart local v4    # "zoom":D
    goto :goto_1

    .line 1120
    .end local v4    # "zoom":D
    .end local v7    # "tilt":F
    :cond_6
    move/from16 v7, p7

    .restart local v7    # "tilt":F
    goto :goto_4

    .line 1126
    :cond_7
    move-wide v4, p5

    .restart local v4    # "zoom":D
    goto :goto_1

    .line 1131
    :cond_8
    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    goto :goto_2

    .line 1137
    :cond_9
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->TRANSITION:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    goto :goto_3
.end method

.method private setTransformCenter()V
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    if-nez v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setTransformCenter:no map"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 406
    :goto_0
    return-void

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    goto :goto_0
.end method

.method private setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
    .locals 2
    .param p1, "state"    # Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .prologue
    .line 409
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    if-nez v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setTransformCenter:no map"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 440
    :goto_0
    return-void

    .line 414
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 421
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setTransformCenter:overview:nav"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 423
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterSmallMiddle:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTransformCenter(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 416
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setTransformCenter:routeSearch"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 417
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterPicker:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTransformCenter(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setTransformCenter:overview:open"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterLargeMiddle:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTransformCenter(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 431
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 432
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setTransformCenter:ar:nav"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 433
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterSmallBottom:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTransformCenter(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 435
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setTransformCenter:ar:open"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterLargeBottom:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTransformCenter(Landroid/graphics/PointF;)V

    goto :goto_0

    .line 414
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setViewMap(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 1163
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1172
    :goto_0
    return-void

    .line 1165
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapView;->setX(F)V

    goto :goto_0

    .line 1169
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewShrinkLeftX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapView;->setX(F)V

    goto :goto_0

    .line 1163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setViewMapIconIndicator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 1151
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1160
    :goto_0
    return-void

    .line 1153
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    goto :goto_0

    .line 1157
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkLeft_X:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    goto :goto_0

    .line 1151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setViewMapMask(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 3
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 1175
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1176
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1187
    :goto_0
    return-void

    .line 1178
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskX:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setX(F)V

    .line 1179
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->fullWidth:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto :goto_0

    .line 1183
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkLeftX:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setX(F)V

    .line 1184
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkWidth:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto :goto_0

    .line 1176
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private startPreviousRoute(Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 3
    .param p1, "center"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 1423
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Lcom/here/android/mpa/common/GeoCoordinate;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1505
    return-void
.end method

.method private switchToOverviewMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Ljava/lang/Runnable;ZZD)V
    .locals 9
    .param p1, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p3, "currentMapRoute"    # Lcom/here/android/mpa/mapping/MapRoute;
    .param p4, "endAction"    # Ljava/lang/Runnable;
    .param p5, "isArrived"    # Z
    .param p6, "animate"    # Z
    .param p7, "zoomLevel"    # D

    .prologue
    .line 457
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchToOverviewMode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 458
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 460
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupFluctuator()V

    .line 462
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 464
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v8

    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, p1

    move v5, p6

    move-wide/from16 v6, p7

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Lcom/here/android/mpa/routing/Route;Ljava/lang/Runnable;Lcom/here/android/mpa/common/GeoCoordinate;ZD)V

    const/16 v1, 0x11

    invoke-virtual {v8, v0, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 536
    return-void
.end method

.method private updateMapIndicator()V
    .locals 4

    .prologue
    .line 960
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v0

    .line 961
    .local v0, "showRawLocation":Z
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const v1, 0x3ecccccd    # 0.4f

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 962
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Z)V

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 978
    if-eqz v0, :cond_1

    .line 979
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->addMarkers(Lcom/navdy/hud/app/maps/here/HereMapController;)V

    .line 983
    :goto_1
    return-void

    .line 961
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 981
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->removeMarkers(Lcom/navdy/hud/app/maps/here/HereMapController;)V

    goto :goto_1
.end method


# virtual methods
.method public addMapOverviewRoutes(Lcom/here/android/mpa/mapping/MapRoute;)V
    .locals 3
    .param p1, "mapRoute"    # Lcom/here/android/mpa/mapping/MapRoute;

    .prologue
    .line 899
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addMapOverviewRoutes:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 900
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 901
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->overviewMapRouteObjects:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 902
    return-void
.end method

.method public adjustMaplayoutHeight(I)V
    .locals 6
    .param p1, "height"    # I

    .prologue
    .line 377
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeMapHeight:I

    if-ne p1, v2, :cond_0

    .line 378
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorActiveY:I

    .line 382
    .local v1, "y":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    int-to-float v3, v1

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterIconHeight:I

    int-to-float v4, v4

    const v5, 0x3f19999a    # 0.6f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 385
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 386
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 388
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->layoutMap()V

    .line 389
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->invalidate()V

    .line 390
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->requestLayout()V

    .line 391
    return-void

    .line 380
    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "y":I
    :cond_0
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorOpenY:I

    .restart local v1    # "y":I
    goto :goto_0
.end method

.method public animateBackfromOverview(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 539
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "animateBackfromOverview"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 540
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$4;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Ljava/lang/Runnable;)V

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 548
    return-void
.end method

.method public animateToOverview(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Ljava/lang/Runnable;Z)V
    .locals 10
    .param p1, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p3, "currentMapRoute"    # Lcom/here/android/mpa/mapping/MapRoute;
    .param p4, "endAction"    # Ljava/lang/Runnable;
    .param p5, "isArrived"    # Z

    .prologue
    .line 447
    const/4 v7, 0x1

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v9}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchToOverviewMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Ljava/lang/Runnable;ZZD)V

    .line 448
    return-void
.end method

.method public changeMarkerSelection(II)V
    .locals 7
    .param p1, "selected"    # I
    .param p2, "unselected"    # I

    .prologue
    .line 727
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v3

    .line 728
    .local v3, "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-eq v3, v4, :cond_1

    .line 729
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changeMarkerSelection: incorrect state:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 755
    :cond_0
    :goto_0
    return-void

    .line 733
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 734
    .local v0, "len":I
    if-lez v0, :cond_0

    .line 735
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changeMarkerSelection {"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "}"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 736
    if-ltz p2, :cond_2

    if-ge p2, v0, :cond_2

    .line 737
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/mapping/MapMarker;

    .line 738
    .local v2, "old":Lcom/here/android/mpa/mapping/MapMarker;
    if-eqz v2, :cond_2

    .line 739
    new-instance v1, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v2}, Lcom/here/android/mpa/mapping/MapMarker;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->unselectedDestinationImage:Lcom/here/android/mpa/common/Image;

    invoke-direct {v1, v4, v5}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    .line 740
    .local v1, "marker":Lcom/here/android/mpa/mapping/MapMarker;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-interface {v4, p2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 741
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4, v2}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 742
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 745
    .end local v1    # "marker":Lcom/here/android/mpa/mapping/MapMarker;
    .end local v2    # "old":Lcom/here/android/mpa/mapping/MapMarker;
    :cond_2
    if-ltz p1, :cond_0

    if-ge p1, v0, :cond_0

    .line 746
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/mapping/MapMarker;

    .line 747
    .restart local v2    # "old":Lcom/here/android/mpa/mapping/MapMarker;
    if-eqz v2, :cond_0

    .line 748
    new-instance v1, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v2}, Lcom/here/android/mpa/mapping/MapMarker;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->selectedDestinationImage:Lcom/here/android/mpa/common/Image;

    invoke-direct {v1, v4, v5}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    .line 749
    .restart local v1    # "marker":Lcom/here/android/mpa/mapping/MapMarker;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-interface {v4, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 750
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4, v2}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 751
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    goto/16 :goto_0
.end method

.method public cleanupFluctuator()V
    .locals 2

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cleanupFluctuator"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1372
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->fluctuatorPosListener:Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 1373
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startDestinationFluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 1374
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startDestinationFluctuatorView:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 1375
    return-void
.end method

.method public cleanupMapOverview()V
    .locals 2

    .prologue
    .line 891
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 892
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverviewRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 896
    :goto_0
    return-void

    .line 894
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverviewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public cleanupMapOverviewRoutes()V
    .locals 6

    .prologue
    .line 906
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "cleanupMapOverviewRoutes"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 907
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->overviewMapRouteObjects:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 908
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->overviewMapRouteObjects:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 909
    .local v0, "copy":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/mapping/MapObject;

    .line 910
    .local v1, "mapObject":Lcom/here/android/mpa/mapping/MapObject;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cleanupMapOverviewRoutes:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 912
    .end local v1    # "mapObject":Lcom/here/android/mpa/mapping/MapObject;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObjects(Ljava/util/List;)V

    .line 913
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->overviewMapRouteObjects:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 915
    .end local v0    # "copy":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    :cond_1
    return-void
.end method

.method public clearState()V
    .locals 0

    .prologue
    .line 1189
    return-void
.end method

.method public declared-synchronized enableRendering()V
    .locals 2

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isRenderingEnabled:Z

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "enableRendering:Maps Rendering already enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    :goto_0
    monitor-exit p0

    return-void

    .line 294
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z

    if-nez v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "enableRendering:Maps Rendering init-engine not complete yet"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 298
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initNavigation()V

    .line 299
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 300
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->installPositionListener()V

    .line 301
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 302
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->getMap()Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapView;->setMap(Lcom/here/android/mpa/mapping/Map;)V

    .line 303
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "enableRendering:Maps Rendering enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 304
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isRenderingEnabled:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 11
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 1197
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getCustomAnimator: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1198
    sget-object v8, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 1266
    :cond_0
    :goto_0
    return-void

    .line 1206
    :pswitch_0
    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    if-ne p1, v8, :cond_1

    .line 1207
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkLeft_X:I

    .line 1208
    .local v1, "iconIndicatorX":I
    sget v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewShrinkLeftX:I

    .line 1209
    .local v7, "mapViewX":I
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkLeftX:I

    .line 1210
    .local v4, "mapMaskX":I
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkWidth:I

    .line 1219
    .local v2, "mapMaskWidth":I
    :goto_1
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    int-to-float v9, v1

    invoke-static {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1220
    .local v0, "iconIndicatorAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1223
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    int-to-float v9, v7

    invoke-static {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1224
    .local v6, "mapViewAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1227
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    int-to-float v9, v4

    invoke-static {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1228
    .local v5, "mapMaskXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    invoke-static {v8, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 1229
    .local v3, "mapMaskWidthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {p2, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1230
    invoke-virtual {p2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1232
    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    if-ne p1, v8, :cond_0

    .line 1233
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isAcquiringLocation()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1234
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    sget-object v9, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v8, v9, p2}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    goto :goto_0

    .line 1212
    .end local v0    # "iconIndicatorAnimator":Landroid/animation/ObjectAnimator;
    .end local v1    # "iconIndicatorX":I
    .end local v2    # "mapMaskWidth":I
    .end local v3    # "mapMaskWidthAnimator":Landroid/animation/ValueAnimator;
    .end local v4    # "mapMaskX":I
    .end local v5    # "mapMaskXAnimator":Landroid/animation/ObjectAnimator;
    .end local v6    # "mapViewAnimator":Landroid/animation/ObjectAnimator;
    .end local v7    # "mapViewX":I
    :cond_1
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkModeX:I

    .line 1213
    .restart local v1    # "iconIndicatorX":I
    sget v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewShrinkModeX:I

    .line 1214
    .restart local v7    # "mapViewX":I
    const/4 v4, 0x0

    .line 1215
    .restart local v4    # "mapMaskX":I
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkWidth:I

    .restart local v2    # "mapMaskWidth":I
    goto :goto_1

    .line 1236
    .restart local v0    # "iconIndicatorAnimator":Landroid/animation/ObjectAnimator;
    .restart local v3    # "mapMaskWidthAnimator":Landroid/animation/ValueAnimator;
    .restart local v5    # "mapMaskXAnimator":Landroid/animation/ObjectAnimator;
    .restart local v6    # "mapViewAnimator":Landroid/animation/ObjectAnimator;
    :cond_2
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    sget-object v9, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    goto :goto_0

    .line 1244
    .end local v0    # "iconIndicatorAnimator":Landroid/animation/ObjectAnimator;
    .end local v1    # "iconIndicatorX":I
    .end local v2    # "mapMaskWidth":I
    .end local v3    # "mapMaskWidthAnimator":Landroid/animation/ValueAnimator;
    .end local v4    # "mapMaskX":I
    .end local v5    # "mapMaskXAnimator":Landroid/animation/ObjectAnimator;
    .end local v6    # "mapViewAnimator":Landroid/animation/ObjectAnimator;
    .end local v7    # "mapViewX":I
    :pswitch_1
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    sget v9, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorX:I

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1245
    .restart local v0    # "iconIndicatorAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1248
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    sget v9, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewX:I

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1249
    .restart local v6    # "mapViewAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1252
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    sget v9, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskX:I

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1253
    .restart local v5    # "mapMaskXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    sget v10, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->fullWidth:I

    add-int/lit8 v10, v10, 0x1

    invoke-static {v8, v9, v10}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;II)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 1254
    .restart local v3    # "mapMaskWidthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {p2, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1255
    invoke-virtual {p2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1257
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isAcquiringLocation()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1259
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    sget-object v9, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v8, v9, p2}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    goto/16 :goto_0

    .line 1261
    :cond_3
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    sget-object v9, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    goto/16 :goto_0

    .line 1198
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getFluctuatorTransformListener()Lcom/here/android/mpa/mapping/Map$OnTransformListener;
    .locals 1

    .prologue
    .line 1419
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->fluctuatorPosListener:Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    return-object v0
.end method

.method public getMapController()Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object v0
.end method

.method public getMapIconIndicatorView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getMapViewX()I
    .locals 1

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/MapView;->getX()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V
    .locals 0
    .param p1, "builder"    # Landroid/animation/AnimatorSet$Builder;
    .param p2, "out"    # Z

    .prologue
    .line 1268
    return-void
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 198
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 199
    return-void
.end method

.method public isAcquiringLocation()Z
    .locals 1

    .prologue
    .line 1367
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->isAcquiringLocation()Z

    move-result v0

    return v0
.end method

.method public isOverviewMapMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1355
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    if-nez v1, :cond_0

    .line 1363
    :goto_0
    return v0

    .line 1358
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1361
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1358
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public layoutMap()V
    .locals 0

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter()V

    .line 372
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 1351
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 260
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitializing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "engine initializing, wait"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handleEngineInit()V

    goto :goto_0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 1278
    return-void
.end method

.method public onEngineInitialized(Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handleEngineInit()V

    .line 271
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 188
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 189
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 190
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 191
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    .line 192
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 193
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initViews()V

    .line 194
    return-void
.end method

.method public onGeoPositionChange(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 2
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v0, :cond_0

    .line 205
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 207
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    goto :goto_0

    .line 213
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 1286
    const/4 v0, 0x0

    return v0
.end method

.method public onHudNetworkInit(Lcom/navdy/hud/app/framework/network/NetworkStateManager$HudNetworkInitialized;)V
    .locals 9
    .param p1, "event"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager$HudNetworkInitialized;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 995
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkPrevRoute:hud n/w is initialized: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->networkCheckRequired:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 996
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->networkCheckRequired:Z

    if-eqz v0, :cond_1

    .line 997
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->networkCheckRequired:Z

    .line 998
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    if-nez v0, :cond_0

    .line 999
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .line 1001
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    .line 1002
    .local v6, "center":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-direct {p0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startPreviousRoute(Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 1004
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "send map engine ready event-2"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1005
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    invoke-direct {v8, v0}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1007
    .end local v6    # "center":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_1
    return-void
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 1292
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$12;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1304
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1294
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->ZOOM_IN:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 1298
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->ZOOM_OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 1292
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLocationFixChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$LocationFix;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 361
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "location fix event locationAvailable["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->locationAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] phone["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingPhoneLocation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] gps ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingLocalGpsLocation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isAcquiringLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->initComplete:Z

    if-eqz v0, :cond_0

    .line 364
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapCenter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->hideLocationUI()V

    .line 368
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1509
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->paused:Z

    if-eqz v0, :cond_0

    .line 1516
    :goto_0
    return-void

    .line 1512
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1513
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->paused:Z

    .line 1514
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->stopMapRendering()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1520
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->paused:Z

    if-nez v0, :cond_0

    .line 1527
    :goto_0
    return-void

    .line 1523
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1524
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->paused:Z

    .line 1525
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->startMapRendering()V

    goto :goto_0
.end method

.method public onSettingsChange(Lcom/navdy/hud/app/config/SettingsManager$SettingsChanged;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/config/SettingsManager$SettingsChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 987
    iget-object v0, p1, Lcom/navdy/hud/app/config/SettingsManager$SettingsChanged;->setting:Lcom/navdy/hud/app/config/Setting;

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsUtils;->SHOW_RAW_GPS:Lcom/navdy/hud/app/config/BooleanSetting;

    if-ne v0, v1, :cond_0

    .line 988
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->updateMapIndicator()V

    .line 990
    :cond_0
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 1281
    return-void
.end method

.method public onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handleEngineInit()V

    .line 310
    return-void
.end method

.method public resetMapOverview()V
    .locals 2

    .prologue
    .line 865
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 866
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->resetMapOverviewRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 871
    :goto_0
    return-void

    .line 868
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->resetMapOverviewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public resetTopViewsAnimator()V
    .locals 0

    .prologue
    .line 1270
    return-void
.end method

.method public setMapMaskVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1530
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMask:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1531
    return-void
.end method

.method public setMapToArMode(ZZZLcom/here/android/mpa/common/GeoPosition;DDZ)V
    .locals 15
    .param p1, "animate"    # Z
    .param p2, "changeState"    # Z
    .param p3, "useLastZoom"    # Z
    .param p4, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p5, "zoom"    # D
    .param p7, "tilt"    # D
    .param p9, "cleanupMapOverview"    # Z

    .prologue
    .line 1060
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1061
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v13

    new-instance v2, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;

    move-object v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move/from16 v12, p9

    invoke-direct/range {v2 .. v12}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;ZZZLcom/here/android/mpa/common/GeoPosition;DDZ)V

    const/16 v3, 0x11

    invoke-virtual {v13, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1070
    :goto_0
    return-void

    .line 1068
    :cond_0
    move-wide/from16 v0, p7

    double-to-float v10, v0

    move-object v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-wide/from16 v8, p5

    move/from16 v11, p9

    invoke-direct/range {v3 .. v11}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapToArModeInternal(ZZZLcom/here/android/mpa/common/GeoPosition;DFZ)V

    goto :goto_0
.end method

.method public setMapViewX(I)V
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapView:Lcom/here/android/mpa/mapping/MapView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapView;->setX(F)V

    .line 1535
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 3
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setview: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 247
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setViewMapIconIndicator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 248
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setViewMap(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 249
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setViewMapMask(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 250
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->noLocationView:Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 251
    return-void
.end method

.method public showOverviewMap(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    .locals 10
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "currentMapRoute"    # Lcom/here/android/mpa/mapping/MapRoute;
    .param p3, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "isArrived"    # Z

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showOverviewMap"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1043
    const/4 v5, 0x0

    const/4 v7, 0x0

    const-wide/high16 v8, 0x4028000000000000L    # 12.0

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v9}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchToOverviewMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Ljava/lang/Runnable;ZZD)V

    .line 1044
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1045
    return-void
.end method

.method public showRouteMap(Lcom/here/android/mpa/common/GeoPosition;DF)V
    .locals 14
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p2, "zoom"    # D
    .param p4, "tilt"    # F

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1048
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "showRouteMap"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1049
    move/from16 v0, p4

    float-to-double v10, v0

    move-object v3, p0

    move v6, v4

    move-object v7, p1

    move-wide/from16 v8, p2

    move v12, v5

    invoke-virtual/range {v3 .. v12}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapToArMode(ZZZLcom/here/android/mpa/common/GeoPosition;DDZ)V

    .line 1050
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1051
    return-void
.end method

.method public showStartMarker()V
    .locals 2

    .prologue
    .line 844
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showStartMarker"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 845
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v0, :cond_0

    .line 846
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 848
    :cond_0
    return-void
.end method

.method public startFluctuator()V
    .locals 2

    .prologue
    .line 1378
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getFluctuatorTransformListener()Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 1379
    return-void
.end method

.method public switchBackfromRouteSearchMode(Z)V
    .locals 19
    .param p1, "addCurrentRoute"    # Z

    .prologue
    .line 759
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v18

    .line 760
    .local v18, "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-object/from16 v0, v18

    if-eq v0, v3, :cond_0

    .line 761
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchBackfromRouteSearchMode: invalid state:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 841
    :goto_0
    return-void

    .line 764
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "map-route- switchBackfromRouteSearchMode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 766
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 767
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getSafetySpotListener()Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->setSafetySpotsEnabledOnMap(Z)V

    .line 770
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v14

    .line 771
    .local v14, "len":I
    if-lez v14, :cond_2

    .line 772
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchBackfromRouteSearchMode remove map markers:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 773
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObjects(Ljava/util/List;)V

    .line 774
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    .line 778
    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapMaskVisibility(I)V

    .line 781
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapViewX(I)V

    .line 784
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->navigationViewsContainer:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 786
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 787
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v3, v4, :cond_3

    .line 788
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 797
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->TRANSITION:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 798
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isManualZoom()Z

    move-result v15

    .line 799
    .local v15, "manualZoom":Z
    if-nez v15, :cond_4

    .line 800
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    const/4 v12, 0x0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v12}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapToArMode(ZZZLcom/here/android/mpa/common/GeoPosition;DDZ)V

    .line 801
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 804
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v3, :cond_5

    .line 805
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 808
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v3, :cond_6

    .line 809
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 813
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "map-route- switchBackfromRouteSearchMode cleanup routes"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 814
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverviewRoutes()V

    .line 817
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v13

    .line 818
    .local v13, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v13}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v17

    .line 819
    .local v17, "navRoute":Lcom/here/android/mpa/routing/Route;
    if-eqz v17, :cond_7

    invoke-virtual {v13}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v13}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v3

    if-nez v3, :cond_7

    .line 820
    if-eqz p1, :cond_a

    .line 821
    new-instance v16, Lcom/here/android/mpa/mapping/MapRoute;

    invoke-direct/range {v16 .. v17}, Lcom/here/android/mpa/mapping/MapRoute;-><init>(Lcom/here/android/mpa/routing/Route;)V

    .line 822
    .local v16, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/here/android/mpa/mapping/MapRoute;->setTrafficEnabled(Z)Lcom/here/android/mpa/mapping/MapRoute;

    .line 823
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->addCurrentRoute(Lcom/here/android/mpa/mapping/MapRoute;)V

    .line 824
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "map-route- switchBackfromRouteSearchMode: nav route added"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 832
    .end local v16    # "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    :cond_7
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationMarker()Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v2

    .line 833
    .local v2, "destinationMarker":Lcom/here/android/mpa/mapping/MapMarker;
    if-eqz v2, :cond_8

    .line 834
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "switchBackfromRouteSearchMode: destination marker added"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 835
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 838
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupFluctuator()V

    .line 840
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->setZoom()Z

    goto/16 :goto_0

    .line 791
    .end local v2    # "destinationMarker":Lcom/here/android/mpa/mapping/MapMarker;
    .end local v13    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v15    # "manualZoom":Z
    .end local v17    # "navRoute":Lcom/here/android/mpa/routing/Route;
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setVisibility(I)V

    .line 792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v3, v4, :cond_3

    .line 793
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v3, v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    goto/16 :goto_1

    .line 826
    .restart local v13    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .restart local v15    # "manualZoom":Z
    .restart local v17    # "navRoute":Lcom/here/android/mpa/routing/Route;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "map-route- switchBackfromRouteSearchMode: nav route not added back"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public switchScreen()V
    .locals 6

    .prologue
    .line 1011
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v1

    .line 1012
    .local v1, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v1, :cond_4

    .line 1013
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v2

    .line 1014
    .local v2, "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v2, :cond_0

    .line 1015
    iget-object v3, v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->globalPreferences:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v4

    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/activity/Main;->saveHomeScreenPreference(Landroid/content/SharedPreferences;I)V

    .line 1016
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    .line 1019
    :cond_0
    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    if-eq v3, v4, :cond_3

    .line 1021
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "current screen is not map:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1022
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    .line 1023
    .local v0, "main":Lcom/navdy/hud/app/ui/activity/Main;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1024
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "switching, but collpasing notif first"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1025
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setShowCollapsedNotification(Z)V

    .line 1029
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v5, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1036
    .end local v0    # "main":Lcom/navdy/hud/app/ui/activity/Main;
    .end local v2    # "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :goto_1
    return-void

    .line 1027
    .restart local v0    # "main":Lcom/navdy/hud/app/ui/activity/Main;
    .restart local v2    # "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "switched to map"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1031
    .end local v0    # "main":Lcom/navdy/hud/app/ui/activity/Main;
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "current screen is map no switch reqd"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 1034
    .end local v2    # "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_4
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "current screen is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public switchToRouteSearchMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZLjava/util/List;I)V
    .locals 21
    .param p1, "from"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "to"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "boundingBox"    # Lcom/here/android/mpa/common/GeoBoundingBox;
    .param p4, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p5, "startFluctuator"    # Z
    .param p7, "destinationIcon"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Lcom/here/android/mpa/common/GeoBoundingBox;",
            "Lcom/here/android/mpa/routing/Route;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 557
    .local p6, "markers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "switchToRouteSearchMode: start["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "] end ["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "] x="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getMapViewX()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 559
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 560
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getSafetySpotListener()Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->setSafetySpotsEnabledOnMap(Z)V

    .line 563
    :cond_0
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapMaskVisibility(I)V

    .line 566
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterMoveX:I

    neg-int v4, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapViewX(I)V

    .line 569
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->navigationViewsContainer:Landroid/widget/RelativeLayout;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 570
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setVisibility(I)V

    .line 572
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_a

    .line 573
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 578
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 580
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapIconIndicator:Landroid/widget/ImageView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 588
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverview()V

    .line 591
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 592
    move-object/from16 v6, p1

    .line 593
    .local v6, "center":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v6, :cond_1

    .line 594
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    .line 597
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    sget-object v7, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    const-wide/high16 v8, 0x402f000000000000L    # 15.5

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenterForState(Lcom/navdy/hud/app/maps/here/HereMapController$State;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    .end local v6    # "center":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v15

    .line 606
    .local v15, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v15}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeCurrentRoute()Z

    move-result v12

    .line 607
    .local v12, "b":Z
    if-eqz v12, :cond_b

    .line 608
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "map-route- switchToRouteSearchMode: nav route removed"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 614
    :goto_2
    invoke-virtual {v15}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationMarker()Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v13

    .line 615
    .local v13, "destMarker":Lcom/here/android/mpa/mapping/MapMarker;
    if-eqz v13, :cond_2

    .line 616
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4, v13}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "switchToRouteSearchMode: dest removed"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 622
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-nez v4, :cond_3

    .line 623
    new-instance v17, Lcom/here/android/mpa/common/Image;

    invoke-direct/range {v17 .. v17}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 624
    .local v17, "image":Lcom/here/android/mpa/common/Image;
    const v4, 0x7f020108

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V

    .line 625
    new-instance v4, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-direct {v4}, Lcom/here/android/mpa/mapping/MapMarker;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/here/android/mpa/mapping/MapMarker;->setIcon(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 628
    .end local v17    # "image":Lcom/here/android/mpa/common/Image;
    :cond_3
    if-eqz p1, :cond_4

    .line 629
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 630
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 638
    :cond_4
    :goto_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-nez v4, :cond_5

    .line 639
    new-instance v17, Lcom/here/android/mpa/common/Image;

    invoke-direct/range {v17 .. v17}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 640
    .restart local v17    # "image":Lcom/here/android/mpa/common/Image;
    const v4, 0x7f0201a4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V

    .line 641
    new-instance v4, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-direct {v4}, Lcom/here/android/mpa/mapping/MapMarker;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 642
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/here/android/mpa/mapping/MapMarker;->setIcon(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 644
    .end local v17    # "image":Lcom/here/android/mpa/common/Image;
    :cond_5
    if-eqz p2, :cond_6

    .line 645
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 646
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->endMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 652
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_7

    .line 653
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObjects(Ljava/util/List;)V

    .line 654
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    .line 657
    :cond_7
    if-eqz p6, :cond_e

    .line 658
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v18

    .line 661
    .local v18, "len":I
    :try_start_3
    new-instance v4, Lcom/here/android/mpa/common/Image;

    invoke-direct {v4}, Lcom/here/android/mpa/common/Image;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->selectedDestinationImage:Lcom/here/android/mpa/common/Image;

    .line 662
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->selectedDestinationImage:Lcom/here/android/mpa/common/Image;

    const/4 v5, -0x1

    move/from16 v0, p7

    if-ne v0, v5, :cond_8

    const p7, 0x7f0201a5

    .end local p7    # "destinationIcon":I
    :cond_8
    move/from16 v0, p7

    invoke-virtual {v4, v0}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    .line 667
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->unselectedDestinationImage:Lcom/here/android/mpa/common/Image;

    if-nez v4, :cond_9

    .line 669
    :try_start_4
    new-instance v4, Lcom/here/android/mpa/common/Image;

    invoke-direct {v4}, Lcom/here/android/mpa/common/Image;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->unselectedDestinationImage:Lcom/here/android/mpa/common/Image;

    .line 670
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->unselectedDestinationImage:Lcom/here/android/mpa/common/Image;

    const v5, 0x7f0200f7

    invoke-virtual {v4, v5}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    .line 676
    :cond_9
    :goto_6
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_7
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_d

    .line 677
    move-object/from16 v0, p6

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 678
    .local v14, "g":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v14, :cond_c

    .line 679
    new-instance v19, Lcom/here/android/mpa/mapping/MapMarker;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->unselectedDestinationImage:Lcom/here/android/mpa/common/Image;

    move-object/from16 v0, v19

    invoke-direct {v0, v14, v4}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    .line 680
    .local v19, "mapObject":Lcom/here/android/mpa/mapping/MapObject;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 681
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 676
    .end local v19    # "mapObject":Lcom/here/android/mpa/mapping/MapObject;
    :goto_8
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 575
    .end local v12    # "b":Z
    .end local v13    # "destMarker":Lcom/here/android/mpa/mapping/MapMarker;
    .end local v14    # "g":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v15    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v16    # "i":I
    .end local v18    # "len":I
    .restart local p7    # "destinationIcon":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v4, v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    goto/16 :goto_0

    .line 598
    :catch_0
    move-exception v20

    .line 600
    .local v20, "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 610
    .end local v20    # "t":Ljava/lang/Throwable;
    .restart local v12    # "b":Z
    .restart local v15    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "map-route- switchToRouteSearchMode: nav route not removed, does not exist"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 632
    .restart local v13    # "destMarker":Lcom/here/android/mpa/mapping/MapMarker;
    :catch_1
    move-exception v20

    .line 633
    .restart local v20    # "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 648
    .end local v20    # "t":Ljava/lang/Throwable;
    :catch_2
    move-exception v20

    .line 649
    .restart local v20    # "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 663
    .end local v20    # "t":Ljava/lang/Throwable;
    .end local p7    # "destinationIcon":I
    .restart local v18    # "len":I
    :catch_3
    move-exception v20

    .line 664
    .restart local v20    # "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_5

    .line 671
    .end local v20    # "t":Ljava/lang/Throwable;
    :catch_4
    move-exception v20

    .line 672
    .restart local v20    # "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 683
    .end local v20    # "t":Ljava/lang/Throwable;
    .restart local v14    # "g":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v16    # "i":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapMarkerList:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 686
    .end local v14    # "g":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "addded dest marker:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 689
    .end local v16    # "i":I
    .end local v18    # "len":I
    :cond_e
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->zoomToBoundBox(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZZ)V

    .line 690
    return-void
.end method

.method public updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;)V
    .locals 3
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;

    .prologue
    .line 394
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v1

    .line 396
    .local v1, "navigationActive":Z
    if-eqz v1, :cond_0

    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeMapHeight:I

    .line 397
    .local v0, "mapHeight":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->adjustMaplayoutHeight(I)V

    .line 398
    return-void

    .line 396
    .end local v0    # "mapHeight":I
    :cond_0
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapHeight:I

    goto :goto_0
.end method

.method public zoomToBoundBox(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZZ)V
    .locals 7
    .param p1, "boundingBox"    # Lcom/here/android/mpa/common/GeoBoundingBox;
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p3, "startFluctuator"    # Z
    .param p4, "cleanExistingRoutes"    # Z

    .prologue
    .line 697
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v1

    .line 698
    .local v1, "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-eq v1, v3, :cond_0

    .line 699
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "zoomToBoundBox: incorrect state:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 724
    :goto_0
    return-void

    .line 703
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupFluctuator()V

    .line 704
    if-eqz p3, :cond_1

    .line 705
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startFluctuator()V

    .line 707
    :cond_1
    if-eqz p1, :cond_4

    .line 708
    if-eqz p4, :cond_2

    .line 709
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverviewRoutes()V

    .line 711
    :cond_2
    if-eqz p2, :cond_3

    .line 712
    new-instance v0, Lcom/here/android/mpa/mapping/MapRoute;

    invoke-direct {v0, p2}, Lcom/here/android/mpa/mapping/MapRoute;-><init>(Lcom/here/android/mpa/routing/Route;)V

    .line 713
    .local v0, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/here/android/mpa/mapping/MapRoute;->setTrafficEnabled(Z)Lcom/here/android/mpa/mapping/MapRoute;

    .line 714
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->addMapOverviewRoutes(Lcom/here/android/mpa/mapping/MapRoute;)V

    .line 716
    .end local v0    # "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->routePickerViewRect:Lcom/here/android/mpa/common/ViewRect;

    sget-object v5, Lcom/here/android/mpa/mapping/Map$Animation;->BOW:Lcom/here/android/mpa/mapping/Map$Animation;

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual {v3, p1, v4, v5, v6}, Lcom/navdy/hud/app/maps/here/HereMapController;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 717
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "zoomed to bbox"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 721
    :catch_0
    move-exception v2

    .line 722
    .local v2, "t":Ljava/lang/Throwable;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 719
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "zoomed to bbox: null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
