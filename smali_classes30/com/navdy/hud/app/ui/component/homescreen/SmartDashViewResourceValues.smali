.class public Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;
.super Ljava/lang/Object;
.source "SmartDashViewResourceValues.java"


# static fields
.field public static final leftGaugeActiveX:I

.field public static final leftGaugeActiveY:I

.field public static final leftGaugeOpenX:I

.field public static final leftGaugeOpenY:I

.field public static final leftGaugeShrinkRightX:I

.field public static final middleGaugeActiveHeight:I

.field public static final middleGaugeActiveShrinkLeftX:I

.field public static final middleGaugeActiveShrinkRightX:I

.field public static final middleGaugeActiveWidth:I

.field public static final middleGaugeActiveX:I

.field public static final middleGaugeActiveY:I

.field public static final middleGaugeLargeText:I

.field public static final middleGaugeOpenHeight:I

.field public static final middleGaugeOpenWidth:I

.field public static final middleGaugeOpenX:I

.field public static final middleGaugeOpenY:I

.field public static final middleGaugeShrinkLeftX:I

.field public static final middleGaugeShrinkRightX:I

.field public static final middleGaugeSmallText:I

.field public static final openAnimationDistance:I

.field public static final rightGaugeActiveX:I

.field public static final rightGaugeActiveY:I

.field public static final rightGaugeOpenX:I

.field public static final rightGaugeOpenY:I

.field public static final rightGaugeShrinkLeftX:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 71
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b013f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->leftGaugeOpenX:I

    .line 75
    const v1, 0x7f0b0140

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->leftGaugeOpenY:I

    .line 77
    const v1, 0x7f0b0143

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeOpenX:I

    .line 78
    const v1, 0x7f0b0144

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeOpenY:I

    .line 80
    const v1, 0x7f0b0145

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->rightGaugeOpenX:I

    .line 81
    const v1, 0x7f0b0146

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->rightGaugeOpenY:I

    .line 85
    const v1, 0x7f0b0130

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->leftGaugeActiveX:I

    .line 86
    const v1, 0x7f0b0131

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->leftGaugeActiveY:I

    .line 88
    const v1, 0x7f0b0134

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeActiveX:I

    .line 89
    const v1, 0x7f0b0135

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeActiveY:I

    .line 91
    const v1, 0x7f0b0136

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->rightGaugeActiveX:I

    .line 92
    const v1, 0x7f0b0137

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->rightGaugeActiveY:I

    .line 95
    const v1, 0x7f0b013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeShrinkLeftX:I

    .line 96
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeShrinkRightX:I

    .line 98
    const v1, 0x7f0b0148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->rightGaugeShrinkLeftX:I

    .line 99
    const v1, 0x7f0b0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->leftGaugeShrinkRightX:I

    .line 101
    const v1, 0x7f0b0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeActiveShrinkLeftX:I

    .line 102
    const v1, 0x7f0b013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeActiveShrinkRightX:I

    .line 104
    const v1, 0x7f0b0147

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->openAnimationDistance:I

    .line 106
    const v1, 0x7f0b0142

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeOpenWidth:I

    .line 107
    const v1, 0x7f0b0141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeOpenHeight:I

    .line 109
    const v1, 0x7f0b0133

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeActiveWidth:I

    .line 110
    const v1, 0x7f0b0132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeActiveHeight:I

    .line 112
    const v1, 0x7f0b013e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeSmallText:I

    .line 113
    const v1, 0x7f0b013b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewResourceValues;->middleGaugeLargeText:I

    .line 114
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
