.class Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "SmartDashView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 766
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 769
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mLeftGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 770
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mRightGaugeViewPager:Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager;->setVisibility(I)V

    .line 771
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    .line 772
    .local v0, "navigationActive":Z
    if-eqz v0, :cond_0

    .line 773
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$6;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const/4 v2, 0x1

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showEta(Z)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$900(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Z)V

    .line 775
    :cond_0
    return-void
.end method
