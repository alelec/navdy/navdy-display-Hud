.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "HomeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->getHomeScreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 101
    .local v0, "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v1

    .line 104
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getHomeScreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    return-object v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;
    invoke-static {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->access$002(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    .line 81
    return-void
.end method

.method protected onUnload()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->access$002(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    .line 86
    return-void
.end method

.method setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->getHomeScreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 94
    .local v0, "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    .line 97
    :cond_0
    return-void
.end method
