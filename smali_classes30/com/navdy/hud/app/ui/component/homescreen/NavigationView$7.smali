.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->updateMapIndicator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field final synthetic val$showRawLocation:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 962
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iput-boolean p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;->val$showRawLocation:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 965
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getPositionIndicator()Lcom/here/android/mpa/mapping/PositionIndicator;

    move-result-object v1

    .line 967
    .local v1, "positionIndicator":Lcom/here/android/mpa/mapping/PositionIndicator;
    :try_start_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;->val$showRawLocation:Z

    if-eqz v3, :cond_0

    .line 968
    new-instance v0, Lcom/here/android/mpa/common/Image;

    invoke-direct {v0}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 969
    .local v0, "image":Lcom/here/android/mpa/common/Image;
    const v3, 0x7f0200c4

    invoke-virtual {v0, v3}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V

    .line 970
    invoke-virtual {v1, v0}, Lcom/here/android/mpa/mapping/PositionIndicator;->setMarker(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/mapping/PositionIndicator;

    .line 972
    .end local v0    # "image":Lcom/here/android/mpa/common/Image;
    :cond_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;->val$showRawLocation:Z

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/mapping/PositionIndicator;->setVisible(Z)Lcom/here/android/mpa/mapping/PositionIndicator;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 976
    :goto_0
    return-void

    .line 973
    :catch_0
    move-exception v2

    .line 974
    .local v2, "t":Ljava/lang/Throwable;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$7;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
