.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;
.super Landroid/widget/FrameLayout;
.source "HomeScreenRightSectionView.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public ejectRightSection()V
    .locals 2

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "ejectRightSection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->removeAllViews()V

    .line 51
    return-void
.end method

.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 4
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 67
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 78
    :goto_0
    return-void

    .line 69
    :pswitch_0
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mainScreenRightSectionShrinkX:I

    int-to-float v2, v2

    invoke-static {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 70
    .local v1, "moveLeft":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 74
    .end local v1    # "moveLeft":Landroid/animation/ObjectAnimator;
    :pswitch_1
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mainScreenRightSectionX:I

    int-to-float v2, v2

    invoke-static {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 75
    .local v0, "expand":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hasView()Z
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public injectRightSection(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "injectRightSection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->removeAllViews()V

    .line 45
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->addView(Landroid/view/View;)V

    .line 46
    return-void
.end method

.method public isViewVisible()Z
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->getX()F

    move-result v0

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mainScreenRightSectionX:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 3
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setview: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 63
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mainScreenRightSectionX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->setX(F)V

    .line 64
    return-void
.end method

.method public updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 3
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;
    .param p2, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 34
    invoke-virtual {p2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v2

    .line 36
    .local v2, "navigationActive":Z
    if-eqz v2, :cond_0

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeMapHeight:I

    .line 37
    .local v1, "mapHeight":I
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 38
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 39
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->requestLayout()V

    .line 40
    return-void

    .line 36
    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v1    # "mapHeight":I
    :cond_0
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapHeight:I

    goto :goto_0
.end method
