.class Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;
.super Ljava/lang/Object;
.source "TbtView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->animateSetInstruction(Landroid/text/SpannableStringBuilder;)Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

.field final synthetic val$spannableStringBuilder:Landroid/text/SpannableStringBuilder;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;Landroid/text/SpannableStringBuilder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    .prologue
    .line 495
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;->val$spannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 508
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 501
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;->val$spannableStringBuilder:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 503
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 504
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 505
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 511
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 497
    return-void
.end method
