.class public Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;
.super Landroid/widget/FrameLayout;
.source "BaseSmartDashView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;
.implements Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;


# instance fields
.field protected bus:Lcom/squareup/otto/Bus;

.field private handler:Landroid/os/Handler;

.field protected homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private lastObdSpeed:I

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field protected uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->lastObdSpeed:I

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->handler:Landroid/os/Handler;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;F)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;
    .param p1, "x1"    # F

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeedLimitInternal(F)V

    return-void
.end method

.method private initViews()V
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentDashboardType()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    move-result-object v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting initial dashtype = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;->Smart:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;->Smart:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setCurrentDashboardType(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentDashboardType()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setDashboard(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;)V

    .line 89
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;)V

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 103
    :cond_2
    return-void
.end method

.method private updateSpeedLimitInternal(F)V
    .locals 5
    .param p1, "speedLimitMetersPerSec"    # F

    .prologue
    .line 243
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v1

    .line 244
    .local v1, "unit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    float-to-double v2, p1

    sget-object v4, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v2, v3, v4, v1}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v2

    int-to-float v0, v2

    .line 245
    .local v0, "speedLimitNativeUnit":F
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeedLimit(I)V

    .line 246
    return-void
.end method


# virtual methods
.method public ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;

    .prologue
    .line 272
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 280
    :goto_0
    return-void

    .line 274
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(Z)V

    goto :goto_0

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method public ObdStateChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 268
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(Z)V

    .line 269
    return-void
.end method

.method protected adjustDashHeight(I)V
    .locals 3
    .param p1, "height"    # I

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 174
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 175
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->invalidate()V

    .line 176
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->requestLayout()V

    .line 178
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapHeight:I

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v1, v2, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->animateToFullMode()V

    .line 182
    :cond_0
    return-void
.end method

.method protected animateToFullMode()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method protected animateToFullModeInternal(Landroid/animation/AnimatorSet$Builder;)V
    .locals 0
    .param p1, "builder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 288
    return-void
.end method

.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 198
    return-void
.end method

.method public getDashBoardType()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentDashboardType()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    move-result-object v0

    return-object v0
.end method

.method public getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V
    .locals 0
    .param p1, "builder"    # Landroid/animation/AnimatorSet$Builder;
    .param p2, "out"    # Z

    .prologue
    .line 200
    return-void
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 2
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 132
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->initializeView()V

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 159
    return-void
.end method

.method protected initializeView()V
    .locals 2

    .prologue
    .line 162
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentSpeedLimit()F

    move-result v0

    .line 163
    .local v0, "speedLimit":F
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeedLimitInternal(F)V

    .line 164
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(Z)V

    .line 165
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 63
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 64
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 65
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 67
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 68
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 69
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->bus:Lcom/squareup/otto/Bus;

    .line 70
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 71
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->initViews()V

    .line 73
    .end local v0    # "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    :cond_0
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 229
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 219
    return-void
.end method

.method public resetTopViewsAnimator()V
    .locals 4

    .prologue
    .line 203
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCustomAnimateMode()Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v0

    .line 204
    .local v0, "mode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode-view:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 206
    return-void
.end method

.method public setDashboard(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;)V
    .locals 2
    .param p1, "dashBoardType"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    .prologue
    .line 112
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$3;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$SmartDashViewConstants$Type:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 123
    :goto_0
    return-void

    .line 114
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setCurrentDashboardType(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;)V

    goto :goto_0

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setCurrentDashboardType(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;)V

    goto :goto_0

    .line 112
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setMiddleGaugeView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 109
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setMiddleGaugeView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 107
    return-void
.end method

.method public shouldShowTopViews()Z
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x1

    return v0
.end method

.method public updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;Z)V
    .locals 3
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;
    .param p2, "forced"    # Z

    .prologue
    .line 185
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v1

    .line 186
    .local v1, "navigationActive":Z
    if-eqz p2, :cond_0

    .line 188
    if-eqz v1, :cond_1

    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeMapHeight:I

    .line 189
    .local v0, "height":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->adjustDashHeight(I)V

    .line 191
    .end local v0    # "height":I
    :cond_0
    return-void

    .line 188
    :cond_1
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapHeight:I

    goto :goto_0
.end method

.method protected updateSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 296
    return-void
.end method

.method protected updateSpeed(Z)V
    .locals 5
    .param p1, "force"    # Z

    .prologue
    .line 249
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v1

    .line 250
    .local v1, "speed":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 251
    const/4 v1, 0x0

    .line 253
    :cond_0
    const/4 v0, 0x0

    .line 255
    .local v0, "changed":Z
    if-nez p1, :cond_1

    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->lastObdSpeed:I

    if-eq v1, v2, :cond_2

    .line 256
    :cond_1
    iput v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->lastObdSpeed:I

    .line 257
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(I)V

    .line 258
    const/4 v0, 0x1

    .line 261
    :cond_2
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 262
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SPEED updated :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 264
    :cond_3
    return-void
.end method

.method protected updateSpeedLimit(I)V
    .locals 0
    .param p1, "speedLimit"    # I

    .prologue
    .line 300
    return-void
.end method
