.class public Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants;
.super Ljava/lang/Object;
.source "SmartDashViewConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;
    }
.end annotation


# static fields
.field public static final DEFAULT_SPEED_LIMIT_THRESHOLD_KMPH:I = 0xd

.field public static final DEFAULT_SPEED_LIMIT_THRESHOLD_MPH:I = 0x8

.field public static final PREFERENCE_SPEED_LIMIT_THRESHOLD:Ljava/lang/String; = "PREFERENCE_SPEED_LIMIT_THRESHOLD"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
