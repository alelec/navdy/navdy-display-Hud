.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;
.super Ljava/lang/Object;
.source "HomeScreenConstants.java"


# static fields
.field public static final EMPTY:Ljava/lang/String; = ""

.field public static final INITIAL_ROUTE_TIMEOUT:I = 0x1388

.field public static final MINUTES_DAY:I = 0x5a0

.field public static final MINUTES_HOUR:I = 0x3c

.field public static final MIN_RENAVIGATION_DISTANCE_METERS:I = 0x1f4

.field public static final MIN_RENAVIGATION_DISTANCE_METERS_GAS:D = 500.0

.field public static final SELECTION_ROUTE_TIMEOUT:I = 0x2710

.field public static final SPACE:Ljava/lang/String; = " "

.field public static final SPEED_KM:Ljava/lang/String;

.field public static final SPEED_METERS:Ljava/lang/String;

.field public static final SPEED_MPH:Ljava/lang/String;

.field public static final TOP_ANIMATION_INITIAL_OUT_INTERVAL:I = 0x7530

.field public static final TOP_ANIMATION_OUT_INTERVAL:I = 0x2710

.field public static final routeOverviewRect:Lcom/here/android/mpa/common/ViewRect;

.field public static final routePickerViewRect:Lcom/here/android/mpa/common/ViewRect;

.field public static final transformCenterLargeBottom:Landroid/graphics/PointF;

.field public static final transformCenterLargeMiddle:Landroid/graphics/PointF;

.field public static final transformCenterPicker:Landroid/graphics/PointF;

.field public static final transformCenterSmallBottom:Landroid/graphics/PointF;

.field public static final transformCenterSmallMiddle:Landroid/graphics/PointF;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 48
    new-instance v1, Lcom/here/android/mpa/common/ViewRect;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxX:I

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxY:I

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxWidth:I

    sget v5, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxHeight:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/here/android/mpa/common/ViewRect;-><init>(IIII)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->routePickerViewRect:Lcom/here/android/mpa/common/ViewRect;

    .line 54
    new-instance v1, Lcom/here/android/mpa/common/ViewRect;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxX:I

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxY:I

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxWidth:I

    sget v5, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxHeight:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/here/android/mpa/common/ViewRect;-><init>(IIII)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->routeOverviewRect:Lcom/here/android/mpa/common/ViewRect;

    .line 60
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0901aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->SPEED_MPH:Ljava/lang/String;

    .line 63
    const v1, 0x7f090196

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->SPEED_KM:Ljava/lang/String;

    .line 64
    const v1, 0x7f0901a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->SPEED_METERS:Ljava/lang/String;

    .line 68
    new-instance v1, Landroid/graphics/PointF;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewX:I

    int-to-float v2, v2

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterMapOnRouteOverviewY:I

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterSmallMiddle:Landroid/graphics/PointF;

    .line 73
    new-instance v1, Landroid/graphics/PointF;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterX:I

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterIconWidth:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterMapOnRouteY:I

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterSmallBottom:Landroid/graphics/PointF;

    .line 78
    new-instance v1, Landroid/graphics/PointF;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewX:I

    int-to-float v2, v2

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewY:I

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterLargeMiddle:Landroid/graphics/PointF;

    .line 83
    new-instance v1, Landroid/graphics/PointF;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterX:I

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterIconWidth:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewRouteY:I

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterLargeBottom:Landroid/graphics/PointF;

    .line 88
    new-instance v1, Landroid/graphics/PointF;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewX:I

    int-to-float v2, v2

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterPickerY:I

    int-to-float v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->transformCenterPicker:Landroid/graphics/PointF;

    .line 89
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
