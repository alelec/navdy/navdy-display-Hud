.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "HomeScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;,
        Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Module;,
        Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f03004d
.end annotation


# static fields
.field private static presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    .prologue
    .line 32
    sput-object p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    return-object p0
.end method


# virtual methods
.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 56
    const/high16 v0, 0x10a0000

    return v0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 61
    const v0, 0x10a0001

    return v0
.end method

.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Module;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;)V

    return-object v0
.end method

.method public getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHomeScreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->getHomeScreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method

.method public setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .prologue
    .line 116
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    .line 119
    :cond_0
    return-void
.end method
