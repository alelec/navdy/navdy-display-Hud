.class synthetic Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;
.super Ljava/lang/Object;
.source "HomeScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 666
    invoke-static {}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->values()[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    .line 629
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    .line 297
    :goto_3
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->values()[Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_4

    .line 629
    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_2

    .line 666
    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_0
.end method
