.class public Lcom/navdy/hud/app/ui/component/homescreen/TimeView;
.super Landroid/widget/RelativeLayout;
.source "TimeView.java"


# static fields
.field public static final CLOCK_UPDATE_INTERVAL:I = 0x7530


# instance fields
.field ampmTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c9
    .end annotation
.end field

.field private bus:Lcom/squareup/otto/Bus;

.field dayTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00de
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private runnable:Ljava/lang/Runnable;

.field private stringBuilder:Ljava/lang/StringBuilder;

.field private timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c8
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->stringBuilder:Ljava/lang/StringBuilder;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)Lcom/navdy/hud/app/common/TimeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->updateTime()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->runnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private updateTime()V
    .locals 4

    .prologue
    .line 109
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->dayTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    invoke-virtual {v2}, Lcom/navdy/hud/app/common/TimeHelper;->getDay()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "time":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->ampmTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 101
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 62
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 63
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 64
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 66
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 71
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->updateTime()V

    .line 72
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 73
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 83
    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$2;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->runnable:Ljava/lang/Runnable;

    .line 90
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->runnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {p0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 91
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->bus:Lcom/squareup/otto/Bus;

    .line 92
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onTimeSettingsChange(Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->updateTime()V

    .line 106
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 116
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView$3;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 118
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setX(F)V

    goto :goto_0

    .line 122
    :pswitch_1
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeShrinkLeftX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setX(F)V

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
