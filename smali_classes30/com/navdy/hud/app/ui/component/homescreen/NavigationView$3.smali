.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchToOverviewMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Ljava/lang/Runnable;ZZD)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field final synthetic val$animate:Z

.field final synthetic val$endAction:Ljava/lang/Runnable;

.field final synthetic val$geoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$route:Lcom/here/android/mpa/routing/Route;

.field final synthetic val$zoomLevel:D


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Lcom/here/android/mpa/routing/Route;Ljava/lang/Runnable;Lcom/here/android/mpa/common/GeoCoordinate;ZD)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 464
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$route:Lcom/here/android/mpa/routing/Route;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$endAction:Ljava/lang/Runnable;

    iput-object p4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$geoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

    iput-boolean p5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$animate:Z

    iput-wide p6, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$zoomLevel:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 467
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 468
    .local v10, "l1":J
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isTrafficEnabled()Z

    move-result v8

    .line 469
    .local v8, "isTrafficEnabled":Z
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$route:Lcom/here/android/mpa/routing/Route;

    if-nez v0, :cond_2

    .line 470
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchToOverviewMode:no route traffic="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$300(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 472
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 483
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$geoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$animate:Z

    if-eqz v3, :cond_1

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->BOW:Lcom/here/android/mpa/mapping/Map$Animation;

    :goto_0
    iget-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$zoomLevel:D

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenterForState(Lcom/navdy/hud/app/maps/here/HereMapController$State;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 486
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$geoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 488
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 534
    :cond_0
    :goto_1
    return-void

    .line 483
    :cond_1
    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    goto :goto_0

    .line 495
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchToOverviewMode:route traffic="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 499
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$geoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 500
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 514
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "switchToOverviewMode:map:executeSynchronized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTilt(F)V

    .line 516
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$endAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_4

    .line 517
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3$2;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 530
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setTransformCenter(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$300(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 531
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/Route;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->routeOverviewRect:Lcom/here/android/mpa/common/ViewRect;

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->val$animate:Z

    if-eqz v0, :cond_5

    sget-object v0, Lcom/here/android/mpa/mapping/Map$Animation;->BOW:Lcom/here/android/mpa/mapping/Map$Animation;

    :goto_2
    invoke-virtual {v1, v2, v3, v0, v6}, Lcom/navdy/hud/app/maps/here/HereMapController;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 532
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$3;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchToOverviewMode took ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 531
    :cond_5
    sget-object v0, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    goto :goto_2
.end method
