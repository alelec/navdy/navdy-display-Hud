.class public Lcom/navdy/hud/app/ui/component/homescreen/ImageWidgetPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "ImageWidgetPresenter.java"


# instance fields
.field private id:Ljava/lang/String;

.field private mDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageResource"    # I
    .param p3, "id"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 19
    if-lez p2, :cond_0

    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/ImageWidgetPresenter;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 22
    :cond_0
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/homescreen/ImageWidgetPresenter;->id:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/ImageWidgetPresenter;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/ImageWidgetPresenter;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V
    .locals 1
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;

    .prologue
    .line 27
    if-eqz p1, :cond_0

    .line 28
    const v0, 0x7f03006b

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 30
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;)V

    .line 31
    return-void
.end method

.method protected updateGauge()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method
