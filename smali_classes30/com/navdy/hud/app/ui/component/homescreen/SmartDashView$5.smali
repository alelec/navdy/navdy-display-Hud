.class Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;
.super Ljava/lang/Object;
.source "SmartDashView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

.field final synthetic val$marginParams:Landroid/view/ViewGroup$MarginLayoutParams;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 759
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;->val$marginParams:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 762
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;->val$marginParams:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 763
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mMiddleGaugeView:Lcom/navdy/hud/app/view/GaugeView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$5;->val$marginParams:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/GaugeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 764
    return-void
.end method
