.class public Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;
.super Landroid/widget/LinearLayout;
.source "LaneGuidanceView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private iconH:I

.field private iconW:I

.field private lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private mapIconIndicatorBottomMarginWithLane:I

.field private needsMeasurement:Z

.field private paused:Z

.field private separatorColor:I

.field private separatorH:I

.field private separatorMargin:I

.field private separatorW:I

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method private addSeparator(Landroid/content/Context;ZZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "leftMargin"    # Z
    .param p3, "rightMargin"    # Z

    .prologue
    .line 172
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 173
    .local v1, "separator":Landroid/view/View;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorW:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorH:I

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 174
    .local v0, "lytParams":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v2, 0x50

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 175
    if-eqz p2, :cond_0

    .line 176
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorMargin:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 178
    :cond_0
    if-eqz p3, :cond_1

    .line 179
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorMargin:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 181
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorColor:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 183
    invoke-virtual {p0, v1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    return-void
.end method

.method private getMapIconX(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)I
    .locals 3
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 200
    if-eqz p1, :cond_0

    .line 201
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 217
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 203
    :pswitch_0
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkLeft_X:I

    goto :goto_0

    .line 206
    :pswitch_1
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorX:I

    goto :goto_0

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    .line 210
    .local v0, "rootScreen":Lcom/navdy/hud/app/ui/activity/Main;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 211
    :cond_1
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkLeft_X:I

    goto :goto_0

    .line 213
    :cond_2
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorX:I

    goto :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private hideMapIconIndicator()V
    .locals 2

    .prologue
    .line 195
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getLaneGuidanceIconIndicator()Landroid/widget/ImageView;

    move-result-object v0

    .line 196
    .local v0, "view":Landroid/widget/ImageView;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 197
    return-void
.end method

.method private showMapIconIndicator()V
    .locals 3

    .prologue
    .line 187
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getLaneGuidanceIconIndicator()Landroid/widget/ImageView;

    move-result-object v1

    .line 188
    .local v1, "view":Landroid/widget/ImageView;
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getMapIconX(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setX(F)V

    .line 189
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 190
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->mapIconIndicatorBottomMarginWithLane:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 191
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 192
    return-void
.end method


# virtual methods
.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 5
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getMeasuredWidth()I

    move-result v3

    if-nez v3, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getMeasuredWidth()I

    move-result v1

    .line 108
    .local v1, "w":I
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getMapIconX(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)I

    move-result v0

    .line 109
    .local v0, "mapIconX":I
    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterIconWidth:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    div-int/lit8 v4, v1, 0x2

    sub-int v2, v3, v4

    .line 110
    .local v2, "x":I
    int-to-float v3, v2

    invoke-static {p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 111
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getLaneGuidanceIconIndicator()Landroid/widget/ImageView;

    move-result-object v3

    int-to-float v4, v0

    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 79
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 80
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 81
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->laneGuidanceY:I

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->setY(F)V

    .line 82
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 83
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->bus:Lcom/squareup/otto/Bus;

    .line 84
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 85
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 86
    .local v1, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0b00b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->iconH:I

    .line 87
    const v2, 0x7f0b00b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->iconW:I

    .line 89
    const v2, 0x7f0b00b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorH:I

    .line 90
    const v2, 0x7f0b00b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorW:I

    .line 91
    const v2, 0x7f0b00b8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorMargin:I

    .line 92
    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->separatorColor:I

    .line 94
    const v2, 0x7f0b00c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->mapIconIndicatorBottomMarginWithLane:I

    .line 95
    return-void
.end method

.method public onHideLaneInfo(Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->needsMeasurement:Z

    .line 163
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "LaneGuidanceView:hideLanes"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 165
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->hideMapIconIndicator()V

    .line 166
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->setVisibility(I)V

    .line 167
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->removeAllViews()V

    .line 169
    :cond_0
    return-void
.end method

.method public onLaneInfoShow(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)V
    .locals 11
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 116
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v4

    .line 117
    .local v4, "mode":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    sget-object v6, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-eq v4, v6, :cond_1

    .line 118
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->onHideLaneInfo(Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;)V

    .line 119
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "LaneGuidanceView:showLanes"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 124
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 126
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->paused:Z

    if-nez v6, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->removeAllViews()V

    .line 131
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 132
    .local v0, "context":Landroid/content/Context;
    iget-object v6, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;->laneData:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 134
    .local v5, "size":I
    invoke-direct {p0, v0, v10, v9}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->addSeparator(Landroid/content/Context;ZZ)V

    .line 136
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v5, :cond_4

    .line 137
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 138
    .local v3, "imageView":Landroid/widget/ImageView;
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    iget v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->iconW:I

    iget v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->iconH:I

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    iget-object v6, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;->laneData:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    iget-object v2, v6, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    .line 140
    .local v2, "icons":[Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_3

    array-length v6, v2

    if-lez v6, :cond_3

    .line 141
    new-instance v6, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v6, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 146
    :goto_2
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->addView(Landroid/view/View;)V

    .line 148
    add-int/lit8 v6, v5, -0x1

    if-ge v1, v6, :cond_2

    .line 149
    invoke-direct {p0, v0, v9, v9}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->addSeparator(Landroid/content/Context;ZZ)V

    .line 136
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 143
    :cond_3
    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 153
    .end local v2    # "icons":[Landroid/graphics/drawable/Drawable;
    .end local v3    # "imageView":Landroid/widget/ImageView;
    :cond_4
    invoke-direct {p0, v0, v9, v10}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->addSeparator(Landroid/content/Context;ZZ)V

    .line 154
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->showMapIconIndicator()V

    .line 155
    iput-boolean v9, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->needsMeasurement:Z

    .line 156
    invoke-virtual {p0, v10}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 66
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->needsMeasurement:Z

    if-eqz v3, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getMeasuredWidth()I

    move-result v1

    .line 68
    .local v1, "w":I
    if-lez v1, :cond_0

    .line 69
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getMapIconX(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)I

    move-result v0

    .line 70
    .local v0, "mapIconX":I
    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterIconWidth:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    div-int/lit8 v4, v1, 0x2

    sub-int v2, v3, v4

    .line 71
    .local v2, "x":I
    int-to-float v3, v2

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->setX(F)V

    .line 72
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->needsMeasurement:Z

    .line 75
    .end local v0    # "mapIconX":I
    .end local v1    # "w":I
    .end local v2    # "x":I
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->paused:Z

    if-eqz v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->paused:Z

    .line 232
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause:lane"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 237
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->paused:Z

    if-nez v2, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->paused:Z

    .line 241
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "::onResume:lane"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 243
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v1

    .line 244
    .local v1, "mode":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v1, v2, :cond_0

    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 249
    .local v0, "event":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    if-eqz v0, :cond_0

    .line 250
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 251
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->onLaneInfoShow(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)V

    .line 252
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "::onResume:shown last lane"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 101
    return-void
.end method

.method public showLastEvent()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->onLaneInfoShow(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)V

    .line 224
    :cond_0
    return-void
.end method
