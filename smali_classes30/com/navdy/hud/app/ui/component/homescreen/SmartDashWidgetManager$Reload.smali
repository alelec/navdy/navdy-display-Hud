.class public final enum Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;
.super Ljava/lang/Enum;
.source "SmartDashWidgetManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reload"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

.field public static final enum RELOADED:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

.field public static final enum RELOAD_CACHE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 131
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    const-string v1, "RELOAD_CACHE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOAD_CACHE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    .line 132
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    const-string v1, "RELOADED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOADED:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    .line 130
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOAD_CACHE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOADED:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->$VALUES:[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 130
    const-class v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->$VALUES:[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    return-object v0
.end method
