.class public Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;
.super Landroid/widget/LinearLayout;
.source "SpeedView.java"


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private lastSpeed:I

.field private lastSpeedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field speedUnitView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0196
    .end annotation
.end field

.field speedView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0197
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeed:I

    .line 53
    return-void
.end method

.method private setTrackingSpeed()V
    .locals 5

    .prologue
    .line 109
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v0

    .line 110
    .local v0, "speed":I
    if-gez v0, :cond_0

    .line 111
    const/4 v0, 0x0

    .line 113
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v1

    .line 114
    .local v1, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeed:I

    if-eq v0, v3, :cond_1

    .line 115
    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeed:I

    .line 116
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedView:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    if-eq v1, v3, :cond_2

    .line 119
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 120
    const-string v2, ""

    .line 121
    .local v2, "str":Ljava/lang/String;
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView$1;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 134
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedUnitView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    .end local v2    # "str":Ljava/lang/String;
    :cond_2
    return-void

    .line 123
    .restart local v2    # "str":Ljava/lang/String;
    :pswitch_0
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->SPEED_MPH:Ljava/lang/String;

    .line 124
    goto :goto_0

    .line 127
    :pswitch_1
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->SPEED_KM:Ljava/lang/String;

    .line 128
    goto :goto_0

    .line 131
    :pswitch_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenConstants;->SPEED_METERS:Ljava/lang/String;

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public GPSSpeedChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->setTrackingSpeed()V

    .line 101
    return-void
.end method

.method public ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 91
    :goto_0
    return-void

    .line 88
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->setTrackingSpeed()V

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method public ObdStateChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeed:I

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 81
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->setTrackingSpeed()V

    .line 82
    return-void
.end method

.method public clearState()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeed:I

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->lastSpeedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 141
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->setTrackingSpeed()V

    .line 142
    return-void
.end method

.method public getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V
    .locals 6
    .param p1, "builder"    # Landroid/animation/AnimatorSet$Builder;
    .param p2, "out"    # Z

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    .line 145
    if-eqz p2, :cond_0

    .line 146
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedUnitView:Landroid/widget/TextView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewSpeedOut:I

    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getTranslationXPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 147
    .local v1, "speedUnitXAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 149
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedUnitView:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 150
    .local v0, "speedUnitAlphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 161
    :goto_0
    return-void

    .line 153
    .end local v0    # "speedUnitAlphaAnimator":Landroid/animation/ObjectAnimator;
    .end local v1    # "speedUnitXAnimator":Landroid/animation/ObjectAnimator;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedUnitView:Landroid/widget/TextView;

    invoke-static {v2, v5}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getTranslationXPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 154
    .restart local v1    # "speedUnitXAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 156
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedUnitView:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 157
    .restart local v0    # "speedUnitAlphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 159
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 149
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 156
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 59
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 60
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 64
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->bus:Lcom/squareup/otto/Bus;

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onSpeedDataExpired(Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;)V
    .locals 0
    .param p1, "speedDataExpired"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->setTrackingSpeed()V

    .line 106
    return-void
.end method

.method public onSpeedUnitChanged(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->setTrackingSpeed()V

    .line 96
    return-void
.end method

.method public onSpeedWarningEvent(Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;)V
    .locals 2
    .param p1, "speedWarning"    # Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 70
    iget-boolean v0, p1, Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;->exceed:Z

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedView:Landroid/widget/TextView;

    const v1, -0xff0001

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedView:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public resetTopViewsAnimator()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedUnitView:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedUnitView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->speedView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    return-void
.end method
