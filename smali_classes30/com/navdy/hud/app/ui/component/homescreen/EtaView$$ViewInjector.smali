.class public Lcom/navdy/hud/app/ui/component/homescreen/EtaView$$ViewInjector;
.super Ljava/lang/Object;
.source "EtaView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/homescreen/EtaView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/EtaView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e018a

    const-string v2, "field \'etaTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTextView:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e018c

    const-string v2, "field \'etaTimeLeft\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeLeft:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e018b

    const-string v2, "field \'etaTimeAmPm\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeAmPm:Landroid/widget/TextView;

    .line 16
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/homescreen/EtaView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTextView:Landroid/widget/TextView;

    .line 20
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeLeft:Landroid/widget/TextView;

    .line 21
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->etaTimeAmPm:Landroid/widget/TextView;

    .line 22
    return-void
.end method
