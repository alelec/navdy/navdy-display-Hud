.class Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;
.super Ljava/lang/Object;
.source "BaseSmartDashView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GPSSpeedChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(Z)V

    .line 137
    return-void
.end method

.method public ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V

    .line 157
    return-void
.end method

.method public onMapEvent(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    iget v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentSpeedLimit:F

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeedLimitInternal(F)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;F)V

    .line 147
    return-void
.end method

.method public onSpeedDataExpired(Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;)V
    .locals 2
    .param p1, "speedDataExpired"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(Z)V

    .line 142
    return-void
.end method

.method public onSpeedUnitChanged(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateSpeed(Z)V

    .line 152
    return-void
.end method
