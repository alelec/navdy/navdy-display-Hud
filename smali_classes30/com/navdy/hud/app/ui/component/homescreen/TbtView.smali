.class public Lcom/navdy/hud/app/ui/component/homescreen/TbtView;
.super Landroid/widget/RelativeLayout;
.source "TbtView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;


# static fields
.field static final MANEUVER_PROGRESS_ANIMATION_DURATION:J = 0xfaL

.field private static final MANEUVER_SWAP_DURATION:J = 0x1f4L

.field private static final maneuverSwapInterpolator:Landroid/animation/TimeInterpolator;


# instance fields
.field activeMapDistance:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01b8
    .end annotation
.end field

.field activeMapIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ba
    .end annotation
.end field

.field activeMapInstruction:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01be
    .end annotation
.end field

.field private bus:Lcom/squareup/otto/Bus;

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private initialProgressBarDistance:J

.field private lastDistance:Ljava/lang/String;

.field private lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

.field private lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field private lastMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field private lastRoadText:Ljava/lang/String;

.field private lastTurnIconId:I

.field private lastTurnText:Ljava/lang/String;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field nextMapIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01bd
    .end annotation
.end field

.field nextMapIconContainer:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01bc
    .end annotation
.end field

.field nextMapInstruction:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c0
    .end annotation
.end field

.field nextMapInstructionContainer:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01bf
    .end annotation
.end field

.field nowIcon:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01b9
    .end annotation
.end field

.field private paused:Z

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01bb
    .end annotation
.end field

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->maneuverSwapInterpolator:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnIconId:I

    .line 110
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    return-object v0
.end method

.method private animateSetDistance(Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;)V
    .locals 14
    .param p1, "maneuverState"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .prologue
    const-wide/16 v12, 0xfa

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 430
    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eq v9, v10, :cond_1

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne p1, v9, :cond_1

    move v6, v7

    .line 432
    .local v6, "isTransitioningToNowState":Z
    :goto_0
    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v9, v10, :cond_2

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eq p1, v9, :cond_2

    move v5, v7

    .line 435
    .local v5, "isTransitioningFromNowState":Z
    :goto_1
    if-eqz v6, :cond_3

    .line 437
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeInAndScaleUpAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v2

    .line 438
    .local v2, "fadeInAndScaleUpAnimator":Landroid/animation/AnimatorSet;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    invoke-static {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 440
    .local v3, "fadeOutActiveMapDistanceAnimator":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 441
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 443
    invoke-virtual {v0, v12, v13}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v7

    .line 444
    invoke-virtual {v7}, Landroid/animation/AnimatorSet;->start()V

    .line 456
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v2    # "fadeInAndScaleUpAnimator":Landroid/animation/AnimatorSet;
    .end local v3    # "fadeOutActiveMapDistanceAnimator":Landroid/animation/ObjectAnimator;
    :cond_0
    :goto_2
    return-void

    .end local v5    # "isTransitioningFromNowState":Z
    .end local v6    # "isTransitioningToNowState":Z
    :cond_1
    move v6, v8

    .line 430
    goto :goto_0

    .restart local v6    # "isTransitioningToNowState":Z
    :cond_2
    move v5, v8

    .line 432
    goto :goto_1

    .line 445
    .restart local v5    # "isTransitioningFromNowState":Z
    :cond_3
    if-eqz v5, :cond_0

    .line 447
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-static {v8}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeOutAndScaleDownAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v4

    .line 448
    .local v4, "fadeOutAndScaleDownAnimator":Landroid/animation/AnimatorSet;
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    invoke-static {v8, v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 450
    .local v1, "fadeInActiveMapDistanceAnimator":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 451
    .restart local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 453
    invoke-virtual {v0, v12, v13}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v7

    .line 454
    invoke-virtual {v7}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_2
.end method

.method private animateSetIcon(I)Landroid/animation/AnimatorSet;
    .locals 5
    .param p1, "imageResourceId"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 385
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 387
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionTranslationYAnimation:I

    .line 388
    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getTranslationYPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 389
    .local v0, "sendActiveDownAnim":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionTranslationYAnimation:I

    .line 390
    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getTranslationYPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 392
    .local v1, "sendNextDownAnim":Landroid/animation/ObjectAnimator;
    new-instance v3, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;

    invoke-direct {v3, p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$3;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;I)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 411
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 412
    .local v2, "swapIcons":Landroid/animation/AnimatorSet;
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 414
    return-object v2
.end method

.method private animateSetInstruction(Landroid/text/SpannableStringBuilder;)Landroid/animation/AnimatorSet;
    .locals 5
    .param p1, "spannableStringBuilder"    # Landroid/text/SpannableStringBuilder;

    .prologue
    .line 483
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 486
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstruction:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionTranslationYAnimation:I

    .line 489
    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getTranslationYPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 491
    .local v0, "sendActiveDownAnim":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionTranslationYAnimation:I

    .line 492
    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getTranslationYPositionAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 495
    .local v1, "sendNextDownAnim":Landroid/animation/ObjectAnimator;
    new-instance v3, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;

    invoke-direct {v3, p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$4;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 514
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 515
    .local v2, "swapInstructions":Landroid/animation/AnimatorSet;
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 517
    return-object v2
.end method

.method private cleanInstructions()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 346
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeOutAndScaleDownAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v0

    const-wide/16 v2, 0xfa

    .line 349
    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 350
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 352
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->setWidth(Landroid/view/View;I)V

    .line 354
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 355
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 356
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstruction:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    return-void
.end method

.method private setDistance(Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "maneuverState"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .prologue
    const/4 v1, 0x0

    .line 418
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastDistance:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    :goto_0
    return-void

    .line 421
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastDistance:Ljava/lang/String;

    .line 422
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isMainUIShrunk()Z

    move-result v0

    if-nez v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 426
    :cond_1
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->animateSetDistance(Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;)V

    goto :goto_0
.end method

.method private setIcon(IZ)Landroid/animation/AnimatorSet;
    .locals 2
    .param p1, "imageResourceId"    # I
    .param p2, "hasSameInstruction"    # Z
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 362
    iget v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnIconId:I

    if-ne v1, p1, :cond_0

    .line 379
    :goto_0
    return-object v0

    .line 365
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnIconId:I

    .line 367
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 368
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 375
    if-eqz p2, :cond_2

    .line 376
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 379
    :cond_2
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->animateSetIcon(I)Landroid/animation/AnimatorSet;

    move-result-object v0

    goto :goto_0
.end method

.method private setInstruction(Ljava/lang/String;Ljava/lang/String;)Landroid/animation/AnimatorSet;
    .locals 5
    .param p1, "turnText"    # Ljava/lang/String;
    .param p2, "roadText"    # Ljava/lang/String;

    .prologue
    .line 459
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnText:Ljava/lang/String;

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastRoadText:Ljava/lang/String;

    invoke-static {p2, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 460
    const/4 v2, 0x0

    .line 479
    :goto_0
    return-object v2

    .line 463
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnText:Ljava/lang/String;

    .line 464
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastRoadText:Ljava/lang/String;

    .line 466
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 468
    .local v0, "spannableStringBuilder":Landroid/text/SpannableStringBuilder;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnText:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnText:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->canShowTurnText(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 469
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 470
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 473
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastRoadText:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 474
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 475
    .local v1, "styleSpan":Landroid/text/style/StyleSpan;
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 476
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastRoadText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 479
    .end local v1    # "styleSpan":Landroid/text/style/StyleSpan;
    :cond_2
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->animateSetInstruction(Landroid/text/SpannableStringBuilder;)Landroid/animation/AnimatorSet;

    move-result-object v2

    goto :goto_0
.end method

.method private setProgressBar(Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;J)V
    .locals 10
    .param p1, "maneuverState"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .param p2, "distanceInMeters"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 521
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v7, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eq v6, v7, :cond_1

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne p1, v6, :cond_1

    move v1, v4

    .line 523
    .local v1, "isTransitioningToApproachingState":Z
    :goto_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v7, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v6, v7, :cond_2

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eq p1, v6, :cond_2

    move v0, v4

    .line 526
    .local v0, "isTransitioningFromApproachingState":Z
    :goto_1
    if-eqz v0, :cond_3

    .line 527
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    invoke-static {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->setWidth(Landroid/view/View;I)V

    .line 528
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->initialProgressBarDistance:J

    .line 541
    :cond_0
    :goto_2
    return-void

    .end local v0    # "isTransitioningFromApproachingState":Z
    .end local v1    # "isTransitioningToApproachingState":Z
    :cond_1
    move v1, v5

    .line 521
    goto :goto_0

    .restart local v1    # "isTransitioningToApproachingState":Z
    :cond_2
    move v0, v5

    .line 523
    goto :goto_1

    .line 532
    .restart local v0    # "isTransitioningFromApproachingState":Z
    :cond_3
    if-eqz v1, :cond_4

    .line 533
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    sget v5, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadProgressBarWidth:I

    invoke-static {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->setWidth(Landroid/view/View;I)V

    .line 534
    iput-wide p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->initialProgressBarDistance:J

    goto :goto_2

    .line 535
    :cond_4
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne p1, v4, :cond_0

    .line 536
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    long-to-double v6, p2

    iget-wide v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->initialProgressBarDistance:J

    long-to-double v8, v8

    div-double/2addr v6, v8

    sub-double v2, v4, v6

    .line 537
    .local v2, "progressFactor":D
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v6, v2

    double-to-int v5, v6

    invoke-static {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getProgressBarAnimator(Landroid/widget/ProgressBar;I)Landroid/animation/ValueAnimator;

    move-result-object v4

    const-wide/16 v6, 0xfa

    .line 538
    invoke-virtual {v4, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v4

    .line 539
    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_2
.end method


# virtual methods
.method public clearState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 131
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnIconId:I

    .line 132
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastDistance:Ljava/lang/String;

    .line 133
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnText:Ljava/lang/String;

    .line 134
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastRoadText:Ljava/lang/String;

    .line 136
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->cleanInstructions()V

    .line 137
    return-void
.end method

.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 15
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 187
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 189
    sget-object v13, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$5;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 278
    :goto_0
    return-void

    .line 192
    :pswitch_0
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 195
    .local v4, "animatorSet":Landroid/animation/AnimatorSet;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconShrinkLeftX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 196
    .local v1, "activeIconAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v4, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    .line 198
    .local v5, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconShrinkLeftX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v10

    .line 199
    .local v10, "nextIconAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v5, v10}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 202
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkLeftX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 203
    .local v3, "activeInstructionXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkWidth:I

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 204
    .local v2, "activeInstructionWidthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 205
    invoke-virtual {v5, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 208
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkLeftX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 209
    .local v12, "nextInstructionXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkWidth:I

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;

    move-result-object v11

    .line 210
    .local v11, "nextInstructionWidthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {v5, v12}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 211
    invoke-virtual {v5, v11}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 213
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v14, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v13, v14, :cond_0

    .line 214
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-static {v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeOutAndScaleDownAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v9

    .line 215
    .local v9, "fadeOutScaleDownNow":Landroid/animation/AnimatorSet;
    invoke-virtual {v5, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 221
    .end local v9    # "fadeOutScaleDownNow":Landroid/animation/AnimatorSet;
    :goto_1
    new-instance v13, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$1;

    invoke-direct {v13, p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)V

    invoke-virtual {v4, v13}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 229
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 217
    :cond_0
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 218
    .local v8, "fadeOutDistance":Landroid/animation/ObjectAnimator;
    invoke-virtual {v5, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1

    .line 234
    .end local v1    # "activeIconAnimator":Landroid/animation/ObjectAnimator;
    .end local v2    # "activeInstructionWidthAnimator":Landroid/animation/ValueAnimator;
    .end local v3    # "activeInstructionXAnimator":Landroid/animation/ObjectAnimator;
    .end local v4    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v5    # "builder":Landroid/animation/AnimatorSet$Builder;
    .end local v8    # "fadeOutDistance":Landroid/animation/ObjectAnimator;
    .end local v10    # "nextIconAnimator":Landroid/animation/ObjectAnimator;
    .end local v11    # "nextInstructionWidthAnimator":Landroid/animation/ValueAnimator;
    .end local v12    # "nextInstructionXAnimator":Landroid/animation/ObjectAnimator;
    :pswitch_1
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 236
    .restart local v4    # "animatorSet":Landroid/animation/AnimatorSet;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 237
    .restart local v1    # "activeIconAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v4, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    .line 239
    .restart local v5    # "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v10

    .line 240
    .restart local v10    # "nextIconAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v5, v10}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 243
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 244
    .restart local v3    # "activeInstructionXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionWidth:I

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 245
    .restart local v2    # "activeInstructionWidthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 246
    invoke-virtual {v5, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 249
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionX:I

    int-to-float v14, v14

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 250
    .restart local v12    # "nextInstructionXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    sget v14, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionWidth:I

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;

    move-result-object v11

    .line 251
    .restart local v11    # "nextInstructionWidthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {v5, v12}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 252
    invoke-virtual {v5, v11}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 254
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v14, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v13, v14, :cond_1

    .line 255
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-static {v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeInAndScaleUpAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v7

    .line 256
    .local v7, "fadeInScaleUpNow":Landroid/animation/AnimatorSet;
    invoke-virtual {v5, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 262
    .end local v7    # "fadeInScaleUpNow":Landroid/animation/AnimatorSet;
    :goto_2
    new-instance v13, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;

    invoke-direct {v13, p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$2;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)V

    invoke-virtual {v4, v13}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 275
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_0

    .line 258
    :cond_1
    iget-object v13, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 259
    .local v6, "fadeInDistance":Landroid/animation/ObjectAnimator;
    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_2

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 128
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 114
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 115
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 116
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 117
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    .line 118
    .local v1, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->bus:Lcom/squareup/otto/Bus;

    .line 119
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 121
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 122
    .local v0, "layoutTransition":Landroid/animation/LayoutTransition;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 123
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 545
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->paused:Z

    if-eqz v0, :cond_0

    .line 550
    :goto_0
    return-void

    .line 548
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->paused:Z

    .line 549
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause:tbt"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 554
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->paused:Z

    if-nez v1, :cond_1

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->paused:Z

    .line 558
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "::onResume:tbt"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 560
    .local v0, "event":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    if-eqz v0, :cond_0

    .line 561
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 562
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "::onResume:tbt updated last event"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 563
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 6
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    const v5, 0x3f666666    # 0.9f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 140
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 142
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 143
    .local v0, "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 145
    .local v1, "nextMargins":Landroid/view/ViewGroup$MarginLayoutParams;
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/TbtView$5;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 184
    :goto_0
    return-void

    .line 147
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v2, v3, :cond_0

    .line 148
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setScaleX(F)V

    .line 149
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setScaleY(F)V

    .line 150
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setAlpha(F)V

    .line 153
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoDistanceX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 155
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 156
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setX(F)V

    .line 158
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 159
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setX(F)V

    .line 161
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionWidth:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 162
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionWidth:I

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto :goto_0

    .line 166
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setScaleX(F)V

    .line 167
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setScaleY(F)V

    .line 168
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 170
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoDistanceX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 172
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconShrinkLeftX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 173
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconShrinkLeftX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setX(F)V

    .line 175
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkLeftX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setX(F)V

    .line 176
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkLeftX:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setX(F)V

    .line 178
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 179
    .restart local v0    # "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "nextMargins":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 180
    .restart local v1    # "nextMargins":Landroid/view/ViewGroup$MarginLayoutParams;
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkWidth:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 181
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkWidth:I

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/16 :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showHideActiveDistance()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 281
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isMainUIShrunk()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 288
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 292
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v2

    .line 293
    .local v2, "navigationActive":Z
    if-eqz v2, :cond_0

    .line 297
    iget-boolean v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->empty:Z

    if-eqz v5, :cond_1

    .line 298
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->clearState()V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    if-nez v5, :cond_2

    .line 303
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "null pending turn"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 304
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    goto :goto_0

    .line 308
    :cond_2
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 310
    iget-boolean v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->paused:Z

    if-nez v5, :cond_0

    .line 314
    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastTurnText:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastRoadText:Ljava/lang/String;

    .line 315
    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v1, 0x1

    .line 317
    .local v1, "hasSameInstruction":Z
    :goto_1
    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    invoke-direct {p0, v5, v6}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->setDistance(Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;)V

    .line 318
    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iget-wide v6, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceInMeters:J

    invoke-direct {p0, v5, v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->setProgressBar(Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;J)V

    .line 320
    iget v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    invoke-direct {p0, v5, v1}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->setIcon(IZ)Landroid/animation/AnimatorSet;

    move-result-object v3

    .line 321
    .local v3, "setIcon":Landroid/animation/AnimatorSet;
    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    invoke-direct {p0, v5, v6}, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->setInstruction(Ljava/lang/String;Ljava/lang/String;)Landroid/animation/AnimatorSet;

    move-result-object v4

    .line 323
    .local v4, "setInstruction":Landroid/animation/AnimatorSet;
    const/4 v0, 0x0

    .line 325
    .local v0, "animation":Landroid/animation/AnimatorSet;
    if-eqz v3, :cond_6

    if-eqz v4, :cond_6

    .line 326
    new-instance v0, Landroid/animation/AnimatorSet;

    .end local v0    # "animation":Landroid/animation/AnimatorSet;
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 327
    .restart local v0    # "animation":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 334
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 335
    const-wide/16 v6, 0x1f4

    .line 336
    invoke-virtual {v0, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->maneuverSwapInterpolator:Landroid/animation/TimeInterpolator;

    .line 337
    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 338
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 341
    :cond_4
    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    goto :goto_0

    .line 315
    .end local v0    # "animation":Landroid/animation/AnimatorSet;
    .end local v1    # "hasSameInstruction":Z
    .end local v3    # "setIcon":Landroid/animation/AnimatorSet;
    .end local v4    # "setInstruction":Landroid/animation/AnimatorSet;
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 328
    .restart local v0    # "animation":Landroid/animation/AnimatorSet;
    .restart local v1    # "hasSameInstruction":Z
    .restart local v3    # "setIcon":Landroid/animation/AnimatorSet;
    .restart local v4    # "setInstruction":Landroid/animation/AnimatorSet;
    :cond_6
    if-eqz v3, :cond_7

    .line 329
    move-object v0, v3

    goto :goto_2

    .line 330
    :cond_7
    if-eqz v4, :cond_3

    .line 331
    move-object v0, v4

    goto :goto_2
.end method
