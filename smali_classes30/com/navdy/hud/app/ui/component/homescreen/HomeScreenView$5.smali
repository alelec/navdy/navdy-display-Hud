.class Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "HomeScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getShrinkLeftAnimator()Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 752
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 755
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 756
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 757
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 758
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v0, v1, :cond_0

    .line 759
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 761
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setAlpha(F)V

    .line 763
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 772
    :cond_2
    :goto_0
    return-void

    .line 765
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$300(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v0, v1, :cond_2

    .line 766
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 767
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setAlpha(F)V

    .line 768
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    goto :goto_0
.end method
