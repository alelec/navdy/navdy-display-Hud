.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startPreviousRoute(Lcom/here/android/mpa/common/GeoCoordinate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field final synthetic val$center:Lcom/here/android/mpa/common/GeoCoordinate;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 1423
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->val$center:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 1427
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "check if prev route was active"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1428
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSavedRouteData(Lcom/navdy/service/library/log/Logger;)Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;

    move-result-object v11

    .line 1429
    .local v11, "savedRouteData":Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    iget-object v10, v11, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1431
    .local v10, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-nez v10, :cond_0

    .line 1432
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "no saved route info"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1433
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->launchSuggestedDestination()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1501
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    .line 1503
    .end local v10    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v11    # "savedRouteData":Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    :goto_1
    return-void

    .line 1435
    .restart local v10    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .restart local v11    # "savedRouteData":Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "saved route found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1436
    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->printRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    .line 1438
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$1100(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1439
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "navigation is already active or route being calculated,ignore the saved route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1440
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->clearSuggestedDestination()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1501
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    goto :goto_1

    .line 1446
    :cond_1
    :try_start_2
    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v2, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DDD)V

    .line 1447
    .local v1, "target":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->val$center:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v2, v1}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v8

    .line 1448
    .local v8, "distance":D
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "distance remaining to destination is ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] current["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->val$center:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] target ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1450
    iget-object v2, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_GAS:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1452
    .local v0, "isGasRoute":Z
    if-eqz v0, :cond_3

    const-wide/16 v2, 0x0

    cmpl-double v2, v8, v2

    if-ltz v2, :cond_3

    const-wide v2, 0x407f400000000000L    # 500.0

    cmpg-double v2, v8, v2

    if-gtz v2, :cond_3

    .line 1453
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "gas route and distance to destination is <= 500.0, don\'t navigate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1454
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "remove gas route and look for non gas route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1455
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    .line 1456
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSavedRouteData(Lcom/navdy/service/library/log/Logger;)Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;

    move-result-object v11

    .line 1457
    iget-object v10, v11, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1459
    if-nez v10, :cond_2

    .line 1460
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "no non gas route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1461
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->launchSuggestedDestination()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1501
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    goto/16 :goto_1

    .line 1465
    :cond_2
    const/4 v0, 0x0

    .line 1466
    :try_start_3
    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v1    # "target":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v2, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DDD)V

    .line 1467
    .restart local v1    # "target":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->val$center:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v2, v1}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v8

    .line 1470
    :cond_3
    if-nez v0, :cond_4

    const-wide/16 v2, 0x0

    cmpl-double v2, v8, v2

    if-ltz v2, :cond_4

    const-wide v2, 0x407f400000000000L    # 500.0

    cmpg-double v2, v8, v2

    if-gtz v2, :cond_4

    .line 1471
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "distance to destination is <= 500, don\'t navigate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1472
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->launchSuggestedDestination()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1501
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    goto/16 :goto_1

    .line 1476
    :cond_4
    :try_start_4
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->clearSuggestedDestination()V

    .line 1478
    new-instance v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    invoke-direct {v2, v10}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    const/4 v3, 0x1

    .line 1479
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v2

    const/4 v3, 0x0

    .line 1480
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->cancelCurrent(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v2

    .line 1481
    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v10

    .line 1485
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "sleep before starting navigation"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1486
    const/16 v2, 0x7d0

    invoke-static {v2}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V

    .line 1487
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "slept"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1489
    iget-boolean v2, v11, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;->isGasRoute:Z

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getFeatureUtil()Lcom/navdy/hud/app/util/FeatureUtil;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/util/FeatureUtil;->isFeatureEnabled(Lcom/navdy/hud/app/util/FeatureUtil$Feature;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1490
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "start saveroute navigation (gas route)"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1495
    :goto_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$1200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/squareup/otto/Bus;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1496
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$1200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/squareup/otto/Bus;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v3, v10}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1498
    .end local v0    # "isGasRoute":Z
    .end local v1    # "target":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v8    # "distance":D
    .end local v10    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v11    # "savedRouteData":Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    :catch_0
    move-exception v12

    .line 1499
    .local v12, "t":Ljava/lang/Throwable;
    :try_start_5
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1501
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    goto/16 :goto_1

    .line 1492
    .end local v12    # "t":Ljava/lang/Throwable;
    .restart local v0    # "isGasRoute":Z
    .restart local v1    # "target":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v8    # "distance":D
    .restart local v10    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .restart local v11    # "savedRouteData":Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    :cond_5
    :try_start_6
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "start saveroute navigation"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 1501
    .end local v0    # "isGasRoute":Z
    .end local v1    # "target":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v8    # "distance":D
    .end local v10    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v11    # "savedRouteData":Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$11;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    throw v2
.end method
