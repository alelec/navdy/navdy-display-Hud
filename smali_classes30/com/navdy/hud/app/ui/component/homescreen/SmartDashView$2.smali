.class Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;
.super Ljava/lang/Object;
.source "SmartDashView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

.field final synthetic val$homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->val$homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ObdStateChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 234
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    .line 235
    .local v0, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    if-eqz v2, :cond_1

    .line 236
    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getEngineRpm()I

    move-result v1

    .line 237
    .local v1, "rpm":I
    if-gez v1, :cond_0

    .line 238
    const/4 v1, 0x0

    .line 240
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->tachometerPresenter:Lcom/navdy/hud/app/view/TachometerGaugePresenter;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/view/TachometerGaugePresenter;->setRPM(I)V

    .line 242
    .end local v1    # "rpm":I
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 243
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->reLoadAvailableWidgets(Z)V

    .line 245
    :cond_2
    return-void
.end method

.method public onDashboardPreferences(Lcom/navdy/service/library/events/preferences/DashboardPreferences;)V
    .locals 5
    .param p1, "dashboardPreferences"    # Lcom/navdy/service/library/events/preferences/DashboardPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 260
    if-eqz p1, :cond_4

    .line 262
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    if-eqz v0, :cond_0

    .line 263
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/MiddleGauge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 273
    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    if-eqz v0, :cond_1

    .line 274
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/ScrollableSide;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 285
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->isValidGaugeId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v1, "PREFERENCE_LEFT_GAUGE"

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V

    .line 290
    :cond_2
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->isValidGaugeId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v1, "PREFERENCE_RIGHT_GAUGE"

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V

    .line 293
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->showViewsAccordingToUserPreferences()V
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$600(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)V

    .line 295
    :cond_4
    return-void

    .line 265
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v1, "PREFERENCE_MIDDLE_GAUGE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 268
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v1, "PREFERENCE_MIDDLE_GAUGE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 276
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v1, "PREFERENCE_SCROLLABLE_SIDE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 279
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v1, "PREFERENCE_SCROLLABLE_SIDE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->savePreference(Ljava/lang/String;Ljava/lang/Object;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$500(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 263
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 274
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 2
    .param p1, "profileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->val$homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateDriverPrefs()V

    .line 203
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->reLoadAvailableWidgets(Z)V

    .line 206
    :cond_0
    return-void
.end method

.method public onGpsLocationChanged(Landroid/location/Location;)V
    .locals 7
    .param p1, "location"    # Landroid/location/Location;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 183
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v4

    float-to-double v0, v4

    .line 184
    .local v0, "heading":D
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/util/HeadingDataUtil;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/navdy/hud/app/util/HeadingDataUtil;->setHeading(D)V

    .line 185
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/util/HeadingDataUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/util/HeadingDataUtil;->getHeading()D

    move-result-wide v2

    .line 186
    .local v2, "newHeadingValue":D
    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 187
    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SmartDash New: Heading :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/gps/GpsUtils;->getHeadingDirection(D)Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Provider :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 189
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v4

    const-string v5, "COMPASS_WIDGET"

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    .line 190
    return-void
.end method

.method public onLocationFixChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$LocationFix;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 194
    iget-boolean v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->locationAvailable:Z

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->headingDataUtil:Lcom/navdy/hud/app/util/HeadingDataUtil;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/util/HeadingDataUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/util/HeadingDataUtil;->reset()V

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v0

    const-string v1, "COMPASS_WIDGET"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    .line 198
    :cond_0
    return-void
.end method

.method public onSupportedPidEventsChange(Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->reLoadAvailableWidgets(Z)V

    .line 250
    return-void
.end method

.method public onUserGaugePreferencesChanged(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$UserPreferenceChanged;)V
    .locals 2
    .param p1, "userPreferenceChanged"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$UserPreferenceChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->reLoadAvailableWidgets(Z)V

    .line 255
    return-void
.end method

.method public updateETA(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->val$homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    if-nez v0, :cond_2

    .line 215
    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "etaDate is not set"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :cond_2
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$300(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$302(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;

    .line 222
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->etaText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$300(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    :cond_3
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaAmPm:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA_AmPm:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$400(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaAmPm:Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA_AmPm:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$402(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;

    .line 228
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->etaAmPm:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$2;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->lastETA_AmPm:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$400(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
