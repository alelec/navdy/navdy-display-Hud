.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
.super Landroid/widget/RelativeLayout;
.source "HomeScreenView.java"

# interfaces
.implements Lcom/navdy/hud/app/view/ICustomAnimator;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.implements Lcom/navdy/hud/app/gesture/GestureDetector$GestureListener;


# static fields
.field private static final MIN_SPEED_LIMIT_VISIBLE_DURATION:I

.field static final SYSPROP_ALWAYS_SHOW_SPEED_LIMIT_SIGN:Ljava/lang/String; = "persist.sys.speedlimit.always"

.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0188
    .end annotation
.end field

.field private alwaysShowSpeedLimitSign:Z

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

.field dashboardWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0185
    .end annotation
.end field

.field driveScoreGaugePresenter:Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

.field private driverPreferences:Landroid/content/SharedPreferences;

.field globalPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private hideSpeedLimitRunnable:Ljava/lang/Runnable;

.field private isRecalculating:Z

.field private isTopInit:Z

.field private isTopViewAnimationOut:Z

.field private isTopViewAnimationRunning:Z

.field laneGuidanceIconIndicator:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0187
    .end annotation
.end field

.field laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e018d
    .end annotation
.end field

.field private lastSpeedLimitShown:J

.field mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0182
    .end annotation
.end field

.field mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e018e
    .end annotation
.end field

.field mapMask:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00b5
    .end annotation
.end field

.field mapViewSpeedContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0183
    .end annotation
.end field

.field private mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

.field protected navigationViewsContainer:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0186
    .end annotation
.end field

.field private notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e019b
    .end annotation
.end field

.field private paused:Z

.field presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01a7
    .end annotation
.end field

.field private rightScreenAnimator:Landroid/animation/AnimatorSet;

.field private screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

.field private showCollapsedNotif:Z

.field smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01ab
    .end annotation
.end field

.field private speedLimit:I

.field speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0184
    .end annotation
.end field

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0195
    .end annotation
.end field

.field tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01b7
    .end annotation
.end field

.field timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c7
    .end annotation
.end field

.field private topViewAnimationRunnable:Ljava/lang/Runnable;

.field private topViewAnimator:Landroid/animation/AnimatorSet;

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 69
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 72
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->MIN_SPEED_LIMIT_VISIBLE_DURATION:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 204
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 205
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    .line 208
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopInit:Z

    .line 157
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 158
    iput v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimit:I

    .line 159
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->alwaysShowSpeedLimitSign:Z

    .line 163
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    .line 171
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    .line 176
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$2;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    .line 195
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .line 419
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$3;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hideSpeedLimitRunnable:Ljava/lang/Runnable;

    .line 209
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 210
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 211
    const-string v0, "persist.sys.speedlimit.always"

    invoke-static {v0, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->alwaysShowSpeedLimitSign:Z

    .line 212
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    new-instance v0, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    const v1, 0x7f030015

    invoke-direct {v0, p1, v1, v2}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->driveScoreGaugePresenter:Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    .line 214
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 216
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showCollapsedNotif:Z

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showCollapsedNotif:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hideSpeedLimit()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z

    return v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z

    return p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating:Z

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationRunning:Z

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private clearRightSectionAnimation()V
    .locals 2

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1147
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1148
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopped rightscreen animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1150
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    .line 1151
    return-void
.end method

.method private getExpandAnimator()Landroid/animation/Animator;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    .line 780
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v3

    .line 781
    .local v3, "isNavigating":Z
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 782
    .local v2, "expandAnimatorSet":Landroid/animation/AnimatorSet;
    new-array v6, v9, [F

    fill-array-data v6, :array_0

    invoke-static {v6}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 784
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z

    if-nez v6, :cond_1

    .line 797
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->getAlpha()F

    move-result v6

    cmpl-float v6, v6, v10

    if-nez v6, :cond_0

    .line 798
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget-object v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->ALPHA:Landroid/util/Property;

    new-array v8, v9, [F

    fill-array-data v8, :array_1

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 799
    .local v5, "timeAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 802
    .end local v5    # "timeAnimator":Landroid/animation/ObjectAnimator;
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->getX()F

    move-result v6

    sget v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeX:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_1

    .line 803
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeX:I

    int-to-float v7, v7

    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 804
    .restart local v5    # "timeAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 808
    .end local v5    # "timeAnimator":Landroid/animation/ObjectAnimator;
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v6, v7, :cond_3

    .line 809
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->getX()F

    move-result v6

    sget v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->etaX:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_2

    .line 810
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    sget v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->etaX:I

    int-to-float v7, v7

    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 811
    .local v1, "etaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 814
    .end local v1    # "etaAnimator":Landroid/animation/ObjectAnimator;
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->getAlpha()F

    move-result v6

    cmpl-float v6, v6, v10

    if-nez v6, :cond_3

    .line 815
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    sget-object v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->ALPHA:Landroid/util/Property;

    new-array v8, v9, [F

    fill-array-data v8, :array_2

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 816
    .restart local v1    # "etaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 820
    .end local v1    # "etaAnimator":Landroid/animation/ObjectAnimator;
    :cond_3
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 822
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v6, v7, :cond_4

    .line 823
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 827
    :cond_4
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 831
    if-nez v3, :cond_5

    .line 832
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 833
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7, v0}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 841
    :goto_0
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating:Z

    if-eqz v6, :cond_6

    .line 843
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    sget v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->recalcX:I

    int-to-float v7, v7

    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 844
    .local v4, "recalcAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 849
    .end local v4    # "recalcAnimator":Landroid/animation/ObjectAnimator;
    :goto_1
    new-instance v6, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;

    invoke-direct {v6, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$6;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v2, v6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 877
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateTopViewsIn()V

    .line 878
    return-object v2

    .line 835
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 836
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 838
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    goto :goto_0

    .line 846
    :cond_6
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    sget-object v7, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    goto :goto_1

    .line 782
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x42c80000    # 100.0f
    .end array-data

    .line 798
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 815
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getShrinkLeftAnimator()Landroid/animation/Animator;
    .locals 12

    .prologue
    const/4 v11, 0x2

    .line 679
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v2

    .line 680
    .local v2, "isNavigating":Z
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 681
    .local v4, "shrinkAnimatorSet":Landroid/animation/AnimatorSet;
    new-array v7, v11, [F

    fill-array-data v7, :array_0

    invoke-static {v7}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 684
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 686
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 687
    .local v5, "speed":Landroid/animation/AnimatorSet;
    const/4 v7, 0x1

    new-array v7, v7, [Landroid/animation/Animator;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    sget-object v10, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v11, v11, [F

    fill-array-data v11, :array_1

    .line 688
    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v7, v8

    .line 687
    invoke-virtual {v5, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 691
    new-instance v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$4;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$4;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v5, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 699
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 702
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 704
    if-nez v2, :cond_0

    .line 706
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 707
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 709
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8, v0}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 711
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget v8, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeShrinkLeftX:I

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 712
    .local v6, "timeAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 744
    .end local v6    # "timeAnimator":Landroid/animation/ObjectAnimator;
    :goto_0
    iget-boolean v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating:Z

    if-eqz v7, :cond_3

    .line 746
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    sget v8, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->recalcShrinkLeftX:I

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 747
    .local v3, "recalcAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 752
    .end local v3    # "recalcAnimator":Landroid/animation/ObjectAnimator;
    :goto_1
    new-instance v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$5;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v4, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 775
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateTopViewsOut()V

    .line 776
    return-object v4

    .line 715
    :cond_0
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 717
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    .line 718
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget v8, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeShrinkLeftX:I

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 719
    .restart local v6    # "timeAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 721
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 738
    .end local v6    # "timeAnimator":Landroid/animation/ObjectAnimator;
    :goto_2
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 741
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8, v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    goto :goto_0

    .line 723
    :cond_1
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 730
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v8, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v7, v8, :cond_2

    .line 731
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    sget v8, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->etaShrinkLeftX:I

    int-to-float v8, v8

    invoke-static {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 732
    .local v1, "etaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_2

    .line 734
    .end local v1    # "etaAnimator":Landroid/animation/ObjectAnimator;
    :cond_2
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    goto :goto_2

    .line 749
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    sget-object v8, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    goto :goto_1

    .line 681
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x42c80000    # 100.0f
    .end array-data

    .line 687
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private hideSpeedLimit()V
    .locals 6

    .prologue
    .line 427
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hideSpeedLimitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 428
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->lastSpeedLimitShown:J

    sub-long v0, v2, v4

    .line 429
    .local v0, "diff":J
    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->MIN_SPEED_LIMIT_VISIBLE_DURATION:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 430
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setVisibility(I)V

    .line 431
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setSpeedLimit(I)V

    .line 432
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->lastSpeedLimitShown:J

    .line 436
    :goto_0
    return-void

    .line 434
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hideSpeedLimitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private pauseNavigationViewContainer()V
    .locals 2

    .prologue
    .line 1093
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause:pauseNavigationViewContainer"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1094
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->onPause()V

    .line 1095
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->onPause()V

    .line 1096
    return-void
.end method

.method private resetTopViewsAnimator()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1026
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "resetTopViewsAnimator"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1027
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1028
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1029
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1031
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->resetTopViewsAnimator()V

    .line 1032
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->resetTopViewsAnimator()V

    .line 1033
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->resetTopViewsAnimator()V

    .line 1034
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->shouldAnimateTopViews()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1035
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1036
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateTopViewsOut()V

    .line 1047
    :goto_0
    return-void

    .line 1038
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 1039
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setAlpha(F)V

    .line 1040
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 1041
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setAlpha(F)V

    .line 1042
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z

    .line 1043
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationRunning:Z

    .line 1044
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1045
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private resumeNavigationViewContainer()V
    .locals 2

    .prologue
    .line 1099
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume:resumeNavigationViewContainer"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1100
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->onResume()V

    .line 1101
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->onResume()V

    .line 1102
    return-void
.end method

.method private setDisplayMode(Z)V
    .locals 5
    .param p1, "resetTopViews"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 296
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDisplayMode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 297
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 358
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateRoadInfoView()V

    .line 360
    if-eqz p1, :cond_0

    .line 361
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->resetTopViewsAnimator()V

    .line 363
    :cond_0
    return-void

    .line 299
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dash invisible"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 300
    const/high16 v0, -0x1000000

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setViewsBackground(I)V

    .line 301
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onResume()V

    .line 302
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->onResume()V

    .line 304
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/obd/ObdManager;->enableInstantaneousMode(Z)V

    .line 305
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setVisibility(I)V

    .line 306
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onPause()V

    .line 307
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showDriveScoreEventsOnMap()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->dashboardWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->driveScoreGaugePresenter:Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->onResume()V

    .line 311
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 312
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 319
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isMainUIShrunk()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 325
    :goto_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->navigationViewsContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->showLastEvent()V

    .line 328
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->resumeNavigationViewContainer()V

    goto :goto_0

    .line 315
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    goto :goto_1

    .line 322
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2

    .line 332
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dash visible"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 333
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setViewsBackground(I)V

    .line 334
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showDriveScoreEventsOnMap()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 335
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->dashboardWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->driveScoreGaugePresenter:Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->onPause()V

    .line 338
    :cond_4
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/obd/ObdManager;->enableInstantaneousMode(Z)V

    .line 339
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onResume()V

    .line 340
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceIconIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onPause()V

    .line 350
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->onPause()V

    .line 352
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->navigationViewsContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 354
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->resumeNavigationViewContainer()V

    goto/16 :goto_0

    .line 297
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setViewSpeed(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 1203
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1212
    :goto_0
    return-void

    .line 1205
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setX(F)V

    goto :goto_0

    .line 1209
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedShrinkLeftX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setX(F)V

    goto :goto_0

    .line 1203
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setViews(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 263
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 264
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 265
    move-object v0, p1

    .line 266
    .local v0, "mainScreenMode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isModeVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 269
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 270
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 271
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setViewSpeed(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 272
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 273
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 274
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 275
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 276
    return-void
.end method

.method private setViewsBackground(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 1105
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setViewsBackground:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1106
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setBackgroundColor(I)V

    .line 1107
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->setBackgroundColor(I)V

    .line 1108
    return-void
.end method

.method public static showDriveScoreEventsOnMap()Z
    .locals 1

    .prologue
    .line 219
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;)V
    .locals 6
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 506
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 507
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating:Z

    .line 508
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    .line 510
    .local v0, "navigationActive":Z
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;)V

    .line 511
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v2, p1, v4}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;Z)V

    .line 512
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v2, p1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 514
    if-eqz v0, :cond_3

    .line 515
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v2, v3, :cond_2

    .line 516
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isMainUIShrunk()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 517
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 526
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 529
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->clearState()V

    .line 530
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setVisibility(I)V

    .line 532
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->clearState()V

    .line 533
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v2, v3, :cond_0

    .line 534
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 535
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 538
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->clearState()V

    .line 539
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->clearState()V

    .line 564
    :goto_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateRoadInfoView()V

    .line 566
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->resetTopViewsAnimator()V

    .line 569
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopInit:Z

    if-eqz v2, :cond_6

    .line 570
    const/16 v1, 0x7530

    .line 571
    .local v1, "time":I
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopInit:Z

    .line 575
    :goto_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 576
    return-void

    .line 519
    .end local v1    # "time":I
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 522
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 543
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->hideRecalculating()V

    .line 545
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->clearState()V

    .line 546
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 548
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->clearState()V

    .line 549
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->setVisibility(I)V

    .line 551
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v2, v3, :cond_5

    .line 552
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 553
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isMainUIShrunk()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 554
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 561
    :goto_3
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->clearState()V

    .line 562
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->clearState()V

    goto :goto_1

    .line 556
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3

    .line 559
    :cond_5
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapViewSpeedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3

    .line 573
    :cond_6
    const/16 v1, 0x2710

    .restart local v1    # "time":I
    goto :goto_2
.end method

.method private updateRoadInfoView()V
    .locals 3

    .prologue
    .line 579
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    .line 580
    .local v0, "navigationActive":Z
    if-eqz v0, :cond_0

    .line 582
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setVisibility(I)V

    .line 587
    :goto_0
    return-void

    .line 584
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setRoad()V

    .line 585
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateSpeedLimitSign()V
    .locals 4

    .prologue
    .line 402
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimit:I

    if-lez v2, :cond_2

    .line 403
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v0

    .line 404
    .local v0, "currentSpeed":I
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimit:I

    if-gt v0, v2, :cond_0

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->alwaysShowSpeedLimitSign:Z

    if-eqz v2, :cond_1

    .line 405
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hideSpeedLimitRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 406
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v1

    .line 407
    .local v1, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setSpeedLimitUnit(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)V

    .line 408
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    iget v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimit:I

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setSpeedLimit(I)V

    .line 409
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimitSignView:Lcom/navdy/hud/app/view/SpeedLimitSignView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/SpeedLimitSignView;->setVisibility(I)V

    .line 410
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->lastSpeedLimitShown:J

    .line 417
    .end local v0    # "currentSpeed":I
    .end local v1    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    :goto_0
    return-void

    .line 412
    .restart local v0    # "currentSpeed":I
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hideSpeedLimit()V

    goto :goto_0

    .line 415
    .end local v0    # "currentSpeed":I
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hideSpeedLimit()V

    goto :goto_0
.end method


# virtual methods
.method public GPSSpeedChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 393
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateSpeedLimitSign()V

    .line 394
    return-void
.end method

.method public ObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 384
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 389
    :goto_0
    return-void

    .line 386
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateSpeedLimitSign()V

    goto :goto_0

    .line 384
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method public animateInModeView()V
    .locals 3

    .prologue
    .line 1162
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isModeVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1163
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "mode-view animateInModeView"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1164
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    .line 1165
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 1166
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    sget-object v2, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 1167
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    sget-object v2, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 1168
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    sget-object v2, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 1169
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1171
    .end local v0    # "builder":Landroid/animation/AnimatorSet$Builder;
    :cond_0
    return-void

    .line 1165
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public animateOutModeView(Z)V
    .locals 4
    .param p1, "removeView"    # Z

    .prologue
    .line 1174
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isModeVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1175
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode-view animateOutModeView:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1176
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    .line 1177
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 1178
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    sget-object v2, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 1179
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    sget-object v2, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 1180
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    sget-object v2, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 1181
    if-eqz p1, :cond_0

    .line 1182
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    new-instance v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$9;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$9;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1189
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1191
    .end local v0    # "builder":Landroid/animation/AnimatorSet$Builder;
    :cond_1
    return-void

    .line 1177
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method animateTopViewsIn()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x0

    .line 965
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationRunning:Z

    if-eqz v3, :cond_1

    .line 1023
    :cond_0
    :goto_0
    return-void

    .line 968
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->shouldAnimateTopViews()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 972
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z

    if-eqz v3, :cond_0

    .line 976
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    .line 979
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 980
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewLeftAnimationIn:I

    int-to-float v4, v4

    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 981
    .local v2, "timeXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 983
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 984
    .local v1, "timeAlphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 996
    .end local v1    # "timeAlphaAnimator":Landroid/animation/ObjectAnimator;
    .end local v2    # "timeXAnimator":Landroid/animation/ObjectAnimator;
    :goto_1
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1009
    :goto_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationRunning:Z

    .line 1011
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    new-instance v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1021
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 1022
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "started in animation"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 987
    .end local v0    # "builder":Landroid/animation/AnimatorSet$Builder;
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    new-array v4, v5, [I

    fill-array-data v4, :array_1

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .restart local v0    # "builder":Landroid/animation/AnimatorSet$Builder;
    goto :goto_1

    .line 998
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v3, v0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V

    .line 999
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    invoke-virtual {v3, v0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V

    goto :goto_2

    .line 1003
    :pswitch_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v3, v0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V

    goto :goto_2

    .line 983
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 996
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 987
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method animateTopViewsOut()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 905
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 907
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationRunning:Z

    if-eqz v3, :cond_1

    .line 958
    :cond_0
    :goto_0
    return-void

    .line 910
    :cond_1
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z

    if-nez v3, :cond_0

    .line 914
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    .line 917
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 918
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewLeftAnimationOut:I

    int-to-float v4, v4

    invoke-static {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 919
    .local v2, "timeXAnimator":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 921
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 922
    .local v1, "timeAlphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 934
    .end local v1    # "timeAlphaAnimator":Landroid/animation/ObjectAnimator;
    .end local v2    # "timeXAnimator":Landroid/animation/ObjectAnimator;
    :goto_1
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 946
    :goto_2
    iput-boolean v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationRunning:Z

    .line 948
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    new-instance v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$7;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$7;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 956
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 957
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "started out animation"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 925
    .end local v0    # "builder":Landroid/animation/AnimatorSet$Builder;
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimator:Landroid/animation/AnimatorSet;

    new-array v4, v5, [I

    fill-array-data v4, :array_1

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .restart local v0    # "builder":Landroid/animation/AnimatorSet$Builder;
    goto :goto_1

    .line 936
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v3, v0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V

    .line 937
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedView:Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;

    invoke-virtual {v3, v0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/SpeedView;->getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V

    goto :goto_2

    .line 941
    :pswitch_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v3, v0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V

    goto :goto_2

    .line 921
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 934
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 925
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public ejectRightSection()V
    .locals 3

    .prologue
    .line 1129
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ejectRightSection"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1130
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->clearRightSectionAnimation()V

    .line 1131
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->isViewVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1132
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    .line 1133
    .local v0, "mainScreen":Lcom/navdy/hud/app/ui/activity/Main;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1134
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ejectRightSection: cannot animate"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1135
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->ejectRightSection()V

    .line 1142
    .end local v0    # "mainScreen":Lcom/navdy/hud/app/ui/activity/Main;
    :goto_0
    return-void

    .line 1137
    .restart local v0    # "mainScreen":Lcom/navdy/hud/app/ui/activity/Main;
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateOutModeView(Z)V

    goto :goto_0

    .line 1140
    .end local v0    # "mainScreen":Lcom/navdy/hud/app/ui/activity/Main;
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->ejectRightSection()V

    goto :goto_0
.end method

.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)Landroid/animation/Animator;
    .locals 2
    .param p1, "customAnimationMode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 666
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 675
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 668
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getShrinkLeftAnimator()Landroid/animation/Animator;

    move-result-object v0

    goto :goto_0

    .line 672
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getExpandAnimator()Landroid/animation/Animator;

    move-result-object v0

    goto :goto_0

    .line 666
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    return-object v0
.end method

.method public getDriverPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->driverPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public getEtaView()Lcom/navdy/hud/app/ui/component/homescreen/EtaView;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    return-object v0
.end method

.method public getLaneGuidanceIconIndicator()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceIconIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    return-object v0
.end method

.method public getOpenRoadView()Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    return-object v0
.end method

.method public getRecalculatingView()Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    return-object v0
.end method

.method public getSmartDashView()Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    return-object v0
.end method

.method public getTbtView()Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    return-object v0
.end method

.method public hasModeView()Z
    .locals 1

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->hasView()Z

    move-result v0

    return v0
.end method

.method public injectRightSection(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1111
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "injectRightSection"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1112
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->injectRightSection(Landroid/view/View;)V

    .line 1114
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->clearRightSectionAnimation()V

    .line 1115
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    .line 1116
    .local v0, "mainScreen":Lcom/navdy/hud/app/ui/activity/Main;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->isViewVisible()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1117
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1118
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "injectRightSection: cannot animate"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1126
    :goto_0
    return-void

    .line 1121
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateInModeView()V

    goto :goto_0

    .line 1124
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "injectRightSection: right section view already visible"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isModeVisible()Z
    .locals 1

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->isViewVisible()Z

    move-result v0

    return v0
.end method

.method public isNavigationActive()Z
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    sget-object v1, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    sget-object v1, Lcom/navdy/hud/app/maps/NavigationMode;->TBT_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecalculating()Z
    .locals 1

    .prologue
    .line 889
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating:Z

    return v0
.end method

.method public isShowCollapsedNotification()Z
    .locals 1

    .prologue
    .line 897
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showCollapsedNotif:Z

    return v0
.end method

.method isTopViewAnimationOut()Z
    .locals 1

    .prologue
    .line 961
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z

    return v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 655
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method public onArrivalEvent(Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 445
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->setVisibility(I)V

    .line 446
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v0, v1, :cond_0

    .line 447
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->timeContainer:Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->setVisibility(I)V

    .line 449
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->resetTopViewsAnimator()V

    .line 450
    return-void
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 597
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateDriverPrefs()V

    .line 441
    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 224
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 225
    const v3, 0x7f0e0181

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 226
    .local v2, "parent":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030057

    invoke-virtual {v3, v4, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 227
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 229
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateDriverPrefs()V

    .line 230
    invoke-direct {p0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Z)V

    .line 231
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapMask:Landroid/widget/ImageView;

    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showDriveScoreEventsOnMap()Z

    move-result v3

    if-eqz v3, :cond_2

    const v3, 0x7f020230

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 232
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 233
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 234
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->recalcRouteContainer:Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/homescreen/RecalculatingView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 235
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->tbtView:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 236
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->openMapRoadInfoContainer:Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 237
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->activeEtaContainer:Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 238
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 239
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mainScreenRightSectionX:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->setX(F)V

    .line 242
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCustomAnimateMode()Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v1

    .line 243
    .local v1, "mode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setViews(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 245
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "setting navigation mode to MAP"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 246
    sget-object v3, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setMode(Lcom/navdy/hud/app/maps/NavigationMode;)V

    .line 248
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    if-eqz v3, :cond_0

    .line 249
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->presenter:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;

    invoke-virtual {v3, p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 251
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showDriveScoreEventsOnMap()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 252
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 253
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "EXTRA_GRAVITY"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 254
    const-string v3, "EXTRA_IS_ACTIVE"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 255
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->driveScoreGaugePresenter:Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->dashboardWidgetView:Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {v3, v4, v0}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 256
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->driveScoreGaugePresenter:Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/view/DriveScoreGaugePresenter;->setWidgetVisibleToUser(Z)V

    .line 258
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addScreenAnimationListener(Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;)V

    .line 259
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v3, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 260
    return-void

    .line 231
    .end local v1    # "mode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    :cond_2
    const v3, 0x7f020235

    goto :goto_0
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v0, 0x1

    .line 605
    iget-object v1, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    if-ne v1, v2, :cond_1

    .line 607
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isModeVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->rightScreenAnimator:Landroid/animation/AnimatorSet;

    .line 608
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v1

    if-nez v1, :cond_1

    .line 609
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "swipe_right animateOutModeView"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 610
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateOutModeView(Z)V

    .line 622
    :goto_0
    return v0

    .line 614
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 622
    const/4 v0, 0x0

    goto :goto_0

    .line 616
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v0

    goto :goto_0

    .line 619
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v0

    goto :goto_0

    .line 614
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->shouldAnimateTopViews()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 640
    :cond_0
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 648
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 631
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateTopViewsIn()V

    goto :goto_0

    .line 635
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateTopViewsOut()V

    goto :goto_0

    .line 642
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_1

    .line 645
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_1

    .line 629
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 640
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onMapEvent(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 372
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v0

    .line 373
    .local v0, "unit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    iget v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentSpeedLimit:F

    float-to-double v2, v1

    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v2, v3, v1, v0}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->speedLimit:I

    .line 374
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateSpeedLimitSign()V

    .line 375
    return-void
.end method

.method public onNavigationModeChange(Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;)V
    .locals 1
    .param p1, "navigationModeChange"    # Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 367
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationMode()Lcom/navdy/hud/app/maps/NavigationMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setMode(Lcom/navdy/hud/app/maps/NavigationMode;)V

    .line 368
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1058
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->paused:Z

    if-eqz v0, :cond_0

    .line 1066
    :goto_0
    return-void

    .line 1061
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->paused:Z

    .line 1062
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onPause()V

    .line 1063
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onPause()V

    .line 1064
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->pauseNavigationViewContainer()V

    .line 1065
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1069
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->paused:Z

    if-nez v0, :cond_0

    .line 1090
    :goto_0
    return-void

    .line 1072
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->paused:Z

    .line 1074
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$10;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1088
    :goto_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->resumeNavigationViewContainer()V

    .line 1089
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1076
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onResume()V

    .line 1077
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->onResume()V

    .line 1078
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onPause()V

    goto :goto_1

    .line 1082
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->onPause()V

    .line 1083
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->laneGuidanceView:Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/LaneGuidanceView;->onPause()V

    .line 1084
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->onResume()V

    goto :goto_1

    .line 1074
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSpeedDataExpired(Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;)V
    .locals 0
    .param p1, "speedDataExpired"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 398
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateSpeedLimitSign()V

    .line 399
    return-void
.end method

.method public onSpeedUnitChanged(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;)V
    .locals 0
    .param p1, "speedUnitChanged"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateSpeedLimitSign()V

    .line 380
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 601
    return-void
.end method

.method public resetModeView()V
    .locals 2

    .prologue
    .line 1194
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isModeVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "mode-view resetModeView"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1196
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 1197
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 1198
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mainscreenRightSection:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenRightSectionView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 1200
    :cond_0
    return-void
.end method

.method public setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v0, p1, :cond_0

    .line 293
    :goto_0
    return-void

    .line 291
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .line 292
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Z)V

    goto :goto_0
.end method

.method public setMode(Lcom/navdy/hud/app/maps/NavigationMode;)V
    .locals 3
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;

    .prologue
    .line 496
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isOverviewMapMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "navigation mode changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 498
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    .line 499
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->updateLayoutForMode(Lcom/navdy/hud/app/maps/NavigationMode;)V

    .line 500
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mapContainer:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->layoutMap()V

    .line 501
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 503
    :cond_1
    return-void
.end method

.method public setRecalculating(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 893
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isRecalculating:Z

    .line 894
    return-void
.end method

.method public setShowCollapsedNotification(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 901
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->showCollapsedNotif:Z

    .line 902
    return-void
.end method

.method public shouldAnimateTopViews()Z
    .locals 2

    .prologue
    .line 1050
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->mode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->smartDashContainer:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->shouldShowTopViews()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateDriverPrefs()V
    .locals 2

    .prologue
    .line 487
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferencesForCurrentDriverProfile(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->driverPreferences:Landroid/content/SharedPreferences;

    .line 488
    return-void
.end method
