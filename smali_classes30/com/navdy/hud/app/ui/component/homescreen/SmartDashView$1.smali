.class Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$1;
.super Ljava/lang/Object;
.source "SmartDashView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

.field final synthetic val$homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$1;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$1;->val$homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/String;)Z
    .locals 9
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 140
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$1;->val$homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDriverPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 141
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v7, "PREFERENCE_LEFT_GAUGE"

    const-string v8, "ANALOG_CLOCK_WIDGET"

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "leftGaugeId":Ljava/lang/String;
    const-string v7, "PREFERENCE_RIGHT_GAUGE"

    const-string v8, "COMPASS_WIDGET"

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 143
    .local v3, "rightGaugeId":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v6

    .line 147
    :cond_1
    const/4 v7, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 150
    :pswitch_0
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z

    move-result v7

    if-nez v7, :cond_3

    :goto_2
    move v6, v5

    goto :goto_0

    .line 147
    :sswitch_0
    const-string v8, "TRAFFIC_INCIDENT_GAUGE_ID"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v7, v6

    goto :goto_1

    :sswitch_1
    const-string v8, "FUEL_GAUGE_ID"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v7, v5

    goto :goto_1

    :sswitch_2
    const-string v8, "ENGINE_TEMPERATURE_GAUGE_ID"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v7, 0x2

    goto :goto_1

    :sswitch_3
    const-string v8, "MPG_AVG_WIDGET"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v7, 0x3

    goto :goto_1

    :cond_3
    move v5, v6

    .line 150
    goto :goto_2

    .line 152
    :pswitch_1
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    .line 153
    .local v1, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v2

    .line 155
    .local v2, "pidSet":Lcom/navdy/obd/PidSet;
    if-eqz v2, :cond_4

    const/16 v7, 0x2f

    invoke-virtual {v2, v7}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v7

    if-nez v7, :cond_0

    :cond_4
    move v6, v5

    goto :goto_0

    .line 158
    .end local v1    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .end local v2    # "pidSet":Lcom/navdy/obd/PidSet;
    :pswitch_2
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    .line 159
    .restart local v1    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v2

    .line 160
    .restart local v2    # "pidSet":Lcom/navdy/obd/PidSet;
    if-eqz v2, :cond_5

    const/4 v7, 0x5

    invoke-virtual {v2, v7}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v7

    if-nez v7, :cond_0

    :cond_5
    move v6, v5

    goto :goto_0

    .line 163
    .end local v1    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .end local v2    # "pidSet":Lcom/navdy/obd/PidSet;
    :pswitch_3
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v1

    .line 164
    .restart local v1    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/obd/ObdManager;->getSupportedPids()Lcom/navdy/obd/PidSet;

    move-result-object v2

    .line 166
    .restart local v2    # "pidSet":Lcom/navdy/obd/PidSet;
    if-eqz v2, :cond_6

    const/16 v7, 0x100

    invoke-virtual {v2, v7}, Lcom/navdy/obd/PidSet;->contains(I)Z

    move-result v7

    if-nez v7, :cond_0

    :cond_6
    move v6, v5

    goto :goto_0

    .line 147
    :sswitch_data_0
    .sparse-switch
        -0x45145f74 -> :sswitch_3
        -0x7dd2557 -> :sswitch_2
        0x45a45eaa -> :sswitch_1
        0x6f9cbd2c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
