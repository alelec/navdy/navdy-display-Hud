.class public Lcom/navdy/hud/app/ui/component/homescreen/TimeView$$ViewInjector;
.super Ljava/lang/Object;
.source "TimeView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/homescreen/TimeView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/TimeView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00de

    const-string v2, "field \'dayTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->dayTextView:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e01c8

    const-string v2, "field \'timeTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeTextView:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e01c9

    const-string v2, "field \'ampmTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->ampmTextView:Landroid/widget/TextView;

    .line 16
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/homescreen/TimeView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/TimeView;

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->dayTextView:Landroid/widget/TextView;

    .line 20
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->timeTextView:Landroid/widget/TextView;

    .line 21
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TimeView;->ampmTextView:Landroid/widget/TextView;

    .line 22
    return-void
.end method
