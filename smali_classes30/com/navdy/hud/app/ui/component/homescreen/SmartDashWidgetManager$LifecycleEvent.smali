.class public final enum Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;
.super Ljava/lang/Enum;
.source "SmartDashWidgetManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LifecycleEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

.field public static final enum PAUSE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

.field public static final enum RESUME:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 126
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->PAUSE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    .line 127
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    const-string v1, "RESUME"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->RESUME:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    .line 125
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->PAUSE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->RESUME:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->$VALUES:[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    const-class v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->$VALUES:[Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    return-object v0
.end method
