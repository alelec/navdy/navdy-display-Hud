.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Lcom/here/android/mpa/mapping/Map$OnTransformListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->animateBackfromOverviewMap(Ljava/lang/Runnable;)Lcom/here/android/mpa/mapping/Map$OnTransformListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field final synthetic val$endAction:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 1309
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->val$endAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapTransformEnd(Lcom/here/android/mpa/mapping/MapState;)V
    .locals 3
    .param p1, "mapState"    # Lcom/here/android/mpa/mapping/MapState;

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$200(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "animateBackfromOverview: transform end"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1316
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$000(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    .line 1317
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$9;)V

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1345
    return-void
.end method

.method public onMapTransformStart()V
    .locals 0

    .prologue
    .line 1311
    return-void
.end method
