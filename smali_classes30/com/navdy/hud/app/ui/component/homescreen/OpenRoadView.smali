.class public Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;
.super Landroid/widget/RelativeLayout;
.source "OpenRoadView.java"


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field openMapRoadInfo:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e019c
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method private setRoad(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 105
    if-nez p1, :cond_0

    .line 106
    const-string p1, ""

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->openMapRoadInfo:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method


# virtual methods
.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 4
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "builder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 81
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 97
    :goto_0
    return-void

    .line 83
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->openMapRoadInfo:Landroid/widget/TextView;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkLeft_L_Margin:I

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkLeft_R_Margin:I

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getMarginAnimator(Landroid/view/View;II)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 87
    .local v0, "openRoadInfoAnimator":Landroid/animation/AnimatorSet;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 91
    .end local v0    # "openRoadInfoAnimator":Landroid/animation/AnimatorSet;
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->openMapRoadInfo:Landroid/widget/TextView;

    sget v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoMargin:I

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoMargin:I

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getMarginAnimator(Landroid/view/View;II)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 94
    .restart local v0    # "openRoadInfoAnimator":Landroid/animation/AnimatorSet;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 49
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 50
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 51
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->bus:Lcom/squareup/otto/Bus;

    .line 52
    return-void
.end method

.method public onManeuverDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setRoad(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public setRoad()V
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadName()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "currentRoad":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->setRoad(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 3
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 66
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView;->openMapRoadInfo:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 67
    .local v0, "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/OpenRoadView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 78
    :goto_0
    return-void

    .line 69
    :pswitch_0
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoMargin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 70
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoMargin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_0

    .line 74
    :pswitch_1
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkLeft_L_Margin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 75
    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkLeft_R_Margin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
