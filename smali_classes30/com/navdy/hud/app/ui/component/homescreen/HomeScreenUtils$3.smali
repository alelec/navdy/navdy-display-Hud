.class final Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$3;
.super Ljava/lang/Object;
.source "HomeScreenUtils.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;II)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$3;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 78
    .local v0, "width":I
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils$3;->val$view:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->setWidth(Landroid/view/View;I)V

    .line 79
    return-void
.end method
