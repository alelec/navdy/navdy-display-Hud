.class Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$1;
.super Ljava/lang/Object;
.source "BaseSmartDashView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;->initViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView$1;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 94
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 95
    const-string v0, "Using OBD RawSpeed"

    .line 99
    .local v0, "str":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowSpeedInput(Ljava/lang/String;)V

    .line 100
    return-void

    .line 97
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    const-string v0, "Using GPS RawSpeed"

    .restart local v0    # "str":Ljava/lang/String;
    goto :goto_0
.end method
