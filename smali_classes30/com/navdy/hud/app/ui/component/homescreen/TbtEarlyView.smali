.class public Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;
.super Landroid/widget/LinearLayout;
.source "TbtEarlyView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;


# static fields
.field private static final TBT_EARLY_SHRUNK_MODE_VISIBLE:Ljava/lang/String; = "persist.sys.tbtearlyshrunk"


# instance fields
.field private currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field earlyManeuverImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e01c2
    .end annotation
.end field

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private final isShrunkVisible:Z

.field private lastNextTurnIconId:I

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private paused:Z

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    .line 57
    const-string v0, "persist.sys.tbtearlyshrunk"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->isShrunkVisible:Z

    .line 58
    return-void
.end method

.method private setEarlyManeuver(I)V
    .locals 2
    .param p1, "iconId"    # I

    .prologue
    .line 71
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    if-ne v0, p1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iput p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    .line 76
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->paused:Z

    if-nez v0, :cond_0

    .line 79
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->earlyManeuverImage:Landroid/widget/ImageView;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->showHideEarlyManeuver()V

    goto :goto_0
.end method


# virtual methods
.method public clearState()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    .line 168
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->showHideEarlyManeuver()V

    .line 169
    return-void
.end method

.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 6
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "builder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 137
    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 164
    :goto_0
    return-void

    .line 139
    :pswitch_0
    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->isShrunkVisible:Z

    if-nez v4, :cond_0

    .line 140
    const/4 v4, 0x0

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 141
    .local v0, "alphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 143
    .end local v0    # "alphaAnimator":Landroid/animation/ObjectAnimator;
    :cond_0
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverWidthShrunk:I

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 144
    .local v1, "widthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {p2, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 145
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverShrinkLeftX:I

    int-to-float v4, v4

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 146
    .local v2, "xPositionAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 147
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverYShrunk:I

    int-to-float v4, v4

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getYPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 148
    .local v3, "yPositionAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 152
    .end local v1    # "widthAnimator":Landroid/animation/ValueAnimator;
    .end local v2    # "xPositionAnimator":Landroid/animation/ObjectAnimator;
    .end local v3    # "yPositionAnimator":Landroid/animation/ObjectAnimator;
    :pswitch_1
    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->isShrunkVisible:Z

    if-nez v4, :cond_1

    .line 153
    const/4 v4, 0x1

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 154
    .restart local v0    # "alphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 156
    .end local v0    # "alphaAnimator":Landroid/animation/ObjectAnimator;
    :cond_1
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverWidth:I

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getWidthAnimator(Landroid/view/View;I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 157
    .restart local v1    # "widthAnimator":Landroid/animation/ValueAnimator;
    invoke-virtual {p2, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 158
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverX:I

    int-to-float v4, v4

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 159
    .restart local v2    # "xPositionAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 160
    sget v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverY:I

    int-to-float v4, v4

    invoke-static {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getYPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 161
    .restart local v3    # "yPositionAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 62
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 64
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 65
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 66
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 67
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public onManeuverDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 105
    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setEarlyManeuver(I)V

    .line 106
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->paused:Z

    if-eqz v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 176
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->paused:Z

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause:tbtearly"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 182
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->paused:Z

    if-nez v1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->paused:Z

    .line 186
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "::onResume:tbtearly"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 187
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    .line 188
    .local v0, "iconId":I
    if-eq v0, v3, :cond_0

    .line 189
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "::onResume:tbtearly set last turn icon"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 190
    iput v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    .line 191
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setEarlyManeuver(I)V

    goto :goto_0
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 116
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 127
    :goto_0
    return-void

    .line 118
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setX(F)V

    .line 119
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverY:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setY(F)V

    goto :goto_0

    .line 123
    :pswitch_1
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverShrinkLeftX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setX(F)V

    .line 124
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverYShrunk:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setY(F)V

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setViewXY()V
    .locals 2

    .prologue
    .line 109
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCustomAnimateMode()Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v0

    .line 110
    .local v0, "mode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 111
    return-void
.end method

.method public showHideEarlyManeuver()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    .line 86
    iget v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->lastNextTurnIconId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 87
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-nez v2, :cond_0

    .line 88
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 90
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    sget-object v3, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    if-eq v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->isShrunkVisible:Z

    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 92
    .local v0, "shrunkShouldHide":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 93
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setVisibility(I)V

    .line 101
    .end local v0    # "shrunkShouldHide":Z
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 90
    goto :goto_0

    .line 95
    .restart local v0    # "shrunkShouldHide":Z
    :cond_2
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setVisibility(I)V

    goto :goto_1

    .line 98
    .end local v0    # "shrunkShouldHide":Z
    :cond_3
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->setVisibility(I)V

    .line 99
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtEarlyView;->earlyManeuverImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
