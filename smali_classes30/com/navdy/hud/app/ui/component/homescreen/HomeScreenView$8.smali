.class Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "HomeScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->animateTopViewsIn()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 1011
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x0

    .line 1014
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationRunning:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$502(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Z)Z

    .line 1015
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isTopViewAnimationOut:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$202(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;Z)Z

    .line 1016
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$700(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$600(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1017
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$700(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->topViewAnimationRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->access$600(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1018
    return-void
.end method
