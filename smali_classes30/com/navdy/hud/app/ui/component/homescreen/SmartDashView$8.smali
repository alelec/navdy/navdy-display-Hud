.class synthetic Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;
.super Ljava/lang/Object;
.source "SmartDashView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 724
    invoke-static {}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->values()[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    .line 610
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    .line 274
    :goto_4
    invoke-static {}, Lcom/navdy/service/library/events/preferences/ScrollableSide;->values()[Lcom/navdy/service/library/events/preferences/ScrollableSide;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide:[I

    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/ScrollableSide;->LEFT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/ScrollableSide;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/ScrollableSide;->RIGHT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/ScrollableSide;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    .line 263
    :goto_6
    invoke-static {}, Lcom/navdy/service/library/events/preferences/MiddleGauge;->values()[Lcom/navdy/service/library/events/preferences/MiddleGauge;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge:[I

    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/MiddleGauge;->SPEEDOMETER:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/MiddleGauge;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$8;->$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge:[I

    sget-object v1, Lcom/navdy/service/library/events/preferences/MiddleGauge;->TACHOMETER:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/MiddleGauge;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    .line 274
    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_5

    .line 610
    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_3

    .line 724
    :catch_6
    move-exception v0

    goto :goto_2

    :catch_7
    move-exception v0

    goto :goto_1

    :catch_8
    move-exception v0

    goto/16 :goto_0
.end method
