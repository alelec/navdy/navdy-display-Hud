.class public Lcom/navdy/hud/app/ui/component/homescreen/TbtView$$ViewInjector;
.super Ljava/lang/Object;
.source "TbtView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/homescreen/TbtView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/TbtView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e01b8

    const-string v2, "field \'activeMapDistance\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e01ba

    const-string v2, "field \'activeMapIcon\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    .line 14
    const v1, 0x7f0e01bc

    const-string v2, "field \'nextMapIconContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    .line 16
    const v1, 0x7f0e01bd

    const-string v2, "field \'nextMapIcon\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIcon:Landroid/widget/ImageView;

    .line 18
    const v1, 0x7f0e01be

    const-string v2, "field \'activeMapInstruction\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e01bf

    const-string v2, "field \'nextMapInstructionContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    .line 22
    const v1, 0x7f0e01c0

    const-string v2, "field \'nextMapInstruction\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstruction:Landroid/widget/TextView;

    .line 24
    const v1, 0x7f0e01bb

    const-string v2, "field \'progressBar\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ProgressBar;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    .line 26
    const v1, 0x7f0e01b9

    const-string v2, "field \'nowIcon\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    .line 28
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/homescreen/TbtView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/homescreen/TbtView;

    .prologue
    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapDistance:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapIcon:Landroid/widget/ImageView;

    .line 33
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIconContainer:Landroid/view/View;

    .line 34
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapIcon:Landroid/widget/ImageView;

    .line 35
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->activeMapInstruction:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstructionContainer:Landroid/view/View;

    .line 37
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nextMapInstruction:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->progressBar:Landroid/widget/ProgressBar;

    .line 39
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/TbtView;->nowIcon:Landroid/view/View;

    .line 40
    return-void
.end method
