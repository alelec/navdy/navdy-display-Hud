.class public Lcom/navdy/hud/app/ui/component/homescreen/MpgView;
.super Landroid/widget/LinearLayout;
.source "MpgView.java"


# instance fields
.field private logger:Lcom/navdy/service/library/log/Logger;

.field mpgLabelTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0199
    .end annotation
.end field

.field mpgTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e019a
    .end annotation
.end field

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method


# virtual methods
.method public getTopAnimator(Landroid/animation/AnimatorSet$Builder;Z)V
    .locals 5
    .param p1, "builder"    # Landroid/animation/AnimatorSet$Builder;
    .param p2, "out"    # Z

    .prologue
    const/4 v4, 0x2

    .line 83
    if-eqz p2, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->getX()F

    move-result v2

    float-to-int v2, v2

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewSpeedOut:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-static {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 85
    .local v1, "xAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 87
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v4, [F

    fill-array-data v3, :array_0

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 88
    .local v0, "alphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 96
    :goto_0
    return-void

    .line 90
    .end local v0    # "alphaAnimator":Landroid/animation/ObjectAnimator;
    .end local v1    # "xAnimator":Landroid/animation/ObjectAnimator;
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->getX()F

    move-result v2

    float-to-int v2, v2

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewSpeedOut:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-static {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 91
    .restart local v1    # "xAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 93
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 94
    .restart local v0    # "alphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 87
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 93
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 56
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 57
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 58
    return-void
.end method

.method public resetTopViewsAnimator()V
    .locals 2

    .prologue
    .line 99
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->setAlpha(F)V

    .line 101
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCustomAnimateMode()Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v0

    .line 102
    .local v0, "mode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 103
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 70
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/MpgView$1;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 79
    :goto_0
    return-void

    .line 72
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->setX(F)V

    goto :goto_0

    .line 76
    :pswitch_1
    sget v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedShrinkLeftX:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->setX(F)V

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method updateInstantaneousFuelConsumption(D)V
    .locals 7
    .param p1, "consumption"    # D

    .prologue
    .line 61
    invoke-static {p1, p2}, Lcom/navdy/hud/app/util/ConversionUtil;->convertLpHundredKmToMPG(D)D

    move-result-wide v0

    .line 62
    .local v0, "mpg":D
    const-string v2, "- -"

    .line 63
    .local v2, "mpgText":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmpl-double v3, v0, v4

    if-lez v3, :cond_0

    .line 64
    const-string v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    double-to-int v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 66
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/MpgView;->mpgTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-void
.end method
