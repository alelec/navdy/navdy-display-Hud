.class Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;
.super Ljava/lang/Object;
.source "SmartDashView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/dashboard/GaugeViewPager$Adapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GaugeViewPagerAdapter"
.end annotation


# instance fields
.field private mExcludedPosition:I

.field private mLeft:Z

.field private mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

.field private mWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;Z)V
    .locals 2
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .param p2, "smartDashWidgetManager"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
    .param p3, "left"    # Z

    .prologue
    .line 526
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 520
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mExcludedPosition:I

    .line 527
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    .line 528
    iput-boolean p3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mLeft:Z

    .line 529
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mLeft:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->buildSmartDashWidgetCache(I)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    .line 531
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->registerForChanges(Ljava/lang/Object;)V

    .line 532
    return-void

    .line 529
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetsCount()I

    move-result v0

    return v0
.end method

.method public getExcludedPosition()I
    .locals 1

    .prologue
    .line 548
    iget v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mExcludedPosition:I

    return v0
.end method

.method public getPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 603
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "recycledView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "isActive"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 564
    if-nez p2, :cond_2

    .line 565
    new-instance v1, Lcom/navdy/hud/app/view/DashboardWidgetView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/navdy/hud/app/view/DashboardWidgetView;-><init>(Landroid/content/Context;)V

    .line 566
    .local v1, "gaugeView":Lcom/navdy/hud/app/view/DashboardWidgetView;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b008d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b008a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-direct {v4, v7, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 571
    :goto_0
    invoke-virtual {v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    .line 574
    .local v2, "oldPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    invoke-virtual {v4, p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresenter(I)Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    move-result-object v3

    .line 576
    .local v3, "presenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    if-eqz v2, :cond_0

    if-eq v2, v3, :cond_0

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetView()Lcom/navdy/hud/app/view/DashboardWidgetView;

    move-result-object v4

    if-ne v4, v1, :cond_0

    .line 577
    invoke-virtual {v2, v10, v10}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 580
    :cond_0
    if-eqz v3, :cond_1

    .line 581
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 582
    .local v0, "args":Landroid/os/Bundle;
    const-string v7, "EXTRA_GRAVITY"

    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mLeft:Z

    if-eqz v4, :cond_3

    move v4, v5

    :goto_1
    invoke-virtual {v0, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 583
    const-string v4, "EXTRA_IS_ACTIVE"

    invoke-virtual {v0, v4, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 584
    if-nez p4, :cond_4

    .line 585
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setWidgetVisibleToUser(Z)V

    .line 595
    :goto_2
    invoke-virtual {v3, v1, v0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 597
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setTag(Ljava/lang/Object;)V

    .line 598
    return-object v1

    .end local v1    # "gaugeView":Lcom/navdy/hud/app/view/DashboardWidgetView;
    .end local v2    # "oldPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    .end local v3    # "presenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :cond_2
    move-object v1, p2

    .line 568
    check-cast v1, Lcom/navdy/hud/app/view/DashboardWidgetView;

    .restart local v1    # "gaugeView":Lcom/navdy/hud/app/view/DashboardWidgetView;
    goto :goto_0

    .line 582
    .restart local v0    # "args":Landroid/os/Bundle;
    .restart local v2    # "oldPresenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    .restart local v3    # "presenter":Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
    :cond_3
    const/4 v4, 0x2

    goto :goto_1

    .line 587
    :cond_4
    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mLeft:Z

    if-eqz v4, :cond_7

    .line 588
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetIdentifier()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeleftGaugeId:Ljava/lang/String;
    invoke-static {v4, v7}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$702(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;

    .line 592
    :goto_3
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    const-string v7, "DRIVE_SCORE_GAUGE_ID"

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeleftGaugeId:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$700(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "DRIVE_SCORE_GAUGE_ID"

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    # getter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeRightGaugeId:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$800(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_5
    move v5, v6

    :cond_6
    iput-boolean v5, v4, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->isShowingDriveScoreGauge:Z

    .line 593
    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setWidgetVisibleToUser(Z)V

    goto :goto_2

    .line 590
    :cond_7
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->getWidgetIdentifier()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->activeRightGaugeId:Ljava/lang/String;
    invoke-static {v4, v7}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;->access$802(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3
.end method

.method public onWidgetsReload(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;)V
    .locals 2
    .param p1, "reload"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 536
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOAD_CACHE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    if-ne p1, v0, :cond_0

    .line 537
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mLeft:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->buildSmartDashWidgetCache(I)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mWidgetCache:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    .line 539
    :cond_0
    return-void

    .line 537
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setExcludedPosition(I)V
    .locals 2
    .param p1, "excludedPosition"    # I

    .prologue
    .line 552
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mSmartDashWidgetManager:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    const-string v1, "EMPTY_WIDGET"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->getIndexForWidgetIdentifier(Ljava/lang/String;)I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 554
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mExcludedPosition:I

    .line 558
    :goto_0
    return-void

    .line 556
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView$GaugeViewPagerAdapter;->mExcludedPosition:I

    goto :goto_0
.end method
