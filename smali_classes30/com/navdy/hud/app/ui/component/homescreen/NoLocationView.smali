.class public Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;
.super Landroid/widget/RelativeLayout;
.source "NoLocationView.java"


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private noLocationAnimator:Landroid/animation/ObjectAnimator;

.field noLocationImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0193
    .end annotation
.end field

.field noLocationTextContainer:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0194
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method


# virtual methods
.method public getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 4
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .param p2, "builder"    # Landroid/animation/AnimatorSet$Builder;

    .prologue
    .line 112
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView$2;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 127
    :goto_0
    return-void

    .line 114
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationImage:Landroid/widget/ImageView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationImageShrinkLeftX:I

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 115
    .local v0, "noLocationAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 116
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationTextContainer:Landroid/widget/LinearLayout;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationTextShrinkLeftX:I

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 117
    .local v1, "noLocationTextAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 121
    .end local v0    # "noLocationAnimator":Landroid/animation/ObjectAnimator;
    .end local v1    # "noLocationTextAnimator":Landroid/animation/ObjectAnimator;
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationImage:Landroid/widget/ImageView;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationImageX:I

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 122
    .restart local v0    # "noLocationAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 123
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationTextContainer:Landroid/widget/LinearLayout;

    sget v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationTextX:I

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 124
    .restart local v1    # "noLocationTextAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p2, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 112
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public hideLocationUI()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "hideLocationUI"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 69
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cancelled animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 75
    :cond_0
    return-void
.end method

.method public isAcquiringLocation()Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->sLogger:Lcom/navdy/service/library/log/Logger;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 57
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 58
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 59
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->bus:Lcom/squareup/otto/Bus;

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 61
    return-void
.end method

.method public setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 98
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView$2;->$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 109
    :goto_0
    return-void

    .line 100
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationImage:Landroid/widget/ImageView;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationImageX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationTextContainer:Landroid/widget/LinearLayout;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationTextX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setX(F)V

    goto :goto_0

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationImage:Landroid/widget/ImageView;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationImageShrinkLeftX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationTextContainer:Landroid/widget/LinearLayout;

    sget v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationTextShrinkLeftX:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setX(F)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showLocationUI()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showLocationUI"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationImage:Landroid/widget/ImageView;

    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/high16 v3, 0x43b40000    # 360.0f

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView$1;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/NoLocationView;->noLocationAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 95
    return-void
.end method
