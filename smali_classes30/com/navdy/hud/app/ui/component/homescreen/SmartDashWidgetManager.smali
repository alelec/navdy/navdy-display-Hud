.class public Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;
.super Ljava/lang/Object;
.source "SmartDashWidgetManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;,
        Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$UserPreferenceChanged;,
        Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;,
        Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;,
        Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;
    }
.end annotation


# static fields
.field public static final ANALOG_CLOCK_WIDGET_ID:Ljava/lang/String; = "ANALOG_CLOCK_WIDGET"

.field public static final CALENDAR_WIDGET_ID:Ljava/lang/String; = "CALENDAR_WIDGET"

.field public static final COMPASS_WIDGET_ID:Ljava/lang/String; = "COMPASS_WIDGET"

.field public static final DIGITAL_CLOCK_2_WIDGET_ID:Ljava/lang/String; = "DIGITAL_CLOCK_2_WIDGET"

.field public static final DIGITAL_CLOCK_WIDGET_ID:Ljava/lang/String; = "DIGITAL_CLOCK_WIDGET"

.field public static final DRIVE_SCORE_GAUGE_ID:Ljava/lang/String; = "DRIVE_SCORE_GAUGE_ID"

.field public static final EMPTY_WIDGET_ID:Ljava/lang/String; = "EMPTY_WIDGET"

.field public static final ENGINE_TEMPERATURE_GAUGE_ID:Ljava/lang/String; = "ENGINE_TEMPERATURE_GAUGE_ID"

.field public static final ETA_GAUGE_ID:Ljava/lang/String; = "ETA_GAUGE_ID"

.field public static final FUEL_GAUGE_ID:Ljava/lang/String; = "FUEL_GAUGE_ID"

.field private static GAUGES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static GAUGE_NAME_LOOKUP:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final GFORCE_WIDGET_ID:Ljava/lang/String; = "GFORCE_WIDGET"

.field public static final MPG_AVG_WIDGET_ID:Ljava/lang/String; = "MPG_AVG_WIDGET"

.field public static final MPG_GRAPH_WIDGET_ID:Ljava/lang/String; = "MPG_GRAPH_WIDGET"

.field public static final MUSIC_WIDGET_ID:Ljava/lang/String; = "MUSIC_WIDGET"

.field public static final PREFERENCE_GAUGE_ENABLED:Ljava/lang/String; = "PREF_GAUGE_ENABLED_"

.field public static final SPEED_LIMIT_SIGN_GAUGE_ID:Ljava/lang/String; = "SPEED_LIMIT_SIGN_GAUGE_ID"

.field public static final TRAFFIC_INCIDENT_GAUGE_ID:Ljava/lang/String; = "TRAFFIC_INCIDENT_GAUGE_ID"

.field public static final WEATHER_WIDGET_ID:Ljava/lang/String; = "WEATHER_GRAPH_WIDGET"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private currentUserPreferences:Landroid/content/SharedPreferences;

.field private filter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;

.field private globalPreferences:Landroid/content/SharedPreferences;

.field private lastLifeCycleEvent:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

.field private mContext:Landroid/content/Context;

.field private mGaugeIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaded:Z

.field private mWidgetIndexMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWidgetPresentersCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    .line 68
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGE_NAME_LOOKUP:Ljava/util/HashSet;

    .line 72
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "CALENDAR_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "COMPASS_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "ANALOG_CLOCK_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "DIGITAL_CLOCK_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "DIGITAL_CLOCK_2_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "DRIVE_SCORE_GAUGE_ID"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "ENGINE_TEMPERATURE_GAUGE_ID"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "FUEL_GAUGE_ID"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "GFORCE_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "MUSIC_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "MPG_AVG_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "SPEED_LIMIT_SIGN_GAUGE_ID"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "TRAFFIC_INCIDENT_GAUGE_ID"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "ETA_GAUGE_ID"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    const-string v2, "EMPTY_WIDGET"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "name":Ljava/lang/String;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "name":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 89
    .restart local v0    # "name":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGE_NAME_LOOKUP:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Landroid/content/Context;)V
    .locals 1
    .param p1, "globalPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetPresentersCache:Ljava/util/HashMap;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mLoaded:Z

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->lastLifeCycleEvent:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    .line 140
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->globalPreferences:Landroid/content/SharedPreferences;

    .line 141
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mContext:Landroid/content/Context;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    .line 143
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetIndexMap:Ljava/util/HashMap;

    .line 144
    new-instance v0, Lcom/squareup/otto/Bus;

    invoke-direct {v0}, Lcom/squareup/otto/Bus;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->bus:Lcom/squareup/otto/Bus;

    .line 145
    return-void
.end method

.method public static isValidGaugeId(Ljava/lang/String;)Z
    .locals 1
    .param p0, "gaugeName"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGE_NAME_LOOKUP:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendLifecycleEvent(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    .prologue
    .line 244
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetPresentersCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 245
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 246
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    .line 247
    .local v0, "cache":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresentersMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 248
    .local v2, "widgetPresenterIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/view/DashboardWidgetPresenter;>;"
    invoke-direct {p0, p1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sendLifecycleEvent(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;Ljava/util/Iterator;)V

    goto :goto_0

    .line 250
    .end local v0    # "cache":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    .end local v2    # "widgetPresenterIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/view/DashboardWidgetPresenter;>;"
    :cond_0
    return-void
.end method

.method private sendLifecycleEvent(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;Ljava/util/Iterator;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/navdy/hud/app/view/DashboardWidgetPresenter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/view/DashboardWidgetPresenter;>;"
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$1;->$SwitchMap$com$navdy$hud$app$ui$component$homescreen$SmartDashWidgetManager$LifecycleEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 256
    :pswitch_0
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->onPause()V

    goto :goto_0

    .line 260
    :pswitch_1
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->onResume()V

    goto :goto_0

    .line 264
    :cond_0
    return-void

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public buildSmartDashWidgetCache(I)Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    .locals 6
    .param p1, "clientIdentifier"    # I

    .prologue
    .line 198
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetPresentersCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 199
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetPresentersCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    .line 200
    .local v0, "cache":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->clear()V

    .line 201
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "widget::: cache cleared for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 208
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 209
    .local v1, "gaugeId":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->add(Ljava/lang/String;)V

    goto :goto_1

    .line 204
    .end local v0    # "cache":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    .end local v1    # "gaugeId":Ljava/lang/String;
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v3}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;-><init>(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;Landroid/content/Context;)V

    .line 205
    .restart local v0    # "cache":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetPresentersCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v3, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "widget::: cache created for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->lastLifeCycleEvent:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    if-eqz v3, :cond_2

    .line 212
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->getWidgetPresentersMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 213
    .local v2, "widgetPresenterIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/view/DashboardWidgetPresenter;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->lastLifeCycleEvent:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    invoke-direct {p0, v3, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sendLifecycleEvent(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;Ljava/util/Iterator;)V

    .line 215
    .end local v2    # "widgetPresenterIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/view/DashboardWidgetPresenter;>;"
    :cond_2
    return-object v0
.end method

.method public getIndexForWidgetIdentifier(Ljava/lang/String;)I
    .locals 1
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetIndexMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetIndexMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getWidgetIdentifierForIndex(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 185
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 188
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGaugeOn(Ljava/lang/String;)Z
    .locals 3
    .param p1, "gaugeIdentifier"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->currentUserPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 96
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "isGaugeOn : Current Preferences is not set, using the global preferences"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->globalPreferences:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->currentUserPreferences:Landroid/content/SharedPreferences;

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->currentUserPreferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PREF_GAUGE_ENABLED_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->PAUSE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->lastLifeCycleEvent:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    .line 234
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->PAUSE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sendLifecycleEvent(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;)V

    .line 235
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 238
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->RESUME:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->lastLifeCycleEvent:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    .line 240
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;->RESUME:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sendLifecycleEvent(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$LifecycleEvent;)V

    .line 241
    return-void
.end method

.method public reLoadAvailableWidgets(Z)V
    .locals 8
    .param p1, "loadAll"    # Z

    .prologue
    .line 148
    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "reLoadAvailableWidgets"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 149
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 150
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetIndexMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 151
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v3

    .line 152
    .local v3, "isDefaultProfile":Z
    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Default profile ? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 153
    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->globalPreferences:Landroid/content/SharedPreferences;

    :goto_0
    iput-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->currentUserPreferences:Landroid/content/SharedPreferences;

    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "i":I
    sget-object v4, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->GAUGES:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 156
    .local v2, "identifier":Ljava/lang/String;
    if-nez p1, :cond_0

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->isGaugeOn(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->filter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->filter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;

    invoke-interface {v5, v2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;->filter(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 157
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "adding Gauge : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", at : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 159
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetIndexMap:Ljava/util/HashMap;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1

    .line 153
    .end local v0    # "i":I
    .end local v2    # "identifier":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v4

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferencesForCurrentDriverProfile(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    goto :goto_0

    .line 161
    .restart local v0    # "i":I
    .restart local v2    # "identifier":Ljava/lang/String;
    :cond_2
    sget-object v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Not adding Gauge, as its filtered out , ID : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 164
    .end local v2    # "identifier":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 165
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mGaugeIds:Ljava/util/List;

    const-string v5, "EMPTY_WIDGET"

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetIndexMap:Ljava/util/HashMap;

    const-string v5, "EMPTY_WIDGET"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    :cond_4
    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mLoaded:Z

    if-eqz v4, :cond_5

    .line 170
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOAD_CACHE:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 171
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v5, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;->RELOADED:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 175
    :goto_2
    return-void

    .line 173
    :cond_5
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mLoaded:Z

    goto :goto_2
.end method

.method public registerForChanges(Ljava/lang/Object;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p1}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 193
    return-void
.end method

.method public setFilter(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->filter:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$IWidgetFilter;

    .line 397
    return-void
.end method

.method public setGaugeOn(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "on"    # Z

    .prologue
    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->currentUserPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 104
    sget-object v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setGaugeOn : Current Preferences is not set, using the global preferences"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->globalPreferences:Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->currentUserPreferences:Landroid/content/SharedPreferences;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->currentUserPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PREF_GAUGE_ENABLED_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 108
    return-void
.end method

.method public updateWidget(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 219
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;->mWidgetPresentersCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 220
    .local v1, "cacheCollection":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;>;"
    if-eqz v1, :cond_1

    .line 221
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 222
    .local v2, "widgetCacheIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 223
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;

    .line 224
    .local v0, "cache":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;->updateWidget(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 229
    .end local v0    # "cache":Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;
    .end local v2    # "widgetCacheIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;>;"
    :cond_1
    return-void
.end method
