.class public Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;
.super Ljava/lang/Object;
.source "HomeScreenResourceValues.java"


# static fields
.field public static final activeMapHeight:I

.field public static final activeRoadEarlyManeuverShrinkLeftX:I

.field public static final activeRoadEarlyManeuverShrinkRightX:I

.field public static final activeRoadEarlyManeuverWidth:I

.field public static final activeRoadEarlyManeuverWidthShrunk:I

.field public static final activeRoadEarlyManeuverX:I

.field public static final activeRoadEarlyManeuverY:I

.field public static final activeRoadEarlyManeuverYShrunk:I

.field public static final activeRoadInfoDistanceX:I

.field public static final activeRoadInfoIconShrinkLeftX:I

.field public static final activeRoadInfoIconShrinkRightX:I

.field public static final activeRoadInfoIconX:I

.field public static final activeRoadInfoInstructionShrinkLeftX:I

.field public static final activeRoadInfoInstructionShrinkRightX:I

.field public static final activeRoadInfoInstructionShrinkWidth:I

.field public static final activeRoadInfoInstructionTranslationYAnimation:I

.field public static final activeRoadInfoInstructionWidth:I

.field public static final activeRoadInfoInstructionX:I

.field public static final activeRoadProgressBarWidth:I

.field public static final activeRoadShrunkDistanceShrinkLeftX:I

.field public static final activeRoadShrunkDistanceShrinkRightX:I

.field public static final activeRoadShrunkDistanceX:I

.field public static final activeRoadShrunkDistanceY:I

.field public static final activeRoadShrunkProgressBarWidth:I

.field public static final badTrafficColor:I

.field public static final calcRoute:Ljava/lang/String;

.field public static final cancelTrip:Ljava/lang/String;

.field public static final etaShrinkLeftX:I

.field public static final etaX:I

.field public static final exitStr:Ljava/lang/String;

.field public static final fullWidth:I

.field public static final goodTrafficColor:I

.field public static final iconIndicatorActiveY:I

.field public static final iconIndicatorOpenY:I

.field public static final iconIndicatorShrinkLeft_X:I

.field public static final iconIndicatorShrinkModeX:I

.field public static final iconIndicatorShrinkRight_X:I

.field public static final iconIndicatorX:I

.field public static final laneGuidanceY:I

.field public static final mainScreenRightSectionShrinkX:I

.field public static final mainScreenRightSectionX:I

.field public static final mapMaskShrinkLeftX:I

.field public static final mapMaskShrinkRightX:I

.field public static final mapMaskShrinkWidth:I

.field public static final mapMaskX:I

.field public static final mapViewShrinkLeftX:I

.field public static final mapViewShrinkModeX:I

.field public static final mapViewShrinkRightX:I

.field public static final mapViewX:I

.field public static final noLocationImageShrinkLeftX:I

.field public static final noLocationImageShrinkRightX:I

.field public static final noLocationImageX:I

.field public static final noLocationTextShrinkLeftX:I

.field public static final noLocationTextShrinkRightX:I

.field public static final noLocationTextX:I

.field public static final openMapHeight:I

.field public static final openMapRoadInfoMargin:I

.field public static final openMapRoadInfoShrinkLeft_L_Margin:I

.field public static final openMapRoadInfoShrinkLeft_R_Margin:I

.field public static final openMapRoadInfoShrinkRight_L_Margin:I

.field public static final openMapRoadInfoShrinkRight_R_Margin:I

.field public static final reCalcAnimColor:I

.field public static final recalcShrinkLeftX:I

.field public static final recalcShrinkRightX:I

.field public static final recalcX:I

.field public static final retry:Ljava/lang/String;

.field public static final routeCalcFailed:Ljava/lang/String;

.field public static final routeCalcFailureColor:I

.field public static final routeCalcIndicatorX:I

.field public static final routeCalcInfoShrinkLeftX:I

.field public static final routeCalcInfoShrinkRightX:I

.field public static final routeCalcInfoShrinkWidth:I

.field public static final routeCalcInfoWidth:I

.field public static final routeCalcInfoX:I

.field public static final routeCalcProgressShrinkLeftX:I

.field public static final routeCalcProgressShrinkRightX:I

.field public static final routeCalcProgressX:I

.field public static final routeCalcRouteNameX:I

.field public static final routeOverviewBoxHeight:I

.field public static final routeOverviewBoxWidth:I

.field public static final routeOverviewBoxX:I

.field public static final routeOverviewBoxY:I

.field public static final routePickerBoxHeight:I

.field public static final routePickerBoxWidth:I

.field public static final routePickerBoxX:I

.field public static final routePickerBoxY:I

.field public static final speedKm:Ljava/lang/String;

.field public static final speedMeters:Ljava/lang/String;

.field public static final speedMph:Ljava/lang/String;

.field public static final speedShrinkLeftX:I

.field public static final speedShrinkRightX:I

.field public static final speedX:I

.field public static final startFluctuatorDimension:I

.field public static final timeShrinkLeftX:I

.field public static final timeShrinkRightX:I

.field public static final timeX:I

.field public static final topViewLeftAnimationIn:I

.field public static final topViewLeftAnimationOut:I

.field public static final topViewRightAnimationIn:I

.field public static final topViewRightAnimationOut:I

.field public static final topViewSpeedOut:I

.field public static final transformCenterIconHeight:I

.field public static final transformCenterIconWidth:I

.field public static final transformCenterMapOnRouteOverviewY:I

.field public static final transformCenterMapOnRouteY:I

.field public static final transformCenterMoveX:I

.field public static final transformCenterOverviewRouteY:I

.field public static final transformCenterOverviewX:I

.field public static final transformCenterOverviewY:I

.field public static final transformCenterPickerY:I

.field public static final transformCenterShrinkLeftX:I

.field public static final transformCenterShrinkRightX:I

.field public static final transformCenterX:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 170
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 177
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0901aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedMph:Ljava/lang/String;

    .line 178
    const v1, 0x7f090196

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedKm:Ljava/lang/String;

    .line 179
    const v1, 0x7f0901a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedMeters:Ljava/lang/String;

    .line 180
    const v1, 0x7f0900fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->exitStr:Ljava/lang/String;

    .line 181
    const v1, 0x7f090039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->calcRoute:Ljava/lang/String;

    .line 182
    const v1, 0x7f090050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->cancelTrip:Ljava/lang/String;

    .line 183
    const v1, 0x7f09023f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcFailed:Ljava/lang/String;

    .line 184
    const v1, 0x7f09022d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->retry:Ljava/lang/String;

    .line 192
    const v1, 0x7f0d009b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->reCalcAnimColor:I

    .line 193
    const v1, 0x7f0d00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->badTrafficColor:I

    .line 194
    const v1, 0x7f0d00b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->goodTrafficColor:I

    .line 195
    const v1, 0x7f0d009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcFailureColor:I

    .line 203
    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeMapHeight:I

    .line 204
    const v1, 0x7f0b00fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapHeight:I

    .line 206
    const v1, 0x7f0b00bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mainScreenRightSectionX:I

    .line 207
    const v1, 0x7f0b00bc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mainScreenRightSectionShrinkX:I

    .line 209
    const v1, 0x7f0b00fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoMargin:I

    .line 210
    const v1, 0x7f0b00fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkLeft_L_Margin:I

    .line 211
    const v1, 0x7f0b00fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkLeft_R_Margin:I

    .line 212
    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkRight_L_Margin:I

    .line 213
    const v1, 0x7f0b00ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->openMapRoadInfoShrinkRight_R_Margin:I

    .line 215
    const v1, 0x7f0b00c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorX:I

    .line 216
    const v1, 0x7f0b00c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorOpenY:I

    .line 217
    const v1, 0x7f0b00bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorActiveY:I

    .line 218
    const v1, 0x7f0b00c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkLeft_X:I

    .line 219
    const v1, 0x7f0b00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkRight_X:I

    .line 220
    const v1, 0x7f0b00c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->iconIndicatorShrinkModeX:I

    .line 222
    const v1, 0x7f0b00ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewX:I

    .line 223
    const v1, 0x7f0b00cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewShrinkLeftX:I

    .line 224
    const v1, 0x7f0b00cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewShrinkRightX:I

    .line 225
    const v1, 0x7f0b00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapViewShrinkModeX:I

    .line 227
    const v1, 0x7f0b00ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskX:I

    .line 228
    const v1, 0x7f0b00c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkLeftX:I

    .line 229
    const v1, 0x7f0b00c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkRightX:I

    .line 230
    const v1, 0x7f0b00c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->mapMaskShrinkWidth:I

    .line 232
    const v1, 0x7f0b0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->fullWidth:I

    .line 234
    const v1, 0x7f0b007e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->etaX:I

    .line 235
    const v1, 0x7f0b0080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->etaShrinkLeftX:I

    .line 237
    const v1, 0x7f0b0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoDistanceX:I

    .line 238
    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconX:I

    .line 239
    const v1, 0x7f0b0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionX:I

    .line 240
    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionTranslationYAnimation:I

    .line 241
    const v1, 0x7f0b000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconShrinkLeftX:I

    .line 242
    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoIconShrinkRightX:I

    .line 243
    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkLeftX:I

    .line 244
    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkRightX:I

    .line 245
    const v1, 0x7f0b0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionWidth:I

    .line 246
    const v1, 0x7f0b000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadInfoInstructionShrinkWidth:I

    .line 247
    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadProgressBarWidth:I

    .line 249
    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverX:I

    .line 250
    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverY:I

    .line 251
    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverYShrunk:I

    .line 252
    const v1, 0x7f0b0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverShrinkLeftX:I

    .line 253
    const v1, 0x7f0b0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverShrinkRightX:I

    .line 254
    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverWidth:I

    .line 255
    const v1, 0x7f0b0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadEarlyManeuverWidthShrunk:I

    .line 257
    const v1, 0x7f0b0016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkDistanceX:I

    .line 258
    const v1, 0x7f0b0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkDistanceY:I

    .line 259
    const v1, 0x7f0b0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkDistanceShrinkLeftX:I

    .line 260
    const v1, 0x7f0b0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkDistanceShrinkRightX:I

    .line 261
    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->activeRoadShrunkProgressBarWidth:I

    .line 263
    const v1, 0x7f0b0108

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->recalcX:I

    .line 264
    const v1, 0x7f0b0106

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->recalcShrinkLeftX:I

    .line 265
    const v1, 0x7f0b0107

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->recalcShrinkRightX:I

    .line 267
    const v1, 0x7f0b0149

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedX:I

    .line 268
    const v1, 0x7f0b014a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedShrinkLeftX:I

    .line 269
    const v1, 0x7f0b014b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->speedShrinkRightX:I

    .line 271
    const v1, 0x7f0b0189

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeX:I

    .line 272
    const v1, 0x7f0b018a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeShrinkLeftX:I

    .line 273
    const v1, 0x7f0b018b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->timeShrinkRightX:I

    .line 275
    const v1, 0x7f0b010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcIndicatorX:I

    .line 276
    const v1, 0x7f0b0115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcRouteNameX:I

    .line 277
    const v1, 0x7f0b0114

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcProgressX:I

    .line 278
    const v1, 0x7f0b0112

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcProgressShrinkLeftX:I

    .line 279
    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcProgressShrinkRightX:I

    .line 280
    const v1, 0x7f0b0111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcInfoX:I

    .line 281
    const v1, 0x7f0b010d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcInfoShrinkLeftX:I

    .line 282
    const v1, 0x7f0b010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcInfoShrinkRightX:I

    .line 283
    const v1, 0x7f0b0110

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcInfoWidth:I

    .line 284
    const v1, 0x7f0b010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeCalcInfoShrinkWidth:I

    .line 286
    const v1, 0x7f0b00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationImageX:I

    .line 287
    const v1, 0x7f0b00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationImageShrinkLeftX:I

    .line 288
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationImageShrinkRightX:I

    .line 289
    const v1, 0x7f0b00e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationTextX:I

    .line 290
    const v1, 0x7f0b00e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationTextShrinkLeftX:I

    .line 291
    const v1, 0x7f0b00e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->noLocationTextShrinkRightX:I

    .line 294
    const v1, 0x7f0b01ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterX:I

    .line 295
    const v1, 0x7f0b01ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterShrinkLeftX:I

    .line 296
    const v1, 0x7f0b01ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterShrinkRightX:I

    .line 297
    const v1, 0x7f0b01a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterMapOnRouteY:I

    .line 298
    const v1, 0x7f0b01a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterIconWidth:I

    .line 299
    const v1, 0x7f0b01a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterIconHeight:I

    .line 300
    const v1, 0x7f0b01a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewX:I

    .line 301
    const v1, 0x7f0b01a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterMapOnRouteOverviewY:I

    .line 302
    const v1, 0x7f0b01a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewY:I

    .line 303
    const v1, 0x7f0b01a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterOverviewRouteY:I

    .line 304
    const v1, 0x7f0b01ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterPickerY:I

    .line 305
    const v1, 0x7f0b01aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->transformCenterMoveX:I

    .line 308
    const v1, 0x7f0b011c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxX:I

    .line 309
    const v1, 0x7f0b011d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxY:I

    .line 310
    const v1, 0x7f0b011b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxWidth:I

    .line 311
    const v1, 0x7f0b011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routePickerBoxHeight:I

    .line 313
    const v1, 0x7f0b0118

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxX:I

    .line 314
    const v1, 0x7f0b0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxY:I

    .line 315
    const v1, 0x7f0b0117

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxWidth:I

    .line 316
    const v1, 0x7f0b0116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->routeOverviewBoxHeight:I

    .line 318
    const v1, 0x7f0b0198

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewLeftAnimationOut:I

    .line 319
    const v1, 0x7f0b0197

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewLeftAnimationIn:I

    .line 320
    const v1, 0x7f0b019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewRightAnimationOut:I

    .line 321
    const v1, 0x7f0b0199

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewRightAnimationIn:I

    .line 323
    const v1, 0x7f0b00ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->laneGuidanceY:I

    .line 325
    const v1, 0x7f0b019b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->topViewSpeedOut:I

    .line 327
    const v1, 0x7f0b0151

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenResourceValues;->startFluctuatorDimension:I

    .line 328
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
