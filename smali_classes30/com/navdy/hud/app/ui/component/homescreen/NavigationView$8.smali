.class Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;
.super Ljava/lang/Object;
.source "NavigationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapToArMode(ZZZLcom/here/android/mpa/common/GeoPosition;DDZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field final synthetic val$animate:Z

.field final synthetic val$changeState:Z

.field final synthetic val$cleanupMapOverview:Z

.field final synthetic val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field final synthetic val$tilt:D

.field final synthetic val$useLastZoom:Z

.field final synthetic val$zoom:D


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;ZZZLcom/here/android/mpa/common/GeoPosition;DDZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 1061
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iput-boolean p2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$animate:Z

    iput-boolean p3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$changeState:Z

    iput-boolean p4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$useLastZoom:Z

    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    iput-wide p6, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$zoom:D

    iput-wide p8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$tilt:D

    iput-boolean p10, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$cleanupMapOverview:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 1064
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->this$0:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$animate:Z

    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$changeState:Z

    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$useLastZoom:Z

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    iget-wide v6, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$zoom:D

    iget-wide v8, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$tilt:D

    double-to-float v8, v8

    iget-boolean v9, p0, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView$8;->val$cleanupMapOverview:Z

    # invokes: Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapToArModeInternal(ZZZLcom/here/android/mpa/common/GeoPosition;DFZ)V
    invoke-static/range {v1 .. v9}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->access$800(Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;ZZZLcom/here/android/mpa/common/GeoPosition;DFZ)V

    .line 1065
    return-void
.end method
