.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;
.super Ljava/lang/Object;
.source "TbtView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/tbt/TbtView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0007\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\nR\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;",
        "",
        "()V",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "getLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "paddingFull",
        "",
        "getPaddingFull",
        "()I",
        "paddingMedium",
        "getPaddingMedium",
        "resources",
        "Landroid/content/res/Resources;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "shrinkLeftX",
        "",
        "getShrinkLeftX",
        "()F",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private final getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 26
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->access$getLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 27
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtView;->resources:Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->access$getResources$cp()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getPaddingFull()I
    .locals 1

    .prologue
    .line 28
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtView;->paddingFull:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->access$getPaddingFull$cp()I

    move-result v0

    return v0
.end method

.method public final getPaddingMedium()I
    .locals 1

    .prologue
    .line 29
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtView;->paddingMedium:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->access$getPaddingMedium$cp()I

    move-result v0

    return v0
.end method

.method public final getShrinkLeftX()F
    .locals 1

    .prologue
    .line 30
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtView;->shrinkLeftX:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->access$getShrinkLeftX$cp()F

    move-result v0

    return v0
.end method
