.class public final synthetic Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-static {}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->values()[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->values()[Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
