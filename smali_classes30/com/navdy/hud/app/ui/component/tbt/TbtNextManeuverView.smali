.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;
.super Landroid/widget/LinearLayout;
.source "TbtNextManeuverView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 !2\u00020\u0001:\u0001!B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0014\u001a\u00020\u0015J\u001a\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u000c2\n\u0010\u0018\u001a\u00060\u0019R\u00020\u001aJ\u0008\u0010\u001b\u001a\u00020\u0015H\u0014J\u000e\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u000cJ\u000e\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u001fJ\u0008\u0010 \u001a\u00020\u0015H\u0002R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "currentMode",
        "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;",
        "lastManeuverState",
        "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;",
        "lastNextTurnIconId",
        "nextManeuverIcon",
        "Landroid/widget/ImageView;",
        "nextManeuverText",
        "Landroid/widget/TextView;",
        "clear",
        "",
        "getCustomAnimator",
        "mode",
        "mainBuilder",
        "Landroid/animation/AnimatorSet$Builder;",
        "Landroid/animation/AnimatorSet;",
        "onFinishInflate",
        "setMode",
        "updateDisplay",
        "event",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "updateIcon",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView$Companion;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field private lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field private lastNextTurnIconId:I

.field private nextManeuverIcon:Landroid/widget/ImageView;

.field private nextManeuverText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView$Companion;

    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TbtNextManeuverView"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "defStyleAttr"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    return-void
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 26
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private final updateIcon()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 77
    iget v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->nextManeuverIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->setVisibility(I)V

    .line 87
    :goto_0
    return-void

    .line 82
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->setVisibility(I)V

    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->nextManeuverIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    :cond_3
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->nextManeuverIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->setVisibility(I)V

    .line 65
    return-void
.end method

.method public final getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 44
    const v0, 0x7f0e021f

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->nextManeuverText:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f0e021e

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.ImageView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->nextManeuverIcon:Landroid/widget/ImageView;

    .line 46
    return-void
.end method

.method public final setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 50
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 51
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->updateIcon()V

    goto :goto_0

    .line 54
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    iget v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    if-ne v0, v1, :cond_0

    .line 74
    :goto_0
    return-void

    .line 71
    :cond_0
    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastNextTurnIconId:I

    .line 72
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 73
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->updateIcon()V

    goto :goto_0
.end method
