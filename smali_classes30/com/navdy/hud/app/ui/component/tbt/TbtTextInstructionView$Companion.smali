.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;
.super Ljava/lang/Object;
.source "TbtTextInstructionView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0007\n\u0002\u0008\r\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006R\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0016R\u0014\u0010\u001d\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0016R\u0014\u0010\u001f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0006\u00a8\u0006!"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;",
        "",
        "()V",
        "fullWidth",
        "",
        "getFullWidth",
        "()I",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "getLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "mediumWidth",
        "getMediumWidth",
        "resources",
        "Landroid/content/res/Resources;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "singleLineMinWidth",
        "getSingleLineMinWidth",
        "size18",
        "",
        "getSize18",
        "()F",
        "size20",
        "getSize20",
        "size22",
        "getSize22",
        "size24",
        "getSize24",
        "size26",
        "getSize26",
        "smallWidth",
        "getSmallWidth",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getFullWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getFullWidth()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getMediumWidth()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSingleLineMinWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSingleLineMinWidth()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getSize18$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize18()F

    move-result v0

    return v0
.end method

.method public static final synthetic access$getSize20$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize20()F

    move-result v0

    return v0
.end method

.method public static final synthetic access$getSize22$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize22()F

    move-result v0

    return v0
.end method

.method public static final synthetic access$getSize24$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize24()F

    move-result v0

    return v0
.end method

.method public static final synthetic access$getSize26$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize26()F

    move-result v0

    return v0
.end method

.method public static final synthetic access$getSmallWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSmallWidth()I

    move-result v0

    return v0
.end method

.method private final getFullWidth()I
    .locals 1

    .prologue
    .line 28
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->fullWidth:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getFullWidth$cp()I

    move-result v0

    return v0
.end method

.method private final getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 26
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getMediumWidth()I
    .locals 1

    .prologue
    .line 29
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->mediumWidth:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getMediumWidth$cp()I

    move-result v0

    return v0
.end method

.method private final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 27
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->resources:Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getResources$cp()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private final getSingleLineMinWidth()I
    .locals 1

    .prologue
    .line 31
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->singleLineMinWidth:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getSingleLineMinWidth$cp()I

    move-result v0

    return v0
.end method

.method private final getSize18()F
    .locals 1

    .prologue
    .line 36
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size18:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getSize18$cp()F

    move-result v0

    return v0
.end method

.method private final getSize20()F
    .locals 1

    .prologue
    .line 35
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size20:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getSize20$cp()F

    move-result v0

    return v0
.end method

.method private final getSize22()F
    .locals 1

    .prologue
    .line 34
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size22:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getSize22$cp()F

    move-result v0

    return v0
.end method

.method private final getSize24()F
    .locals 1

    .prologue
    .line 33
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size24:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getSize24$cp()F

    move-result v0

    return v0
.end method

.method private final getSize26()F
    .locals 1

    .prologue
    .line 32
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size26:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getSize26$cp()F

    move-result v0

    return v0
.end method

.method private final getSmallWidth()I
    .locals 1

    .prologue
    .line 30
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->smallWidth:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->access$getSmallWidth$cp()I

    move-result v0

    return v0
.end method
