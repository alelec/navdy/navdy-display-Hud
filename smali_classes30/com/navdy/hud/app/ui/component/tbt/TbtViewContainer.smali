.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;
.super Landroid/widget/FrameLayout;
.source "TbtViewContainer.kt"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000 .2\u00020\u00012\u00020\u0002:\u0001.B\u000f\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u0017\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008B\u001f\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0006\u0010\u001f\u001a\u00020 J\u001a\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u00192\n\u0010#\u001a\u00060$R\u00020%J\u0008\u0010&\u001a\u0004\u0018\u00010\u0012J\u000e\u0010\'\u001a\u00020 2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0008\u0010(\u001a\u00020 H\u0014J\u0008\u0010)\u001a\u00020 H\u0016J\u0008\u0010*\u001a\u00020 H\u0016J\u000e\u0010+\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u0019J\u0010\u0010,\u001a\u00020 2\u0006\u0010-\u001a\u00020\u0012H\u0007R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;",
        "Landroid/widget/FrameLayout;",
        "Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "activeView",
        "Lcom/navdy/hud/app/ui/component/tbt/TbtView;",
        "homeScreenView",
        "Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;",
        "inactiveView",
        "lastEvent",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "lastManeuverDisplay",
        "lastManeuverId",
        "",
        "lastManeuverState",
        "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;",
        "lastMode",
        "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;",
        "lastRoadText",
        "lastTurnIconId",
        "lastTurnText",
        "paused",
        "",
        "clearState",
        "",
        "getCustomAnimator",
        "mode",
        "mainBuilder",
        "Landroid/animation/AnimatorSet$Builder;",
        "Landroid/animation/AnimatorSet;",
        "getLastManeuverDisplay",
        "init",
        "onFinishInflate",
        "onPause",
        "onResume",
        "setView",
        "updateDisplay",
        "event",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

# The value of this static final field might be set in the static constructor
.field private static final MANEUVER_PROGRESS_ANIMATION_DURATION:J = 0xfaL

# The value of this static final field might be set in the static constructor
.field private static final MANEUVER_SWAP_DELAY:J = 0x12cL

# The value of this static final field might be set in the static constructor
.field private static final MANEUVER_SWAP_DURATION:J = 0x1f4L

.field private static final bus:Lcom/squareup/otto/Bus;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final itemHeight:F

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final maneuverSwapInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field private static final remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final resources:Landroid/content/res/Resources;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

.field private lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

.field private lastManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

.field private lastManeuverId:Ljava/lang/String;

.field private lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field private lastMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field private lastRoadText:Ljava/lang/String;

.field private lastTurnIconId:I

.field private lastTurnText:Ljava/lang/String;

.field private paused:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TbtViewContainer"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->logger:Lcom/navdy/service/library/log/Logger;

    .line 31
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "HudApplication.getAppContext().resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->resources:Landroid/content/res/Resources;

    .line 32
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->itemHeight:F

    .line 33
    const-wide/16 v0, 0xfa

    sput-wide v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_PROGRESS_ANIMATION_DURATION:J

    .line 34
    const-wide/16 v0, 0x1f4

    sput-wide v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_SWAP_DURATION:J

    .line 35
    const-wide/16 v0, 0x12c

    sput-wide v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_SWAP_DELAY:J

    .line 36
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->maneuverSwapInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 37
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    const-string v1, "RemoteDeviceManager.getInstance()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    .line 38
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getRemoteDeviceManager()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    const-string v1, "remoteDeviceManager.bus"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->bus:Lcom/squareup/otto/Bus;

    .line 39
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getRemoteDeviceManager()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    const-string v1, "remoteDeviceManager.uiStateManager"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastTurnIconId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastTurnIconId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "defStyleAttr"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastTurnIconId:I

    return-void
.end method

.method public static final synthetic access$getBus$cp()Lcom/squareup/otto/Bus;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method public static final synthetic access$getInactiveView$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;)Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    return-object v0
.end method

.method public static final synthetic access$getItemHeight$cp()F
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->itemHeight:F

    return v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getMANEUVER_PROGRESS_ANIMATION_DURATION$cp()J
    .locals 2

    .prologue
    .line 28
    sget-wide v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_PROGRESS_ANIMATION_DURATION:J

    return-wide v0
.end method

.method public static final synthetic access$getMANEUVER_SWAP_DELAY$cp()J
    .locals 2

    .prologue
    .line 28
    sget-wide v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_SWAP_DELAY:J

    return-wide v0
.end method

.method public static final synthetic access$getMANEUVER_SWAP_DURATION$cp()J
    .locals 2

    .prologue
    .line 28
    sget-wide v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_SWAP_DURATION:J

    return-wide v0
.end method

.method public static final synthetic access$getManeuverSwapInterpolator$cp()Landroid/view/animation/AccelerateDecelerateInterpolator;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->maneuverSwapInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    return-object v0
.end method

.method public static final synthetic access$getRemoteDeviceManager$cp()Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    return-object v0
.end method

.method public static final synthetic access$getResources$cp()Landroid/content/res/Resources;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->resources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static final synthetic access$getUiStateManager$cp()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-object v0
.end method

.method public static final synthetic access$setInactiveView$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;Lcom/navdy/hud/app/ui/component/tbt/TbtView;)V
    .locals 0
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;
    .param p1, "<set-?>"    # Lcom/navdy/hud/app/ui/component/tbt/TbtView;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .prologue
    .line 28
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->_$_findViewCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final clearState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastTurnIconId:I

    move-object v0, v1

    .line 79
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastTurnText:Ljava/lang/String;

    .line 80
    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastRoadText:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->clear()V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->clear()V

    .line 83
    :cond_1
    return-void
.end method

.method public final getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V

    .line 95
    :cond_1
    return-void
.end method

.method public final getLastManeuverDisplay()Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    return-object v0
.end method

.method public final init(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 1
    .param p1, "homeScreenView"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "homeScreenView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 74
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 75
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 67
    const v0, 0x7f0e0216

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    .line 68
    const v0, 0x7f0e0217

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v0, :cond_2

    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getItemHeight()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setTranslationY(F)V

    .line 70
    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->paused:Z

    if-eqz v0, :cond_0

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->paused:Z

    .line 147
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "::onPause:tbt"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 151
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->paused:Z

    if-nez v1, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->paused:Z

    .line 155
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "::onResume:tbt"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 157
    .local v0, "event":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    if-eqz v0, :cond_0

    .line 158
    const/4 v1, 0x0

    check-cast v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 159
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "::onResume:tbt updated last event"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V

    goto :goto_0
.end method

.method public final setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 89
    :cond_1
    return-void
.end method

.method public final updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const-string v2, "event"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 100
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v0

    .line 101
    .local v0, "navigationActive":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 105
    iget-boolean v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->empty:Z

    if-eqz v2, :cond_2

    .line 106
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->clearState()V

    .line 140
    :cond_0
    :goto_1
    return-void

    .line 100
    .end local v0    # "navigationActive":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 109
    .restart local v0    # "navigationActive":Z
    :cond_2
    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 110
    sget-object v2, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v4, "null pending turn"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move-object v2, v3

    .line 111
    check-cast v2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 112
    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastManeuverId:Ljava/lang/String;

    goto :goto_1

    .line 115
    :cond_3
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastEvent:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 116
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->paused:Z

    if-eqz v2, :cond_4

    .line 117
    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverId:Ljava/lang/String;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastManeuverId:Ljava/lang/String;

    goto :goto_1

    .line 120
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastManeuverId:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverId:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 122
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    .line 123
    .local v1, "temp":Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    .line 124
    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    .line 125
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v2, :cond_5

    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getItemHeight()F

    move-result v3

    neg-float v3, v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setTranslationY(F)V

    .line 126
    :cond_5
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v2, :cond_6

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V

    .line 127
    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_7

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_7

    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getMANEUVER_SWAP_DURATION()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_7

    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getMANEUVER_SWAP_DELAY()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 128
    :cond_7
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_8

    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getItemHeight()F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_8

    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getMANEUVER_SWAP_DURATION()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_8

    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getMANEUVER_SWAP_DELAY()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    if-eqz v3, :cond_8

    new-instance v2, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$1;-><init>(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;)V

    check-cast v2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v3, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 132
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 136
    .end local v1    # "temp":Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    :cond_8
    :goto_2
    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverId:Ljava/lang/String;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastManeuverId:Ljava/lang/String;

    .line 138
    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    goto/16 :goto_1

    .line 135
    :cond_9
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->activeView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    if-eqz v2, :cond_8

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V

    goto :goto_2
.end method
