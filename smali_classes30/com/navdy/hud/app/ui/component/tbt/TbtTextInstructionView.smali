.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;
.super Landroid/widget/TextView;
.source "TbtTextInstructionView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;,
        Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\u0018\u0000 82\u00020\u0001:\u000289B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ(\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\tH\u0002J\u0006\u0010\"\u001a\u00020\u001dJ\u0010\u0010#\u001a\u00020$2\u0006\u0010\u001e\u001a\u00020\u0010H\u0002J\u001a\u0010%\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\u000c2\n\u0010\'\u001a\u00060(R\u00020)J \u0010*\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u000e2\u0006\u0010.\u001a\u00020\u000eH\u0002J\u000e\u0010/\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\u000cJ\u0010\u00100\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\tH\u0002J\u0018\u00102\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010.\u001a\u00020\u000eH\u0002J8\u00103\u001a\u00020\u000e2\u0006\u00104\u001a\u00020 2\u0006\u0010!\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u00105\u001a\u00020\u000e2\u0006\u00106\u001a\u00020\u000eH\u0002J\"\u00107\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\u0008\u0008\u0002\u0010-\u001a\u00020\u000e2\u0008\u0008\u0002\u0010.\u001a\u00020\u000eR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u00060\u0018j\u0002`\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006:"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;",
        "Landroid/widget/TextView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "currentMode",
        "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;",
        "instructionWidthIncludesNext",
        "",
        "lastInstruction",
        "",
        "lastInstructionWidth",
        "lastManeuverState",
        "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;",
        "lastRoadText",
        "lastTurnText",
        "sizeCalculationTextView",
        "stringBuilder",
        "Ljava/lang/StringBuilder;",
        "Lkotlin/text/StringBuilder;",
        "widthInfo",
        "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;",
        "calculateTextWidth",
        "",
        "instruction",
        "textSize",
        "",
        "maxWidth",
        "clear",
        "getBoldText",
        "Landroid/text/SpannableStringBuilder;",
        "getCustomAnimator",
        "mode",
        "mainBuilder",
        "Landroid/animation/AnimatorSet$Builder;",
        "Landroid/animation/AnimatorSet;",
        "setInstruction",
        "event",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "adjustWidthForNextManeuver",
        "nextManeuverVisible",
        "setMode",
        "setViewWidth",
        "width",
        "setWidth",
        "tryTextSize",
        "fontSize",
        "check2Lines",
        "applyBold",
        "updateDisplay",
        "Companion",
        "WidthInfo",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

.field private static final fullWidth:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final mediumWidth:I

.field private static final resources:Landroid/content/res/Resources;

.field private static final singleLineMinWidth:I

.field private static final size18:F

.field private static final size20:F

.field private static final size22:F

.field private static final size24:F

.field private static final size26:F

.field private static final smallWidth:I


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field private instructionWidthIncludesNext:Z

.field private lastInstruction:Ljava/lang/String;

.field private lastInstructionWidth:I

.field private lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field private lastRoadText:Ljava/lang/String;

.field private lastTurnText:Ljava/lang/String;

.field private final sizeCalculationTextView:Landroid/widget/TextView;

.field private final stringBuilder:Ljava/lang/StringBuilder;

.field private final widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TbtTextInstructionView"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 27
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "HudApplication.getAppContext().resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->resources:Landroid/content/res/Resources;

    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->fullWidth:I

    .line 29
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0180

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->mediumWidth:I

    .line 30
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0182

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->smallWidth:I

    .line 31
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0181

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->singleLineMinWidth:I

    .line 32
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size26:F

    .line 33
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size24:F

    .line 34
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size22:F

    .line 35
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size20:F

    .line 36
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size18:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    .line 64
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    .line 65
    new-instance v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->sizeCalculationTextView:Landroid/widget/TextView;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    .line 64
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    .line 65
    new-instance v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->sizeCalculationTextView:Landroid/widget/TextView;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "defStyleAttr"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    .line 64
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    .line 65
    new-instance v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->sizeCalculationTextView:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$getFullWidth$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->fullWidth:I

    return v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getMediumWidth$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->mediumWidth:I

    return v0
.end method

.method public static final synthetic access$getResources$cp()Landroid/content/res/Resources;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->resources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static final synthetic access$getSingleLineMinWidth$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->singleLineMinWidth:I

    return v0
.end method

.method public static final synthetic access$getSize18$cp()F
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size18:F

    return v0
.end method

.method public static final synthetic access$getSize20$cp()F
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size20:F

    return v0
.end method

.method public static final synthetic access$getSize22$cp()F
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size22:F

    return v0
.end method

.method public static final synthetic access$getSize24$cp()F
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size24:F

    return v0
.end method

.method public static final synthetic access$getSize26$cp()F
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->size26:F

    return v0
.end method

.method public static final synthetic access$getSmallWidth$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->smallWidth:I

    return v0
.end method

.method private final calculateTextWidth(Ljava/lang/String;FLcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;I)V
    .locals 9
    .param p1, "instruction"    # Ljava/lang/String;
    .param p2, "textSize"    # F
    .param p3, "widthInfo"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;
    .param p4, "maxWidth"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 164
    invoke-virtual {p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->clear()V

    .line 165
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->sizeCalculationTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 166
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->sizeCalculationTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 167
    .local v2, "paint":Landroid/text/TextPaint;
    new-instance v0, Landroid/text/StaticLayout;

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move v3, p4

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 168
    .local v0, "layout":Landroid/text/StaticLayout;
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    invoke-virtual {p3, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->setNumLines$app_hudRelease(I)V

    .line 169
    invoke-virtual {v0, v7}, Landroid/text/StaticLayout;->getLineMax(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->setLineWidth1$app_hudRelease(I)V

    .line 170
    invoke-virtual {p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->getNumLines$app_hudRelease()I

    move-result v1

    if-le v1, v8, :cond_0

    invoke-virtual {v0, v8}, Landroid/text/StaticLayout;->getLineMax(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->setLineWidth2$app_hudRelease(I)V

    .line 171
    :cond_0
    return-void
.end method

.method private final getBoldText(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 5
    .param p1, "instruction"    # Ljava/lang/String;

    .prologue
    .line 180
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 181
    .local v0, "spannableStringBuilder":Landroid/text/SpannableStringBuilder;
    check-cast p1, Ljava/lang/CharSequence;

    .end local p1    # "instruction":Ljava/lang/String;
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 182
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 183
    return-object v0
.end method

.method private final setInstruction(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .param p2, "adjustWidthForNextManeuver"    # Z
    .param p3, "nextManeuverVisible"    # Z

    .prologue
    .line 115
    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastTurnText:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastRoadText:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    .line 116
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    .line 135
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastTurnText:Ljava/lang/String;

    .line 121
    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastRoadText:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 125
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastTurnText:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastTurnText:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->canShowTurnText(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastTurnText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastRoadText:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 130
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastRoadText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "str":Ljava/lang/String;
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstruction:Ljava/lang/String;

    .line 134
    const-string v1, "str"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setWidth(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private final setViewWidth(I)V
    .locals 3
    .param p1, "width"    # I

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 175
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 176
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->requestLayout()V

    .line 177
    return-void
.end method

.method private final setWidth(Ljava/lang/String;Z)V
    .locals 13
    .param p1, "instruction"    # Ljava/lang/String;
    .param p2, "nextManeuverVisible"    # Z

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 140
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    .line 141
    const/4 v2, 0x0

    .line 142
    .local v2, "maxWidth":I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    if-nez v0, :cond_1

    .line 147
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize26()F
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSize26$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F

    move-result v1

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    move-object v0, p0

    move-object v3, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->tryTextSize(FILjava/lang/String;Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;ZZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    :cond_0
    :goto_1
    return-void

    .line 142
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 144
    :pswitch_0
    if-eqz p2, :cond_2

    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getMediumWidth()I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v2

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getFullWidth()I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getFullWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v2

    goto :goto_0

    .line 146
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSmallWidth()I
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSmallWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v2

    goto :goto_0

    .line 150
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize24()F
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSize24$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F

    move-result v7

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    move-object v6, p0

    move v8, v2

    move-object v9, p1

    move v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->tryTextSize(FILjava/lang/String;Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize22()F
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSize22$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F

    move-result v7

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    move-object v6, p0

    move v8, v2

    move-object v9, p1

    move v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->tryTextSize(FILjava/lang/String;Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize20()F
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSize20$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F

    move-result v1

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->widthInfo:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;

    move-object v0, p0

    move-object v3, p1

    move v5, v11

    move v6, v11

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->tryTextSize(FILjava/lang/String;Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSize18()F
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSize18$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setTextSize(F)V

    .line 155
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getBoldText(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setViewWidth(I)V

    .line 157
    iput v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    goto :goto_1

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final tryTextSize(FILjava/lang/String;Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;ZZ)Z
    .locals 5
    .param p1, "fontSize"    # F
    .param p2, "maxWidth"    # I
    .param p3, "instruction"    # Ljava/lang/String;
    .param p4, "widthInfo"    # Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;
    .param p5, "check2Lines"    # Z
    .param p6, "applyBold"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 192
    invoke-direct {p0, p3, p1, p4, p2}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->calculateTextWidth(Ljava/lang/String;FLcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;I)V

    .line 193
    invoke-virtual {p4}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->getNumLines$app_hudRelease()I

    move-result v3

    if-ne v3, v2, :cond_3

    .line 194
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setTextSize(F)V

    .line 195
    if-nez p6, :cond_2

    check-cast p3, Ljava/lang/CharSequence;

    .end local p3    # "instruction":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    invoke-virtual {p4}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->getLineWidth1$app_hudRelease()I

    move-result v0

    .line 197
    .local v0, "newWidth":I
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSingleLineMinWidth()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSingleLineMinWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 198
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getSingleLineMinWidth()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getSingleLineMinWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v0

    .line 200
    :cond_0
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setViewWidth(I)V

    .line 201
    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    move v1, v2

    .line 212
    .end local v0    # "newWidth":I
    :cond_1
    :goto_1
    return v1

    .line 195
    .restart local p3    # "instruction":Ljava/lang/String;
    :cond_2
    invoke-direct {p0, p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getBoldText(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object p3, v1

    goto :goto_0

    .line 204
    :cond_3
    if-eqz p5, :cond_1

    .line 205
    invoke-virtual {p4}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->getNumLines$app_hudRelease()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 206
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setTextSize(F)V

    .line 207
    if-nez p6, :cond_4

    check-cast p3, Ljava/lang/CharSequence;

    .end local p3    # "instruction":Ljava/lang/String;
    :goto_2
    invoke-virtual {p0, p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setViewWidth(I)V

    .line 209
    iput p2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    move v1, v2

    .line 210
    goto :goto_1

    .line 207
    .restart local p3    # "instruction":Ljava/lang/String;
    :cond_4
    invoke-direct {p0, p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getBoldText(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object p3, v1

    goto :goto_2
.end method

.method public static bridge synthetic updateDisplay$default(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    and-int/lit8 v1, p4, 0x2

    if-eqz v1, :cond_0

    move p2, v0

    .line 106
    :cond_0
    and-int/lit8 v1, p4, 0x4

    if-eqz v1, :cond_1

    move p3, v0

    .line 107
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V

    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 98
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastTurnText:Ljava/lang/String;

    move-object v0, v1

    .line 99
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastRoadText:Ljava/lang/String;

    .line 100
    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstruction:Ljava/lang/String;

    .line 101
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->instructionWidthIncludesNext:Z

    .line 103
    return-void
.end method

.method public final getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public final setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 5
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v3, "mode"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    :goto_0
    return-void

    .line 69
    :cond_0
    iget v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 70
    sget-object v3, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 71
    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getFullWidth()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getFullWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setViewWidth(I)V

    .line 90
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    goto :goto_0

    .line 73
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getMediumWidth()I
    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setViewWidth(I)V

    goto :goto_1

    .line 76
    :cond_3
    const/4 v1, 0x0

    .line 77
    .local v1, "setWidth":Z
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v3, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v3, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 78
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget-object v3, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 85
    :cond_5
    :goto_2
    if-eqz v1, :cond_1

    .line 87
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstruction:Ljava/lang/String;

    .line 88
    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_1

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setWidth(Ljava/lang/String;Z)V

    goto :goto_1

    .line 80
    .end local v2    # "str":Ljava/lang/String;
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    sget-object v4, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    sget-object v4, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getMediumWidth()I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v4

    if-ne v3, v4, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    .line 83
    :pswitch_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    sget-object v4, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastInstructionWidth:I

    sget-object v4, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->getMediumWidth()I
    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;->access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;)I

    move-result v4

    if-le v3, v4, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "adjustWidthForNextManeuver"    # Z
    .param p3, "nextManeuverVisible"    # Z

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setInstruction(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V

    .line 109
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 110
    return-void
.end method
