.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;
.super Landroid/support/constraint/ConstraintLayout;
.source "TbtDistanceView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 ,2\u00020\u0001:\u0001,B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010\u001c\u001a\u00020\u001bJ\u001a\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u000c2\n\u0010\u001f\u001a\u00060 R\u00020!J\u0008\u0010\"\u001a\u00020\u001bH\u0014J$\u0010#\u001a\u00020\u001b2\u0008\u0010$\u001a\u0004\u0018\u00010\u00132\u0008\u0010%\u001a\u0004\u0018\u00010\u00112\u0006\u0010&\u001a\u00020\'H\u0002J\u000e\u0010(\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u000cJ\u000e\u0010)\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020+R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;",
        "Landroid/support/constraint/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "currentMode",
        "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;",
        "distanceView",
        "Landroid/widget/TextView;",
        "initialProgressBarDistance",
        "lastDistance",
        "",
        "lastManeuverState",
        "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;",
        "nowView",
        "Landroid/widget/ImageView;",
        "progressAnimator",
        "Landroid/animation/ValueAnimator;",
        "progressView",
        "Landroid/widget/ProgressBar;",
        "cancelProgressAnimator",
        "",
        "clear",
        "getCustomAnimator",
        "mode",
        "mainBuilder",
        "Landroid/animation/AnimatorSet$Builder;",
        "Landroid/animation/AnimatorSet;",
        "onFinishInflate",
        "setDistance",
        "maneuverState",
        "text",
        "distanceInMeters",
        "",
        "setMode",
        "updateDisplay",
        "event",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

.field private static final fullWidth:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final mediumWidth:I

.field private static final nowHeightFull:I

.field private static final nowHeightMedium:I

.field private static final nowWidthFull:I

.field private static final nowWidthMedium:I

.field private static final progressHeightFull:I

.field private static final progressHeightMedium:I

.field private static final resources:Landroid/content/res/Resources;

.field private static final size18:F

.field private static final size22:F


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field private distanceView:Landroid/widget/TextView;

.field private initialProgressBarDistance:I

.field private lastDistance:Ljava/lang/String;

.field private lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field private nowView:Landroid/widget/ImageView;

.field private progressAnimator:Landroid/animation/ValueAnimator;

.field private progressView:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TbtDistanceView"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 31
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "HudApplication.getAppContext().resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->resources:Landroid/content/res/Resources;

    .line 32
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0172

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->fullWidth:I

    .line 33
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0173

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->mediumWidth:I

    .line 34
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->size22:F

    .line 35
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->size18:F

    .line 36
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressHeightFull:I

    .line 37
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0179

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressHeightMedium:I

    .line 38
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowWidthFull:I

    .line 39
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0174

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowHeightFull:I

    .line 40
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0177

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowWidthMedium:I

    .line 41
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0175

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowHeightMedium:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0, p1}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "defStyleAttr"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/support/constraint/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getFullWidth$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->fullWidth:I

    return v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getMediumWidth$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->mediumWidth:I

    return v0
.end method

.method public static final synthetic access$getNowHeightFull$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowHeightFull:I

    return v0
.end method

.method public static final synthetic access$getNowHeightMedium$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowHeightMedium:I

    return v0
.end method

.method public static final synthetic access$getNowWidthFull$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowWidthFull:I

    return v0
.end method

.method public static final synthetic access$getNowWidthMedium$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowWidthMedium:I

    return v0
.end method

.method public static final synthetic access$getProgressHeightFull$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressHeightFull:I

    return v0
.end method

.method public static final synthetic access$getProgressHeightMedium$cp()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressHeightMedium:I

    return v0
.end method

.method public static final synthetic access$getResources$cp()Landroid/content/res/Resources;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->resources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static final synthetic access$getSize18$cp()F
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->size18:F

    return v0
.end method

.method public static final synthetic access$getSize22$cp()F
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->size22:F

    return v0
.end method

.method private final cancelProgressAnimator()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_2

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 191
    :cond_1
    const/4 v0, 0x0

    check-cast v0, Landroid/animation/ValueAnimator;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressAnimator:Landroid/animation/ValueAnimator;

    .line 193
    :cond_2
    return-void

    .line 188
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final setDistance(Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;Ljava/lang/String;J)V
    .locals 19
    .param p1, "maneuverState"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "distanceInMeters"    # J

    .prologue
    .line 125
    move-object/from16 v11, p2

    check-cast v11, Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastDistance:Ljava/lang/String;

    check-cast v12, Ljava/lang/CharSequence;

    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 185
    .end local p2    # "text":Ljava/lang/String;
    :goto_0
    return-void

    .line 126
    .restart local p2    # "text":Ljava/lang/String;
    :cond_0
    if-nez p1, :cond_7

    .line 127
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->cancelProgressAnimator()V

    .line 128
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v11, :cond_1

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 129
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    if-eqz v12, :cond_2

    const-string v11, ""

    check-cast v11, Ljava/lang/CharSequence;

    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_3

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 131
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_4

    const v12, 0x3f666666    # 0.9f

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 132
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_5

    const v12, 0x3f666666    # 0.9f

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 133
    :cond_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_6

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 134
    :cond_6
    const/16 v11, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->setVisibility(I)V

    goto :goto_0

    .line 137
    :cond_7
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastDistance:Ljava/lang/String;

    .line 138
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    if-eqz v12, :cond_8

    if-eqz p2, :cond_10

    check-cast p2, Ljava/lang/CharSequence;

    .end local p2    # "text":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_11

    sget-object v11, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    const/4 v7, 0x1

    .line 142
    .local v7, "showNowIcon":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    sget-object v11, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_12

    const/4 v4, 0x1

    .line 143
    .local v4, "hideNowIcon":Z
    :goto_3
    if-eqz v7, :cond_13

    .line 145
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_9

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 146
    :cond_9
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_a

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    :cond_a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    check-cast v11, Landroid/view/View;

    invoke-static {v11}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getFadeInAndScaleUpAnimator(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v3

    .line 148
    .local v3, "fadeIn":Landroid/animation/AnimatorSet;
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->cancelProgressAnimator()V

    .line 149
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v11, :cond_b

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 150
    :cond_b
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    if-eqz v11, :cond_c

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setAlpha(F)V

    .line 151
    :cond_c
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 152
    .local v2, "distanceAnimator":Landroid/animation/AnimatorSet;
    check-cast v3, Landroid/animation/Animator;

    .end local v3    # "fadeIn":Landroid/animation/AnimatorSet;
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 153
    sget-object v11, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v11}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getMANEUVER_PROGRESS_ANIMATION_DURATION()J

    move-result-wide v12

    invoke-virtual {v2, v12, v13}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v11

    invoke-virtual {v11}, Landroid/animation/AnimatorSet;->start()V

    .line 164
    .end local v2    # "distanceAnimator":Landroid/animation/AnimatorSet;
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_18

    sget-object v11, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_18

    const/4 v10, 0x1

    .line 168
    .local v10, "showProgress":Z
    :goto_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_19

    sget-object v11, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_19

    const/4 v5, 0x1

    .line 169
    .local v5, "hideProgress":Z
    :goto_6
    if-eqz v5, :cond_1a

    .line 170
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v11, :cond_e

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 171
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->cancelProgressAnimator()V

    .line 172
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->initialProgressBarDistance:I

    .line 183
    :cond_f
    :goto_7
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->setVisibility(I)V

    goto/16 :goto_0

    .line 138
    .end local v4    # "hideNowIcon":Z
    .end local v5    # "hideProgress":Z
    .end local v7    # "showNowIcon":Z
    .end local v10    # "showProgress":Z
    .restart local p2    # "text":Ljava/lang/String;
    :cond_10
    const-string v11, ""

    check-cast v11, Ljava/lang/CharSequence;

    move-object/from16 p2, v11

    goto/16 :goto_1

    .line 141
    .end local p2    # "text":Ljava/lang/String;
    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 142
    .restart local v7    # "showNowIcon":Z
    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 154
    .restart local v4    # "hideNowIcon":Z
    :cond_13
    if-eqz v4, :cond_16

    .line 156
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_14

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 157
    :cond_14
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_15

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 158
    :cond_15
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    check-cast v11, Landroid/view/View;

    const/4 v12, 0x1

    invoke-static {v11, v12}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getAlphaAnimator(Landroid/view/View;I)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 159
    .local v3, "fadeIn":Landroid/animation/ObjectAnimator;
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 160
    .restart local v2    # "distanceAnimator":Landroid/animation/AnimatorSet;
    check-cast v3, Landroid/animation/Animator;

    .end local v3    # "fadeIn":Landroid/animation/ObjectAnimator;
    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 161
    sget-object v11, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v11}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getMANEUVER_PROGRESS_ANIMATION_DURATION()J

    move-result-wide v12

    invoke-virtual {v2, v12, v13}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v11

    if-eqz v11, :cond_d

    invoke-virtual {v11}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_4

    .line 163
    .end local v2    # "distanceAnimator":Landroid/animation/AnimatorSet;
    :cond_16
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v11, :cond_17

    invoke-virtual {v11}, Landroid/widget/ImageView;->getVisibility()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    :goto_8
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_d

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    if-eqz v11, :cond_d

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_4

    :cond_17
    const/4 v11, 0x0

    goto :goto_8

    .line 164
    :cond_18
    const/4 v10, 0x0

    goto/16 :goto_5

    .line 168
    .restart local v10    # "showProgress":Z
    :cond_19
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 173
    .restart local v5    # "hideProgress":Z
    :cond_1a
    if-eqz v10, :cond_1d

    .line 174
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->cancelProgressAnimator()V

    .line 175
    move-wide/from16 v0, p3

    long-to-int v11, v0

    move-object/from16 v0, p0

    iput v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->initialProgressBarDistance:I

    .line 176
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v11, :cond_1b

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 177
    :cond_1b
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v11, :cond_1c

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 178
    :cond_1c
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v11, :cond_f

    invoke-virtual {v11}, Landroid/widget/ProgressBar;->requestLayout()V

    goto/16 :goto_7

    .line 179
    :cond_1d
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 180
    const/4 v11, 0x1

    int-to-double v12, v11

    move-wide/from16 v0, p3

    long-to-double v14, v0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->initialProgressBarDistance:I

    int-to-double v0, v11

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    sub-double v8, v12, v14

    .line 181
    .local v8, "progressFactor":D
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    const/16 v12, 0x64

    int-to-double v12, v12

    mul-double/2addr v12, v8

    double-to-int v12, v12

    invoke-static {v11, v12}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getProgressBarAnimator(Landroid/widget/ProgressBar;I)Landroid/animation/ValueAnimator;

    move-result-object v6

    .line 182
    .local v6, "progressAnimator":Landroid/animation/ValueAnimator;
    sget-object v11, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v11}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getMANEUVER_PROGRESS_ANIMATION_DURATION()J

    move-result-wide v12

    invoke-virtual {v6, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v11

    invoke-virtual {v11}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_7
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 111
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastDistance:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->cancelProgressAnimator()V

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 115
    :cond_1
    return-void
.end method

.method public final getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0}, Landroid/support/constraint/ConstraintLayout;->onFinishInflate()V

    .line 62
    const v0, 0x7f0e021a

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0e017d

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.ProgressBar"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    .line 64
    const v0, 0x7f0e021b

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.ImageView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    .line 65
    return-void
.end method

.method public final setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 9
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const-string v7, "mode"

    invoke-static {p1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v7, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 105
    :goto_0
    return-void

    .line 69
    :cond_0
    const/4 v2, 0x0

    .line 70
    .local v2, "lytWidth":I
    const/4 v0, 0x0

    .line 71
    .local v0, "fontSize":F
    const/4 v5, 0x0

    .line 72
    .local v5, "progressSize":I
    const/4 v4, 0x0

    .line 73
    .local v4, "nowW":I
    const/4 v3, 0x0

    .line 75
    .local v3, "nowH":I
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 91
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v6, Lkotlin/TypeCastException;

    const-string v7, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v6, v7}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 77
    :pswitch_0
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getFullWidth()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getFullWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v2

    .line 78
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getSize22()F
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getSize22$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)F

    move-result v0

    .line 79
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getProgressHeightFull()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getProgressHeightFull$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v5

    .line 80
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowWidthFull()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getNowWidthFull$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v4

    .line 81
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowHeightFull()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getNowHeightFull$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v3

    goto :goto_1

    .line 85
    :pswitch_1
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getMediumWidth()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v2

    .line 86
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getSize18()F
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getSize18$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)F

    move-result v0

    .line 87
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getProgressHeightMedium()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getProgressHeightMedium$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v5

    .line 88
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowWidthMedium()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getNowWidthMedium$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v4

    .line 89
    sget-object v7, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowHeightMedium()I
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getNowHeightMedium$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I

    move-result v3

    goto :goto_1

    .line 91
    :cond_1
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 94
    .local v1, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 95
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->distanceView:Landroid/widget/TextView;

    if-eqz v7, :cond_2

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 96
    :cond_2
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressView:Landroid/widget/ProgressBar;

    if-eqz v7, :cond_3

    invoke-virtual {v7}, Landroid/widget/ProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :goto_2
    if-nez v1, :cond_4

    new-instance v6, Lkotlin/TypeCastException;

    const-string v7, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v6, v7}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v6

    .restart local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_3
    move-object v1, v6

    goto :goto_2

    .end local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_4
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 97
    .restart local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 98
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowView:Landroid/widget/ImageView;

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :goto_3
    if-nez v1, :cond_6

    new-instance v6, Lkotlin/TypeCastException;

    const-string v7, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v6, v7}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v6

    .restart local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_5
    move-object v1, v6

    goto :goto_3

    .end local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_6
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 99
    .restart local v1    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 100
    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 101
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->invalidate()V

    .line 102
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->requestLayout()V

    .line 103
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 104
    sget-object v6, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->access$getLogger$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "view width = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    iget-wide v2, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceInMeters:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->setDistance(Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;Ljava/lang/String;J)V

    .line 119
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 120
    return-void
.end method
