.class final Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;
.super Ljava/lang/Object;
.source "TbtTextInstructionView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WidthInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000b\n\u0002\u0010\u0002\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000f\u001a\u00020\u0010R\u001a\u0010\u0003\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u0006\"\u0004\u0008\u000b\u0010\u0008R\u001a\u0010\u000c\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u0006\"\u0004\u0008\u000e\u0010\u0008\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;",
        "",
        "()V",
        "lineWidth1",
        "",
        "getLineWidth1$app_hudRelease",
        "()I",
        "setLineWidth1$app_hudRelease",
        "(I)V",
        "lineWidth2",
        "getLineWidth2$app_hudRelease",
        "setLineWidth2$app_hudRelease",
        "numLines",
        "getNumLines$app_hudRelease",
        "setNumLines$app_hudRelease",
        "clear",
        "",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private lineWidth1:I

.field private lineWidth2:I

.field private numLines:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->numLines:I

    .line 46
    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->lineWidth1:I

    .line 47
    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->lineWidth2:I

    .line 48
    return-void
.end method

.method public final getLineWidth1$app_hudRelease()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->lineWidth1:I

    return v0
.end method

.method public final getLineWidth2$app_hudRelease()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->lineWidth2:I

    return v0
.end method

.method public final getNumLines$app_hudRelease()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->numLines:I

    return v0
.end method

.method public final setLineWidth1$app_hudRelease(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->lineWidth1:I

    return-void
.end method

.method public final setLineWidth2$app_hudRelease(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->lineWidth2:I

    return-void
.end method

.method public final setNumLines$app_hudRelease(I)V
    .locals 0
    .param p1, "<set-?>"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;->numLines:I

    return-void
.end method
