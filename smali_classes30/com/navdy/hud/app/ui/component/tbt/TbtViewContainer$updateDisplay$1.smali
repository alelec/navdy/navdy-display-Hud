.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "TbtViewContainer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$1",
        "Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;",
        "(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;)V",
        "onAnimationEnd",
        "",
        "animation",
        "Landroid/animation/Animator;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;)V
    .locals 0
    .param p1, "$outer"    # Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 128
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$1;->this$0:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "animation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$updateDisplay$1;->this$0:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;

    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->inactiveView:Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getInactiveView$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;)Lcom/navdy/hud/app/ui/component/tbt/TbtView;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getItemHeight()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setTranslationY(F)V

    .line 131
    :cond_0
    return-void
.end method
