.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;
.super Ljava/lang/Object;
.source "TbtViewContainer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0007\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020 \u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010#\u001a\u00020$\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;",
        "",
        "()V",
        "MANEUVER_PROGRESS_ANIMATION_DURATION",
        "",
        "getMANEUVER_PROGRESS_ANIMATION_DURATION",
        "()J",
        "MANEUVER_SWAP_DELAY",
        "getMANEUVER_SWAP_DELAY",
        "MANEUVER_SWAP_DURATION",
        "getMANEUVER_SWAP_DURATION",
        "bus",
        "Lcom/squareup/otto/Bus;",
        "getBus",
        "()Lcom/squareup/otto/Bus;",
        "itemHeight",
        "",
        "getItemHeight",
        "()F",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "getLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "maneuverSwapInterpolator",
        "Landroid/view/animation/AccelerateDecelerateInterpolator;",
        "getManeuverSwapInterpolator",
        "()Landroid/view/animation/AccelerateDecelerateInterpolator;",
        "remoteDeviceManager",
        "Lcom/navdy/hud/app/manager/RemoteDeviceManager;",
        "getRemoteDeviceManager",
        "()Lcom/navdy/hud/app/manager/RemoteDeviceManager;",
        "resources",
        "Landroid/content/res/Resources;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "uiStateManager",
        "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
        "getUiStateManager",
        "()Lcom/navdy/hud/app/ui/framework/UIStateManager;",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getLogger$p(Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 30
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getManeuverSwapInterpolator()Landroid/view/animation/AccelerateDecelerateInterpolator;
    .locals 1

    .prologue
    .line 36
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->maneuverSwapInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getManeuverSwapInterpolator$cp()Landroid/view/animation/AccelerateDecelerateInterpolator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getBus()Lcom/squareup/otto/Bus;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 38
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->bus:Lcom/squareup/otto/Bus;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getBus$cp()Lcom/squareup/otto/Bus;

    move-result-object v0

    return-object v0
.end method

.method public final getItemHeight()F
    .locals 1

    .prologue
    .line 32
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->itemHeight:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getItemHeight$cp()F

    move-result v0

    return v0
.end method

.method public final getMANEUVER_PROGRESS_ANIMATION_DURATION()J
    .locals 2

    .prologue
    .line 33
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_PROGRESS_ANIMATION_DURATION:J
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getMANEUVER_PROGRESS_ANIMATION_DURATION$cp()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getMANEUVER_SWAP_DELAY()J
    .locals 2

    .prologue
    .line 35
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_SWAP_DELAY:J
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getMANEUVER_SWAP_DELAY$cp()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getMANEUVER_SWAP_DURATION()J
    .locals 2

    .prologue
    .line 34
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->MANEUVER_SWAP_DURATION:J
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getMANEUVER_SWAP_DURATION$cp()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getRemoteDeviceManager()Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 37
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getRemoteDeviceManager$cp()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    return-object v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 31
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->resources:Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getResources$cp()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 39
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;->access$getUiStateManager$cp()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    return-object v0
.end method
