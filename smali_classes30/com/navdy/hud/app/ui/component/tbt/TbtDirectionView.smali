.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;
.super Landroid/widget/ImageView;
.source "TbtDirectionView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0010\u001a\u00020\u0011J\u001a\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u000c2\n\u0010\u0014\u001a\u00060\u0015R\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u000cJ\u000e\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001aR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;",
        "Landroid/widget/ImageView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "currentMode",
        "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;",
        "lastManeuverState",
        "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;",
        "lastTurnIconId",
        "clear",
        "",
        "getCustomAnimator",
        "mode",
        "mainBuilder",
        "Landroid/animation/AnimatorSet$Builder;",
        "Landroid/animation/AnimatorSet;",
        "setMode",
        "updateDisplay",
        "event",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

.field private static final fullWidth:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final mediumWidth:I

.field private static final resources:Landroid/content/res/Resources;


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field private lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field private lastTurnIconId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TbtDirectionView"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 22
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "HudApplication.getAppContext().resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->resources:Landroid/content/res/Resources;

    .line 23
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0170

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->fullWidth:I

    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0171

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->mediumWidth:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->lastTurnIconId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->lastTurnIconId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "defStyleAttr"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->lastTurnIconId:I

    return-void
.end method

.method public static final synthetic access$getFullWidth$cp()I
    .locals 1

    .prologue
    .line 19
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->fullWidth:I

    return v0
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getMediumWidth$cp()I
    .locals 1

    .prologue
    .line 19
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->mediumWidth:I

    return v0
.end method

.method public static final synthetic access$getResources$cp()Landroid/content/res/Resources;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->resources:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->lastTurnIconId:I

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    return-void
.end method

.method public final getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public final setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 3
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v1, "mode"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 39
    .local v0, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 40
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->getFullWidth()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->access$getFullWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 41
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->getFullWidth()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->access$getFullWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 45
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->requestLayout()V

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    goto :goto_0

    .line 43
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->getMediumWidth()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 44
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->getMediumWidth()I
    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;->access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView$Companion;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto :goto_1
.end method

.method public final updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 60
    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->setImageResource(I)V

    .line 63
    :goto_0
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->lastManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 65
    iget v0, p1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->lastTurnIconId:I

    .line 66
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
