.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;
.super Ljava/lang/Object;
.source "TbtDistanceView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0007\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0006R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0006R\u0014\u0010\u0015\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0006R\u0014\u0010\u0017\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0006R\u0014\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0014\u0010!\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010 \u00a8\u0006#"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;",
        "",
        "()V",
        "fullWidth",
        "",
        "getFullWidth",
        "()I",
        "logger",
        "Lcom/navdy/service/library/log/Logger;",
        "getLogger",
        "()Lcom/navdy/service/library/log/Logger;",
        "mediumWidth",
        "getMediumWidth",
        "nowHeightFull",
        "getNowHeightFull",
        "nowHeightMedium",
        "getNowHeightMedium",
        "nowWidthFull",
        "getNowWidthFull",
        "nowWidthMedium",
        "getNowWidthMedium",
        "progressHeightFull",
        "getProgressHeightFull",
        "progressHeightMedium",
        "getProgressHeightMedium",
        "resources",
        "Landroid/content/res/Resources;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "size18",
        "",
        "getSize18",
        "()F",
        "size22",
        "getSize22",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0
    .param p1, "$constructor_marker"    # Lkotlin/jvm/internal/DefaultConstructorMarker;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getFullWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getFullWidth()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getLogger$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getLogger()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getMediumWidth$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getMediumWidth()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getNowHeightFull$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowHeightFull()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getNowHeightMedium$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowHeightMedium()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getNowWidthFull$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowWidthFull()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getNowWidthMedium$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getNowWidthMedium()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getProgressHeightFull$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getProgressHeightFull()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getProgressHeightMedium$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)I
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getProgressHeightMedium()I

    move-result v0

    return v0
.end method

.method public static final synthetic access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$getSize18$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)F
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getSize18()F

    move-result v0

    return v0
.end method

.method public static final synthetic access$getSize22$p(Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;)F
    .locals 1
    .param p0, "$this"    # Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;->getSize22()F

    move-result v0

    return v0
.end method

.method private final getFullWidth()I
    .locals 1

    .prologue
    .line 32
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->fullWidth:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getFullWidth$cp()I

    move-result v0

    return v0
.end method

.method private final getLogger()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 30
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getLogger$cp()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    return-object v0
.end method

.method private final getMediumWidth()I
    .locals 1

    .prologue
    .line 33
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->mediumWidth:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getMediumWidth$cp()I

    move-result v0

    return v0
.end method

.method private final getNowHeightFull()I
    .locals 1

    .prologue
    .line 39
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowHeightFull:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getNowHeightFull$cp()I

    move-result v0

    return v0
.end method

.method private final getNowHeightMedium()I
    .locals 1

    .prologue
    .line 41
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowHeightMedium:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getNowHeightMedium$cp()I

    move-result v0

    return v0
.end method

.method private final getNowWidthFull()I
    .locals 1

    .prologue
    .line 38
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowWidthFull:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getNowWidthFull$cp()I

    move-result v0

    return v0
.end method

.method private final getNowWidthMedium()I
    .locals 1

    .prologue
    .line 40
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->nowWidthMedium:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getNowWidthMedium$cp()I

    move-result v0

    return v0
.end method

.method private final getProgressHeightFull()I
    .locals 1

    .prologue
    .line 36
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressHeightFull:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getProgressHeightFull$cp()I

    move-result v0

    return v0
.end method

.method private final getProgressHeightMedium()I
    .locals 1

    .prologue
    .line 37
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->progressHeightMedium:I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getProgressHeightMedium$cp()I

    move-result v0

    return v0
.end method

.method private final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 31
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->resources:Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getResources$cp()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private final getSize18()F
    .locals 1

    .prologue
    .line 35
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->size18:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getSize18$cp()F

    move-result v0

    return v0
.end method

.method private final getSize22()F
    .locals 1

    .prologue
    .line 34
    # getter for: Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->size22:F
    invoke-static {}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->access$getSize22$cp()F

    move-result v0

    return v0
.end method
