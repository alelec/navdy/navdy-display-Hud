.class public final Lcom/navdy/hud/app/ui/component/tbt/TbtView;
.super Landroid/widget/FrameLayout;
.source "TbtView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 %2\u00020\u0001:\u0001%B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0017\u001a\u00020\u0018J\u001a\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u000c2\n\u0010\u001b\u001a\u00060\u001cR\u00020\u001dJ\u0008\u0010\u001e\u001a\u00020\u0018H\u0014J\u0012\u0010\u001f\u001a\u00020\u00182\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u000cH\u0002J\u000e\u0010 \u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u000cJ\u0010\u0010!\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u000cH\u0002J\u000e\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020$R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/navdy/hud/app/ui/component/tbt/TbtView;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "currentMode",
        "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;",
        "directionView",
        "Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;",
        "distanceView",
        "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;",
        "groupContainer",
        "Landroid/support/constraint/ConstraintLayout;",
        "tbtNextManeuverView",
        "Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;",
        "textInstructionView",
        "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;",
        "clear",
        "",
        "getCustomAnimator",
        "mode",
        "mainBuilder",
        "Landroid/animation/AnimatorSet$Builder;",
        "Landroid/animation/AnimatorSet;",
        "onFinishInflate",
        "setConstraints",
        "setView",
        "setViewAttrs",
        "updateDisplay",
        "event",
        "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;",
        "Companion",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# static fields
.field public static final Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final paddingFull:I

.field private static final paddingMedium:I

.field private static final resources:Landroid/content/res/Resources;

.field private static final shrinkLeftX:F


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

.field private directionView:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

.field private distanceView:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;

.field private groupContainer:Landroid/support/constraint/ConstraintLayout;

.field private tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

.field private textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    new-instance v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TbtView"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 27
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "HudApplication.getAppContext().resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->resources:Landroid/content/res/Resources;

    .line 28
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0183

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->paddingFull:I

    .line 29
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0184

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->paddingMedium:I

    .line 30
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    # invokes: Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getResources()Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->access$getResources$p(Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0185

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->shrinkLeftX:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3, "defStyleAttr"    # I

    .prologue
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getLogger$cp()Lcom/navdy/service/library/log/Logger;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static final synthetic access$getPaddingFull$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->paddingFull:I

    return v0
.end method

.method public static final synthetic access$getPaddingMedium$cp()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->paddingMedium:I

    return v0
.end method

.method public static final synthetic access$getResources$cp()Landroid/content/res/Resources;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->resources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static final synthetic access$getShrinkLeftX$cp()F
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->shrinkLeftX:F

    return v0
.end method

.method private final setConstraints(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 10
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 109
    if-nez p1, :cond_0

    .line 133
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->getVisibility()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v1, v9}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    .line 111
    .local v6, "addNextManeuver":Z
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtView$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v9

    aget v1, v1, v9

    packed-switch v1, :pswitch_data_0

    .line 114
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getPaddingMedium()I

    move-result v8

    .line 115
    .local v8, "padding":I
    :goto_2
    sget v1, Lcom/navdy/hud/app/R$id;->tbtDirectionView:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    if-nez v7, :cond_3

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .end local v6    # "addNextManeuver":Z
    .end local v8    # "padding":I
    :cond_1
    move-object v1, v3

    .line 110
    goto :goto_1

    .line 113
    .restart local v6    # "addNextManeuver":Z
    :pswitch_0
    if-eqz v6, :cond_2

    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getPaddingMedium()I

    move-result v8

    .restart local v8    # "padding":I
    goto :goto_2

    .end local v8    # "padding":I
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getPaddingFull()I

    move-result v8

    .restart local v8    # "padding":I
    goto :goto_2

    .line 115
    :cond_3
    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 117
    .local v7, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v8, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 118
    sget v1, Lcom/navdy/hud/app/R$id;->tbtTextInstructionView:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    .end local v7    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    if-nez v7, :cond_4

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 119
    .restart local v7    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v8, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 120
    if-eqz v6, :cond_7

    .line 121
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    .end local v7    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :goto_3
    if-nez v7, :cond_6

    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .restart local v7    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_5
    move-object v7, v3

    goto :goto_3

    .end local v7    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_6
    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 122
    .restart local v7    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget-object v1, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getPaddingMedium()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iput v1, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 125
    :cond_7
    new-instance v0, Landroid/support/constraint/ConstraintSet;

    invoke-direct {v0}, Landroid/support/constraint/ConstraintSet;-><init>()V

    .line 126
    .local v0, "constraintSet":Landroid/support/constraint/ConstraintSet;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->groupContainer:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintSet;->clone(Landroid/support/constraint/ConstraintLayout;)V

    .line 127
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->directionView:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->getId()I

    move-result v1

    :goto_4
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->distanceView:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;

    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->getId()I

    move-result v3

    :goto_5
    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 128
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getId()I

    move-result v1

    :goto_6
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->directionView:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    if-eqz v3, :cond_c

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->getId()I

    move-result v3

    :goto_7
    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 129
    if-eqz v6, :cond_8

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->getId()I

    move-result v1

    :goto_8
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    if-eqz v3, :cond_e

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->getId()I

    move-result v3

    :goto_9
    invoke-virtual/range {v0 .. v5}, Landroid/support/constraint/ConstraintSet;->connect(IIIII)V

    .line 130
    :cond_8
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->groupContainer:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintSet;->applyTo(Landroid/support/constraint/ConstraintLayout;)V

    .line 131
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->invalidate()V

    .line 132
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->requestLayout()V

    goto/16 :goto_0

    :cond_9
    move v1, v5

    .line 127
    goto :goto_4

    :cond_a
    move v3, v5

    goto :goto_5

    :cond_b
    move v1, v5

    .line 128
    goto :goto_6

    :cond_c
    move v3, v5

    goto :goto_7

    :cond_d
    move v1, v5

    .line 129
    goto :goto_8

    :cond_e
    move v3, v5

    goto :goto_9

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private final setViewAttrs(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->distanceView:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->directionView:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->setMode(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 90
    :cond_3
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setConstraints(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 91
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 92
    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->distanceView:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->clear()V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->directionView:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->clear()V

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->clear()V

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->clear()V

    .line 59
    :cond_3
    return-void
.end method

.method public final getCustomAnimator(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;Landroid/animation/AnimatorSet$Builder;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2, "mainBuilder"    # Landroid/animation/AnimatorSet$Builder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setViewAttrs(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 75
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 82
    .end local p0    # "this":Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    :goto_0
    return-void

    .line 77
    .restart local p0    # "this":Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    :pswitch_0
    check-cast p0, Landroid/view/View;

    .end local p0    # "this":Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 80
    .restart local p0    # "this":Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    :pswitch_1
    check-cast p0, Landroid/view/View;

    .end local p0    # "this":Lcom/navdy/hud/app/ui/component/tbt/TbtView;
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getShrinkLeftX()F

    move-result v0

    invoke-static {p0, v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenUtils;->getXPositionAnimator(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 47
    const v0, 0x7f0e021c

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.support.constraint.ConstraintLayout"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/support/constraint/ConstraintLayout;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->groupContainer:Landroid/support/constraint/ConstraintLayout;

    .line 48
    const v0, 0x7f0e0219

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDistanceView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->distanceView:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;

    .line 49
    const v0, 0x7f0e0218

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDirectionView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->directionView:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    .line 50
    const v0, 0x7f0e0220

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    .line 51
    const v0, 0x7f0e021d

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    .line 52
    return-void
.end method

.method public final setView(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setViewAttrs(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 63
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 70
    :goto_0
    return-void

    .line 65
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setX(F)V

    goto :goto_0

    .line 68
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->Companion:Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;->getShrinkLeftX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setX(F)V

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->distanceView:Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->directionView:Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->getVisibility()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v7, v1

    .line 98
    .local v7, "visibleBefore":Z
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;)V

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->tbtNextManeuverView:Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;->getVisibility()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v6, v1

    .line 100
    .local v6, "visibleAfter":Z
    :goto_3
    if-eq v7, v6, :cond_9

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1, v1, v6}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->updateDisplay(Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->currentMode:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->setConstraints(Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;)V

    .line 105
    :cond_4
    :goto_4
    return-void

    .end local v6    # "visibleAfter":Z
    .end local v7    # "visibleBefore":Z
    :cond_5
    move-object v0, v5

    .line 97
    goto :goto_0

    :cond_6
    move v7, v2

    goto :goto_1

    .restart local v7    # "visibleBefore":Z
    :cond_7
    move-object v0, v5

    .line 99
    goto :goto_2

    :cond_8
    move v6, v2

    goto :goto_3

    .line 104
    .restart local v6    # "visibleAfter":Z
    :cond_9
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/tbt/TbtView;->textInstructionView:Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;

    if-eqz v0, :cond_4

    const/4 v4, 0x6

    move-object v1, p1

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;->updateDisplay$default(Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZILjava/lang/Object;)V

    goto :goto_4
.end method
