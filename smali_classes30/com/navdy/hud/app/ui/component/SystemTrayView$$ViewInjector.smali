.class public Lcom/navdy/hud/app/ui/component/SystemTrayView$$ViewInjector;
.super Ljava/lang/Object;
.source "SystemTrayView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/SystemTrayView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/SystemTrayView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e0211

    const-string v2, "field \'dialImageView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialImageView:Landroid/widget/ImageView;

    .line 12
    const v1, 0x7f0e0214

    const-string v2, "field \'phoneImageView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneImageView:Landroid/widget/ImageView;

    .line 14
    const v1, 0x7f0e0213

    const-string v2, "field \'phoneNetworkImageView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneNetworkImageView:Landroid/widget/ImageView;

    .line 16
    const v1, 0x7f0e0210

    const-string v2, "field \'locationImageView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/SystemTrayView;->locationImageView:Landroid/widget/ImageView;

    .line 18
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/SystemTrayView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/SystemTrayView;

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->dialImageView:Landroid/widget/ImageView;

    .line 22
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneImageView:Landroid/widget/ImageView;

    .line 23
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->phoneNetworkImageView:Landroid/widget/ImageView;

    .line 24
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/SystemTrayView;->locationImageView:Landroid/widget/ImageView;

    .line 25
    return-void
.end method
