.class public final enum Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
.super Ljava/lang/Enum;
.source "InitialsImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Style"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

.field public static final enum DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

.field public static final enum LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

.field public static final enum MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

.field public static final enum SMALL:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

.field public static final enum TINY:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    const-string v1, "TINY"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->TINY:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 40
    new-instance v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->SMALL:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 41
    new-instance v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 42
    new-instance v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 43
    new-instance v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->TINY:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->SMALL:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->$VALUES:[Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    const-class v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->$VALUES:[Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    return-object v0
.end method
