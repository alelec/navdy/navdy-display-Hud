.class Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$2;
.super Ljava/lang/Object;
.source "CrossFadeImageView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$2;->this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$2;->this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 109
    return-void
.end method
