.class public Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;
.super Landroid/widget/FrameLayout;
.source "CrossFadeImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;
    }
.end annotation


# instance fields
.field big:Landroid/view/View;

.field mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

.field small:Landroid/view/View;

.field smallAlpha:F

.field smallAlphaSet:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method public getBig()Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    return-object v0
.end method

.method public getCrossFadeAnimator()Landroid/animation/AnimatorSet;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 72
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 79
    .local v4, "set":Landroid/animation/AnimatorSet;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    sget-object v8, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    if-ne v7, v8, :cond_1

    .line 80
    const/high16 v1, 0x3f800000    # 1.0f

    .line 81
    .local v1, "bigFrom":F
    const/4 v2, 0x0

    .line 82
    .local v2, "bigTo":F
    const/4 v5, 0x0

    .line 83
    .local v5, "smallFrom":F
    iget-boolean v7, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlphaSet:Z

    if-nez v7, :cond_0

    .line 84
    const/high16 v6, 0x3f800000    # 1.0f

    .line 95
    .local v6, "smallTo":F
    :goto_0
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    sget-object v8, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v9, v12, [F

    aput v1, v9, v10

    aput v2, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 96
    .local v0, "a":Landroid/animation/ObjectAnimator;
    new-instance v7, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$1;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$1;-><init>(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;)V

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 102
    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    .line 104
    .local v3, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    sget-object v8, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v9, v12, [F

    aput v5, v9, v10

    aput v6, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 105
    new-instance v7, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$2;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$2;-><init>(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;)V

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 112
    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 114
    new-instance v7, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$3;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$3;-><init>(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;)V

    invoke-virtual {v4, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 124
    return-object v4

    .line 86
    .end local v0    # "a":Landroid/animation/ObjectAnimator;
    .end local v3    # "builder":Landroid/animation/AnimatorSet$Builder;
    .end local v6    # "smallTo":F
    :cond_0
    iget v6, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlpha:F

    .restart local v6    # "smallTo":F
    goto :goto_0

    .line 89
    .end local v1    # "bigFrom":F
    .end local v2    # "bigTo":F
    .end local v5    # "smallFrom":F
    .end local v6    # "smallTo":F
    :cond_1
    const/4 v1, 0x0

    .line 90
    .restart local v1    # "bigFrom":F
    const/high16 v2, 0x3f800000    # 1.0f

    .line 91
    .restart local v2    # "bigTo":F
    const/high16 v5, 0x3f800000    # 1.0f

    .line 92
    .restart local v5    # "smallFrom":F
    const/4 v6, 0x0

    .restart local v6    # "smallTo":F
    goto :goto_0
.end method

.method public getMode()Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    return-object v0
.end method

.method public getSmall()Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    return-object v0
.end method

.method public inject(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V
    .locals 3
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 45
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    if-nez v0, :cond_0

    .line 46
    const v0, 0x7f0e00d6

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    .line 47
    const v0, 0x7f0e00d7

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    .line 48
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .line 49
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    if-ne p1, v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlphaSet:Z

    if-nez v0, :cond_2

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 58
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 56
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method

.method public setMode(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;)V
    .locals 3
    .param p1, "newMode"    # Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    if-ne v0, p1, :cond_0

    .line 143
    :goto_0
    return-void

    .line 131
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    if-ne p1, v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 142
    :goto_1
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_0

    .line 135
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlphaSet:Z

    if-nez v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 140
    :goto_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->big:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->small:Landroid/view/View;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_2
.end method

.method public setSmallAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 150
    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlphaSet:Z

    .line 152
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlpha:F

    .line 157
    :goto_0
    return-void

    .line 154
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlphaSet:Z

    .line 155
    iput p1, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->smallAlpha:F

    goto :goto_0
.end method
