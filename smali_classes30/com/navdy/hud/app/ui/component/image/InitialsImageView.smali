.class public Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
.super Landroid/widget/ImageView;
.source "InitialsImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    }
.end annotation


# static fields
.field private static greyColor:I

.field private static largeStyleTextSize:I

.field private static mediumStyleTextSize:I

.field private static smallStyleTextSize:I

.field private static tinyStyleTextSize:I

.field private static typefaceLarge:Landroid/graphics/Typeface;

.field private static typefaceSmall:Landroid/graphics/Typeface;

.field private static valuesSet:Z

.field private static whiteColor:I


# instance fields
.field private bkColor:I

.field private initials:Ljava/lang/String;

.field private paint:Landroid/graphics/Paint;

.field private scaled:Z

.field private style:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->style:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 36
    sget v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->greyColor:I

    iput v0, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->bkColor:I

    .line 56
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->init()V

    .line 57
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 60
    sget-boolean v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->valuesSet:Z

    if-nez v1, :cond_0

    .line 61
    sput-boolean v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->valuesSet:Z

    .line 62
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0d002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->greyColor:I

    .line 64
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->whiteColor:I

    .line 65
    const v1, 0x7f0b0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->tinyStyleTextSize:I

    .line 66
    const v1, 0x7f0b0064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->smallStyleTextSize:I

    .line 67
    const v1, 0x7f0b0063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->mediumStyleTextSize:I

    .line 68
    const v1, 0x7f0b0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->largeStyleTextSize:I

    .line 69
    const-string v1, "sans-serif-medium"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->typefaceSmall:Landroid/graphics/Typeface;

    .line 70
    const-string v1, "sans-serif"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->typefaceLarge:Landroid/graphics/Typeface;

    .line 72
    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    .line 73
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 74
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 75
    return-void
.end method


# virtual methods
.method public getStyle()Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->style:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 95
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->style:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    sget-object v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    if-ne v5, v6, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->getWidth()I

    move-result v2

    .line 102
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->getHeight()I

    move-result v0

    .line 103
    .local v0, "height":I
    sget-object v5, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$1;->$SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style:[I

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->style:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 114
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->style:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 115
    .local v1, "s":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    iget-boolean v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->scaled:Z

    if-eqz v5, :cond_3

    .line 118
    sget-object v1, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->SMALL:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 122
    :cond_3
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->initials:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 123
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->whiteColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 124
    sget-object v5, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$1;->$SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style:[I

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 146
    :goto_2
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 147
    div-int/lit8 v3, v2, 0x2

    .line 148
    .local v3, "x":I
    div-int/lit8 v5, v0, 0x2

    int-to-float v5, v5

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->descent()F

    move-result v6

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Landroid/graphics/Paint;->ascent()F

    move-result v7

    add-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    float-to-int v4, v5

    .line 149
    .local v4, "y":I
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->initials:Ljava/lang/String;

    int-to-float v6, v3

    int-to-float v7, v4

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 106
    .end local v1    # "s":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    .end local v3    # "x":I
    .end local v4    # "y":I
    :pswitch_0
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 107
    iget v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->bkColor:I

    if-eqz v5, :cond_2

    .line 108
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->bkColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    div-int/lit8 v5, v2, 0x2

    int-to-float v5, v5

    div-int/lit8 v6, v0, 0x2

    int-to-float v6, v6

    div-int/lit8 v7, v2, 0x2

    int-to-float v7, v7

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 126
    .restart local v1    # "s":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :pswitch_1
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->smallStyleTextSize:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 127
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget-object v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->typefaceSmall:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_2

    .line 131
    :pswitch_2
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->mediumStyleTextSize:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 132
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget-object v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->typefaceLarge:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_2

    .line 136
    :pswitch_3
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->largeStyleTextSize:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 137
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget-object v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->typefaceLarge:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_2

    .line 141
    :pswitch_4
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->tinyStyleTextSize:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 142
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->paint:Landroid/graphics/Paint;

    sget-object v6, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->typefaceSmall:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto/16 :goto_2

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 124
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setBkColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 158
    iput p1, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->bkColor:I

    .line 159
    return-void
.end method

.method public setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V
    .locals 0
    .param p1, "resourceId"    # I
    .param p2, "initials"    # Ljava/lang/String;
    .param p3, "style"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageResource(I)V

    .line 85
    invoke-virtual {p0, p2, p3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 86
    return-void
.end method

.method public setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V
    .locals 0
    .param p1, "initials"    # Ljava/lang/String;
    .param p2, "style"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->initials:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->style:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 80
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->invalidate()V

    .line 81
    return-void
.end method

.method public setScaled(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 154
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->scaled:Z

    .line 155
    return-void
.end method
