.class public final enum Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
.super Ljava/lang/Enum;
.source "IconColorImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IconShape"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

.field public static final enum CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

.field public static final enum SQUARE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    const-string v1, "CIRCLE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 34
    new-instance v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    const-string v1, "SQUARE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->SQUARE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->SQUARE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->$VALUES:[Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const-class v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->$VALUES:[Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    return-object v0
.end method
