.class public Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
.super Landroid/widget/ImageView;
.source "IconColorImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;
    }
.end annotation


# instance fields
.field private bkColor:I

.field private bkColorGradient:Landroid/graphics/Shader;

.field private context:Landroid/content/Context;

.field private draw:Z

.field private iconResource:I

.field private iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

.field private paint:Landroid/graphics/Paint;

.field private scaleF:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->draw:Z

    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->context:Landroid/content/Context;

    .line 50
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->init()V

    .line 51
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 54
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->getWidth()I

    move-result v9

    .line 74
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->getHeight()I

    move-result v8

    .line 77
    .local v8, "height":I
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->bkColorGradient:Landroid/graphics/Shader;

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->bkColorGradient:Landroid/graphics/Shader;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    if-ne v0, v2, :cond_3

    .line 88
    :cond_0
    div-int/lit8 v0, v9, 0x2

    int-to-float v0, v0

    div-int/lit8 v2, v8, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v9, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 93
    :goto_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->draw:Z

    if-nez v0, :cond_4

    .line 95
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 110
    :cond_1
    :goto_2
    return-void

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->bkColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 90
    :cond_3
    int-to-float v3, v9

    int-to-float v4, v8

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 100
    :cond_4
    iget v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->iconResource:I

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->context:Landroid/content/Context;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->iconResource:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 102
    .local v7, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v7, :cond_1

    .line 103
    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 104
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_1

    .line 105
    iget v0, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->scaleF:F

    iget v2, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->scaleF:F

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 106
    invoke-virtual {p1, v6, v1, v1, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2
.end method

.method public setDraw(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->draw:Z

    .line 114
    return-void
.end method

.method public setIcon(IILandroid/graphics/Shader;F)V
    .locals 0
    .param p1, "iconResource"    # I
    .param p2, "bkColor"    # I
    .param p3, "gradient"    # Landroid/graphics/Shader;
    .param p4, "scaleF"    # F

    .prologue
    .line 60
    iput p1, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->iconResource:I

    .line 61
    iput p2, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->bkColor:I

    .line 62
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->bkColorGradient:Landroid/graphics/Shader;

    .line 63
    iput p4, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->scaleF:F

    .line 64
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->invalidate()V

    .line 65
    return-void
.end method

.method public setIconShape(Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V
    .locals 0
    .param p1, "iconShape"    # Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->iconShape:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .line 69
    return-void
.end method
