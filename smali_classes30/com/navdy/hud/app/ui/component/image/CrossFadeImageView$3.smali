.class Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$3;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "CrossFadeImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->getCrossFadeAnimator()Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$3;->this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$3;->this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    if-ne v0, v1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$3;->this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->SMALL:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$3;->this$0:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;

    sget-object v1, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;->BIG:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    iput-object v1, v0, Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView;->mode:Lcom/navdy/hud/app/ui/component/image/CrossFadeImageView$Mode;

    goto :goto_0
.end method
