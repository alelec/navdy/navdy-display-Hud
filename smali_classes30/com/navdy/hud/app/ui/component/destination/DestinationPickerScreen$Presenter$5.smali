.class Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;
.super Ljava/lang/Object;
.source "DestinationPickerScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->performAction(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

.field final synthetic val$id:I

.field final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 774
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iput p2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$pos:I

    iput p3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$id:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 777
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # invokes: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$900(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 778
    .local v5, "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-nez v5, :cond_0

    .line 807
    :goto_0
    return-void

    .line 781
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1000(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_1

    iget v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$pos:I

    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;
    invoke-static {v7}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1000(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v6, v7, :cond_2

    .line 782
    :cond_1
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "invalid list,no selection"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 783
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1102(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Z)Z

    goto :goto_0

    .line 785
    :cond_2
    const/4 v2, 0x0

    .line 786
    .local v2, "override":Z
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$400(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 787
    new-instance v4, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    invoke-direct {v4}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;-><init>()V

    .line 788
    .local v4, "state":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1000(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/util/List;

    move-result-object v6

    iget v7, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$pos:I

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v0, v6, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .line 789
    .local v0, "destinationParcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    if-eqz v0, :cond_4

    iget v6, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->id:I

    if-eqz v6, :cond_4

    iget v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->id:I

    .line 790
    .local v3, "selectionId":I
    :goto_1
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$400(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    move-result-object v6

    iget v7, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$pos:I

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v3, v7, v4}, Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;->onItemClicked(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z

    move-result v2

    .line 791
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1200(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 792
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "map-route- doNotAddOriginalRoute="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, v4, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;->doNotAddOriginalRoute:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 793
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iget-boolean v7, v4, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;->doNotAddOriginalRoute:Z

    iput-boolean v7, v6, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->doNotAddOriginalRoute:Z

    .line 796
    .end local v0    # "destinationParcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v3    # "selectionId":I
    .end local v4    # "state":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;
    :cond_3
    if-nez v2, :cond_5

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iget v7, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$pos:I

    # invokes: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isNotADestination(I)Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1300(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;I)Z

    move-result v6

    if-nez v6, :cond_5

    .line 797
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1000(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/util/List;

    move-result-object v6

    iget v7, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$pos:I

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 798
    .local v1, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "not overriden, launching pos:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$pos:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " title="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->title:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 799
    iget-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .line 800
    .restart local v0    # "destinationParcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # invokes: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->launchDestination(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V
    invoke-static {v6, v5, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1400(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V

    .line 805
    .end local v0    # "destinationParcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v1    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :goto_2
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    const/4 v7, 0x1

    # setter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$1102(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Z)Z

    goto/16 :goto_0

    .line 789
    .restart local v0    # "destinationParcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .restart local v4    # "state":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;
    :cond_4
    iget v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->val$id:I

    goto/16 :goto_1

    .line 802
    .end local v0    # "destinationParcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v4    # "state":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;
    :cond_5
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "overridden"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 803
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->close()V

    goto :goto_2
.end method
