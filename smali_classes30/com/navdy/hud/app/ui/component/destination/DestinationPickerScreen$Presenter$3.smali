.class Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;
.super Ljava/lang/Object;
.source "DestinationPickerScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->launchDestination(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

.field final synthetic val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 639
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 643
    new-instance v1, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    invoke-direct {v1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    .line 644
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationTitle(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    .line 645
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationSubtitle(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationAddress:Ljava/lang/String;

    .line 646
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->fullAddress(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-wide v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLatitude:D

    .line 647
    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLatitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-wide v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLongitude:D

    .line 648
    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLongitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-wide v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLatitude:D

    .line 649
    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLatitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-wide v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLongitude:D

    .line 650
    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLongitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->DEFAULT:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .line 651
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationType(Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->identifier:Ljava/lang/String;

    .line 652
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_NONE:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 653
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->favoriteDestinationType(Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->icon:I

    .line 654
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIcon(I)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    .line 655
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor(I)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationPlaceId:Ljava/lang/String;

    .line 656
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationPlaceId(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 657
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeType(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->distanceStr:Ljava/lang/String;

    .line 658
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->distanceStr(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->contacts:Ljava/util/List;

    .line 659
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->contacts(Ljava/util/List;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;->val$dest:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->phoneNumbers:Ljava/util/List;

    .line 660
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->phoneNumbers(Ljava/util/List;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v1

    .line 661
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->build()Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v0

    .line 662
    .local v0, "d":Lcom/navdy/hud/app/framework/destinations/Destination;
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "call requestNavigation"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 663
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->requestNavigationWithNavLookup(Lcom/navdy/hud/app/framework/destinations/Destination;)V

    .line 664
    return-void
.end method
