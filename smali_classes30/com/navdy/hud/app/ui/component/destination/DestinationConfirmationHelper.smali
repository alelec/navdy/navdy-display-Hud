.class public Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;
.super Ljava/lang/Object;
.source "DestinationConfirmationHelper.java"


# static fields
.field private static FONT_SIZES:[F

.field private static MAX_LINES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 88
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->MAX_LINES:[I

    .line 89
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->FONT_SIZES:[F

    return-void

    .line 88
    :array_0
    .array-data 4
        0x1
        0x1
        0x2
        0x2
    .end array-data

    .line 89
    :array_1
    .array-data 4
        0x41f00000    # 30.0f
        0x41d00000    # 26.0f
        0x41d00000    # 26.0f
        0x41b00000    # 22.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static configure(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/framework/destinations/Destination;Ljava/lang/String;Ljava/lang/CharSequence;ZZII)V
    .locals 13
    .param p0, "confirmationLayout"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .param p1, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subTitle"    # Ljava/lang/CharSequence;
    .param p4, "subTitleFormatted"    # Z
    .param p5, "hasBkColor"    # Z
    .param p6, "icon"    # I
    .param p7, "iconBkColor"    # I

    .prologue
    .line 92
    move/from16 v0, p5

    move/from16 v1, p6

    move/from16 v2, p7

    invoke-static {p0, p1, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->setImage(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/framework/destinations/Destination;ZII)V

    .line 93
    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenTitle:Landroid/widget/TextView;

    const v11, 0x7f0900b1

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    .line 94
    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    .line 96
    .local v9, "titleTextView":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    .line 98
    .local v8, "subTitleTextView":Landroid/widget/TextView;
    const v10, 0x7f0e00bf

    invoke-virtual {p0, v10}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;

    .line 99
    .local v4, "infoContainer":Lcom/navdy/hud/app/view/MaxWidthLinearLayout;
    invoke-virtual {v4}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getPaddingStart()I

    move-result v10

    invoke-virtual {v4}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getPaddingEnd()I

    move-result v11

    add-int v7, v10, v11

    .line 100
    .local v7, "padding":I
    invoke-virtual {v4}, Lcom/navdy/hud/app/view/MaxWidthLinearLayout;->getMaxWidth()I

    move-result v6

    .line 101
    .local v6, "maxWidth":I
    invoke-virtual {v9, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    const/4 v10, 0x2

    new-array v3, v10, [I

    .line 104
    .local v3, "holder":[I
    sget-object v10, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->MAX_LINES:[I

    sub-int v11, v6, v7

    sget-object v12, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->FONT_SIZES:[F

    invoke-static {v9, v10, v11, v12, v3}, Lcom/navdy/hud/app/util/ViewUtil;->autosize(Landroid/widget/TextView;[II[F[I)F

    .line 105
    const/4 v10, 0x1

    aget v5, v3, v10

    .line 106
    .local v5, "maxLines":I
    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 107
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 108
    if-eqz p4, :cond_0

    .line 109
    check-cast p3, Ljava/lang/String;

    .end local p3    # "subTitle":Ljava/lang/CharSequence;
    invoke-static/range {p3 .. p3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p3

    .line 112
    .restart local p3    # "subTitle":Ljava/lang/CharSequence;
    :cond_0
    const/4 v10, 0x2

    const v11, 0x7f0c0014

    move-object/from16 v0, p3

    invoke-static {v8, v0, v10, v11}, Lcom/navdy/hud/app/util/ViewUtil;->applyTextAndStyle(Landroid/widget/TextView;Ljava/lang/CharSequence;II)I

    .line 113
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    return-void
.end method

.method public static configure(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V
    .locals 8
    .param p0, "confirmationLayout"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .param p1, "destination"    # Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .prologue
    .line 27
    const/4 v1, 0x0

    iget-object v2, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    iget-boolean v4, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitleFormatted:Z

    iget v0, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconUnselected:I

    if-nez v0, :cond_0

    const/4 v5, 0x1

    :goto_0
    iget v6, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->icon:I

    iget v7, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->configure(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/framework/destinations/Destination;Ljava/lang/String;Ljava/lang/CharSequence;ZZII)V

    .line 35
    return-void

    .line 27
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static configure(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Lcom/navdy/hud/app/framework/destinations/Destination;)V
    .locals 8
    .param p0, "confirmationLayout"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;

    .prologue
    const/4 v4, 0x0

    .line 38
    iget-object v2, p2, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    iget-object v3, p2, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationSubtitle:Ljava/lang/String;

    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_BKCOLOR:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    if-ne v0, v1, :cond_0

    const/4 v5, 0x1

    :goto_0
    iget v6, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    iget v7, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->configure(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/framework/destinations/Destination;Ljava/lang/String;Ljava/lang/CharSequence;ZZII)V

    .line 46
    return-void

    :cond_0
    move v5, v4

    .line 38
    goto :goto_0
.end method

.method private static setImage(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/framework/destinations/Destination;ZII)V
    .locals 5
    .param p0, "confirmationLayout"    # Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .param p1, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;
    .param p2, "hasBkColor"    # Z
    .param p3, "icon"    # I
    .param p4, "iconBkColor"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 76
    if-eqz p2, :cond_0

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImageIconBkColor:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const/4 v1, 0x0

    const/high16 v2, 0x3fa00000    # 1.25f

    invoke-virtual {v0, p3, p4, v1, v2}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImageIconBkColor:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 86
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-static {p1, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->setInitials(Lcom/navdy/hud/app/framework/destinations/Destination;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;)V

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, p3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageResource(I)V

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImageIconBkColor:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static setInitials(Lcom/navdy/hud/app/framework/destinations/Destination;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;)V
    .locals 3
    .param p0, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;
    .param p1, "initialsImageView"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .prologue
    .line 49
    if-eqz p0, :cond_3

    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->initials:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 51
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->isInitialNumber:Z

    if-eqz v1, :cond_1

    .line 52
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->initials:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_0

    .line 53
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .line 64
    .local v0, "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->initials:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 68
    .end local v0    # "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :goto_1
    return-void

    .line 55
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .restart local v0    # "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    goto :goto_0

    .line 58
    .end local v0    # "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->initials:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_2

    .line 59
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .restart local v0    # "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    goto :goto_0

    .line 61
    .end local v0    # "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .restart local v0    # "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    goto :goto_0

    .line 66
    .end local v0    # "style":Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    :cond_3
    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {p1, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_1
.end method
