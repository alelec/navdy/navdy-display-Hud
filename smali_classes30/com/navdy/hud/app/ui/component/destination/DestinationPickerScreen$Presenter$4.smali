.class Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;
.super Ljava/lang/Object;
.source "DestinationPickerScreen.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showConfirmation(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

.field final synthetic val$selection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 729
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->val$selection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(II)V
    .locals 5
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 732
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # invokes: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$600(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 733
    .local v1, "view1":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-nez v1, :cond_1

    .line 734
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "not alive"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 754
    :cond_0
    :goto_0
    return-void

    .line 738
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 741
    :pswitch_0
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "called presenter select"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 742
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    # invokes: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;
    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$700(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 743
    .local v0, "v":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-eqz v0, :cond_0

    .line 744
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->val$selection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    iget v3, v3, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->val$selection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    iget v4, v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    # invokes: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->performAction(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;II)V
    invoke-static {v2, v0, v3, v4}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->access$800(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;II)V

    goto :goto_0

    .line 749
    .end local v0    # "v":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    :pswitch_1
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 750
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->clearSelection()V

    .line 751
    iget-object v2, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock()V

    goto :goto_0

    .line 738
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public itemSelected(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 757
    return-void
.end method
