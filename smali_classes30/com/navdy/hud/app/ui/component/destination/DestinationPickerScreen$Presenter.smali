.class public Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "DestinationPickerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private closed:Z

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private destinationGeoBBoxList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoBoundingBox;",
            ">;"
        }
    .end annotation
.end field

.field private destinationGeoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;"
        }
    .end annotation
.end field

.field private destinationIcon:I

.field destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

.field doNotAddOriginalRoute:Z

.field private end:Lcom/here/android/mpa/common/GeoCoordinate;

.field private handledSelection:Z

.field private hideScreenOnNavStop:Z

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field initialSelection:I

.field itemSelection:I

.field private leftBkColor:I

.field private leftIcon:I

.field private leftTitle:Ljava/lang/String;

.field private navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field private pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

.field private registered:Z

.field private showDestinationMap:Z

.field private showRouteMap:Z

.field private start:Lcom/here/android/mpa/common/GeoCoordinate;

.field private switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;
    .param p1, "x1"    # Z

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;
    .param p1, "x1"    # I

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isNotADestination(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    .param p2, "x2"    # Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->launchDestination(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 219
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->performAction(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;II)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private buildDestinationBBox()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const-wide/16 v12, 0x0

    .line 670
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 671
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 672
    .local v1, "current":Lcom/here/android/mpa/common/GeoCoordinate;
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "destinationbbox-ce:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 674
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoList:Ljava/util/List;

    .line 675
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoBBoxList:Ljava/util/List;

    .line 677
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    array-length v8, v8

    if-ge v5, v8, :cond_3

    .line 678
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v8, v8, v5

    iget-wide v8, v8, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLatitude:D

    cmpl-double v8, v8, v12

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v8, v8, v5

    iget-wide v8, v8, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLongitude:D

    cmpl-double v8, v8, v12

    if-nez v8, :cond_0

    .line 679
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoList:Ljava/util/List;

    invoke-interface {v8, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoBBoxList:Ljava/util/List;

    invoke-interface {v8, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 677
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 684
    :cond_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 685
    if-eqz v1, :cond_1

    .line 687
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    :cond_1
    new-instance v4, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v8, v8, v5

    iget-wide v8, v8, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLatitude:D

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v10, v10, v5

    iget-wide v10, v10, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLongitude:D

    invoke-direct {v4, v8, v9, v10, v11}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 691
    .local v4, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    invoke-static {v6}, Lcom/here/android/mpa/common/GeoBoundingBox;->getBoundingBoxContainingGeoCoordinates(Ljava/util/List;)Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v0

    .line 695
    .local v0, "bbox":Lcom/here/android/mpa/common/GeoBoundingBox;
    if-eqz v1, :cond_2

    .line 696
    invoke-virtual {v1, v4}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v8

    double-to-long v2, v8

    .line 697
    .local v2, "distance":J
    long-to-float v8, v2

    const v9, 0x459c4000    # 5000.0f

    cmpg-float v8, v8, v9

    if-gtz v8, :cond_2

    .line 699
    const v8, 0x453b8000    # 3000.0f

    const v9, 0x453b8000    # 3000.0f

    :try_start_0
    invoke-virtual {v0, v8, v9}, Lcom/here/android/mpa/common/GeoBoundingBox;->expand(FF)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 706
    .end local v2    # "distance":J
    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoList:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 707
    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoBBoxList:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 700
    .restart local v2    # "distance":J
    :catch_0
    move-exception v7

    .line 701
    .local v7, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "expand bbox"

    invoke-virtual {v8, v9, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 709
    .end local v0    # "bbox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v2    # "distance":J
    .end local v4    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v7    # "t":Ljava/lang/Throwable;
    :cond_3
    return-void
.end method

.method private isNotADestination(I)Z
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 572
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .line 573
    .local v0, "destination":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->type:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    sget-object v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->DESTINATION:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private launchDestination(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V
    .locals 4
    .param p1, "view"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    .param p2, "dest"    # Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .prologue
    .line 625
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "launchDestination {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "} display {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLatitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLongitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nav {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLatitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLongitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 628
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 629
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Here maps engine not initialized, exit"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 630
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$2;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->close(Ljava/lang/Runnable;)V

    .line 666
    :goto_0
    return-void

    .line 639
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;

    invoke-direct {v0, p0, p2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$3;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->close(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private performAction(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;II)V
    .locals 3
    .param p1, "view"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    .param p2, "id"    # I
    .param p3, "pos"    # I

    .prologue
    .line 764
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performAction {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 765
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 766
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    if-eqz v0, :cond_1

    .line 768
    :cond_0
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->setBackgroundColor(I)V

    .line 770
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    if-eqz v0, :cond_1

    .line 771
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapMaskVisibility(I)V

    .line 774
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    new-instance v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;

    invoke-direct {v1, p0, p3, p2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$5;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;II)V

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 809
    return-void
.end method

.method private showConfirmation(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 5
    .param p1, "view"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    .param p2, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v4, 0x0

    .line 726
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    iget v2, p2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    iget-object v0, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .line 727
    .local v0, "destination":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-static {v1, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationConfirmationHelper;->configure(Lcom/navdy/hud/app/ui/component/ConfirmationLayout;Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;)V

    .line 729
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->CONFIRMATION_CHOICES:Ljava/util/List;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$500()Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;

    invoke-direct {v3, p0, p2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$4;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    invoke-virtual {v1, v2, v4, v3}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 760
    iget-object v1, p1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->setVisibility(I)V

    .line 761
    return-void
.end method

.method private updateView()V
    .locals 30

    .prologue
    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 333
    .local v29, "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-eqz v29, :cond_4

    .line 334
    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 336
    .local v25, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    if-eqz v2, :cond_7

    .line 337
    :cond_0
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 338
    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030047

    const/4 v4, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v24

    .line 339
    .local v24, "maskView":Landroid/view/View;
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 340
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v3, 0x7f0b0074

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setLeftContainerWidth(I)V

    .line 341
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v3, 0x7f0b0075

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setRightContainerWidth(I)V

    .line 342
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 343
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 344
    const/high16 v2, -0x1000000

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->setBackgroundColor(I)V

    .line 345
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->rightBackground:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 347
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-eqz v2, :cond_6

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->routeId:Ljava/lang/String;

    move-object/from16 v26, v0

    .line 349
    .local v26, "routeId":Ljava/lang/String;
    const/4 v5, 0x0

    .line 350
    .local v5, "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    const/4 v6, 0x0

    .line 351
    .local v6, "route":Lcom/here/android/mpa/routing/Route;
    if-eqz v26, :cond_c

    .line 352
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v27

    .line 353
    .local v27, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v27, :cond_c

    .line 354
    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    .line 355
    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Route;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v5

    move-object/from16 v20, v5

    .line 358
    .end local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v27    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .local v20, "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    :goto_0
    if-nez v20, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    if-eqz v2, :cond_5

    .line 360
    :try_start_0
    new-instance v5, Lcom/here/android/mpa/common/GeoBoundingBox;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    const v3, 0x459c4000    # 5000.0f

    const v4, 0x459c4000    # 5000.0f

    invoke-direct {v5, v2, v3, v4}, Lcom/here/android/mpa/common/GeoBoundingBox;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;FF)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    .end local v20    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->end:Lcom/here/android/mpa/common/GeoCoordinate;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    invoke-virtual/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchToRouteSearchMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZLjava/util/List;I)V

    .line 376
    .end local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v6    # "route":Lcom/here/android/mpa/routing/Route;
    .end local v24    # "maskView":Landroid/view/View;
    .end local v26    # "routeId":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftTitle:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 377
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftIcon:I

    if-eqz v2, :cond_2

    .line 380
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftBkColor:I

    if-eqz v2, :cond_8

    .line 381
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftIcon:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftBkColor:I

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3, v4, v7, v8}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;F)V

    .line 386
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    array-length v2, v2

    if-nez v2, :cond_9

    .line 387
    :cond_3
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "no destinations"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 388
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 427
    .end local v25    # "resources":Landroid/content/res/Resources;
    :cond_4
    :goto_4
    return-void

    .line 361
    .restart local v6    # "route":Lcom/here/android/mpa/routing/Route;
    .restart local v20    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v24    # "maskView":Landroid/view/View;
    .restart local v25    # "resources":Landroid/content/res/Resources;
    .restart local v26    # "routeId":Ljava/lang/String;
    :catch_0
    move-exception v28

    .line 362
    .local v28, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .end local v28    # "t":Ljava/lang/Throwable;
    :cond_5
    move-object/from16 v5, v20

    .end local v20    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    goto :goto_1

    .line 367
    .end local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v6    # "route":Lcom/here/android/mpa/routing/Route;
    .end local v26    # "routeId":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v11

    .line 368
    .local v11, "currentRoute":Lcom/here/android/mpa/routing/Route;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->end:Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoBBoxList:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/here/android/mpa/common/GeoBoundingBox;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoList:Ljava/util/List;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationIcon:I

    invoke-virtual/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchToRouteSearchMode(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZLjava/util/List;I)V

    goto/16 :goto_2

    .line 372
    .end local v11    # "currentRoute":Lcom/here/android/mpa/routing/Route;
    .end local v24    # "maskView":Landroid/view/View;
    :cond_7
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v3, 0x7f0b0073

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setLeftContainerWidth(I)V

    .line 373
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const v3, 0x7f0b0076

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setRightContainerWidth(I)V

    goto/16 :goto_2

    .line 383
    :cond_8
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftIcon:I

    const/4 v4, 0x0

    sget-object v7, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v2, v3, v4, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto/16 :goto_3

    .line 391
    :cond_9
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "title:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 392
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "destinations:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 393
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 394
    .local v23, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->title:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleViewHolder;->buildModel(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    array-length v2, v2

    move/from16 v0, v22

    if-ge v0, v2, :cond_b

    .line 397
    const/16 v21, 0x0

    .line 398
    .local v21, "destinationModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconUnselected:I

    if-nez v2, :cond_a

    .line 399
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v12, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->id:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v13, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->icon:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v14, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v15, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconDeselectedColor:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v12 .. v19}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v21

    .line 419
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-boolean v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitleFormatted:Z

    move-object/from16 v0, v21

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitleFormatted:Z

    .line 420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-boolean v2, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2Formatted:Z

    move-object/from16 v0, v21

    iput-boolean v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2Formatted:Z

    .line 421
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->state:Ljava/lang/Object;

    .line 422
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_5

    .line 409
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v12, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->id:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v13, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->icon:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v14, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconUnselected:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v15, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconDeselectedColor:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v2, v2, v22

    iget-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v12 .. v19}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconsTwoViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v21

    goto :goto_6

    .line 424
    .end local v21    # "destinationModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :cond_b
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    .line 425
    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->initialSelection:I

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v4, v7}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZ)V

    goto/16 :goto_4

    .end local v22    # "i":I
    .end local v23    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    .restart local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v6    # "route":Lcom/here/android/mpa/routing/Route;
    .restart local v24    # "maskView":Landroid/view/View;
    .restart local v26    # "routeId":Ljava/lang/String;
    :cond_c
    move-object/from16 v20, v5

    .end local v5    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .restart local v20    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    goto/16 :goto_0
.end method


# virtual methods
.method clearSelection()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 479
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->closed:Z

    .line 480
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z

    .line 481
    return-void
.end method

.method clearState()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 454
    iput v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->initialSelection:I

    .line 455
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->title:Ljava/lang/String;

    .line 456
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .line 457
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    .line 458
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    .line 459
    iput v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationIcon:I

    .line 460
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .line 461
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoList:Ljava/util/List;

    .line 462
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoBBoxList:Ljava/util/List;

    .line 463
    iput v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelection:I

    .line 464
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 465
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->end:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 466
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    .line 467
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    .line 468
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->doNotAddOriginalRoute:Z

    .line 469
    return-void
.end method

.method close()V
    .locals 1

    .prologue
    .line 577
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->close(Ljava/lang/Runnable;)V

    .line 578
    return-void
.end method

.method close(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    const/4 v3, 0x1

    .line 581
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->closed:Z

    if-eqz v1, :cond_1

    .line 582
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "already closed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    if-eqz v1, :cond_4

    .line 588
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 589
    .local v0, "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-eqz v0, :cond_3

    .line 590
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->setBackgroundColor(I)V

    .line 593
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    if-eqz v1, :cond_4

    .line 594
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->setMapMaskVisibility(I)V

    .line 597
    .end local v0    # "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    :cond_4
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 598
    iput-boolean v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->closed:Z

    .line 599
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "close"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 602
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 603
    .restart local v0    # "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-eqz v0, :cond_0

    .line 604
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    new-instance v2, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$1;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter$1;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateOut(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method isAlive()Z
    .locals 1

    .prologue
    .line 516
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->registered:Z

    return v0
.end method

.method isClosed()Z
    .locals 1

    .prologue
    .line 621
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->closed:Z

    return v0
.end method

.method isShowDestinationMap()Z
    .locals 1

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    return v0
.end method

.method isShowingRouteMap()Z
    .locals 1

    .prologue
    .line 512
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    return v0
.end method

.method itemClicked(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 6
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v2, 0x1

    .line 484
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "itemClicked:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 485
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z

    if-eqz v3, :cond_1

    .line 486
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "already handled ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 508
    :cond_0
    :goto_0
    return v2

    .line 490
    :cond_1
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z

    .line 492
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 493
    .local v1, "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-eqz v1, :cond_0

    .line 497
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    if-eqz v3, :cond_2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isNotADestination(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 500
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    .line 501
    .local v0, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v3

    if-nez v3, :cond_2

    .line 502
    invoke-direct {p0, v1, p1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showConfirmation(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    goto :goto_0

    .line 507
    .end local v0    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_2
    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-direct {p0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->performAction(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;II)V

    .line 508
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z

    goto :goto_0
.end method

.method itemSelected(I)V
    .locals 7
    .param p1, "pos"    # I

    .prologue
    const/4 v6, 0x1

    .line 551
    invoke-static {p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchDestinationScroll(I)V

    .line 552
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-eqz v3, :cond_2

    .line 553
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    aget-object v3, v3, p1

    iget-object v1, v3, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->routeId:Ljava/lang/String;

    .line 554
    .local v1, "routeId":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v2

    .line 555
    .local v2, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v2, :cond_1

    .line 556
    iget-object v3, v2, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Route;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v0

    .line 557
    .local v0, "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-object v4, v2, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v3, v0, v4, v6, v6}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->zoomToBoundBox(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZZ)V

    .line 569
    .end local v0    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    .end local v1    # "routeId":Ljava/lang/String;
    .end local v2    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :cond_0
    :goto_0
    return-void

    .line 559
    .restart local v1    # "routeId":Ljava/lang/String;
    .restart local v2    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :cond_1
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "itemSelected: route not found:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 561
    .end local v1    # "routeId":Ljava/lang/String;
    .end local v2    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :cond_2
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    if-eqz v3, :cond_0

    .line 562
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelection:I

    invoke-virtual {v3, p1, v4}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->changeMarkerSelection(II)V

    .line 563
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationGeoBBoxList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/common/GeoBoundingBox;

    .line 564
    .restart local v0    # "boundingBox":Lcom/here/android/mpa/common/GeoBoundingBox;
    if-eqz v0, :cond_3

    .line 565
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v5, v6}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->zoomToBoundBox(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/routing/Route;ZZ)V

    .line 567
    :cond_3
    iput p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelection:I

    goto :goto_0
.end method

.method itemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 5
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    const/4 v4, -0x1

    .line 531
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    if-nez v1, :cond_0

    .line 548
    :goto_0
    return-void

    .line 534
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "itemSelected:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 535
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    if-eqz v1, :cond_1

    .line 536
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;-><init>()V

    .line 537
    .local v0, "state":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v2, v3, v0}, Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;->onItemSelected(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 538
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "overridden onItemSelected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 542
    .end local v0    # "state":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;
    :cond_1
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isNotADestination(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 543
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelection:I

    invoke-virtual {v1, v4, v2}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->changeMarkerSelection(II)V

    .line 544
    iput v4, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelection:I

    goto :goto_0

    .line 547
    :cond_2
    iget v1, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelected(I)V

    goto/16 :goto_0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    const/4 v12, 0x0

    .line 258
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "onLoad"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 259
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 260
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v6

    invoke-virtual {v6, v11}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enablePhoneNotifications(Z)V

    .line 261
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->clearState()V

    .line 262
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->reset()V

    .line 263
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v6, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 264
    iput-boolean v11, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->registered:Z

    .line 265
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v5

    .line 266
    .local v5, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .line 267
    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 269
    if-eqz p1, :cond_1

    .line 270
    const-string v6, "PICKER_LEFT_TITLE"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftTitle:Ljava/lang/String;

    .line 271
    const-string v6, "PICKER_LEFT_ICON"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftIcon:I

    .line 272
    const-string v6, "PICKER_LEFT_ICON_BKCOLOR"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->leftBkColor:I

    .line 273
    const-string v6, "PICKER_INITIAL_SELECTION"

    invoke-virtual {p1, v6, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->initialSelection:I

    .line 274
    const-string v6, "PICKER_TITLE"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->title:Ljava/lang/String;

    .line 275
    const-string v6, "PICKER_DESTINATIONS"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    check-cast v6, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinations:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    .line 276
    const-string v6, "PICKER_HIDE"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->hideScreenOnNavStop:Z

    .line 277
    const-string v6, "PICKER_SHOW_ROUTE_MAP"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    .line 278
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-eqz v6, :cond_4

    .line 279
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "show route map"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 280
    const-string v6, "PICKER_MAP_START_LAT"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 281
    .local v0, "lat":D
    const-string v6, "PICKER_MAP_START_LNG"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 282
    .local v2, "lng":D
    cmpl-double v6, v0, v8

    if-eqz v6, :cond_2

    cmpl-double v6, v2, v8

    if-eqz v6, :cond_2

    .line 283
    new-instance v6, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 287
    :goto_0
    const-string v6, "PICKER_MAP_END_LAT"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 288
    const-string v6, "PICKER_MAP_END_LNG"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 289
    cmpl-double v6, v0, v8

    if-eqz v6, :cond_3

    cmpl-double v6, v2, v8

    if-eqz v6, :cond_3

    .line 290
    new-instance v6, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->end:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 310
    .end local v0    # "lat":D
    .end local v2    # "lng":D
    :goto_1
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    if-eqz v6, :cond_1

    .line 311
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    .line 312
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    sget-object v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v6, v7, :cond_7

    .line 313
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "switchbackmode: null"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 321
    :cond_1
    :goto_2
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$200()Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 322
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$200()Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    move-result-object v6

    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->arguments2:Ljava/lang/Object;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$300(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    .line 323
    # setter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
    invoke-static {v12}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$202(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    .line 327
    :goto_3
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->updateView()V

    .line 328
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 329
    return-void

    .line 285
    .restart local v0    # "lat":D
    .restart local v2    # "lng":D
    :cond_2
    iput-object v12, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    goto :goto_0

    .line 292
    :cond_3
    iput-object v12, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->end:Lcom/here/android/mpa/common/GeoCoordinate;

    goto :goto_1

    .line 295
    .end local v0    # "lat":D
    .end local v2    # "lng":D
    :cond_4
    const-string v6, "PICKER_SHOW_DESTINATION_MAP"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    .line 296
    iget-boolean v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showDestinationMap:Z

    if-eqz v6, :cond_5

    .line 297
    const-string v6, "PICKER_DESTINATION_ICON"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->destinationIcon:I

    .line 298
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "show dest map"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 299
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->buildDestinationBBox()V

    .line 301
    :cond_5
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->start:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 302
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationRouteRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v4

    .line 303
    .local v4, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v4, :cond_6

    .line 304
    new-instance v6, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v7, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iget-object v7, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    invoke-direct {v6, v8, v9, v10, v11}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->end:Lcom/here/android/mpa/common/GeoCoordinate;

    goto/16 :goto_1

    .line 306
    :cond_6
    iput-object v12, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->end:Lcom/here/android/mpa/common/GeoCoordinate;

    goto/16 :goto_1

    .line 315
    .end local v4    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_7
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " onLoad switchbackmode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 316
    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    sget-object v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    goto/16 :goto_2

    .line 325
    :cond_8
    iput-object v12, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    goto/16 :goto_3
.end method

.method public onNavigationModeChanged(Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 713
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->hideScreenOnNavStop:Z

    if-eqz v1, :cond_0

    .line 714
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .line 715
    .local v0, "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    if-eqz v0, :cond_0

    .line 716
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNavigationModeChanged event["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;->navigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 717
    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;->navigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v1, v2, :cond_0

    .line 719
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->close()V

    .line 723
    .end local v0    # "view":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 431
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->doNotAddOriginalRoute:Z

    if-nez v3, :cond_3

    move v0, v1

    .line 432
    .local v0, "addRoute":Z
    :goto_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->showRouteMap:Z

    if-nez v3, :cond_0

    .line 433
    const/4 v0, 0x1

    .line 435
    :cond_0
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onUnload addbackRoute:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 436
    # setter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
    invoke-static {v6}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$202(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    .line 437
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->clearState()V

    .line 438
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->switchBackfromRouteSearchMode(Z)V

    .line 439
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-eqz v3, :cond_1

    .line 440
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onUnload switchbackmode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 441
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->switchBackMode:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    .line 443
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 444
    iput-object v6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->pickerCallback:Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;

    .line 445
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->registered:Z

    if-eqz v1, :cond_2

    .line 446
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->registered:Z

    .line 447
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 449
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->reset()V

    .line 450
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 451
    return-void

    .end local v0    # "addRoute":Z
    :cond_3
    move v0, v2

    .line 431
    goto :goto_0
.end method

.method reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 472
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->closed:Z

    .line 473
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->handledSelection:Z

    .line 474
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->data:Ljava/util/List;

    .line 475
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->hideScreenOnNavStop:Z

    .line 476
    return-void
.end method

.method startMapFluctuator()V
    .locals 1

    .prologue
    .line 524
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->registered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupFluctuator()V

    .line 526
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->startFluctuator()V

    .line 528
    :cond_0
    return-void
.end method
