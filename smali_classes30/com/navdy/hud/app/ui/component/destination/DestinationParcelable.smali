.class public Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
.super Ljava/lang/Object;
.source "DestinationParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY:Ljava/lang/String; = ""


# instance fields
.field contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field destinationAddress:Ljava/lang/String;

.field destinationPlaceId:Ljava/lang/String;

.field destinationSubTitle:Ljava/lang/String;

.field destinationSubTitle2:Ljava/lang/String;

.field destinationSubTitle2Formatted:Z

.field destinationSubTitleFormatted:Z

.field destinationTitle:Ljava/lang/String;

.field displayLatitude:D

.field displayLongitude:D

.field public distanceStr:Ljava/lang/String;

.field icon:I

.field iconDeselectedColor:I

.field iconSelectedColor:I

.field iconUnselected:I

.field id:I

.field identifier:Ljava/lang/String;

.field navLatitude:D

.field navLongitude:D

.field phoneNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field placeType:Lcom/navdy/service/library/events/places/PlaceType;

.field routeId:Ljava/lang/String;

.field type:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;DDDDIIIILcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;Lcom/navdy/service/library/events/places/PlaceType;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "destinationTitle"    # Ljava/lang/String;
    .param p3, "destinationSubTitle"    # Ljava/lang/String;
    .param p4, "destinationSubTitleFormatted"    # Z
    .param p5, "destinationSubTitle2"    # Ljava/lang/String;
    .param p6, "destinationSubTitleFormatted2"    # Z
    .param p7, "destinationAddress"    # Ljava/lang/String;
    .param p8, "navLatitude"    # D
    .param p10, "navLongitude"    # D
    .param p12, "displayLatitude"    # D
    .param p14, "displayLongitude"    # D
    .param p16, "icon"    # I
    .param p17, "iconUnselected"    # I
    .param p18, "iconSelectedColor"    # I
    .param p19, "iconDeselectedColor"    # I
    .param p20, "type"    # Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;
    .param p21, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->id:I

    .line 73
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    .line 75
    iput-boolean p4, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitleFormatted:Z

    .line 76
    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2:Ljava/lang/String;

    .line 77
    iput-boolean p6, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2Formatted:Z

    .line 78
    iput-object p7, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationAddress:Ljava/lang/String;

    .line 79
    iput-wide p8, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLatitude:D

    .line 80
    iput-wide p10, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLongitude:D

    .line 81
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLatitude:D

    .line 82
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLongitude:D

    .line 83
    move/from16 v0, p16

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->icon:I

    .line 84
    move/from16 v0, p17

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconUnselected:I

    .line 85
    move/from16 v0, p18

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    .line 86
    move/from16 v0, p19

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconDeselectedColor:I

    .line 87
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->type:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    .line 88
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 89
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitleFormatted:Z

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2:Ljava/lang/String;

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2Formatted:Z

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationAddress:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLatitude:D

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLongitude:D

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLatitude:D

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLongitude:D

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->icon:I

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconUnselected:I

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconDeselectedColor:I

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->routeId:Ljava/lang/String;

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationPlaceId:Ljava/lang/String;

    .line 159
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->values()[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->type:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->id:I

    .line 161
    invoke-static {}, Lcom/navdy/service/library/events/places/PlaceType;->values()[Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 162
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->contacts:Ljava/util/List;

    .line 163
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->phoneNumbers:Ljava/util/List;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->identifier:Ljava/lang/String;

    .line 165
    return-void

    :cond_0
    move v0, v2

    .line 145
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 147
    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public setContacts(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->contacts:Ljava/util/List;

    .line 105
    return-void
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->identifier:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public setPhoneNumbers(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->phoneNumbers:Ljava/util/List;

    .line 109
    return-void
.end method

.method public setPlaceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationPlaceId:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setRouteId(Ljava/lang/String;)V
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->routeId:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitleFormatted:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2Formatted:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationAddress:Ljava/lang/String;

    if-nez v0, :cond_5

    const-string v0, ""

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    iget-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLatitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 125
    iget-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->navLongitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 126
    iget-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLatitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 127
    iget-wide v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->displayLongitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 128
    iget v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->icon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconUnselected:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconSelectedColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->iconDeselectedColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->routeId:Ljava/lang/String;

    if-nez v0, :cond_6

    const-string v0, ""

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationPlaceId:Ljava/lang/String;

    if-nez v0, :cond_7

    const-string v0, ""

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->type:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->contacts:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 138
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->phoneNumbers:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 139
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->identifier:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationTitle:Ljava/lang/String;

    goto/16 :goto_0

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle:Ljava/lang/String;

    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 120
    goto/16 :goto_2

    .line 121
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationSubTitle2:Ljava/lang/String;

    goto/16 :goto_3

    :cond_4
    move v1, v2

    .line 122
    goto :goto_4

    .line 123
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->destinationAddress:Ljava/lang/String;

    goto :goto_5

    .line 132
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->routeId:Ljava/lang/String;

    goto :goto_6

    .line 133
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->routeId:Ljava/lang/String;

    goto :goto_7
.end method
