.class Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;
.super Ljava/lang/Object;
.source "DestinationPickerView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordRouteSelectionCancelled()V

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->close()V

    .line 112
    return-void
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 85
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 90
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 92
    return-void
.end method

.method public onLoad()V
    .locals 6

    .prologue
    .line 55
    # getter for: Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "onLoad"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    if-eqz v1, :cond_1

    .line 57
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isShowingRouteMap()Z

    move-result v0

    .line 58
    .local v0, "b":Z
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isShowDestinationMap()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->setBackgroundColor(I)V

    .line 60
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 76
    .end local v0    # "b":Z
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateIn(Landroid/animation/Animator$AnimatorListener;)V

    .line 77
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 49
    iget v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemClicked(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z

    .line 51
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method
