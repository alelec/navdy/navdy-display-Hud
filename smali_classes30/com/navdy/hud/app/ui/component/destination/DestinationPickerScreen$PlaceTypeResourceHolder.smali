.class public Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
.super Ljava/lang/Object;
.source "DestinationPickerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaceTypeResourceHolder"
.end annotation


# instance fields
.field public final colorRes:I

.field public final destinationIcon:I

.field public final iconRes:I

.field public final titleRes:I


# direct methods
.method private constructor <init>(IIII)V
    .locals 0
    .param p1, "iconRes"    # I
    .param p2, "titleRes"    # I
    .param p3, "colorRes"    # I
    .param p4, "destinationIcon"    # I

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->iconRes:I

    .line 159
    iput p2, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->titleRes:I

    .line 160
    iput p3, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->colorRes:I

    .line 161
    iput p4, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->destinationIcon:I

    .line 162
    return-void
.end method

.method synthetic constructor <init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;

    .prologue
    .line 151
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIII)V

    return-void
.end method
