.class public Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "DestinationPickerScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;,
        Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;,
        Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Module;,
        Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f030046
.end annotation


# static fields
.field private static final CANCEL_POSITION:I = 0x1

.field private static CONFIRMATION_CHOICES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MIN_DISTANCE_EXPANSION:F = 3000.0f

.field private static final MIN_DISTANCE_THRESHOLD:F = 5000.0f

.field private static final NAVIGATE_POSITION:I = 0x0

.field public static final PICKER_DESTINATIONS:Ljava/lang/String; = "PICKER_DESTINATIONS"

.field public static final PICKER_DESTINATION_ICON:Ljava/lang/String; = "PICKER_DESTINATION_ICON"

.field public static final PICKER_HIDE_IF_NAV_STOPS:Ljava/lang/String; = "PICKER_HIDE"

.field public static final PICKER_INITIAL_SELECTION:Ljava/lang/String; = "PICKER_INITIAL_SELECTION"

.field public static final PICKER_LEFT_ICON:Ljava/lang/String; = "PICKER_LEFT_ICON"

.field public static final PICKER_LEFT_ICON_BKCOLOR:Ljava/lang/String; = "PICKER_LEFT_ICON_BKCOLOR"

.field public static final PICKER_LEFT_TITLE:Ljava/lang/String; = "PICKER_LEFT_TITLE"

.field public static final PICKER_MAP_END_LAT:Ljava/lang/String; = "PICKER_MAP_END_LAT"

.field public static final PICKER_MAP_END_LNG:Ljava/lang/String; = "PICKER_MAP_END_LNG"

.field public static final PICKER_MAP_START_LAT:Ljava/lang/String; = "PICKER_MAP_START_LAT"

.field public static final PICKER_MAP_START_LNG:Ljava/lang/String; = "PICKER_MAP_START_LNG"

.field public static final PICKER_SHOW_DESTINATION_MAP:Ljava/lang/String; = "PICKER_SHOW_DESTINATION_MAP"

.field public static final PICKER_SHOW_ROUTE_MAP:Ljava/lang/String; = "PICKER_SHOW_ROUTE_MAP"

.field public static final PICKER_TITLE:Ljava/lang/String; = "PICKER_TITLE"

.field private static final SELECTION_ANIMATION_DELAY:I = 0x3e8

.field private static instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

.field private static final placeResMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/places/PlaceType;",
            "Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const v10, 0x7f0201ab

    const v4, 0x7f0201a9

    const v9, 0x7f0201a5

    const/4 v5, 0x0

    .line 68
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->CONFIRMATION_CHOICES:Ljava/util/List;

    .line 95
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 96
    .local v6, "resources":Landroid/content/res/Resources;
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->CONFIRMATION_CHOICES:Ljava/util/List;

    const v1, 0x7f0900b4

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->CONFIRMATION_CHOICES:Ljava/util/List;

    const v1, 0x7f0900b3

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    .line 170
    sget-object v7, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    sget-object v8, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    const v1, 0x7f0201b6

    const v2, 0x7f090108

    const v3, 0x7f0d0077

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v7, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    sget-object v8, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    const v1, 0x7f0201b9

    const v2, 0x7f0901f4

    const v3, 0x7f0d007a

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v7, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    sget-object v8, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    const v1, 0x7f0201bb

    const v2, 0x7f090106

    const v3, 0x7f0d0076

    move v4, v10

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v7, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    sget-object v8, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    const v1, 0x7f0201bc

    const v2, 0x7f090130

    const v3, 0x7f0d0078

    move v4, v9

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v7, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    sget-object v8, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    const v1, 0x7f0201b4

    const v2, 0x7f0900a8

    const v3, 0x7f0d0075

    move v4, v10

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v7, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    sget-object v8, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    const v1, 0x7f0201b1

    const v2, 0x7f090020

    const v3, 0x7f0d0074

    move v4, v9

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v7, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    sget-object v8, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    const v1, 0x7f0201b8

    const v2, 0x7f09017b

    const v3, 0x7f0d0079

    const v4, 0x7f0201ac

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;-><init>(IIIILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$1;)V

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    .line 87
    sput-object p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    .line 88
    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    .prologue
    .line 67
    sput-object p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->instance:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    return-object p0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->arguments2:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500()Ljava/util/List;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->CONFIRMATION_CHOICES:Ljava/util/List;

    return-object v0
.end method

.method public static getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    .locals 1
    .param p0, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 215
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->placeResMapping:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    return-object v0
.end method


# virtual methods
.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 139
    const/4 v0, -0x1

    return v0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 144
    const/4 v0, -0x1

    return v0
.end method

.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Module;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
