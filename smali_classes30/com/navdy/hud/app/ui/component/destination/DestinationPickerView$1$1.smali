.class Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;
.super Ljava/lang/Object;
.source "DestinationPickerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isShowingRouteMap()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->startMapFluctuator()V

    goto :goto_0

    .line 68
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->isShowDestinationMap()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->initialSelection:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelected(I)V

    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1$1;->this$1:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;->this$0:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    iget v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->initialSelection:I

    iput v1, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->itemSelection:I

    goto :goto_0
.end method
