.class public final enum Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;
.super Ljava/lang/Enum;
.source "DestinationParcelable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DestinationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

.field public static final enum DESTINATION:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

.field public static final enum NONE:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->NONE:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    .line 19
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    const-string v1, "DESTINATION"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->DESTINATION:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    sget-object v1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->NONE:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->DESTINATION:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->$VALUES:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->$VALUES:[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    return-object v0
.end method
