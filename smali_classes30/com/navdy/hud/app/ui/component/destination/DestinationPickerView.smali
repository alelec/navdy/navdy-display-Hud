.class public Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;
.super Landroid/widget/RelativeLayout;
.source "DestinationPickerView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

.field confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0174
    .end annotation
.end field

.field public presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field rightBackground:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0173
    .end annotation
.end field

.field vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 129
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView$1;-><init>(Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-static {p1}, Lcom/navdy/hud/app/HudApplication;->setContext(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 182
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 149
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 152
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clear()V

    .line 158
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->presenter:Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 160
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 139
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 140
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 141
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 143
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 144
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 167
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handleGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->confirmationLayout:Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    .line 176
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
