.class public Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
.super Ljava/lang/Object;
.source "ChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Icon"
.end annotation


# instance fields
.field public resIdSelected:I

.field public resIdUnSelected:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdSelected:I

    .line 74
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdUnSelected:I

    .line 75
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "resIdSelected"    # I
    .param p2, "resIdUnSelected"    # I

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdSelected:I

    .line 79
    iput p2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdUnSelected:I

    .line 80
    return-void
.end method
