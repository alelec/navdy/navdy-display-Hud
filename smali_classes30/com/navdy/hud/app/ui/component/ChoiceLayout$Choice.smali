.class public Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
.super Ljava/lang/Object;
.source "ChoiceLayout.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Choice"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

.field public id:I

.field public label:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, -0x1

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->label:Ljava/lang/String;

    .line 99
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->label:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->label:Ljava/lang/String;

    .line 102
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 103
    .local v0, "i":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 104
    .local v1, "j":I
    if-eq v0, v3, :cond_1

    if-eq v1, v3, :cond_1

    .line 105
    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    invoke-direct {v2, v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;-><init>(II)V

    iput-object v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    .line 107
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;I)V
    .locals 0
    .param p1, "icon"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    .param p2, "id"    # I

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    .line 94
    iput p2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    .line 95
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->label:Ljava/lang/String;

    .line 89
    iput p2, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    .line 90
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, -0x1

    .line 134
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->label:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->label:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    iget v0, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdSelected:I

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    iget v1, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdUnSelected:I

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget v0, p0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    return-void

    .line 134
    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    move v0, v1

    .line 135
    goto :goto_1
.end method
