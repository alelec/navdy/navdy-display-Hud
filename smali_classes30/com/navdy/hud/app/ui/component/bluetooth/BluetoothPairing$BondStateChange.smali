.class public Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;
.super Ljava/lang/Object;
.source "BluetoothPairing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BondStateChange"
.end annotation


# instance fields
.field public final device:Landroid/bluetooth/BluetoothDevice;

.field public final newState:I

.field public final oldState:I


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;II)V
    .locals 0
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "oldState"    # I
    .param p3, "newState"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->device:Landroid/bluetooth/BluetoothDevice;

    .line 48
    iput p2, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->oldState:I

    .line 49
    iput p3, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->newState:I

    .line 50
    return-void
.end method
