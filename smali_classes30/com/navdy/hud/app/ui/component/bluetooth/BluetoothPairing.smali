.class public Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;
.super Ljava/lang/Object;
.source "BluetoothPairing.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/toast/IToastCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$PairingCancelled;,
        Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;
    }
.end annotation


# static fields
.field public static final ARG_AUTO_ACCEPT:Ljava/lang/String; = "auto"

.field public static final ARG_DEVICE:Ljava/lang/String; = "device"

.field public static final ARG_PIN:Ljava/lang/String; = "pin"

.field public static final ARG_VARIANT:Ljava/lang/String; = "variant"

.field private static final BLUETOOTH_PAIRING_TOAST_ID:Ljava/lang/String; = "bt-pairing"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private acceptDone:Z

.field private autoAccept:Z

.field private bus:Lcom/squareup/otto/Bus;

.field private choices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private confirmationRequired:Z

.field private device:Landroid/bluetooth/BluetoothDevice;

.field private initialized:Z

.field private pin:I

.field private variant:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>(Landroid/bluetooth/BluetoothDevice;IIZLcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "pin"    # I
    .param p3, "variant"    # I
    .param p4, "autoAccept"    # Z
    .param p5, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->choices:Ljava/util/ArrayList;

    .line 97
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    .line 98
    iput p2, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->pin:I

    .line 99
    iput p3, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->variant:I

    .line 100
    iput-boolean p4, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->autoAccept:Z

    .line 101
    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->bus:Lcom/squareup/otto/Bus;

    .line 102
    return-void
.end method

.method private confirm()V
    .locals 3

    .prologue
    .line 229
    sget-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "confirm()"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->variant:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->pin:I

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/util/BluetoothUtil;->confirmPairing(Landroid/bluetooth/BluetoothDevice;II)V

    .line 231
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->finish()V

    .line 232
    return-void
.end method

.method private finish()V
    .locals 2

    .prologue
    .line 224
    sget-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "finish()"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 225
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    .line 226
    return-void
.end method

.method private formatPin(II)Ljava/lang/String;
    .locals 4
    .param p1, "pin"    # I
    .param p2, "variant"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 193
    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    .line 194
    const-string v0, "%04d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 196
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%06d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static showBluetoothPairingToast(Landroid/os/Bundle;)V
    .locals 8
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 62
    sget-object v5, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "showBluetoothPairingToast"

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 63
    const-string v5, "device"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 64
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v5, "pin"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 65
    .local v2, "pin":I
    const-string v5, "variant"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 66
    .local v3, "variant":I
    const-string v5, "auto"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 69
    .local v4, "autoAccept":Z
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 70
    .local v6, "dialConnected":Z
    if-nez v6, :cond_0

    .line 71
    const/4 v4, 0x1

    .line 74
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;

    .line 75
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;-><init>(Landroid/bluetooth/BluetoothDevice;IIZLcom/squareup/otto/Bus;)V

    .line 76
    .local v0, "pairingToast":Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;
    sget-object v5, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "show()"

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 77
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->show()V

    .line 78
    return-void
.end method


# virtual methods
.method public executeChoiceItem(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 178
    sget-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "execute:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 179
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->confirmationRequired:Z

    if-eqz v0, :cond_1

    .line 181
    if-nez p1, :cond_0

    .line 182
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->confirm()V

    .line 189
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->finish()V

    goto :goto_0

    .line 187
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->finish()V

    goto :goto_0
.end method

.method public onBondStateChange(Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 208
    sget-object v1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onBondStateChange"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 209
    iget-object v0, p1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->device:Landroid/bluetooth/BluetoothDevice;

    .line 210
    .local v0, "dev":Landroid/bluetooth/BluetoothDevice;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    iget v1, p1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->newState:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->newState:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_2

    .line 213
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ending pairing dialog because of bond state change"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 214
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->finish()V

    .line 221
    :cond_1
    :goto_0
    return-void

    .line 216
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring bond state change for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;->newState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dialog device:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    .line 218
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 216
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public onPairingCancelled(Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$PairingCancelled;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$PairingCancelled;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 202
    sget-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onPairingCancelled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 203
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->finish()V

    .line 204
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 3
    .param p1, "view"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 154
    sget-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "registerBus"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 157
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->acceptDone:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->autoAccept:Z

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->acceptDone:Z

    .line 159
    sget-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "auto-accept"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    iget v1, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->variant:I

    iget v2, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->pin:I

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/util/BluetoothUtil;->confirmPairing(Landroid/bluetooth/BluetoothDevice;II)V

    .line 163
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 167
    sget-object v0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unregisterBus"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 169
    return-void
.end method

.method show()V
    .locals 12

    .prologue
    const v6, 0x7f09004f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 105
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 107
    .local v9, "resources":Landroid/content/res/Resources;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 108
    .local v2, "data":Landroid/os/Bundle;
    const-string v0, "8"

    const v1, 0x7f0200e8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 109
    const-string v0, "1"

    const v1, 0x7f090033

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "2"

    const v1, 0x7f090032

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const/4 v7, 0x0

    .line 113
    .local v7, "displayName":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    .line 115
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    .line 120
    :cond_0
    if-nez v7, :cond_1

    .line 121
    const v0, 0x7f0902d9

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 123
    :cond_1
    const v0, 0x7f09002f

    new-array v1, v5, [Ljava/lang/Object;

    aput-object v7, v1, v4

    invoke-virtual {v9, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 124
    .local v10, "subHeading":Ljava/lang/String;
    const-string v0, "6"

    invoke-virtual {v2, v0, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "4"

    iget v1, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->pin:I

    iget v3, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->variant:I

    invoke-direct {p0, v1, v3}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->formatPin(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->autoAccept:Z

    if-nez v0, :cond_4

    move v8, v5

    .line 129
    .local v8, "required":Z
    :goto_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->confirmationRequired:Z

    if-ne v8, v0, :cond_2

    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->initialized:Z

    if-nez v0, :cond_3

    .line 130
    :cond_2
    iput-boolean v5, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->initialized:Z

    .line 131
    iput-boolean v8, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->confirmationRequired:Z

    .line 132
    if-eqz v8, :cond_5

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->choices:Ljava/util/ArrayList;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    const v3, 0x7f09002e

    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->choices:Ljava/util/ArrayList;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :goto_1
    const-string v0, "20"

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->choices:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 141
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v11

    .line 142
    .local v11, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual {v11}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    .line 143
    invoke-virtual {v11}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 144
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "bt-pairing"

    move-object v3, p0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZZ)V

    invoke-virtual {v11, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 150
    return-void

    .end local v8    # "required":Z
    .end local v11    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_4
    move v8, v4

    .line 128
    goto :goto_0

    .line 136
    .restart local v8    # "required":Z
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->choices:Ljava/util/ArrayList;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
