.class Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VerticalMenuComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    .prologue
    .line 780
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;->this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x4

    .line 783
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;->this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$1000(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 784
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;->this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$1100(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 785
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;->this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$1200(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 786
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;->this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$1300(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 787
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;->this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->val$endAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;->this$1:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->val$endAction:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 790
    :cond_0
    return-void
.end method
