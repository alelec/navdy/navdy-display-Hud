.class Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VerticalMenuComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performEnterAnimation(Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

.field final synthetic val$endAction:Ljava/lang/Runnable;

.field final synthetic val$startAction:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 929
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->val$startAction:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->val$endAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x4

    .line 945
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 946
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 949
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$1000(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 950
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$1100(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 951
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$1200(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 953
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->val$endAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->val$endAction:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 956
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 932
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->val$startAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 933
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->val$startAction:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 936
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setPivotX(F)V

    .line 937
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v1, v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setPivotY(F)V

    .line 938
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 939
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setScaleY(F)V

    .line 940
    return-void
.end method
