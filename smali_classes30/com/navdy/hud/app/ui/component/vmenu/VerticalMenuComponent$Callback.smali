.class public interface abstract Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
.super Ljava/lang/Object;
.source "VerticalMenuComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract isClosed()Z
.end method

.method public abstract isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
.end method

.method public abstract onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
.end method

.method public abstract onFastScrollEnd()V
.end method

.method public abstract onFastScrollStart()V
.end method

.method public abstract onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
.end method

.method public abstract onLoad()V
.end method

.method public abstract onScrollIdle()V
.end method

.method public abstract select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
.end method

.method public abstract showToolTip()V
.end method
