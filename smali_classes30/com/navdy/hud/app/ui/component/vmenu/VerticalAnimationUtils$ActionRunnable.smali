.class public Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;
.super Ljava/lang/Object;
.source "VerticalAnimationUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActionRunnable"
.end annotation


# instance fields
.field private args:Landroid/os/Bundle;

.field private bus:Lcom/squareup/otto/Bus;

.field private ignoreAnimation:Z

.field private screen:Lcom/navdy/service/library/events/ui/Screen;


# direct methods
.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 92
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    .line 93
    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V
    .locals 0
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "screen"    # Lcom/navdy/service/library/events/ui/Screen;
    .param p3, "args"    # Landroid/os/Bundle;
    .param p4, "ignoreAnimation"    # Z

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->bus:Lcom/squareup/otto/Bus;

    .line 100
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->screen:Lcom/navdy/service/library/events/ui/Screen;

    .line 101
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->args:Landroid/os/Bundle;

    .line 102
    iput-boolean p4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->ignoreAnimation:Z

    .line 103
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->args:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->screen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->screen:Lcom/navdy/service/library/events/ui/Screen;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->args:Landroid/os/Bundle;

    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;->ignoreAnimation:Z

    invoke-direct {v1, v2, v3, v4}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
