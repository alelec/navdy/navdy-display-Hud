.class public Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;
.super Ljava/lang/Object;
.source "VerticalAnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$ActionRunnable;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static animateDimension(Landroid/view/View;I)Landroid/animation/Animator;
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "toDimension"    # I

    .prologue
    .line 130
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 131
    .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    aput v4, v2, v3

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 132
    .local v0, "dimensionAnimator":Landroid/animation/ValueAnimator;
    new-instance v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;

    invoke-direct {v2, v1, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 141
    return-object v0
.end method

.method public static animateMargin(Landroid/view/View;I)Landroid/animation/Animator;
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "toMargin"    # I

    .prologue
    .line 116
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 117
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    aput v4, v2, v3

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 118
    .local v1, "marginAnimator":Landroid/animation/ValueAnimator;
    new-instance v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$3;

    invoke-direct {v2, v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$3;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 126
    return-object v1
.end method

.method public static copyImage(Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 1
    .param p0, "from"    # Landroid/widget/ImageView;
    .param p1, "to"    # Landroid/widget/ImageView;

    .prologue
    .line 60
    invoke-static {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->getBitmapFromImageView(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 61
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 64
    :cond_0
    return-void
.end method

.method public static getBitmapFromImageView(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 67
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 68
    .local v3, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 69
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    instance-of v4, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_0

    move-object v1, v3

    .line 70
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 72
    :cond_0
    instance-of v4, p0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    if-nez v4, :cond_1

    if-eqz v1, :cond_1

    .line 74
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 77
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 78
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    .local v2, "c":Landroid/graphics/Canvas;
    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public static performClick(Landroid/view/View;ILjava/lang/Runnable;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 27
    div-int/lit8 v0, p1, 0x2

    const/4 v1, 0x1

    invoke-static {p0, v0, p2, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->performClickDown(Landroid/view/View;ILjava/lang/Runnable;Z)V

    .line 28
    return-void
.end method

.method public static performClickDown(Landroid/view/View;ILjava/lang/Runnable;Z)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "endAction"    # Ljava/lang/Runnable;
    .param p3, "clickup"    # Z

    .prologue
    const v1, 0x3f4ccccd    # 0.8f

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$1;

    invoke-direct {v1, p3, p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$1;-><init>(ZLandroid/view/View;ILjava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 46
    return-void
.end method

.method public static performClickUp(Landroid/view/View;ILjava/lang/Runnable;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "endAction"    # Ljava/lang/Runnable;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 49
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$2;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$2;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 57
    return-void
.end method
