.class public Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
.super Ljava/lang/Object;
.source "VerticalMenuComponent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    }
.end annotation


# static fields
.field public static final ANIMATION_IN_OUT_DURATION:I = 0x96

.field public static final CLICK_ANIMATION_DURATION:I = 0x32

.field public static final CLOSE_ANIMATION_DURATION:I = 0x96

.field private static final FAST_SCROLL_APPEARANCE_ANIM_TIME:I = 0x32

.field private static final FAST_SCROLL_CLOSE_MENU_WAIT_PERIOD:I = 0x96

.field private static final FAST_SCROLL_EXIT_DELAY:I = 0x320

.field private static final FAST_SCROLL_TRIGGER_CLICKS:I = 0xa

.field private static final FAST_SCROLL_TRIGGER_TIME:I

.field private static final INDICATOR_FADE_IDLE_DURATION:I = 0x7d0

.field private static final INDICATOR_FADE_IN_DURATION:I = 0x1f4

.field private static final INDICATOR_FADE_OUT_DURATION:I = 0x1f4

.field private static final MAIN_TO_SELECTED_ICON_SCALE:F = 1.0f

.field public static final MIN_FAST_SCROLL_ITEM:I = 0x28

.field public static final MIN_INDEX_ENTRY_COUNT:I = 0x2

.field private static final NO_ANIMATION:I = -0x1

.field private static final SELECTED_TO_MAIN_ICON_SCALE:F = 1.2f

.field public static final animateTranslateY:I

.field private static final closeContainerHeight:I

.field private static final closeContainerScrollY:I

.field private static final closeHaloColor:I

.field private static final indicatorY:I

.field private static final location:[I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private accelerateInterpolator:Landroid/view/animation/Interpolator;

.field private animImageView:Landroid/widget/ImageView;

.field private animSubTitle:Landroid/widget/TextView;

.field private animSubTitle2:Landroid/widget/TextView;

.field private animTitle:Landroid/widget/TextView;

.field private callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

.field private clickCount:I

.field private clickTime:J

.field public closeContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e023b
    .end annotation
.end field

.field public closeContainerScrim:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e023a
    .end annotation
.end field

.field public closeHalo:Lcom/navdy/hud/app/ui/component/HaloView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e023d
    .end annotation
.end field

.field public closeIconContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e023c
    .end annotation
.end field

.field private closeIconEndAction:Ljava/lang/Runnable;

.field private closeMenuAnimationActive:Z

.field private closeMenuHideListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private closeMenuShowListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

.field private decelerateInterpolator:Landroid/view/animation/Interpolator;

.field private fadeOutRunnable:Ljava/lang/Runnable;

.field private final fastScrollCloseWaitPeriod:I

.field public fastScrollContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0237
    .end annotation
.end field

.field private fastScrollCurrentItem:I

.field private fastScrollIn:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field private fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

.field private fastScrollOffset:I

.field private fastScrollOut:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

.field public fastScrollText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0238
    .end annotation
.end field

.field private fastScrollTimeout:Ljava/lang/Runnable;

.field private handler:Landroid/os/Handler;

.field public indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0239
    .end annotation
.end field

.field private lastUpEvent:J

.field public leftContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e022e
    .end annotation
.end field

.field private linearInterpolator:Landroid/view/animation/Interpolator;

.field private listLoaded:Z

.field private overrideDefaultKeyEvents:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private parentContainer:Landroid/view/ViewGroup;

.field public recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0235
    .end annotation
.end field

.field public rightContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0234
    .end annotation
.end field

.field private scrollDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;

.field public selectedCustomView:Landroid/widget/FrameLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0233
    .end annotation
.end field

.field public selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0230
    .end annotation
.end field

.field public selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0231
    .end annotation
.end field

.field public selectedImage:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e022f
    .end annotation
.end field

.field public selectedText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0232
    .end annotation
.end field

.field private stoppingFastScrolling:Z

.field private sublistAnimationCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public toolTip:Lcom/navdy/hud/app/view/ToolTipView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0236
    .end annotation
.end field

.field public verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

.field private vlistCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 59
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-string v2, "VerticalMenuC"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 69
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->FAST_SCROLL_TRIGGER_TIME:I

    .line 101
    const/4 v1, 0x2

    new-array v1, v1, [I

    sput-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->location:[I

    .line 104
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 105
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b01e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerHeight:I

    .line 106
    const v1, 0x7f0b01ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrollY:I

    .line 107
    const v1, 0x7f0b01ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicatorY:I

    .line 108
    const v1, 0x7f0b01e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    .line 109
    const v1, 0x7f0d000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeHaloColor:I

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;Z)V
    .locals 10
    .param p1, "root"    # Landroid/view/ViewGroup;
    .param p2, "callback"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    .param p3, "allowTwoLineTitles"    # Z

    .prologue
    const v9, 0x7f0c0032

    const/4 v8, 0x4

    const/4 v7, -0x2

    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->linearInterpolator:Landroid/view/animation/Interpolator;

    .line 196
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->decelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 197
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->accelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 215
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->overrideDefaultKeyEvents:Ljava/util/HashSet;

    .line 217
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$1;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuShowListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 232
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$2;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIn:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 240
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$3;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollOut:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 249
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$4;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollTimeout:Ljava/lang/Runnable;

    .line 256
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$5;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$5;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuHideListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    .line 266
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->vlistCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    .line 294
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    .line 318
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$8;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$8;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeOutRunnable:Ljava/lang/Runnable;

    .line 326
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$9;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$9;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeIconEndAction:Ljava/lang/Runnable;

    .line 333
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    .line 344
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sublistAnimationCache:Ljava/util/HashMap;

    .line 349
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 350
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 353
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->isVerticalListNoCloseTimeout()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCloseWaitPeriod:I

    .line 354
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    .line 355
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 356
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->parentContainer:Landroid/view/ViewGroup;

    .line 357
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    sget v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setY(F)V

    .line 358
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    sget v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicatorY:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setY(F)V

    .line 359
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeoutIndicator(I)V

    .line 360
    new-instance v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->vlistCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->containerCallback:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;-><init>(Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    .line 361
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeHalo:Lcom/navdy/hud/app/ui/component/HaloView;

    sget v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeHaloColor:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/HaloView;->setStrokeColor(I)V

    .line 362
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->toolTip:Lcom/navdy/hud/app/view/ToolTipView;

    sget v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->mainIconSize:I

    sget v2, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->iconMargin:I

    invoke-virtual {v0, v1, v2, v8}, Lcom/navdy/hud/app/view/ToolTipView;->setParams(III)V

    .line 364
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 365
    .local v6, "context":Landroid/content/Context;
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    .line 366
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->parentContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    sget v3, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedIconSize:I

    sget v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedIconSize:I

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 369
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    .line 370
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    const v1, 0x7f0c0034

    invoke-virtual {v0, v6, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 371
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->parentContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 374
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    .line 375
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v6, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 376
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->parentContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    .line 380
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    invoke-virtual {v0, v6, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 381
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->parentContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 383
    return-void

    .line 353
    .end local v6    # "context":Landroid/content/Context;
    :cond_2
    const/16 v0, 0x96

    goto/16 :goto_0
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuAnimationActive:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollTimeout:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->stoppingFastScrolling:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->startFadeOut()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->stopFastScrolling()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    return-object v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->listLoaded:Z

    return p1
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollVisible()Z

    move-result v0

    return v0
.end method

.method private addToCache(Landroid/widget/ImageView;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 4
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    .line 1003
    if-nez p2, :cond_1

    .line 1014
    :cond_0
    :goto_0
    return-void

    .line 1006
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 1007
    .local v1, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    if-eqz v1, :cond_0

    .line 1009
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1010
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1011
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sublistAnimationCache:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private changeScrollIndex(I)V
    .locals 8
    .param p1, "n"    # I

    .prologue
    .line 1082
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollTimeout:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1083
    iget v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCurrentItem:I

    add-int v1, v4, p1

    .line 1084
    .local v1, "newIndex":I
    if-ltz v1, :cond_1

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    iget v4, v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->length:I

    if-ge v1, v4, :cond_1

    .line 1086
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getTitle(I)Ljava/lang/String;

    move-result-object v3

    .line 1087
    .local v3, "title":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollText:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1088
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCurrentItem:I

    .line 1090
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    invoke-virtual {v4, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getPosition(Ljava/lang/String;)I

    move-result v2

    .line 1091
    .local v2, "pos":I
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v4, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    .line 1101
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollTimeout:Ljava/lang/Runnable;

    const-wide/16 v6, 0x320

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1102
    return-void

    .line 1094
    .end local v2    # "pos":I
    .end local v3    # "title":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    iget v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCurrentItem:I

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getTitle(I)Ljava/lang/String;

    move-result-object v3

    .line 1095
    .restart local v3    # "title":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    invoke-virtual {v4, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getPosition(Ljava/lang/String;)I

    move-result v2

    .line 1096
    .restart local v2    # "pos":I
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getCurrentItem()I

    move-result v0

    .line 1097
    .local v0, "current":I
    if-eq v2, v0, :cond_0

    .line 1098
    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v4, v2}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setCurrentItem(I)V

    goto :goto_0
.end method

.method private checkFastScroll(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;IJ)Z
    .locals 9
    .param p1, "direction"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;
    .param p2, "changeIndex"    # I
    .param p3, "now"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1109
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->stoppingFastScrolling:Z

    if-eqz v3, :cond_0

    .line 1147
    :goto_0
    return v1

    .line 1112
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1113
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->changeScrollIndex(I)V

    goto :goto_0

    .line 1116
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->scrollDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;

    if-ne v3, p1, :cond_5

    .line 1117
    iget-wide v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickTime:J

    sub-long v4, p3, v4

    sget v3, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->FAST_SCROLL_TRIGGER_TIME:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 1118
    iput-wide p3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickTime:J

    .line 1119
    iput v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickCount:I

    :cond_2
    :goto_1
    move v1, v2

    .line 1147
    goto :goto_0

    .line 1122
    :cond_3
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickCount:I

    .line 1123
    iget v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickCount:I

    const/16 v4, 0xa

    if-lt v3, v4, :cond_2

    .line 1126
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v0

    .line 1127
    .local v0, "currentPos":I
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getIndexForPosition(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCurrentItem:I

    .line 1129
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->scrollDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;

    sget-object v4, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;->DOWN:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCurrentItem:I

    iget-object v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getEntryCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_4

    .line 1130
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "checkFastScroll: not allowed,last element"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1131
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickCount:I

    .line 1132
    iput-wide p3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickTime:J

    move v1, v2

    .line 1133
    goto :goto_0

    .line 1135
    :cond_4
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->startFastScrolling()V

    .line 1136
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeinIndicator(I)V

    .line 1137
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeOutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1142
    .end local v0    # "currentPos":I
    :cond_5
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->scrollDirection:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;

    .line 1143
    iput v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickCount:I

    .line 1144
    iput-wide p3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clickTime:J

    goto :goto_1
.end method

.method private getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    .line 1017
    if-nez p1, :cond_0

    .line 1018
    const/4 v0, 0x0

    .line 1020
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sublistAnimationCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private hideCloseMenu()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 455
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isCloseMenuVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    sget-object v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "hideCloseMenu"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 457
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 458
    .local v6, "set":Landroid/animation/AnimatorSet;
    const-wide/16 v0, 0x96

    invoke-virtual {v6, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 459
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->linearInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 460
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuHideListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 461
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    sget-object v3, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v5, v2, [F

    sget v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerHeight:I

    neg-int v7, v7

    int-to-float v7, v7

    aput v7, v5, v4

    .line 462
    invoke-static {v1, v3, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrim:Landroid/view/View;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v2, [F

    aput v8, v5, v4

    .line 463
    invoke-static {v1, v3, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    sget-object v5, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v7, v2, [F

    aput v8, v7, v4

    .line 464
    invoke-static {v3, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    sget-object v5, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v7, v2, [F

    aput v8, v7, v4

    .line 465
    invoke-static {v3, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    sget-object v5, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v7, v2, [F

    sget v8, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicatorY:I

    int-to-float v8, v8

    aput v8, v7, v4

    .line 466
    invoke-static {v3, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v0, v1

    .line 461
    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 468
    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuAnimationActive:Z

    .line 469
    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    .line 470
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v1

    const/16 v3, 0x96

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(IZIZZ)V

    .line 472
    .end local v6    # "set":Landroid/animation/AnimatorSet;
    :cond_0
    return-void
.end method

.method private isFastScrollAvailable()Z
    .locals 1

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFastScrollVisible()Z
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private selectFastScroll()V
    .locals 2

    .prologue
    .line 1151
    sget-object v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "selectFastScroll"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1152
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1153
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollTimeout:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1154
    return-void
.end method

.method private showCloseMenu()V
    .locals 11

    .prologue
    const/16 v3, 0x96

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 434
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isCloseMenuVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    sget-object v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showCloseMenu"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 436
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 437
    .local v6, "set":Landroid/animation/AnimatorSet;
    const-wide/16 v0, 0x96

    invoke-virtual {v6, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 438
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->linearInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 439
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuShowListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 440
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    sget-object v5, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v7, v4, [F

    const/4 v8, 0x0

    aput v8, v7, v2

    .line 441
    invoke-static {v1, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrim:Landroid/view/View;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v7, v4, [F

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v7, v2

    .line 442
    invoke-static {v1, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    sget-object v7, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v8, v4, [F

    sget v9, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrollY:I

    int-to-float v9, v9

    aput v9, v8, v2

    .line 443
    invoke-static {v5, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v0, v1

    const/4 v1, 0x3

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    sget-object v7, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v8, v4, [F

    sget v9, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrollY:I

    int-to-float v9, v9

    aput v9, v8, v2

    .line 444
    invoke-static {v5, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v0, v1

    const/4 v1, 0x4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    sget-object v7, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v8, v4, [F

    sget v9, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicatorY:I

    sget v10, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrollY:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    aput v9, v8, v2

    .line 445
    invoke-static {v5, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    aput-object v5, v0, v1

    .line 440
    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 447
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuAnimationActive:Z

    .line 448
    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    .line 449
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getRawPosition()I

    move-result v1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->animate(IZIZZ)V

    .line 450
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeoutIndicator(I)V

    .line 452
    .end local v6    # "set":Landroid/animation/AnimatorSet;
    :cond_0
    return-void
.end method

.method private startFadeOut()V
    .locals 4

    .prologue
    .line 663
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeOutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 664
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeOutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 665
    return-void
.end method

.method private startFastScrolling()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1047
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1059
    :goto_0
    return-void

    .line 1050
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->lock()V

    .line 1051
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    iget v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCurrentItem:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getTitle(I)Ljava/lang/String;

    move-result-object v0

    .line 1052
    .local v0, "title":Ljava/lang/String;
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startFastScrolling:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1053
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 1054
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1055
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1056
    iput-boolean v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->stoppingFastScrolling:Z

    .line 1057
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    invoke-interface {v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->onFastScrollStart()V

    .line 1058
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIn:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private stopFastScrolling()V
    .locals 6

    .prologue
    .line 1062
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1079
    :goto_0
    return-void

    .line 1065
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1066
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getPosition(Ljava/lang/String;)I

    move-result v0

    .line 1067
    .local v0, "pos":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->isFirstEntryBlank()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1068
    add-int/lit8 v0, v0, 0x1

    .line 1070
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    if-eqz v2, :cond_2

    .line 1071
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getOffset()I

    move-result v2

    add-int/2addr v0, v2

    .line 1073
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopFastScrolling:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1074
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->onFastScrollEnd()V

    .line 1075
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->stoppingFastScrolling:Z

    .line 1076
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock()V

    .line 1077
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->scrollToPosition(I)V

    .line 1078
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x32

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollOut:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method


# virtual methods
.method public animateIn(Landroid/animation/Animator$AnimatorListener;)V
    .locals 13
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 479
    sget-object v5, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "animateIn"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 480
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 481
    .local v4, "set":Landroid/animation/AnimatorSet;
    const-wide/16 v6, 0x96

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 482
    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->decelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 483
    if-eqz p1, :cond_0

    .line 484
    invoke-virtual {v4, p1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 486
    :cond_0
    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v10, [F

    sget v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    int-to-float v7, v7

    aput v7, v6, v8

    aput v11, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 487
    .local v0, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v9, [F

    aput v12, v6, v8

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 488
    .local v1, "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v10, [F

    sget v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    int-to-float v7, v7

    aput v7, v6, v8

    aput v11, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 489
    .local v2, "p3":Landroid/animation/PropertyValuesHolder;
    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v9, [F

    aput v12, v6, v8

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 490
    .local v3, "p4":Landroid/animation/PropertyValuesHolder;
    new-array v5, v10, [Landroid/animation/Animator;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    new-array v7, v10, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v7, v8

    aput-object v1, v7, v9

    .line 491
    invoke-static {v6, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    new-array v7, v10, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v7, v8

    aput-object v3, v7, v9

    .line 492
    invoke-static {v6, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    aput-object v6, v5, v9

    .line 490
    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 494
    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    .line 495
    return-void
.end method

.method public animateOut(Landroid/animation/Animator$AnimatorListener;)V
    .locals 12
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 498
    sget-object v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "animateOut"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 499
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setVisibility(I)V

    .line 500
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 501
    .local v6, "set":Landroid/animation/AnimatorSet;
    const-wide/16 v8, 0x96

    invoke-virtual {v6, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 502
    iget-object v7, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->accelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 503
    if-eqz p1, :cond_0

    .line 504
    invoke-virtual {v6, p1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 506
    :cond_0
    sget-object v7, Landroid/view/View;->Y:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getY()F

    move-result v10

    sget v11, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    aput v10, v8, v9

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 507
    .local v0, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v7, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 508
    .local v1, "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v7, Landroid/view/View;->Y:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getY()F

    move-result v10

    sget v11, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    aput v10, v8, v9

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 509
    .local v2, "p3":Landroid/animation/PropertyValuesHolder;
    sget-object v7, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 510
    .local v3, "p4":Landroid/animation/PropertyValuesHolder;
    sget-object v7, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    sget v10, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    int-to-float v10, v10

    aput v10, v8, v9

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 511
    .local v4, "p5":Landroid/animation/PropertyValuesHolder;
    sget-object v7, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 512
    .local v5, "p6":Landroid/animation/PropertyValuesHolder;
    const/4 v7, 0x3

    new-array v7, v7, [Landroid/animation/Animator;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    const/4 v10, 0x2

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    const/4 v11, 0x1

    aput-object v1, v10, v11

    .line 513
    invoke-static {v9, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    const/4 v10, 0x2

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v2, v10, v11

    const/4 v11, 0x1

    aput-object v3, v10, v11

    .line 514
    invoke-static {v9, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    const/4 v10, 0x2

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    const/4 v11, 0x1

    aput-object v5, v10, v11

    .line 515
    invoke-static {v9, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v7, v8

    .line 512
    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 518
    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    .line 519
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sublistAnimationCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1026
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock(Z)V

    .line 1027
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->clearAllAnimations()V

    .line 1028
    return-void
.end method

.method public fadeinIndicator(I)V
    .locals 4
    .param p1, "duration"    # I

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 641
    sget-object v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fadeinIndicator:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 642
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 643
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setAlpha(F)V

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 645
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public fadeoutIndicator(I)V
    .locals 4
    .param p1, "duration"    # I

    .prologue
    const/4 v1, 0x0

    .line 653
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 654
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setAlpha(F)V

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 656
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public getFastScrollIndex()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 1

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    return-object v0
.end method

.method public getSelectedCustomImage()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public handleGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v0, 0x0

    .line 522
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->listLoaded:Z

    if-nez v1, :cond_0

    .line 523
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "list not loaded yet"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 537
    :goto_0
    return v0

    .line 526
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuAnimationActive:Z

    if-eqz v1, :cond_1

    .line 527
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "close menu animation is active, no-op"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 530
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$14;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 532
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->close()V

    .line 533
    const/4 v0, 0x1

    goto :goto_0

    .line 530
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 14
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 541
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->listLoaded:Z

    if-nez v2, :cond_0

    .line 542
    sget-object v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "list not loaded yet"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 543
    const/4 v2, 0x0

    .line 635
    :goto_0
    return v2

    .line 545
    :cond_0
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuAnimationActive:Z

    if-eqz v2, :cond_1

    .line 546
    sget-object v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "close menu animation is active, no-op"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 547
    const/4 v2, 0x0

    goto :goto_0

    .line 549
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 550
    .local v10, "now":J
    sget-object v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$14;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 632
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->overrideDefaultKeyEvents:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 633
    const/4 v2, 0x1

    goto :goto_0

    .line 552
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v2

    if-nez v2, :cond_5

    .line 554
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->up()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    move-result-object v8

    .line 555
    .local v8, "keyHandlerState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    iget-boolean v2, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->keyHandled:Z

    if-eqz v2, :cond_2

    .line 556
    iput-wide v10, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->lastUpEvent:J

    .line 557
    const/4 v2, 0x1

    goto :goto_0

    .line 560
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v12, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->lastUpEvent:J

    sub-long v6, v4, v12

    .line 561
    .local v6, "diff":J
    iget v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollCloseWaitPeriod:I

    int-to-long v4, v2

    cmp-long v2, v6, v4

    if-gez v2, :cond_3

    .line 563
    sget-object v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "user scrolled fast"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 564
    iput-wide v10, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->lastUpEvent:J

    .line 565
    const/4 v2, 0x1

    goto :goto_0

    .line 567
    :cond_3
    iput-wide v10, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->lastUpEvent:J

    .line 568
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->showCloseMenu()V

    .line 581
    .end local v6    # "diff":J
    :cond_4
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 570
    .end local v8    # "keyHandlerState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    :cond_5
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollAvailable()Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;->UP:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;

    const/4 v4, -0x1

    invoke-direct {p0, v2, v4, v10, v11}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->checkFastScroll(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;IJ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 571
    const/4 v2, 0x1

    goto :goto_0

    .line 574
    :cond_6
    iput-wide v10, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->lastUpEvent:J

    .line 575
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->up()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    move-result-object v8

    .line 576
    .restart local v8    # "keyHandlerState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    iget-boolean v2, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    if-eqz v2, :cond_4

    .line 577
    const/16 v2, 0x1f4

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeinIndicator(I)V

    .line 578
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->startFadeOut()V

    goto :goto_1

    .line 584
    .end local v8    # "keyHandlerState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isCloseMenuVisible()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 585
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->hideCloseMenu()V

    .line 598
    :cond_7
    :goto_2
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 587
    :cond_8
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollAvailable()Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;->DOWN:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;

    const/4 v4, 0x1

    invoke-direct {p0, v2, v4, v10, v11}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->checkFastScroll(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Direction;IJ)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 588
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 591
    :cond_9
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->hideToolTip()V

    .line 592
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->down()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;

    move-result-object v8

    .line 593
    .restart local v8    # "keyHandlerState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    iget-boolean v2, v8, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;->listMoved:Z

    if-eqz v2, :cond_7

    .line 594
    const/16 v2, 0x1f4

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fadeinIndicator(I)V

    .line 595
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->startFadeOut()V

    goto :goto_2

    .line 601
    .end local v8    # "keyHandlerState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$KeyHandlerState;
    :pswitch_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isCloseMenuVisible()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    invoke-interface {v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->isClosed()Z

    move-result v2

    if-nez v2, :cond_a

    .line 602
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeHalo:Lcom/navdy/hud/app/ui/component/HaloView;

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 603
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeIconContainer:Landroid/view/ViewGroup;

    const/16 v4, 0x32

    iget-object v5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeIconEndAction:Ljava/lang/Runnable;

    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->performClick(Landroid/view/View;ILjava/lang/Runnable;)V

    .line 629
    :goto_3
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 605
    :cond_a
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v3

    .line 606
    .local v3, "pos":I
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentModel()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v1

    .line 607
    .local v1, "current":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    if-nez v1, :cond_b

    .line 608
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 610
    :cond_b
    iget v2, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    packed-switch v2, :pswitch_data_1

    .line 616
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollAvailable()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollVisible()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 617
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectFastScroll()V

    .line 618
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 613
    :pswitch_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 621
    :cond_c
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getItemSelectionState()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    move-result-object v0

    .line 622
    .local v0, "itemSelectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    iget v2, v1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->id:I

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->set(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;IIII)V

    .line 623
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    invoke-interface {v2, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 624
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->select()V

    goto :goto_3

    .line 626
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 635
    .end local v0    # "itemSelectionState":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;
    .end local v1    # "current":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v3    # "pos":I
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 550
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 610
    :pswitch_data_1
    .packed-switch 0x7f0e008d
        :pswitch_4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public hideToolTip()V
    .locals 1

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->toolTip:Lcom/navdy/hud/app/view/ToolTipView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/ToolTipView;->hide()V

    .line 1162
    return-void
.end method

.method public isCloseMenuVisible()Z
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performBackAnimation(Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V
    .locals 24
    .param p1, "startAction"    # Ljava/lang/Runnable;
    .param p2, "endAction"    # Ljava/lang/Runnable;
    .param p3, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p4, "xOffset"    # I

    .prologue
    .line 737
    sget-object v19, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v20, "performBackAnimation"

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 740
    sget v19, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedIconSize:I

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const v20, 0x3e4cccd0    # 0.20000005f

    mul-float v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    div-float v4, v19, v20

    .line 741
    .local v4, "adjustment":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x3f99999a    # 1.2f

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x3f99999a    # 1.2f

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 743
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/ViewGroup;->getX()F

    move-result v20

    add-float v20, v20, v4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setX(F)V

    .line 744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/ViewGroup;->getY()F

    move-result v20

    add-float v20, v20, v4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setY(F)V

    .line 746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->getVisibility()I

    move-result v19

    if-nez v19, :cond_5

    .line 747
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 752
    .local v8, "from":Landroid/widget/ImageView;
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 753
    .local v6, "cacheBitmap":Landroid/graphics/Bitmap;
    if-nez v6, :cond_6

    .line 754
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v8, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->copyImage(Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->addToCache(Landroid/widget/ImageView;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    .line 759
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 762
    const/16 v7, 0x96

    .line 765
    .local v7, "duration":I
    sget-object v19, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/high16 v22, 0x3f800000    # 1.0f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    .line 766
    .local v12, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v19, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/high16 v22, 0x3f800000    # 1.0f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    .line 767
    .local v13, "p2":Landroid/animation/PropertyValuesHolder;
    sget-object v19, Landroid/view/View;->X:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sget v22, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedImageX:I

    add-int v22, v22, p4

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v14

    .line 768
    .local v14, "p3":Landroid/animation/PropertyValuesHolder;
    sget-object v19, Landroid/view/View;->Y:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sget v22, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedImageY:I

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v15

    .line 769
    .local v15, "p4":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    const/16 v21, 0x2

    aput-object v14, v20, v21

    const/16 v21, 0x3

    aput-object v15, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 770
    .local v5, "animatorImage":Landroid/animation/ObjectAnimator;
    int-to-long v0, v7

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 771
    new-instance v19, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Ljava/lang/Runnable;)V

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 795
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    .line 798
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    sget v20, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedTextX:I

    sget v21, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    sub-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setX(F)V

    .line 799
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    move-object/from16 v19, v0

    if-nez v19, :cond_0

    .line 800
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->allowsTwoLineTitles()Z

    move-result v19

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setFontSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    .line 802
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextSize(F)V

    .line 803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setAlpha(F)V

    .line 804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 807
    const/4 v9, 0x0

    .line 808
    .local v9, "hasSubtitle":Z
    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    move-object/from16 v19, v0

    if-nez v19, :cond_7

    .line 809
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 820
    :goto_2
    const/4 v10, 0x0

    .line 821
    .local v10, "hasSubtitle2":Z
    if-eqz p3, :cond_2

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    move-object/from16 v19, v0

    if-nez v19, :cond_8

    .line 822
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 832
    :goto_3
    new-instance v18, Landroid/animation/AnimatorSet;

    invoke-direct/range {v18 .. v18}, Landroid/animation/AnimatorSet;-><init>()V

    .line 833
    .local v18, "textSet":Landroid/animation/AnimatorSet;
    sget-object v19, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/high16 v22, 0x3f800000    # 1.0f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    .line 834
    sget-object v19, Landroid/view/View;->X:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getX()F

    move-result v22

    sget v23, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    add-float v22, v22, v23

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    .line 835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v17

    .line 838
    .local v17, "textBuilder":Landroid/animation/AnimatorSet$Builder;
    if-eqz v9, :cond_3

    .line 839
    sget-object v19, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/high16 v22, 0x3f800000    # 1.0f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    .line 840
    sget-object v19, Landroid/view/View;->X:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getX()F

    move-result v22

    sget v23, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    add-float v22, v22, v23

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    .line 841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 845
    :cond_3
    if-eqz v10, :cond_4

    .line 846
    sget-object v19, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/high16 v22, 0x3f800000    # 1.0f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    .line 847
    sget-object v19, Landroid/view/View;->X:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getX()F

    move-result v22

    sget v23, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    add-float v22, v22, v23

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    .line 848
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 851
    :cond_4
    const v19, 0x3f2aaaab

    int-to-float v0, v7

    move/from16 v20, v0

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 852
    const/16 v19, 0x32

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 853
    invoke-virtual/range {v18 .. v18}, Landroid/animation/AnimatorSet;->start()V

    .line 856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    move-object/from16 v19, v0

    sget-object v20, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v23, v21, v22

    invoke-static/range {v19 .. v21}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 857
    .local v11, "mainTitle":Landroid/animation/ObjectAnimator;
    const/16 v19, 0x32

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v11, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 858
    invoke-virtual {v11}, Landroid/animation/ObjectAnimator;->start()V

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/ViewGroup;->setPivotX(F)V

    .line 862
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;->getMeasuredHeight()I

    move-result v20

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/view/ViewGroup;->setPivotY(F)V

    .line 863
    new-instance v16, Landroid/animation/AnimatorSet;

    invoke-direct/range {v16 .. v16}, Landroid/animation/AnimatorSet;-><init>()V

    .line 864
    .local v16, "rightAnimator":Landroid/animation/AnimatorSet;
    sget-object v19, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/high16 v22, 0x3f000000    # 0.5f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    .line 865
    sget-object v19, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/high16 v22, 0x3f000000    # 0.5f

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    .line 866
    sget-object v19, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    aput v22, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v14

    .line 867
    const v19, 0x3f2aaaab

    int-to-float v0, v7

    move/from16 v20, v0

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 868
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v12, v20, v21

    const/16 v21, 0x1

    aput-object v13, v20, v21

    const/16 v21, 0x2

    aput-object v14, v20, v21

    invoke-static/range {v19 .. v20}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 869
    new-instance v19, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$12;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$12;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Ljava/lang/Runnable;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 880
    invoke-virtual/range {v16 .. v16}, Landroid/animation/AnimatorSet;->start()V

    .line 881
    return-void

    .line 749
    .end local v5    # "animatorImage":Landroid/animation/ObjectAnimator;
    .end local v6    # "cacheBitmap":Landroid/graphics/Bitmap;
    .end local v7    # "duration":I
    .end local v8    # "from":Landroid/widget/ImageView;
    .end local v9    # "hasSubtitle":Z
    .end local v10    # "hasSubtitle2":Z
    .end local v11    # "mainTitle":Landroid/animation/ObjectAnimator;
    .end local v12    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v13    # "p2":Landroid/animation/PropertyValuesHolder;
    .end local v14    # "p3":Landroid/animation/PropertyValuesHolder;
    .end local v15    # "p4":Landroid/animation/PropertyValuesHolder;
    .end local v16    # "rightAnimator":Landroid/animation/AnimatorSet;
    .end local v17    # "textBuilder":Landroid/animation/AnimatorSet$Builder;
    .end local v18    # "textSet":Landroid/animation/AnimatorSet;
    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .restart local v8    # "from":Landroid/widget/ImageView;
    goto/16 :goto_0

    .line 757
    .restart local v6    # "cacheBitmap":Landroid/graphics/Bitmap;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 811
    .restart local v5    # "animatorImage":Landroid/animation/ObjectAnimator;
    .restart local v7    # "duration":I
    .restart local v9    # "hasSubtitle":Z
    .restart local v12    # "p1":Landroid/animation/PropertyValuesHolder;
    .restart local v13    # "p2":Landroid/animation/PropertyValuesHolder;
    .restart local v14    # "p3":Landroid/animation/PropertyValuesHolder;
    .restart local v15    # "p4":Landroid/animation/PropertyValuesHolder;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    sget v20, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedTextX:I

    sget v21, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    sub-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setX(F)V

    .line 812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextSize(F)V

    .line 814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setAlpha(F)V

    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 816
    const/4 v9, 0x1

    goto/16 :goto_2

    .line 824
    .restart local v10    # "hasSubtitle2":Z
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    sget v20, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedTextX:I

    sget v21, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    sub-int v20, v20, v21

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setX(F)V

    .line 825
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->subTitle2:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 826
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextSize(F)V

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setAlpha(F)V

    .line 828
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 829
    const/4 v10, 0x1

    goto/16 :goto_3
.end method

.method public performEnterAnimation(Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V
    .locals 22
    .param p1, "startAction"    # Ljava/lang/Runnable;
    .param p2, "endAction"    # Ljava/lang/Runnable;
    .param p3, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .prologue
    .line 887
    sget-object v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "performEnterAnimation"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 888
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    sget-object v5, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->location:[I

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 889
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 890
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAlpha(F)V

    .line 891
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;->setAlpha(F)V

    .line 897
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 898
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 899
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 900
    const/4 v12, 0x0

    .line 901
    .local v12, "cacheBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    sget-object v5, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->ICON_OPTIONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    if-eq v4, v5, :cond_0

    .line 902
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->getFromCache(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 904
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle2:Landroid/widget/TextView;

    if-nez v12, :cond_3

    const/4 v9, 0x1

    :goto_0
    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->copyAndPosition(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Z)V

    .line 905
    if-eqz v12, :cond_4

    .line 906
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 910
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 911
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAlpha(F)V

    .line 912
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 913
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 914
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAlpha(F)V

    .line 915
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 917
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 919
    const/16 v13, 0x96

    .line 922
    .local v13, "duration":I
    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const v7, 0x3f99999a    # 1.2f

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v15

    .line 923
    .local v15, "p1":Landroid/animation/PropertyValuesHolder;
    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const v7, 0x3f99999a    # 1.2f

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v16

    .line 924
    .local v16, "p2":Landroid/animation/PropertyValuesHolder;
    sget v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->selectedIconSize:I

    int-to-float v4, v4

    const v5, -0x41b33330    # -0.20000005f

    mul-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v10, v4, v5

    .line 925
    .local v10, "adjustment":F
    sget-object v4, Landroid/view/View;->X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->location:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    int-to-float v7, v7

    sub-float/2addr v7, v10

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v17

    .line 926
    .local v17, "p3":Landroid/animation/PropertyValuesHolder;
    sget-object v4, Landroid/view/View;->Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    sget-object v7, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->location:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    sget v8, Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;->rootTopOffset:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    sub-float/2addr v7, v10

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v18

    .line 927
    .local v18, "p4":Landroid/animation/PropertyValuesHolder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    const/4 v6, 0x1

    aput-object v16, v5, v6

    const/4 v6, 0x2

    aput-object v17, v5, v6

    const/4 v6, 0x3

    aput-object v18, v5, v6

    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 928
    .local v11, "animatorImage":Landroid/animation/ObjectAnimator;
    int-to-long v4, v13

    invoke-virtual {v11, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 929
    new-instance v4, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v4, v0, v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$13;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    invoke-virtual {v11, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 958
    invoke-virtual {v11}, Landroid/animation/ObjectAnimator;->start()V

    .line 962
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    sget-object v5, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->location:[I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 963
    new-instance v21, Landroid/animation/AnimatorSet;

    invoke-direct/range {v21 .. v21}, Landroid/animation/AnimatorSet;-><init>()V

    .line 964
    .local v21, "textSet":Landroid/animation/AnimatorSet;
    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v15

    .line 965
    sget-object v4, Landroid/view/View;->X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getX()F

    move-result v7

    sget v8, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v16

    .line 966
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animTitle:Landroid/widget/TextView;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    const/4 v6, 0x1

    aput-object v16, v5, v6

    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v20

    .line 969
    .local v20, "textBuilder":Landroid/animation/AnimatorSet$Builder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 970
    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v15

    .line 971
    sget-object v4, Landroid/view/View;->X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getX()F

    move-result v7

    sget v8, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateTranslateY:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v16

    .line 972
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animSubTitle:Landroid/widget/TextView;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    const/4 v6, 0x1

    aput-object v16, v5, v6

    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 974
    :cond_2
    const/16 v4, 0x32

    int-to-long v4, v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 975
    invoke-virtual/range {v21 .. v21}, Landroid/animation/AnimatorSet;->start()V

    .line 978
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v14

    .line 979
    .local v14, "mainTitle":Landroid/animation/ObjectAnimator;
    const/16 v4, 0x32

    int-to-long v4, v4

    invoke-virtual {v14, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 980
    const v4, 0x3f2aaaab

    int-to-float v5, v13

    mul-float/2addr v4, v5

    float-to-int v4, v4

    int-to-long v4, v4

    invoke-virtual {v14, v4, v5}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 981
    invoke-virtual {v14}, Landroid/animation/ObjectAnimator;->start()V

    .line 984
    new-instance v19, Landroid/animation/AnimatorSet;

    invoke-direct/range {v19 .. v19}, Landroid/animation/AnimatorSet;-><init>()V

    .line 985
    .local v19, "rightAnimator":Landroid/animation/AnimatorSet;
    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v15

    .line 986
    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v16

    .line 987
    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v17

    .line 988
    const/16 v4, 0x32

    int-to-long v4, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 989
    const v4, 0x3f2aaaab

    int-to-float v5, v13

    mul-float/2addr v4, v5

    float-to-int v4, v4

    int-to-long v4, v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 990
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    const/4 v6, 0x0

    aput-object v15, v5, v6

    const/4 v6, 0x1

    aput-object v16, v5, v6

    const/4 v6, 0x2

    aput-object v17, v5, v6

    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 991
    invoke-virtual/range {v19 .. v19}, Landroid/animation/AnimatorSet;->start()V

    .line 992
    return-void

    .line 904
    .end local v10    # "adjustment":F
    .end local v11    # "animatorImage":Landroid/animation/ObjectAnimator;
    .end local v13    # "duration":I
    .end local v14    # "mainTitle":Landroid/animation/ObjectAnimator;
    .end local v15    # "p1":Landroid/animation/PropertyValuesHolder;
    .end local v16    # "p2":Landroid/animation/PropertyValuesHolder;
    .end local v17    # "p3":Landroid/animation/PropertyValuesHolder;
    .end local v18    # "p4":Landroid/animation/PropertyValuesHolder;
    .end local v19    # "rightAnimator":Landroid/animation/AnimatorSet;
    .end local v20    # "textBuilder":Landroid/animation/AnimatorSet$Builder;
    .end local v21    # "textSet":Landroid/animation/AnimatorSet;
    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 908
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animImageView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v4, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->addToCache(Landroid/widget/ImageView;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto/16 :goto_1
.end method

.method public performSelectionAnimation(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 709
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 710
    return-void
.end method

.method public performSelectionAnimation(Ljava/lang/Runnable;I)V
    .locals 8
    .param p1, "endAction"    # Ljava/lang/Runnable;
    .param p2, "startDelay"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 713
    sget-object v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "performSelectionAnimation"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 714
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 715
    .local v1, "set":Landroid/animation/AnimatorSet;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 716
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 717
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->addCurrentHighlightAnimation(Landroid/animation/AnimatorSet$Builder;)V

    .line 718
    const-wide/16 v2, 0x32

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 719
    new-instance v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$10;

    invoke-direct {v2, p0, p1, p2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$10;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Ljava/lang/Runnable;I)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 729
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 730
    return-void
.end method

.method public setLeftContainerWidth(I)V
    .locals 4
    .param p1, "width"    # I

    .prologue
    .line 386
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "leftContainer width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 388
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 389
    return-void
.end method

.method public setOverrideDefaultKeyEvents(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;Z)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    .param p2, "override"    # Z

    .prologue
    .line 1031
    if-eqz p2, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->overrideDefaultKeyEvents:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1036
    :goto_0
    return-void

    .line 1034
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->overrideDefaultKeyEvents:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setRightContainerWidth(I)V
    .locals 4
    .param p1, "width"    # I

    .prologue
    .line 392
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rightContainer width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 393
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 394
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 395
    return-void
.end method

.method public setSelectedIconColorImage(IILandroid/graphics/Shader;F)V
    .locals 6
    .param p1, "resourceId"    # I
    .param p2, "bkColor"    # I
    .param p3, "gradient"    # Landroid/graphics/Shader;
    .param p4, "scaleF"    # F

    .prologue
    .line 668
    sget-object v5, Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;->CIRCLE:Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setSelectedIconColorImage(IILandroid/graphics/Shader;FLcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V

    .line 669
    return-void
.end method

.method public setSelectedIconColorImage(IILandroid/graphics/Shader;FLcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V
    .locals 3
    .param p1, "resourceId"    # I
    .param p2, "bkColor"    # I
    .param p3, "gradient"    # Landroid/graphics/Shader;
    .param p4, "scaleF"    # F
    .param p5, "iconShape"    # Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;

    .prologue
    const/16 v2, 0x8

    .line 672
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 673
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 674
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, p5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIconShape(Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)V

    .line 675
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 676
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 677
    return-void
.end method

.method public setSelectedIconImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V
    .locals 3
    .param p1, "resourceId"    # I
    .param p2, "initials"    # Ljava/lang/String;
    .param p3, "style"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    .prologue
    const/16 v2, 0x8

    .line 680
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 681
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 682
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 683
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 684
    return-void
.end method

.method public setSelectedIconImage(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v3, 0x8

    .line 687
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 688
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 689
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 690
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 692
    return-void
.end method

.method public showSelectedCustomView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 698
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 699
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 700
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 701
    return-void
.end method

.method public showToolTip(ILjava/lang/String;)V
    .locals 1
    .param p1, "posIndex"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->toolTip:Lcom/navdy/hud/app/view/ToolTipView;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/view/ToolTipView;->show(ILjava/lang/String;)V

    .line 1158
    return-void
.end method

.method public unlock()V
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock()V

    .line 996
    return-void
.end method

.method public unlock(Z)V
    .locals 1
    .param p1, "startFluctuator"    # Z

    .prologue
    .line 999
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->unlock(Z)V

    .line 1000
    return-void
.end method

.method public updateView(Ljava/util/List;IZ)V
    .locals 1
    .param p2, "initialSelection"    # I
    .param p3, "firstEntryBlank"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 400
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V

    .line 401
    return-void
.end method

.method public updateView(Ljava/util/List;IZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V
    .locals 6
    .param p2, "initialSelection"    # I
    .param p3, "firstEntryBlank"    # Z
    .param p4, "scrollIndex"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;IZ",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;",
            ")V"
        }
    .end annotation

    .prologue
    .line 407
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V

    .line 408
    return-void
.end method

.method public updateView(Ljava/util/List;IZZLcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;)V
    .locals 4
    .param p2, "initialSelection"    # I
    .param p3, "firstEntryBlank"    # Z
    .param p4, "hasScrollModel"    # Z
    .param p5, "scrollIndex"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;IZZ",
            "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;",
            ")V"
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->listLoaded:Z

    .line 416
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    .line 417
    if-eqz p5, :cond_0

    .line 418
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 419
    .local v0, "n":I
    const/16 v1, 0x28

    if-lt v0, v1, :cond_1

    invoke-virtual {p5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getEntryCount()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    .line 420
    iput-object p5, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollIndex:Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    .line 421
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fast scroll available:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 426
    .end local v0    # "n":I
    :cond_0
    :goto_0
    if-nez p4, :cond_2

    .line 427
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->updateView(Ljava/util/List;IZ)V

    .line 431
    :goto_1
    return-void

    .line 423
    .restart local v0    # "n":I
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fast scroll threshold not met:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->getEntryCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 429
    .end local v0    # "n":I
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v1, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->updateViewWithScrollableContent(Ljava/util/List;IZ)V

    goto :goto_1
.end method
