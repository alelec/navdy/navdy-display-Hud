.class public Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
.super Ljava/lang/Object;
.source "VerticalFastScrollIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
    }
.end annotation


# instance fields
.field private final entries:[Ljava/lang/String;

.field public final length:I

.field private offsetIndex:[I

.field private final offsetMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final positionOffset:I


# direct methods
.method private constructor <init>([Ljava/lang/String;Ljava/util/Map;[II)V
    .locals 1
    .param p1, "entries"    # [Ljava/lang/String;
    .param p3, "offsetIndex"    # [I
    .param p4, "positionOffset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;[II)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "offsetMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->entries:[Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->offsetMap:Ljava/util/Map;

    .line 29
    iput-object p3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->offsetIndex:[I

    .line 30
    iput p4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->positionOffset:I

    .line 31
    array-length v0, p1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->length:I

    .line 32
    return-void
.end method

.method synthetic constructor <init>([Ljava/lang/String;Ljava/util/Map;[IILcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$1;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/String;
    .param p2, "x1"    # Ljava/util/Map;
    .param p3, "x2"    # [I
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$1;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;-><init>([Ljava/lang/String;Ljava/util/Map;[II)V

    return-void
.end method


# virtual methods
.method public getEntryCount()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->entries:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getIndexForPosition(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 39
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->offsetIndex:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->offsetIndex:[I

    array-length v3, v3

    invoke-static {v1, v2, v3, p1}, Ljava/util/Arrays;->binarySearch([IIII)I

    move-result v0

    .line 40
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 41
    neg-int v1, v0

    add-int/lit8 v0, v1, -0x1

    .line 42
    if-lez v0, :cond_0

    .line 43
    add-int/lit8 v0, v0, -0x1

    .line 46
    :cond_0
    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->positionOffset:I

    return v0
.end method

.method public getPosition(Ljava/lang/String;)I
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->offsetMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;->entries:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method
