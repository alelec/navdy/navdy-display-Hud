.class Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VerticalMenuComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performBackAnimation(Ljava/lang/Runnable;Ljava/lang/Runnable;Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

.field final synthetic val$endAction:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 771
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->val$endAction:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 8
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 775
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 776
    .local v1, "set":Landroid/animation/AnimatorSet;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 777
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 778
    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 779
    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 780
    new-instance v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11$1;-><init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$11;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 792
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 793
    return-void
.end method
