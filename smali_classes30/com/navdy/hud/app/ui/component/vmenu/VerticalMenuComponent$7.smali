.class Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;
.super Ljava/lang/Object;
.source "VerticalMenuComponent.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ContainerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hideToolTips()V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->hideToolTip()V

    .line 315
    return-void
.end method

.method public isCloseMenuVisible()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isCloseMenuVisible()Z

    move-result v0

    return v0
.end method

.method public isFastScrolling()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # invokes: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isFastScrollVisible()Z
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$900(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Z

    move-result v0

    return v0
.end method

.method public showToolTips()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$7;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->showToolTip()V

    .line 310
    :cond_0
    return-void
.end method
