.class Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;
.super Ljava/lang/Object;
.source "VerticalMenuComponent.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 1
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V

    .line 281
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 286
    return-void
.end method

.method public onLoad()V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->listLoaded:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$802(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Z)Z

    .line 275
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->onLoad()V

    .line 276
    return-void
.end method

.method public onScrollIdle()V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->onScrollIdle()V

    .line 291
    return-void
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$6;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;
    invoke-static {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$700(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;->select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V

    .line 270
    return-void
.end method
