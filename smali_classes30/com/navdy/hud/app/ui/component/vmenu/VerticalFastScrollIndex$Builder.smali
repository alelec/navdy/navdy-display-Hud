.class public Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
.super Ljava/lang/Object;
.source "VerticalFastScrollIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private map:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private positionOffset:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->map:Ljava/util/LinkedHashMap;

    return-void
.end method


# virtual methods
.method public build()Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;
    .locals 10

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v9

    .line 77
    .local v9, "len":I
    new-array v1, v9, [Ljava/lang/String;

    .line 78
    .local v1, "entries":[Ljava/lang/String;
    new-array v3, v9, [I

    .line 79
    .local v3, "offsetIndex":[I
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 80
    .local v7, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 81
    .local v6, "counter":I
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 83
    .local v8, "key":Ljava/lang/String;
    aput-object v8, v1, v6

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v6

    .line 85
    add-int/lit8 v6, v6, 0x1

    .line 86
    goto :goto_0

    .line 87
    .end local v8    # "key":Ljava/lang/String;
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->map:Ljava/util/LinkedHashMap;

    iget v4, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->positionOffset:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;-><init>([Ljava/lang/String;Ljava/util/Map;[IILcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$1;)V

    return-object v0
.end method

.method public positionOffset(I)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->positionOffset:I

    .line 72
    return-object p0
.end method

.method public setEntry(CI)Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;
    .locals 3
    .param p1, "ch"    # C
    .param p2, "startIndex"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex$Builder;->map:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-object p0
.end method
