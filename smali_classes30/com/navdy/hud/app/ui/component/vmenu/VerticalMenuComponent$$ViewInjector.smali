.class public Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$$ViewInjector;
.super Ljava/lang/Object;
.source "VerticalMenuComponent$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e022e

    const-string v2, "field \'leftContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    .line 12
    const v1, 0x7f0e022f

    const-string v2, "field \'selectedImage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    .line 14
    const v1, 0x7f0e0230

    const-string v2, "field \'selectedIconColorImage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 16
    const v1, 0x7f0e0231

    const-string v2, "field \'selectedIconImage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 18
    const v1, 0x7f0e0232

    const-string v2, "field \'selectedText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e0234

    const-string v2, "field \'rightContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    .line 22
    const v1, 0x7f0e0239

    const-string v2, "field \'indicator\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 24
    const v1, 0x7f0e0235

    const-string v2, "field \'recyclerView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    .line 26
    const v1, 0x7f0e0236

    const-string v2, "field \'toolTip\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/view/ToolTipView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->toolTip:Lcom/navdy/hud/app/view/ToolTipView;

    .line 28
    const v1, 0x7f0e023a

    const-string v2, "field \'closeContainerScrim\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrim:Landroid/view/View;

    .line 30
    const v1, 0x7f0e023c

    const-string v2, "field \'closeIconContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeIconContainer:Landroid/view/ViewGroup;

    .line 32
    const v1, 0x7f0e023b

    const-string v2, "field \'closeContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    .line 34
    const v1, 0x7f0e023d

    const-string v2, "field \'closeHalo\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 35
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/HaloView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeHalo:Lcom/navdy/hud/app/ui/component/HaloView;

    .line 36
    const v1, 0x7f0e0237

    const-string v2, "field \'fastScrollContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 37
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollContainer:Landroid/view/ViewGroup;

    .line 38
    const v1, 0x7f0e0238

    const-string v2, "field \'fastScrollText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollText:Landroid/widget/TextView;

    .line 40
    const v1, 0x7f0e0233

    const-string v2, "field \'selectedCustomView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/FrameLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    .line 42
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    .line 46
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedImage:Landroid/view/ViewGroup;

    .line 47
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconColorImage:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 48
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedIconImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 49
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedText:Landroid/widget/TextView;

    .line 50
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    .line 51
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->indicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    .line 52
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->recyclerView:Lcom/navdy/hud/app/ui/component/vlist/VerticalRecyclerView;

    .line 53
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->toolTip:Lcom/navdy/hud/app/view/ToolTipView;

    .line 54
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrim:Landroid/view/View;

    .line 55
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeIconContainer:Landroid/view/ViewGroup;

    .line 56
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    .line 57
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeHalo:Lcom/navdy/hud/app/ui/component/HaloView;

    .line 58
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollContainer:Landroid/view/ViewGroup;

    .line 59
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->fastScrollText:Landroid/widget/TextView;

    .line 60
    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->selectedCustomView:Landroid/widget/FrameLayout;

    .line 61
    return-void
.end method
