.class Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$5;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VerticalMenuComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$5;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/16 v2, 0x8

    .line 259
    # getter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "closeMenuHideListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$5;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeMenuAnimationActive:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->access$102(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Z)Z

    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$5;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$5;->this$0:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->closeContainerScrim:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 263
    return-void
.end method
