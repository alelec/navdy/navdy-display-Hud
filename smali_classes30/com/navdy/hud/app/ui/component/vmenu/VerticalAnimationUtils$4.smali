.class final Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;
.super Ljava/lang/Object;
.source "VerticalAnimationUtils.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils;->animateDimension(Landroid/view/View;I)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;->val$layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

    iput-object p2, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 136
    .local v0, "size":I
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;->val$layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 137
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;->val$layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 138
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/vmenu/VerticalAnimationUtils$4;->val$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 139
    return-void
.end method
