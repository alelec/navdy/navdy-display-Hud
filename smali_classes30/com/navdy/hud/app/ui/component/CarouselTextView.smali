.class public Lcom/navdy/hud/app/ui/component/CarouselTextView;
.super Landroid/widget/TextView;
.source "CarouselTextView.java"


# static fields
.field private static final CAROUSEL_ANIM_DURATION:J = 0x1f4L

.field private static final CAROUSEL_VIEW_INTERVAL:J = 0x1388L


# instance fields
.field private carouselChangeRunnable:Ljava/lang/Runnable;

.field private currentTextIndex:I

.field private final handler:Landroid/os/Handler;

.field private textList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final translationY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance v0, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/component/CarouselTextView$1;-><init>(Lcom/navdy/hud/app/ui/component/CarouselTextView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->carouselChangeRunnable:Ljava/lang/Runnable;

    .line 49
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->translationY:F

    .line 50
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->handler:Landroid/os/Handler;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/CarouselTextView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/CarouselTextView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->textList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/component/CarouselTextView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/CarouselTextView;

    .prologue
    .line 19
    iget v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->currentTextIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/ui/component/CarouselTextView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/CarouselTextView;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->currentTextIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/component/CarouselTextView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/CarouselTextView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->rotateWithAnimation(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/component/CarouselTextView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/CarouselTextView;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private rotateWithAnimation(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->translationY:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/component/CarouselTextView$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/ui/component/CarouselTextView$2;-><init>(Lcom/navdy/hud/app/ui/component/CarouselTextView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 74
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->carouselChangeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->textList:Ljava/util/List;

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->currentTextIndex:I

    .line 82
    return-void
.end method

.method public varargs setCarousel([I)V
    .locals 5
    .param p1, "resIds"    # [I

    .prologue
    .line 54
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->carouselChangeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 56
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->textList:Ljava/util/List;

    .line 58
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v0, p1, v1

    .line 59
    .local v0, "resId":I
    iget-object v3, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->textList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/component/CarouselTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 62
    .end local v0    # "resId":I
    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->currentTextIndex:I

    .line 63
    iget-object v1, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/component/CarouselTextView;->carouselChangeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 64
    return-void
.end method
