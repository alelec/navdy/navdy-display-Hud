.class public Lcom/navdy/hud/app/ui/activity/Main$Module;
.super Ljava/lang/Object;
.source "Main.java"


# annotations
.annotation runtime Ldagger/Module;
    addsTo = Lcom/navdy/hud/app/common/ProdModule;
    injects = {
        Lcom/navdy/hud/app/ui/activity/MainActivity;,
        Lcom/navdy/hud/app/view/MainView;,
        Lcom/navdy/hud/app/view/NotificationView;,
        Lcom/navdy/hud/app/presenter/NotificationPresenter;,
        Lcom/navdy/hud/app/ui/framework/UIStateManager;,
        Lcom/navdy/hud/app/view/ContainerView;,
        Lcom/navdy/hud/app/debug/GestureEngine;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/activity/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/activity/Main;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/ui/activity/Main;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/activity/Main;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/navdy/hud/app/ui/activity/Main$Module;->this$0:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
