.class synthetic Lcom/navdy/hud/app/ui/activity/Main$1;
.super Ljava/lang/Object;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/activity/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$input$Gesture:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$ui$Screen:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1639
    invoke-static {}, Lcom/navdy/hud/app/event/Shutdown$State;->values()[Lcom/navdy/hud/app/event/Shutdown$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->CONFIRMATION:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_1cd

    :goto_14
    :try_start_14
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->SHUTTING_DOWN:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/Shutdown$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_1ca

    .line 1565
    :goto_1f
    invoke-static {}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->values()[Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus:[I

    :try_start_28
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus:[I

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_33
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_33} :catch_1c7

    :goto_33
    :try_start_33
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus:[I

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_EXTREMELY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_33 .. :try_end_3e} :catch_1c4

    :goto_3e
    :try_start_3e
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus:[I

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_49
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3e .. :try_end_49} :catch_1c1

    .line 1423
    :goto_49
    invoke-static {}, Lcom/navdy/hud/app/event/InitEvents$Phase;->values()[Lcom/navdy/hud/app/event/InitEvents$Phase;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    :try_start_52
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->PRE_USER_INTERACTION:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/InitEvents$Phase;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_52 .. :try_end_5d} :catch_1be

    :goto_5d
    :try_start_5d
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->POST_START:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/InitEvents$Phase;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_68
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5d .. :try_end_68} :catch_1bb

    .line 1349
    :goto_68
    invoke-static {}, Lcom/navdy/service/library/events/input/Gesture;->values()[Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    :try_start_71
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_71 .. :try_end_7c} :catch_1b8

    .line 1279
    :goto_7c
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->values()[Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    :try_start_85
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_90
    .catch Ljava/lang/NoSuchFieldError; {:try_start_85 .. :try_end_90} :catch_1b5

    :goto_90
    :try_start_90
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_90 .. :try_end_9b} :catch_1b2

    :goto_9b
    :try_start_9b
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->VERY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9b .. :try_end_a6} :catch_1af

    :goto_a6
    :try_start_a6
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a6 .. :try_end_b1} :catch_1ac

    .line 1234
    :goto_b1
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    :try_start_ba
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ba .. :try_end_c5} :catch_1a9

    :goto_c5
    :try_start_c5
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c5 .. :try_end_d0} :catch_1a6

    .line 940
    :goto_d0
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->values()[Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory:[I

    :try_start_d9
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory:[I

    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->NOTIFICATION:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_e4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d9 .. :try_end_e4} :catch_1a3

    :goto_e4
    :try_start_e4
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory:[I

    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->SCREEN:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_ef
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e4 .. :try_end_ef} :catch_1a0

    .line 692
    :goto_ef
    invoke-static {}, Lcom/navdy/service/library/events/ui/Screen;->values()[Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    :try_start_f8
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_103
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f8 .. :try_end_103} :catch_19d

    :goto_103
    :try_start_103
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_10e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_103 .. :try_end_10e} :catch_19a

    :goto_10e
    :try_start_10e
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_119
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10e .. :try_end_119} :catch_198

    :goto_119
    :try_start_119
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_VOICE_CONTROL:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_124
    .catch Ljava/lang/NoSuchFieldError; {:try_start_119 .. :try_end_124} :catch_196

    :goto_124
    :try_start_124
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_12f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_124 .. :try_end_12f} :catch_194

    :goto_12f
    :try_start_12f
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_13a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12f .. :try_end_13a} :catch_192

    :goto_13a
    :try_start_13a
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_145
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13a .. :try_end_145} :catch_190

    .line 569
    :goto_145
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_14e
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_159
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14e .. :try_end_159} :catch_18e

    :goto_159
    :try_start_159
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_164
    .catch Ljava/lang/NoSuchFieldError; {:try_start_159 .. :try_end_164} :catch_18c

    :goto_164
    :try_start_164
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_16f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_164 .. :try_end_16f} :catch_18a

    :goto_16f
    :try_start_16f
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_DOUBLE_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_17a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16f .. :try_end_17a} :catch_188

    :goto_17a
    :try_start_17a
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_185
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17a .. :try_end_185} :catch_186

    :goto_185
    return-void

    :catch_186
    move-exception v0

    goto :goto_185

    :catch_188
    move-exception v0

    goto :goto_17a

    :catch_18a
    move-exception v0

    goto :goto_16f

    :catch_18c
    move-exception v0

    goto :goto_164

    :catch_18e
    move-exception v0

    goto :goto_159

    .line 692
    :catch_190
    move-exception v0

    goto :goto_145

    :catch_192
    move-exception v0

    goto :goto_13a

    :catch_194
    move-exception v0

    goto :goto_12f

    :catch_196
    move-exception v0

    goto :goto_124

    :catch_198
    move-exception v0

    goto :goto_119

    :catch_19a
    move-exception v0

    goto/16 :goto_10e

    :catch_19d
    move-exception v0

    goto/16 :goto_103

    .line 940
    :catch_1a0
    move-exception v0

    goto/16 :goto_ef

    :catch_1a3
    move-exception v0

    goto/16 :goto_e4

    .line 1234
    :catch_1a6
    move-exception v0

    goto/16 :goto_d0

    :catch_1a9
    move-exception v0

    goto/16 :goto_c5

    .line 1279
    :catch_1ac
    move-exception v0

    goto/16 :goto_b1

    :catch_1af
    move-exception v0

    goto/16 :goto_a6

    :catch_1b2
    move-exception v0

    goto/16 :goto_9b

    :catch_1b5
    move-exception v0

    goto/16 :goto_90

    .line 1349
    :catch_1b8
    move-exception v0

    goto/16 :goto_7c

    .line 1423
    :catch_1bb
    move-exception v0

    goto/16 :goto_68

    :catch_1be
    move-exception v0

    goto/16 :goto_5d

    .line 1565
    :catch_1c1
    move-exception v0

    goto/16 :goto_49

    :catch_1c4
    move-exception v0

    goto/16 :goto_3e

    :catch_1c7
    move-exception v0

    goto/16 :goto_33

    .line 1639
    :catch_1ca
    move-exception v0

    goto/16 :goto_1f

    :catch_1cd
    move-exception v0

    goto/16 :goto_14
.end method
