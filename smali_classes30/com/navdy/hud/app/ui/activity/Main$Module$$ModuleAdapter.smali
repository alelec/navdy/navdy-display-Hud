.class public final Lcom/navdy/hud/app/ui/activity/Main$Module$$ModuleAdapter;
.super Ldagger/internal/ModuleAdapter;
.source "Main$Module$$ModuleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ModuleAdapter",
        "<",
        "Lcom/navdy/hud/app/ui/activity/Main$Module;",
        ">;"
    }
.end annotation


# static fields
.field private static final INCLUDES:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final INJECTS:[Ljava/lang/String;

.field private static final STATIC_INJECTIONS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 11
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "members/com.navdy.hud.app.ui.activity.MainActivity"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "members/com.navdy.hud.app.view.MainView"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "members/com.navdy.hud.app.view.NotificationView"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "members/com.navdy.hud.app.presenter.NotificationPresenter"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "members/com.navdy.hud.app.ui.framework.UIStateManager"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "members/com.navdy.hud.app.view.ContainerView"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "members/com.navdy.hud.app.debug.GestureEngine"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$Module$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    .line 12
    new-array v0, v3, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$Module$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    .line 13
    new-array v0, v3, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$Module$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 16
    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Module;

    sget-object v2, Lcom/navdy/hud/app/ui/activity/Main$Module$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    sget-object v3, Lcom/navdy/hud/app/ui/activity/Main$Module$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    sget-object v5, Lcom/navdy/hud/app/ui/activity/Main$Module$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    const/4 v6, 0x1

    move-object v0, p0

    move v7, v4

    invoke-direct/range {v0 .. v7}, Ldagger/internal/ModuleAdapter;-><init>(Ljava/lang/Class;[Ljava/lang/String;[Ljava/lang/Class;Z[Ljava/lang/Class;ZZ)V

    .line 17
    return-void
.end method
