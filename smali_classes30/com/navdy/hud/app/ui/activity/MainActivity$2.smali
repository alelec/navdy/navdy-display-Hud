.class Lcom/navdy/hud/app/ui/activity/MainActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/activity/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/activity/MainActivity;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/activity/MainActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/activity/MainActivity;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->this$0:Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 136
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "action":Ljava/lang/String;
    const-string v13, "android.bluetooth.device.extra.DEVICE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/bluetooth/BluetoothDevice;

    .line 138
    .local v7, "device":Landroid/bluetooth/BluetoothDevice;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->this$0:Lcom/navdy/hud/app/ui/activity/MainActivity;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/activity/MainActivity;->mainPresenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    iget-object v6, v13, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    .line 140
    .local v6, "bus":Lcom/squareup/otto/Bus;
    const-string v13, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 141
    const-string v13, "android.bluetooth.device.extra.BOND_STATE"

    const/high16 v14, -0x80000000

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 143
    .local v3, "bondState":I
    const-string v13, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    const/high16 v14, -0x80000000

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 145
    .local v10, "oldState":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->this$0:Lcom/navdy/hud/app/ui/activity/MainActivity;

    # getter for: Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v13}, Lcom/navdy/hud/app/ui/activity/MainActivity;->access$000(Lcom/navdy/hud/app/ui/activity/MainActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Bond state change - device:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " state:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " prevState:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 147
    new-instance v13, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;

    invoke-direct {v13, v7, v10, v3}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$BondStateChange;-><init>(Landroid/bluetooth/BluetoothDevice;II)V

    invoke-virtual {v6, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 189
    .end local v3    # "bondState":I
    .end local v10    # "oldState":I
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    const-string v13, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 152
    const-string v13, "android.bluetooth.adapter.extra.STATE"

    const/high16 v14, -0x80000000

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 153
    .local v4, "btState":I
    const/16 v13, 0xc

    if-eq v4, v13, :cond_2

    const/16 v13, 0xa

    if-ne v4, v13, :cond_0

    .line 154
    :cond_2
    const/16 v13, 0xc

    if-ne v4, v13, :cond_3

    const/4 v8, 0x1

    .line 155
    .local v8, "enabled":Z
    :goto_1
    new-instance v13, Lcom/navdy/hud/app/event/InitEvents$BluetoothStateChanged;

    invoke-direct {v13, v8}, Lcom/navdy/hud/app/event/InitEvents$BluetoothStateChanged;-><init>(Z)V

    invoke-virtual {v6, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 154
    .end local v8    # "enabled":Z
    :cond_3
    const/4 v8, 0x0

    goto :goto_1

    .line 157
    .end local v4    # "btState":I
    :cond_4
    const-string v13, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->abortBroadcast()V

    .line 160
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v13

    invoke-virtual {v13, v7}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 161
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->this$0:Lcom/navdy/hud/app/ui/activity/MainActivity;

    # getter for: Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v13}, Lcom/navdy/hud/app/ui/activity/MainActivity;->access$100(Lcom/navdy/hud/app/ui/activity/MainActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Pairing request from dial device, ignore ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :cond_5
    const-string v13, "android.bluetooth.device.extra.PAIRING_VARIANT"

    const/high16 v14, -0x80000000

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 167
    .local v12, "type":I
    const-string v13, "android.bluetooth.device.extra.PAIRING_KEY"

    const/high16 v14, -0x80000000

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 169
    .local v11, "pairingKey":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->this$0:Lcom/navdy/hud/app/ui/activity/MainActivity;

    # getter for: Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v13}, Lcom/navdy/hud/app/ui/activity/MainActivity;->access$200(Lcom/navdy/hud/app/ui/activity/MainActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Showing pairing request from "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " of type:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " with pin:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 172
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 173
    .local v5, "bundle":Landroid/os/Bundle;
    const-string v13, "device"

    invoke-virtual {v5, v13, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 174
    const-string v13, "variant"

    invoke-virtual {v5, v13, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    const-string v13, "pin"

    invoke-virtual {v5, v13, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 177
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->this$0:Lcom/navdy/hud/app/ui/activity/MainActivity;

    iget-object v13, v13, Lcom/navdy/hud/app/ui/activity/MainActivity;->pairingManager:Lcom/navdy/hud/app/manager/PairingManager;

    invoke-virtual {v13}, Lcom/navdy/hud/app/manager/PairingManager;->isAutoPairing()Z

    move-result v2

    .line 178
    .local v2, "autoAccept":Z
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v9

    .line 179
    .local v9, "extra":Landroid/os/Bundle;
    if-nez v2, :cond_6

    if-eqz v9, :cond_6

    .line 181
    const-string v13, "auto"

    const/4 v14, 0x0

    invoke-virtual {v9, v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 183
    :cond_6
    const-string v13, "auto"

    invoke-virtual {v5, v13, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 184
    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing;->showBluetoothPairingToast(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 185
    .end local v2    # "autoAccept":Z
    .end local v5    # "bundle":Landroid/os/Bundle;
    .end local v9    # "extra":Landroid/os/Bundle;
    .end local v11    # "pairingKey":I
    .end local v12    # "type":I
    :cond_7
    const-string v13, "android.bluetooth.device.action.PAIRING_CANCEL"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/activity/MainActivity$2;->abortBroadcast()V

    .line 187
    new-instance v13, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$PairingCancelled;

    invoke-direct {v13, v7}, Lcom/navdy/hud/app/ui/component/bluetooth/BluetoothPairing$PairingCancelled;-><init>(Landroid/bluetooth/BluetoothDevice;)V

    invoke-virtual {v6, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
