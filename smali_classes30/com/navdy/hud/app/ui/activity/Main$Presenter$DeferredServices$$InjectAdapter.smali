.class public final Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "Main$Presenter$DeferredServices$$InjectAdapter.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;",
        ">;"
    }
.end annotation


# instance fields
.field private ancsService:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ancs/AncsServiceConnector;",
            ">;"
        }
    .end annotation
.end field

.field private gestureService:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/gesture/GestureServiceConnector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 28
    const/4 v0, 0x0

    const-string v1, "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 29
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 38
    const-string v0, "com.navdy.hud.app.gesture.GestureServiceConnector"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    .line 39
    const-string v0, "com.navdy.hud.app.ancs.AncsServiceConnector"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;->ancsService:Ldagger/internal/Binding;

    .line 40
    return-void
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;->ancsService:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;->ancsService:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;->ancsService:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    .line 60
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;)V

    return-void
.end method
