.class Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/toast/IToastCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/activity/Main$Presenter;->showInstallingUpdateToast()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field animator:Landroid/animation/ObjectAnimator;

.field final synthetic this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .prologue
    .line 1689
    iput-object p1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    return-void
.end method


# virtual methods
.method public executeChoiceItem(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 1728
    return-void
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 1724
    const/4 v0, 0x0

    return v0
.end method

.method public onStart(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 6
    .param p1, "view"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    .line 1693
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getView()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    .line 1694
    .local v0, "lyt":Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1695
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1696
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1697
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title2:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1698
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    if-nez v1, :cond_4a

    .line 1699
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->sideImage:Landroid/widget/ImageView;

    sget-object v2, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/high16 v4, 0x43b40000    # 360.0f

    aput v4, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    .line 1700
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1701
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1702
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8$1;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1711
    :cond_4a
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_57

    .line 1712
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 1714
    :cond_57
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 1717
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_e

    .line 1718
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 1719
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1721
    :cond_e
    return-void
.end method
