.class Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/activity/Main$Presenter;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .prologue
    .line 517
    iput-object p1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart(Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 5
    .param p1, "baseScreen"    # Lcom/navdy/hud/app/screen/BaseScreen;
    .param p2, "baseScreen2"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    const/4 v4, 0x4

    .line 520
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "screen anim start in["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]  out["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 521
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->clearInputFocus()V

    .line 522
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    iget-object v0, v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 523
    .local v0, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {p1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isFullscreenMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v1

    if-nez v1, :cond_48

    .line 524
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setNotificationColorVisibility(I)V

    .line 525
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setSystemTrayVisibility(I)V

    .line 527
    :cond_48
    return-void
.end method

.method public onStop(Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 3
    .param p1, "baseScreen"    # Lcom/navdy/hud/app/screen/BaseScreen;
    .param p2, "baseScreen2"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    .line 531
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "screen anim stop in["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]  out["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->SCREEN:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    invoke-virtual {p1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->access$300(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;Lcom/navdy/service/library/events/ui/Screen;)V

    .line 533
    return-void
.end method
