.class public final Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "MainActivity$$InjectAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/ui/activity/MainActivity;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/ui/activity/MainActivity;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/ui/activity/MainActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private mainPresenter:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/activity/Main$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private pairingManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/manager/PairingManager;",
            ">;"
        }
    .end annotation
.end field

.field private powerManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/device/PowerManager;",
            ">;"
        }
    .end annotation
.end field

.field private supertype:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/activity/HudBaseActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 31
    const-string v0, "com.navdy.hud.app.ui.activity.MainActivity"

    const-string v1, "members/com.navdy.hud.app.ui.activity.MainActivity"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 32
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 6
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 41
    const-string v0, "com.navdy.hud.app.ui.activity.Main$Presenter"

    const-class v1, Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->mainPresenter:Ldagger/internal/Binding;

    .line 42
    const-string v0, "com.navdy.hud.app.manager.PairingManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->pairingManager:Ldagger/internal/Binding;

    .line 43
    const-string v0, "com.navdy.hud.app.device.PowerManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    .line 44
    const-string v1, "members/com.navdy.hud.app.ui.activity.HudBaseActivity"

    const-class v2, Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;ZZ)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    .line 45
    return-void
.end method

.method public get()Lcom/navdy/hud/app/ui/activity/MainActivity;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/activity/MainActivity;-><init>()V

    .line 66
    .local v0, "result":Lcom/navdy/hud/app/ui/activity/MainActivity;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/ui/activity/MainActivity;)V

    .line 67
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->get()Lcom/navdy/hud/app/ui/activity/MainActivity;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->mainPresenter:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->pairingManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/ui/activity/MainActivity;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/ui/activity/MainActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->mainPresenter:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/MainActivity;->mainPresenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->pairingManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/PairingManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/MainActivity;->pairingManager:Lcom/navdy/hud/app/manager/PairingManager;

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/PowerManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/MainActivity;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-virtual {v0, p1}, Ldagger/internal/Binding;->injectMembers(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/navdy/hud/app/ui/activity/MainActivity;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/activity/MainActivity$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/ui/activity/MainActivity;)V

    return-void
.end method
