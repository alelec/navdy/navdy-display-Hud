.class public Lcom/navdy/hud/app/ui/activity/MainActivity;
.super Lcom/navdy/hud/app/ui/activity/HudBaseActivity;
.source "MainActivity.java"


# static fields
.field private static final ACTION_PAIRING_CANCEL:Ljava/lang/String; = "android.bluetooth.device.action.PAIRING_CANCEL"


# instance fields
.field private bluetoothReceiver:Landroid/content/BroadcastReceiver;

.field container:Lcom/navdy/hud/app/view/ContainerView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0132
    .end annotation
.end field

.field debugText:Landroid/widget/TextView;

.field mainPresenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field pairingManager:Lcom/navdy/hud/app/manager/PairingManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;-><init>()V

    .line 133
    new-instance v0, Lcom/navdy/hud/app/ui/activity/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/activity/MainActivity$2;-><init>(Lcom/navdy/hud/app/ui/activity/MainActivity;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/activity/MainActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/MainActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/activity/MainActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/MainActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ui/activity/MainActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/MainActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method protected getBlueprint()Lmortar/Blueprint;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/navdy/hud/app/ui/activity/Main;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/activity/Main;-><init>()V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 195
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 196
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 72
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 75
    const v5, 0x7f030031

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/MainActivity;->setContentView(I)V

    .line 87
    :goto_0
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 88
    const-string v5, "HUD"

    const/high16 v7, 0x7f060000

    invoke-static {p0, v5, v6, v7, v6}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;Ljava/lang/String;IIZ)V

    .line 93
    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/MainActivity;->makeImmersive(Z)V

    .line 96
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 97
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    const-string v5, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 98
    const-string v5, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    const-string v5, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    const-string v5, "android.bluetooth.device.action.PAIRING_CANCEL"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 101
    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v2}, Lcom/navdy/hud/app/ui/activity/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 103
    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->mainPresenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    iget-object v5, v5, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v5, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 104
    return-void

    .line 78
    .end local v2    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    const v5, 0x7f030003

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/MainActivity;->setContentView(I)V

    .line 79
    const v5, 0x7f0e00bc

    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->debugText:Landroid/widget/TextView;

    .line 80
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 81
    .local v3, "resources":Landroid/content/res/Resources;
    const v5, 0x7f0b0071

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v1, v5

    .line 82
    .local v1, "hudWidth":I
    const v5, 0x7f0b0070

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    .line 83
    .local v0, "hudHeight":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Device:  "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " width = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " height = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " scale factor = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    div-int/lit16 v7, v1, 0x280

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 84
    .local v4, "str":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->debugText:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .end local v0    # "hudHeight":I
    .end local v1    # "hudWidth":I
    .end local v3    # "resources":Landroid/content/res/Resources;
    .end local v4    # "str":Ljava/lang/String;
    :cond_1
    move v5, v6

    .line 93
    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->onDestroy()V

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/activity/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 111
    return-void
.end method

.method public onInitEvent(Lcom/navdy/hud/app/event/InitEvents$InitPhase;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/event/InitEvents$InitPhase;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 115
    iget-object v0, p1, Lcom/navdy/hud/app/event/InitEvents$InitPhase;->phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->POST_START:Lcom/navdy/hud/app/event/InitEvents$Phase;

    if-ne v0, v1, :cond_0

    .line 116
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/activity/MainActivity$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/activity/MainActivity$1;-><init>(Lcom/navdy/hud/app/ui/activity/MainActivity;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 124
    :cond_0
    return-void
.end method

.method public onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "enableNormalMode(): FLAG_KEEP_SCREEN_ON set"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 132
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 200
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->onWindowFocusChanged(Z)V

    .line 201
    if-eqz p1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/MainActivity;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/activity/MainActivity;->makeImmersive(Z)V

    .line 205
    :cond_0
    return-void

    .line 203
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
