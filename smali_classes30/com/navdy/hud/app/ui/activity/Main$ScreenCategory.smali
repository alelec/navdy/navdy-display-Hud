.class final enum Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;
.super Ljava/lang/Enum;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/activity/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ScreenCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

.field public static final enum NOTIFICATION:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

.field public static final enum SCREEN:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1962
    new-instance v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    const-string v1, "NOTIFICATION"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->NOTIFICATION:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    .line 1963
    new-instance v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    const-string v1, "SCREEN"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->SCREEN:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    .line 1960
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->NOTIFICATION:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->SCREEN:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->$VALUES:[Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1960
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1960
    const-class v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;
    .locals 1

    .prologue
    .line 1960
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->$VALUES:[Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    return-object v0
.end method
