.class public abstract Lcom/navdy/hud/app/ui/activity/HudBaseActivity;
.super Lcom/navdy/hud/app/common/BaseActivity;
.source "HudBaseActivity.java"


# instance fields
.field inputManager:Lcom/navdy/hud/app/manager/InputManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/navdy/hud/app/common/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected makeImmersive(Z)V
    .locals 4
    .param p1, "keepScreenOn"    # Z

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 64
    .local v1, "window":Landroid/view/Window;
    const/high16 v0, 0x480000

    .line 67
    .local v0, "flags":I
    if-eqz p1, :cond_0

    .line 68
    or-int/lit16 v0, v0, 0x80

    .line 71
    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 73
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x1706

    invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 80
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "setting immersive mode"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 38
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keydown:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 39
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v1, p1, p2}, Lcom/navdy/hud/app/manager/InputManager;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 42
    :cond_0
    if-eqz v0, :cond_1

    .line 43
    const/4 v1, 0x1

    .line 45
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/common/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v1, p1, p2}, Lcom/navdy/hud/app/manager/InputManager;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 55
    :cond_0
    if-eqz v0, :cond_1

    .line 56
    const/4 v1, 0x1

    .line 58
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/common/BaseActivity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 24
    const/4 v0, 0x0

    .line 25
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    if-eqz v1, :cond_0

    .line 26
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/HudBaseActivity;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v1, p1, p2}, Lcom/navdy/hud/app/manager/InputManager;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 28
    :cond_0
    if-eqz v0, :cond_1

    .line 29
    const/4 v1, 0x1

    .line 31
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/common/BaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method
