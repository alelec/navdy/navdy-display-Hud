.class public final Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "Main$Presenter$$InjectAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/ui/activity/Main$Presenter;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/ui/activity/Main$Presenter;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/ui/activity/Main$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private callManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/framework/phonecall/CallManager;",
            ">;"
        }
    .end annotation
.end field

.field private connectionHandler:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/service/ConnectionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private dialSimulatorMessagesHandler:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;",
            ">;"
        }
    .end annotation
.end field

.field private driverProfileManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/profile/DriverProfileManager;",
            ">;"
        }
    .end annotation
.end field

.field private globalPreferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private inputManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/manager/InputManager;",
            ">;"
        }
    .end annotation
.end field

.field private musicManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager;",
            ">;"
        }
    .end annotation
.end field

.field private pandoraManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/service/pandora/PandoraManager;",
            ">;"
        }
    .end annotation
.end field

.field private powerManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/device/PowerManager;",
            ">;"
        }
    .end annotation
.end field

.field private preferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private settingsManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/config/SettingsManager;",
            ">;"
        }
    .end annotation
.end field

.field private supertype:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/framework/BasePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private tripManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/framework/trips/TripManager;",
            ">;"
        }
    .end annotation
.end field

.field private uiStateManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 42
    const-string v0, "com.navdy.hud.app.ui.activity.Main$Presenter"

    const-string v1, "members/com.navdy.hud.app.ui.activity.Main$Presenter"

    const/4 v2, 0x1

    const-class v3, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 43
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 6
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 52
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->bus:Ldagger/internal/Binding;

    .line 53
    const-string v0, "com.navdy.hud.app.manager.InputManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->inputManager:Ldagger/internal/Binding;

    .line 54
    const-string v0, "com.navdy.hud.app.service.ConnectionHandler"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    .line 55
    const-string v0, "com.navdy.hud.app.ui.framework.UIStateManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    .line 56
    const-string v0, "com.navdy.hud.app.framework.phonecall.CallManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->callManager:Ldagger/internal/Binding;

    .line 57
    const-string v0, "com.navdy.hud.app.manager.MusicManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->musicManager:Ldagger/internal/Binding;

    .line 58
    const-string v0, "com.navdy.hud.app.service.pandora.PandoraManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->pandoraManager:Ldagger/internal/Binding;

    .line 59
    const-string v0, "com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->dialSimulatorMessagesHandler:Ldagger/internal/Binding;

    .line 60
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->globalPreferences:Ldagger/internal/Binding;

    .line 61
    const-string v0, "com.navdy.hud.app.config.SettingsManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->settingsManager:Ldagger/internal/Binding;

    .line 62
    const-string v0, "com.navdy.hud.app.device.PowerManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    .line 63
    const-string v0, "com.navdy.hud.app.profile.DriverProfileManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->driverProfileManager:Ldagger/internal/Binding;

    .line 64
    const-string v0, "com.navdy.hud.app.framework.trips.TripManager"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    .line 65
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->preferences:Ldagger/internal/Binding;

    .line 66
    const-string v1, "members/com.navdy.hud.app.ui.framework.BasePresenter"

    const-class v2, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;ZZ)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    .line 67
    return-void
.end method

.method public get()Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-direct {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;-><init>()V

    .line 99
    .local v0, "result":Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    .line 100
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->get()Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->inputManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->callManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->musicManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->pandoraManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->dialSimulatorMessagesHandler:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->globalPreferences:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->settingsManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->driverProfileManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->preferences:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->inputManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/InputManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/ConnectionHandler;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->callManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->musicManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/MusicManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->pandoraManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/pandora/PandoraManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->pandoraManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->dialSimulatorMessagesHandler:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->dialSimulatorMessagesHandler:Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->globalPreferences:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->globalPreferences:Landroid/content/SharedPreferences;

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->settingsManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/config/SettingsManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->settingsManager:Lcom/navdy/hud/app/config/SettingsManager;

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->powerManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/PowerManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->driverProfileManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/trips/TripManager;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->preferences:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->preferences:Landroid/content/SharedPreferences;

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->supertype:Ldagger/internal/Binding;

    invoke-virtual {v0, p1}, Ldagger/internal/Binding;->injectMembers(Ljava/lang/Object;)V

    .line 124
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    return-void
.end method
