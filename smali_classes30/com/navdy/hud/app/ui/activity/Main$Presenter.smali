.class public Lcom/navdy/hud/app/ui/activity/Main$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "Main.java"

# interfaces
.implements Lflow/Flow$Listener;
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/activity/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/view/MainView;",
        ">;",
        "Lflow/Flow$Listener;",
        "Lcom/navdy/hud/app/manager/InputManager$IInputHandler;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final MAIN_SETTINGS:Ljava/lang/String; = "main.settings"


# instance fields
.field private animationRunning:Z

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private createdScreens:Z

.field private currentScreen:Lcom/navdy/service/library/events/ui/Screen;

.field private defaultNotifColor:I

.field dialSimulatorMessagesHandler:Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field flow:Lflow/Flow;

.field globalPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private hudSettings:Lcom/navdy/hud/app/settings/HUDSettings;

.field private initManager:Lcom/navdy/hud/app/manager/InitManager;

.field private innerEventsDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

.field inputManager:Lcom/navdy/hud/app/manager/InputManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field musicManager:Lcom/navdy/hud/app/manager/MusicManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private notifAnimationRunning:Z

.field private notificationAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

.field private notificationColorEnabled:Z

.field private notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field private operationQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;",
            ">;"
        }
    .end annotation
.end field

.field pandoraManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field preferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

.field private services:Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

.field private settings:Lcom/navdy/hud/app/settings/MainScreenSettings;

.field settingsManager:Lcom/navdy/hud/app/config/SettingsManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private showingPortrait:Z

.field private systemTrayEnabled:Z

.field tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private voiceAssistNotification:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

.field private voiceSearchNotification:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 491
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    .line 492
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->systemTrayEnabled:Z

    .line 493
    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationColorEnabled:Z

    .line 494
    new-instance v0, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    const/4 v1, 0x4

    new-instance v2, Lcom/navdy/hud/app/ui/activity/Main$Presenter$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$1;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;-><init>(ILcom/navdy/hud/app/gesture/MultipleClickGestureDetector$IMultipleClickKeyGesture;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->innerEventsDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    .line 515
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->operationQueue:Ljava/util/Queue;

    .line 516
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->handler:Landroid/os/Handler;

    .line 517
    new-instance v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$2;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    .line 535
    new-instance v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    .line 557
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .param p1, "x1"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 438
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->executeKeyEvent(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/activity/Main$Presenter;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .param p1, "x1"    # I

    .prologue
    .line 438
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->executeMultipleKeyEvent(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->services:Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;Lcom/navdy/service/library/events/ui/Screen;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .param p1, "x1"    # Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;
    .param p2, "x2"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 438
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateSystemTrayVisibility(Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;Lcom/navdy/service/library/events/ui/Screen;)V

    return-void
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .param p1, "x1"    # Z

    .prologue
    .line 438
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notifAnimationRunning:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .prologue
    .line 438
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->runQueuedOperation()V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .param p1, "x1"    # Lcom/navdy/service/library/events/ui/Screen;
    .param p2, "x2"    # Landroid/os/Bundle;
    .param p3, "x3"    # Ljava/lang/Object;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Z

    .prologue
    .line 438
    invoke-direct/range {p0 .. p5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateScreen(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;ZZ)V

    return-void
.end method

.method private clearViews()V
    .locals 3

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 561
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_1e

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->isNotificationViewShowing()Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notifAnimationRunning:Z

    if-nez v1, :cond_1e

    .line 562
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "NotificationManager:dismissNotification"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 563
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->dismissNotification()V

    .line 565
    :cond_1e
    return-void
.end method

.method private executeKeyEvent(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 7
    .param p1, "customKeyEvent"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v6, 0x0

    .line 568
    const/4 v0, 0x0

    .line 569
    .local v0, "b":Z
    sget-object v2, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_a4

    .line 609
    :goto_d
    return v0

    .line 571
    :pswitch_e
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 572
    const/4 v0, 0x1

    .line 573
    goto :goto_d

    .line 576
    :pswitch_24
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    .line 577
    .local v1, "instance":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_56

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isAppConnected()Z

    move-result v2

    if-eqz v2, :cond_56

    .line 578
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getLongPressAction()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    if-ne v2, v3, :cond_52

    .line 579
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "longpress, Place Search "

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 580
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->launchVoiceSearch()V

    .line 589
    :goto_50
    const/4 v0, 0x1

    .line 590
    goto :goto_d

    .line 583
    :cond_52
    invoke-virtual {p0, v6}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->startVoiceAssistance(Z)V

    goto :goto_50

    .line 587
    :cond_56
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendConnectionNotification()V

    goto :goto_50

    .line 593
    .end local v1    # "instance":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    :pswitch_5c
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "power button click: show shutdown"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 594
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    sget-object v5, Lcom/navdy/hud/app/event/Shutdown$Reason;->POWER_BUTTON:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v5}, Lcom/navdy/hud/app/event/Shutdown$Reason;->asBundle()Landroid/os/Bundle;

    move-result-object v5

    invoke-direct {v3, v4, v5, v6}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 595
    const/4 v0, 0x1

    .line 596
    goto :goto_d

    .line 599
    :pswitch_79
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "power button double click: show dial pairing"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 600
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 601
    const/4 v0, 0x1

    .line 602
    goto/16 :goto_d

    .line 605
    :pswitch_99
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "power button click: no-op"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 569
    :pswitch_data_a4
    .packed-switch 0x1
        :pswitch_e
        :pswitch_24
        :pswitch_5c
        :pswitch_79
        :pswitch_99
    .end packed-switch
.end method

.method private executeMultipleKeyEvent(I)V
    .locals 5
    .param p1, "n"    # I

    .prologue
    .line 613
    const/4 v1, 0x2

    if-ne p1, v1, :cond_10

    .line 614
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Double click"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 615
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->takeNotificationAction()V

    .line 643
    :cond_f
    :goto_f
    return-void

    .line 617
    :cond_10
    const/4 v1, 0x3

    if-ne p1, v1, :cond_40

    .line 618
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Triple click"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 619
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$600()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 620
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$700()V

    goto :goto_f

    .line 622
    :cond_26
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 623
    .local v0, "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    if-ne v1, v2, :cond_3a

    .line 624
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    goto :goto_f

    .line 627
    :cond_3a
    sget-object v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    goto :goto_f

    .line 631
    .end local v0    # "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :cond_40
    const/4 v1, 0x4

    if-lt p1, v1, :cond_f

    .line 632
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "4 clicks, dumping a snapshot"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 633
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->dumpSnapshotAsync()V

    .line 634
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v1

    if-nez v1, :cond_f

    .line 635
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/ui/activity/Main$Presenter$4;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$4;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_f
.end method

.method private hideNotification()V
    .locals 3

    .prologue
    .line 646
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 647
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_1c

    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    if-eqz v1, :cond_1c

    .line 648
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "NotificationManager:calling shownotification"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 649
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->dismissNotification()V

    .line 651
    :cond_1c
    return-void
.end method

.method private isPortrait()Z
    .locals 2

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 655
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_14

    .line 656
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/navdy/hud/app/util/ScreenOrientation;->isPortrait(Landroid/app/Activity;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->showingPortrait:Z

    .line 658
    :cond_14
    iget-boolean v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->showingPortrait:Z

    return v1
.end method

.method private launchVoiceAssistance(Z)V
    .locals 6
    .param p1, "b"    # Z

    .prologue
    .line 662
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 663
    .local v0, "instance":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->isToastDisplayed()Z

    move-result v3

    if-eqz v3, :cond_20

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "incomingcall#toast"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 664
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "startVoiceAssistance:cannot launch, phone toast"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 679
    :goto_1f
    return-void

    .line 667
    :cond_20
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 668
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startVoiceAssistance: disable toast, current notif:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 669
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 671
    .local v2, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    if-eqz p1, :cond_4e

    .line 672
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->voiceSearchNotification:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .line 677
    .local v1, "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    :goto_4a
    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto :goto_1f

    .line 675
    .end local v1    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    :cond_4e
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->voiceAssistNotification:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .restart local v1    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    goto :goto_4a
.end method

.method private launchVoiceSearch()V
    .locals 3

    .prologue
    .line 682
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    .line 684
    .local v0, "instance":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v2, "navdy#voicesearch#notif"

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .local v1, "voiceSearchNotification":Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    if-nez v1, :cond_13

    .line 685
    new-instance v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .end local v1    # "voiceSearchNotification":Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    invoke-direct {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;-><init>()V

    .line 687
    .restart local v1    # "voiceSearchNotification":Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    :cond_13
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 688
    return-void
.end method

.method private static lookupScreen(Lcom/navdy/service/library/events/ui/Screen;)Lmortar/Blueprint;
    .locals 5
    .param p0, "screen_HOME"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 692
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    invoke-virtual {p0}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_3c

    .line 699
    :goto_b
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$800()Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {p0}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 700
    .local v0, "clz":Ljava/lang/Class;
    if-eqz v0, :cond_2f

    .line 701
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmortar/Blueprint;

    .line 710
    .end local v0    # "clz":Ljava/lang/Class;
    :goto_21
    return-object v3

    .line 695
    :pswitch_22
    sget-object p0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;
    :try_end_24
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_24} :catch_25
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_24} :catch_31

    goto :goto_b

    .line 704
    :catch_25
    move-exception v1

    .line 705
    .local v1, "ex":Ljava/lang/InstantiationException;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Failed to instantiate screen"

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 710
    .end local v1    # "ex":Ljava/lang/InstantiationException;
    :cond_2f
    :goto_2f
    const/4 v3, 0x0

    goto :goto_21

    .line 707
    :catch_31
    move-exception v2

    .line 708
    .local v2, "ex2":Ljava/lang/IllegalAccessException;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Failed to instantiate screen"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2f

    .line 692
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_22
        :pswitch_22
    .end packed-switch
.end method

.method private persistSettings()V
    .locals 4

    .prologue
    .line 714
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Starting to persist settings"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 715
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->settings:Lcom/navdy/hud/app/settings/MainScreenSettings;

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 716
    .local v0, "json":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "main.settings"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 717
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Persisted settings:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 718
    return-void
.end method

.method private runQueuedOperation()V
    .locals 5

    .prologue
    .line 721
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v1

    .line 722
    .local v1, "size":I
    if-lez v1, :cond_41

    .line 723
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->operationQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;

    .line 724
    .local v0, "screenInfo":Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "runQueuedOperation size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " posting:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;->screen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 725
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/hud/app/ui/activity/Main$Presenter$5;

    invoke-direct {v3, p0, v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$5;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 737
    .end local v0    # "screenInfo":Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;
    :goto_40
    return-void

    .line 733
    :cond_41
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setInputFocus()V

    .line 734
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->animationRunning:Z

    .line 735
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "animationRunning:false"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_40
.end method

.method private setDisplayFormat(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;)V
    .locals 4
    .param p1, "displayFormat"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .prologue
    .line 740
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Switching display format to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 741
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 742
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_25

    .line 743
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/MainView;->setDisplayFormat(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;)V

    .line 745
    :cond_25
    return-void
.end method

.method private setupFlow()V
    .locals 3

    .prologue
    .line 748
    new-instance v1, Lcom/navdy/hud/app/screen/FirstLaunchScreen;

    invoke-direct {v1}, Lcom/navdy/hud/app/screen/FirstLaunchScreen;-><init>()V

    invoke-static {v1}, Lflow/Backstack;->single(Ljava/lang/Object;)Lflow/Backstack;

    move-result-object v0

    .line 749
    .local v0, "single":Lflow/Backstack;
    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FIRST_LAUNCH:Lcom/navdy/service/library/events/ui/Screen;

    iput-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->currentScreen:Lcom/navdy/service/library/events/ui/Screen;

    .line 750
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->currentScreen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setCurrentViewMode(Lcom/navdy/service/library/events/ui/Screen;)V

    .line 751
    new-instance v1, Lflow/Flow;

    invoke-direct {v1, v0, p0}, Lflow/Flow;-><init>(Lflow/Backstack;Lflow/Flow$Listener;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->flow:Lflow/Flow;

    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v2}, Lflow/Flow;->getBackstack()Lflow/Backstack;

    move-result-object v2

    invoke-virtual {v2}, Lflow/Backstack;->current()Lflow/Backstack$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflow/Flow;->resetTo(Ljava/lang/Object;)V

    .line 752
    return-void
.end method

.method private showInstallingUpdateToast()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 1673
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v8

    .line 1674
    .local v8, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual {v8}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 1675
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 1676
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 1677
    .local v7, "resources":Landroid/content/res/Resources;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1678
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "#ota#toast"

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 1679
    const-string v0, "1"

    const v1, 0x7f0902fd

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1680
    const-string v0, "8"

    const v1, 0x7f020226

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1681
    const-string v0, "11"

    const v1, 0x7f0201e5

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1682
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->getConnectionType()Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;->POWER_ONLY:Lcom/navdy/hud/app/obd/ObdManager$ConnectionType;

    if-ne v0, v1, :cond_99

    const v6, 0x7f0900de

    .line 1683
    .local v6, "powerWarning":I
    :goto_49
    const-string v0, "2"

    const v1, 0x7f090214

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1684
    const-string v0, "3"

    const v1, 0x7f0c001f

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1685
    const-string v0, "4"

    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1686
    const-string v0, "5"

    const v1, 0x7f0c0020

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1687
    const-string v0, "16"

    const v1, 0x7f0b0190

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1688
    const-string v0, "16_2"

    const v1, 0x7f0b018f

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1689
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "#ota#toast"

    new-instance v3, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$8;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 1730
    return-void

    .line 1682
    .end local v6    # "powerWarning":I
    :cond_99
    const v6, 0x7f0900dd

    goto :goto_49
.end method

.method private showNotification()V
    .locals 5

    .prologue
    .line 1733
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/MainView;

    .line 1734
    .local v2, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v2, :cond_1c

    .line 1735
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    .line 1736
    .local v0, "currentNotification":Lcom/navdy/hud/app/framework/notifications/INotification;
    if-nez v0, :cond_1d

    .line 1737
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "no current notification to be displayed"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1738
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->runQueuedOperation()V

    .line 1750
    .end local v0    # "currentNotification":Lcom/navdy/hud/app/framework/notifications/INotification;
    :cond_1c
    :goto_1c
    return-void

    .line 1741
    .restart local v0    # "currentNotification":Lcom/navdy/hud/app/framework/notifications/INotification;
    :cond_1d
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "NotificationManager:calling shownotification"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1742
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v1

    .line 1743
    .local v1, "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->hasModeView()Z

    move-result v3

    if-eqz v3, :cond_44

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isModeVisible()Z

    move-result v3

    if-eqz v3, :cond_44

    .line 1744
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "mode-view: reset mode view show notif"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1745
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->resetModeView()V

    .line 1747
    :cond_44
    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotification;->getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/view/MainView;->showNotification(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;)V

    goto :goto_1c
.end method

.method private signalBootComplete()V
    .locals 2

    .prologue
    .line 755
    const-string v0, "dev.app_started"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    const-string v0, "dev.late_service"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    return-void
.end method

.method private startLateServices()V
    .locals 6

    .prologue
    .line 760
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "starting update service"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 761
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 762
    .local v0, "appContext":Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 763
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "COMMAND"

    const-string v4, "VERIFY_UPDATE"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 764
    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 765
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->scheduleSync()V

    .line 766
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "initializing additional services"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 767
    new-instance v3, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    invoke-direct {v3, v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->services:Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    .line 768
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getFeatureUtil()Lcom/navdy/hud/app/util/FeatureUtil;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_ENGINE:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/util/FeatureUtil;->isFeatureEnabled(Lcom/navdy/hud/app/util/FeatureUtil$Feature;)Z

    move-result v3

    if-eqz v3, :cond_6d

    .line 769
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "gesture is supported"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 770
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getInputPreferences()Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v1

    .line 771
    .local v1, "inputPreferences":Lcom/navdy/service/library/events/preferences/InputPreferences;
    if-eqz v1, :cond_6e

    iget-object v3, v1, Lcom/navdy/service/library/events/preferences/InputPreferences;->use_gestures:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6e

    .line 772
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v3

    new-instance v4, Lcom/navdy/hud/app/ui/activity/Main$Presenter$6;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$6;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 784
    .end local v1    # "inputPreferences":Lcom/navdy/service/library/events/preferences/InputPreferences;
    :cond_6d
    :goto_6d
    return-void

    .line 781
    .restart local v1    # "inputPreferences":Lcom/navdy/service/library/events/preferences/InputPreferences;
    :cond_6e
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "gesture is not enabled"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_6d
.end method

.method private takeNotificationAction()V
    .locals 3

    .prologue
    .line 787
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 788
    .local v0, "instance":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_38

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isAppConnected()Z

    move-result v1

    if-eqz v1, :cond_38

    .line 789
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const-string v2, "navdy#phone#call#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 790
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "show phone call"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 791
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandNotification()Z

    .line 801
    :goto_28
    return-void

    .line 794
    :cond_29
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "show music"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 795
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager;->showMusicNotification()V

    goto :goto_28

    .line 799
    :cond_38
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendConnectionNotification()V

    goto :goto_28
.end method

.method private updateDisplayFormat()V
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDisplayFormat()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setDisplayFormat(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;)V

    .line 805
    return-void
.end method

.method private updateScreen(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;ZZ)V
    .locals 17
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "o"    # Ljava/lang/Object;
    .param p4, "b"    # Z
    .param p5, "b2"    # Z

    .prologue
    .line 808
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "updateScreen screen["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] animRunning["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->animationRunning:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] ignoreAnim ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] dismiss["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 809
    if-nez p4, :cond_65

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->animationRunning:Z

    if-eqz v12, :cond_65

    .line 810
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->operationQueue:Ljava/util/Queue;

    new-instance v13, Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v3, p5

    invoke-direct {v13, v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-interface {v12, v13}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 937
    :cond_64
    :goto_64
    return-void

    .line 813
    :cond_65
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->animationRunning:Z

    .line 814
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    const-string v13, "animationRunning:true"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 815
    const/4 v9, 0x0

    .line 816
    .local v9, "n":I
    sget-object v12, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_238

    .line 843
    :goto_7f
    if-eqz v9, :cond_d7

    .line 844
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    const-string v13, "notif screen"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 845
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->runQueuedOperation()V

    goto :goto_64

    .line 818
    :pswitch_8e
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v12

    if-eqz v12, :cond_a1

    .line 819
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v12}, Lcom/navdy/hud/app/manager/MusicManager;->showMusicNotification()V

    .line 824
    :goto_9f
    const/4 v9, 0x1

    .line 825
    goto :goto_7f

    .line 822
    :cond_a1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v12}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendConnectionNotification()V

    goto :goto_9f

    .line 828
    :pswitch_a9
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v12

    if-eqz v12, :cond_ca

    .line 829
    if-eqz p2, :cond_c8

    const-string v12, "extra_voice_search_notification"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_c8

    const/4 v12, 0x1

    :goto_c1
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->startVoiceAssistance(Z)V

    .line 834
    :goto_c6
    const/4 v9, 0x1

    .line 835
    goto :goto_7f

    .line 829
    :cond_c8
    const/4 v12, 0x0

    goto :goto_c1

    .line 832
    :cond_ca
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v12}, Lcom/navdy/hud/app/service/ConnectionHandler;->sendConnectionNotification()V

    goto :goto_c6

    .line 838
    :pswitch_d2
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$700()V

    .line 839
    const/4 v9, 0x1

    goto :goto_7f

    .line 849
    :cond_d7
    move-object/from16 v4, p1

    .local v4, "currentViewMode":Lcom/navdy/service/library/events/ui/Screen;
    sget-object v12, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    move-object/from16 v0, p1

    if-ne v0, v12, :cond_e7

    .line 850
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v12}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentViewMode()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v4

    .line 852
    :cond_e7
    invoke-static {v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isSidePanelMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v12

    if-eqz v12, :cond_151

    .line 853
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->currentScreen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-static {v12}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isOverlayMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v12

    if-eqz v12, :cond_11b

    .line 854
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v13, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v14, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-direct/range {v13 .. v16}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v12, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 855
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v13, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    const/4 v14, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v13, v4, v0, v1, v14}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v12, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_64

    .line 858
    :cond_11b
    sget-object v12, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    invoke-virtual {v4}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_242

    goto/16 :goto_64

    .line 860
    :pswitch_128
    if-nez p5, :cond_12f

    .line 861
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->showNotification()V

    goto/16 :goto_64

    .line 864
    :cond_12f
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/hud/app/view/MainView;

    .line 865
    .local v8, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v8, :cond_64

    invoke-virtual {v8}, Lcom/navdy/hud/app/view/MainView;->isNotificationViewShowing()Z

    move-result v12

    if-eqz v12, :cond_64

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notifAnimationRunning:Z

    if-nez v12, :cond_64

    .line 866
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v12

    const-string v13, "NotificationManager:calling dismissNotification"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 867
    invoke-virtual {v8}, Lcom/navdy/hud/app/view/MainView;->dismissNotification()V

    goto/16 :goto_64

    .line 876
    .end local v8    # "mainView":Lcom/navdy/hud/app/view/MainView;
    :cond_151
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->clearViews()V

    .line 877
    invoke-static {v4}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->lookupScreen(Lcom/navdy/service/library/events/ui/Screen;)Lmortar/Blueprint;

    move-result-object v7

    .line 878
    .local v7, "lookupScreen":Lmortar/Blueprint;
    instance-of v12, v7, Lcom/navdy/hud/app/screen/BaseScreen;

    if-eqz v12, :cond_166

    move-object v12, v7

    .line 879
    check-cast v12, Lcom/navdy/hud/app/screen/BaseScreen;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v12, v0, v1}, Lcom/navdy/hud/app/screen/BaseScreen;->setArguments(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 881
    :cond_166
    move-object/from16 p1, v4

    .line 882
    instance-of v12, v7, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    if-eqz v12, :cond_179

    .line 883
    sget-object v12, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$ui$Screen:[I

    invoke-virtual {v4}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v13

    aget v12, v12, v13

    sparse-switch v12, :sswitch_data_248

    .line 885
    move-object/from16 p1, v4

    .line 927
    :cond_179
    :goto_179
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isFullscreenMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v12

    if-eqz v12, :cond_188

    .line 928
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setCurrentViewMode(Lcom/navdy/service/library/events/ui/Screen;)V

    .line 930
    :cond_188
    if-eqz v7, :cond_64

    .line 931
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->currentScreen:Lcom/navdy/service/library/events/ui/Screen;

    .line 932
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v12, v7}, Lflow/Flow;->replaceTo(Ljava/lang/Object;)V

    goto/16 :goto_64

    .line 889
    :sswitch_199
    sget-object p1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    .line 890
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->globalPreferences:Landroid/content/SharedPreferences;

    sget-object v13, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v13

    invoke-static {v12, v13}, Lcom/navdy/hud/app/ui/activity/Main;->saveHomeScreenPreference(Landroid/content/SharedPreferences;I)V

    move-object v12, v7

    .line 891
    check-cast v12, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    sget-object v13, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v12, v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    goto :goto_179

    .line 895
    :sswitch_1b1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->globalPreferences:Landroid/content/SharedPreferences;

    sget-object v13, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v13

    invoke-static {v12, v13}, Lcom/navdy/hud/app/ui/activity/Main;->saveHomeScreenPreference(Landroid/content/SharedPreferences;I)V

    move-object v12, v7

    .line 896
    check-cast v12, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    sget-object v13, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v12, v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    .line 897
    sget-object p1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    .line 898
    goto :goto_179

    :sswitch_1c9
    move-object v12, v7

    .line 901
    check-cast v12, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    invoke-virtual {v12}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v6

    .line 902
    .local v6, "displayMode":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v10

    .line 903
    .local v10, "n2":I
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v5

    .line 904
    .local v5, "defaultProfile":Z
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v12

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferencesForCurrentDriverProfile(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 905
    .local v11, "sharedPreferences":Landroid/content/SharedPreferences;
    if-nez v5, :cond_1f4

    if-nez v11, :cond_1f8

    .line 906
    :cond_1f4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->globalPreferences:Landroid/content/SharedPreferences;

    .line 908
    :cond_1f8
    if-eqz v11, :cond_204

    .line 909
    const-string v12, "PREFERENCE_LAST_HOME_SCREEN_MODE"

    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v13

    invoke-interface {v11, v12, v13}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 911
    :cond_204
    move-object/from16 p1, v4

    .line 912
    invoke-virtual {v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v12

    if-eq v10, v12, :cond_179

    .line 913
    sget-object v12, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v12}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v12

    if-ne v10, v12, :cond_227

    move-object v12, v7

    .line 914
    check-cast v12, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    sget-object v13, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v12, v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    .line 919
    :cond_21c
    :goto_21c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->globalPreferences:Landroid/content/SharedPreferences;

    invoke-static {v12, v10}, Lcom/navdy/hud/app/ui/activity/Main;->saveHomeScreenPreference(Landroid/content/SharedPreferences;I)V

    .line 920
    move-object/from16 p1, v4

    .line 921
    goto/16 :goto_179

    .line 916
    :cond_227
    sget-object v12, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v12}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->ordinal()I

    move-result v12

    if-ne v10, v12, :cond_21c

    move-object v12, v7

    .line 917
    check-cast v12, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    sget-object v13, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->SMART_DASH:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual {v12, v13}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;->setDisplayMode(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;)V

    goto :goto_21c

    .line 816
    :pswitch_data_238
    .packed-switch 0x3
        :pswitch_8e
        :pswitch_a9
        :pswitch_d2
    .end packed-switch

    .line 858
    :pswitch_data_242
    .packed-switch 0x6
        :pswitch_128
    .end packed-switch

    .line 883
    :sswitch_data_248
    .sparse-switch
        0x1 -> :sswitch_199
        0x2 -> :sswitch_1b1
        0x7 -> :sswitch_1c9
    .end sparse-switch
.end method

.method private updateSystemTrayVisibility(Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;Lcom/navdy/service/library/events/ui/Screen;)V
    .locals 6
    .param p1, "screenCategory"    # Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;
    .param p2, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    const/4 v5, 0x0

    .line 940
    sget-object v3, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$ui$activity$Main$ScreenCategory:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_48

    .line 970
    :cond_c
    :goto_c
    return-void

    .line 942
    :pswitch_d
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v0

    .line 943
    .local v0, "currentScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v0, :cond_19

    .line 944
    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object p2

    .line 946
    :cond_19
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 947
    .local v1, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {p2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isFullscreenMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 948
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setNotificationColorVisibility(I)V

    .line 949
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setSystemTrayVisibility(I)V

    goto :goto_c

    .line 955
    .end local v0    # "currentScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    .end local v1    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :pswitch_28
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notifAnimationRunning:Z

    if-nez v3, :cond_2f

    .line 956
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->runQueuedOperation()V

    .line 958
    :cond_2f
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notifAnimationRunning:Z

    if-nez v3, :cond_c

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationViewShowing()Z

    move-result v3

    if-nez v3, :cond_c

    .line 961
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 962
    .local v2, "uiStateManager2":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {p2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isFullscreenMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 963
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setNotificationColorVisibility(I)V

    .line 964
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setSystemTrayVisibility(I)V

    goto :goto_c

    .line 940
    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_d
        :pswitch_28
    .end packed-switch
.end method

.method private verifyProtocolVersion(Lcom/navdy/service/library/events/DeviceInfo;)Z
    .locals 10
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 974
    const/4 v0, 0x0

    .line 975
    .local v0, "b":Z
    const/4 v3, -0x1

    .line 978
    .local v3, "int1":I
    :goto_5
    :try_start_5
    iget-object v4, p1, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    const-string v5, "\\."

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 979
    sget-object v4, Lcom/navdy/service/library/Version;->PROTOCOL_VERSION:Lcom/navdy/service/library/Version;

    iget v4, v4, Lcom/navdy/service/library/Version;->majorVersion:I

    if-eq v4, v3, :cond_23

    sget-object v4, Lcom/navdy/service/library/Version;->PROTOCOL_VERSION:Lcom/navdy/service/library/Version;

    iget v4, v4, Lcom/navdy/service/library/Version;->majorVersion:I

    if-ne v4, v9, :cond_48

    if-nez v3, :cond_48

    .line 980
    :cond_23
    sget-object v4, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_VALID:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sput-object v4, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;
    :try_end_27
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_27} :catch_2a

    .line 981
    const/4 v0, 0x1

    move v1, v0

    .line 998
    .end local v0    # "b":Z
    .local v1, "b":I
    :goto_29
    return v1

    .line 985
    .end local v1    # "b":I
    .restart local v0    # "b":Z
    :catch_2a
    move-exception v2

    .line 986
    .local v2, "ex":Ljava/lang/NumberFormatException;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid remote device protocol version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_5

    .line 991
    .end local v2    # "ex":Ljava/lang/NumberFormatException;
    :cond_48
    sget-object v4, Lcom/navdy/service/library/Version;->PROTOCOL_VERSION:Lcom/navdy/service/library/Version;

    iget v4, v4, Lcom/navdy/service/library/Version;->majorVersion:I

    if-ge v4, v3, :cond_6f

    .line 992
    sget-object v4, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_LOCAL_NEEDS_UPDATE:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sput-object v4, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    .line 997
    :goto_52
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "protocol version mismatch remote = %s, local = %s"

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p1, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    aput-object v7, v6, v8

    sget-object v7, Lcom/navdy/service/library/Version;->PROTOCOL_VERSION:Lcom/navdy/service/library/Version;

    invoke-virtual {v7}, Lcom/navdy/service/library/Version;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move v1, v0

    .line 998
    .restart local v1    # "b":I
    goto :goto_29

    .line 995
    .end local v1    # "b":I
    :cond_6f
    sget-object v4, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_REMOTE_NEEDS_UPDATE:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sput-object v4, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    goto :goto_52
.end method


# virtual methods
.method public addNotificationExtensionView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1003
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1004
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_b

    .line 1005
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/MainView;->addNotificationExtensionView(Landroid/view/View;)V

    .line 1007
    :cond_b
    return-void
.end method

.method public clearInputFocus()V
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/InputManager;->getFocus()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    instance-of v0, v0, Lcom/navdy/hud/app/view/ToastView;

    if-nez v0, :cond_19

    .line 1011
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/InputManager;->setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V

    .line 1012
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "[focus] cleared"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1014
    :cond_19
    return-void
.end method

.method public createScreens()V
    .locals 4

    .prologue
    .line 1017
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->createdScreens:Z

    if-nez v2, :cond_1f

    .line 1018
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MainView;

    .line 1019
    .local v1, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v1, :cond_1f

    .line 1020
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "createScreens"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1021
    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView;->getContainerView()Lcom/navdy/hud/app/view/ContainerView;

    move-result-object v0

    .line 1022
    .local v0, "containerView":Lcom/navdy/hud/app/view/ContainerView;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->createdScreens:Z

    .line 1023
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/ContainerView;->createScreens()V

    .line 1026
    .end local v0    # "containerView":Lcom/navdy/hud/app/view/ContainerView;
    .end local v1    # "mainView":Lcom/navdy/hud/app/view/MainView;
    :cond_1f
    return-void
.end method

.method public ejectMainLowerView()V
    .locals 1

    .prologue
    .line 1029
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1030
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_b

    .line 1031
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->ejectMainLowerView()V

    .line 1033
    :cond_b
    return-void
.end method

.method enableNotificationColor(Z)V
    .locals 5
    .param p1, "notificationColorEnabled"    # Z

    .prologue
    const/4 v4, 0x4

    .line 1036
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationColorEnabled:Z

    .line 1037
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MainView;

    .line 1038
    .local v1, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v1, :cond_1b

    .line 1039
    iget-boolean v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationColorEnabled:Z

    if-nez v3, :cond_1c

    .line 1040
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/view/MainView;->setNotificationColorVisibility(I)V

    .line 1041
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "notif invisible"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1057
    :cond_1b
    :goto_1b
    return-void

    .line 1044
    :cond_1c
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v0

    .line 1045
    .local v0, "currentScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v0, :cond_3e

    .line 1046
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 1047
    .local v2, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isFullscreenMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v3

    if-eqz v3, :cond_3e

    .line 1048
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setNotificationColorVisibility(I)V

    .line 1049
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "notif visible"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1b

    .line 1053
    .end local v2    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :cond_3e
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/view/MainView;->setNotificationColorVisibility(I)V

    .line 1054
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "notif invisible"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1b
.end method

.method enableSystemTray(Z)V
    .locals 3
    .param p1, "systemTrayEnabled"    # Z

    .prologue
    .line 1060
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->systemTrayEnabled:Z

    .line 1061
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1062
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_16

    .line 1063
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v1

    .line 1064
    .local v1, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    iget-boolean v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->systemTrayEnabled:Z

    if-nez v2, :cond_17

    .line 1065
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setVisibility(I)V

    .line 1071
    .end local v1    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_16
    :goto_16
    return-void

    .line 1068
    .restart local v1    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_17
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setSystemTrayVisibility(I)V

    goto :goto_16
.end method

.method public getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;
    .locals 1

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    return-object v0
.end method

.method getExpandedNotificationCoverView()Landroid/view/View;
    .locals 2

    .prologue
    .line 1078
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MainView;

    .line 1080
    .local v1, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v1, :cond_d

    .line 1081
    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView;->getExpandedNotificationCoverView()Landroid/view/View;

    move-result-object v0

    .line 1086
    .local v0, "expandedNotificationCoverView":Landroid/view/View;
    :goto_c
    return-object v0

    .line 1084
    .end local v0    # "expandedNotificationCoverView":Landroid/view/View;
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "expandedNotificationCoverView":Landroid/view/View;
    goto :goto_c
.end method

.method getExpandedNotificationView()Landroid/widget/FrameLayout;
    .locals 2

    .prologue
    .line 1090
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/view/MainView;

    .line 1092
    .local v1, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v1, :cond_d

    .line 1093
    invoke-virtual {v1}, Lcom/navdy/hud/app/view/MainView;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 1098
    .local v0, "expandedNotificationView":Landroid/widget/FrameLayout;
    :goto_c
    return-object v0

    .line 1096
    .end local v0    # "expandedNotificationView":Landroid/widget/FrameLayout;
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "expandedNotificationView":Landroid/widget/FrameLayout;
    goto :goto_c
.end method

.method getNotificationExtensionView()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 1102
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1104
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_f

    .line 1105
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getNotificationExtensionView()Landroid/widget/FrameLayout;

    move-result-object v1

    .line 1110
    :goto_c
    check-cast v1, Landroid/view/ViewGroup;

    return-object v1

    .line 1108
    :cond_f
    const/4 v1, 0x0

    .local v1, "notificationExtensionView":Ljava/lang/Object;
    goto :goto_c
.end method

.method getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .locals 2

    .prologue
    .line 1114
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1116
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_d

    .line 1117
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v1

    .line 1122
    .local v1, "notificationIndicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    :goto_c
    return-object v1

    .line 1120
    .end local v1    # "notificationIndicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    :cond_d
    const/4 v1, 0x0

    .restart local v1    # "notificationIndicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    goto :goto_c
.end method

.method getNotificationScrollIndicator()Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    .locals 2

    .prologue
    .line 1126
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1128
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_d

    .line 1129
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getNotificationScrollIndicator()Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    move-result-object v1

    .line 1134
    .local v1, "notificationScrollIndicator":Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    :goto_c
    return-object v1

    .line 1132
    .end local v1    # "notificationScrollIndicator":Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    :cond_d
    const/4 v1, 0x0

    .restart local v1    # "notificationScrollIndicator":Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    goto :goto_c
.end method

.method getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    .locals 2

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1140
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_d

    .line 1141
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v1

    .line 1146
    .local v1, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    :goto_c
    return-object v1

    .line 1144
    .end local v1    # "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    :cond_d
    const/4 v1, 0x0

    .restart local v1    # "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    goto :goto_c
.end method

.method public getScreenConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    .locals 2

    .prologue
    .line 1151
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isPortrait()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1152
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->settings:Lcom/navdy/hud/app/settings/MainScreenSettings;

    invoke-virtual {v1}, Lcom/navdy/hud/app/settings/MainScreenSettings;->getPortraitConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    move-result-object v0

    .line 1157
    .local v0, "screenConfiguration":Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    :goto_c
    return-object v0

    .line 1155
    .end local v0    # "screenConfiguration":Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    :cond_d
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->settings:Lcom/navdy/hud/app/settings/MainScreenSettings;

    invoke-virtual {v1}, Lcom/navdy/hud/app/settings/MainScreenSettings;->getLandscapeConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    move-result-object v0

    .restart local v0    # "screenConfiguration":Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    goto :goto_c
.end method

.method getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;
    .locals 2

    .prologue
    .line 1161
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1163
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-nez v0, :cond_a

    .line 1164
    const/4 v1, 0x0

    .line 1169
    .local v1, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :goto_9
    return-object v1

    .line 1167
    .end local v1    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_a
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v1

    .restart local v1    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    goto :goto_9
.end method

.method public go(Lflow/Backstack;Lflow/Flow$Direction;Lflow/Flow$Callback;)V
    .locals 7
    .param p1, "backstack"    # Lflow/Backstack;
    .param p2, "direction"    # Lflow/Flow$Direction;
    .param p3, "callback"    # Lflow/Flow$Callback;

    .prologue
    .line 1174
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/view/MainView;

    .line 1175
    .local v4, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v4, :cond_3e

    .line 1176
    invoke-virtual {v4}, Lcom/navdy/hud/app/view/MainView;->getContainerView()Lcom/navdy/hud/app/view/ContainerView;

    move-result-object v3

    .line 1177
    .local v3, "containerView":Lcom/navdy/hud/app/view/ContainerView;
    invoke-virtual {p1}, Lflow/Backstack;->current()Lflow/Backstack$Entry;

    move-result-object v5

    invoke-virtual {v5}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/screen/BaseScreen;

    .line 1178
    .local v2, "baseScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    const/4 v0, -0x1

    .line 1179
    .local v0, "animationOut":I
    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v5}, Lflow/Flow;->getBackstack()Lflow/Backstack;

    move-result-object v1

    .line 1180
    .local v1, "backstack2":Lflow/Backstack;
    invoke-virtual {v1}, Lflow/Backstack;->size()I

    move-result v5

    if-lez v5, :cond_33

    .line 1181
    invoke-virtual {v1}, Lflow/Backstack;->current()Lflow/Backstack$Entry;

    move-result-object v5

    invoke-virtual {v5}, Lflow/Backstack$Entry;->getScreen()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/screen/BaseScreen;

    sget-object v6, Lflow/Flow$Direction;->FORWARD:Lflow/Flow$Direction;

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/screen/BaseScreen;->getAnimationOut(Lflow/Flow$Direction;)I

    move-result v0

    .line 1183
    :cond_33
    sget-object v5, Lflow/Flow$Direction;->FORWARD:Lflow/Flow$Direction;

    sget-object v6, Lflow/Flow$Direction;->FORWARD:Lflow/Flow$Direction;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/screen/BaseScreen;->getAnimationIn(Lflow/Flow$Direction;)I

    move-result v6

    invoke-virtual {v3, v2, v5, v6, v0}, Lcom/navdy/hud/app/view/ContainerView;->showScreen(Lmortar/Blueprint;Lflow/Flow$Direction;II)V

    .line 1185
    .end local v0    # "animationOut":I
    .end local v1    # "backstack2":Lflow/Backstack;
    .end local v2    # "baseScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    .end local v3    # "containerView":Lcom/navdy/hud/app/view/ContainerView;
    :cond_3e
    invoke-interface {p3}, Lflow/Flow$Callback;->onComplete()V

    .line 1186
    return-void
.end method

.method public injectMainLowerView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1189
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1190
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_b

    .line 1191
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/view/MainView;->injectMainLowerView(Landroid/view/View;)V

    .line 1193
    :cond_b
    return-void
.end method

.method public isMainLowerViewVisible()Z
    .locals 2

    .prologue
    .line 1196
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1197
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->isMainLowerViewVisible()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method isMainUIShrunk()Z
    .locals 2

    .prologue
    .line 1201
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1202
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->isMainUIShrunk()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method isNotificationCollapsing()Z
    .locals 2

    .prologue
    .line 1206
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1207
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->isNotificationCollapsing()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method isNotificationExpanding()Z
    .locals 2

    .prologue
    .line 1211
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1212
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->isNotificationExpanding()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method isNotificationViewShowing()Z
    .locals 2

    .prologue
    .line 1216
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1217
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->isNotificationViewShowing()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method isScreenAnimating()Z
    .locals 2

    .prologue
    .line 1221
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1222
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->isScreenAnimating()Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 1227
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDeviceConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 4
    .param p1, "connectionStateChange"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1232
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1233
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v0

    .line 1234
    .local v0, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_7c

    .line 1254
    :cond_3d
    :goto_3d
    return-void

    .line 1236
    :pswitch_3e
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager;->initialize()V

    .line 1237
    if-eqz v0, :cond_3d

    .line 1238
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_3d

    .line 1244
    :pswitch_4d
    if-eqz v0, :cond_56

    .line 1245
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1247
    :cond_56
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sget-object v2, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_LOCAL_NEEDS_UPDATE:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    if-ne v1, v2, :cond_76

    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->currentScreen:Lcom/navdy/service/library/events/ui/Screen;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v1, v2, :cond_76

    .line 1248
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1250
    :cond_76
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_VALID:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sput-object v1, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    goto :goto_3d

    .line 1234
    nop

    :pswitch_data_7c
    .packed-switch 0x1
        :pswitch_3e
        :pswitch_4d
    .end packed-switch
.end method

.method public onDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 4
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1258
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->verifyProtocolVersion(Lcom/navdy/service/library/events/DeviceInfo;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 1259
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;-><init>()V

    .line 1260
    .local v0, "builder":Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;
    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->state(Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;

    .line 1261
    iget-object v1, p1, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->remoteDeviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;

    .line 1262
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->build()Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1267
    .end local v0    # "builder":Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;
    :goto_1e
    return-void

    .line 1265
    :cond_1f
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_1e
.end method

.method public onDialBatteryEvent(Lcom/navdy/hud/app/device/dial/DialConstants$BatteryChangeEvent;)V
    .locals 5
    .param p1, "batteryChangeEvent"    # Lcom/navdy/hud/app/device/dial/DialConstants$BatteryChangeEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1271
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v2

    .line 1272
    .local v2, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v2, :cond_18

    .line 1273
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->getBatteryNotificationReason()Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    move-result-object v0

    .line 1274
    .local v0, "batteryNotificationReason":Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    const/4 v1, 0x0

    .line 1275
    .local v1, "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    if-nez v0, :cond_19

    .line 1276
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 1295
    :goto_13
    sget-object v3, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Dial:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    invoke-virtual {v2, v3, v1}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1297
    .end local v0    # "batteryNotificationReason":Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    .end local v1    # "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    :cond_18
    return-void

    .line 1279
    .restart local v0    # "batteryNotificationReason":Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    .restart local v1    # "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    :cond_19
    sget-object v3, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2e

    goto :goto_13

    .line 1281
    :pswitch_25
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 1282
    goto :goto_13

    .line 1286
    :pswitch_28
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 1287
    goto :goto_13

    .line 1290
    :pswitch_2b
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    goto :goto_13

    .line 1279
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_25
        :pswitch_28
        :pswitch_28
        :pswitch_2b
    .end packed-switch
.end method

.method public onDialConnectionEvent(Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;)V
    .locals 4
    .param p1, "dialConnectionStatus"    # Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1301
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v1

    .line 1302
    .local v1, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v1, :cond_13

    .line 1304
    iget-object v2, p1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->status:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    if-ne v2, v3, :cond_14

    .line 1305
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 1310
    .local v0, "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    :goto_e
    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Dial:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1312
    .end local v0    # "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    :cond_13
    return-void

    .line 1308
    :cond_14
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .restart local v0    # "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    goto :goto_e
.end method

.method public onDismissScreen(Lcom/navdy/service/library/events/ui/DismissScreen;)V
    .locals 6
    .param p1, "dismissScreen"    # Lcom/navdy/service/library/events/ui/DismissScreen;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1316
    iget-object v0, p1, Lcom/navdy/service/library/events/ui/DismissScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v0, v1, :cond_10

    .line 1317
    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateScreen(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;ZZ)V

    .line 1319
    :cond_10
    return-void
.end method

.method public onDisplayScaleChange(Lcom/navdy/hud/app/event/DisplayScaleChange;)V
    .locals 1
    .param p1, "displayScaleChange"    # Lcom/navdy/hud/app/event/DisplayScaleChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1323
    iget-object v0, p1, Lcom/navdy/hud/app/event/DisplayScaleChange;->format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setDisplayFormat(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;)V

    .line 1324
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 0
    .param p1, "driverProfileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1328
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateDisplayFormat()V

    .line 1329
    return-void
.end method

.method public onDriverProfileUpdate(Lcom/navdy/hud/app/event/DriverProfileUpdated;)V
    .locals 2
    .param p1, "driverProfileUpdated"    # Lcom/navdy/hud/app/event/DriverProfileUpdated;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1333
    iget-object v0, p1, Lcom/navdy/hud/app/event/DriverProfileUpdated;->state:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    sget-object v1, Lcom/navdy/hud/app/event/DriverProfileUpdated$State;->UPDATED:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    if-ne v0, v1, :cond_9

    .line 1334
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateDisplayFormat()V

    .line 1336
    :cond_9
    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .prologue
    .line 1340
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onExitScope()V

    .line 1341
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Shutting down main service"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1342
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->disconnect()V

    .line 1343
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 4
    .param p1, "gestureEvent"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 1348
    if-eqz p1, :cond_28

    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    if-eqz v2, :cond_28

    .line 1349
    sget-object v2, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2a

    .line 1359
    :goto_13
    const/4 v0, 0x1

    .line 1364
    .local v0, "b":Z
    :goto_14
    return v0

    .line 1351
    .end local v0    # "b":Z
    :pswitch_15
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    .line 1352
    .local v1, "instance":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationCount()I

    move-result v2

    if-lez v2, :cond_24

    .line 1353
    const-string v2, "swipe"

    invoke-static {v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->setGlanceNavigationSource(Ljava/lang/String;)V

    .line 1355
    :cond_24
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->expandNotification()Z

    goto :goto_13

    .line 1362
    .end local v1    # "instance":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_28
    const/4 v0, 0x0

    .restart local v0    # "b":Z
    goto :goto_14

    .line 1349
    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_15
    .end packed-switch
.end method

.method public onGpsSatelliteData(Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;)V
    .locals 6
    .param p1, "gpsSatelliteData"    # Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1369
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 1370
    iget-object v3, p1, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;->data:Landroid/os/Bundle;

    const-string v4, "SAT_FIX_TYPE"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1371
    .local v1, "int1":I
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v2

    .line 1372
    .local v2, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v2, :cond_19

    .line 1373
    const/4 v0, 0x0

    .line 1375
    .local v0, "debugGpsMarker":Ljava/lang/String;
    packed-switch v1, :pswitch_data_26

    .line 1401
    .end local v0    # "debugGpsMarker":Ljava/lang/String;
    .end local v1    # "int1":I
    .end local v2    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_19
    :goto_19
    return-void

    .line 1381
    .restart local v0    # "debugGpsMarker":Ljava/lang/String;
    .restart local v1    # "int1":I
    .restart local v2    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :pswitch_1a
    const-string v0, "DR"

    .line 1382
    goto :goto_19

    .line 1385
    :pswitch_1d
    const-string v0, "Diff"

    .line 1386
    goto :goto_19

    .line 1389
    :pswitch_20
    const-string v0, "2D"

    .line 1394
    :pswitch_22
    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setDebugGpsMarker(Ljava/lang/String;)V

    goto :goto_19

    .line 1375
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_20
        :pswitch_1d
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_1a
    .end packed-switch
.end method

.method public onGpsSwitchEvent(Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;)V
    .locals 5
    .param p1, "gpsSwitch"    # Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1405
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v2

    if-eqz v2, :cond_31

    .line 1406
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "gps_switch_event:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p1, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;->usingPhone:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1407
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v1

    .line 1408
    .local v1, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v1, :cond_31

    .line 1410
    iget-boolean v2, p1, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;->usingPhone:Z

    if-eqz v2, :cond_32

    .line 1411
    const-string v0, "P"

    .line 1416
    .local v0, "debugGpsMarker":Ljava/lang/String;
    :goto_2e
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setDebugGpsMarker(Ljava/lang/String;)V

    .line 1419
    .end local v0    # "debugGpsMarker":Ljava/lang/String;
    .end local v1    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_31
    return-void

    .line 1414
    .restart local v1    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_32
    const-string v0, "U"

    .restart local v0    # "debugGpsMarker":Ljava/lang/String;
    goto :goto_2e
.end method

.method public onInitEvent(Lcom/navdy/hud/app/event/InitEvents$InitPhase;)V
    .locals 7
    .param p1, "initPhase"    # Lcom/navdy/hud/app/event/InitEvents$InitPhase;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1423
    sget-object v4, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    iget-object v5, p1, Lcom/navdy/hud/app/event/InitEvents$InitPhase;->phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v5}, Lcom/navdy/hud/app/event/InitEvents$Phase;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_a4

    .line 1466
    :cond_d
    :goto_d
    return-void

    .line 1425
    :pswitch_e
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v4

    if-eqz v4, :cond_73

    .line 1426
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/device/PowerManager;->enterSleepMode()V

    .line 1431
    :goto_1b
    const-string v4, "persist.sys.demo.video"

    invoke-static {v4}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1433
    .local v3, "value":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7b

    .line 1434
    const/4 v0, 0x1

    .line 1439
    .local v0, "b":Z
    :goto_28
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->preferences:Landroid/content/SharedPreferences;

    const-string v5, "start_video_on_boot_preference"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_35

    if-eqz v0, :cond_d

    .line 1442
    :cond_35
    sget-object v4, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1443
    .local v1, "externalStoragePublicDirectory":Ljava/io/File;
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->signalBootComplete()V

    .line 1444
    if-nez v0, :cond_42

    .line 1445
    const-string v3, "navdy-demo.mov"

    .line 1447
    :cond_42
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1448
    .local v2, "file":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Trying to play video: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1449
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_7d

    .line 1450
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->playMedia(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_d

    .line 1429
    .end local v0    # "b":Z
    .end local v1    # "externalStoragePublicDirectory":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "value":Ljava/lang/String;
    :cond_73
    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->resetFrontLED(Lcom/navdy/hud/app/device/light/LightManager;)V

    goto :goto_1b

    .line 1437
    .restart local v3    # "value":Ljava/lang/String;
    :cond_7b
    const/4 v0, 0x0

    .restart local v0    # "b":Z
    goto :goto_28

    .line 1453
    .restart local v1    # "externalStoragePublicDirectory":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :cond_7d
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Missing demo video file..."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_d

    .line 1457
    .end local v0    # "b":Z
    .end local v1    # "externalStoragePublicDirectory":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "value":Ljava/lang/String;
    :pswitch_87
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "initializing obd manager"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1458
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    .line 1459
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v4

    if-nez v4, :cond_9e

    .line 1460
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->startLateServices()V

    .line 1462
    :cond_9e
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->signalBootComplete()V

    goto/16 :goto_d

    .line 1423
    nop

    :pswitch_data_a4
    .packed-switch 0x1
        :pswitch_e
        :pswitch_87
    .end packed-switch
.end method

.method public onInstallingUpdate(Lcom/navdy/hud/app/util/OTAUpdateService$InstallingUpdate;)V
    .locals 0
    .param p1, "installingUpdate"    # Lcom/navdy/hud/app/util/OTAUpdateService$InstallingUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1470
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->showInstallingUpdateToast()V

    .line 1471
    return-void
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "customKeyEvent"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->innerEventsDetector:Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/gesture/MultipleClickGestureDetector;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1479
    invoke-static {p0}, Lcom/navdy/hud/app/ui/activity/Main;->access$1102(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .line 1480
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 1481
    new-instance v4, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    invoke-direct {v4}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->voiceAssistNotification:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .line 1482
    new-instance v4, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    invoke-direct {v4}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->voiceSearchNotification:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .line 1483
    const/4 v4, 0x0

    iput v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->defaultNotifColor:I

    .line 1484
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$1200()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setRootScreen(Lcom/navdy/hud/app/ui/activity/Main;)V

    .line 1485
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->screenAnimationListener:Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addScreenAnimationListener(Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;)V

    .line 1486
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationAnimationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addNotificationAnimationListener(Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;)V

    .line 1487
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 1488
    const/4 v2, 0x0

    .line 1489
    .local v2, "savedSettings":Lcom/navdy/hud/app/settings/MainScreenSettings;
    if-eqz p1, :cond_3f

    .line 1490
    const-string v4, "main.settings"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .end local v2    # "savedSettings":Lcom/navdy/hud/app/settings/MainScreenSettings;
    check-cast v2, Lcom/navdy/hud/app/settings/MainScreenSettings;

    .line 1492
    .restart local v2    # "savedSettings":Lcom/navdy/hud/app/settings/MainScreenSettings;
    :cond_3f
    if-nez v2, :cond_59

    .line 1493
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->preferences:Landroid/content/SharedPreferences;

    const-string v5, "main.settings"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1495
    .local v1, "json":Ljava/lang/String;
    :try_start_4a
    new-instance v4, Lcom/google/gson/Gson;

    invoke-direct {v4}, Lcom/google/gson/Gson;-><init>()V

    const-class v5, Lcom/navdy/hud/app/settings/MainScreenSettings;

    invoke-virtual {v4, v1, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/navdy/hud/app/settings/MainScreenSettings;

    move-object v2, v0
    :try_end_59
    .catch Ljava/lang/Throwable; {:try_start_4a .. :try_end_59} :catch_b2

    .line 1500
    .end local v1    # "json":Ljava/lang/String;
    :cond_59
    :goto_59
    if-nez v2, :cond_60

    .line 1501
    new-instance v2, Lcom/navdy/hud/app/settings/MainScreenSettings;

    .end local v2    # "savedSettings":Lcom/navdy/hud/app/settings/MainScreenSettings;
    invoke-direct {v2}, Lcom/navdy/hud/app/settings/MainScreenSettings;-><init>()V

    .line 1503
    .restart local v2    # "savedSettings":Lcom/navdy/hud/app/settings/MainScreenSettings;
    :cond_60
    iput-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->settings:Lcom/navdy/hud/app/settings/MainScreenSettings;

    .line 1504
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->settingsManager:Lcom/navdy/hud/app/config/SettingsManager;

    sget-object v5, Lcom/navdy/hud/app/device/gps/GpsUtils;->SHOW_RAW_GPS:Lcom/navdy/hud/app/config/BooleanSetting;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/config/SettingsManager;->addSetting(Lcom/navdy/hud/app/config/Setting;)V

    .line 1505
    new-instance v4, Lcom/navdy/hud/app/settings/HUDSettings;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->preferences:Landroid/content/SharedPreferences;

    invoke-direct {v4, v5}, Lcom/navdy/hud/app/settings/HUDSettings;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->hudSettings:Lcom/navdy/hud/app/settings/HUDSettings;

    .line 1506
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateDisplayFormat()V

    .line 1507
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v4, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 1508
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v4}, Lcom/navdy/hud/app/service/ConnectionHandler;->connect()V

    .line 1509
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setupFlow()V

    .line 1510
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateView()V

    .line 1511
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->dialSimulatorMessagesHandler:Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 1512
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 1513
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->pandoraManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 1514
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 1515
    new-instance v4, Lcom/navdy/hud/app/manager/InitManager;

    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    iget-object v6, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-direct {v4, v5, v6}, Lcom/navdy/hud/app/manager/InitManager;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/service/ConnectionHandler;)V

    iput-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->initManager:Lcom/navdy/hud/app/manager/InitManager;

    .line 1516
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->initManager:Lcom/navdy/hud/app/manager/InitManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/InitManager;->start()V

    .line 1517
    return-void

    .line 1496
    .restart local v1    # "json":Ljava/lang/String;
    :catch_b2
    move-exception v3

    .line 1497
    .local v3, "th":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to parse json "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_59
.end method

.method public onLocationFixChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$LocationFix;)V
    .locals 4
    .param p1, "locationFix"    # Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1521
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v0

    .line 1522
    .local v0, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v0, :cond_23

    .line 1523
    iget-boolean v1, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->locationAvailable:Z

    if-eqz v1, :cond_24

    .line 1524
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Gps:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOCATION_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1525
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 1526
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;

    iget-boolean v2, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingPhoneLocation:Z

    iget-boolean v3, p1, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingLocalGpsLocation:Z

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;-><init>(ZZ)V

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->onGpsSwitchEvent(Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;)V

    .line 1533
    :cond_23
    :goto_23
    return-void

    .line 1530
    :cond_24
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Gps:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOCATION_LOST:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_23
.end method

.method public onNetworkStateChange(Lcom/navdy/service/library/events/settings/NetworkStateChange;)V
    .locals 3
    .param p1, "networkStateChange"    # Lcom/navdy/service/library/events/settings/NetworkStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v0

    .line 1538
    .local v0, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v0, :cond_17

    .line 1539
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1540
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->PHONE_NETWORK_AVAILABLE:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1546
    :cond_17
    :goto_17
    return-void

    .line 1543
    :cond_18
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->NO_PHONE_NETWORK:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_17
.end method

.method public onNotificationChange(Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;)V
    .locals 2
    .param p1, "notificationChange"    # Lcom/navdy/hud/app/framework/notifications/NotificationManager$NotificationChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1550
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1551
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_f

    .line 1552
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->getNotificationColorVisibility()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setNotificationColorVisibility(I)V

    .line 1554
    :cond_f
    return-void
.end method

.method public onPhoneBatteryStatusEvent(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V
    .locals 4
    .param p1, "phoneBatteryStatus"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1558
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhoneBatteryStatus status["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] level["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] charging["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1559
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v0

    .line 1560
    .local v0, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v0, :cond_51

    .line 1561
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 1562
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1581
    :cond_51
    :goto_51
    return-void

    .line 1565
    :cond_52
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_78

    goto :goto_51

    .line 1567
    :pswitch_60
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->OK_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_51

    .line 1571
    :pswitch_68
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_51

    .line 1575
    :pswitch_70
    sget-object v1, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v2, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOW_BATTERY:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    goto :goto_51

    .line 1565
    :pswitch_data_78
    .packed-switch 0x1
        :pswitch_60
        :pswitch_68
        :pswitch_70
    .end packed-switch
.end method

.method public onReadSettings(Lcom/navdy/service/library/events/settings/ReadSettingsRequest;)V
    .locals 11
    .param p1, "readSettingsRequest"    # Lcom/navdy/service/library/events/settings/ReadSettingsRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1585
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v8

    .line 1586
    .local v8, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v8, :cond_4d

    .line 1587
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getScreenConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    move-result-object v9

    .line 1588
    .local v9, "screenConfiguration":Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    invoke-virtual {v9}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->getMargins()Landroid/graphics/Rect;

    move-result-object v7

    .line 1589
    .local v7, "margins":Landroid/graphics/Rect;
    new-instance v10, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;

    new-instance v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget v1, v7, Landroid/graphics/Rect;->left:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, v7, Landroid/graphics/Rect;->top:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v7, Landroid/graphics/Rect;->right:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, v7, Landroid/graphics/Rect;->bottom:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->getScaleX()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v9}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->getScaleY()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Float;)V

    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->hudSettings:Lcom/navdy/hud/app/settings/HUDSettings;

    iget-object v2, p1, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->settings:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/settings/HUDSettings;->readSettings(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v10, v0, v1}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;-><init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration;Ljava/util/List;)V

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 1591
    .end local v7    # "margins":Landroid/graphics/Rect;
    .end local v9    # "screenConfiguration":Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    :cond_4d
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1594
    const-string v0, "main.settings"

    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->settings:Lcom/navdy/hud/app/settings/MainScreenSettings;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1595
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->persistSettings()V

    .line 1596
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->services:Lcom/navdy/hud/app/ui/activity/Main$Presenter$DeferredServices;

    if-eqz v0, :cond_1b

    .line 1597
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/ui/activity/Main$Presenter$7;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter$7;-><init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1604
    :cond_1b
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onSave(Landroid/os/Bundle;)V

    .line 1605
    return-void
.end method

.method public onShowScreen(Lcom/navdy/hud/app/event/ShowScreenWithArgs;)V
    .locals 6
    .param p1, "showScreenWithArgs"    # Lcom/navdy/hud/app/event/ShowScreenWithArgs;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1609
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    if-nez v0, :cond_23

    .line 1610
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Main screen not up, cannot show screen-args:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1615
    :goto_22
    return-void

    .line 1613
    :cond_23
    iget-object v1, p1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->screen:Lcom/navdy/service/library/events/ui/Screen;

    iget-object v2, p1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->args:Landroid/os/Bundle;

    iget-object v3, p1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->args2:Ljava/lang/Object;

    iget-boolean v4, p1, Lcom/navdy/hud/app/event/ShowScreenWithArgs;->ignoreAnimation:Z

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateScreen(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;ZZ)V

    goto :goto_22
.end method

.method public onShowScreen(Lcom/navdy/service/library/events/ui/ShowScreen;)V
    .locals 13
    .param p1, "showScreen"    # Lcom/navdy/service/library/events/ui/ShowScreen;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1619
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    if-nez v0, :cond_25

    .line 1620
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Main screen not up, cannot show screen:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1635
    :goto_24
    return-void

    .line 1622
    :cond_25
    iget-object v0, p1, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    if-eqz v0, :cond_31

    iget-object v0, p1, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3a

    .line 1623
    :cond_31
    iget-object v1, p1, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    move-object v0, p0

    move-object v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateScreen(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;ZZ)V

    goto :goto_24

    .line 1626
    :cond_3a
    iget-object v11, p1, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    .line 1627
    .local v11, "arguments":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1628
    .local v7, "bundle":Landroid/os/Bundle;
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_45
    :goto_45
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_61

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/navdy/service/library/events/glances/KeyValue;

    .line 1629
    .local v12, "keyValue":Lcom/navdy/service/library/events/glances/KeyValue;
    iget-object v1, v12, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    if-eqz v1, :cond_45

    iget-object v1, v12, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    if-eqz v1, :cond_45

    .line 1630
    iget-object v1, v12, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    iget-object v3, v12, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_45

    .line 1633
    .end local v12    # "keyValue":Lcom/navdy/service/library/events/glances/KeyValue;
    :cond_61
    iget-object v6, p1, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    move-object v5, p0

    move-object v8, v2

    move v9, v4

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateScreen(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;ZZ)V

    goto :goto_24
.end method

.method public onShutdownEvent(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 6
    .param p1, "shutdown"    # Lcom/navdy/hud/app/event/Shutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1639
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$1;->$SwitchMap$com$navdy$hud$app$event$Shutdown$State:[I

    iget-object v2, p1, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    invoke-virtual {v2}, Lcom/navdy/hud/app/event/Shutdown$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3a

    .line 1651
    :goto_d
    return-void

    .line 1641
    :pswitch_e
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    iget-object v4, p1, Lcom/navdy/hud/app/event/Shutdown;->reason:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-virtual {v4}, Lcom/navdy/hud/app/event/Shutdown$Reason;->asBundle()Landroid/os/Bundle;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_d

    .line 1645
    :pswitch_22
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1646
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Stopping update service"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1647
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_d

    .line 1639
    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_e
        :pswitch_22
    .end packed-switch
.end method

.method public onUpdateSettings(Lcom/navdy/service/library/events/settings/UpdateSettings;)V
    .locals 7
    .param p1, "updateSettings"    # Lcom/navdy/service/library/events/settings/UpdateSettings;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1655
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/UpdateSettings;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .line 1656
    .local v0, "screenConfiguration":Lcom/navdy/service/library/events/settings/ScreenConfiguration;
    if-eqz v0, :cond_3a

    .line 1657
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getScreenConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    move-result-object v1

    .line 1658
    .local v1, "screenConfiguration2":Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->setMargins(Landroid/graphics/Rect;)V

    .line 1659
    iget-object v2, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->setScaleX(F)V

    .line 1660
    iget-object v2, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->setScaleY(F)V

    .line 1662
    .end local v1    # "screenConfiguration2":Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;
    :cond_3a
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->hudSettings:Lcom/navdy/hud/app/settings/HUDSettings;

    iget-object v3, p1, Lcom/navdy/service/library/events/settings/UpdateSettings;->settings:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/settings/HUDSettings;->updateSettings(Ljava/util/List;)V

    .line 1663
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->persistSettings()V

    .line 1664
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateView()V

    .line 1665
    return-void
.end method

.method public onWakeUp(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 0
    .param p1, "wakeup"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1669
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->startLateServices()V

    .line 1670
    return-void
.end method

.method public playMedia(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1755
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playMedia:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1757
    :try_start_1a
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1758
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "video/mp4"

    invoke-virtual {v1, p2, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1759
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1760
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_2e} :catch_2f

    .line 1765
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_2e
    return-void

    .line 1762
    :catch_2f
    move-exception v0

    .line 1763
    .local v0, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Unable to start video loop"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2e
.end method

.method public removeNotificationExtensionView()V
    .locals 1

    .prologue
    .line 1768
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/MainView;

    .line 1769
    .local v0, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v0, :cond_b

    .line 1770
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/MainView;->removeNotificationExtensionView()V

    .line 1772
    :cond_b
    return-void
.end method

.method public setInputFocus()V
    .locals 6

    .prologue
    .line 1775
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/InputManager;->getFocus()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v3

    instance-of v3, v3, Lcom/navdy/hud/app/view/ToastView;

    if-eqz v3, :cond_14

    .line 1776
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[focus] not setting focus[toast]"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1806
    :goto_13
    return-void

    .line 1779
    :cond_14
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/MainView;

    .line 1780
    .local v2, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-nez v2, :cond_26

    .line 1781
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[focus] mainview is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_13

    .line 1783
    :cond_26
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationViewShowing()Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 1784
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/MainView;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/manager/InputManager;->setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V

    .line 1785
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[focus] notification"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_13

    .line 1788
    :cond_3f
    invoke-virtual {v2}, Lcom/navdy/hud/app/view/MainView;->getContainerView()Lcom/navdy/hud/app/view/ContainerView;

    move-result-object v0

    .line 1789
    .local v0, "containerView":Lcom/navdy/hud/app/view/ContainerView;
    if-nez v0, :cond_54

    .line 1790
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/manager/InputManager;->setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V

    .line 1791
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[focus] mainView"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_13

    .line 1794
    :cond_54
    invoke-virtual {v0}, Lcom/navdy/hud/app/view/ContainerView;->getCurrentView()Landroid/view/View;

    move-result-object v1

    .line 1795
    .local v1, "currentView":Landroid/view/View;
    instance-of v3, v1, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    if-eqz v3, :cond_89

    .line 1796
    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    move-object v3, v1

    check-cast v3, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    invoke-virtual {v4, v3}, Lcom/navdy/hud/app/manager/InputManager;->setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V

    .line 1797
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[focus] screen:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    check-cast v1, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    .end local v1    # "currentView":Landroid/view/View;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_13

    .line 1800
    .restart local v1    # "currentView":Landroid/view/View;
    :cond_89
    iget-object v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/manager/InputManager;->setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V

    .line 1801
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[focus] mainView"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_13
.end method

.method public setMargins(IIII)V
    .locals 3
    .param p1, "n"    # I
    .param p2, "n2"    # I
    .param p3, "n3"    # I
    .param p4, "n4"    # I

    .prologue
    const/4 v2, 0x0

    .line 1809
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getScreenConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    move-result-object v0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v2, p2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->setMargins(Landroid/graphics/Rect;)V

    .line 1810
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->persistSettings()V

    .line 1811
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->updateView()V

    .line 1812
    return-void
.end method

.method setNotificationColorVisibility(I)V
    .locals 7
    .param p1, "n"    # I

    .prologue
    .line 1815
    iget-boolean v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationColorEnabled:Z

    if-nez v5, :cond_e

    .line 1816
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "notifColor is disabled"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1849
    :cond_d
    :goto_d
    return-void

    .line 1819
    :cond_e
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/MainView;

    .line 1820
    .local v2, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v2, :cond_d

    .line 1821
    if-nez p1, :cond_66

    .line 1822
    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v5

    if-eqz v5, :cond_36

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationExpanding()Z

    move-result v5

    if-nez v5, :cond_2c

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationViewShowing()Z

    move-result v5

    if-eqz v5, :cond_36

    .line 1823
    :cond_2c
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "not showing notifcolor, pending notif"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_d

    .line 1827
    :cond_36
    iget-object v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotificationColor()I

    move-result v3

    .local v3, "notificationColor":I
    const/4 v5, -0x1

    if-ne v3, v5, :cond_41

    .line 1828
    iget v3, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->defaultNotifColor:I

    .line 1830
    :cond_41
    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/view/MainView;->setNotificationColor(I)V

    .line 1831
    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/view/MainView;->setNotificationColorVisibility(I)V

    .line 1837
    .end local v3    # "notificationColor":I
    :goto_47
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    .line 1838
    .local v0, "access$200":Lcom/navdy/service/library/log/Logger;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifColor: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1840
    .local v1, "append":Ljava/lang/StringBuilder;
    if-nez p1, :cond_6f

    .line 1841
    const-string v4, "visible"

    .line 1846
    .local v4, "s":Ljava/lang/String;
    :goto_5a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_d

    .line 1834
    .end local v0    # "access$200":Lcom/navdy/service/library/log/Logger;
    .end local v1    # "append":Ljava/lang/StringBuilder;
    .end local v4    # "s":Ljava/lang/String;
    :cond_66
    iget v5, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->defaultNotifColor:I

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/view/MainView;->setNotificationColor(I)V

    .line 1835
    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/view/MainView;->setNotificationColorVisibility(I)V

    goto :goto_47

    .line 1844
    .restart local v0    # "access$200":Lcom/navdy/service/library/log/Logger;
    .restart local v1    # "append":Ljava/lang/StringBuilder;
    :cond_6f
    const-string v4, "invisible"

    .restart local v4    # "s":Ljava/lang/String;
    goto :goto_5a
.end method

.method public setSystemTrayVisibility(I)V
    .locals 6
    .param p1, "visibility"    # I

    .prologue
    .line 1852
    iget-boolean v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->systemTrayEnabled:Z

    if-nez v4, :cond_e

    .line 1853
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "systemtray is disabled"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1876
    :cond_d
    :goto_d
    return-void

    .line 1856
    :cond_e
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/view/MainView;

    .line 1857
    .local v2, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v2, :cond_d

    .line 1858
    if-nez p1, :cond_36

    iget-object v4, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v4

    if-eqz v4, :cond_36

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationExpanding()Z

    move-result v4

    if-nez v4, :cond_2c

    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationViewShowing()Z

    move-result v4

    if-eqz v4, :cond_36

    .line 1859
    :cond_2c
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "not showing systemtray, pending notif"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_d

    .line 1862
    :cond_36
    invoke-virtual {v2}, Lcom/navdy/hud/app/view/MainView;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setVisibility(I)V

    .line 1863
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    .line 1864
    .local v0, "access$200":Lcom/navdy/service/library/log/Logger;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "systemtray is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1866
    .local v1, "append":Ljava/lang/StringBuilder;
    if-nez p1, :cond_5c

    .line 1867
    const-string v3, "visible"

    .line 1872
    .local v3, "s":Ljava/lang/String;
    :goto_50
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_d

    .line 1870
    .end local v3    # "s":Ljava/lang/String;
    :cond_5c
    const-string v3, "invisible"

    .restart local v3    # "s":Ljava/lang/String;
    goto :goto_50
.end method

.method public startVoiceAssistance(Z)V
    .locals 6
    .param p1, "b"    # Z

    .prologue
    const/4 v5, 0x1

    .line 1879
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startVoiceAssistance , Voice search : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1880
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationViewShowing()Z

    move-result v2

    if-eqz v2, :cond_54

    .line 1881
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "startVoiceAssistance:add pending"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1882
    iget-object v1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 1884
    .local v1, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    if-eqz p1, :cond_4b

    .line 1885
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->voiceSearchNotification:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .line 1890
    .local v0, "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    :goto_32
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addPendingNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 1891
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpanded()Z

    move-result v2

    if-nez v2, :cond_45

    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isExpandedNotificationVisible()Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 1892
    :cond_45
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2, v5, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseExpandedNotification(ZZ)V

    .line 1901
    .end local v0    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    .end local v1    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :goto_4a
    return-void

    .line 1888
    .restart local v1    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_4b
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->voiceAssistNotification:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .restart local v0    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    goto :goto_32

    .line 1895
    :cond_4e
    iget-object v2, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->collapseNotification()Z

    goto :goto_4a

    .line 1899
    .end local v0    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    .end local v1    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_54
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->launchVoiceAssistance(Z)V

    goto :goto_4a
.end method

.method public updateView()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1904
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/view/MainView;

    .line 1905
    .local v3, "mainView":Lcom/navdy/hud/app/view/MainView;
    if-eqz v3, :cond_61

    .line 1906
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getScreenConfiguration()Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/settings/MainScreenSettings$ScreenConfiguration;->getMargins()Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v8}, Lcom/navdy/hud/app/view/MainView;->setVerticalOffset(I)V

    .line 1907
    invoke-virtual {p0, v9}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setNotificationColorVisibility(I)V

    .line 1908
    invoke-virtual {p0, v9}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setSystemTrayVisibility(I)V

    .line 1909
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getSystemTray()Lcom/navdy/hud/app/ui/component/SystemTrayView;

    move-result-object v7

    .line 1910
    .local v7, "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    if-eqz v7, :cond_61

    .line 1911
    sget-object v0, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Dial:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    .line 1913
    .local v0, "dial":Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v8

    if-eqz v8, :cond_62

    .line 1914
    sget-object v5, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 1919
    .local v5, "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    :goto_30
    invoke-virtual {v7, v0, v5}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1920
    sget-object v4, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Phone:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    .line 1922
    .local v4, "phone":Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v8

    if-eqz v8, :cond_65

    .line 1923
    sget-object v6, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->CONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .line 1928
    .local v6, "state2":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    :goto_41
    invoke-virtual {v7, v4, v6}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1929
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    .line 1930
    .local v1, "instance":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v8

    if-eqz v8, :cond_61

    .line 1931
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v2

    .line 1932
    .local v2, "locationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    if-eqz v2, :cond_61

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->hasLocationFix()Z

    move-result v8

    if-nez v8, :cond_61

    .line 1933
    sget-object v8, Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;->Gps:Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;

    sget-object v9, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->LOCATION_LOST:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    invoke-virtual {v7, v8, v9}, Lcom/navdy/hud/app/ui/component/SystemTrayView;->setState(Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;Lcom/navdy/hud/app/ui/component/SystemTrayView$State;)V

    .line 1938
    .end local v0    # "dial":Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    .end local v1    # "instance":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .end local v2    # "locationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .end local v4    # "phone":Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    .end local v5    # "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    .end local v6    # "state2":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    .end local v7    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_61
    return-void

    .line 1917
    .restart local v0    # "dial":Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    .restart local v7    # "systemTray":Lcom/navdy/hud/app/ui/component/SystemTrayView;
    :cond_62
    sget-object v5, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .restart local v5    # "state":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    goto :goto_30

    .line 1926
    .restart local v4    # "phone":Lcom/navdy/hud/app/ui/component/SystemTrayView$Device;
    :cond_65
    sget-object v6, Lcom/navdy/hud/app/ui/component/SystemTrayView$State;->DISCONNECTED:Lcom/navdy/hud/app/ui/component/SystemTrayView$State;

    .restart local v6    # "state2":Lcom/navdy/hud/app/ui/component/SystemTrayView$State;
    goto :goto_41
.end method
