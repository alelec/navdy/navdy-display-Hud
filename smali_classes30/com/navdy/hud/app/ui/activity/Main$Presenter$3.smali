.class Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/ui/activity/Main$Presenter;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .prologue
    .line 535
    iput-object p1, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "notificationType"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    const/4 v3, 0x4

    .line 538
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification anim start id["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]  type["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] mode["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 539
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p3, v0, :cond_43

    .line 540
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setNotificationColorVisibility(I)V

    .line 541
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setSystemTrayVisibility(I)V

    .line 543
    :cond_43
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->clearInputFocus()V

    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->access$402(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Z)Z

    .line 545
    return-void
.end method

.method public onStop(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "notificationType"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 549
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification anim stop id["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]  type["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] mode["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 550
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->access$402(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Z)Z

    .line 551
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p3, v0, :cond_46

    .line 552
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;->NOTIFICATION:Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->access$300(Lcom/navdy/hud/app/ui/activity/Main$Presenter;Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;Lcom/navdy/service/library/events/ui/Screen;)V

    .line 554
    :cond_46
    iget-object v0, p0, Lcom/navdy/hud/app/ui/activity/Main$Presenter$3;->this$0:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-static {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->access$500(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)V

    .line 555
    return-void
.end method
