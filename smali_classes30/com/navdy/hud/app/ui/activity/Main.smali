.class public Lcom/navdy/hud/app/ui/activity/Main;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/activity/Main$ScreenInfo;,
        Lcom/navdy/hud/app/ui/activity/Main$ScreenCategory;,
        Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;,
        Lcom/navdy/hud/app/ui/activity/Main$Presenter;,
        Lcom/navdy/hud/app/ui/activity/Main$Module;,
        Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;
    }
.end annotation


# static fields
.field public static final DEMO_VIDEO_PROPERTY:Ljava/lang/String; = "persist.sys.demo.video"

.field private static final INSTALLING_OTA_TOAST_ID:Ljava/lang/String; = "#ota#toast"

.field public static final MIME_TYPE_VIDEO:Ljava/lang/String; = "video/mp4"

.field static final PREFERENCE_HOME_SCREEN_MODE:Ljava/lang/String; = "PREFERENCE_LAST_HOME_SCREEN_MODE"

.field private static SCREEN_MAP:Landroid/util/SparseArray; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final SNAPSHOT_TITLE_PICKER_DELAY_MILLIS:I = 0x7d0

.field private static final TRIPLE_CLICK_BRIGHTNESS:Z

.field public static mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

.field private static mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

.field private static presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 185
    const-string v0, "persist.hud.tclick.brightness"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/ui/activity/Main;->TRIPLE_CLICK_BRIGHTNESS:Z

    .line 189
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ui/activity/Main;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 190
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FIRST_LAUNCH:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/FirstLaunchScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 191
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 192
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/WelcomeScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 193
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/OSUpdateConfirmationScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/ShutDownScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/AutoBrightnessScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FACTORY_RESET:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/FactoryResetScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/DialManagerScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_PROGRESS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 200
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TEMPERATURE_WARNING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/TemperatureWarningScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 201
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/ForceUpdateScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 202
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/DialUpdateConfirmationScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 203
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/screen/GestureLearningScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 205
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC_DETAILS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v1

    const-class v2, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 206
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_VALID:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    .line 207
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    .line 210
    sput-object p0, Lcom/navdy/hud/app/ui/activity/Main;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 211
    return-void
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/ui/activity/Main$Presenter;)Lcom/navdy/hud/app/ui/activity/Main$Presenter;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    .prologue
    .line 173
    sput-object p0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    return-object p0
.end method

.method static synthetic access$1200()Lcom/navdy/hud/app/ui/activity/Main;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->mainScreen:Lcom/navdy/hud/app/ui/activity/Main;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$600()Z
    .locals 1

    .prologue
    .line 173
    sget-boolean v0, Lcom/navdy/hud/app/ui/activity/Main;->TRIPLE_CLICK_BRIGHTNESS:Z

    return v0
.end method

.method static synthetic access$700()V
    .locals 0

    .prologue
    .line 173
    invoke-static {}, Lcom/navdy/hud/app/ui/activity/Main;->showBrightnessNotification()V

    return-void
.end method

.method static synthetic access$800()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->SCREEN_MAP:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static handleLibraryInitializationError(Ljava/lang/String;)V
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 214
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forcing update due to initialization failure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 215
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;->PROTOCOL_LOCAL_NEEDS_UPDATE:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    sput-object v0, Lcom/navdy/hud/app/ui/activity/Main;->mProtocolStatus:Lcom/navdy/hud/app/ui/activity/Main$ProtocolStatus;

    .line 216
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_37

    .line 217
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    iget-object v0, v0, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 222
    :goto_36
    return-void

    .line 220
    :cond_37
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unable to launch FORCE_UPDATE screen."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_36
.end method

.method public static saveHomeScreenPreference(Landroid/content/SharedPreferences;I)V
    .locals 4
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p1, "n"    # I

    .prologue
    .line 225
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v0

    .line 226
    .local v0, "defaultProfile":Z
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v2

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferencesForCurrentDriverProfile(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 227
    .local v1, "localPreferencesForCurrentDriverProfile":Landroid/content/SharedPreferences;
    if-nez v0, :cond_2d

    if-eqz v1, :cond_2d

    .line 228
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "PREFERENCE_LAST_HOME_SCREEN_MODE"

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 230
    :cond_2d
    if-eqz p0, :cond_3c

    .line 231
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "PREFERENCE_LAST_HOME_SCREEN_MODE"

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 233
    :cond_3c
    return-void
.end method

.method private static showBrightnessNotification()V
    .locals 2

    .prologue
    .line 236
    sget-boolean v0, Lcom/navdy/hud/app/settings/HUDSettings;->USE_ADAPTIVE_AUTOBRIGHTNESS:Z

    if-eqz v0, :cond_f

    .line 237
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Use adaptive autobrightness"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 238
    invoke-static {}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->showNotification()V

    .line 244
    :goto_e
    return-void

    .line 241
    :cond_f
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Use non-adaptive autobrightness"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/navdy/hud/app/framework/BrightnessNotification;->showNotification()V

    goto :goto_e
.end method


# virtual methods
.method public addNotificationExtensionView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 247
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 248
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->addNotificationExtensionView(Landroid/view/View;)V

    .line 250
    :cond_9
    return-void
.end method

.method public clearInputFocus()V
    .locals 1

    .prologue
    .line 253
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 254
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->clearInputFocus()V

    .line 256
    :cond_9
    return-void
.end method

.method public ejectMainLowerView()V
    .locals 1

    .prologue
    .line 259
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 260
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->ejectMainLowerView()V

    .line 262
    :cond_9
    return-void
.end method

.method public enableNotificationColor(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 265
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 266
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->enableNotificationColor(Z)V

    .line 268
    :cond_9
    return-void
.end method

.method public enableSystemTray(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 271
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 272
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->enableSystemTray(Z)V

    .line 274
    :cond_9
    return-void
.end method

.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 278
    const/4 v0, -0x1

    return v0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 283
    const/4 v0, -0x1

    return v0
.end method

.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/navdy/hud/app/ui/activity/Main$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/activity/Main$Module;-><init>(Lcom/navdy/hud/app/ui/activity/Main;)V

    return-object v0
.end method

.method public getExpandedNotificationCoverView()Landroid/view/View;
    .locals 2

    .prologue
    .line 293
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-nez v1, :cond_6

    .line 294
    const/4 v0, 0x0

    .line 299
    .local v0, "expandedNotificationCoverView":Landroid/view/View;
    :goto_5
    return-object v0

    .line 297
    .end local v0    # "expandedNotificationCoverView":Landroid/view/View;
    :cond_6
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getExpandedNotificationCoverView()Landroid/view/View;

    move-result-object v0

    .restart local v0    # "expandedNotificationCoverView":Landroid/view/View;
    goto :goto_5
.end method

.method public getExpandedNotificationView()Landroid/widget/FrameLayout;
    .locals 2

    .prologue
    .line 304
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-nez v1, :cond_6

    .line 305
    const/4 v0, 0x0

    .line 310
    .local v0, "expandedNotificationView":Landroid/widget/FrameLayout;
    :goto_5
    return-object v0

    .line 308
    .end local v0    # "expandedNotificationView":Landroid/widget/FrameLayout;
    :cond_6
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getExpandedNotificationView()Landroid/widget/FrameLayout;

    move-result-object v0

    .restart local v0    # "expandedNotificationView":Landroid/widget/FrameLayout;
    goto :goto_5
.end method

.method public getInputHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationExtensionViewHolder()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 324
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v1, :cond_b

    .line 325
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getNotificationExtensionView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 330
    .local v0, "notificationExtensionView":Landroid/view/ViewGroup;
    :goto_a
    return-object v0

    .line 328
    .end local v0    # "notificationExtensionView":Landroid/view/ViewGroup;
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "notificationExtensionView":Landroid/view/ViewGroup;
    goto :goto_a
.end method

.method public getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    .locals 2

    .prologue
    .line 335
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-nez v1, :cond_6

    .line 336
    const/4 v0, 0x0

    .line 341
    .local v0, "notificationIndicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    :goto_5
    return-object v0

    .line 339
    .end local v0    # "notificationIndicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    :cond_6
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getNotificationIndicator()Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;

    move-result-object v0

    .restart local v0    # "notificationIndicator":Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
    goto :goto_5
.end method

.method public getNotificationScrollIndicator()Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    .locals 2

    .prologue
    .line 346
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-nez v1, :cond_6

    .line 347
    const/4 v0, 0x0

    .line 352
    .local v0, "notificationScrollIndicator":Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    :goto_5
    return-object v0

    .line 350
    .end local v0    # "notificationScrollIndicator":Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    :cond_6
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getNotificationScrollIndicator()Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;

    move-result-object v0

    .restart local v0    # "notificationScrollIndicator":Lcom/navdy/hud/app/ui/component/carousel/ProgressIndicator;
    goto :goto_5
.end method

.method public getNotificationView()Lcom/navdy/hud/app/view/NotificationView;
    .locals 2

    .prologue
    .line 357
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-nez v1, :cond_6

    .line 358
    const/4 v0, 0x0

    .line 363
    .local v0, "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    :goto_5
    return-object v0

    .line 361
    .end local v0    # "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    :cond_6
    sget-object v1, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->getNotificationView()Lcom/navdy/hud/app/view/NotificationView;

    move-result-object v0

    .restart local v0    # "notificationView":Lcom/navdy/hud/app/view/NotificationView;
    goto :goto_5
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x0

    return-object v0
.end method

.method public handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V
    .locals 1
    .param p1, "customKeyEvent"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 372
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 373
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    .line 375
    :cond_9
    return-void
.end method

.method public injectMainLowerView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 378
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 379
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->injectMainLowerView(Landroid/view/View;)V

    .line 381
    :cond_9
    return-void
.end method

.method public isMainLowerViewVisible()Z
    .locals 1

    .prologue
    .line 384
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isMainLowerViewVisible()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public isMainUIShrunk()Z
    .locals 1

    .prologue
    .line 388
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isMainUIShrunk()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public isNotificationCollapsing()Z
    .locals 1

    .prologue
    .line 392
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationCollapsing()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public isNotificationExpanding()Z
    .locals 1

    .prologue
    .line 396
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationExpanding()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public isNotificationViewShowing()Z
    .locals 1

    .prologue
    .line 400
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isNotificationViewShowing()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public isScreenAnimating()Z
    .locals 1

    .prologue
    .line 404
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->isScreenAnimating()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public removeNotificationExtensionView()V
    .locals 1

    .prologue
    .line 408
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 409
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->removeNotificationExtensionView()V

    .line 411
    :cond_9
    return-void
.end method

.method public setInputFocus()V
    .locals 1

    .prologue
    .line 414
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 415
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setInputFocus()V

    .line 417
    :cond_9
    return-void
.end method

.method public setSystemTrayVisibility(I)V
    .locals 1
    .param p1, "systemTrayVisibility"    # I

    .prologue
    .line 420
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    if-eqz v0, :cond_9

    .line 421
    sget-object v0, Lcom/navdy/hud/app/ui/activity/Main;->presenter:Lcom/navdy/hud/app/ui/activity/Main$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main$Presenter;->setSystemTrayVisibility(I)V

    .line 423
    :cond_9
    return-void
.end method
