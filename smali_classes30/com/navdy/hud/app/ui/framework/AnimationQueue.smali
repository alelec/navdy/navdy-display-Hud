.class public Lcom/navdy/hud/app/ui/framework/AnimationQueue;
.super Ljava/lang/Object;
.source "AnimationQueue.java"


# instance fields
.field private animationRunning:Z

.field private animatorQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/animation/AnimatorSet;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animatorQueue:Ljava/util/Queue;

    .line 15
    new-instance v0, Lcom/navdy/hud/app/ui/framework/AnimationQueue$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/ui/framework/AnimationQueue$1;-><init>(Lcom/navdy/hud/app/ui/framework/AnimationQueue;)V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->defaultAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/ui/framework/AnimationQueue;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/framework/AnimationQueue;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animationRunning:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/ui/framework/AnimationQueue;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/framework/AnimationQueue;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->runQueuedAnimation()V

    return-void
.end method

.method private runQueuedAnimation()V
    .locals 2

    .prologue
    .line 40
    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animatorQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animatorQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    .line 44
    .local v0, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->startAnimation(Landroid/animation/AnimatorSet;)V

    goto :goto_0
.end method


# virtual methods
.method public isAnimationRunning()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animationRunning:Z

    return v0
.end method

.method public startAnimation(Landroid/animation/AnimatorSet;)V
    .locals 1
    .param p1, "animatorSet"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animationRunning:Z

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animationRunning:Z

    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->defaultAnimationListener:Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;

    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 32
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/AnimationQueue;->animatorQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
