.class public Lcom/navdy/hud/app/ui/framework/BasePresenter;
.super Lmortar/ViewPresenter;
.source "BasePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lmortar/ViewPresenter",
        "<TV;>;"
    }
.end annotation


# instance fields
.field protected active:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    .local p0, "this":Lcom/navdy/hud/app/ui/framework/BasePresenter;, "Lcom/navdy/hud/app/ui/framework/BasePresenter<TV;>;"
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public dropView(Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/navdy/hud/app/ui/framework/BasePresenter;, "Lcom/navdy/hud/app/ui/framework/BasePresenter<TV;>;"
    .local p1, "view":Landroid/view/View;, "TV;"
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 29
    .local v0, "v":Landroid/view/View;
    if-nez v0, :cond_0

    .line 37
    :goto_0
    return-void

    .line 32
    :cond_0
    if-ne p1, v0, :cond_1

    .line 33
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/ui/framework/BasePresenter;->active:Z

    .line 34
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 36
    :cond_1
    invoke-super {p0, v0}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    .local p0, "this":Lcom/navdy/hud/app/ui/framework/BasePresenter;, "Lcom/navdy/hud/app/ui/framework/BasePresenter<TV;>;"
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->dropView(Landroid/view/View;)V

    return-void
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/navdy/hud/app/ui/framework/BasePresenter;, "Lcom/navdy/hud/app/ui/framework/BasePresenter<TV;>;"
    iget-boolean v0, p0, Lcom/navdy/hud/app/ui/framework/BasePresenter;->active:Z

    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 16
    .local p0, "this":Lcom/navdy/hud/app/ui/framework/BasePresenter;, "Lcom/navdy/hud/app/ui/framework/BasePresenter<TV;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ui/framework/BasePresenter;->active:Z

    .line 17
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 18
    return-void
.end method

.method protected onUnload()V
    .locals 0

    .prologue
    .line 24
    .local p0, "this":Lcom/navdy/hud/app/ui/framework/BasePresenter;, "Lcom/navdy/hud/app/ui/framework/BasePresenter<TV;>;"
    return-void
.end method
