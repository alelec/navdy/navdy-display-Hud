.class public Lcom/navdy/hud/app/ui/framework/UIStateManager;
.super Ljava/lang/Object;
.source "UIStateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;
    }
.end annotation


# static fields
.field private static final FULLSCREEN_MODES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAIN_SCREENS_SET:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private static final OVERLAY_MODES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private static final PAUSE_HOME_SCREEN_SET:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private static final SIDE_PANEL_MODES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile currentDashboardType:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

.field private currentScreen:Lcom/navdy/hud/app/screen/BaseScreen;

.field private volatile currentViewMode:Lcom/navdy/service/library/events/ui/Screen;

.field private defaultMainActiveScreen:Lcom/navdy/service/library/events/ui/Screen;

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private mainActiveScreen:Lcom/navdy/hud/app/screen/BaseScreen;

.field private mainPanelWidth:I

.field private notificationListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;",
            ">;"
        }
    .end annotation
.end field

.field private rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

.field private screenListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;",
            ">;"
        }
    .end annotation
.end field

.field private sidePanelWidth:I

.field private toastView:Lcom/navdy/hud/app/view/ToastView;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 50
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->SIDE_PANEL_MODES:Ljava/util/EnumSet;

    .line 55
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    const/16 v1, 0x11

    new-array v1, v1, [Lcom/navdy/service/library/events/ui/Screen;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v4

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECENT_CALLS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v5

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECOMMENDED_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v6

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v7

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_CONTACTS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SETTINGS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FACTORY_RESET:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_REPORT_ISSUE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_TOAST:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC_DETAILS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->OVERLAY_MODES:Ljava/util/EnumSet;

    .line 77
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FIRST_LAUNCH:Lcom/navdy/service/library/events/ui/Screen;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-static {v0, v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->FULLSCREEN_MODES:Ljava/util/EnumSet;

    .line 87
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    const/16 v1, 0xd

    new-array v1, v1, [Lcom/navdy/service/library/events/ui/Screen;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECENT_CALLS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v4

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_RECOMMENDED_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v5

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_PLACES:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v6

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v7

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FAVORITE_CONTACTS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SHUTDOWN_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_SETTINGS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_AUTO_BRIGHTNESS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FACTORY_RESET:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_REPORT_ISSUE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FORCE_UPDATE:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC_DETAILS:Lcom/navdy/service/library/events/ui/Screen;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->PAUSE_HOME_SCREEN_SET:Ljava/util/EnumSet;

    .line 105
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->MAIN_SCREENS_SET:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HOME:Lcom/navdy/service/library/events/ui/Screen;

    iput-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->defaultMainActiveScreen:Lcom/navdy/service/library/events/ui/Screen;

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->screenListeners:Ljava/util/HashSet;

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->notificationListeners:Ljava/util/HashSet;

    return-void
.end method

.method private checkListener(Ljava/lang/Object;)V
    .locals 1
    .param p1, "listener"    # Ljava/lang/Object;

    .prologue
    .line 243
    if-nez p1, :cond_0

    .line 244
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 246
    :cond_0
    return-void
.end method

.method public static isFullscreenMode(Lcom/navdy/service/library/events/ui/Screen;)Z
    .locals 1
    .param p0, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 118
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->FULLSCREEN_MODES:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isOverlayMode(Lcom/navdy/service/library/events/ui/Screen;)Z
    .locals 1
    .param p0, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 114
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->OVERLAY_MODES:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isPauseHomescreen(Lcom/navdy/service/library/events/ui/Screen;)Z
    .locals 1
    .param p0, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 122
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->PAUSE_HOME_SCREEN_SET:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isSidePanelMode(Lcom/navdy/service/library/events/ui/Screen;)Z
    .locals 1
    .param p0, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 110
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->SIDE_PANEL_MODES:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addNotificationAnimationListener(Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    .prologue
    .line 228
    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->notificationListeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 229
    :try_start_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->checkListener(Ljava/lang/Object;)V

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->notificationListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 231
    monitor-exit v1

    .line 232
    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addScreenAnimationListener(Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->checkListener(Ljava/lang/Object;)V

    .line 218
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->screenListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 219
    return-void
.end method

.method public enableNotificationColor(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 351
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main;->enableNotificationColor(Z)V

    .line 354
    :cond_0
    return-void
.end method

.method public enableSystemTray(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 339
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main;->enableSystemTray(Z)V

    .line 342
    :cond_0
    return-void
.end method

.method public getCurrentDashboardType()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->currentDashboardType:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    return-object v0
.end method

.method public getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->currentScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    return-object v0
.end method

.method public getCurrentViewMode()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->currentViewMode:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method

.method public getCustomAnimateMode()Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    .locals 2

    .prologue
    .line 275
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 276
    .local v0, "mode":Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isMainUIShrunk()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 277
    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationViewShowing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->isNotificationExpanding()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 278
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_LEFT:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 281
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->EXPAND:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 282
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isModeVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 283
    sget-object v0, Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;->SHRINK_MODE:Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;

    .line 285
    :cond_2
    return-object v0
.end method

.method public getDefaultMainActiveScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->defaultMainActiveScreen:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method

.method public getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    return-object v0
.end method

.method public getMainActiveScreen()Lcom/navdy/hud/app/screen/BaseScreen;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->mainActiveScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    return-object v0
.end method

.method public getMainPanelWidth()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->mainPanelWidth:I

    return v0
.end method

.method public getMainScreensSet()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/service/library/events/ui/Screen;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->MAIN_SCREENS_SET:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    return-object v0
.end method

.method public getSidePanelWidth()I
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->sidePanelWidth:I

    return v0
.end method

.method public getSmartDashView()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getSmartDashView()Lcom/navdy/hud/app/ui/component/homescreen/BaseSmartDashView;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/homescreen/SmartDashView;

    .line 137
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getToastView()Lcom/navdy/hud/app/view/ToastView;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->toastView:Lcom/navdy/hud/app/view/ToastView;

    return-object v0
.end method

.method public isDialPairingScreenOn()Z
    .locals 3

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v0

    .line 299
    .local v0, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v1, v2, :cond_0

    .line 300
    const/4 v1, 0x1

    .line 302
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDialPairingScreenScanning()Z
    .locals 4

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v1

    .line 308
    .local v1, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v2, v3, :cond_0

    move-object v0, v1

    .line 309
    check-cast v0, Lcom/navdy/hud/app/screen/DialManagerScreen;

    .line 310
    .local v0, "dialManagerScreen":Lcom/navdy/hud/app/screen/DialManagerScreen;
    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/DialManagerScreen;->isScanningMode()Z

    move-result v2

    .line 312
    .end local v0    # "dialManagerScreen":Lcom/navdy/hud/app/screen/DialManagerScreen;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isMainUIShrunk()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->isMainUIShrunk()Z

    move-result v0

    .line 162
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNavigationActive()Z
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 358
    .local v0, "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v0, :cond_0

    .line 359
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->isNavigationActive()Z

    move-result v1

    .line 361
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isWelcomeScreenOn()Z
    .locals 3

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v0

    .line 290
    .local v0, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v1, v2, :cond_0

    .line 291
    const/4 v1, 0x1

    .line 293
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public postNotificationAnimationEvent(ZLjava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 4
    .param p1, "start"    # Z
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p4, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 259
    iget-object v2, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->notificationListeners:Ljava/util/HashSet;

    monitor-enter v2

    .line 260
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->notificationListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    .line 261
    .local v0, "listener":Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;
    if-eqz p1, :cond_0

    .line 262
    invoke-interface {v0, p2, p3, p4}, Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;->onStart(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    goto :goto_0

    .line 267
    .end local v0    # "listener":Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 264
    .restart local v0    # "listener":Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;
    :cond_0
    :try_start_1
    invoke-interface {v0, p2, p3, p4}, Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;->onStop(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V

    goto :goto_0

    .line 267
    .end local v0    # "listener":Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268
    return-void
.end method

.method public postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 3
    .param p1, "start"    # Z
    .param p2, "in"    # Lcom/navdy/hud/app/screen/BaseScreen;
    .param p3, "out"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    .line 249
    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->screenListeners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    .line 250
    .local v0, "listener":Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;
    if-eqz p1, :cond_0

    .line 251
    invoke-interface {v0, p2, p3}, Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;->onStart(Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    goto :goto_0

    .line 253
    :cond_0
    invoke-interface {v0, p2, p3}, Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;->onStop(Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    goto :goto_0

    .line 256
    .end local v0    # "listener":Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;
    :cond_1
    return-void
.end method

.method public removeNotificationAnimationListener(Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    .prologue
    .line 235
    iget-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->notificationListeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 236
    if-eqz p1, :cond_0

    .line 237
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->notificationListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 239
    :cond_0
    monitor-exit v1

    .line 240
    return-void

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeScreenAnimationListener(Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/hud/app/ui/framework/IScreenAnimationListener;

    .prologue
    .line 222
    if-eqz p1, :cond_0

    .line 223
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->screenListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 225
    :cond_0
    return-void
.end method

.method public setCurrentDashboardType(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;)V
    .locals 0
    .param p1, "dashboardType"    # Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->currentDashboardType:Lcom/navdy/hud/app/ui/component/homescreen/SmartDashViewConstants$Type;

    .line 151
    return-void
.end method

.method public setCurrentViewMode(Lcom/navdy/service/library/events/ui/Screen;)V
    .locals 0
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->currentViewMode:Lcom/navdy/service/library/events/ui/Screen;

    .line 143
    return-void
.end method

.method public setHomescreenView(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V
    .locals 0
    .param p1, "view"    # Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 182
    return-void
.end method

.method public setInputFocus()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->setInputFocus()V

    .line 348
    :cond_0
    return-void
.end method

.method public setMainActiveScreen(Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 2
    .param p1, "screen"    # Lcom/navdy/hud/app/screen/BaseScreen;

    .prologue
    .line 166
    invoke-virtual {p1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isFullscreenMode(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 167
    iput-object p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->mainActiveScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    .line 168
    invoke-virtual {p1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v0

    .line 169
    .local v0, "s":Lcom/navdy/service/library/events/ui/Screen;
    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_FIRST_LAUNCH:Lcom/navdy/service/library/events/ui/Screen;

    if-eq v0, v1, :cond_0

    .line 172
    iput-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->defaultMainActiveScreen:Lcom/navdy/service/library/events/ui/Screen;

    .line 177
    .end local v0    # "s":Lcom/navdy/service/library/events/ui/Screen;
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->currentScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    .line 178
    return-void

    .line 175
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->mainActiveScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    goto :goto_0
.end method

.method public setMainPanelWidth(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 321
    iput p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->mainPanelWidth:I

    .line 322
    return-void
.end method

.method public setRootScreen(Lcom/navdy/hud/app/ui/activity/Main;)V
    .locals 0
    .param p1, "rootScreen"    # Lcom/navdy/hud/app/ui/activity/Main;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    .line 206
    return-void
.end method

.method public setSidePanelWidth(I)V
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 329
    iput p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->sidePanelWidth:I

    .line 330
    return-void
.end method

.method public setToastView(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 0
    .param p1, "toastView"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 194
    return-void
.end method

.method public showSystemTray(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 333
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/navdy/hud/app/ui/framework/UIStateManager;->rootScreen:Lcom/navdy/hud/app/ui/activity/Main;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/activity/Main;->setSystemTrayVisibility(I)V

    .line 336
    :cond_0
    return-void
.end method
