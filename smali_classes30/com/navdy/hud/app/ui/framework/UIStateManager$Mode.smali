.class public final enum Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;
.super Ljava/lang/Enum;
.source "UIStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ui/framework/UIStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

.field public static final enum COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

.field public static final enum EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    const-string v1, "EXPAND"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .line 46
    new-instance v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    const-string v1, "COLLAPSE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    sget-object v1, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->$VALUES:[Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->$VALUES:[Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    return-object v0
.end method
