.class public final Lcom/navdy/hud/app/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final animationDuration:I = 0x7f010033

.field public static final auto_popup:I = 0x7f010066

.field public static final batteryLevel:I = 0x7f010058

.field public static final choice2_icon_halo_duration:I = 0x7f010056

.field public static final choice2_icon_halo_end_radius:I = 0x7f010053

.field public static final choice2_icon_halo_middle_radius:I = 0x7f010054

.field public static final choice2_icon_halo_size:I = 0x7f010051

.field public static final choice2_icon_halo_start_delay:I = 0x7f010055

.field public static final choice2_icon_halo_start_radius:I = 0x7f010052

.field public static final choice2_icon_size:I = 0x7f010050

.field public static final choice2_item_padding:I = 0x7f01004e

.field public static final choice2_item_top_padding:I = 0x7f01004d

.field public static final choice2_text_size:I = 0x7f01004f

.field public static final choice_text_padding_bottom:I = 0x7f01004b

.field public static final choice_text_padding_left:I = 0x7f010048

.field public static final choice_text_padding_right:I = 0x7f010049

.field public static final choice_text_padding_top:I = 0x7f01004a

.field public static final choice_text_size:I = 0x7f01004c

.field public static final circleIndicatorFocusSize:I = 0x7f01003e

.field public static final circleIndicatorMargin:I = 0x7f01003f

.field public static final circleIndicatorSize:I = 0x7f01003d

.field public static final constraintSet:I = 0x7f010000

.field public static final defaultArtwork:I = 0x7f01002f

.field public static final dialState:I = 0x7f010057

.field public static final fluctuator_alpha_animation:I = 0x7f010060

.field public static final fluctuator_duration:I = 0x7f01005e

.field public static final fluctuator_duration_delay:I = 0x7f01005f

.field public static final fluctuator_end_radius:I = 0x7f01005c

.field public static final fluctuator_fill_color:I = 0x7f01005a

.field public static final fluctuator_shrink_animation:I = 0x7f010061

.field public static final fluctuator_start_radius:I = 0x7f01005b

.field public static final fluctuator_stroke_color:I = 0x7f010059

.field public static final fluctuator_stroke_width:I = 0x7f01005d

.field public static final fontFile:I = 0x7f010062

.field public static final fontPath:I = 0x7f010001

.field public static final gaugeBackgroundColor:I = 0x7f01006f

.field public static final gaugeBackgroundThickness:I = 0x7f01006a

.field public static final gaugeCenterSubText:I = 0x7f010074

.field public static final gaugeCenterText:I = 0x7f010073

.field public static final gaugeEndColor:I = 0x7f010078

.field public static final gaugeMaxValue:I = 0x7f01006d

.field public static final gaugeMinValue:I = 0x7f01006c

.field public static final gaugePivotValue:I = 0x7f010030

.field public static final gaugeShadowColor:I = 0x7f010079

.field public static final gaugeShadowThickness:I = 0x7f01006b

.field public static final gaugeStartAngle:I = 0x7f010067

.field public static final gaugeStartColor:I = 0x7f010077

.field public static final gaugeSubTextSize:I = 0x7f010076

.field public static final gaugeSweepAngle:I = 0x7f010068

.field public static final gaugeTextColor:I = 0x7f010072

.field public static final gaugeTextSize:I = 0x7f010075

.field public static final gaugeThickness:I = 0x7f010069

.field public static final gaugeTicInterval:I = 0x7f01007d

.field public static final gaugeTicLength:I = 0x7f01007c

.field public static final gaugeTicPadding:I = 0x7f01007b

.field public static final gaugeTicStyle:I = 0x7f01007a

.field public static final gaugeValue:I = 0x7f01006e

.field public static final gaugeWarningColor:I = 0x7f010071

.field public static final gaugeWarningValue:I = 0x7f010070

.field public static final halo_delay:I = 0x7f010085

.field public static final halo_duration:I = 0x7f010084

.field public static final halo_end_radius:I = 0x7f010082

.field public static final halo_middle_radius:I = 0x7f010083

.field public static final halo_start_radius:I = 0x7f010081

.field public static final halo_stroke_color:I = 0x7f010080

.field public static final indicatorCurveRadius:I = 0x7f010088

.field public static final indicatorHeight:I = 0x7f010087

.field public static final indicatorStrokeWidth:I = 0x7f010089

.field public static final indicatorValue:I = 0x7f01008a

.field public static final indicatorWidth:I = 0x7f010086

.field public static final interpolator:I = 0x7f010034

.field public static final layout:I = 0x7f01007f

.field public static final layoutManager:I = 0x7f01008f

.field public static final layout_constraintBaseline_creator:I = 0x7f010002

.field public static final layout_constraintBaseline_toBaselineOf:I = 0x7f010003

.field public static final layout_constraintBottom_creator:I = 0x7f010004

.field public static final layout_constraintBottom_toBottomOf:I = 0x7f010005

.field public static final layout_constraintBottom_toTopOf:I = 0x7f010006

.field public static final layout_constraintDimensionRatio:I = 0x7f010007

.field public static final layout_constraintEnd_toEndOf:I = 0x7f010008

.field public static final layout_constraintEnd_toStartOf:I = 0x7f010009

.field public static final layout_constraintGuide_begin:I = 0x7f01000a

.field public static final layout_constraintGuide_end:I = 0x7f01000b

.field public static final layout_constraintGuide_percent:I = 0x7f01000c

.field public static final layout_constraintHeight_default:I = 0x7f01000d

.field public static final layout_constraintHeight_max:I = 0x7f01000e

.field public static final layout_constraintHeight_min:I = 0x7f01000f

.field public static final layout_constraintHorizontal_bias:I = 0x7f010010

.field public static final layout_constraintHorizontal_chainStyle:I = 0x7f010011

.field public static final layout_constraintHorizontal_weight:I = 0x7f010012

.field public static final layout_constraintLeft_creator:I = 0x7f010013

.field public static final layout_constraintLeft_toLeftOf:I = 0x7f010014

.field public static final layout_constraintLeft_toRightOf:I = 0x7f010015

.field public static final layout_constraintRight_creator:I = 0x7f010016

.field public static final layout_constraintRight_toLeftOf:I = 0x7f010017

.field public static final layout_constraintRight_toRightOf:I = 0x7f010018

.field public static final layout_constraintStart_toEndOf:I = 0x7f010019

.field public static final layout_constraintStart_toStartOf:I = 0x7f01001a

.field public static final layout_constraintTop_creator:I = 0x7f01001b

.field public static final layout_constraintTop_toBottomOf:I = 0x7f01001c

.field public static final layout_constraintTop_toTopOf:I = 0x7f01001d

.field public static final layout_constraintVertical_bias:I = 0x7f01001e

.field public static final layout_constraintVertical_chainStyle:I = 0x7f01001f

.field public static final layout_constraintVertical_weight:I = 0x7f010020

.field public static final layout_constraintWidth_default:I = 0x7f010021

.field public static final layout_constraintWidth_max:I = 0x7f010022

.field public static final layout_constraintWidth_min:I = 0x7f010023

.field public static final layout_editor_absoluteX:I = 0x7f010024

.field public static final layout_editor_absoluteY:I = 0x7f010025

.field public static final layout_goneMarginBottom:I = 0x7f010026

.field public static final layout_goneMarginEnd:I = 0x7f010027

.field public static final layout_goneMarginLeft:I = 0x7f010028

.field public static final layout_goneMarginRight:I = 0x7f010029

.field public static final layout_goneMarginStart:I = 0x7f01002a

.field public static final layout_goneMarginTop:I = 0x7f01002b

.field public static final layout_optimizationLevel:I = 0x7f01002c

.field public static final mainImageSize:I = 0x7f010037

.field public static final mainLeftPadding:I = 0x7f010038

.field public static final mainRightPadding:I = 0x7f010039

.field public static final mask:I = 0x7f01002e

.field public static final maskColor:I = 0x7f01008d

.field public static final maskHorizontalRadius:I = 0x7f01008b

.field public static final maskVerticalRadius:I = 0x7f01008c

.field public static final maxWidth:I = 0x7f01008e

.field public static final metaButtonBarButtonStyle:I = 0x7f010032

.field public static final metaButtonBarStyle:I = 0x7f010031

.field public static final notification_anim_direction:I = 0x7f010065

.field public static final notification_footer:I = 0x7f010064

.field public static final notification_title:I = 0x7f010063

.field public static final progress_currentitem_padding_radius:I = 0x7f010043

.field public static final progress_full_background:I = 0x7f010044

.field public static final progress_item_padding:I = 0x7f010042

.field public static final progress_item_radius:I = 0x7f010041

.field public static final progress_round_radius:I = 0x7f010040

.field public static final progress_view_bar_parent_size:I = 0x7f010047

.field public static final progress_view_bar_size:I = 0x7f010046

.field public static final progress_view_padding:I = 0x7f010045

.field public static final reverseLayout:I = 0x7f010091

.field public static final rightImageStart:I = 0x7f01003a

.field public static final rightSectionStart:I = 0x7f01003b

.field public static final rightSectionWidth:I = 0x7f01003c

.field public static final riv_border_color:I = 0x7f010095

.field public static final riv_border_width:I = 0x7f010094

.field public static final riv_corner_radius:I = 0x7f010093

.field public static final riv_mutate_background:I = 0x7f010096

.field public static final riv_oval:I = 0x7f010097

.field public static final riv_tile_mode:I = 0x7f010098

.field public static final riv_tile_mode_x:I = 0x7f010099

.field public static final riv_tile_mode_y:I = 0x7f01009a

.field public static final shutdownState:I = 0x7f01009b

.field public static final sideImageSize:I = 0x7f010036

.field public static final spanCount:I = 0x7f010090

.field public static final stackFromEnd:I = 0x7f010092

.field public static final typeface:I = 0x7f01002d

.field public static final value_text_size:I = 0x7f01007e

.field public static final viewPadding:I = 0x7f010035

.field public static final welcomeState:I = 0x7f01009c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
