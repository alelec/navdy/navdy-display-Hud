.class public interface abstract Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
.super Ljava/lang/Object;
.source "MusicManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/MusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MusicUpdateListener"
.end annotation


# virtual methods
.method public abstract onAlbumArtUpdate(Lokio/ByteString;Z)V
    .param p1    # Lokio/ByteString;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;Z)V"
        }
    .end annotation
.end method
