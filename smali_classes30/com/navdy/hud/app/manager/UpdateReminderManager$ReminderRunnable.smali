.class Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;
.super Ljava/lang/Object;
.source "UpdateReminderManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/UpdateReminderManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReminderRunnable"
.end annotation


# instance fields
.field isDialUpdate:Z

.field final synthetic this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/UpdateReminderManager;Z)V
    .locals 0
    .param p2, "isDial"    # Z

    .prologue
    .line 61
    iput-object p1, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean p2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->isDialUpdate:Z

    .line 63
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 66
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    .local v0, "args":Landroid/os/Bundle;
    iget-boolean v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->isDialUpdate:Z

    if-eqz v2, :cond_3

    .line 68
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v1

    .line 69
    .local v1, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 70
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->isUpdateAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    const-string v2, "UPDATE_REMINDER"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    sget-boolean v2, Lcom/navdy/hud/app/screen/DialUpdateProgressScreen;->updateStarted:Z

    if-nez v2, :cond_0

    .line 73
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/UpdateReminderManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_UPDATE_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v3, v4, v0, v5}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 78
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dial_reminder_time"

    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 80
    # getter for: Lcom/navdy/hud/app/manager/UpdateReminderManager;->REMINDER_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->access$000()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 78
    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 80
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 103
    .end local v1    # "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    # invokes: Lcom/navdy/hud/app/manager/UpdateReminderManager;->scheduleReminder()V
    invoke-static {v2}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->access$300(Lcom/navdy/hud/app/manager/UpdateReminderManager;)V

    .line 104
    return-void

    .line 83
    .restart local v1    # "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dial_reminder_time"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 88
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dial_reminder_time"

    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    # getter for: Lcom/navdy/hud/app/manager/UpdateReminderManager;->DIAL_CONNECT_CHECK_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->access$100()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 88
    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 89
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 93
    .end local v1    # "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    # invokes: Lcom/navdy/hud/app/manager/UpdateReminderManager;->isOTAUpdateRequired()Z
    invoke-static {v2}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->access$200(Lcom/navdy/hud/app/manager/UpdateReminderManager;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 94
    const-string v2, "UPDATE_REMINDER"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 95
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/UpdateReminderManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OTA_CONFIRMATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v3, v4, v0, v5}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 97
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ota_reminder_time"

    .line 98
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    # getter for: Lcom/navdy/hud/app/manager/UpdateReminderManager;->REMINDER_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->access$000()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 97
    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 98
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 100
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;->this$0:Lcom/navdy/hud/app/manager/UpdateReminderManager;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ota_reminder_time"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
