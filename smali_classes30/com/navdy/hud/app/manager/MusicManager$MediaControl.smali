.class public final enum Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
.super Ljava/lang/Enum;
.source "MusicManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/MusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaControl"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

.field public static final enum MUSIC_MENU:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

.field public static final enum MUSIC_MENU_DEEP:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

.field public static final enum NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

.field public static final enum PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

.field public static final enum PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

.field public static final enum PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 97
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 98
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 99
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 100
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 102
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const-string v1, "MUSIC_MENU"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 103
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const-string v1, "MUSIC_MENU_DEEP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU_DEEP:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 96
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU_DEEP:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->$VALUES:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    const-class v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->$VALUES:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    return-object v0
.end method
