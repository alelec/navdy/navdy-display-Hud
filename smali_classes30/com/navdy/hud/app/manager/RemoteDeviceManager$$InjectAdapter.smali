.class public final Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "RemoteDeviceManager$$InjectAdapter.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/manager/RemoteDeviceManager;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/manager/RemoteDeviceManager;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private calendarManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/framework/calendar/CalendarManager;",
            ">;"
        }
    .end annotation
.end field

.field private callManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/framework/phonecall/CallManager;",
            ">;"
        }
    .end annotation
.end field

.field private connectionHandler:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/service/ConnectionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private driveRecorder:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/debug/DriveRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private featureUtil:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/util/FeatureUtil;",
            ">;"
        }
    .end annotation
.end field

.field private gestureServiceConnector:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/gesture/GestureServiceConnector;",
            ">;"
        }
    .end annotation
.end field

.field private httpManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/service/library/network/http/IHttpManager;",
            ">;"
        }
    .end annotation
.end field

.field private inputManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/manager/InputManager;",
            ">;"
        }
    .end annotation
.end field

.field private musicManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager;",
            ">;"
        }
    .end annotation
.end field

.field private preferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private telemetryDataManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/analytics/TelemetryDataManager;",
            ">;"
        }
    .end annotation
.end field

.field private timeHelper:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/common/TimeHelper;",
            ">;"
        }
    .end annotation
.end field

.field private tripManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/framework/trips/TripManager;",
            ">;"
        }
    .end annotation
.end field

.field private uiStateManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private voiceSearchHandler:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 42
    const/4 v0, 0x0

    const-string v1, "members/com.navdy.hud.app.manager.RemoteDeviceManager"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 43
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 52
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    .line 53
    const-string v0, "com.navdy.hud.app.service.ConnectionHandler"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    .line 54
    const-string v0, "com.navdy.hud.app.framework.phonecall.CallManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->callManager:Ldagger/internal/Binding;

    .line 55
    const-string v0, "com.navdy.hud.app.manager.InputManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->inputManager:Ldagger/internal/Binding;

    .line 56
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->preferences:Ldagger/internal/Binding;

    .line 57
    const-string v0, "com.navdy.hud.app.common.TimeHelper"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->timeHelper:Ldagger/internal/Binding;

    .line 58
    const-string v0, "com.navdy.hud.app.framework.calendar.CalendarManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->calendarManager:Ldagger/internal/Binding;

    .line 59
    const-string v0, "com.navdy.hud.app.gesture.GestureServiceConnector"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->gestureServiceConnector:Ldagger/internal/Binding;

    .line 60
    const-string v0, "com.navdy.hud.app.ui.framework.UIStateManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    .line 61
    const-string v0, "com.navdy.hud.app.framework.trips.TripManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    .line 62
    const-string v0, "com.navdy.hud.app.debug.DriveRecorder"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->driveRecorder:Ldagger/internal/Binding;

    .line 63
    const-string v0, "com.navdy.hud.app.manager.MusicManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->musicManager:Ldagger/internal/Binding;

    .line 64
    const-string v0, "com.navdy.hud.app.framework.voice.VoiceSearchHandler"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->voiceSearchHandler:Ldagger/internal/Binding;

    .line 65
    const-string v0, "com.navdy.hud.app.util.FeatureUtil"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->featureUtil:Ldagger/internal/Binding;

    .line 66
    const-string v0, "com.navdy.service.library.network.http.IHttpManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->httpManager:Ldagger/internal/Binding;

    .line 67
    const-string v0, "com.navdy.hud.app.analytics.TelemetryDataManager"

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->telemetryDataManager:Ldagger/internal/Binding;

    .line 68
    return-void
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->callManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->inputManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->preferences:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->timeHelper:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->calendarManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->gestureServiceConnector:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->driveRecorder:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->musicManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->voiceSearchHandler:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->featureUtil:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->httpManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->telemetryDataManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/manager/RemoteDeviceManager;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->bus:Lcom/squareup/otto/Bus;

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/ConnectionHandler;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    .line 102
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->callManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->inputManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/InputManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->preferences:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->preferences:Landroid/content/SharedPreferences;

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->timeHelper:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/common/TimeHelper;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->calendarManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->calendarManager:Lcom/navdy/hud/app/framework/calendar/CalendarManager;

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->gestureServiceConnector:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->uiStateManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->tripManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/trips/TripManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    .line 110
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->driveRecorder:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/debug/DriveRecorder;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->musicManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/MusicManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->voiceSearchHandler:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->featureUtil:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/util/FeatureUtil;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->featureUtil:Lcom/navdy/hud/app/util/FeatureUtil;

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->httpManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/network/http/IHttpManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->httpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->telemetryDataManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    iput-object v0, p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->telemetryDataManager:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    .line 116
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/manager/RemoteDeviceManager;)V

    return-void
.end method
