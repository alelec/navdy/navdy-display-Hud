.class public interface abstract Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.super Ljava/lang/Object;
.source "InputManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/InputManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IInputHandler"
.end annotation


# virtual methods
.method public abstract nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
.end method

.method public abstract onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
.end method

.method public abstract onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
.end method
