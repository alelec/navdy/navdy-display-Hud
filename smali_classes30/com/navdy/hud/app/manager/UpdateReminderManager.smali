.class public Lcom/navdy/hud/app/manager/UpdateReminderManager;
.super Ljava/lang/Object;
.source "UpdateReminderManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;
    }
.end annotation


# static fields
.field private static DIAL_CONNECT_CHECK_INTERVAL:J = 0x0L

.field public static final DIAL_REMINDER_TIME:Ljava/lang/String; = "dial_reminder_time"

.field public static final EXTRA_UPDATE_REMINDER:Ljava/lang/String; = "UPDATE_REMINDER"

.field private static INITIAL_REMINDER_INTERVAL:J = 0x0L

.field public static final OTA_REMINDER_TIME:Ljava/lang/String; = "ota_reminder_time"

.field private static REMINDER_INTERVAL:J

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dialUpdateAvailable:Z

.field private handler:Landroid/os/Handler;

.field private reminderRunnable:Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private timeAvailable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/manager/UpdateReminderManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 34
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->INITIAL_REMINDER_INTERVAL:J

    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->REMINDER_INTERVAL:J

    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->DIAL_CONNECT_CHECK_INTERVAL:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->handler:Landroid/os/Handler;

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->reminderRunnable:Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

    .line 108
    iput-boolean v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->timeAvailable:Z

    .line 109
    iput-boolean v2, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->dialUpdateAvailable:Z

    .line 49
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 28
    sget-wide v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->REMINDER_INTERVAL:J

    return-wide v0
.end method

.method static synthetic access$100()J
    .locals 2

    .prologue
    .line 28
    sget-wide v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->DIAL_CONNECT_CHECK_INTERVAL:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/manager/UpdateReminderManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/UpdateReminderManager;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->isOTAUpdateRequired()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/manager/UpdateReminderManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/UpdateReminderManager;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->scheduleReminder()V

    return-void
.end method

.method private clearReminder()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->reminderRunnable:Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->reminderRunnable:Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->reminderRunnable:Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

    .line 154
    :cond_0
    return-void
.end method

.method private isOTAUpdateRequired()Z
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->isUpdateAvailable()Z

    move-result v0

    return v0
.end method

.method private scheduleReminder()V
    .locals 20

    .prologue
    .line 116
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->timeAvailable:Z

    if-nez v7, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->clearReminder()V

    .line 120
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v14, "ota_reminder_time"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v7, v14, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 121
    .local v10, "ota_time":J
    const-wide/16 v8, 0x0

    .line 122
    .local v8, "dial_time":J
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->dialUpdateAvailable:Z

    if-eqz v7, :cond_2

    .line 123
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v14, "dial_reminder_time"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v7, v14, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 125
    :cond_2
    const-wide/16 v12, 0x0

    .line 126
    .local v12, "time":J
    const/4 v6, 0x0

    .line 128
    .local v6, "dialUpdate":Z
    const-wide/16 v14, 0x0

    cmp-long v7, v10, v14

    if-eqz v7, :cond_6

    const-wide/16 v14, 0x0

    cmp-long v7, v8, v14

    if-eqz v7, :cond_3

    cmp-long v7, v10, v8

    if-gtz v7, :cond_6

    .line 129
    :cond_3
    move-wide v12, v10

    .line 134
    :cond_4
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 135
    .local v2, "curTime":J
    const-wide/16 v14, 0x0

    cmp-long v7, v12, v14

    if-eqz v7, :cond_0

    .line 136
    sub-long v4, v12, v2

    .line 137
    .local v4, "delta":J
    if-eqz v6, :cond_7

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v14, 0x1

    invoke-virtual {v7, v14, v15}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v14

    cmp-long v7, v4, v14

    if-gez v7, :cond_7

    .line 138
    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v14, 0x1

    invoke-virtual {v7, v14, v15}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 143
    :cond_5
    :goto_2
    sget-object v14, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "new scheduling %s reminder for +%d seconds."

    const/4 v7, 0x2

    new-array v0, v7, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    if-eqz v6, :cond_8

    const-string v7, "dial"

    :goto_3
    aput-object v7, v16, v17

    const/4 v7, 0x1

    sget-object v17, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v16, v7

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v14, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 144
    new-instance v7, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v6}, Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;-><init>(Lcom/navdy/hud/app/manager/UpdateReminderManager;Z)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->reminderRunnable:Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

    .line 145
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->reminderRunnable:Lcom/navdy/hud/app/manager/UpdateReminderManager$ReminderRunnable;

    invoke-virtual {v7, v14, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 130
    .end local v2    # "curTime":J
    .end local v4    # "delta":J
    :cond_6
    const-wide/16 v14, 0x0

    cmp-long v7, v8, v14

    if-eqz v7, :cond_4

    .line 131
    move-wide v12, v8

    .line 132
    const/4 v6, 0x1

    goto :goto_1

    .line 139
    .restart local v2    # "curTime":J
    .restart local v4    # "delta":J
    :cond_7
    const-wide/16 v14, 0x0

    cmp-long v7, v4, v14

    if-gez v7, :cond_5

    .line 140
    const-wide/16 v4, 0x0

    goto :goto_2

    .line 143
    :cond_8
    const-string v7, "OTA"

    goto :goto_3
.end method


# virtual methods
.method public handleDialUpdate(Lcom/navdy/hud/app/device/dial/DialManager$DialUpdateStatus;)V
    .locals 12
    .param p1, "event"    # Lcom/navdy/hud/app/device/dial/DialManager$DialUpdateStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v11, 0x0

    const-wide/16 v6, 0x0

    const/4 v10, 0x1

    .line 159
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "dial_reminder_time"

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 160
    .local v2, "time":J
    iget-boolean v4, p1, Lcom/navdy/hud/app/device/dial/DialManager$DialUpdateStatus;->available:Z

    if-eqz v4, :cond_2

    .line 162
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 163
    .local v0, "curTime":J
    cmp-long v4, v2, v6

    if-nez v4, :cond_1

    .line 164
    sget-wide v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->INITIAL_REMINDER_INTERVAL:J

    add-long v2, v0, v4

    .line 165
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "dial_reminder_time"

    invoke-interface {v4, v5, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 166
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleDialUpdate(): setting dial update reminder time to +%d seconds"

    new-array v6, v10, [Ljava/lang/Object;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v8, Lcom/navdy/hud/app/manager/UpdateReminderManager;->INITIAL_REMINDER_INTERVAL:J

    .line 167
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v11

    .line 166
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 173
    :goto_0
    iput-boolean v10, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->dialUpdateAvailable:Z

    .line 174
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->scheduleReminder()V

    .line 180
    .end local v0    # "curTime":J
    :cond_0
    :goto_1
    return-void

    .line 169
    .restart local v0    # "curTime":J
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleDialUpdate(): dial update reminder time already set to +%d seconds"

    new-array v6, v10, [Ljava/lang/Object;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v8, v2, v0

    .line 171
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v11

    .line 169
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    .end local v0    # "curTime":J
    :cond_2
    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    .line 176
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "dial_reminder_time"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 177
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleDialUpdate(): clearing dial reminder"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public handleOTAUpdate(Lcom/navdy/hud/app/util/OTAUpdateService$UpdateVerified;)V
    .locals 11
    .param p1, "event"    # Lcom/navdy/hud/app/util/OTAUpdateService$UpdateVerified;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 207
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "ota_reminder_time"

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 208
    .local v2, "reminderTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 209
    .local v0, "curTime":J
    cmp-long v4, v2, v8

    if-nez v4, :cond_0

    .line 210
    sget-wide v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->INITIAL_REMINDER_INTERVAL:J

    add-long v2, v4, v0

    .line 211
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "ota_reminder_time"

    invoke-interface {v4, v5, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 212
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleOTAUpdate() setting OTA reminder for +%d seconds "

    new-array v6, v6, [Ljava/lang/Object;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v8, Lcom/navdy/hud/app/manager/UpdateReminderManager;->INITIAL_REMINDER_INTERVAL:J

    .line 213
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    .line 212
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 218
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->scheduleReminder()V

    .line 219
    return-void

    .line 215
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleOTAUpdate() OTA reminder already set for +%d seconds"

    new-array v6, v6, [Ljava/lang/Object;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v8, v2, v0

    .line 216
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    .line 215
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDateTimeAvailable(Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;)V
    .locals 11
    .param p1, "event"    # Lcom/navdy/hud/app/common/TimeHelper$DateTimeAvailableEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 185
    iput-boolean v6, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->timeAvailable:Z

    .line 186
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "ota_reminder_time"

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 187
    .local v2, "reminderTime":J
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->isOTAUpdateRequired()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 188
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 189
    .local v0, "curTime":J
    cmp-long v4, v2, v8

    if-nez v4, :cond_1

    .line 190
    sget-wide v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->INITIAL_REMINDER_INTERVAL:J

    add-long v2, v4, v0

    .line 191
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "ota_reminder_time"

    invoke-interface {v4, v5, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 192
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "onDateTimeAvailable(): setting OTA reminder time to +%d seconds "

    new-array v6, v6, [Ljava/lang/Object;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v8, Lcom/navdy/hud/app/manager/UpdateReminderManager;->INITIAL_REMINDER_INTERVAL:J

    .line 193
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    .line 192
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 202
    .end local v0    # "curTime":J
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/UpdateReminderManager;->scheduleReminder()V

    .line 203
    return-void

    .line 195
    .restart local v0    # "curTime":J
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "onDateTimeAvailable() OTA reminder time already set to +%d seconds"

    new-array v6, v6, [Ljava/lang/Object;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v8, v2, v0

    .line 196
    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    .line 195
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    .end local v0    # "curTime":J
    :cond_2
    cmp-long v4, v2, v8

    if-eqz v4, :cond_0

    .line 199
    iget-object v4, p0, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "ota_reminder_time"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 200
    sget-object v4, Lcom/navdy/hud/app/manager/UpdateReminderManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "onDateTimeAvailable(): clearing ota reminder"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
