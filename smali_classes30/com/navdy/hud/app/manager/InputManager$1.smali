.class Lcom/navdy/hud/app/manager/InputManager$1;
.super Ljava/lang/Object;
.source "InputManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/InputManager;->injectKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/manager/InputManager;

.field final synthetic val$event:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/InputManager;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/manager/InputManager;

    .prologue
    .line 317
    iput-object p1, p0, Lcom/navdy/hud/app/manager/InputManager$1;->this$0:Lcom/navdy/hud/app/manager/InputManager;

    iput-object p2, p0, Lcom/navdy/hud/app/manager/InputManager$1;->val$event:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 320
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager$1;->val$event:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-eqz v2, :cond_0

    .line 321
    # getter for: Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inject key event:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/manager/InputManager$1;->val$event:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 322
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager$1;->this$0:Lcom/navdy/hud/app/manager/InputManager;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/app/manager/InputManager$1;->val$event:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    # invokes: Lcom/navdy/hud/app/manager/InputManager;->invokeHandler(Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/manager/InputManager;->access$100(Lcom/navdy/hud/app/manager/InputManager;Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v1

    .line 323
    .local v1, "ret":Z
    if-nez v1, :cond_0

    .line 325
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager$1;->this$0:Lcom/navdy/hud/app/manager/InputManager;

    # getter for: Lcom/navdy/hud/app/manager/InputManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-static {v2}, Lcom/navdy/hud/app/manager/InputManager;->access$200(Lcom/navdy/hud/app/manager/InputManager;)Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    .line 326
    .local v0, "main":Lcom/navdy/hud/app/ui/activity/Main;
    if-eqz v0, :cond_0

    .line 327
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager$1;->val$event:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/activity/Main;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V

    .line 331
    .end local v0    # "main":Lcom/navdy/hud/app/ui/activity/Main;
    .end local v1    # "ret":Z
    :cond_0
    return-void
.end method
