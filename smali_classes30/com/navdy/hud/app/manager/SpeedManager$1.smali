.class Lcom/navdy/hud/app/manager/SpeedManager$1;
.super Ljava/lang/Object;
.source "SpeedManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/SpeedManager;->setGpsSpeed(FJ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/manager/SpeedManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/SpeedManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/manager/SpeedManager;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/navdy/hud/app/manager/SpeedManager$1;->this$0:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 107
    # getter for: Lcom/navdy/hud/app/manager/SpeedManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Gps RawSpeed data expired, resetting to zero"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager$1;->this$0:Lcom/navdy/hud/app/manager/SpeedManager;

    const/4 v1, -0x1

    # setter for: Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeed:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/manager/SpeedManager;->access$102(Lcom/navdy/hud/app/manager/SpeedManager;I)I

    .line 109
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getApplication()Lcom/navdy/hud/app/HudApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/HudApplication;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;

    invoke-direct {v1}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 110
    return-void
.end method
