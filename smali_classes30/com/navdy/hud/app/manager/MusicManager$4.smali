.class Lcom/navdy/hud/app/manager/MusicManager$4;
.super Ljava/lang/Object;
.source "MusicManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/MusicManager;->addMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/manager/MusicManager;

.field final synthetic val$listener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 665
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager$4;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    iput-object p2, p0, Lcom/navdy/hud/app/manager/MusicManager$4;->val$listener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 668
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$4;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$1000(Lcom/navdy/hud/app/manager/MusicManager;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/manager/MusicManager$4$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/manager/MusicManager$4$1;-><init>(Lcom/navdy/hud/app/manager/MusicManager$4;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 675
    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/MusicManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "album art listeners:"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 676
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$4;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$1200(Lcom/navdy/hud/app/manager/MusicManager;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    .line 677
    .local v0, "l":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/MusicManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 679
    .end local v0    # "l":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    :cond_0
    return-void
.end method
