.class Lcom/navdy/hud/app/manager/MusicManager$4$1;
.super Ljava/lang/Object;
.source "MusicManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/MusicManager$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/manager/MusicManager$4;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/MusicManager$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/manager/MusicManager$4;

    .prologue
    .line 668
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager$4$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 671
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager$4$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$4;

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager$4;->val$listener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$4$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$4;

    iget-object v1, v1, Lcom/navdy/hud/app/manager/MusicManager$4;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$100(Lcom/navdy/hud/app/manager/MusicManager;)Lokio/ByteString;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;->onAlbumArtUpdate(Lokio/ByteString;Z)V

    .line 672
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager$4$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$4;

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager$4;->val$listener:Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$4$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$4;

    iget-object v1, v1, Lcom/navdy/hud/app/manager/MusicManager$4;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentTrack()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$4$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$4;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/MusicManager$4;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentControls()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2, v3}, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;->onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V

    .line 673
    return-void
.end method
