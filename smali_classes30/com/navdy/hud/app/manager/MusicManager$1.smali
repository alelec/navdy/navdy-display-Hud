.class Lcom/navdy/hud/app/manager/MusicManager$1;
.super Ljava/lang/Object;
.source "MusicManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/MusicManager;->requestArtwork(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/manager/MusicManager;

.field final synthetic val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager$1;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    iput-object p2, p0, Lcom/navdy/hud/app/manager/MusicManager$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 352
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager$1;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->artworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;
    invoke-static {v0}, Lcom/navdy/hud/app/manager/MusicManager;->access$400(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/hud/app/util/MusicArtworkCache;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/manager/MusicManager$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v3, v3, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    new-instance v4, Lcom/navdy/hud/app/manager/MusicManager$1$1;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/manager/MusicManager$1$1;-><init>(Lcom/navdy/hud/app/manager/MusicManager$1;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/util/MusicArtworkCache;->getArtwork(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V

    .line 373
    return-void
.end method
