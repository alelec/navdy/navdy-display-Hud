.class public Lcom/navdy/hud/app/manager/MusicManager;
.super Ljava/lang/Object;
.source "MusicManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;,
        Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    }
.end annotation


# static fields
.field public static final CONTROLS:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

.field public static final DEFAULT_ARTWORK_SIZE:I = 0xc8

.field public static final EMPTY_TRACK:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

.field public static final PAUSED_DUE_TO_HFP_EXPIRY_TIME:J

.field public static final PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE:Ljava/lang/String; = "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE"

.field public static final PREFERENCE_MUSIC_CACHE_PROFILE:Ljava/lang/String; = "PREFERENCE_MUSIC_CACHE_PROFILE"

.field public static final PREFERENCE_MUSIC_CACHE_SERIAL:Ljava/lang/String; = "PREFERENCE_MUSIC_CACHE_SERIAL"

.field private static final PROGRESS_BAR_UPDATE_ON_ANDROID:I = 0x7d0

.field public static final THRESHOLD_TIME_BETWEEN_PAUSE_AND_POSSIBLE_CAUSE_FOR_HFP:I = 0x2710

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private acceptingResumes:Z

.field private artworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

.field private bus:Lcom/squareup/otto/Bus;

.field private clientSupportsArtworkCaching:Z

.field private currentControls:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;"
        }
    .end annotation
.end field

.field private currentPhoto:Lokio/ByteString;

.field private currentPosition:I

.field private currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private volatile interestingEventTime:J

.field private isLongPress:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isMusicLibraryCachingEnabled:Z

.field private volatile lastPausedTime:J

.field private lastTrackUpdateTime:J

.field private musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

.field private musicCapabilityTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionSource;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ">;>;"
        }
    .end annotation
.end field

.field private musicCollectionResponseMessageCache:Lcom/navdy/hud/app/storage/cache/MessageCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/navdy/hud/app/storage/cache/MessageCache",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;"
        }
    .end annotation
.end field

.field private musicMenuPath:Ljava/lang/String;

.field private musicNotification:Lcom/navdy/hud/app/framework/music/MusicNotification;

.field private musicUpdateListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final openNotificationRunnable:Ljava/lang/Runnable;

.field private pandoraManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

.field private pausedByUs:Z

.field private volatile pausedDueToHfpDetectedTime:J

.field private previousDataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private progressBarUpdater:Ljava/lang/Runnable;

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

.field private updateListeners:Z

.field private volatile wasPossiblyPausedDueToHFP:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 80
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/manager/MusicManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 106
    invoke-static {}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->values()[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager;->CONTROLS:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 128
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/manager/MusicManager;->PAUSED_DUE_TO_HFP_EXPIRY_TIME:J

    .line 144
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 145
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 146
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0901c8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    const-string v1, ""

    .line 147
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    const-string v1, ""

    .line 148
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 149
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isPreviousAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 150
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->isNextAllowed(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager;->EMPTY_TRACK:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 152
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/storage/cache/MessageCache;Lcom/squareup/otto/Bus;Landroid/content/res/Resources;Lcom/navdy/hud/app/ui/framework/UIStateManager;Lcom/navdy/hud/app/service/pandora/PandoraManager;Lcom/navdy/hud/app/util/MusicArtworkCache;)V
    .locals 3
    .param p2, "bus"    # Lcom/squareup/otto/Bus;
    .param p3, "resources"    # Landroid/content/res/Resources;
    .param p4, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .param p5, "pandoraManager"    # Lcom/navdy/hud/app/service/pandora/PandoraManager;
    .param p6, "artworkCache"    # Lcom/navdy/hud/app/util/MusicArtworkCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/storage/cache/MessageCache",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;",
            "Lcom/squareup/otto/Bus;",
            "Landroid/content/res/Resources;",
            "Lcom/navdy/hud/app/ui/framework/UIStateManager;",
            "Lcom/navdy/hud/app/service/pandora/PandoraManager;",
            "Lcom/navdy/hud/app/util/MusicArtworkCache;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "musicCollectionResponseMessageCache":Lcom/navdy/hud/app/storage/cache/MessageCache;, "Lcom/navdy/hud/app/storage/cache/MessageCache<Lcom/navdy/service/library/events/audio/MusicCollectionResponse;>;"
    const/4 v2, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->updateListeners:Z

    .line 109
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;

    .line 119
    iput-boolean v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->pausedByUs:Z

    .line 120
    iput-boolean v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->acceptingResumes:Z

    .line 121
    iput-boolean v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled:Z

    .line 124
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->previousDataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 129
    iput-boolean v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    .line 130
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->pausedDueToHfpDetectedTime:J

    .line 136
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicMenuPath:Ljava/lang/String;

    .line 410
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/manager/MusicManager$2;-><init>(Lcom/navdy/hud/app/manager/MusicManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->progressBarUpdater:Ljava/lang/Runnable;

    .line 424
    new-instance v0, Lcom/navdy/hud/app/manager/MusicManager$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/manager/MusicManager$3;-><init>(Lcom/navdy/hud/app/manager/MusicManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->openNotificationRunnable:Ljava/lang/Runnable;

    .line 432
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->isLongPress:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 164
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCollectionResponseMessageCache:Lcom/navdy/hud/app/storage/cache/MessageCache;

    .line 165
    iput-object p2, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    .line 166
    iput-object p4, p0, Lcom/navdy/hud/app/manager/MusicManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 167
    iput-object p5, p0, Lcom/navdy/hud/app/manager/MusicManager;->pandoraManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    .line 169
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->EMPTY_TRACK:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 170
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 172
    new-instance v0, Lcom/navdy/hud/app/framework/music/MusicNotification;

    invoke-direct {v0, p0, p2}, Lcom/navdy/hud/app/framework/music/MusicNotification;-><init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicNotification:Lcom/navdy/hud/app/framework/music/MusicNotification;

    .line 173
    iput-object p6, p0, Lcom/navdy/hud/app/manager/MusicManager;->artworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

    .line 174
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/manager/MusicManager;)Lokio/ByteString;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/manager/MusicManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/manager/MusicManager;Lokio/ByteString;)Lokio/ByteString;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;
    .param p1, "x1"    # Lokio/ByteString;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/manager/MusicManager;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/manager/MusicManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->callAlbumArtCallbacks()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/hud/app/util/MusicArtworkCache;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->artworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/manager/MusicManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->lastTrackUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/manager/MusicManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPosition:I

    return v0
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/manager/MusicManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;
    .param p1, "x1"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPosition:I

    return p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/hud/app/framework/music/MusicNotification;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicNotification:Lcom/navdy/hud/app/framework/music/MusicNotification;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/manager/MusicManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->progressBarUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method private cacheAlbumArt(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
    .locals 5
    .param p1, "musicArtworkResponse"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .prologue
    .line 837
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 838
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Not a now playing update, returning"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 842
    :goto_0
    return-void

    .line 841
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->artworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    invoke-virtual {v4}, Lokio/ByteString;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/util/MusicArtworkCache;->putArtwork(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method private cacheAlbumArt([BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 4
    .param p1, "photo"    # [B
    .param p2, "musicTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 845
    if-eqz p2, :cond_0

    .line 846
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->artworkCache:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    iget-object v2, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    iget-object v3, p2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/navdy/hud/app/util/MusicArtworkCache;->putArtwork(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 848
    :cond_0
    return-void
.end method

.method private callAlbumArtCallbacks()V
    .locals 3

    .prologue
    .line 745
    iget-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->updateListeners:Z

    if-eqz v0, :cond_0

    .line 746
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "callAlbumArtCallbacks"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 747
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/manager/MusicManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/manager/MusicManager$5;-><init>(Lcom/navdy/hud/app/manager/MusicManager;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 763
    :goto_0
    return-void

    .line 761
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "callAlbumArtCallbacks but updateListeners was false"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private callTrackUpdateCallbacks(Z)V
    .locals 4
    .param p1, "willOpenNotification"    # Z

    .prologue
    .line 734
    iget-boolean v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->updateListeners:Z

    if-eqz v1, :cond_0

    .line 735
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "callTrackUpdateCallbacks"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 736
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    .line 737
    .local v0, "listener":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v3, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    invoke-interface {v0, v2, v3, p1}, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;->onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V

    goto :goto_0

    .line 740
    .end local v0    # "listener":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "callTrackUpdateCallbacks but updateListeners was false"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 742
    :cond_1
    return-void
.end method

.method private canSeekBackward()Z
    .locals 2

    .prologue
    .line 592
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    .line 593
    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canSeekForward()Z
    .locals 2

    .prologue
    .line 587
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isOnlineStream:Ljava/lang/Boolean;

    .line 588
    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkCache(Ljava/lang/String;J)V
    .locals 12
    .param p1, "musicCollectionSource"    # Ljava/lang/String;
    .param p2, "serialNumber"    # J

    .prologue
    const/4 v10, 0x0

    .line 872
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    .line 873
    .local v6, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    .line 874
    .local v4, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v5

    .line 875
    .local v5, "profileName":Ljava/lang/String;
    const-string v7, "PREFERENCE_MUSIC_CACHE_PROFILE"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 876
    .local v0, "cacheProfileName":Ljava/lang/String;
    const-string v7, "PREFERENCE_MUSIC_CACHE_SERIAL"

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 877
    .local v2, "cacheSerialNumber":J
    const-string v7, "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 878
    .local v1, "cachedMusicCollectionSource":Ljava/lang/String;
    sget-object v7, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Current Cache details "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Source : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Serial : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 879
    sget-object v7, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Current Profile details "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Source : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", Serial : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 880
    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    cmp-long v7, v2, p2

    if-nez v7, :cond_0

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 882
    :cond_0
    sget-object v7, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Clearing the Music data cache"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 883
    iget-object v7, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCollectionResponseMessageCache:Lcom/navdy/hud/app/storage/cache/MessageCache;

    invoke-virtual {v7}, Lcom/navdy/hud/app/storage/cache/MessageCache;->clear()V

    .line 884
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "PREFERENCE_MUSIC_CACHE_PROFILE"

    invoke-interface {v7, v8, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "PREFERENCE_MUSIC_CACHE_SERIAL"

    .line 885
    invoke-interface {v7, v8, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE"

    .line 886
    invoke-interface {v7, v8, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 887
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 889
    :cond_1
    return-void
.end method

.method private dataSourceToUse()Lcom/navdy/service/library/events/audio/MusicDataSource;
    .locals 3

    .prologue
    .line 614
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 616
    .local v0, "currentTrackAvailable":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 617
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicDataSource;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 618
    iget-object v1, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 621
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->previousDataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    goto :goto_0
.end method

.method private postKeyDownUp(Lcom/navdy/service/library/events/input/MediaRemoteKey;)V
    .locals 4
    .param p1, "key"    # Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .prologue
    .line 979
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    sget-object v3, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_DOWN:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-direct {v2, p1, v3}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKey;Lcom/navdy/service/library/events/input/KeyEvent;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 980
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/manager/MusicManager$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/manager/MusicManager$6;-><init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/navdy/service/library/events/input/MediaRemoteKey;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 986
    return-void
.end method

.method private recordAnalytics(Landroid/view/KeyEvent;Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)V
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "control"    # Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .param p3, "isKeyPressedDown"    # Z

    .prologue
    .line 544
    if-nez p1, :cond_1

    .line 546
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 548
    :pswitch_0
    const-string v0, "Play"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMusicAction(Ljava/lang/String;)V

    goto :goto_0

    .line 551
    :pswitch_1
    const-string v0, "Pause"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMusicAction(Ljava/lang/String;)V

    goto :goto_0

    .line 556
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 558
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->isLongPress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 561
    :pswitch_3
    const-string v0, "Fast-forward"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMusicAction(Ljava/lang/String;)V

    goto :goto_0

    .line 564
    :pswitch_4
    const-string v0, "Rewind"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMusicAction(Ljava/lang/String;)V

    goto :goto_0

    .line 570
    :pswitch_5
    if-eqz p3, :cond_0

    .line 571
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->isLongPress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 572
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 574
    :pswitch_6
    const-string v0, "Next"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMusicAction(Ljava/lang/String;)V

    goto :goto_0

    .line 577
    :pswitch_7
    const-string v0, "Previous"

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMusicAction(Ljava/lang/String;)V

    goto :goto_0

    .line 546
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 556
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 559
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 572
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private requestArtwork(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 3
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 342
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestArtwork: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 343
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    .line 345
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->callAlbumArtCallbacks()V

    .line 346
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Empty track, returning"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 375
    :goto_0
    return-void

    .line 349
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/manager/MusicManager$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/manager/MusicManager$1;-><init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private requestPhotoUpdates(Z)V
    .locals 4
    .param p1, "start"    # Z

    .prologue
    .line 704
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->clientSupportsArtworkCaching:Z

    if-eqz v0, :cond_0

    .line 705
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "client supports caching, no need to request photo updates"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 712
    :goto_0
    return-void

    .line 708
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;-><init>()V

    .line 709
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->start(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 710
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;

    move-result-object v2

    .line 711
    invoke-virtual {v2}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 708
    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z
    .locals 4
    .param p1, "action"    # Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .prologue
    .line 597
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->dataSourceToUse()Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-result-object v0

    .line 598
    .local v0, "targetDataSource":Lcom/navdy/service/library/events/audio/MusicDataSource;
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v1, p1}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 599
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/events/audio/MusicDataSource;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 602
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->pandoraManager:Lcom/navdy/hud/app/service/pandora/PandoraManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/pandora/PandoraManager;->startAndPlay()V

    .line 610
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 604
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Media action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 605
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;-><init>()V

    .line 606
    invoke-virtual {v3, p1}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v3

    .line 607
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->dataSource(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v3

    .line 608
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->build()Lcom/navdy/service/library/events/audio/MusicEvent;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 605
    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private sendMediaKeyEvent(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)Z
    .locals 5
    .param p1, "control"    # Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .param p2, "down"    # Z

    .prologue
    .line 625
    const/4 v0, 0x0

    .line 626
    .local v0, "key":Lcom/navdy/service/library/events/input/MediaRemoteKey;
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 638
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;-><init>()V

    .line 639
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->key(Lcom/navdy/service/library/events/input/MediaRemoteKey;)Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;

    move-result-object v4

    if-eqz p2, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_DOWN:Lcom/navdy/service/library/events/input/KeyEvent;

    :goto_1
    invoke-virtual {v4, v1}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->action(Lcom/navdy/service/library/events/input/KeyEvent;)Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->build()Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 638
    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 640
    const/4 v1, 0x1

    return v1

    .line 629
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PLAY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 630
    goto :goto_0

    .line 632
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_NEXT:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 633
    goto :goto_0

    .line 635
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PREV:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    goto :goto_0

    .line 639
    :cond_0
    sget-object v1, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_UP:Lcom/navdy/service/library/events/input/KeyEvent;

    goto :goto_1

    .line 626
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static tryingToPlay(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Z
    .locals 1
    .param p0, "playbackState"    # Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .prologue
    .line 189
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_CONNECTING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 190
    invoke-virtual {v0, p0}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_ERROR:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 191
    invoke-virtual {v0, p0}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 192
    invoke-virtual {v0, p0}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_STOPPED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 193
    invoke-virtual {v0, p0}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateControls(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 2
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 396
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    .line 397
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 400
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-static {v0}, Lcom/navdy/hud/app/manager/MusicManager;->tryingToPlay(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 401
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 405
    :goto_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_1
    return-void

    .line 403
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public acceptResumes()V
    .locals 2

    .prologue
    .line 956
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->acceptingResumes:Z

    .line 957
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Now accepting music resume events"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 958
    return-void
.end method

.method public addMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 653
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "addMusicUpdateListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 655
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 656
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 657
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Enabling album art updates (PhotoUpdate)"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 658
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/manager/MusicManager;->requestPhotoUpdates(Z)V

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 665
    :goto_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/manager/MusicManager$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/manager/MusicManager$4;-><init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 681
    return-void

    .line 663
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Tried to add a listener that\'s already listening"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public executeMediaControl(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)V
    .locals 3
    .param p1, "control"    # Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .param p2, "isKeyPressedDown"    # Z

    .prologue
    .line 520
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v0

    .line 521
    .local v0, "remotePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    if-nez v0, :cond_0

    .line 541
    :goto_0
    return-void

    .line 524
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lcom/navdy/hud/app/manager/MusicManager;->recordAnalytics(Landroid/view/KeyEvent;Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)V

    .line 525
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    invoke-virtual {v0}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 527
    :pswitch_0
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 529
    :pswitch_1
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto :goto_0

    .line 532
    :pswitch_2
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PAUSE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto :goto_0

    .line 537
    :pswitch_3
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/navdy/hud/app/manager/MusicManager;->sendMediaKeyEvent(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)Z

    .line 538
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/navdy/hud/app/manager/MusicManager;->sendMediaKeyEvent(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)Z

    goto :goto_0

    .line 525
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 527
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCollectionTypesForSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Ljava/util/List;
    .locals 1
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicCollectionSource;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 900
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilityTypeMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getCurrentControls()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    return-object v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPosition:I

    return v0
.end method

.method public getCurrentTrack()Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    return-object v0
.end method

.method public getMusicCapabilities()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    return-object v0
.end method

.method public getMusicMenuPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicMenuPath:Ljava/lang/String;

    return-object v0
.end method

.method public handleKeyEvent(Landroid/view/KeyEvent;Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)V
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "control"    # Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .param p3, "isKeyPressedDown"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 436
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v0

    .line 437
    .local v0, "remotePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-static {v1}, Lcom/navdy/hud/app/manager/InputManager;->isCenterKey(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/manager/MusicManager;->recordAnalytics(Landroid/view/KeyEvent;Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)V

    .line 441
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    invoke-virtual {v0}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 443
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 445
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->isLongPress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 446
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 448
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->canSeekBackward()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 449
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto :goto_0

    .line 451
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Fast-forward is not allowed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 455
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->canSeekForward()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 456
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto :goto_0

    .line 458
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Rewind is not allowed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 465
    :pswitch_4
    if-eqz p3, :cond_0

    .line 466
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->isLongPress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 467
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3

    goto :goto_0

    .line 469
    :pswitch_5
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto :goto_0

    .line 472
    :pswitch_6
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto :goto_0

    .line 476
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_4

    goto/16 :goto_0

    .line 478
    :pswitch_7
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isNextAllowed:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 479
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_NEXT:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto/16 :goto_0

    .line 481
    :cond_5
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Next song action is not allowed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 485
    :pswitch_8
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->isPreviousAllowed:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 486
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->runAndroidAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Z

    goto/16 :goto_0

    .line 488
    :cond_6
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Previous song action is not allowed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 498
    :pswitch_9
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_5

    goto/16 :goto_0

    .line 500
    :pswitch_a
    if-nez p3, :cond_0

    .line 501
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    if-eq p2, v1, :cond_7

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    if-ne p2, v1, :cond_8

    .line 502
    :cond_7
    invoke-direct {p0, p2, v4}, Lcom/navdy/hud/app/manager/MusicManager;->sendMediaKeyEvent(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)Z

    .line 503
    invoke-direct {p0, p2, v3}, Lcom/navdy/hud/app/manager/MusicManager;->sendMediaKeyEvent(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)Z

    goto/16 :goto_0

    .line 505
    :cond_8
    invoke-direct {p0, p2, v4}, Lcom/navdy/hud/app/manager/MusicManager;->sendMediaKeyEvent(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)Z

    goto/16 :goto_0

    .line 510
    :pswitch_b
    if-eqz p3, :cond_0

    .line 511
    invoke-direct {p0, p2, v3}, Lcom/navdy/hud/app/manager/MusicManager;->sendMediaKeyEvent(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)Z

    goto/16 :goto_0

    .line 441
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_9
    .end packed-switch

    .line 443
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 446
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 467
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 476
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 498
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public hasMusicCapabilities()Z
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;->capabilities:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;->capabilities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize()V
    .locals 3

    .prologue
    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/audio/MusicTrackInfoRequest;

    invoke-direct {v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfoRequest;-><init>()V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 649
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Request for music track info sent to the client."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 650
    return-void
.end method

.method public isMusicLibraryCachingEnabled()Z
    .locals 1

    .prologue
    .line 975
    iget-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled:Z

    return v0
.end method

.method public isPandoraDataSource()Z
    .locals 2

    .prologue
    .line 770
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicDataSource;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isShuffling()Z
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 817
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 819
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicNotification:Lcom/navdy/hud/app/framework/music/MusicNotification;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/music/MusicNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 821
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->EMPTY_TRACK:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iput-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 822
    iput-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    .line 823
    iput-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentControls:Ljava/util/Set;

    .line 824
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->updateListeners:Z

    .line 826
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->callAlbumArtCallbacks()V

    .line 827
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/manager/MusicManager;->callTrackUpdateCallbacks(Z)V

    .line 829
    iput-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    .line 830
    iput-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilityTypeMap:Ljava/util/Map;

    .line 831
    iput-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicMenuPath:Ljava/lang/String;

    .line 832
    invoke-static {}, Lcom/navdy/hud/app/ui/component/mainmenu/MusicMenu2;->clearMenuData()V

    .line 834
    :cond_0
    return-void
.end method

.method public onDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 4
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 852
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceInfo - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 854
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest$Builder;-><init>()V

    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 856
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    .line 857
    .local v0, "capabilities":Lcom/navdy/service/library/events/Capabilities;
    iget-object v1, p1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v2, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-eq v1, v2, :cond_0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/navdy/service/library/events/Capabilities;->musicArtworkCache:Ljava/lang/Boolean;

    const/4 v2, 0x0

    .line 858
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 859
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->clientSupportsArtworkCaching:Z

    .line 861
    :cond_1
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 4
    .param p1, "driverProfileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 806
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDriverProfileChanged - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 808
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 809
    .local v0, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 811
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->requestPhotoUpdates(Z)V

    .line 813
    :cond_0
    return-void
.end method

.method public onLocalSpeechRequest(Lcom/navdy/hud/app/event/LocalSpeechRequest;)V
    .locals 4
    .param p1, "localSpeechRequest"    # Lcom/navdy/hud/app/event/LocalSpeechRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 797
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 798
    .local v0, "currentTime":J
    iget-boolean v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    if-nez v2, :cond_0

    .line 799
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Recording the speech request as an interesting event"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 800
    iput-wide v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->interestingEventTime:J

    .line 802
    :cond_0
    return-void
.end method

.method public onMediaKeyEventFromClient(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;)V
    .locals 2
    .param p1, "mediaRemoteKeyEvent"    # Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 932
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 933
    return-void
.end method

.method public onMusicArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
    .locals 5
    .param p1, "musicArtworkResponse"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 379
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onMusicArtworkResponse "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 380
    if-eqz p1, :cond_1

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    if-eqz v2, :cond_1

    .line 381
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/manager/MusicManager;->cacheAlbumArt(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V

    .line 383
    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-static {v2}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v1

    .line 384
    .local v1, "currentTrackPhotoId":Ljava/lang/String;
    invoke-static {p1}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "artworkResponseIdentifier":Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 386
    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    iput-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    .line 391
    .end local v0    # "artworkResponseIdentifier":Ljava/lang/String;
    .end local v1    # "currentTrackPhotoId":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->callAlbumArtCallbacks()V

    .line 392
    return-void

    .line 389
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    goto :goto_0
.end method

.method public onMusicCapabilitiesResponse(Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;)V
    .locals 8
    .param p1, "musicCapabilitiesResponse"    # Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 905
    sget-object v4, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onMusicCapabilitiesResponse "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 906
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    .line 907
    const/4 v1, 0x0

    .line 908
    .local v1, "musicCollectionSource":Ljava/lang/String;
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled:Z

    .line 909
    const-wide/16 v2, -0x1

    .line 910
    .local v2, "serialNumber":J
    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/MusicManager;->hasMusicCapabilities()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 911
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilityTypeMap:Ljava/util/Map;

    .line 912
    iget-object v4, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilities:Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;->capabilities:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCapability;

    .line 913
    .local v0, "capability":Lcom/navdy/service/library/events/audio/MusicCapability;
    iget-object v5, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicCapabilityTypeMap:Ljava/util/Map;

    iget-object v6, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v7, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionTypes:Ljava/util/List;

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 914
    iget-boolean v5, p0, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled:Z

    if-nez v5, :cond_0

    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    if-eqz v5, :cond_0

    .line 916
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled:Z

    .line 917
    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->name()Ljava/lang/String;

    move-result-object v1

    .line 918
    iget-object v5, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 919
    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Enabling caching with Source "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Serial :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 923
    .end local v0    # "capability":Lcom/navdy/service/library/events/audio/MusicCapability;
    :cond_1
    iget-boolean v4, p0, Lcom/navdy/hud/app/manager/MusicManager;->isMusicLibraryCachingEnabled:Z

    if-eqz v4, :cond_2

    .line 924
    invoke-direct {p0, v1, v2, v3}, Lcom/navdy/hud/app/manager/MusicManager;->checkCache(Ljava/lang/String;J)V

    .line 928
    :goto_1
    return-void

    .line 926
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Cache is not enabled"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onMusicCollectionSourceUpdate(Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;)V
    .locals 4
    .param p1, "musicCollectionSourceUpdate"    # Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 865
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMusicCollectionSourceUpdate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 866
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->serial_number:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 867
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/navdy/hud/app/manager/MusicManager;->checkCache(Ljava/lang/String;J)V

    .line 869
    :cond_0
    return-void
.end method

.method public onPhoneEvent(Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V
    .locals 6
    .param p1, "phoneEvent"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 776
    if-eqz p1, :cond_1

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eqz v2, :cond_1

    .line 777
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PhoneEvent : new status "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 778
    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v3, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eq v2, v3, :cond_0

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v3, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eq v2, v3, :cond_0

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v3, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-ne v2, v3, :cond_1

    .line 779
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 780
    .local v0, "currentTime":J
    iget-boolean v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    if-nez v2, :cond_1

    .line 782
    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->lastPausedTime:J

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 783
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Current state is paused recently, possibly due to call"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 784
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    .line 785
    iput-wide v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->pausedDueToHfpDetectedTime:J

    .line 793
    .end local v0    # "currentTime":J
    :cond_1
    :goto_0
    return-void

    .line 787
    .restart local v0    # "currentTime":J
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Recording the phone event as interesting event"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 788
    iput-wide v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->interestingEventTime:J

    goto :goto_0
.end method

.method public onPhotoUpdate(Lcom/navdy/service/library/events/photo/PhotoUpdate;)V
    .locals 4
    .param p1, "photoUpdate"    # Lcom/navdy/service/library/events/photo/PhotoUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 716
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPhotoUpdate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 717
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    if-eqz v1, :cond_1

    .line 718
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    iput-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    .line 719
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-static {v1}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v0

    .line 720
    .local v0, "currentTrackPhotoId":Ljava/lang/String;
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 721
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    invoke-virtual {v1}, Lokio/ByteString;->toByteArray()[B

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/manager/MusicManager;->cacheAlbumArt([BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 730
    .end local v0    # "currentTrackPhotoId":Ljava/lang/String;
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/MusicManager;->callAlbumArtCallbacks()V

    .line 731
    return-void

    .line 725
    .restart local v0    # "currentTrackPhotoId":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received PhotoUpdate with wrong identifier: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 728
    .end local v0    # "currentTrackPhotoId":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;

    goto :goto_0
.end method

.method public onResumeMusicRequest(Lcom/navdy/service/library/events/audio/ResumeMusicRequest;)V
    .locals 0
    .param p1, "resumeMusicRequest"    # Lcom/navdy/service/library/events/audio/ResumeMusicRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 962
    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/MusicManager;->softResume()V

    .line 963
    return-void
.end method

.method public onTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 28
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 204
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "updateState "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 205
    sget-object v23, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v14

    .line 206
    .local v14, "isPlaying":Z
    sget-object v23, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 209
    .local v11, "isOldStatePlaying":Z
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    move-object/from16 v23, v0

    sget-object v24, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_0

    .line 210
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->setMusicMenuPath(Ljava/lang/String;)V

    .line 215
    :cond_0
    if-nez v14, :cond_2

    if-eqz v11, :cond_2

    sget-object v23, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v24, v0

    .line 216
    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicDataSource;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_2

    sget-object v23, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_PANDORA_API:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v24, v0

    .line 217
    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicDataSource;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 218
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "Ignoring update from non playing Pandora"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 333
    :cond_1
    :goto_0
    return-void

    .line 226
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-object/from16 v24, v0

    .line 227
    invoke-static/range {v24 .. v24}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v24

    .line 225
    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_16

    const/16 v20, 0x1

    .line 229
    .local v20, "trackChanged":Z
    :goto_1
    if-eqz v20, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->clientSupportsArtworkCaching:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    .line 230
    invoke-direct/range {p0 .. p1}, Lcom/navdy/hud/app/manager/MusicManager;->requestArtwork(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 233
    :cond_3
    if-eqz v14, :cond_17

    if-eqz v20, :cond_17

    const/4 v7, 0x1

    .line 234
    .local v7, "isNewSongPlaying":Z
    :goto_2
    if-eqz v14, :cond_18

    if-nez v11, :cond_18

    const/4 v9, 0x1

    .line 236
    .local v9, "isNewStatePlaying":Z
    :goto_3
    sget-object v23, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v13

    .line 237
    .local v13, "isPaused":Z
    sget-object v23, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 239
    .local v10, "isOldStatePaused":Z
    if-eqz v13, :cond_19

    if-nez v10, :cond_19

    const/4 v8, 0x1

    .line 241
    .local v8, "isNewStatePaused":Z
    :goto_4
    if-nez v9, :cond_4

    if-eqz v8, :cond_5

    .line 242
    :cond_4
    sget-object v23, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v23, v0

    new-instance v24, Lcom/navdy/hud/app/event/RemoteEvent;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual/range {v23 .. v24}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 247
    :cond_5
    if-eqz v9, :cond_6

    .line 248
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "Music started playing"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 251
    :cond_6
    if-eqz v9, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->pausedByUs:Z

    move/from16 v23, v0

    if-eqz v23, :cond_7

    .line 252
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "User started playback after we paused - setting pauseByUs = false"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 253
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/manager/MusicManager;->pausedByUs:Z

    .line 256
    :cond_7
    if-eqz v8, :cond_8

    .line 257
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "New state is paused"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 258
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    .line 259
    .local v16, "pausedTime":J
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    move/from16 v23, v0

    if-nez v23, :cond_8

    .line 260
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->interestingEventTime:J

    move-wide/from16 v24, v0

    sub-long v24, v16, v24

    const-wide/16 v26, 0x2710

    cmp-long v23, v24, v26

    if-gez v23, :cond_1a

    .line 261
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "Interesting event happened just before the song was paused, possibly playing through HFP"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 262
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    .line 263
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/manager/MusicManager;->pausedDueToHfpDetectedTime:J

    .line 270
    .end local v16    # "pausedTime":J
    :cond_8
    :goto_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/manager/MusicManager;->lastTrackUpdateTime:J

    .line 271
    if-eqz v9, :cond_1b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    move/from16 v23, v0

    if-eqz v23, :cond_1b

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->lastTrackUpdateTime:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->pausedDueToHfpDetectedTime:J

    move-wide/from16 v26, v0

    sub-long v24, v24, v26

    sget-wide v26, Lcom/navdy/hud/app/manager/MusicManager;->PAUSED_DUE_TO_HFP_EXPIRY_TIME:J

    cmp-long v23, v24, v26

    if-gez v23, :cond_1b

    const/16 v18, 0x1

    .line 272
    .local v18, "shouldSuppressNotification":Z
    :goto_6
    if-eqz v18, :cond_9

    .line 273
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "Should suppress Notification"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 275
    :cond_9
    if-nez v18, :cond_a

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->lastTrackUpdateTime:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->pausedDueToHfpDetectedTime:J

    move-wide/from16 v26, v0

    sub-long v24, v24, v26

    sget-wide v26, Lcom/navdy/hud/app/manager/MusicManager;->PAUSED_DUE_TO_HFP_EXPIRY_TIME:J

    cmp-long v23, v24, v26

    if-lez v23, :cond_b

    .line 276
    :cond_a
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/manager/MusicManager;->wasPossiblyPausedDueToHFP:Z

    .line 277
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/manager/MusicManager;->pausedDueToHfpDetectedTime:J

    .line 281
    :cond_b
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v23, v0

    if-eqz v23, :cond_c

    sget-object v23, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v24, v0

    .line 282
    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicDataSource;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_c

    .line 283
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/manager/MusicManager;->previousDataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 286
    :cond_c
    invoke-direct/range {p0 .. p1}, Lcom/navdy/hud/app/manager/MusicManager;->updateControls(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 288
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    move-object/from16 v23, v0

    if-eqz v23, :cond_1c

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    if-lez v23, :cond_1c

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    move-object/from16 v23, v0

    if-eqz v23, :cond_1c

    const/4 v4, 0x1

    .line 289
    .local v4, "hasProgressInfo":Z
    :goto_7
    if-eqz v4, :cond_d

    .line 290
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/manager/MusicManager;->currentPosition:I

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->musicNotification:Lcom/navdy/hud/app/framework/music/MusicNotification;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentPosition:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/hud/app/framework/music/MusicNotification;->updateProgressBar(I)V

    .line 295
    :cond_d
    sget-object v23, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->progressBarUpdater:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 297
    sget-object v23, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    if-eqz v4, :cond_e

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->progressBarUpdater:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    const-wide/16 v26, 0x7d0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 303
    :cond_e
    sget-object v23, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_f

    sget-object v23, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    move-object/from16 v24, v0

    .line 304
    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1d

    :cond_f
    const/4 v12, 0x1

    .line 305
    .local v12, "isOldStateStoppedOrNone":Z
    :goto_8
    if-eqz v8, :cond_1e

    if-nez v20, :cond_1e

    const/4 v15, 0x1

    .line 306
    .local v15, "pausedExistingTrack":Z
    :goto_9
    if-eqz v4, :cond_1f

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1f

    const/16 v21, 0x1

    .line 307
    .local v21, "trackEnd":Z
    :goto_a
    if-eqz v13, :cond_20

    if-nez v21, :cond_20

    if-nez v15, :cond_11

    if-eqz v10, :cond_10

    if-nez v20, :cond_11

    :cond_10
    if-eqz v12, :cond_20

    :cond_11
    const/4 v5, 0x1

    .line 309
    .local v5, "interestingPauseTransition":Z
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_21

    const/16 v19, 0x1

    .line 310
    .local v19, "shuffleModeChange":Z
    :goto_c
    if-nez v14, :cond_12

    if-nez v5, :cond_12

    if-eqz v19, :cond_22

    :cond_12
    const/16 v23, 0x1

    :goto_d
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/manager/MusicManager;->updateListeners:Z

    .line 311
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->updateListeners:Z

    move/from16 v23, v0

    if-eqz v23, :cond_25

    .line 312
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 313
    if-nez v7, :cond_13

    if-eqz v9, :cond_23

    :cond_13
    const/4 v6, 0x1

    .line 314
    .local v6, "interestingPlayTransition":Z
    :goto_e
    if-nez v18, :cond_24

    .line 315
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isMusicNotificationEnabled()Z

    move-result v23

    if-eqz v23, :cond_24

    if-eqz v6, :cond_24

    const/16 v22, 0x1

    .line 318
    .local v22, "willShowMusicNotification":Z
    :goto_f
    if-nez v6, :cond_14

    if-nez v5, :cond_14

    if-eqz v19, :cond_15

    .line 319
    :cond_14
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->callTrackUpdateCallbacks(Z)V

    .line 320
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->clientSupportsArtworkCaching:Z

    move/from16 v23, v0

    if-nez v23, :cond_15

    .line 321
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/manager/MusicManager;->callAlbumArtCallbacks()V

    .line 324
    :cond_15
    if-eqz v22, :cond_1

    .line 325
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Showing notification New Song ? : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", New state playing : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->openNotificationRunnable:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager;->openNotificationRunnable:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 225
    .end local v4    # "hasProgressInfo":Z
    .end local v5    # "interestingPauseTransition":Z
    .end local v6    # "interestingPlayTransition":Z
    .end local v7    # "isNewSongPlaying":Z
    .end local v8    # "isNewStatePaused":Z
    .end local v9    # "isNewStatePlaying":Z
    .end local v10    # "isOldStatePaused":Z
    .end local v12    # "isOldStateStoppedOrNone":Z
    .end local v13    # "isPaused":Z
    .end local v15    # "pausedExistingTrack":Z
    .end local v18    # "shouldSuppressNotification":Z
    .end local v19    # "shuffleModeChange":Z
    .end local v20    # "trackChanged":Z
    .end local v21    # "trackEnd":Z
    .end local v22    # "willShowMusicNotification":Z
    :cond_16
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 233
    .restart local v20    # "trackChanged":Z
    :cond_17
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 234
    .restart local v7    # "isNewSongPlaying":Z
    :cond_18
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 239
    .restart local v9    # "isNewStatePlaying":Z
    .restart local v10    # "isOldStatePaused":Z
    .restart local v13    # "isPaused":Z
    :cond_19
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 265
    .restart local v8    # "isNewStatePaused":Z
    .restart local v16    # "pausedTime":J
    :cond_1a
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/manager/MusicManager;->lastPausedTime:J

    goto/16 :goto_5

    .line 271
    .end local v16    # "pausedTime":J
    :cond_1b
    const/16 v18, 0x0

    goto/16 :goto_6

    .line 288
    .restart local v18    # "shouldSuppressNotification":Z
    :cond_1c
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 304
    .restart local v4    # "hasProgressInfo":Z
    :cond_1d
    const/4 v12, 0x0

    goto/16 :goto_8

    .line 305
    .restart local v12    # "isOldStateStoppedOrNone":Z
    :cond_1e
    const/4 v15, 0x0

    goto/16 :goto_9

    .line 306
    .restart local v15    # "pausedExistingTrack":Z
    :cond_1f
    const/16 v21, 0x0

    goto/16 :goto_a

    .line 307
    .restart local v21    # "trackEnd":Z
    :cond_20
    const/4 v5, 0x0

    goto/16 :goto_b

    .line 309
    .restart local v5    # "interestingPauseTransition":Z
    :cond_21
    const/16 v19, 0x0

    goto/16 :goto_c

    .line 310
    .restart local v19    # "shuffleModeChange":Z
    :cond_22
    const/16 v23, 0x0

    goto/16 :goto_d

    .line 313
    :cond_23
    const/4 v6, 0x0

    goto/16 :goto_e

    .line 315
    .restart local v6    # "interestingPlayTransition":Z
    :cond_24
    const/16 v22, 0x0

    goto/16 :goto_f

    .line 331
    .end local v6    # "interestingPlayTransition":Z
    :cond_25
    sget-object v23, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "Ignoring updates"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public removeMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 684
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "removeMusicUpdateListener"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 686
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 687
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 689
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 690
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Disabling album art updates"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 691
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->requestPhotoUpdates(Z)V

    .line 697
    :cond_0
    :goto_0
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "album art listeners:"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 698
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    .line 699
    .local v0, "l":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 694
    .end local v0    # "l":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Tried to remove a non-existent listener"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 701
    :cond_2
    return-void
.end method

.method public setMusicMenuPath(Ljava/lang/String;)V
    .locals 3
    .param p1, "musicMenuPath"    # Ljava/lang/String;

    .prologue
    .line 940
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMusicMenuPath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 941
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicMenuPath:Ljava/lang/String;

    .line 942
    return-void
.end method

.method public showMusicNotification()V
    .locals 2

    .prologue
    .line 766
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager;->musicNotification:Lcom/navdy/hud/app/framework/music/MusicNotification;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 767
    return-void
.end method

.method public softPause()V
    .locals 2

    .prologue
    .line 946
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    if-eq v0, v1, :cond_0

    .line 953
    :goto_0
    return-void

    .line 949
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Pausing music"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 950
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->pausedByUs:Z

    .line 951
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->acceptingResumes:Z

    .line 952
    sget-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PLAY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/manager/MusicManager;->postKeyDownUp(Lcom/navdy/service/library/events/input/MediaRemoteKey;)V

    goto :goto_0
.end method

.method public softResume()V
    .locals 2

    .prologue
    .line 967
    iget-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->pausedByUs:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->acceptingResumes:Z

    if-eqz v0, :cond_0

    .line 968
    sget-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_PLAY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/manager/MusicManager;->postKeyDownUp(Lcom/navdy/service/library/events/input/MediaRemoteKey;)V

    .line 969
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/MusicManager;->pausedByUs:Z

    .line 970
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Unpausing music (resuming)"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 972
    :cond_0
    return-void
.end method
