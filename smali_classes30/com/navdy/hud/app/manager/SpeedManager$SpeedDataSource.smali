.class public final enum Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;
.super Ljava/lang/Enum;
.source "SpeedManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/SpeedManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpeedDataSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

.field public static final enum GPS:Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

.field public static final enum OBD:Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    const-string v1, "GPS"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;->GPS:Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    .line 68
    new-instance v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    const-string v1, "OBD"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;->OBD:Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    .line 66
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;->GPS:Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;->OBD:Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;->$VALUES:[Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;->$VALUES:[Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;

    return-object v0
.end method
