.class public Lcom/navdy/hud/app/manager/PairingManager;
.super Ljava/lang/Object;
.source "PairingManager.java"


# instance fields
.field private autoPairing:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isAutoPairing()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/navdy/hud/app/manager/PairingManager;->autoPairing:Z

    return v0
.end method

.method public setAutoPairing(Z)V
    .locals 0
    .param p1, "pairing"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/navdy/hud/app/manager/PairingManager;->autoPairing:Z

    .line 19
    return-void
.end method
