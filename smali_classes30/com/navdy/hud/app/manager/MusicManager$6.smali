.class Lcom/navdy/hud/app/manager/MusicManager$6;
.super Ljava/lang/Object;
.source "MusicManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/MusicManager;->postKeyDownUp(Lcom/navdy/service/library/events/input/MediaRemoteKey;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/manager/MusicManager;

.field final synthetic val$key:Lcom/navdy/service/library/events/input/MediaRemoteKey;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/navdy/service/library/events/input/MediaRemoteKey;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 980
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager$6;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    iput-object p2, p0, Lcom/navdy/hud/app/manager/MusicManager$6;->val$key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 983
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager$6;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/manager/MusicManager;->access$300(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    iget-object v3, p0, Lcom/navdy/hud/app/manager/MusicManager$6;->val$key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    sget-object v4, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_UP:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKey;Lcom/navdy/service/library/events/input/KeyEvent;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 984
    return-void
.end method
