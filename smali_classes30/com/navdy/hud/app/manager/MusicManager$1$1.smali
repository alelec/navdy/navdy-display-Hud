.class Lcom/navdy/hud/app/manager/MusicManager$1$1;
.super Ljava/lang/Object;
.source "MusicManager.java"

# interfaces
.implements Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/MusicManager$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/manager/MusicManager$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/MusicManager$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/manager/MusicManager$1;

    .prologue
    .line 352
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager$1$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHit([B)V
    .locals 2
    .param p1, "artwork"    # [B
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 355
    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/MusicManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "CACHE HIT"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager$1$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$1;

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager$1;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-static {p1}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v1

    # setter for: Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$102(Lcom/navdy/hud/app/manager/MusicManager;Lokio/ByteString;)Lokio/ByteString;

    .line 357
    iget-object v0, p0, Lcom/navdy/hud/app/manager/MusicManager$1$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$1;

    iget-object v0, v0, Lcom/navdy/hud/app/manager/MusicManager$1;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # invokes: Lcom/navdy/hud/app/manager/MusicManager;->callAlbumArtCallbacks()V
    invoke-static {v0}, Lcom/navdy/hud/app/manager/MusicManager;->access$200(Lcom/navdy/hud/app/manager/MusicManager;)V

    .line 358
    return-void
.end method

.method public onMiss()V
    .locals 4

    .prologue
    .line 362
    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/MusicManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "CACHE MISS"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 363
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$1$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/MusicManager$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    .line 364
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$1$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/MusicManager$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    .line 365
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$1$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/MusicManager$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 366
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    const/16 v2, 0xc8

    .line 367
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->size(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    move-result-object v1

    .line 368
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->build()Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    move-result-object v0

    .line 369
    .local v0, "musicArtworkRequest":Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/MusicManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requesting artwork: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 370
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$1$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$1;

    iget-object v1, v1, Lcom/navdy/hud/app/manager/MusicManager$1;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$300(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 371
    return-void
.end method
