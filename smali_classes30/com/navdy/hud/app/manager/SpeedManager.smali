.class public Lcom/navdy/hud/app/manager/SpeedManager;
.super Ljava/lang/Object;
.source "SpeedManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;,
        Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataExpired;,
        Lcom/navdy/hud/app/manager/SpeedManager$SpeedDataSource;,
        Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    }
.end annotation


# static fields
.field private static final EPSILON:D = 1.0E-5

.field private static final GPS_SPEED_EXPIRY_INTERVAL:J = 0x1388L

.field private static final MAX_VALID_METERS_PER_SEC:D = 89.41333333333333

.field private static final METERS_PER_KILOMETER:D = 1000.0

.field private static final METERS_PER_MILE:D = 1609.44

.field private static final SECONDS_PER_HOUR:I = 0xe10

.field public static final SPEED_NOT_AVAILABLE:I = -0x1

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/manager/SpeedManager;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private volatile gpsSpeed:I

.field private gpsSpeedExpiryRunnable:Ljava/lang/Runnable;

.field private handler:Landroid/os/Handler;

.field private volatile obdSpeed:I

.field private volatile rawGpsSpeed:F

.field private volatile rawGpsSpeedTimeStamp:J

.field private volatile rawObdSpeed:I

.field private volatile rawObdSpeedTimeStamp:J

.field private speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/manager/SpeedManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 41
    new-instance v0, Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/manager/SpeedManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/manager/SpeedManager;->singleton:Lcom/navdy/hud/app/manager/SpeedManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, -0x1

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->obdSpeed:I

    .line 56
    iput v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeed:I

    .line 57
    iput-wide v2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeedTimeStamp:J

    .line 58
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeed:F

    .line 59
    iput-wide v2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeedTimeStamp:J

    .line 62
    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->MILES_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    iput-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 77
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 79
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->handler:Landroid/os/Handler;

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 81
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/SpeedManager;->setUnit()V

    .line 82
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/manager/SpeedManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/SpeedManager;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeed:I

    return p1
.end method

.method private static almostEqual(FF)Z
    .locals 4
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 119
    sub-float v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I
    .locals 2
    .param p0, "metersPerSec"    # D
    .param p2, "unit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .prologue
    .line 228
    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {p0, p1, v0, p2}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v0

    return v0
.end method

.method public static convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I
    .locals 2
    .param p0, "source"    # D
    .param p2, "sourceUnit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .param p3, "desiredUnit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .prologue
    .line 156
    invoke-static {p0, p1, p2, p3}, Lcom/navdy/hud/app/manager/SpeedManager;->convertWithPrecision(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)F

    move-result v0

    .line 157
    .local v0, "speedF":F
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public static convertWithPrecision(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)F
    .locals 10
    .param p0, "source"    # D
    .param p2, "sourceUnit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .param p3, "desiredUnit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .prologue
    const-wide v8, 0x409925c28f5c28f6L    # 1609.44

    const-wide v6, 0x408f400000000000L    # 1000.0

    const-wide v4, 0x40ac200000000000L    # 3600.0

    .line 161
    const-wide/16 v2, 0x0

    cmpl-double v2, p0, v2

    if-eqz v2, :cond_0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v2, p0, v2

    if-nez v2, :cond_1

    .line 162
    :cond_0
    double-to-float v2, p0

    .line 195
    :goto_0
    return v2

    .line 164
    :cond_1
    if-ne p2, p3, :cond_2

    .line 165
    double-to-float v2, p0

    goto :goto_0

    .line 168
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/manager/SpeedManager$2;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 179
    const-wide/16 v0, 0x0

    .line 181
    .local v0, "metersPerSecond":D
    :goto_1
    sget-object v2, Lcom/navdy/hud/app/manager/SpeedManager$2;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {p3}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 195
    const/4 v2, 0x0

    goto :goto_0

    .line 170
    .end local v0    # "metersPerSecond":D
    :pswitch_0
    mul-double v2, p0, v8

    div-double v0, v2, v4

    .line 171
    .restart local v0    # "metersPerSecond":D
    goto :goto_1

    .line 173
    .end local v0    # "metersPerSecond":D
    :pswitch_1
    mul-double v2, p0, v6

    div-double v0, v2, v4

    .line 174
    .restart local v0    # "metersPerSecond":D
    goto :goto_1

    .line 176
    .end local v0    # "metersPerSecond":D
    :pswitch_2
    move-wide v0, p0

    .line 177
    .restart local v0    # "metersPerSecond":D
    goto :goto_1

    .line 183
    :pswitch_3
    mul-double v2, v0, v4

    div-double/2addr v2, v8

    double-to-float v2, v2

    goto :goto_0

    .line 187
    :pswitch_4
    mul-double v2, v0, v4

    div-double/2addr v2, v6

    double-to-float v2, v2

    goto :goto_0

    .line 191
    :pswitch_5
    double-to-float v2, v0

    goto :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 181
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getInstance()Lcom/navdy/hud/app/manager/SpeedManager;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager;->singleton:Lcom/navdy/hud/app/manager/SpeedManager;

    return-object v0
.end method

.method private setUnit()V
    .locals 4

    .prologue
    .line 244
    iget-object v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    if-ne v1, v2, :cond_0

    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->KILOMETERS_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 248
    .local v0, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    if-eq v1, v0, :cond_1

    .line 249
    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ondriverprofileupdate speed unit has changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 250
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/manager/SpeedManager;->setSpeedUnit(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)V

    .line 251
    iget-object v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;

    invoke-direct {v2}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;-><init>()V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 255
    :goto_1
    return-void

    .line 244
    .end local v0    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->MILES_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    goto :goto_0

    .line 253
    .restart local v0    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ondriverprofileupdated speed unit is still "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private declared-synchronized updateSpeed(FJ)Z
    .locals 6
    .param p1, "metersPerSec"    # F
    .param p2, "timeStamp"    # J

    .prologue
    const/4 v0, 0x0

    .line 123
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeed:F

    invoke-static {v1, p1}, Lcom/navdy/hud/app/manager/SpeedManager;->almostEqual(FF)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    iput-wide p2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeedTimeStamp:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 127
    :cond_1
    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    float-to-double v2, p1

    const-wide v4, 0x40565a740da740daL    # 89.41333333333333

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 130
    :try_start_1
    iput-wide p2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeedTimeStamp:J

    .line 131
    iput p1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeed:F

    .line 132
    float-to-double v0, p1

    sget-object v2, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    iget-object v3, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeed:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    const/4 v0, 0x1

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized getCurrentSpeed()I
    .locals 2

    .prologue
    .line 203
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 204
    .local v0, "obdSpeed":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 207
    .end local v0    # "obdSpeed":I
    :goto_0
    monitor-exit p0

    return v0

    .restart local v0    # "obdSpeed":I
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/SpeedManager;->getGpsSpeed()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 203
    .end local v0    # "obdSpeed":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getCurrentSpeedInMetersPerSecond()Lcom/navdy/hud/app/analytics/RawSpeed;
    .locals 5

    .prologue
    .line 214
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeed:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 215
    new-instance v0, Lcom/navdy/hud/app/analytics/RawSpeed;

    iget v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeed:I

    int-to-double v2, v1

    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->KILOMETERS_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    sget-object v4, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v2, v3, v1, v4}, Lcom/navdy/hud/app/manager/SpeedManager;->convertWithPrecision(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)F

    move-result v1

    iget-wide v2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeedTimeStamp:J

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/analytics/RawSpeed;-><init>(FJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/navdy/hud/app/analytics/RawSpeed;

    iget v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeed:F

    iget-wide v2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeedTimeStamp:J

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/analytics/RawSpeed;-><init>(FJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getGpsSpeed()I
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeed:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getGpsSpeedInMetersPerSecond()F
    .locals 1

    .prologue
    .line 224
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawGpsSpeed:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getObdSpeed()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->obdSpeed:I

    return v0
.end method

.method public getRawObdSpeed()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeed:I

    return v0
.end method

.method public getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    return-object v0
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/SpeedManager;->setUnit()V

    .line 241
    return-void
.end method

.method public onDriverProfileUpdated(Lcom/navdy/hud/app/event/DriverProfileUpdated;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileUpdated;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 233
    iget-object v0, p1, Lcom/navdy/hud/app/event/DriverProfileUpdated;->state:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    sget-object v1, Lcom/navdy/hud/app/event/DriverProfileUpdated$State;->UPDATED:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    if-ne v0, v1, :cond_0

    .line 234
    invoke-direct {p0}, Lcom/navdy/hud/app/manager/SpeedManager;->setUnit()V

    .line 236
    :cond_0
    return-void
.end method

.method public declared-synchronized setGpsSpeed(FJ)Z
    .locals 4
    .param p1, "metersPerSec"    # F
    .param p2, "rawGpsSpeedTimeStamp"    # J

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeedExpiryRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcom/navdy/hud/app/manager/SpeedManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/manager/SpeedManager$1;-><init>(Lcom/navdy/hud/app/manager/SpeedManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeedExpiryRunnable:Ljava/lang/Runnable;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeedExpiryRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeedExpiryRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 115
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/manager/SpeedManager;->updateSpeed(FJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setObdSpeed(IJ)V
    .locals 4
    .param p1, "kph"    # I
    .param p2, "timeStamp"    # J

    .prologue
    .line 146
    monitor-enter p0

    const/4 v0, -0x1

    if-lt p1, v0, :cond_0

    const/16 v0, 0x100

    if-le p1, v0, :cond_1

    .line 147
    :cond_0
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/manager/SpeedManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid OBD speed ignored:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :goto_0
    monitor-exit p0

    return-void

    .line 150
    :cond_1
    :try_start_1
    iput p1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeed:I

    .line 151
    iput-wide p2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->rawObdSpeedTimeStamp:J

    .line 152
    int-to-double v0, p1

    sget-object v2, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->KILOMETERS_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    iget-object v3, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->obdSpeed:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSpeedUnit(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)V
    .locals 3
    .param p1, "speedUnit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    if-eq p1, v0, :cond_0

    .line 91
    iget v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeed:I

    int-to-double v0, v0

    iget-object v2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v0, v1, v2, p1}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->gpsSpeed:I

    .line 92
    iget v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->obdSpeed:I

    int-to-double v0, v0

    iget-object v2, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v0, v1, v2, p1}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/manager/SpeedManager;->obdSpeed:I

    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/manager/SpeedManager;->speedUnit:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .line 95
    :cond_0
    return-void
.end method
