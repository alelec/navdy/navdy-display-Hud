.class Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;
.super Ljava/lang/Object;
.source "InitManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/InitManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhaseEmitter"
.end annotation


# instance fields
.field private phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

.field final synthetic this$0:Lcom/navdy/hud/app/manager/InitManager;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/manager/InitManager;Lcom/navdy/hud/app/event/InitEvents$Phase;)V
    .locals 0
    .param p2, "phase"    # Lcom/navdy/hud/app/event/InitEvents$Phase;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;->phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

    .line 60
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 64
    # getter for: Lcom/navdy/hud/app/manager/InitManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/InitManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Triggering init phase:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;->phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    # getter for: Lcom/navdy/hud/app/manager/InitManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/manager/InitManager;->access$200(Lcom/navdy/hud/app/manager/InitManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/InitEvents$InitPhase;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;->phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/InitEvents$InitPhase;-><init>(Lcom/navdy/hud/app/event/InitEvents$Phase;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 66
    return-void
.end method
