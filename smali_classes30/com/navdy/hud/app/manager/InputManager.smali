.class public Lcom/navdy/hud/app/manager/InputManager;
.super Ljava/lang/Object;
.source "InputManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/manager/InputManager$IInputHandler;,
        Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private defaultHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

.field private dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

.field private handler:Landroid/os/Handler;

.field private inputHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

.field private isLongPressDetected:Z

.field private lastInputEventTime:J

.field private powerManager:Lcom/navdy/hud/app/device/PowerManager;

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/manager/InputManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/device/PowerManager;Lcom/navdy/hud/app/ui/framework/UIStateManager;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "powerManager"    # Lcom/navdy/hud/app/device/PowerManager;
    .param p3, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/manager/InputManager;->lastInputEventTime:J

    .line 79
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/InputManager;->handler:Landroid/os/Handler;

    .line 83
    iput-object p2, p0, Lcom/navdy/hud/app/manager/InputManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    .line 84
    iput-object p1, p0, Lcom/navdy/hud/app/manager/InputManager;->bus:Lcom/squareup/otto/Bus;

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InputManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 86
    iput-object p3, p0, Lcom/navdy/hud/app/manager/InputManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 87
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/manager/InputManager;Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/InputManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/input/GestureEvent;
    .param p2, "x2"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/manager/InputManager;->invokeHandler(Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/manager/InputManager;)Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/InputManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InputManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-object v0
.end method

.method private convertKeyToCustomEvent(ILandroid/view/KeyEvent;)Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "customKeyEvent":Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    sparse-switch p1, :sswitch_data_0

    .line 216
    :goto_0
    return-object v0

    .line 188
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    goto :goto_0

    .line 191
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 193
    goto :goto_0

    .line 202
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 203
    goto :goto_0

    .line 212
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    goto :goto_0

    .line 180
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x9 -> :sswitch_0
        0xa -> :sswitch_2
        0x14 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x1d -> :sswitch_1
        0x1f -> :sswitch_2
        0x20 -> :sswitch_2
        0x21 -> :sswitch_2
        0x26 -> :sswitch_1
        0x27 -> :sswitch_0
        0x28 -> :sswitch_2
        0x2d -> :sswitch_1
        0x2f -> :sswitch_0
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x36 -> :sswitch_1
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method private invokeHandler(Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 6
    .param p1, "gesture"    # Lcom/navdy/service/library/events/input/GestureEvent;
    .param p2, "keyEvent"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 248
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 249
    const/4 v2, 0x0

    .line 300
    :cond_0
    :goto_0
    return v2

    .line 252
    :cond_1
    const/4 v2, 0x0

    .line 254
    .local v2, "result":Z
    iget-object v3, p0, Lcom/navdy/hud/app/manager/InputManager;->inputHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    if-eqz v3, :cond_3

    .line 255
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InputManager;->inputHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    .line 257
    .local v0, "handler":Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    :goto_1
    if-eqz v0, :cond_3

    .line 258
    if-eqz p1, :cond_5

    .line 259
    invoke-interface {v0, p1}, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;->onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 260
    invoke-interface {v0}, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;->nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    goto :goto_1

    .line 262
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showGestureDetected(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    .line 263
    const/4 v2, 0x1

    .line 278
    .end local v0    # "handler":Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    :cond_3
    :goto_2
    if-nez v2, :cond_4

    if-eqz p2, :cond_4

    .line 279
    sget-object v3, Lcom/navdy/hud/app/manager/InputManager$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 295
    :cond_4
    :goto_3
    if-eqz v2, :cond_0

    .line 297
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/manager/InputManager;->lastInputEventTime:J

    .line 298
    invoke-static {}, Lcom/navdy/hud/app/service/ShutdownMonitor;->getInstance()Lcom/navdy/hud/app/service/ShutdownMonitor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/service/ShutdownMonitor;->recordInputEvent()V

    goto :goto_0

    .line 267
    .restart local v0    # "handler":Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    :cond_5
    invoke-interface {v0, p2}, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 268
    invoke-interface {v0}, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;->nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    goto :goto_1

    .line 270
    :cond_6
    const/4 v2, 0x1

    .line 271
    goto :goto_2

    .line 281
    .end local v0    # "handler":Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    :pswitch_0
    sget-object v3, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "long press not handled"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 282
    iget-object v3, p0, Lcom/navdy/hud/app/manager/InputManager;->defaultHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    if-nez v3, :cond_7

    .line 283
    iget-object v3, p0, Lcom/navdy/hud/app/manager/InputManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v1

    .line 284
    .local v1, "main":Lcom/navdy/hud/app/ui/activity/Main;
    if-eqz v1, :cond_7

    .line 285
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->getInputHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/manager/InputManager;->defaultHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    .line 288
    .end local v1    # "main":Lcom/navdy/hud/app/ui/activity/Main;
    :cond_7
    iget-object v3, p0, Lcom/navdy/hud/app/manager/InputManager;->defaultHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    if-eqz v3, :cond_4

    .line 289
    iget-object v3, p0, Lcom/navdy/hud/app/manager/InputManager;->defaultHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    invoke-interface {v3, p2}, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;->onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v2

    goto :goto_3

    .line 279
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static isCenterKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 220
    const/16 v0, 0x2f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x34

    if-eq p0, v0, :cond_0

    const/16 v0, 0x33

    if-eq p0, v0, :cond_0

    const/16 v0, 0x42

    if-eq p0, v0, :cond_0

    const/16 v0, 0x14

    if-eq p0, v0, :cond_0

    const/16 v0, 0x27

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLeftKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 230
    const/16 v0, 0x1d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x36

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x15

    if-eq p0, v0, :cond_0

    const/16 v0, 0x26

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRightKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 239
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0x1f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x21

    if-eq p0, v0, :cond_0

    const/16 v0, 0x16

    if-eq p0, v0, :cond_0

    const/16 v0, 0x28

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 305
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 306
    .local v0, "parent":Landroid/view/ViewParent;
    :goto_0
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    if-nez v1, :cond_0

    .line 307
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 309
    :cond_0
    if-eqz v0, :cond_1

    .line 310
    check-cast v0, Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    .line 312
    .end local v0    # "parent":Landroid/view/ViewParent;
    :goto_1
    return-object v0

    .restart local v0    # "parent":Landroid/view/ViewParent;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getFocus()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InputManager;->inputHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    return-object v0
.end method

.method public getLastInputEventTime()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/navdy/hud/app/manager/InputManager;->lastInputEventTime:J

    return-wide v0
.end method

.method public injectKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InputManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    sget-object v1, Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;->POWERBUTTON:Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/PowerManager;->wakeUp(Lcom/navdy/hud/app/analytics/AnalyticsSupport$WakeupReason;)V

    .line 317
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InputManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/manager/InputManager$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/manager/InputManager$1;-><init>(Lcom/navdy/hud/app/manager/InputManager;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 333
    return-void
.end method

.method public onGestureEvent(Lcom/navdy/service/library/events/input/GestureEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/manager/InputManager;->invokeHandler(Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    .line 105
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 109
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onKeyDown:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isLongPress:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 113
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v2, p2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 115
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    if-nez v2, :cond_1

    .line 116
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/manager/InputManager;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    .line 119
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v2, p2}, Lcom/navdy/hud/app/device/dial/DialManager;->reportInputEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 121
    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v3

    .line 120
    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->dialActionDetected(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V

    .line 124
    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/navdy/hud/app/manager/InputManager;->isLongPressDetected:Z

    if-eqz v2, :cond_4

    .line 141
    :cond_3
    :goto_0
    return v1

    .line 129
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/manager/InputManager;->convertKeyToCustomEvent(ILandroid/view/KeyEvent;)Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    .line 130
    .local v0, "customKeyEvent":Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    if-eqz v0, :cond_6

    .line 131
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-ne v0, v2, :cond_5

    .line 133
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    goto :goto_0

    .line 137
    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/navdy/hud/app/manager/InputManager;->invokeHandler(Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v1

    goto :goto_0

    .line 141
    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 171
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyLongPress:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 174
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/InputManager;->isLongPressDetected:Z

    .line 175
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/manager/InputManager;->convertKeyToCustomEvent(ILandroid/view/KeyEvent;)Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/manager/InputManager;->invokeHandler(Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    .line 145
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onKeyUp:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isLongPress:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 149
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/manager/InputManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v2, p2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 151
    iget-boolean v2, p0, Lcom/navdy/hud/app/manager/InputManager;->isLongPressDetected:Z

    if-eqz v2, :cond_2

    .line 152
    iput-boolean v1, p0, Lcom/navdy/hud/app/manager/InputManager;->isLongPressDetected:Z

    .line 153
    const/4 v1, 0x1

    .line 166
    :cond_1
    :goto_0
    return v1

    .line 155
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/manager/InputManager;->convertKeyToCustomEvent(ILandroid/view/KeyEvent;)Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    .line 156
    .local v0, "customKeyEvent":Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    if-eqz v0, :cond_1

    .line 160
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 163
    :pswitch_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/navdy/hud/app/manager/InputManager;->invokeHandler(Lcom/navdy/service/library/events/input/GestureEvent;Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v1

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setFocus(Lcom/navdy/hud/app/manager/InputManager$IInputHandler;)V
    .locals 0
    .param p1, "inputHandler"    # Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/hud/app/manager/InputManager;->inputHandler:Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    .line 91
    return-void
.end method
