.class Lcom/navdy/hud/app/manager/InitManager$1;
.super Ljava/lang/Object;
.source "InitManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/InitManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field public notified:Z

.field final synthetic this$0:Lcom/navdy/hud/app/manager/InitManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/InitManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/manager/InitManager;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/navdy/hud/app/manager/InitManager$1;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/navdy/hud/app/manager/InitManager$1;->notified:Z

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager$1;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    # getter for: Lcom/navdy/hud/app/manager/InitManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;
    invoke-static {v0}, Lcom/navdy/hud/app/manager/InitManager;->access$000(Lcom/navdy/hud/app/manager/InitManager;)Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->serviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    # getter for: Lcom/navdy/hud/app/manager/InitManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/manager/InitManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "bt and connection service ready"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/manager/InitManager$1;->notified:Z

    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager$1;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    # getter for: Lcom/navdy/hud/app/manager/InitManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/manager/InitManager;->access$200(Lcom/navdy/hud/app/manager/InitManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/InitEvents$InitPhase;

    sget-object v2, Lcom/navdy/hud/app/event/InitEvents$Phase;->CONNECTION_SERVICE_STARTED:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/InitEvents$InitPhase;-><init>(Lcom/navdy/hud/app/event/InitEvents$Phase;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager$1;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    # getter for: Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/manager/InitManager;->access$300(Lcom/navdy/hud/app/manager/InitManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/InitManager$1;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    sget-object v3, Lcom/navdy/hud/app/event/InitEvents$Phase;->POST_START:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;-><init>(Lcom/navdy/hud/app/manager/InitManager;Lcom/navdy/hud/app/event/InitEvents$Phase;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager$1;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    # getter for: Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/manager/InitManager;->access$300(Lcom/navdy/hud/app/manager/InitManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/InitManager$1;->this$0:Lcom/navdy/hud/app/manager/InitManager;

    sget-object v3, Lcom/navdy/hud/app/event/InitEvents$Phase;->LATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;-><init>(Lcom/navdy/hud/app/manager/InitManager;Lcom/navdy/hud/app/event/InitEvents$Phase;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 46
    :cond_0
    return-void
.end method
