.class Lcom/navdy/hud/app/manager/MusicManager$2;
.super Ljava/lang/Object;
.source "MusicManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/MusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/manager/MusicManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/MusicManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/manager/MusicManager;

    .prologue
    .line 410
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 413
    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    invoke-static {v2}, Lcom/navdy/hud/app/manager/MusicManager;->access$500(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    .line 414
    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$500(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 415
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->lastTrackUpdateTime:J
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$600(Lcom/navdy/hud/app/manager/MusicManager;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 416
    .local v0, "timeDelta":I
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->currentTrack:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    invoke-static {v2}, Lcom/navdy/hud/app/manager/MusicManager;->access$500(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v0

    # setter for: Lcom/navdy/hud/app/manager/MusicManager;->currentPosition:I
    invoke-static {v1, v2}, Lcom/navdy/hud/app/manager/MusicManager;->access$702(Lcom/navdy/hud/app/manager/MusicManager;I)I

    .line 417
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->musicNotification:Lcom/navdy/hud/app/framework/music/MusicNotification;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$800(Lcom/navdy/hud/app/manager/MusicManager;)Lcom/navdy/hud/app/framework/music/MusicNotification;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->currentPosition:I
    invoke-static {v2}, Lcom/navdy/hud/app/manager/MusicManager;->access$700(Lcom/navdy/hud/app/manager/MusicManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/music/MusicNotification;->updateProgressBar(I)V

    .line 419
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$1000(Lcom/navdy/hud/app/manager/MusicManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$2;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->progressBarUpdater:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/manager/MusicManager;->access$900(Lcom/navdy/hud/app/manager/MusicManager;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 421
    .end local v0    # "timeDelta":I
    :cond_0
    return-void
.end method
