.class public Lcom/navdy/hud/app/manager/InitManager;
.super Ljava/lang/Object;
.source "InitManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private checkConnectionServiceReady:Ljava/lang/Runnable;

.field private connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

.field private handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/manager/InitManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/manager/InitManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/service/ConnectionHandler;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "connectionHandler"    # Lcom/navdy/hud/app/service/ConnectionHandler;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Lcom/navdy/hud/app/manager/InitManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/manager/InitManager$1;-><init>(Lcom/navdy/hud/app/manager/InitManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->checkConnectionServiceReady:Ljava/lang/Runnable;

    .line 24
    iput-object p1, p0, Lcom/navdy/hud/app/manager/InitManager;->bus:Lcom/squareup/otto/Bus;

    .line 25
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;

    .line 26
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 27
    iput-object p2, p0, Lcom/navdy/hud/app/manager/InitManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/manager/InitManager;)Lcom/navdy/hud/app/service/ConnectionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/InitManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/hud/app/manager/InitManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/manager/InitManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/InitManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/manager/InitManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/manager/InitManager;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBluetoothChanged(Lcom/navdy/hud/app/event/InitEvents$BluetoothStateChanged;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/InitEvents$BluetoothStateChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/InitManager;->checkConnectionServiceReady:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 72
    return-void
.end method

.method public onConnectionService(Lcom/navdy/hud/app/event/InitEvents$ConnectionServiceStarted;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/InitEvents$ConnectionServiceStarted;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/InitManager;->checkConnectionServiceReady:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 77
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;

    sget-object v2, Lcom/navdy/hud/app/event/InitEvents$Phase;->PRE_USER_INTERACTION:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-direct {v1, p0, v2}, Lcom/navdy/hud/app/manager/InitManager$PhaseEmitter;-><init>(Lcom/navdy/hud/app/manager/InitManager;Lcom/navdy/hud/app/event/InitEvents$Phase;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 52
    iget-object v0, p0, Lcom/navdy/hud/app/manager/InitManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/manager/InitManager;->checkConnectionServiceReady:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 53
    return-void
.end method
