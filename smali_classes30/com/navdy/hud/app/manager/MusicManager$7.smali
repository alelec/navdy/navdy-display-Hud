.class synthetic Lcom/navdy/hud/app/manager/MusicManager$7;
.super Ljava/lang/Object;
.source "MusicManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/MusicManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 441
    invoke-static {}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->values()[Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    .line 446
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->values()[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/manager/MusicManager$7;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_2

    .line 441
    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_0
.end method
