.class public final Lcom/navdy/hud/app/manager/RemoteDeviceManager;
.super Ljava/lang/Object;
.source "RemoteDeviceManager.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/manager/RemoteDeviceManager;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field calendarManager:Lcom/navdy/hud/app/framework/calendar/CalendarManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field featureUtil:Lcom/navdy/hud/app/util/FeatureUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field httpManager:Lcom/navdy/service/library/network/http/IHttpManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field inputManager:Lcom/navdy/hud/app/manager/InputManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field musicManager:Lcom/navdy/hud/app/manager/MusicManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field preferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field telemetryDataManager:Lcom/navdy/hud/app/analytics/TelemetryDataManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field timeHelper:Lcom/navdy/hud/app/common/TimeHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 38
    new-instance v0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->singleton:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 94
    .local v0, "appContext":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 95
    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 97
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->singleton:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    return-object v0
.end method


# virtual methods
.method public doesRemoteDeviceHasCapability(Lcom/navdy/service/library/events/LegacyCapability;)Z
    .locals 4
    .param p1, "capability"    # Lcom/navdy/service/library/events/LegacyCapability;

    .prologue
    const/4 v2, 0x0

    .line 109
    iget-object v3, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v3}, Lcom/navdy/hud/app/service/ConnectionHandler;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 110
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 112
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/navdy/service/library/events/DeviceInfo;->legacyCapabilities:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 114
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_0
    return v2
.end method

.method public getBus()Lcom/squareup/otto/Bus;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method public getCalendarManager()Lcom/navdy/hud/app/framework/calendar/CalendarManager;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->calendarManager:Lcom/navdy/hud/app/framework/calendar/CalendarManager;

    return-object v0
.end method

.method public getCallManager()Lcom/navdy/hud/app/framework/phonecall/CallManager;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    return-object v0
.end method

.method public getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    return-object v0
.end method

.method public getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 2

    .prologue
    .line 119
    iget-object v1, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ConnectionHandler;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 120
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    .line 123
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDriveRecorder()Lcom/navdy/hud/app/debug/DriveRecorder;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    return-object v0
.end method

.method public getFeatureUtil()Lcom/navdy/hud/app/util/FeatureUtil;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->featureUtil:Lcom/navdy/hud/app/util/FeatureUtil;

    return-object v0
.end method

.method public getGestureServiceConnector()Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    return-object v0
.end method

.method public getHttpManager()Lcom/navdy/service/library/network/http/IHttpManager;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->httpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    return-object v0
.end method

.method public getInputManager()Lcom/navdy/hud/app/manager/InputManager;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->inputManager:Lcom/navdy/hud/app/manager/InputManager;

    return-object v0
.end method

.method public getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    return-object v0
.end method

.method public getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;
    .locals 2

    .prologue
    .line 100
    iget-object v1, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ConnectionHandler;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 101
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v1

    .line 104
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .locals 3

    .prologue
    .line 148
    iget-object v2, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/ConnectionHandler;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 149
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_1

    .line 150
    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 151
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_0

    .line 152
    iget-object v2, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    .line 157
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :goto_0
    return-object v2

    .line 154
    .restart local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_0
    sget-object v2, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    goto :goto_0

    .line 157
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->preferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public getTelemetryDataManager()Lcom/navdy/hud/app/analytics/TelemetryDataManager;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->telemetryDataManager:Lcom/navdy/hud/app/analytics/TelemetryDataManager;

    return-object v0
.end method

.method public getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    return-object v0
.end method

.method public getTripManager()Lcom/navdy/hud/app/framework/trips/TripManager;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->tripManager:Lcom/navdy/hud/app/framework/trips/TripManager;

    return-object v0
.end method

.method public getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    return-object v0
.end method

.method public getVoiceSearchHandler()Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    return-object v0
.end method

.method public isAppConnected()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/ConnectionHandler;->isAppClosed()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 135
    :cond_0
    return v0
.end method

.method public isNetworkLinkReady()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/service/ConnectionHandler;->isNetworkLinkReady()Z

    move-result v0

    return v0
.end method

.method public isRemoteDeviceConnected()Z
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemoteDeviceIos()Z
    .locals 2

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v0

    .line 162
    .local v0, "platform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method
