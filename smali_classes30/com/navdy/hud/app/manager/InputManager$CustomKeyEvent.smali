.class public final enum Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
.super Ljava/lang/Enum;
.source "InputManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/manager/InputManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CustomKeyEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final enum LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final enum LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final enum POWER_BUTTON_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final enum POWER_BUTTON_DOUBLE_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final enum POWER_BUTTON_LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final enum RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

.field public static final enum SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 52
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 53
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const-string v1, "SELECT"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 54
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const-string v1, "LONG_PRESS"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 55
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const-string v1, "POWER_BUTTON_CLICK"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 56
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const-string v1, "POWER_BUTTON_DOUBLE_CLICK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_DOUBLE_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 57
    new-instance v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const-string v1, "POWER_BUTTON_LONG_PRESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .line 50
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_DOUBLE_CLICK:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->POWER_BUTTON_LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->$VALUES:[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->$VALUES:[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    return-object v0
.end method
