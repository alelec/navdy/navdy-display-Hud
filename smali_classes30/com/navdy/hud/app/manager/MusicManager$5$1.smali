.class Lcom/navdy/hud/app/manager/MusicManager$5$1;
.super Ljava/lang/Object;
.source "MusicManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/manager/MusicManager$5;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/manager/MusicManager$5;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/manager/MusicManager$5;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/manager/MusicManager$5;

    .prologue
    .line 750
    iput-object p1, p0, Lcom/navdy/hud/app/manager/MusicManager$5$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 753
    iget-object v1, p0, Lcom/navdy/hud/app/manager/MusicManager$5$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$5;

    iget-object v1, v1, Lcom/navdy/hud/app/manager/MusicManager$5;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->musicUpdateListeners:Ljava/util/Set;
    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->access$1200(Lcom/navdy/hud/app/manager/MusicManager;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;

    .line 754
    .local v0, "listener":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    iget-object v2, p0, Lcom/navdy/hud/app/manager/MusicManager$5$1;->this$1:Lcom/navdy/hud/app/manager/MusicManager$5;

    iget-object v2, v2, Lcom/navdy/hud/app/manager/MusicManager$5;->this$0:Lcom/navdy/hud/app/manager/MusicManager;

    # getter for: Lcom/navdy/hud/app/manager/MusicManager;->currentPhoto:Lokio/ByteString;
    invoke-static {v2}, Lcom/navdy/hud/app/manager/MusicManager;->access$100(Lcom/navdy/hud/app/manager/MusicManager;)Lokio/ByteString;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;->onAlbumArtUpdate(Lokio/ByteString;Z)V

    goto :goto_0

    .line 756
    .end local v0    # "listener":Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;
    :cond_0
    return-void
.end method
