.class public final Lcom/navdy/hud/app/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept:I = 0x7f09000f

.field public static final add_fuel_stop:I = 0x7f090010

.field public static final add_to_trip:I = 0x7f090011

.field public static final albums:I = 0x7f090012

.field public static final allow_contact_access_android_title:I = 0x7f090013

.field public static final allow_contact_access_iphone_title:I = 0x7f090014

.field public static final already_in_state:I = 0x7f090015

.field public static final am:I = 0x7f090016

.field public static final am_marker:I = 0x7f090017

.field public static final app_disconnected:I = 0x7f090018

.field public static final app_disconnected_msg:I = 0x7f090019

.field public static final app_disconnected_msg_launch_failed:I = 0x7f09001a

.field public static final app_disconnected_title:I = 0x7f09001b

.field public static final app_name:I = 0x7f09001c

.field public static final apple_podcasts:I = 0x7f09001d

.field public static final arrived:I = 0x7f09001e

.field public static final arrived_at_destination:I = 0x7f090000

.field public static final artists:I = 0x7f09001f

.field public static final atm:I = 0x7f090020

.field public static final auto_brightness_choice_cancel:I = 0x7f090021

.field public static final auto_brightness_choice_disable:I = 0x7f090022

.field public static final auto_brightness_choice_enable:I = 0x7f090023

.field public static final auto_brightness_description_text:I = 0x7f090024

.field public static final auto_brightness_off_status_label:I = 0x7f090025

.field public static final auto_brightness_off_title:I = 0x7f090026

.field public static final auto_brightness_on_status_label:I = 0x7f090027

.field public static final auto_brightness_on_title:I = 0x7f090028

.field public static final auto_brightness_status_label_text:I = 0x7f090029

.field public static final auto_zoom:I = 0x7f09002a

.field public static final avg:I = 0x7f09002b

.field public static final avoid_overheating:I = 0x7f09002c

.field public static final aws_access_key_id:I = 0x7f090323

.field public static final aws_secret_access_key:I = 0x7f090324

.field public static final back:I = 0x7f09002d

.field public static final bluetooth_confirm:I = 0x7f09002e

.field public static final bluetooth_device:I = 0x7f09002f

.field public static final bluetooth_init:I = 0x7f090030

.field public static final bluetooth_microphone:I = 0x7f090031

.field public static final bluetooth_pairing_code:I = 0x7f090032

.field public static final bluetooth_pairing_title:I = 0x7f090033

.field public static final boot_bluetooth_warning:I = 0x7f090034

.field public static final brightness_auto_suffix:I = 0x7f090035

.field public static final brightness_notification_subtitle:I = 0x7f090036

.field public static final brightness_notification_title:I = 0x7f090037

.field public static final bt:I = 0x7f090038

.field public static final calc_route:I = 0x7f090039

.field public static final call:I = 0x7f09003a

.field public static final call_accept:I = 0x7f09003b

.field public static final call_cancel:I = 0x7f09003c

.field public static final call_dialing:I = 0x7f09003d

.field public static final call_dismiss:I = 0x7f09003e

.field public static final call_end:I = 0x7f09003f

.field public static final call_ended:I = 0x7f090040

.field public static final call_failed:I = 0x7f090041

.field public static final call_ignore:I = 0x7f090042

.field public static final call_missed:I = 0x7f090043

.field public static final call_mute:I = 0x7f090044

.field public static final call_muted:I = 0x7f090045

.field public static final call_name:I = 0x7f090046

.field public static final call_no_name:I = 0x7f090047

.field public static final call_person:I = 0x7f090048

.field public static final call_type:I = 0x7f090049

.field public static final call_unmute:I = 0x7f09004a

.field public static final caller_unknown:I = 0x7f09004b

.field public static final camera_warning_both:I = 0x7f09004c

.field public static final camera_warning_red_light:I = 0x7f09004d

.field public static final camera_warning_speed:I = 0x7f09004e

.field public static final cancel:I = 0x7f09004f

.field public static final cancel_trip:I = 0x7f090050

.field public static final cannot_move_state:I = 0x7f090051

.field public static final car_md_auth:I = 0x7f090325

.field public static final car_md_token:I = 0x7f090326

.field public static final carousel_destinations_exit_title:I = 0x7f090052

.field public static final carousel_destinations_main_title:I = 0x7f090053

.field public static final carousel_destinations_voice_search_title:I = 0x7f090054

.field public static final carousel_fav_contacts_exit_title:I = 0x7f090055

.field public static final carousel_fav_contacts_main_title:I = 0x7f090056

.field public static final carousel_menu_brightness_description:I = 0x7f090057

.field public static final carousel_menu_connect_description:I = 0x7f090058

.field public static final carousel_menu_contacts:I = 0x7f090059

.field public static final carousel_menu_dial_pairing:I = 0x7f09005a

.field public static final carousel_menu_fav_contacts_description:I = 0x7f09005b

.field public static final carousel_menu_fav_contacts_title:I = 0x7f09005c

.field public static final carousel_menu_fav_places_description:I = 0x7f09005d

.field public static final carousel_menu_fav_places_title:I = 0x7f09005e

.field public static final carousel_menu_main_title:I = 0x7f09005f

.field public static final carousel_menu_map_description:I = 0x7f090060

.field public static final carousel_menu_map_dial_pairing_description:I = 0x7f090061

.field public static final carousel_menu_map_options:I = 0x7f090062

.field public static final carousel_menu_map_places:I = 0x7f090063

.field public static final carousel_menu_map_title:I = 0x7f090064

.field public static final carousel_menu_map_update_mode_description:I = 0x7f090065

.field public static final carousel_menu_music_control:I = 0x7f090066

.field public static final carousel_menu_music_control_description:I = 0x7f090067

.field public static final carousel_menu_no_fav_contacts_description_android:I = 0x7f090068

.field public static final carousel_menu_no_fav_contacts_description_ios:I = 0x7f090069

.field public static final carousel_menu_no_fav_contacts_title:I = 0x7f09006a

.field public static final carousel_menu_no_fav_places_description:I = 0x7f09006b

.field public static final carousel_menu_no_recent_contacts_description:I = 0x7f09006c

.field public static final carousel_menu_no_recent_contacts_title:I = 0x7f09006d

.field public static final carousel_menu_no_suggested_places_description:I = 0x7f09006e

.field public static final carousel_menu_recent_contacts_description:I = 0x7f09006f

.field public static final carousel_menu_recent_contacts_title:I = 0x7f090070

.field public static final carousel_menu_recent_place:I = 0x7f090071

.field public static final carousel_menu_search_title:I = 0x7f090072

.field public static final carousel_menu_settings_description:I = 0x7f090073

.field public static final carousel_menu_settings_title:I = 0x7f090074

.field public static final carousel_menu_shutdown_description:I = 0x7f090075

.field public static final carousel_menu_smartdash_description:I = 0x7f090076

.field public static final carousel_menu_smartdash_options:I = 0x7f090077

.field public static final carousel_menu_smartdash_select_center_gauge:I = 0x7f090078

.field public static final carousel_menu_smartdash_side_gauges:I = 0x7f090079

.field public static final carousel_menu_smartdash_title:I = 0x7f09007a

.field public static final carousel_menu_software_update_description:I = 0x7f09007b

.field public static final carousel_menu_suggested_places_description:I = 0x7f09007c

.field public static final carousel_menu_system_info_description:I = 0x7f09007d

.field public static final carousel_menu_system_info_title:I = 0x7f09007e

.field public static final carousel_menu_voice_control_description:I = 0x7f09007f

.field public static final carousel_menu_voice_control_description_android:I = 0x7f090080

.field public static final carousel_menu_voice_control_description_ios:I = 0x7f090081

.field public static final carousel_menu_voice_control_google:I = 0x7f090082

.field public static final carousel_menu_voice_control_siri:I = 0x7f090083

.field public static final carousel_menu_voice_search_description:I = 0x7f090084

.field public static final carousel_music_albums:I = 0x7f090085

.field public static final carousel_music_artists:I = 0x7f090086

.field public static final carousel_music_exit_title:I = 0x7f090087

.field public static final carousel_music_genres:I = 0x7f090088

.field public static final carousel_music_playlists:I = 0x7f090089

.field public static final carousel_places_exit_title:I = 0x7f09008a

.field public static final carousel_places_main_title:I = 0x7f09008b

.field public static final carousel_recents_exit_title:I = 0x7f09008c

.field public static final carousel_recents_main_title:I = 0x7f09008d

.field public static final carousel_search_atm:I = 0x7f09008e

.field public static final carousel_search_coffee:I = 0x7f09008f

.field public static final carousel_search_food:I = 0x7f090090

.field public static final carousel_search_gas:I = 0x7f090091

.field public static final carousel_search_grocery_store:I = 0x7f090092

.field public static final carousel_search_hospital:I = 0x7f090093

.field public static final carousel_search_parking:I = 0x7f090094

.field public static final carousel_search_parks:I = 0x7f090095

.field public static final carousel_settings_auto_brightness_description:I = 0x7f090096

.field public static final carousel_settings_auto_brightness_title:I = 0x7f090097

.field public static final carousel_settings_brightness:I = 0x7f090098

.field public static final carousel_settings_brightness_description:I = 0x7f090099

.field public static final carousel_settings_brightness_title:I = 0x7f09009a

.field public static final carousel_settings_connect_phone_description:I = 0x7f09009b

.field public static final carousel_settings_connect_phone_title:I = 0x7f09009c

.field public static final carousel_settings_dial_update_description:I = 0x7f09009d

.field public static final carousel_settings_dial_update_title:I = 0x7f09009e

.field public static final carousel_settings_factory_reset_description:I = 0x7f09009f

.field public static final carousel_settings_factory_reset_title:I = 0x7f0900a0

.field public static final carousel_settings_learning_gesture_description:I = 0x7f0900a1

.field public static final carousel_settings_learning_gesture_title:I = 0x7f0900a2

.field public static final carousel_settings_shutdown_description:I = 0x7f0900a3

.field public static final carousel_settings_shutdown_title:I = 0x7f0900a4

.field public static final carousel_settings_software_update_description:I = 0x7f0900a5

.field public static final carousel_settings_software_update_title:I = 0x7f0900a6

.field public static final click_to_expire:I = 0x7f090001

.field public static final click_to_reply:I = 0x7f090002

.field public static final click_to_view:I = 0x7f090003

.field public static final close:I = 0x7f0900a7

.field public static final coffee:I = 0x7f0900a8

.field public static final connect:I = 0x7f0900a9

.field public static final connecting_gps:I = 0x7f0900aa

.field public static final connection_status_connected:I = 0x7f0900ab

.field public static final connection_status_disconnected:I = 0x7f0900ac

.field public static final connection_status_unknown:I = 0x7f0900ad

.field public static final contact_person:I = 0x7f0900ae

.field public static final count:I = 0x7f0900af

.field public static final cpu:I = 0x7f0900b0

.field public static final define_roundedimageview:I = 0x7f090327

.field public static final destination_change_navigation_title:I = 0x7f0900b1

.field public static final destination_home_initials:I = 0x7f0900b2

.field public static final destination_start_navigation_cancel:I = 0x7f0900b3

.field public static final destination_start_navigation_yes:I = 0x7f0900b4

.field public static final destination_work_initials:I = 0x7f0900b5

.field public static final dial_battery_level_extremely_low:I = 0x7f0900b6

.field public static final dial_battery_level_low:I = 0x7f0900b7

.field public static final dial_battery_level_ok:I = 0x7f0900b8

.field public static final dial_battery_level_very_low:I = 0x7f0900b9

.field public static final dial_ex_low_battery:I = 0x7f0900ba

.field public static final dial_firmware_rev:I = 0x7f0900bb

.field public static final dial_firmware_update_message:I = 0x7f0900bc

.field public static final dial_firmware_update_message_failed:I = 0x7f0900bd

.field public static final dial_firmware_update_message_success:I = 0x7f0900be

.field public static final dial_firmware_update_title:I = 0x7f0900bf

.field public static final dial_firmware_update_update:I = 0x7f0900c0

.field public static final dial_forgotten:I = 0x7f0900c1

.field public static final dial_forgotten_multiple:I = 0x7f0900c2

.field public static final dial_low_battery:I = 0x7f0900c3

.field public static final dial_not_found:I = 0x7f0900c4

.field public static final dial_paired_choice_dismiss:I = 0x7f0900c5

.field public static final dial_paired_choice_forget:I = 0x7f0900c6

.field public static final dial_paired_hint:I = 0x7f0900c7

.field public static final dial_paired_instructions:I = 0x7f0900c8

.field public static final dial_paired_text:I = 0x7f0900c9

.field public static final dial_paired_title:I = 0x7f0900ca

.field public static final dial_pairing:I = 0x7f0900cb

.field public static final dial_pairing_failed:I = 0x7f0900cc

.field public static final dial_searching_hint:I = 0x7f0900cd

.field public static final dial_searching_text:I = 0x7f0900ce

.field public static final dial_searching_text_long:I = 0x7f0900cf

.field public static final dial_software_update:I = 0x7f0900d0

.field public static final dial_str:I = 0x7f0900d1

.field public static final dial_update_available:I = 0x7f0900d2

.field public static final dial_update_err:I = 0x7f0900d3

.field public static final dial_update_installation_will_begin_in:I = 0x7f0900d4

.field public static final dial_update_installation_will_take:I = 0x7f0900d5

.field public static final dial_update_ready_to_install:I = 0x7f0900d6

.field public static final dial_update_started:I = 0x7f0900d7

.field public static final dial_update_warning:I = 0x7f0900d8

.field public static final dial_update_will_upgrade_to_from:I = 0x7f0900d9

.field public static final dial_very_low_battery:I = 0x7f0900da

.field public static final disable_traffic:I = 0x7f0900db

.field public static final dismiss:I = 0x7f0900dc

.field public static final do_not_remove_power:I = 0x7f0900dd

.field public static final do_not_remove_power_or_turn_off_vehicle:I = 0x7f0900de

.field public static final done:I = 0x7f0900df

.field public static final done_recording:I = 0x7f0900e0

.field public static final done_recording_detail:I = 0x7f0900e1

.field public static final dont_shutdown:I = 0x7f0900e2

.field public static final double_tap_1:I = 0x7f0900e3

.field public static final double_tap_2:I = 0x7f0900e4

.field public static final double_tap_detail_1:I = 0x7f0900e5

.field public static final double_tap_detail_2:I = 0x7f0900e6

.field public static final drive_score:I = 0x7f0900e7

.field public static final empty_gauge:I = 0x7f0900e8

.field public static final empty_queue:I = 0x7f0900e9

.field public static final empty_search_query:I = 0x7f0900ea

.field public static final enable_internet:I = 0x7f0900eb

.field public static final enable_traffic:I = 0x7f0900ec

.field public static final end_maneuver:I = 0x7f0900ed

.field public static final end_maneuver_direction:I = 0x7f0900ee

.field public static final end_maneuver_direction_no_road:I = 0x7f0900ef

.field public static final end_maneuver_turn:I = 0x7f0900f0

.field public static final end_trip:I = 0x7f0900f1

.field public static final enter_highway:I = 0x7f0900f2

.field public static final eta:I = 0x7f0900f3

.field public static final eta_inaccurate_traffic:I = 0x7f0900f4

.field public static final eta_time_day:I = 0x7f0900f5

.field public static final eta_time_day_no_hour:I = 0x7f0900f6

.field public static final eta_time_hour:I = 0x7f0900f7

.field public static final eta_time_hour_no_min:I = 0x7f0900f8

.field public static final eta_time_min:I = 0x7f0900f9

.field public static final excessive_speeding:I = 0x7f0900fa

.field public static final exit:I = 0x7f0900fb

.field public static final exit_number:I = 0x7f0900fc

.field public static final exit_recording:I = 0x7f0900fd

.field public static final facebook:I = 0x7f0900fe

.field public static final factory_reset_cancel:I = 0x7f0900ff

.field public static final factory_reset_description:I = 0x7f090100

.field public static final factory_reset_go:I = 0x7f090101

.field public static final factory_reset_screen_title:I = 0x7f090102

.field public static final fastest_route:I = 0x7f090103

.field public static final finding_location:I = 0x7f090104

.field public static final finding_route:I = 0x7f090105

.field public static final food:I = 0x7f090106

.field public static final fuel_low:I = 0x7f090107

.field public static final gas:I = 0x7f090108

.field public static final gas_station:I = 0x7f090109

.field public static final gas_station_detail:I = 0x7f09010a

.field public static final gauge_label_compass:I = 0x7f09010b

.field public static final gauge_label_fuel:I = 0x7f09010c

.field public static final gauge_label_fuel_range:I = 0x7f09010d

.field public static final gauge_label_krpm:I = 0x7f09010e

.field public static final gauge_speedo_meter:I = 0x7f09010f

.field public static final gauge_tacho_meter:I = 0x7f090110

.field public static final geocoding_failed:I = 0x7f090111

.field public static final gesture_tips_title:I = 0x7f090112

.field public static final glances:I = 0x7f090113

.field public static final glances_cannot_delete:I = 0x7f090114

.field public static final glances_deleted:I = 0x7f090115

.field public static final glances_disabled:I = 0x7f090116

.field public static final glances_disabled_msg:I = 0x7f090117

.field public static final glances_dismiss:I = 0x7f090118

.field public static final glances_dismiss_all:I = 0x7f090119

.field public static final glances_none:I = 0x7f09011a

.field public static final glances_ok:I = 0x7f09011b

.field public static final glympse_api_key:I = 0x7f090328

.field public static final glympse_was_received:I = 0x7f090004

.field public static final go:I = 0x7f09011c

.field public static final google_now:I = 0x7f09011d

.field public static final gps:I = 0x7f09011e

.field public static final gps_calibrated:I = 0x7f09011f

.field public static final gps_calibrating_fusion:I = 0x7f090120

.field public static final gps_calibrating_sensor:I = 0x7f090121

.field public static final gps_calibration_unknown:I = 0x7f090122

.field public static final gps_connnected:I = 0x7f090123

.field public static final gps_current:I = 0x7f090124

.field public static final gps_fix_type:I = 0x7f090125

.field public static final gps_location:I = 0x7f090126

.field public static final gps_lost:I = 0x7f090127

.field public static final gps_no_fix:I = 0x7f090128

.field public static final gps_phone:I = 0x7f090129

.field public static final gps_satellites_in_use:I = 0x7f09012a

.field public static final gps_signal_level:I = 0x7f09012b

.field public static final gps_speed:I = 0x7f09012c

.field public static final gps_ublox:I = 0x7f09012d

.field public static final gps_ublox_colon:I = 0x7f09012e

.field public static final gps_ublox_navdy:I = 0x7f09012f

.field public static final grocery_store:I = 0x7f090130

.field public static final group_invitation_was_received:I = 0x7f090005

.field public static final hard_accel:I = 0x7f090131

.field public static final hard_braking:I = 0x7f090132

.field public static final here_maps_appid:I = 0x7f090329

.field public static final here_maps_apptoken:I = 0x7f09032a

.field public static final here_maps_license_key:I = 0x7f09032b

.field public static final high_g:I = 0x7f090133

.field public static final hockeyapp_crash_dialog_app_name_fallback:I = 0x7f090134

.field public static final hockeyapp_crash_dialog_message:I = 0x7f090135

.field public static final hockeyapp_crash_dialog_negative_button:I = 0x7f090136

.field public static final hockeyapp_crash_dialog_neutral_button:I = 0x7f090137

.field public static final hockeyapp_crash_dialog_positive_button:I = 0x7f090138

.field public static final hockeyapp_crash_dialog_title:I = 0x7f090139

.field public static final hockeyapp_dialog_error_message:I = 0x7f09013a

.field public static final hockeyapp_dialog_error_title:I = 0x7f09013b

.field public static final hockeyapp_dialog_negative_button:I = 0x7f09013c

.field public static final hockeyapp_dialog_positive_button:I = 0x7f09013d

.field public static final hockeyapp_download_failed_dialog_message:I = 0x7f09013e

.field public static final hockeyapp_download_failed_dialog_negative_button:I = 0x7f09013f

.field public static final hockeyapp_download_failed_dialog_positive_button:I = 0x7f090140

.field public static final hockeyapp_download_failed_dialog_title:I = 0x7f090141

.field public static final hockeyapp_error_no_network_message:I = 0x7f090142

.field public static final hockeyapp_expiry_info_text:I = 0x7f090143

.field public static final hockeyapp_expiry_info_title:I = 0x7f090144

.field public static final hockeyapp_feedback_attach_file:I = 0x7f090145

.field public static final hockeyapp_feedback_attach_picture:I = 0x7f090146

.field public static final hockeyapp_feedback_attachment_button_text:I = 0x7f090147

.field public static final hockeyapp_feedback_attachment_error:I = 0x7f090148

.field public static final hockeyapp_feedback_attachment_loading:I = 0x7f090149

.field public static final hockeyapp_feedback_email_hint:I = 0x7f09014a

.field public static final hockeyapp_feedback_failed_text:I = 0x7f09014b

.field public static final hockeyapp_feedback_failed_title:I = 0x7f09014c

.field public static final hockeyapp_feedback_fetching_feedback_text:I = 0x7f09014d

.field public static final hockeyapp_feedback_generic_error:I = 0x7f09014e

.field public static final hockeyapp_feedback_last_updated_text:I = 0x7f09014f

.field public static final hockeyapp_feedback_max_attachments_allowed:I = 0x7f090150

.field public static final hockeyapp_feedback_message_hint:I = 0x7f090151

.field public static final hockeyapp_feedback_name_hint:I = 0x7f090152

.field public static final hockeyapp_feedback_refresh_button_text:I = 0x7f090153

.field public static final hockeyapp_feedback_response_button_text:I = 0x7f090154

.field public static final hockeyapp_feedback_select_file:I = 0x7f090155

.field public static final hockeyapp_feedback_select_picture:I = 0x7f090156

.field public static final hockeyapp_feedback_send_button_text:I = 0x7f090157

.field public static final hockeyapp_feedback_send_generic_error:I = 0x7f090158

.field public static final hockeyapp_feedback_send_network_error:I = 0x7f090159

.field public static final hockeyapp_feedback_sending_feedback_text:I = 0x7f09015a

.field public static final hockeyapp_feedback_subject_hint:I = 0x7f09015b

.field public static final hockeyapp_feedback_title:I = 0x7f09015c

.field public static final hockeyapp_feedback_validate_email_empty:I = 0x7f09015d

.field public static final hockeyapp_feedback_validate_email_error:I = 0x7f09015e

.field public static final hockeyapp_feedback_validate_name_error:I = 0x7f09015f

.field public static final hockeyapp_feedback_validate_subject_error:I = 0x7f090160

.field public static final hockeyapp_feedback_validate_text_error:I = 0x7f090161

.field public static final hockeyapp_login_email_hint:I = 0x7f090162

.field public static final hockeyapp_login_headline_text:I = 0x7f090163

.field public static final hockeyapp_login_headline_text_email_only:I = 0x7f090164

.field public static final hockeyapp_login_login_button_text:I = 0x7f090165

.field public static final hockeyapp_login_missing_credentials_toast:I = 0x7f090166

.field public static final hockeyapp_login_password_hint:I = 0x7f090167

.field public static final hockeyapp_paint_dialog_message:I = 0x7f090168

.field public static final hockeyapp_paint_dialog_negative_button:I = 0x7f090169

.field public static final hockeyapp_paint_dialog_neutral_button:I = 0x7f09016a

.field public static final hockeyapp_paint_dialog_positive_button:I = 0x7f09016b

.field public static final hockeyapp_paint_indicator_toast:I = 0x7f09016c

.field public static final hockeyapp_paint_menu_clear:I = 0x7f09016d

.field public static final hockeyapp_paint_menu_save:I = 0x7f09016e

.field public static final hockeyapp_paint_menu_undo:I = 0x7f09016f

.field public static final hockeyapp_permission_dialog_negative_button:I = 0x7f090170

.field public static final hockeyapp_permission_dialog_positive_button:I = 0x7f090171

.field public static final hockeyapp_permission_update_message:I = 0x7f090172

.field public static final hockeyapp_permission_update_title:I = 0x7f090173

.field public static final hockeyapp_update_button:I = 0x7f090174

.field public static final hockeyapp_update_dialog_message:I = 0x7f090175

.field public static final hockeyapp_update_dialog_negative_button:I = 0x7f090176

.field public static final hockeyapp_update_dialog_positive_button:I = 0x7f090177

.field public static final hockeyapp_update_dialog_title:I = 0x7f090178

.field public static final hockeyapp_update_mandatory_toast:I = 0x7f090179

.field public static final hockeyapp_update_version_details_label:I = 0x7f09032c

.field public static final home:I = 0x7f09017a

.field public static final hospital:I = 0x7f09017b

.field public static final hour:I = 0x7f09017c

.field public static final hours:I = 0x7f09017d

.field public static final hr:I = 0x7f09017e

.field public static final hud_update_download:I = 0x7f09017f

.field public static final hud_update_install:I = 0x7f090180

.field public static final hyphen:I = 0x7f090181

.field public static final i_am_driving:I = 0x7f090182

.field public static final i_am_here:I = 0x7f090183

.field public static final i_can_add_trip_gas_station:I = 0x7f090184

.field public static final i_cannot_add_gasstation:I = 0x7f090185

.field public static final ignore:I = 0x7f090186

.field public static final incoming_call:I = 0x7f090187

.field public static final install:I = 0x7f090188

.field public static final install_and_shutdown:I = 0x7f090189

.field public static final install_later:I = 0x7f09018a

.field public static final install_now:I = 0x7f09018b

.field public static final invalid_param:I = 0x7f09018c

.field public static final invalid_routeid:I = 0x7f09018d

.field public static final invalid_state:I = 0x7f09018e

.field public static final invalid_target_state:I = 0x7f09018f

.field public static final ios_sms_format:I = 0x7f090190

.field public static final issue_successfully_sent:I = 0x7f090191

.field public static final jira_credentials:I = 0x7f09032d

.field public static final junction:I = 0x7f090192

.field public static final keep_left:I = 0x7f090193

.field public static final keep_middle:I = 0x7f090194

.field public static final keep_right:I = 0x7f090195

.field public static final kilometers_per_hour:I = 0x7f090196

.field public static final kmpl:I = 0x7f090197

.field public static final krpm:I = 0x7f090198

.field public static final launch:I = 0x7f090199

.field public static final left:I = 0x7f09019a

.field public static final library_roundedimageview_author:I = 0x7f09032e

.field public static final library_roundedimageview_authorWebsite:I = 0x7f09032f

.field public static final library_roundedimageview_isOpenSource:I = 0x7f090330

.field public static final library_roundedimageview_libraryDescription:I = 0x7f090331

.field public static final library_roundedimageview_libraryName:I = 0x7f090332

.field public static final library_roundedimageview_libraryVersion:I = 0x7f090333

.field public static final library_roundedimageview_libraryWebsite:I = 0x7f090334

.field public static final library_roundedimageview_licenseId:I = 0x7f090335

.field public static final library_roundedimageview_repositoryLink:I = 0x7f090336

.field public static final limit:I = 0x7f09019b

.field public static final litre:I = 0x7f090337

.field public static final local_music:I = 0x7f09019c

.field public static final locale_not_supported:I = 0x7f09019d

.field public static final localytics_app_key:I = 0x7f090338

.field public static final location_sent:I = 0x7f09019e

.field public static final long_drive_error:I = 0x7f09019f

.field public static final longer:I = 0x7f0901a0

.field public static final low_battey_warning:I = 0x7f090006

.field public static final manual_zoom:I = 0x7f0901a1

.field public static final map_engine_not_ready:I = 0x7f0901a2

.field public static final maps:I = 0x7f0901a3

.field public static final max:I = 0x7f0901a4

.field public static final memory:I = 0x7f0901a5

.field public static final menu:I = 0x7f0901a6

.field public static final menu_str:I = 0x7f0901a7

.field public static final message:I = 0x7f0901a8

.field public static final meters_per_second:I = 0x7f0901a9

.field public static final miles_per_hour:I = 0x7f0901aa

.field public static final min:I = 0x7f0901ab

.field public static final minimum:I = 0x7f0901ac

.field public static final minute:I = 0x7f0901ad

.field public static final minutes:I = 0x7f0901ae

.field public static final mm_active_trip:I = 0x7f0901af

.field public static final mm_active_trip_arrive:I = 0x7f0901b0

.field public static final mm_active_trip_arrived:I = 0x7f0901b1

.field public static final mm_active_trip_arrived_dist:I = 0x7f0901b2

.field public static final mm_active_trip_arrived_no_label:I = 0x7f0901b3

.field public static final mm_active_trip_eta:I = 0x7f0901b4

.field public static final mm_active_trip_formatted:I = 0x7f0901b5

.field public static final mm_call_info:I = 0x7f0901b6

.field public static final mm_dial_update:I = 0x7f0901b7

.field public static final mm_glances:I = 0x7f0901b8

.field public static final mm_glances_disabled:I = 0x7f0901b9

.field public static final mm_glances_single:I = 0x7f0901ba

.field public static final mm_glances_subtitle:I = 0x7f0901bb

.field public static final mm_navdy_update:I = 0x7f0901bc

.field public static final mm_voice_search:I = 0x7f0901bd

.field public static final mm_voice_search_description:I = 0x7f0901be

.field public static final mobile:I = 0x7f0901bf

.field public static final more_routes:I = 0x7f0901c0

.field public static final mpg:I = 0x7f0901c1

.field public static final mph:I = 0x7f0901c2

.field public static final music:I = 0x7f0901c3

.field public static final music_enable_permissions_subtitle_android:I = 0x7f0901c4

.field public static final music_enable_permissions_subtitle_ios:I = 0x7f0901c5

.field public static final music_enable_permissions_title_android:I = 0x7f0901c6

.field public static final music_enable_permissions_title_ios:I = 0x7f0901c7

.field public static final music_init_title:I = 0x7f0901c8

.field public static final music_library:I = 0x7f0901c9

.field public static final music_pandora_ad_title:I = 0x7f0901ca

.field public static final music_paused:I = 0x7f0901cb

.field public static final mute_tbt_2:I = 0x7f0901cc

.field public static final navdy_dial:I = 0x7f0901cd

.field public static final navdy_dial_name:I = 0x7f0901ce

.field public static final navdy_dial_repair_text:I = 0x7f0901cf

.field public static final navdy_inactivity:I = 0x7f0901d0

.field public static final navdy_is_listening_to:I = 0x7f0901d1

.field public static final navdy_too_hot:I = 0x7f0901d2

.field public static final navigation_muted:I = 0x7f0901d3

.field public static final navigation_unmuted:I = 0x7f0901d4

.field public static final network:I = 0x7f0901d5

.field public static final network_not_initialized:I = 0x7f0901d6

.field public static final new_dial_paired_title:I = 0x7f0901d7

.field public static final next_recording_step:I = 0x7f0901d8

.field public static final no_active_glympses:I = 0x7f090007

.field public static final no_current_position:I = 0x7f0901d9

.field public static final no_more_events:I = 0x7f0901da

.field public static final no_network_connection:I = 0x7f090008

.field public static final no_turn:I = 0x7f0901db

.field public static final notification:I = 0x7f0901dc

.field public static final notification_answer:I = 0x7f0901dd

.field public static final notification_ask_navdy:I = 0x7f0901de

.field public static final notification_end:I = 0x7f0901df

.field public static final notification_ignore:I = 0x7f0901e0

.field public static final notification_listening:I = 0x7f0901e1

.field public static final notification_previous:I = 0x7f0901e2

.field public static final now:I = 0x7f0901e3

.field public static final obd:I = 0x7f0901e4

.field public static final obd_connected:I = 0x7f0901e5

.field public static final obd_no_speed_pid:I = 0x7f0901e6

.field public static final obd_not_connected:I = 0x7f0901e7

.field public static final off:I = 0x7f0901e8

.field public static final offline:I = 0x7f0901e9

.field public static final offline_maps:I = 0x7f0901ea

.field public static final on:I = 0x7f0901eb

.field public static final on_my_way:I = 0x7f0901ec

.field public static final onto:I = 0x7f0901ed

.field public static final operation_failed:I = 0x7f0901ee

.field public static final options_dash:I = 0x7f0901ef

.field public static final options_map:I = 0x7f0901f0

.field public static final options_none:I = 0x7f0901f1

.field public static final ota_update_installation_will_take:I = 0x7f0901f2

.field public static final other_navigation_issue:I = 0x7f0901f3

.field public static final parking:I = 0x7f0901f4

.field public static final pause_demo:I = 0x7f0901f5

.field public static final permanent_closure:I = 0x7f0901f6

.field public static final phone_call_duration:I = 0x7f0901f7

.field public static final phone_call_duration_min:I = 0x7f0901f8

.field public static final phone_connected:I = 0x7f0901f9

.field public static final phone_disconnected:I = 0x7f0901fa

.field public static final phone_ex_low_battery:I = 0x7f0901fb

.field public static final phone_low_battery:I = 0x7f0901fc

.field public static final phone_microphone:I = 0x7f0901fd

.field public static final phone_no_network:I = 0x7f0901fe

.field public static final phone_update_required:I = 0x7f0901ff

.field public static final pick_destination:I = 0x7f090200

.field public static final pick_reply:I = 0x7f090201

.field public static final pick_route:I = 0x7f090202

.field public static final place_search_results:I = 0x7f090203

.field public static final place_type_search_failed:I = 0x7f090204

.field public static final play_demo:I = 0x7f090205

.field public static final playlists:I = 0x7f090206

.field public static final please_pair_phone:I = 0x7f090207

.field public static final please_pair_phone_detail:I = 0x7f090208

.field public static final please_wait:I = 0x7f090209

.field public static final pm:I = 0x7f09020a

.field public static final pm_marker:I = 0x7f09020b

.field public static final podcasts:I = 0x7f09020c

.field public static final positioning_left:I = 0x7f09020d

.field public static final positioning_right:I = 0x7f09020e

.field public static final powering_off:I = 0x7f09020f

.field public static final preference_hud_settings:I = 0x7f090210

.field public static final preference_restart_on_crash:I = 0x7f090211

.field public static final preference_start_on_boot:I = 0x7f090212

.field public static final preference_start_video_on_boot:I = 0x7f090213

.field public static final preparing_to_install_update:I = 0x7f090214

.field public static final pressure_unit:I = 0x7f090339

.field public static final question_mark:I = 0x7f090215

.field public static final quick:I = 0x7f090216

.field public static final quick_menu:I = 0x7f090217

.field public static final quick_search:I = 0x7f090218

.field public static final quite_left:I = 0x7f090219

.field public static final quite_right:I = 0x7f09021a

.field public static final rate_limited:I = 0x7f090009

.field public static final read:I = 0x7f09021b

.field public static final recents:I = 0x7f09021c

.field public static final record:I = 0x7f09021d

.field public static final remaining_details:I = 0x7f09000a

.field public static final remaining_title:I = 0x7f09000b

.field public static final remaining_title_glympse:I = 0x7f09000c

.field public static final reply:I = 0x7f09021e

.field public static final reply_failed:I = 0x7f09021f

.field public static final reply_sent:I = 0x7f090220

.field public static final reply_text_msg_1:I = 0x7f090221

.field public static final reply_text_msg_2:I = 0x7f090222

.field public static final reply_text_msg_3:I = 0x7f090223

.field public static final reply_text_msg_4:I = 0x7f090224

.field public static final reply_text_msg_5:I = 0x7f090225

.field public static final reply_text_msg_6:I = 0x7f090226

.field public static final reply_with_a_glympse:I = 0x7f09000d

.field public static final report_navigation_issue:I = 0x7f090227

.field public static final request_was_received:I = 0x7f09000e

.field public static final reroute_event_dismiss:I = 0x7f090228

.field public static final reroute_event_go:I = 0x7f090229

.field public static final restart_demo:I = 0x7f09022a

.field public static final results:I = 0x7f09022b

.field public static final results_for:I = 0x7f09022c

.field public static final retry:I = 0x7f09022d

.field public static final retry_search:I = 0x7f09022e

.field public static final right:I = 0x7f09022f

.field public static final road_closed:I = 0x7f090230

.field public static final road_closed_message:I = 0x7f090231

.field public static final road_name:I = 0x7f090232

.field public static final roundabout_1:I = 0x7f090233

.field public static final roundabout_10:I = 0x7f090234

.field public static final roundabout_11:I = 0x7f090235

.field public static final roundabout_12:I = 0x7f090236

.field public static final roundabout_2:I = 0x7f090237

.field public static final roundabout_3:I = 0x7f090238

.field public static final roundabout_4:I = 0x7f090239

.field public static final roundabout_5:I = 0x7f09023a

.field public static final roundabout_6:I = 0x7f09023b

.field public static final roundabout_7:I = 0x7f09023c

.field public static final roundabout_8:I = 0x7f09023d

.field public static final roundabout_9:I = 0x7f09023e

.field public static final route_calc_failed:I = 0x7f09023f

.field public static final route_change_msg:I = 0x7f090240

.field public static final route_desc:I = 0x7f090241

.field public static final route_failed:I = 0x7f090242

.field public static final route_go:I = 0x7f090243

.field public static final route_inefficient:I = 0x7f090244

.field public static final route_num:I = 0x7f090245

.field public static final route_recalculating:I = 0x7f090246

.field public static final route_start_msg:I = 0x7f090247

.field public static final route_time_day:I = 0x7f090248

.field public static final route_time_hr:I = 0x7f090249

.field public static final route_time_min:I = 0x7f09024a

.field public static final route_time_min_short:I = 0x7f09024b

.field public static final route_title:I = 0x7f09024c

.field public static final routes:I = 0x7f09024d

.field public static final rpm:I = 0x7f09024e

.field public static final running_late:I = 0x7f09024f

.field public static final saving_recording:I = 0x7f090250

.field public static final say_something_like:I = 0x7f090251

.field public static final scroll_left_gauge:I = 0x7f090252

.field public static final scroll_right_gauge:I = 0x7f090253

.field public static final search_again:I = 0x7f090254

.field public static final search_error:I = 0x7f090255

.field public static final search_gas_station:I = 0x7f090256

.field public static final send_without_message:I = 0x7f090257

.field public static final sending_failed:I = 0x7f090258

.field public static final sensor_is_blocked:I = 0x7f090259

.field public static final sensor_is_blocked_message:I = 0x7f09025a

.field public static final share_location:I = 0x7f09025b

.field public static final share_trip:I = 0x7f09025c

.field public static final share_your_location_with:I = 0x7f09025d

.field public static final share_your_trip_with:I = 0x7f09025e

.field public static final shorter:I = 0x7f09025f

.field public static final shortest_route:I = 0x7f090260

.field public static final show_raw_gps:I = 0x7f09033a

.field public static final shuffle:I = 0x7f090261

.field public static final shuffle_off:I = 0x7f090262

.field public static final shuffle_on:I = 0x7f090263

.field public static final shuffle_play:I = 0x7f090264

.field public static final shutdown:I = 0x7f090265

.field public static final shutting_down:I = 0x7f090266

.field public static final si_auto_on:I = 0x7f09033b

.field public static final si_auto_trains:I = 0x7f09033c

.field public static final si_celsius:I = 0x7f09033d

.field public static final si_check_engine_light:I = 0x7f09033e

.field public static final si_compact:I = 0x7f09033f

.field public static final si_device_mileage:I = 0x7f090340

.field public static final si_dial_bt:I = 0x7f090341

.field public static final si_dial_fw:I = 0x7f090342

.field public static final si_disabled:I = 0x7f090343

.field public static final si_display:I = 0x7f090344

.field public static final si_display_bt:I = 0x7f090345

.field public static final si_enabled:I = 0x7f090346

.field public static final si_eng_temp:I = 0x7f090347

.field public static final si_engine_oil_pressure:I = 0x7f090348

.field public static final si_engine_trip_fuel:I = 0x7f090349

.field public static final si_engine_trouble_codes:I = 0x7f090267

.field public static final si_fahrenheit:I = 0x7f09034a

.field public static final si_fastest:I = 0x7f09034b

.field public static final si_ferries:I = 0x7f09034c

.field public static final si_fix:I = 0x7f09034d

.field public static final si_fuel:I = 0x7f09034e

.field public static final si_fuel_level:I = 0x7f09034f

.field public static final si_general_pref:I = 0x7f090350

.field public static final si_gestures:I = 0x7f090351

.field public static final si_gps:I = 0x7f090352

.field public static final si_gps_signal:I = 0x7f090353

.field public static final si_gps_speed:I = 0x7f090354

.field public static final si_here_map_version:I = 0x7f090355

.field public static final si_here_sdk_version:I = 0x7f090356

.field public static final si_highways:I = 0x7f090322

.field public static final si_maf:I = 0x7f090357

.field public static final si_make:I = 0x7f090358

.field public static final si_maps:I = 0x7f090359

.field public static final si_maps_data_verified:I = 0x7f09035a

.field public static final si_maps_voices_verified:I = 0x7f09035b

.field public static final si_model:I = 0x7f09035c

.field public static final si_nav_pref:I = 0x7f09035d

.field public static final si_no:I = 0x7f09035e

.field public static final si_no_data:I = 0x7f09035f

.field public static final si_normal:I = 0x7f090360

.field public static final si_obd_data:I = 0x7f090361

.field public static final si_obd_fw:I = 0x7f090362

.field public static final si_odometer:I = 0x7f090363

.field public static final si_off:I = 0x7f090364

.field public static final si_offline_maps:I = 0x7f090365

.field public static final si_on:I = 0x7f090366

.field public static final si_route_calc:I = 0x7f090367

.field public static final si_route_pref:I = 0x7f090368

.field public static final si_rpm:I = 0x7f090369

.field public static final si_satellites:I = 0x7f09036a

.field public static final si_seen:I = 0x7f09036b

.field public static final si_shortest:I = 0x7f09036c

.field public static final si_speed:I = 0x7f09036d

.field public static final si_stats:I = 0x7f09036e

.field public static final si_temperature:I = 0x7f09036f

.field public static final si_tollroads:I = 0x7f090370

.field public static final si_tunnels:I = 0x7f090371

.field public static final si_ui_scaling:I = 0x7f090372

.field public static final si_unpaved_roads:I = 0x7f090373

.field public static final si_updated:I = 0x7f090374

.field public static final si_vehicle:I = 0x7f090375

.field public static final si_versions:I = 0x7f090376

.field public static final si_volt:I = 0x7f090377

.field public static final si_voltage:I = 0x7f090378

.field public static final si_year:I = 0x7f090379

.field public static final si_yes:I = 0x7f09037a

.field public static final siri:I = 0x7f090268

.field public static final siri_active:I = 0x7f090269

.field public static final siri_starting:I = 0x7f09026a

.field public static final slack_team:I = 0x7f09026b

.field public static final slight_left:I = 0x7f09026c

.field public static final slight_right:I = 0x7f09026d

.field public static final snapshot_drive_score:I = 0x7f09026e

.field public static final snapshot_maps:I = 0x7f09026f

.field public static final snapshot_navigation:I = 0x7f090270

.field public static final snapshot_smart_dash:I = 0x7f090271

.field public static final software_update:I = 0x7f090272

.field public static final speed_limit:I = 0x7f090273

.field public static final speed_limit_unavailable:I = 0x7f090274

.field public static final speed_limit_with_unit:I = 0x7f090275

.field public static final speeding:I = 0x7f090276

.field public static final speedometer:I = 0x7f090277

.field public static final start_maneuver:I = 0x7f090278

.field public static final start_maneuver_no_next_road:I = 0x7f090279

.field public static final start_maneuver_turn:I = 0x7f09027a

.field public static final start_trip:I = 0x7f09027b

.field public static final starts:I = 0x7f09027c

.field public static final stay_on:I = 0x7f09027d

.field public static final suggested:I = 0x7f09027e

.field public static final suggested_dest_find_gas_desc:I = 0x7f09027f

.field public static final suggested_dest_find_gas_on_route_desc:I = 0x7f090280

.field public static final suggested_dest_find_gas_on_route_title:I = 0x7f090281

.field public static final suggested_dest_find_gas_title:I = 0x7f090282

.field public static final suggested_eta:I = 0x7f090283

.field public static final suggested_trip:I = 0x7f090284

.field public static final swipe_left:I = 0x7f090285

.field public static final swipe_right:I = 0x7f090286

.field public static final tachometer:I = 0x7f090287

.field public static final take_snapshot:I = 0x7f090288

.field public static final tbt_audio_destination_reached:I = 0x7f090289

.field public static final tbt_audio_destination_reached_pattern:I = 0x7f09028a

.field public static final temperature_title:I = 0x7f09028b

.field public static final temperature_unit_celsius:I = 0x7f09028c

.field public static final temperature_unit_fahrenheit:I = 0x7f09028d

.field public static final then:I = 0x7f09028e

.field public static final tips:I = 0x7f09028f

.field public static final title_app_update_required:I = 0x7f090290

.field public static final title_display_update_required:I = 0x7f090291

.field public static final to_contact_name:I = 0x7f090292

.field public static final today:I = 0x7f090293

.field public static final toward:I = 0x7f090294

.field public static final traffic_faster_route_available:I = 0x7f090295

.field public static final traffic_faster_route_tts:I = 0x7f090296

.field public static final traffic_faster_route_tts_eta_only:I = 0x7f090297

.field public static final traffic_jam_title:I = 0x7f090298

.field public static final traffic_no_updated:I = 0x7f090299

.field public static final traffic_notification_ahead:I = 0x7f09029a

.field public static final traffic_notification_delay:I = 0x7f09029b

.field public static final traffic_notification_delay_title:I = 0x7f09029c

.field public static final traffic_notification_dismiss:I = 0x7f09029d

.field public static final traffic_notification_save:I = 0x7f09029e

.field public static final traffic_notification_text_default:I = 0x7f09029f

.field public static final traffic_notification_text_incident:I = 0x7f0902a0

.field public static final traffic_notification_text_slowtraffic:I = 0x7f0902a1

.field public static final traffic_notification_text_stoppedtraffic:I = 0x7f0902a2

.field public static final traffic_reroute_faster_route:I = 0x7f0902a3

.field public static final traffic_reroute_go:I = 0x7f0902a4

.field public static final traffic_reroute_msg:I = 0x7f0902a5

.field public static final traffic_reroute_msg_eta_only:I = 0x7f0902a6

.field public static final traffic_reroute_via:I = 0x7f0902a7

.field public static final trip:I = 0x7f0902a8

.field public static final trip_maneuvers:I = 0x7f0902a9

.field public static final trip_sent:I = 0x7f0902aa

.field public static final tts_auto_zoon:I = 0x7f0902ab

.field public static final tts_calendar_location:I = 0x7f0902ac

.field public static final tts_calendar_time_at:I = 0x7f0902ad

.field public static final tts_calendar_time_min:I = 0x7f0902ae

.field public static final tts_calendar_time_mins:I = 0x7f0902af

.field public static final tts_calendar_title:I = 0x7f0902b0

.field public static final tts_dial_battery_extremely_low:I = 0x7f0902b1

.field public static final tts_dial_battery_low:I = 0x7f0902b2

.field public static final tts_dial_battery_very_low:I = 0x7f0902b3

.field public static final tts_dial_connected:I = 0x7f0902b4

.field public static final tts_dial_disconnected:I = 0x7f0902b5

.field public static final tts_dial_forgotten:I = 0x7f0902b6

.field public static final tts_fuel_gauge_added:I = 0x7f0902b7

.field public static final tts_fuel_range_gauge_added:I = 0x7f0902b8

.field public static final tts_heading_gauge_added:I = 0x7f0902b9

.field public static final tts_kms_per_hour:I = 0x7f0902ba

.field public static final tts_manual_zoom:I = 0x7f0902bb

.field public static final tts_meters_per_second:I = 0x7f0902bc

.field public static final tts_miles_per_hour:I = 0x7f0902bd

.field public static final tts_nav_stopped:I = 0x7f0902be

.field public static final tts_phone_battery_low:I = 0x7f0902bf

.field public static final tts_phone_battery_very_low:I = 0x7f0902c0

.field public static final tts_recalculate_route:I = 0x7f0902c1

.field public static final tts_recalculate_route_failed:I = 0x7f0902c2

.field public static final tts_rpm_gauge_added:I = 0x7f0902c3

.field public static final tts_speed_warning:I = 0x7f0902c4

.field public static final tts_tbt_disabled:I = 0x7f0902c5

.field public static final tts_tbt_enabled:I = 0x7f0902c6

.field public static final tts_traffic_disabled:I = 0x7f0902c7

.field public static final tts_traffic_enabled:I = 0x7f0902c8

.field public static final twilio_account_sid:I = 0x7f09037b

.field public static final twilio_auth_token:I = 0x7f09037c

.field public static final twilio_msg_service_id:I = 0x7f09037d

.field public static final unable_to_resume:I = 0x7f0902c9

.field public static final unavailable:I = 0x7f0902ca

.field public static final undefined_turn:I = 0x7f0902cb

.field public static final unit_feet:I = 0x7f0902cc

.field public static final unit_feet_ext:I = 0x7f0902cd

.field public static final unit_feet_ext_singular:I = 0x7f0902ce

.field public static final unit_kilometers:I = 0x7f0902cf

.field public static final unit_kilometers_ext:I = 0x7f0902d0

.field public static final unit_kilometers_ext_singular:I = 0x7f0902d1

.field public static final unit_meters:I = 0x7f0902d2

.field public static final unit_meters_ext:I = 0x7f0902d3

.field public static final unit_meters_ext_singular:I = 0x7f0902d4

.field public static final unit_miles:I = 0x7f0902d5

.field public static final unit_miles_ext:I = 0x7f0902d6

.field public static final unit_miles_ext_singular:I = 0x7f0902d7

.field public static final unknown_address:I = 0x7f0902d8

.field public static final unknown_device:I = 0x7f0902d9

.field public static final unknown_error:I = 0x7f0902da

.field public static final unmute_tbt_2:I = 0x7f0902db

.field public static final update_and_reconnect:I = 0x7f0902dc

.field public static final update_available:I = 0x7f0902dd

.field public static final update_before_shutdown:I = 0x7f0902de

.field public static final update_installation_will_begin_in:I = 0x7f0902df

.field public static final update_navdy_will_upgrade_from_to:I = 0x7f0902e0

.field public static final update_ready_to_install:I = 0x7f0902e1

.field public static final updating_language:I = 0x7f0902e2

.field public static final upload_failure:I = 0x7f0902e3

.field public static final upload_failure_detail:I = 0x7f0902e4

.field public static final uploading:I = 0x7f0902e5

.field public static final uploading_detail:I = 0x7f0902e6

.field public static final uploading_success:I = 0x7f0902e7

.field public static final uturn:I = 0x7f0902e8

.field public static final via_desc:I = 0x7f0902e9

.field public static final view:I = 0x7f0902ea

.field public static final viewed_your_location:I = 0x7f0902eb

.field public static final viewed_your_trip:I = 0x7f0902ec

.field public static final voice_assist_disabled:I = 0x7f0902ed

.field public static final voice_assist_not_available:I = 0x7f0902ee

.field public static final voice_search:I = 0x7f0902ef

.field public static final voice_search_ambient_noise:I = 0x7f0902f0

.field public static final voice_search_failed:I = 0x7f0902f1

.field public static final voice_search_failed_to_detect:I = 0x7f0902f2

.field public static final voice_search_listening:I = 0x7f0902f3

.field public static final voice_search_more_results:I = 0x7f0902f4

.field public static final voice_search_need_permission:I = 0x7f0902f5

.field public static final voice_search_no_results_found:I = 0x7f0902f6

.field public static final voice_search_offline:I = 0x7f0902f7

.field public static final voice_search_processing:I = 0x7f0902f8

.field public static final voice_search_searching:I = 0x7f0902f9

.field public static final voice_search_speak_now:I = 0x7f0902fa

.field public static final voice_search_starting:I = 0x7f0902fb

.field public static final voice_search_wait:I = 0x7f0902fc

.field public static final warning:I = 0x7f0902fd

.field public static final welcome_add_driver:I = 0x7f0902fe

.field public static final welcome_bluetooth_disabled:I = 0x7f0902ff

.field public static final welcome_cancel:I = 0x7f090300

.field public static final welcome_cant_connect:I = 0x7f090301

.field public static final welcome_connecting:I = 0x7f090302

.field public static final welcome_current_driver:I = 0x7f090303

.field public static final welcome_download_app:I = 0x7f090304

.field public static final welcome_load_profile:I = 0x7f090305

.field public static final welcome_looking_for_drivers:I = 0x7f090306

.field public static final welcome_no_drivers_found:I = 0x7f090307

.field public static final welcome_welcome:I = 0x7f090308

.field public static final welcome_welcome_back:I = 0x7f090309

.field public static final welcome_welcome_driver:I = 0x7f09030a

.field public static final welcome_who_is_driving:I = 0x7f09030b

.field public static final which_number:I = 0x7f09030c

.field public static final widget_calendar:I = 0x7f09030d

.field public static final widget_clock_analog:I = 0x7f09030e

.field public static final widget_clock_digital:I = 0x7f09030f

.field public static final widget_clock_time_and_date:I = 0x7f090310

.field public static final widget_compass:I = 0x7f090311

.field public static final widget_drive_score:I = 0x7f090312

.field public static final widget_empty:I = 0x7f090313

.field public static final widget_engine_temperature:I = 0x7f090314

.field public static final widget_fuel:I = 0x7f090315

.field public static final widget_g_meter:I = 0x7f090316

.field public static final widget_mpg:I = 0x7f090317

.field public static final widget_music:I = 0x7f090318

.field public static final widget_speed_limit:I = 0x7f090319

.field public static final widget_traffic_incident:I = 0x7f09031a

.field public static final widget_trip_eta:I = 0x7f09031b

.field public static final widget_weather:I = 0x7f09031c

.field public static final work:I = 0x7f09031d

.field public static final wrong_direction:I = 0x7f09031e

.field public static final yesterday:I = 0x7f09031f

.field public static final your_fuel_level_is_low_tts:I = 0x7f090320

.field public static final zero:I = 0x7f090321


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
