.class public final Lcom/navdy/hud/app/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final analog_clock_frame_color:I = 0x7f0d0000

.field public static final apple_calendar:I = 0x7f0d0001

.field public static final apple_mail:I = 0x7f0d0002

.field public static final audio_feedback:I = 0x7f0d0003

.field public static final black_half_transparent:I = 0x7f0d0004

.field public static final black_overlay:I = 0x7f0d0005

.field public static final call_dial:I = 0x7f0d0006

.field public static final call_end:I = 0x7f0d0007

.field public static final call_mute:I = 0x7f0d0008

.field public static final call_unmute:I = 0x7f0d0009

.field public static final choice_deselection:I = 0x7f0d000a

.field public static final choice_highlight:I = 0x7f0d000b

.field public static final choice_selection:I = 0x7f0d000c

.field public static final close_halo:I = 0x7f0d000d

.field public static final compass_frame_color:I = 0x7f0d000e

.field public static final cyan:I = 0x7f0d000f

.field public static final delete_all:I = 0x7f0d0010

.field public static final download_app_1:I = 0x7f0d0011

.field public static final download_app_2:I = 0x7f0d0012

.field public static final driving_score_warning_color:I = 0x7f0d0013

.field public static final eco_main_gauge_bk:I = 0x7f0d0014

.field public static final engine_temperature_high_temperature_color:I = 0x7f0d0015

.field public static final facebook:I = 0x7f0d0016

.field public static final facebook_messenger:I = 0x7f0d0017

.field public static final faster_route:I = 0x7f0d0018

.field public static final flucating_anim_color:I = 0x7f0d0019

.field public static final fuel_gauge_frame_color:I = 0x7f0d001a

.field public static final fuel_gauge_ver_low_warning_color:I = 0x7f0d001b

.field public static final fuel_level:I = 0x7f0d001c

.field public static final generic:I = 0x7f0d001d

.field public static final gforce_text:I = 0x7f0d001e

.field public static final glance_add:I = 0x7f0d001f

.field public static final glance_back:I = 0x7f0d0020

.field public static final glance_dismiss:I = 0x7f0d0021

.field public static final glance_more:I = 0x7f0d0022

.field public static final glance_msg:I = 0x7f0d0023

.field public static final glance_ok_blue:I = 0x7f0d0024

.field public static final glance_ok_go:I = 0x7f0d0025

.field public static final glance_read:I = 0x7f0d0026

.field public static final glance_reply:I = 0x7f0d0027

.field public static final glance_retry:I = 0x7f0d0028

.field public static final google_calendar:I = 0x7f0d0029

.field public static final google_hangout:I = 0x7f0d002a

.field public static final google_inbox:I = 0x7f0d002b

.field public static final google_mail:I = 0x7f0d002c

.field public static final grey:I = 0x7f0d002d

.field public static final grey_4a:I = 0x7f0d002e

.field public static final grey_ababab:I = 0x7f0d002f

.field public static final grey_c6:I = 0x7f0d0030

.field public static final grey_fluctuator:I = 0x7f0d0031

.field public static final hockeyapp_background_header:I = 0x7f0d0032

.field public static final hockeyapp_background_light:I = 0x7f0d0033

.field public static final hockeyapp_background_white:I = 0x7f0d0034

.field public static final hockeyapp_button_background:I = 0x7f0d0035

.field public static final hockeyapp_button_background_pressed:I = 0x7f0d0036

.field public static final hockeyapp_button_background_selected:I = 0x7f0d0037

.field public static final hockeyapp_text_black:I = 0x7f0d0038

.field public static final hockeyapp_text_light:I = 0x7f0d0039

.field public static final hockeyapp_text_normal:I = 0x7f0d003a

.field public static final hockeyapp_text_white:I = 0x7f0d003b

.field public static final hud_cyan:I = 0x7f0d003c

.field public static final hud_white:I = 0x7f0d003d

.field public static final icon_bk_color_unselected:I = 0x7f0d003e

.field public static final icon_user_bg_0:I = 0x7f0d003f

.field public static final icon_user_bg_1:I = 0x7f0d0040

.field public static final icon_user_bg_2:I = 0x7f0d0041

.field public static final icon_user_bg_3:I = 0x7f0d0042

.field public static final icon_user_bg_4:I = 0x7f0d0043

.field public static final icon_user_bg_5:I = 0x7f0d0044

.field public static final icon_user_bg_6:I = 0x7f0d0045

.field public static final icon_user_bg_7:I = 0x7f0d0046

.field public static final icon_user_bg_8:I = 0x7f0d0047

.field public static final imessage:I = 0x7f0d0048

.field public static final indicator_color:I = 0x7f0d0049

.field public static final led_blue:I = 0x7f0d004a

.field public static final led_dial_pairing_color:I = 0x7f0d004b

.field public static final led_error_color:I = 0x7f0d004c

.field public static final led_gesture_color:I = 0x7f0d004d

.field public static final led_gesture_not_recognized_color:I = 0x7f0d004e

.field public static final led_red:I = 0x7f0d004f

.field public static final led_usb_power_color:I = 0x7f0d0050

.field public static final light_gray:I = 0x7f0d0051

.field public static final main_gauge_bk:I = 0x7f0d0052

.field public static final mm_active_trip:I = 0x7f0d0053

.field public static final mm_back:I = 0x7f0d0054

.field public static final mm_connnect_phone:I = 0x7f0d0055

.field public static final mm_contacts:I = 0x7f0d0056

.field public static final mm_dash:I = 0x7f0d0057

.field public static final mm_dash_options:I = 0x7f0d0058

.field public static final mm_glances:I = 0x7f0d0059

.field public static final mm_icon_gradient_end:I = 0x7f0d005a

.field public static final mm_icon_gradient_start:I = 0x7f0d005b

.field public static final mm_map:I = 0x7f0d005c

.field public static final mm_map_options:I = 0x7f0d005d

.field public static final mm_music:I = 0x7f0d005e

.field public static final mm_now_playing:I = 0x7f0d005f

.field public static final mm_options_auto_zoom:I = 0x7f0d0060

.field public static final mm_options_demo:I = 0x7f0d0061

.field public static final mm_options_end_trip:I = 0x7f0d0062

.field public static final mm_options_manual_zoom:I = 0x7f0d0063

.field public static final mm_options_mute_tbt_audio:I = 0x7f0d0064

.field public static final mm_options_report_issue:I = 0x7f0d0065

.field public static final mm_options_scroll_gauge:I = 0x7f0d0066

.field public static final mm_options_side_gauges_halo:I = 0x7f0d0067

.field public static final mm_options_speedometer:I = 0x7f0d0068

.field public static final mm_options_tachometer:I = 0x7f0d0069

.field public static final mm_options_unmute_tbt_audio:I = 0x7f0d006a

.field public static final mm_place_contact:I = 0x7f0d006b

.field public static final mm_place_favorite:I = 0x7f0d006c

.field public static final mm_place_gas:I = 0x7f0d006d

.field public static final mm_place_home:I = 0x7f0d006e

.field public static final mm_place_suggested:I = 0x7f0d006f

.field public static final mm_place_work:I = 0x7f0d0070

.field public static final mm_places:I = 0x7f0d0071

.field public static final mm_recent_places:I = 0x7f0d0072

.field public static final mm_search:I = 0x7f0d0073

.field public static final mm_search_atm:I = 0x7f0d0074

.field public static final mm_search_coffee:I = 0x7f0d0075

.field public static final mm_search_food:I = 0x7f0d0076

.field public static final mm_search_gas:I = 0x7f0d0077

.field public static final mm_search_grocery_store:I = 0x7f0d0078

.field public static final mm_search_hospital:I = 0x7f0d0079

.field public static final mm_search_parking:I = 0x7f0d007a

.field public static final mm_search_parks:I = 0x7f0d007b

.field public static final mm_settings:I = 0x7f0d007c

.field public static final mm_settings_brightness:I = 0x7f0d007d

.field public static final mm_settings_connect_phone:I = 0x7f0d007e

.field public static final mm_settings_factory_reset:I = 0x7f0d007f

.field public static final mm_settings_learn_gestures:I = 0x7f0d0080

.field public static final mm_settings_shutdown:I = 0x7f0d0081

.field public static final mm_settings_sys_info:I = 0x7f0d0082

.field public static final mm_settings_update_dial:I = 0x7f0d0083

.field public static final mm_settings_update_display:I = 0x7f0d0084

.field public static final mm_voice_assist:I = 0x7f0d0085

.field public static final mm_voice_assist_gnow:I = 0x7f0d0086

.field public static final mm_voice_assist_siri:I = 0x7f0d0087

.field public static final mm_voice_search:I = 0x7f0d0088

.field public static final music_android_local:I = 0x7f0d0089

.field public static final music_apple_music:I = 0x7f0d008a

.field public static final music_apple_podcasts:I = 0x7f0d008b

.field public static final music_details_sel:I = 0x7f0d008c

.field public static final music_now_playing:I = 0x7f0d008d

.field public static final options_dash_green:I = 0x7f0d008e

.field public static final options_dash_purple:I = 0x7f0d008f

.field public static final options_nav_light_blue:I = 0x7f0d0090

.field public static final options_nav_light_cyan:I = 0x7f0d0091

.field public static final options_nav_orange:I = 0x7f0d0092

.field public static final options_nav_red:I = 0x7f0d0093

.field public static final overlay_body:I = 0x7f0d0094

.field public static final overlay_title:I = 0x7f0d0095

.field public static final phone_call:I = 0x7f0d0096

.field public static final pivot_indicator_color:I = 0x7f0d0097

.field public static final place_type_search_active_color:I = 0x7f0d0098

.field public static final place_type_search_secondary_color:I = 0x7f0d0099

.field public static final progress_indicator_color:I = 0x7f0d009a

.field public static final recalc_anim_color:I = 0x7f0d009b

.field public static final route_calc_failure:I = 0x7f0d009c

.field public static final route_sel:I = 0x7f0d009d

.field public static final route_unsel:I = 0x7f0d009e

.field public static final scroll_option_color:I = 0x7f0d009f

.field public static final share_location:I = 0x7f0d00a0

.field public static final share_location_trip_alert_color:I = 0x7f0d00a1

.field public static final share_location_trip_color:I = 0x7f0d00a2

.field public static final slack:I = 0x7f0d00a3

.field public static final small_analog_clock_frame_color:I = 0x7f0d00a4

.field public static final sms:I = 0x7f0d00a5

.field public static final speed_very_fast_warning_color:I = 0x7f0d00a6

.field public static final speedometer_frame_color:I = 0x7f0d00a7

.field public static final speedometer_option_color:I = 0x7f0d00a8

.field public static final start_dest_fill_color:I = 0x7f0d00a9

.field public static final start_dest_stroke_color:I = 0x7f0d00aa

.field public static final suggested_dest_cal:I = 0x7f0d00ab

.field public static final suggested_dest_contact:I = 0x7f0d00ac

.field public static final suggested_dest_fav:I = 0x7f0d00ad

.field public static final suggested_dest_home:I = 0x7f0d00ae

.field public static final suggested_dest_recent:I = 0x7f0d00af

.field public static final suggested_dest_work:I = 0x7f0d00b0

.field public static final tachometer_color:I = 0x7f0d00b1

.field public static final tachometer_max_marker_color:I = 0x7f0d00b2

.field public static final tachometer_option_color:I = 0x7f0d00b3

.field public static final tachometer_warning_color:I = 0x7f0d00b4

.field public static final thumb_color:I = 0x7f0d00b5

.field public static final traffic_bad:I = 0x7f0d00b6

.field public static final traffic_delay:I = 0x7f0d00b7

.field public static final traffic_good:I = 0x7f0d00b8

.field public static final traffic_notification_neg:I = 0x7f0d00b9

.field public static final traffic_notification_pos:I = 0x7f0d00ba

.field public static final traffic_reroute:I = 0x7f0d00bb

.field public static final twitter:I = 0x7f0d00bc

.field public static final vlist_subtitle:I = 0x7f0d00bd

.field public static final voice_search_active_color:I = 0x7f0d00be

.field public static final warning_color:I = 0x7f0d00bf

.field public static final whatsapp:I = 0x7f0d00c0

.field public static final white_half_transparent:I = 0x7f0d00c1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
