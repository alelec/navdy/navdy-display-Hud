.class public Lcom/navdy/hud/app/framework/calendar/CalendarManager;
.super Ljava/lang/Object;
.source "CalendarManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;
    }
.end annotation


# static fields
.field private static final CALENDAR_EVENTS_LIST_INITIAL_SIZE:I = 0x14

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public mBus:Lcom/squareup/otto/Bus;

.field private mCalendarEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/calendars/CalendarEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mLastCalendarEventUpdate:Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;

.field private profileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/calendar/CalendarManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mBus:Lcom/squareup/otto/Bus;

    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mCalendarEvents:Ljava/util/List;

    .line 46
    return-void
.end method


# virtual methods
.method public getCalendarEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/calendars/CalendarEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mCalendarEvents:Ljava/util/List;

    return-object v0
.end method

.method public onCalendarEventsUpdate(Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;)V
    .locals 4
    .param p1, "calendarEventUpdates"    # Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 50
    sget-object v1, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received Calendar updates :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 51
    iput-object p1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mLastCalendarEventUpdate:Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;

    .line 52
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 53
    .local v0, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->profileName:Ljava/lang/String;

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mCalendarEvents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 57
    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;->calendar_events:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 58
    sget-object v1, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calendar events count :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;->calendar_events:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mCalendarEvents:Ljava/util/List;

    iget-object v2, p1, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;->calendar_events:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mBus:Lcom/squareup/otto/Bus;

    sget-object v2, Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;->UPDATED:Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 3
    .param p1, "driverProfileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 66
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 67
    .local v0, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mCalendarEvents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 70
    iget-object v1, p0, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->mBus:Lcom/squareup/otto/Bus;

    sget-object v2, Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;->UPDATED:Lcom/navdy/hud/app/framework/calendar/CalendarManager$CalendarManagerEvent;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/calendar/CalendarManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Default profile loaded, not clearing the calendar events"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
