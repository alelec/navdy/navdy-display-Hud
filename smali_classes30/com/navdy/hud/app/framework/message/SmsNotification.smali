.class public Lcom/navdy/hud/app/framework/message/SmsNotification;
.super Ljava/lang/Object;
.source "SmsNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;
.implements Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;
    }
.end annotation


# static fields
.field private static final NOTIFICATION_TIMEOUT:I

.field private static final TIMEOUT:I

.field private static final choicesDismiss:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final choicesRetryAndDismiss:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final handler:Landroid/os/Handler;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0153
    .end annotation
.end field

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private final message:Ljava/lang/String;

.field private final mode:Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;

.field private final name:Ljava/lang/String;

.field private final notifId:Ljava/lang/String;

.field notificationUserImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e014f
    .end annotation
.end field

.field private final number:Ljava/lang/String;

.field private final rawNumber:Ljava/lang/String;

.field private retrySendingMessage:Z

.field private roundTransformation:Lcom/squareup/picasso/Transformation;

.field sideImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0151
    .end annotation
.end field

.field subtitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e014c
    .end annotation
.end field

.field title:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00c3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const v14, 0x7f0e000b

    const v2, 0x7f02012c

    const v8, 0x7f020125

    .line 47
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/message/SmsNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0xa

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->TIMEOUT:I

    .line 50
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x5

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->NOTIFICATION_TIMEOUT:I

    .line 52
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->resources:Landroid/content/res/Resources;

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->handler:Landroid/os/Handler;

    .line 64
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 65
    .local v12, "dismiss":Ljava/lang/String;
    const/high16 v5, -0x1000000

    .line 66
    .local v5, "unselectedColor":I
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 67
    .local v9, "dismissColor":I
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 68
    .local v3, "retryColor":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choicesRetryAndDismiss:Ljava/util/List;

    .line 69
    sget-object v10, Lcom/navdy/hud/app/framework/message/SmsNotification;->choicesRetryAndDismiss:Ljava/util/List;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v1, 0x7f0e006f

    sget-object v4, Lcom/navdy/hud/app/framework/message/SmsNotification;->resources:Landroid/content/res/Resources;

    const v6, 0x7f09022d

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move v4, v2

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choicesRetryAndDismiss:Ljava/util/List;

    new-instance v6, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    move v7, v14

    move v10, v8

    move v11, v5

    move v13, v9

    invoke-direct/range {v6 .. v13}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choicesDismiss:Ljava/util/List;

    .line 72
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choicesDismiss:Ljava/util/List;

    new-instance v6, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    move v7, v14

    move v10, v8

    move v11, v5

    move v13, v9

    invoke-direct/range {v6 .. v13}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "number"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Lcom/makeramen/RoundedTransformationBuilder;

    invoke-direct {v0}, Lcom/makeramen/RoundedTransformationBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/makeramen/RoundedTransformationBuilder;->oval(Z)Lcom/makeramen/RoundedTransformationBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/makeramen/RoundedTransformationBuilder;->build()Lcom/squareup/picasso/Transformation;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 104
    iput-object p1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->mode:Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "navdy#sms#notif#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->notifId:Ljava/lang/String;

    .line 106
    invoke-static {p3}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;

    .line 107
    invoke-static {p3}, Lcom/navdy/hud/app/util/PhoneUtil;->convertToE164Format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->rawNumber:Ljava/lang/String;

    .line 108
    iput-object p4, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->message:Ljava/lang/String;

    .line 109
    iput-object p5, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;

    .line 110
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/message/SmsNotification;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->rawNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/message/SmsNotification;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->message:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/message/SmsNotification;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/message/SmsNotification;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/message/SmsNotification;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->notifId:Ljava/lang/String;

    return-object v0
.end method

.method private getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;

    .line 318
    :goto_0
    return-object v0

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;

    goto :goto_0

    .line 318
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private setUI()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->subtitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->notificationUserImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    .line 139
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification$2;->$SwitchMap$com$navdy$hud$app$framework$message$SmsNotification$Mode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->mode:Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 154
    :goto_1
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->subtitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->subtitle:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 141
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->title:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/framework/message/SmsNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09021f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->subtitle:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/navdy/hud/app/framework/message/SmsNotification;->getContactName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->sideImage:Landroid/widget/ImageView;

    const v1, 0x7f020190

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 144
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/message/SmsNotification;->choicesRetryAndDismiss:Ljava/util/List;

    invoke-virtual {v0, v1, v2, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    goto :goto_1

    .line 148
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->title:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/framework/message/SmsNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090220

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->subtitle:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/navdy/hud/app/framework/message/SmsNotification;->getContactName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->sideImage:Landroid/widget/ImageView;

    const v1, 0x7f020191

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/message/SmsNotification;->choicesDismiss:Ljava/util/List;

    invoke-virtual {v0, v1, v2, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    goto :goto_1

    .line 139
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x1

    return v0
.end method

.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 2
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 259
    iget v0, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 269
    :goto_0
    return-void

    .line 261
    :sswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->retrySendingMessage:Z

    .line 262
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->notifId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :sswitch_1
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->notifId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 259
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e000b -> :sswitch_1
        0x7f0e006f -> :sswitch_0
    .end sparse-switch
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 164
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->notifId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeout()I
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification$2;->$SwitchMap$com$navdy$hud$app$framework$message$SmsNotification$Mode:[I

    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->mode:Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/message/SmsNotification$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 209
    sget v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->NOTIFICATION_TIMEOUT:I

    :goto_0
    return v0

    .line 206
    :pswitch_0
    sget v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->TIMEOUT:I

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->SMS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030040

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 124
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 125
    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 244
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method public isContextValid()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 272
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 237
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 276
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 281
    iget-object v2, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_0

    .line 299
    :goto_0
    return v0

    .line 284
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/message/SmsNotification$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 286
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    .line 287
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    move v0, v1

    .line 288
    goto :goto_0

    .line 291
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    move v0, v1

    .line 293
    goto :goto_0

    .line 296
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    move v0, v1

    .line 297
    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 234
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 0
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 175
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/message/SmsNotification;->setUI()V

    .line 176
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 184
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->retrySendingMessage:Z

    if-eqz v0, :cond_0

    .line 186
    sget-object v0, Lcom/navdy/hud/app/framework/message/SmsNotification;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/message/SmsNotification$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/message/SmsNotification$1;-><init>(Lcom/navdy/hud/app/framework/message/SmsNotification;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 200
    :cond_0
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 160
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return v0
.end method
