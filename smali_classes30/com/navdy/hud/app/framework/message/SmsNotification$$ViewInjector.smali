.class public Lcom/navdy/hud/app/framework/message/SmsNotification$$ViewInjector;
.super Ljava/lang/Object;
.source "SmsNotification$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/framework/message/SmsNotification;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/framework/message/SmsNotification;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00c3

    const-string v2, "field \'title\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/message/SmsNotification;->title:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e014c

    const-string v2, "field \'subtitle\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/message/SmsNotification;->subtitle:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e0153

    const-string v2, "field \'choiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/message/SmsNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 16
    const v1, 0x7f0e014f

    const-string v2, "field \'notificationUserImage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/message/SmsNotification;->notificationUserImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 18
    const v1, 0x7f0e0151

    const-string v2, "field \'sideImage\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/message/SmsNotification;->sideImage:Landroid/widget/ImageView;

    .line 20
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/framework/message/SmsNotification;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/framework/message/SmsNotification;

    .prologue
    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->title:Landroid/widget/TextView;

    .line 24
    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->subtitle:Landroid/widget/TextView;

    .line 25
    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 26
    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->notificationUserImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 27
    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/SmsNotification;->sideImage:Landroid/widget/ImageView;

    .line 28
    return-void
.end method
