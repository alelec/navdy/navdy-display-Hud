.class Lcom/navdy/hud/app/framework/message/SmsNotification$1;
.super Ljava/lang/Object;
.source "SmsNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/message/SmsNotification;->onStop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/message/SmsNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/message/SmsNotification;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 189
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->getInstance()Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    move-result-object v0

    .line 190
    .local v0, "glanceHandler":Lcom/navdy/hud/app/framework/glance/GlanceHandler;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->rawNumber:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$000(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->message:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$100(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$200(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "retry message not sent"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$400(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->message:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$100(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$200(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsFailedNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_0
    return-void

    .line 194
    :cond_0
    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "retry message sent"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 195
    iget-object v1, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->notifId:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$500(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->number:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$400(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->message:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$100(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/message/SmsNotification$1;->this$0:Lcom/navdy/hud/app/framework/message/SmsNotification;

    # getter for: Lcom/navdy/hud/app/framework/message/SmsNotification;->name:Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/message/SmsNotification;->access$200(Lcom/navdy/hud/app/framework/message/SmsNotification;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsSuccessNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
