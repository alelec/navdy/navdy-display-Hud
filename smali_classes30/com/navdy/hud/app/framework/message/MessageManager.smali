.class public Lcom/navdy/hud/app/framework/message/MessageManager;
.super Ljava/lang/Object;
.source "MessageManager.java"


# static fields
.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final sInstance:Lcom/navdy/hud/app/framework/message/MessageManager;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/message/MessageManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/message/MessageManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 34
    new-instance v0, Lcom/navdy/hud/app/framework/message/MessageManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/message/MessageManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/message/MessageManager;->sInstance:Lcom/navdy/hud/app/framework/message/MessageManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/message/MessageManager;->bus:Lcom/squareup/otto/Bus;

    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/framework/message/MessageManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/message/MessageManager;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/hud/app/framework/message/MessageManager;->sInstance:Lcom/navdy/hud/app/framework/message/MessageManager;

    return-object v0
.end method


# virtual methods
.method public onMessage(Lcom/navdy/service/library/events/notification/NotificationEvent;)V
    .locals 22
    .param p1, "event"    # Lcom/navdy/service/library/events/notification/NotificationEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 50
    sget-object v3, Lcom/navdy/hud/app/framework/message/MessageManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "old notification --> convert to glance"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 52
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 53
    sget-object v3, Lcom/navdy/hud/app/framework/message/MessageManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignoring notification with null sourceIdentifier"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 122
    :goto_0
    return-void

    .line 57
    :cond_0
    const/4 v2, 0x0

    .line 58
    .local v2, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    sget-object v4, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SOCIAL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    if-ne v3, v4, :cond_1

    .line 60
    new-instance v2, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .end local v2    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    const/4 v3, 0x0

    sget-object v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->MESSAGE:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    sget-object v6, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    sget-object v8, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    const/4 v9, -0x1

    const-wide/16 v10, 0x0

    invoke-direct/range {v2 .. v11}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;Ljava/util/Date;Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;IJ)V

    .line 69
    .restart local v2    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->handleNewCall(Lcom/navdy/hud/app/framework/recentcall/RecentCall;)Z

    .line 73
    :cond_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 74
    .local v16, "from":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->message:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 75
    .local v18, "message":Ljava/lang/String;
    const/16 v20, 0x0

    .line 76
    .local v20, "number":Ljava/lang/String;
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->cannotReplyBack:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v14

    .line 77
    .local v14, "cannotReplyBack":Z
    const/16 v21, 0x0

    .line 79
    .local v21, "validNumber":Z
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 80
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 81
    const/16 v21, 0x1

    .line 89
    :cond_2
    :goto_2
    if-nez v21, :cond_3

    .line 90
    const/4 v14, 0x1

    .line 93
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v17

    .line 94
    .local v17, "id":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v15, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    const/4 v12, 0x0

    .line 96
    .local v12, "action":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-direct {v3, v4, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    if-eqz v20, :cond_4

    .line 99
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v3, v4, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    if-nez v14, :cond_4

    .line 101
    new-instance v12, Ljava/util/ArrayList;

    .end local v12    # "action":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    const/4 v3, 0x1

    invoke-direct {v12, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 102
    .restart local v12    # "action":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    sget-object v3, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_IS_SMS:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-direct {v3, v4, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_4
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v3, v4, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    new-instance v3, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 110
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    const-string v4, "com.navdy.sms"

    .line 111
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 112
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 114
    invoke-virtual {v3, v15}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v13

    .line 116
    .local v13, "builder":Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    if-eqz v12, :cond_5

    .line 117
    invoke-virtual {v13, v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    .line 120
    :cond_5
    invoke-virtual {v13}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v19

    .line 121
    .local v19, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/framework/message/MessageManager;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 73
    .end local v12    # "action":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    .end local v13    # "builder":Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .end local v14    # "cannotReplyBack":Z
    .end local v15    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v16    # "from":Ljava/lang/String;
    .end local v17    # "id":Ljava/lang/String;
    .end local v18    # "message":Ljava/lang/String;
    .end local v19    # "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v20    # "number":Ljava/lang/String;
    .end local v21    # "validNumber":Z
    :cond_6
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    move-object/from16 v16, v0

    goto/16 :goto_1

    .line 83
    .restart local v14    # "cannotReplyBack":Z
    .restart local v16    # "from":Ljava/lang/String;
    .restart local v18    # "message":Ljava/lang/String;
    .restart local v20    # "number":Ljava/lang/String;
    .restart local v21    # "validNumber":Z
    :cond_7
    if-eqz v2, :cond_2

    iget-object v3, v2, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 84
    iget-object v0, v2, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 85
    const/16 v21, 0x1

    goto/16 :goto_2
.end method
