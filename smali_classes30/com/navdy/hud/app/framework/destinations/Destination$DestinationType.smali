.class public final enum Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;
.super Ljava/lang/Enum;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/destinations/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DestinationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

.field public static final enum DEFAULT:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

.field public static final enum FIND_GAS:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->DEFAULT:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .line 46
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    const-string v1, "FIND_GAS"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->FIND_GAS:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->DEFAULT:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->FIND_GAS:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->$VALUES:[Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->$VALUES:[Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    return-object v0
.end method
