.class Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;
.super Ljava/lang/Object;
.source "DestinationsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->onDestinationSuggestion(Lcom/navdy/service/library/events/places/SuggestedDestination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

.field final synthetic val$suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/places/SuggestedDestination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->val$suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 206
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDestinationSuggestion:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->val$suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 208
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    .line 209
    .local v1, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_0

    .line 210
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onDestinationSuggestion: Map engine not initialized, cannot process suggestion, at this time"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 212
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->val$suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    # setter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$402(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/places/SuggestedDestination;)Lcom/navdy/service/library/events/places/SuggestedDestination;

    .line 247
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 217
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onDestinationSuggestion: nav mode is on"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v2

    .line 222
    .local v2, "homescreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v2, :cond_2

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->isUIShowingRouteCalculation()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 223
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onDestinationSuggestion: route calc is on"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 227
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->val$suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    # setter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$402(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/places/SuggestedDestination;)Lcom/navdy/service/library/events/places/SuggestedDestination;

    .line 230
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSavedRouteData(Lcom/navdy/service/library/log/Logger;)Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;

    move-result-object v3

    .line 231
    .local v3, "savedRouteData":Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    iget-object v4, v3, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v4, :cond_3

    .line 234
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onDestinationSuggestion: has saved route data"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :cond_3
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 239
    .local v0, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_4

    .line 242
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onDestinationSuggestion: no current position"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :cond_4
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;->this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->launchSuggestedDestination()Z

    goto :goto_0
.end method
