.class public Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
.super Ljava/lang/Object;
.source "DestinationsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;,
        Lcom/navdy/hud/app/framework/destinations/DestinationsManager$SuggestedDestinationsChanged;,
        Lcom/navdy/hud/app/framework/destinations/DestinationsManager$RecentDestinationsChanged;,
        Lcom/navdy/hud/app/framework/destinations/DestinationsManager$FavoriteDestinationsChanged;
    }
.end annotation


# static fields
.field private static final FAVORITE_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$FavoriteDestinationsChanged;

.field public static final FAVORITE_DESTINATIONS_FILENAME:Ljava/lang/String; = "favoriteDestinations.pb"

.field private static final INITIALS_ARRAY:[C

.field public static final MAX_DESTINATIONS:I = 0x1e

.field private static final MAX_INITIALS:I = 0x4

.field private static final RECENT_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$RecentDestinationsChanged;

.field private static final SUGGESTED_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$SuggestedDestinationsChanged;

.field private static final sInstance:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private volatile favoriteDestinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;"
        }
    .end annotation
.end field

.field private lastUpdate:Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

.field private volatile recentDestinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;"
        }
    .end annotation
.end field

.field resources:Landroid/content/res/Resources;

.field private suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

.field private volatile suggestedDestinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 65
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$FavoriteDestinationsChanged;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$FavoriteDestinationsChanged;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->FAVORITE_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$FavoriteDestinationsChanged;

    .line 66
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$RecentDestinationsChanged;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$RecentDestinationsChanged;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->RECENT_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$RecentDestinationsChanged;

    .line 67
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$SuggestedDestinationsChanged;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$SuggestedDestinationsChanged;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->SUGGESTED_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$SuggestedDestinationsChanged;

    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [C

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->INITIALS_ARRAY:[C

    .line 72
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sInstance:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->resources:Landroid/content/res/Resources;

    .line 92
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->requestDestinations()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->parseDestinations(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;)V

    return-void
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->parseDestinations(Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V

    return-void
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/places/SuggestedDestination;)Lcom/navdy/service/library/events/places/SuggestedDestination;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/places/SuggestedDestination;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->load()V

    return-void
.end method

.method private buildDestinations()V
    .locals 3

    .prologue
    .line 264
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$5;-><init>(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 274
    :goto_0
    return-void

    .line 272
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->load()V

    goto :goto_0
.end method

.method private clearDestinations()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 288
    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->lastUpdate:Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    .line 289
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->setFavoriteDestinations(Ljava/util/List;)V

    .line 290
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->setRecentDestinations(Ljava/util/List;)V

    .line 291
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->setSuggestedDestinations(Ljava/util/List;)V

    .line 292
    return-void
.end method

.method private static getDestinationInitials(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x4

    const/4 v8, 0x0

    .line 650
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    move-object v3, v8

    .line 705
    :cond_0
    :goto_0
    return-object v3

    .line 654
    :cond_1
    invoke-static {p0}, Lcom/navdy/hud/app/util/GenericUtil;->removePunctuation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 655
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 656
    const/4 v3, 0x0

    .line 657
    .local v3, "firstWord":Ljava/lang/String;
    const/4 v2, 0x0

    .line 659
    .local v2, "firstNumber":Z
    const-string v9, " "

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 660
    .local v4, "index":I
    if-ltz v4, :cond_2

    .line 661
    const/4 v9, 0x0

    invoke-virtual {p0, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 665
    :cond_2
    if-eqz v3, :cond_4

    move-object v9, v3

    :goto_1
    :try_start_1
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 666
    const/4 v2, 0x1

    .line 667
    if-nez v3, :cond_3

    .line 668
    move-object v3, p0

    .line 674
    :cond_3
    :goto_2
    if-eqz v2, :cond_5

    .line 675
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v12, :cond_0

    move-object v3, v8

    .line 676
    goto :goto_0

    :cond_4
    move-object v9, p0

    .line 665
    goto :goto_1

    .line 683
    :cond_5
    if-nez v3, :cond_6

    .line 685
    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {p0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 687
    :cond_6
    sget-object v10, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->INITIALS_ARRAY:[C

    monitor-enter v10
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 688
    :try_start_3
    new-instance v7, Ljava/util/StringTokenizer;

    const-string v9, " "

    invoke-direct {v7, p0, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    .local v7, "tokenizer":Ljava/util/StringTokenizer;
    const/4 v0, 0x0

    .local v0, "count":I
    move v1, v0

    .line 690
    .end local v0    # "count":I
    .local v1, "count":I
    :goto_3
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 691
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 692
    .local v6, "token":Ljava/lang/String;
    sget-object v9, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->INITIALS_ARRAY:[C

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    aput-char v11, v9, v1

    .line 693
    if-ne v0, v12, :cond_7

    .line 694
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 695
    monitor-exit v10

    move-object v3, v8

    goto :goto_0

    :cond_7
    move v1, v0

    .line 698
    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_3

    .line 700
    .end local v6    # "token":Ljava/lang/String;
    :cond_8
    new-instance v3, Ljava/lang/String;

    .end local v3    # "firstWord":Ljava/lang/String;
    sget-object v9, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->INITIALS_ARRAY:[C

    const/4 v11, 0x0

    invoke-direct {v3, v9, v11, v1}, Ljava/lang/String;-><init>([CII)V

    monitor-exit v10

    goto :goto_0

    .line 701
    .end local v1    # "count":I
    .end local v7    # "tokenizer":Ljava/util/StringTokenizer;
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v9
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 703
    .end local v2    # "firstNumber":Z
    .end local v4    # "index":I
    :catch_0
    move-exception v5

    .line 704
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v9, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    move-object v3, v8

    .line 705
    goto/16 :goto_0

    .line 670
    .end local v5    # "t":Ljava/lang/Throwable;
    .restart local v2    # "firstNumber":Z
    .restart local v3    # "firstWord":Ljava/lang/String;
    .restart local v4    # "index":I
    :catch_1
    move-exception v9

    goto :goto_2
.end method

.method private getDestinationType(Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .locals 2
    .param p1, "type"    # Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .prologue
    .line 581
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$6;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 593
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    :goto_0
    return-object v0

    .line 583
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto :goto_0

    .line 586
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto :goto_0

    .line 589
    :pswitch_3
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto :goto_0

    .line 581
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getInitials(Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "favoriteDestinationType"    # Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .prologue
    .line 631
    if-eqz p1, :cond_0

    .line 632
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$6;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 642
    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 635
    :pswitch_1
    invoke-static {p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getDestinationInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 638
    :pswitch_2
    invoke-static {p0}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 632
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sInstance:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    return-object v0
.end method

.method public static getNavigationRouteRequestForDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 9
    .param p0, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    .line 850
    const/4 v1, 0x0

    .line 852
    .local v1, "displayCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v4, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v4, v4, v6

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v4, v4, v6

    if-nez v4, :cond_2

    .line 854
    :cond_0
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 855
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 856
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 857
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 870
    .local v2, "navCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    :goto_0
    new-instance v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;-><init>()V

    .line 871
    invoke-virtual {v4, v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    .line 872
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    .line 873
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->streetAddress(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    .line 874
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination_identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 875
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    const/4 v5, 0x0

    .line 876
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->geoCodeStreetAddress(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 877
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->autoNavigate(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 878
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v0

    .line 880
    .local v0, "builder":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    if-eqz v1, :cond_1

    .line 881
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationDisplay(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    .line 884
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v3

    .line 885
    .local v3, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    return-object v3

    .line 859
    .end local v0    # "builder":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .end local v2    # "navCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v3    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_2
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 860
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 861
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 862
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 864
    .restart local v2    # "navCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 865
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 866
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 867
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    goto :goto_0
.end method

.method private getPlaceCategory(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;
    .locals 4
    .param p1, "d"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 710
    const/4 v1, 0x0

    .line 711
    .local v1, "suggested":Z
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 712
    const/4 v1, 0x1

    .line 715
    :cond_0
    const/4 v0, 0x0

    .line 716
    .local v0, "recent":Z
    iget-object v2, p1, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    sget-object v3, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_RECENT:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-ne v2, v3, :cond_1

    .line 717
    const/4 v0, 0x1

    .line 720
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 721
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->SUGGESTED_RECENT:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    .line 728
    :goto_0
    return-object v2

    .line 722
    :cond_2
    if-eqz v1, :cond_3

    .line 723
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->SUGGESTED:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    goto :goto_0

    .line 724
    :cond_3
    if-eqz v0, :cond_4

    .line 725
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->RECENT:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    goto :goto_0

    .line 728
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getRecentTimeLabel(Lcom/navdy/service/library/events/destination/Destination;)Ljava/lang/String;
    .locals 6
    .param p1, "d"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    const/4 v0, 0x0

    .line 732
    iget-object v1, p1, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    sget-object v2, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_RECENT:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-ne v1, v2, :cond_0

    .line 733
    iget-object v1, p1, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 734
    new-instance v0, Ljava/util/Date;

    iget-object v1, p1, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lcom/navdy/hud/app/util/DateUtil;->getDateLabel(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 739
    :cond_0
    return-object v0
.end method

.method private getSuggestionLabelColor(Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;)I
    .locals 4
    .param p1, "d"    # Lcom/navdy/service/library/events/destination/Destination;
    .param p2, "favoriteDestinationType"    # Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    .param p3, "placeCategory"    # Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    .prologue
    const v3, 0x7f0d00b0

    .line 746
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 747
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 748
    .local v0, "resources":Landroid/content/res/Resources;
    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->SUGGESTED_RECENT:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    if-ne p3, v1, :cond_0

    .line 749
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 768
    .end local v0    # "resources":Landroid/content/res/Resources;
    :goto_0
    return v1

    .line 751
    .restart local v0    # "resources":Landroid/content/res/Resources;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$6;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType:[I

    invoke-virtual {p2}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 765
    const v1, 0x7f0d00ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 753
    :pswitch_0
    const v1, 0x7f0d00ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 756
    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 759
    :pswitch_2
    const v1, 0x7f0d00ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 762
    :pswitch_3
    const v1, 0x7f0d00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 768
    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 751
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private handleDefaultDestination(Lcom/navdy/hud/app/framework/destinations/Destination;Z)V
    .locals 10
    .param p1, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;
    .param p2, "navCoordinateLookup"    # Z

    .prologue
    const-wide/16 v8, 0x0

    .line 510
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleDefaultDestination ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/destinations/Destination;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] lookup="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 511
    if-eqz p2, :cond_0

    .line 512
    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->supportsNavigationCoordinateLookup()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 513
    iget-wide v4, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    cmpl-double v4, v4, v8

    if-eqz v4, :cond_2

    iget-wide v4, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    cmpl-double v4, v4, v8

    if-eqz v4, :cond_2

    .line 515
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleDefaultDestination nav coordinate exists already"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 528
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 530
    .local v1, "displayCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    iget-wide v4, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    cmpl-double v4, v4, v8

    if-nez v4, :cond_4

    iget-wide v4, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    cmpl-double v4, v4, v8

    if-nez v4, :cond_4

    .line 532
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-wide v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    .line 533
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    iget-wide v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    .line 534
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 535
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 548
    .local v2, "navCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    :goto_1
    new-instance v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;-><init>()V

    .line 549
    invoke-virtual {v4, v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    .line 550
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    .line 551
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->streetAddress(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->identifier:Ljava/lang/String;

    .line 552
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination_identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 553
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transformToProtoDestination(Lcom/navdy/hud/app/framework/destinations/Destination;)Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    const/4 v5, 0x1

    .line 554
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    const/4 v5, 0x0

    .line 555
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->geoCodeStreetAddress(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 556
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getDestinationType(Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationType(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 557
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v0

    .line 559
    .local v0, "builder":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    if-eqz v1, :cond_1

    .line 560
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationDisplay(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    .line 563
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v3

    .line 565
    .local v3, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "launched navigation route request"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 566
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v5, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 567
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v4, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 568
    .end local v0    # "builder":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .end local v1    # "displayCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v2    # "navCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v3    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :goto_2
    return-void

    .line 518
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleDefaultDestination do nav lookup"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 519
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->startNavigationLookup(Lcom/navdy/hud/app/framework/destinations/Destination;)V

    goto :goto_2

    .line 523
    :cond_3
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleDefaultDestination client does not support nav lookup"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 537
    .restart local v1    # "displayCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_4
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-wide v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    .line 538
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    iget-wide v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    .line 539
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 540
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 542
    .restart local v2    # "navCoordinate":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-wide v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    .line 543
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    iget-wide v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    .line 544
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 545
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    goto/16 :goto_1
.end method

.method private handleFindGasDestination(Lcom/navdy/hud/app/framework/destinations/Destination;)V
    .locals 3
    .param p1, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;

    .prologue
    .line 572
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 575
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v0

    .line 576
    .local v0, "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->showFindingGasStationToast()V

    .line 577
    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->findGasStations(IZ)V

    .line 578
    return-void
.end method

.method private load()V
    .locals 5

    .prologue
    .line 278
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getPreferencesDirectory()Ljava/io/File;

    move-result-object v1

    .line 279
    .local v1, "prefDirectory":Ljava/io/File;
    new-instance v0, Lcom/navdy/service/library/events/MessageStore;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/MessageStore;-><init>(Ljava/io/File;)V

    .line 280
    .local v0, "messageStore":Lcom/navdy/service/library/events/MessageStore;
    const-string v3, "favoriteDestinations.pb"

    const-class v4, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    invoke-virtual {v0, v3, v4}, Lcom/navdy/service/library/events/MessageStore;->readMessage(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->parseDestinations(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    .end local v0    # "messageStore":Lcom/navdy/service/library/events/MessageStore;
    .end local v1    # "prefDirectory":Ljava/io/File;
    :goto_0
    return-void

    .line 281
    :catch_0
    move-exception v2

    .line 282
    .local v2, "t":Ljava/lang/Throwable;
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->setFavoriteDestinations(Ljava/util/List;)V

    .line 283
    sget-object v3, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private parseDestinations(Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V
    .locals 6
    .param p1, "response"    # Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    .prologue
    .line 311
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "parsing RecommendedDestinationsUpdate"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 312
    iget-object v4, p1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->destinations:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 313
    iget-object v4, p1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->destinations:Ljava/util/List;

    invoke-direct {p0, v4}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transform(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 315
    .local v1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 316
    .local v2, "recentDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 318
    .local v3, "suggestedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 319
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/destinations/Destination;

    .line 320
    .local v0, "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->recommendation:Z

    if-eqz v5, :cond_0

    .line 322
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 325
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 330
    .end local v0    # "destination":Lcom/navdy/hud/app/framework/destinations/Destination;
    :cond_1
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->setRecentDestinations(Ljava/util/List;)V

    .line 331
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->setSuggestedDestinations(Ljava/util/List;)V

    .line 335
    .end local v1    # "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    .end local v2    # "recentDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    .end local v3    # "suggestedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    :goto_1
    return-void

    .line 333
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "rec-destination list returned is null"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private parseDestinations(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;)V
    .locals 5
    .param p1, "response"    # Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    .prologue
    .line 295
    sget-object v3, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "parsing FavoriteDestinationsUpdate"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 296
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->lastUpdate:Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    .line 297
    const/4 v0, 0x0

    .line 298
    .local v0, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 299
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/profile/DriverProfile;->getPreferencesDirectory()Ljava/io/File;

    move-result-object v2

    .line 300
    .local v2, "prefDirectory":Ljava/io/File;
    new-instance v1, Lcom/navdy/service/library/events/MessageStore;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/MessageStore;-><init>(Ljava/io/File;)V

    .line 301
    .local v1, "messageStore":Lcom/navdy/service/library/events/MessageStore;
    const-string v3, "favoriteDestinations.pb"

    invoke-virtual {v1, p1, v3}, Lcom/navdy/service/library/events/MessageStore;->writeMessage(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 303
    iget-object v3, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transform(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 307
    .end local v1    # "messageStore":Lcom/navdy/service/library/events/MessageStore;
    .end local v2    # "prefDirectory":Ljava/io/File;
    :goto_0
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->setFavoriteDestinations(Ljava/util/List;)V

    .line 308
    return-void

    .line 305
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "fav-destination list returned is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestDestinations()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 252
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 254
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->lastUpdate:Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->lastUpdate:Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    iget-object v4, v4, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 256
    .local v0, "favoritesVersion":J
    :goto_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v6, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;-><init>(Ljava/lang/Long;)V

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 257
    iget-object v4, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v6, Lcom/navdy/service/library/events/destination/RecommendedDestinationsRequest;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v6, v2}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsRequest;-><init>(Ljava/lang/Long;)V

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 259
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sent favorite destinations request with current version="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 260
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "sent recommended destinations request with current version"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 261
    return-void

    .end local v0    # "favoritesVersion":J
    :cond_0
    move-wide v0, v2

    .line 254
    goto :goto_0
.end method

.method private setFavoriteDestinations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "favoriteDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->favoriteDestinations:Ljava/util/List;

    if-eq p1, v0, :cond_0

    .line 103
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->favoriteDestinations:Ljava/util/List;

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->FAVORITE_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$FavoriteDestinationsChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 106
    :cond_0
    return-void
.end method

.method private transform(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    .local p1, "protoDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 340
    .local v1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/destination/Destination;

    .line 341
    .local v0, "d":Lcom/navdy/service/library/events/destination/Destination;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transformToInternalDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 342
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x1e

    if-ne v3, v4, :cond_0

    .line 343
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "exceeded max size"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 347
    .end local v0    # "d":Lcom/navdy/service/library/events/destination/Destination;
    :cond_1
    return-object v1
.end method


# virtual methods
.method public clearSuggestedDestination()V
    .locals 2

    .prologue
    .line 784
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clearSuggestedDestination"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 785
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->dismissSuggestionToast()V

    .line 786
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    .line 787
    return-void
.end method

.method public getFavoriteDestinations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->favoriteDestinations:Ljava/util/List;

    return-object v0
.end method

.method public getGasDestination()Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 790
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getFeatureUtil()Lcom/navdy/hud/app/util/FeatureUtil;

    move-result-object v7

    sget-object v8, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v7, v8}, Lcom/navdy/hud/app/util/FeatureUtil;->isFeatureEnabled(Lcom/navdy/hud/app/util/FeatureUtil$Feature;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isAvailable()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 801
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    move-result-object v1

    .line 802
    .local v1, "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isBusy()Z

    move-result v5

    .line 803
    .local v5, "isBusy":Z
    if-eqz v5, :cond_1

    .line 804
    sget-object v7, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "fuel route manager is busy"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 843
    .end local v1    # "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .end local v5    # "isBusy":Z
    :cond_0
    :goto_0
    return-object v3

    .line 807
    .restart local v1    # "fuelRoutingManager":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .restart local v5    # "isBusy":Z
    :cond_1
    sget-object v7, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "fuel route manager is not busy"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 810
    const/4 v4, 0x0

    .line 811
    .local v4, "hasFuelPid":Z
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v6

    .line 812
    .local v6, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v6}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_2

    .line 813
    const/4 v4, 0x1

    .line 816
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/Destination;->getGasDestination()Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v2

    .line 820
    .local v2, "gasDestination":Lcom/navdy/hud/app/framework/destinations/Destination;
    if-nez v4, :cond_3

    .line 822
    const/4 v0, 0x0

    .line 832
    .local v0, "addFirst":Z
    :goto_1
    new-instance v3, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;

    invoke-direct {v3}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;-><init>()V

    .line 833
    .local v3, "gasDestinationContainer":Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;
    iput-object v2, v3, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;->destination:Lcom/navdy/hud/app/framework/destinations/Destination;

    .line 834
    if-eqz v0, :cond_5

    .line 836
    const/4 v7, 0x1

    iput-boolean v7, v3, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;->showFirst:Z

    goto :goto_0

    .line 825
    .end local v0    # "addFirst":Z
    .end local v3    # "gasDestinationContainer":Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;
    :cond_3
    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getFuelGlanceDismissTime()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-lez v7, :cond_4

    .line 826
    const/4 v0, 0x1

    .restart local v0    # "addFirst":Z
    goto :goto_1

    .line 828
    .end local v0    # "addFirst":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "addFirst":Z
    goto :goto_1

    .line 839
    .restart local v3    # "gasDestinationContainer":Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;
    :cond_5
    const/4 v7, 0x0

    iput-boolean v7, v3, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$GasDestination;->showFirst:Z

    goto :goto_0
.end method

.method public getRecentDestinations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->recentDestinations:Ljava/util/List;

    return-object v0
.end method

.method public getSuggestedDestinations()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestinations:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestinations:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 114
    :cond_0
    return-object v0
.end method

.method public goToSuggestedDestination()V
    .locals 4

    .prologue
    .line 598
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    if-nez v2, :cond_0

    .line 599
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "gotToSuggestedDestination : Suggested destination is null, cannot to navigate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 618
    :goto_0
    return-void

    .line 603
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    iget-object v0, v2, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 605
    .local v0, "destination":Lcom/navdy/service/library/events/destination/Destination;
    if-nez v0, :cond_1

    .line 606
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "gotToSuggestedDestination : Destination in Suggested destination is null, cannot to navigate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 610
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 611
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "gotToSuggestedDestination: Cannot navigate as the user is already navigating"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 614
    :cond_2
    invoke-static {v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getNavigationRouteRequestForDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v1

    .line 615
    .local v1, "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "launched navigation route request"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 616
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v3, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 617
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v2, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public launchSuggestedDestination()Z
    .locals 2

    .prologue
    .line 773
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    if-eqz v0, :cond_0

    .line 774
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "launchSuggestedDestination: suggested destination toast"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 775
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestination:Lcom/navdy/service/library/events/places/SuggestedDestination;

    invoke-static {v0}, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->showSuggestion(Lcom/navdy/service/library/events/places/SuggestedDestination;)V

    .line 776
    const/4 v0, 0x1

    .line 779
    :goto_0
    return v0

    .line 778
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "launchSuggestedDestination: no suggested destination available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 779
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConnectionStatusChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 147
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->clearDestinations()V

    .line 150
    :cond_0
    return-void
.end method

.method public onDestinationSuggestion(Lcom/navdy/service/library/events/places/SuggestedDestination;)V
    .locals 3
    .param p1, "suggestedDestination"    # Lcom/navdy/service/library/events/places/SuggestedDestination;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 203
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$4;-><init>(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/places/SuggestedDestination;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 249
    return-void
.end method

.method public onDeviceSyncRequired(Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 135
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "syncDestinations"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 136
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$1;-><init>(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 142
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->buildDestinations()V

    .line 156
    return-void
.end method

.method public onFavoriteDestinationsUpdate(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 160
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received FavoriteDestinationsUpdate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 161
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$2;-><init>(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 177
    return-void
.end method

.method public onRecommendedDestinationsUpdate(Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 182
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received RecommendedDestinationsUpdate:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 183
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;-><init>(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 199
    return-void
.end method

.method public requestNavigation(Lcom/navdy/hud/app/framework/destinations/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;

    .prologue
    .line 494
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$6;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$DestinationType:[I

    iget-object v1, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 503
    :goto_0
    return-void

    .line 496
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->handleDefaultDestination(Lcom/navdy/hud/app/framework/destinations/Destination;Z)V

    goto :goto_0

    .line 500
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->handleFindGasDestination(Lcom/navdy/hud/app/framework/destinations/Destination;)V

    goto :goto_0

    .line 494
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public requestNavigationWithNavLookup(Lcom/navdy/hud/app/framework/destinations/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/hud/app/framework/destinations/Destination;

    .prologue
    .line 506
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->handleDefaultDestination(Lcom/navdy/hud/app/framework/destinations/Destination;Z)V

    .line 507
    return-void
.end method

.method public setRecentDestinations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "recentDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->recentDestinations:Ljava/util/List;

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->RECENT_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$RecentDestinationsChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 130
    return-void
.end method

.method public setSuggestedDestinations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/destinations/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "suggestedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/destinations/Destination;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->suggestedDestinations:Ljava/util/List;

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->SUGGESTED_DESTINATIONS_CHANGED:Lcom/navdy/hud/app/framework/destinations/DestinationsManager$SuggestedDestinationsChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 120
    return-void
.end method

.method public transformToInternalDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination;
    .locals 32
    .param p1, "d"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 351
    if-nez p1, :cond_0

    .line 352
    const/4 v5, 0x0

    .line 400
    :goto_0
    return-object v5

    .line 355
    :cond_0
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    invoke-virtual {v4, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v24

    .line 356
    .local v24, "suggested":Z
    const/16 v23, -0x1

    .line 357
    .local v23, "color":I
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-eqz v4, :cond_3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    sget-object v6, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-ne v4, v6, :cond_3

    .line 358
    const/16 v24, 0x1

    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->resources:Landroid/content/res/Resources;

    const v6, 0x7f0d00ab

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v23

    .line 360
    sget-object v18, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_CALENDAR:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 365
    .local v18, "favoriteDestinationType":Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    :goto_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInitials(Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Ljava/lang/String;

    move-result-object v20

    .line 366
    .local v20, "initials":Ljava/lang/String;
    const/16 v22, 0x0

    .line 367
    .local v22, "recentTimeLabel":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getPlaceCategory(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    move-result-object v21

    .line 368
    .local v21, "placeCategory":Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;
    sget-object v4, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->RECENT:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    move-object/from16 v0, v21

    if-ne v0, v4, :cond_1

    .line 369
    invoke-direct/range {p0 .. p1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getRecentTimeLabel(Lcom/navdy/service/library/events/destination/Destination;)Ljava/lang/String;

    move-result-object v22

    .line 372
    :cond_1
    if-eqz v24, :cond_2

    .line 373
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getSuggestionLabelColor(Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;)I

    move-result v23

    .line 376
    :cond_2
    new-instance v5, Lcom/navdy/hud/app/framework/destinations/Destination;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v4, :cond_4

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 377
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    :goto_2
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v4, :cond_5

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 378
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    :goto_3
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v4, :cond_6

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 379
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    :goto_4
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v4, :cond_7

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    .line 380
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    :goto_5
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    move-object/from16 v17, v0

    sget-object v19, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->DEFAULT:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    if-eqz v4, :cond_8

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    .line 392
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v25

    :goto_6
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    if-eqz v4, :cond_9

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    .line 393
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v26

    :goto_7
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    move-object/from16 v28, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationDistance:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    .line 397
    invoke-static {v4}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->fromPhoneNumbers(Ljava/util/List;)Ljava/util/List;

    move-result-object v30

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    .line 398
    invoke-static {v4}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->fromContacts(Ljava/util/List;)Ljava/util/List;

    move-result-object v31

    invoke-direct/range {v5 .. v31}, Lcom/navdy/hud/app/framework/destinations/Destination;-><init>(DDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;Ljava/lang/String;IZIILjava/lang/String;Lcom/navdy/service/library/events/places/PlaceType;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 400
    .local v5, "dest":Lcom/navdy/hud/app/framework/destinations/Destination;
    goto/16 :goto_0

    .line 362
    .end local v5    # "dest":Lcom/navdy/hud/app/framework/destinations/Destination;
    .end local v18    # "favoriteDestinationType":Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    .end local v20    # "initials":Ljava/lang/String;
    .end local v21    # "placeCategory":Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;
    .end local v22    # "recentTimeLabel":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->getValue()I

    move-result v4

    invoke-static {v4}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->buildFromValue(I)Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    move-result-object v18

    .restart local v18    # "favoriteDestinationType":Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    goto/16 :goto_1

    .line 377
    .restart local v20    # "initials":Ljava/lang/String;
    .restart local v21    # "placeCategory":Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;
    .restart local v22    # "recentTimeLabel":Ljava/lang/String;
    :cond_4
    const-wide/16 v6, 0x0

    goto/16 :goto_2

    .line 378
    :cond_5
    const-wide/16 v8, 0x0

    goto/16 :goto_3

    .line 379
    :cond_6
    const-wide/16 v10, 0x0

    goto/16 :goto_4

    .line 380
    :cond_7
    const-wide/16 v12, 0x0

    goto :goto_5

    .line 392
    :cond_8
    const/16 v25, 0x0

    goto :goto_6

    .line 393
    :cond_9
    const/16 v26, 0x0

    goto :goto_7
.end method

.method public transformToProtoDestination(Lcom/navdy/hud/app/framework/destinations/Destination;)Lcom/navdy/service/library/events/destination/Destination;
    .locals 10
    .param p1, "d"    # Lcom/navdy/hud/app/framework/destinations/Destination;

    .prologue
    const/4 v7, 0x1

    .line 404
    const/4 v2, 0x0

    .line 405
    .local v2, "isRecommended":Ljava/lang/Boolean;
    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 406
    .local v1, "favoriteType":Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    const/4 v4, 0x0

    .line 408
    .local v4, "suggestionType":Lcom/navdy/service/library/events/destination/Destination$SuggestionType;
    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    if-eqz v5, :cond_0

    .line 409
    sget-object v5, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$6;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType:[I

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 415
    sget-object v5, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$6;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType:[I

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 442
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    if-eqz v5, :cond_1

    .line 443
    sget-object v5, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$6;->$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$PlaceCategory:[I

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_2

    .line 457
    :cond_1
    :goto_1
    sget-object v3, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    .line 459
    .local v3, "placeType":Lcom/navdy/service/library/events/places/PlaceType;
    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    if-eqz v5, :cond_2

    .line 460
    iget-object v3, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 463
    :cond_2
    new-instance v5, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    new-instance v6, Lcom/navdy/service/library/events/location/LatLong;

    iget-wide v8, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    .line 464
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iget-wide v8, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    new-instance v6, Lcom/navdy/service/library/events/location/LatLong;

    iget-wide v8, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    .line 465
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iget-wide v8, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    .line 466
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    .line 467
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationSubtitle:Ljava/lang/String;

    .line 468
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->identifier:Ljava/lang/String;

    .line 469
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    .line 470
    invoke-virtual {v5, v2}, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    .line 471
    invoke-virtual {v5, v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_type(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    .line 472
    invoke-virtual {v5, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationPlaceId:Ljava/lang/String;

    .line 473
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_id(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    .line 474
    invoke-virtual {v5, v1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->contacts:Ljava/util/List;

    .line 475
    invoke-static {v6}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->toContacts(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->contacts(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->phoneNumbers:Ljava/util/List;

    .line 476
    invoke-static {v6}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->toPhoneNumbers(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->phoneNumbers(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v0

    .line 478
    .local v0, "builder":Lcom/navdy/service/library/events/destination/Destination$Builder;
    iget v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIcon:I

    if-eqz v5, :cond_3

    .line 479
    iget v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIcon:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIcon(Ljava/lang/Integer;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    .line 482
    :cond_3
    iget v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIconBkColor:I

    if-eqz v5, :cond_4

    .line 483
    iget v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIconBkColor:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIconBkColor(Ljava/lang/Integer;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    .line 486
    :cond_4
    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->distanceStr:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 487
    iget-object v5, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->distanceStr:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationDistance(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    .line 490
    :cond_5
    invoke-virtual {v0}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v5

    return-object v5

    .line 411
    .end local v0    # "builder":Lcom/navdy/service/library/events/destination/Destination$Builder;
    .end local v3    # "placeType":Lcom/navdy/service/library/events/places/PlaceType;
    :pswitch_1
    sget-object v4, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 412
    goto/16 :goto_0

    .line 420
    :pswitch_2
    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 421
    goto/16 :goto_0

    .line 424
    :pswitch_3
    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 425
    goto/16 :goto_0

    .line 428
    :pswitch_4
    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CONTACT:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 429
    goto/16 :goto_0

    .line 435
    :pswitch_5
    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto/16 :goto_0

    .line 445
    :pswitch_6
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 446
    goto/16 :goto_1

    .line 449
    :pswitch_7
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 450
    sget-object v4, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_RECENT:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 451
    goto/16 :goto_1

    .line 409
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
    .end packed-switch

    .line 415
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 443
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
