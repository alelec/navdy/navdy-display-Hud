.class final Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;
.super Ljava/lang/Object;
.source "DestinationSuggestionToast.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/toast/IToastCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->showSuggestion(Lcom/navdy/service/library/events/places/SuggestedDestination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field toastView:Lcom/navdy/hud/app/view/ToastView;

.field final synthetic val$destination:Lcom/navdy/service/library/events/destination/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;->val$destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeChoiceItem(II)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 136
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    if-nez v2, :cond_0

    .line 158
    :goto_0
    return-void

    .line 140
    :cond_0
    const/4 v0, 0x0

    .line 141
    .local v0, "accepted":Z
    packed-switch p2, :pswitch_data_0

    .line 154
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;->val$destination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v2, v2, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    sget-object v3, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_2
    invoke-static {v0, v2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordDestinationSuggestion(ZZ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v1

    .line 156
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Error while recording the destination suggestion"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 143
    .end local v1    # "t":Ljava/lang/Throwable;
    :pswitch_0
    const/4 v0, 0x1

    .line 144
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/ToastView;->dismissToast()V

    .line 145
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->goToSuggestedDestination()V

    goto :goto_1

    .line 149
    :pswitch_1
    const/4 v0, 0x0

    .line 150
    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v2}, Lcom/navdy/hud/app/view/ToastView;->dismissToast()V

    goto :goto_1

    .line 154
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 141
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public onStart(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 4
    .param p1, "view"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 119
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getConfirmation()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    .line 120
    .local v0, "layout":Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c0021

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 121
    iget-object v1, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 122
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 127
    return-void
.end method
