.class public Lcom/navdy/hud/app/framework/destinations/Destination;
.super Ljava/lang/Object;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/destinations/Destination$Builder;,
        Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;,
        Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;,
        Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    }
.end annotation


# instance fields
.field public final contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public final destinationIcon:I

.field public final destinationIconBkColor:I

.field public final destinationPlaceId:Ljava/lang/String;

.field public final destinationSubtitle:Ljava/lang/String;

.field public final destinationTitle:Ljava/lang/String;

.field public final destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

.field public final displayPositionLatitude:D

.field public final displayPositionLongitude:D

.field public final distanceStr:Ljava/lang/String;

.field public final favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field public final fullAddress:Ljava/lang/String;

.field public final identifier:Ljava/lang/String;

.field public final initials:Ljava/lang/String;

.field public isInitialNumber:Z

.field public final navigationPositionLatitude:D

.field public final navigationPositionLongitude:D

.field public needShortTitleFont:Z

.field public final phoneNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public final placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

.field public final placeType:Lcom/navdy/service/library/events/places/PlaceType;

.field public final recentTimeLabel:Ljava/lang/String;

.field public final recentTimeLabelColor:I

.field public final recommendation:Z


# direct methods
.method public constructor <init>(DDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;Ljava/lang/String;IZIILjava/lang/String;Lcom/navdy/service/library/events/places/PlaceType;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .param p1, "navigationPositionLatitude"    # D
    .param p3, "navigationPositionLongitude"    # D
    .param p5, "displayPositionLatitude"    # D
    .param p7, "displayPositionLongitude"    # D
    .param p9, "fullAddress"    # Ljava/lang/String;
    .param p10, "destinationTitle"    # Ljava/lang/String;
    .param p11, "destinationSubtitle"    # Ljava/lang/String;
    .param p12, "identifier"    # Ljava/lang/String;
    .param p13, "favoriteDestinationType"    # Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    .param p14, "destinationType"    # Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;
    .param p15, "initials"    # Ljava/lang/String;
    .param p16, "placeCategory"    # Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;
    .param p17, "recentTimeLabel"    # Ljava/lang/String;
    .param p18, "recentTimeLabelColor"    # I
    .param p19, "recommendation"    # Z
    .param p20, "destinationIcon"    # I
    .param p21, "destinationIconBkColor"    # I
    .param p22, "destinationPlaceId"    # Ljava/lang/String;
    .param p23, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;
    .param p24, "distanceStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDDD",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;",
            "Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;",
            "Ljava/lang/String;",
            "IZII",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/places/PlaceType;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 293
    .local p25, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .local p26, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    .line 295
    iput-wide p3, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    .line 296
    iput-wide p5, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    .line 297
    iput-wide p7, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    .line 298
    iput-object p9, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    .line 299
    iput-object p10, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    .line 300
    iput-object p11, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationSubtitle:Ljava/lang/String;

    .line 301
    iput-object p12, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->identifier:Ljava/lang/String;

    .line 302
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 303
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .line 304
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->initials:Ljava/lang/String;

    .line 305
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    .line 306
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->recentTimeLabel:Ljava/lang/String;

    .line 307
    move/from16 v0, p18

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->recentTimeLabelColor:I

    .line 308
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->recommendation:Z

    .line 309
    move/from16 v0, p20

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIcon:I

    .line 310
    move/from16 v0, p21

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIconBkColor:I

    .line 311
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationPlaceId:Ljava/lang/String;

    .line 312
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 313
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->distanceStr:Ljava/lang/String;

    .line 314
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->phoneNumbers:Ljava/util/List;

    .line 315
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->contacts:Ljava/util/List;

    .line 318
    :try_start_0
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 319
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->isInitialNumber:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v1

    .line 321
    .local v1, "t":Ljava/lang/Throwable;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->isInitialNumber:Z

    goto :goto_0
.end method

.method private constructor <init>(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)V
    .locals 28
    .param p1, "builder"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 248
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLatitude:D
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$100(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D

    move-result-wide v2

    .line 249
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLongitude:D
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$200(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D

    move-result-wide v4

    .line 250
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLatitude:D
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$300(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D

    move-result-wide v6

    .line 251
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLongitude:D
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$400(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D

    move-result-wide v8

    .line 252
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->fullAddress:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$500(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v10

    .line 253
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationTitle:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$600(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v11

    .line 254
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationSubtitle:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$700(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v12

    .line 255
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->identifier:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$800(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v13

    .line 256
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$900(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    move-result-object v14

    .line 257
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1000(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    move-result-object v15

    .line 258
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->initials:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1100(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v16

    .line 259
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1200(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    move-result-object v17

    .line 260
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabel:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1300(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v18

    .line 261
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabelColor:I
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1400(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)I

    move-result v19

    .line 262
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recommendation:Z
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1500(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Z

    move-result v20

    .line 263
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIcon:I
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1600(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)I

    move-result v21

    .line 264
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor:I
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1700(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)I

    move-result v22

    .line 265
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationPlaceId:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1800(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v23

    .line 266
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeType:Lcom/navdy/service/library/events/places/PlaceType;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$1900(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v24

    .line 267
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->distanceStr:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$2000(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;

    move-result-object v25

    .line 268
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->phoneNumbers:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$2100(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/util/List;

    move-result-object v26

    .line 269
    # getter for: Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->contacts:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->access$2200(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/util/List;

    move-result-object v27

    move-object/from16 v1, p0

    .line 248
    invoke-direct/range {v1 .. v27}, Lcom/navdy/hud/app/framework/destinations/Destination;-><init>(DDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;Ljava/lang/String;IZIILjava/lang/String;Lcom/navdy/service/library/events/places/PlaceType;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 270
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;Lcom/navdy/hud/app/framework/destinations/Destination$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .param p2, "x1"    # Lcom/navdy/hud/app/framework/destinations/Destination$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/destinations/Destination;-><init>(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)V

    return-void
.end method

.method public static getGasDestination()Lcom/navdy/hud/app/framework/destinations/Destination;
    .locals 5

    .prologue
    .line 328
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 330
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 331
    const v3, 0x7f090281

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 332
    .local v2, "title":Ljava/lang/String;
    const v3, 0x7f090280

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 338
    .local v1, "subtitle":Ljava/lang/String;
    :goto_0
    new-instance v3, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationTitle(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v3

    .line 339
    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationSubtitle(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_NONE:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 340
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->favoriteDestinationType(Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->FIND_GAS:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .line 341
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationType(Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v3

    .line 342
    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->build()Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v3

    return-object v3

    .line 334
    .end local v1    # "subtitle":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    const v3, 0x7f090282

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 335
    .restart local v2    # "title":Ljava/lang/String;
    const v3, 0x7f09027f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "subtitle":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] placeid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationPlaceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] nav_pos["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " display_pos["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] title["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] subTitle["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationSubtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] address["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] place_type["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/Destination;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
