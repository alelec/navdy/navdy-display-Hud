.class Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;
.super Ljava/lang/Object;
.source "DestinationsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->onRecommendedDestinationsUpdate(Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

.field final synthetic val$response:Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->val$response:Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->val$response:Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v1, v2, :cond_0

    .line 188
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->this$0:Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->val$response:Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    # invokes: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->parseDestinations(Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$300(Lcom/navdy/hud/app/framework/destinations/DestinationsManager;Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;)V

    .line 197
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->val$response:Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v1, v2, :cond_1

    .line 190
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fav-destination response: version"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->val$response:Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    iget-object v3, v3, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is current"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 192
    .end local v0    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_1
    # getter for: Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sent fav-destination response error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/destinations/DestinationsManager$3;->val$response:Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    iget-object v3, v3, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
