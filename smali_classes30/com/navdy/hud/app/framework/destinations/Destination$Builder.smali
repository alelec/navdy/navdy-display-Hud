.class public Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
.super Ljava/lang/Object;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/destinations/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private destinationIcon:I

.field private destinationIconBkColor:I

.field private destinationPlaceId:Ljava/lang/String;

.field private destinationSubtitle:Ljava/lang/String;

.field private destinationTitle:Ljava/lang/String;

.field private destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

.field private displayPositionLatitude:D

.field private displayPositionLongitude:D

.field private distanceStr:Ljava/lang/String;

.field private favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field private fullAddress:Ljava/lang/String;

.field private identifier:Ljava/lang/String;

.field private initials:Ljava/lang/String;

.field private navigationPositionLatitude:D

.field private navigationPositionLongitude:D

.field private phoneNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

.field private placeType:Lcom/navdy/service/library/events/places/PlaceType;

.field private recentTimeLabel:Ljava/lang/String;

.field private recentTimeLabelColor:I

.field private recommendation:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor:I

    .line 106
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/framework/destinations/Destination;)V
    .locals 2
    .param p1, "d"    # Lcom/navdy/hud/app/framework/destinations/Destination;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor:I

    .line 109
    iget-wide v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLatitude:D

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLatitude:D

    .line 110
    iget-wide v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->navigationPositionLongitude:D

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLongitude:D

    .line 111
    iget-wide v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLatitude:D

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLatitude:D

    .line 112
    iget-wide v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->displayPositionLongitude:D

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLongitude:D

    .line 113
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 114
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->fullAddress:Ljava/lang/String;

    .line 115
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationTitle:Ljava/lang/String;

    .line 116
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationSubtitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationSubtitle:Ljava/lang/String;

    .line 117
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->identifier:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .line 119
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->initials:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->initials:Ljava/lang/String;

    .line 120
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    .line 121
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->recentTimeLabel:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabel:Ljava/lang/String;

    .line 122
    iget v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->recentTimeLabelColor:I

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabelColor:I

    .line 123
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->recommendation:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recommendation:Z

    .line 124
    iget v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIcon:I

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIcon:I

    .line 125
    iget v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIconBkColor:I

    iput v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor:I

    .line 126
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationPlaceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationPlaceId:Ljava/lang/String;

    .line 127
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->distanceStr:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->distanceStr:Ljava/lang/String;

    .line 128
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->phoneNumbers:Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->phoneNumbers:Ljava/util/List;

    .line 129
    iget-object v0, p1, Lcom/navdy/hud/app/framework/destinations/Destination;->contacts:Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->contacts:Ljava/util/List;

    .line 130
    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLatitude:D

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->initials:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabelColor:I

    return v0
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recommendation:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIcon:I

    return v0
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor:I

    return v0
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationPlaceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/service/library/events/places/PlaceType;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLongitude:D

    return-wide v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->distanceStr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->phoneNumbers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->contacts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLatitude:D

    return-wide v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLongitude:D

    return-wide v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->fullAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationSubtitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->identifier:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;)Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/navdy/hud/app/framework/destinations/Destination;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/framework/destinations/Destination;-><init>(Lcom/navdy/hud/app/framework/destinations/Destination$Builder;Lcom/navdy/hud/app/framework/destinations/Destination$1;)V

    return-object v0
.end method

.method public contacts(Ljava/util/List;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)",
            "Lcom/navdy/hud/app/framework/destinations/Destination$Builder;"
        }
    .end annotation

    .prologue
    .line 238
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->contacts:Ljava/util/List;

    .line 239
    return-object p0
.end method

.method public destinationIcon(I)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "destinationIcon"    # I

    .prologue
    .line 208
    iput p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIcon:I

    .line 209
    return-object p0
.end method

.method public destinationIconBkColor(I)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "destinationIconBkColor"    # I

    .prologue
    .line 213
    iput p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor:I

    .line 214
    return-object p0
.end method

.method public destinationPlaceId(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationPlaceId:Ljava/lang/String;

    .line 219
    return-object p0
.end method

.method public destinationSubtitle(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "destinationSubtitle"    # Ljava/lang/String;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationSubtitle:Ljava/lang/String;

    .line 164
    return-object p0
.end method

.method public destinationTitle(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "destinationTitle"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationTitle:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public destinationType(Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "destinationType"    # Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    .line 174
    return-object p0
.end method

.method public displayPositionLatitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 1
    .param p1, "displayPositionLatitude"    # D

    .prologue
    .line 143
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLatitude:D

    .line 144
    return-object p0
.end method

.method public displayPositionLongitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 1
    .param p1, "displayPositionLongitude"    # D

    .prologue
    .line 148
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->displayPositionLongitude:D

    .line 149
    return-object p0
.end method

.method public distanceStr(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "distanceStr"    # Ljava/lang/String;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->distanceStr:Ljava/lang/String;

    .line 229
    return-object p0
.end method

.method public favoriteDestinationType(Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "favoriteDestinationType"    # Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 179
    return-object p0
.end method

.method public fullAddress(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "fullAddress"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->fullAddress:Ljava/lang/String;

    .line 154
    return-object p0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->identifier:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public initials(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "initials"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->initials:Ljava/lang/String;

    .line 184
    return-object p0
.end method

.method public navigationPositionLatitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 1
    .param p1, "navigationPositionLatitude"    # D

    .prologue
    .line 133
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLatitude:D

    .line 134
    return-object p0
.end method

.method public navigationPositionLongitude(D)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 1
    .param p1, "navigationPositionLongitude"    # D

    .prologue
    .line 138
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->navigationPositionLongitude:D

    .line 139
    return-object p0
.end method

.method public phoneNumbers(Ljava/util/List;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)",
            "Lcom/navdy/hud/app/framework/destinations/Destination$Builder;"
        }
    .end annotation

    .prologue
    .line 233
    .local p1, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->phoneNumbers:Ljava/util/List;

    .line 234
    return-object p0
.end method

.method public placeCategory(Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "placeCategory"    # Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeCategory:Lcom/navdy/hud/app/framework/destinations/Destination$PlaceCategory;

    .line 189
    return-object p0
.end method

.method public placeType(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 224
    return-object p0
.end method

.method public recentTimeLabel(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "recentTimeLabel"    # Ljava/lang/String;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabel:Ljava/lang/String;

    .line 194
    return-object p0
.end method

.method public recentTimeLabelColor(I)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "recentTimeLabelColor"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recentTimeLabelColor:I

    .line 199
    return-object p0
.end method

.method public recommendation(Z)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;
    .locals 0
    .param p1, "recommendation"    # Z

    .prologue
    .line 203
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->recommendation:Z

    .line 204
    return-object p0
.end method
