.class public final enum Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
.super Ljava/lang/Enum;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/destinations/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FavoriteDestinationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field public static final enum FAVORITE_CALENDAR:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field public static final enum FAVORITE_CONTACT:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field public static final enum FAVORITE_CUSTOM:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field public static final enum FAVORITE_HOME:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field public static final enum FAVORITE_NONE:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

.field public static final enum FAVORITE_WORK:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;


# instance fields
.field public final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    const-string v1, "FAVORITE_NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_NONE:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    const-string v1, "FAVORITE_HOME"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_HOME:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 23
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    const-string v1, "FAVORITE_WORK"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_WORK:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 24
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    const-string v1, "FAVORITE_CONTACT"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_CONTACT:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 25
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    const-string v1, "FAVORITE_CALENDAR"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_CALENDAR:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    const-string v1, "FAVORITE_CUSTOM"

    const/4 v2, 0x5

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_CUSTOM:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_NONE:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_HOME:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_WORK:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_CONTACT:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_CALENDAR:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_CUSTOM:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->$VALUES:[Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->value:I

    .line 32
    return-void
.end method

.method public static buildFromValue(I)Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    .locals 5
    .param p0, "n"    # I

    .prologue
    .line 35
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->values()[Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 36
    .local v0, "type":Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    iget v4, v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->value:I

    if-ne p0, v4, :cond_0

    .line 40
    .end local v0    # "type":Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    :goto_1
    return-object v0

    .line 35
    .restart local v0    # "type":Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "type":Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_NONE:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->$VALUES:[Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    return-object v0
.end method
