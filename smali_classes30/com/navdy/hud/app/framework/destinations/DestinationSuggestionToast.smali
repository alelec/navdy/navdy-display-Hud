.class public Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;
.super Ljava/lang/Object;
.source "DestinationSuggestionToast.java"


# static fields
.field public static final DESTINATION_SUGGESTION_TOAST_ID:Ljava/lang/String; = "connection#toast"

.field private static final TAG_ACCEPT:I = 0x1

.field private static final TAG_IGNORE:I = 0x2

.field private static final TOAST_TIMEOUT:I = 0x3a98

.field private static final accept:Ljava/lang/String;

.field private static final ignore:Ljava/lang/String;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final suggestionChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 40
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->suggestionChoices:Ljava/util/ArrayList;

    .line 48
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 49
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f090284

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->title:Ljava/lang/String;

    .line 50
    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->accept:Ljava/lang/String;

    .line 51
    const v1, 0x7f090186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->ignore:Ljava/lang/String;

    .line 52
    sget-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->suggestionChoices:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v3, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->accept:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v1, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->suggestionChoices:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v3, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->ignore:Ljava/lang/String;

    invoke-direct {v2, v3, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dismissSuggestionToast()V
    .locals 2

    .prologue
    .line 177
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 178
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v1, "connection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 179
    const-string v1, "connection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public static showSuggestion(Lcom/navdy/service/library/events/places/SuggestedDestination;)V
    .locals 20
    .param p0, "suggestion"    # Lcom/navdy/service/library/events/places/SuggestedDestination;

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 59
    .local v9, "destination":Lcom/navdy/service/library/events/destination/Destination;
    const v12, 0x7f0201be

    .line 60
    .local v12, "iconRes":I
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 61
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v2, "13"

    const/16 v3, 0x3a98

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    if-eqz v2, :cond_0

    .line 64
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$2;->$SwitchMap$com$navdy$service$library$events$places$SuggestedDestination$SuggestionType:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 71
    :cond_0
    :goto_0
    const v2, 0x7f0201be

    if-ne v12, v2, :cond_1

    .line 72
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$2;->$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

    iget-object v3, v9, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 87
    :cond_1
    :goto_1
    const-string v2, "8"

    invoke-virtual {v4, v2, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    const-string v2, "1_1"

    const v3, 0x7f0c0010

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 89
    const-string v2, "19"

    const/4 v3, 0x1

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    const-string v2, "1"

    sget-object v3, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->title:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v2, "20"

    sget-object v3, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->suggestionChoices:Ljava/util/ArrayList;

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 94
    iget-object v2, v9, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v14, v9, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    .line 95
    .local v14, "title2":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->duration_traffic:Ljava/lang/Integer;

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 96
    .local v10, "durationWithTraffic":I
    const-string v15, ""

    .line 97
    .local v15, "title3":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 98
    .local v13, "resources":Landroid/content/res/Resources;
    if-nez v13, :cond_4

    .line 174
    :goto_3
    return-void

    .line 66
    .end local v10    # "durationWithTraffic":I
    .end local v13    # "resources":Landroid/content/res/Resources;
    .end local v14    # "title2":Ljava/lang/String;
    .end local v15    # "title3":Ljava/lang/String;
    :pswitch_0
    const v12, 0x7f0201b2

    goto :goto_0

    .line 74
    :pswitch_1
    const v12, 0x7f0201c0

    .line 75
    goto :goto_1

    .line 77
    :pswitch_2
    const v12, 0x7f0201c1

    .line 78
    goto :goto_1

    .line 80
    :pswitch_3
    const v12, 0x7f0201be

    .line 81
    goto :goto_1

    .line 83
    :pswitch_4
    const v12, 0x7f02018b

    goto :goto_1

    .line 94
    :cond_2
    iget-object v2, v9, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v14, v9, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-object v14, v9, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    goto :goto_2

    .line 101
    .restart local v10    # "durationWithTraffic":I
    .restart local v13    # "resources":Landroid/content/res/Resources;
    .restart local v14    # "title2":Ljava/lang/String;
    .restart local v15    # "title3":Ljava/lang/String;
    :cond_4
    if-lez v10, :cond_5

    .line 102
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v0, v10

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v13, v2}, Lcom/navdy/hud/app/maps/util/RouteUtils;->formatEtaMinutes(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v11

    .line 103
    .local v11, "etaTime":Ljava/lang/String;
    const v2, 0x7f090283

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v11, v3, v6

    invoke-virtual {v13, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 108
    .end local v11    # "etaTime":Ljava/lang/String;
    :goto_4
    const-string v2, "4"

    invoke-virtual {v4, v2, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v2, "5"

    const v3, 0x7f0c0012

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 111
    const-string v2, "6"

    invoke-virtual {v4, v2, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v2, "7"

    const v3, 0x7f0c0011

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    const-string v2, "16"

    const v3, 0x7f0b0087

    invoke-virtual {v13, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    new-instance v5, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;

    invoke-direct {v5, v9}, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast$1;-><init>(Lcom/navdy/service/library/events/destination/Destination;)V

    .line 161
    .local v5, "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    const/4 v7, 0x1

    .line 162
    .local v7, "makeCurrent":Z
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v16

    .line 164
    .local v16, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v8

    .line 165
    .local v8, "currentToast":Ljava/lang/String;
    const-string v2, "incomingcall#toast"

    invoke-static {v8, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 166
    sget-object v2, Lcom/navdy/hud/app/framework/destinations/DestinationSuggestionToast;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "phone toast active"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 167
    const/4 v7, 0x0

    .line 173
    :goto_5
    new-instance v2, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v3, "connection#toast"

    const/4 v6, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    goto/16 :goto_3

    .line 105
    .end local v5    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .end local v7    # "makeCurrent":Z
    .end local v8    # "currentToast":Ljava/lang/String;
    .end local v16    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_5
    iget-object v2, v9, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v15, v9, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    :goto_6
    goto :goto_4

    :cond_6
    iget-object v15, v9, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    goto :goto_6

    .line 169
    .restart local v5    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .restart local v7    # "makeCurrent":Z
    .restart local v8    # "currentToast":Ljava/lang/String;
    .restart local v16    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_7
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 170
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    goto :goto_5

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 72
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
