.class public Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;
.super Landroid/database/ContentObserver;
.source "AdaptiveBrightnessNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# static fields
.field private static final DEFAULT_BRIGHTNESS_INT:I

.field private static final DETENT_TIMEOUT:J = 0xfaL

.field private static final INITIAL_CHANGING_STEP:I = 0x1

.field private static final MAX_BRIGHTNESS:I = 0xff

.field private static final MAX_STEP:I = 0x10

.field private static final NOTIFICATION_TIMEOUT:I = 0x2710

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final AUTO_BRIGHTNESS_ADJ_ENABLED:Z

.field private changingStep:I

.field private choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

.field private container:Landroid/view/ViewGroup;

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private currentValue:I

.field private isAutoBrightnessOn:Z

.field private isLastChangeIncreasing:Z

.field private lastChangeActionTime:J

.field private progressBar:Lcom/navdy/hud/app/view/Gauge;

.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

.field private textIndicator:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 45
    const-string v0, "128"

    .line 46
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->DEFAULT_BRIGHTNESS_INT:I

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 71
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 58
    iput v4, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    .line 59
    const-wide/16 v2, -0xfb

    iput-wide v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->lastChangeActionTime:J

    .line 60
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->isLastChangeIncreasing:Z

    .line 68
    iput-boolean v4, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->AUTO_BRIGHTNESS_ADJ_ENABLED:Z

    .line 72
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    .line 73
    .local v0, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 74
    iget-object v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->getCurrentIsAutoBrightnessOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->isAutoBrightnessOn:Z

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    .prologue
    .line 40
    iget v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->currentValue:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->close()V

    return-void
.end method

.method private close()V
    .locals 2

    .prologue
    .line 250
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 251
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->getCurrentBrightness()I

    move-result v0

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordAdaptiveAutobrightnessAdjustmentChanged(I)V

    .line 252
    return-void
.end method

.method private detentChangingStep(Z)V
    .locals 7
    .param p1, "isChangeIncreasing"    # Z

    .prologue
    const/16 v6, 0x10

    .line 269
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 270
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->isLastChangeIncreasing:Z

    if-ne p1, v2, :cond_1

    iget-wide v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->lastChangeActionTime:J

    const-wide/16 v4, 0xfa

    add-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-ltz v2, :cond_1

    .line 272
    iget v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    shl-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    .line 273
    iget v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    if-le v2, v6, :cond_0

    .line 274
    iput v6, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    .line 279
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->isLastChangeIncreasing:Z

    .line 280
    iput-wide v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->lastChangeActionTime:J

    .line 281
    return-void

    .line 277
    :cond_1
    const/4 v2, 0x1

    iput v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    goto :goto_0
.end method

.method private getCurrentBrightness()I
    .locals 4

    .prologue
    .line 325
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->isAutoBrightnessOn:Z

    if-eqz v1, :cond_0

    .line 327
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 338
    :goto_0
    return v1

    .line 328
    :catch_0
    move-exception v0

    .line 329
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v1, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Settings not found exception "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 330
    sget v1, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->DEFAULT_BRIGHTNESS_INT:I

    goto :goto_0

    .line 334
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "screen.brightness"

    const-string v3, "128"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    goto :goto_0

    .line 336
    :catch_1
    move-exception v0

    .line 337
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Cannot parse brightness from the shared preferences"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 338
    sget v1, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->DEFAULT_BRIGHTNESS_INT:I

    goto :goto_0
.end method

.method private getCurrentIsAutoBrightnessOn()Z
    .locals 4

    .prologue
    .line 321
    const-string v0, "true"

    iget-object v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "screen.auto_brightness"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private showFixedChoices(Landroid/view/ViewGroup;)V
    .locals 10
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    const v2, 0x7f020128

    .line 120
    const v0, 0x7f0e00d2

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 122
    new-instance v8, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 124
    .local v8, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 125
    .local v9, "resources":Landroid/content/res/Resources;
    const v0, 0x7f09011b

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 126
    .local v6, "ok":Ljava/lang/String;
    const v0, 0x7f0d0024

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 127
    .local v3, "okColor":I
    const/high16 v5, -0x1000000

    .line 131
    .local v5, "unselectedColor":I
    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v1, 0x2

    move v4, v2

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v1, 0x1

    new-instance v2, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;-><init>(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)V

    invoke-virtual {v0, v8, v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 146
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 148
    return-void
.end method

.method public static showNotification()V
    .locals 2

    .prologue
    .line 246
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    invoke-direct {v1}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;-><init>()V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 247
    return-void
.end method

.method private updateBrightness(I)V
    .locals 4
    .param p1, "delta"    # I

    .prologue
    .line 352
    if-nez p1, :cond_0

    .line 365
    :goto_0
    return-void

    .line 356
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->currentValue:I

    add-int v0, v1, p1

    .line 357
    .local v0, "newValue":I
    if-gez v0, :cond_1

    .line 358
    const/4 v0, 0x0

    .line 360
    :cond_1
    const/16 v1, 0xff

    if-le v0, v1, :cond_2

    .line 361
    const/16 v0, 0xff

    .line 363
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "screen.brightness"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 364
    iget-object v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "navdy#brightness#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 195
    const/16 v0, 0x2710

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->BRIGHTNESS:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 95
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030035

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->container:Landroid/view/ViewGroup;

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->textIndicator:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0147

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/Gauge;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->progressBar:Lcom/navdy/hud/app/view/Gauge;

    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->progressBar:Lcom/navdy/hud/app/view/Gauge;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/Gauge;->setAnimated(Z)V

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->container:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->showFixedChoices(Landroid/view/ViewGroup;)V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 232
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return-object v0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 0
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 79
    return-void
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->close()V

    .line 257
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 224
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 265
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 285
    iget-object v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_0

    .line 312
    :goto_0
    return v0

    .line 289
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    if-eq p1, v2, :cond_1

    .line 290
    iget-object v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 292
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 294
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    :goto_1
    move v0, v1

    .line 299
    goto :goto_0

    .line 297
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->close()V

    goto :goto_1

    .line 301
    :pswitch_1
    sget-object v2, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Decreasing brightness"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 303
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->detentChangingStep(Z)V

    .line 304
    iget v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    neg-int v0, v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->updateBrightness(I)V

    move v0, v1

    .line 305
    goto :goto_0

    .line 307
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Increasing brightness"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 308
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->detentChangingStep(Z)V

    .line 309
    iget v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->changingStep:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->updateBrightness(I)V

    move v0, v1

    .line 310
    goto :goto_0

    .line 292
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 220
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 346
    const-string v0, "screen.brightness"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->updateState()V

    .line 349
    :cond_0
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 3
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->getCurrentBrightness()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->currentValue:I

    .line 154
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "screen.auto_brightness"

    const-string v2, "false"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 155
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "screen.brightness"

    iget v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->currentValue:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->isAutoBrightnessOn:Z

    .line 160
    iput-object p1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 161
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 162
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->updateState()V

    .line 163
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "screen.auto_brightness"

    const-string v2, "true"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->isAutoBrightnessOn:Z

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 191
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 261
    return-void
.end method

.method public onUpdate()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->updateState()V

    .line 180
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    return v0
.end method

.method public updateState()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_0

    .line 167
    sget-object v0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "brightness notif offscreen"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->getCurrentBrightness()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->currentValue:I

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->progressBar:Lcom/navdy/hud/app/view/Gauge;

    iget v1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->currentValue:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/Gauge;->setValue(I)V

    goto :goto_0
.end method
