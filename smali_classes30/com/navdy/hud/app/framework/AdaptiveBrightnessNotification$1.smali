.class Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;
.super Ljava/lang/Object;
.source "AdaptiveBrightnessNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->showFixedChoices(Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;->this$0:Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 3
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;->this$0:Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    # getter for: Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->access$100(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "screen.auto_brightness_adj"

    iget-object v2, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;->this$0:Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    # getter for: Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->currentValue:I
    invoke-static {v2}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->access$000(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 138
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;->this$0:Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    # getter for: Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->sharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->access$100(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 140
    iget-object v0, p0, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification$1;->this$0:Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;

    # invokes: Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->close()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;->access$200(Lcom/navdy/hud/app/framework/AdaptiveBrightnessNotification;)V

    .line 141
    return-void
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 144
    return-void
.end method
