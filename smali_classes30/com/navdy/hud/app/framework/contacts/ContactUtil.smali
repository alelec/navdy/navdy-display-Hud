.class public Lcom/navdy/hud/app/framework/contacts/ContactUtil;
.super Ljava/lang/Object;
.source "ContactUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;
    }
.end annotation


# static fields
.field private static final EMPTY:Ljava/lang/String; = ""

.field private static HOME_STR:Ljava/lang/String; = null

.field private static MARKER_CHARS:Ljava/util/HashSet; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static MOBILE_STR:Ljava/lang/String; = null

.field public static final SPACE:Ljava/lang/String; = " "

.field private static final SPACE_CHAR:C = ' '

.field private static WORK_STR:Ljava/lang/String;

.field private static builder:Ljava/lang/StringBuilder;

.field private static handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x200e

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->builder:Ljava/lang/StringBuilder;

    .line 47
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->handler:Landroid/os/Handler;

    .line 54
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f09031d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->WORK_STR:Ljava/lang/String;

    .line 56
    const v1, 0x7f0901bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MOBILE_STR:Ljava/lang/String;

    .line 57
    const v1, 0x7f09017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->HOME_STR:Ljava/lang/String;

    .line 58
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MARKER_CHARS:Ljava/util/HashSet;

    .line 59
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MARKER_CHARS:Ljava/util/HashSet;

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MARKER_CHARS:Ljava/util/HashSet;

    const/16 v2, 0x202a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MARKER_CHARS:Ljava/util/HashSet;

    const/16 v2, 0x202c

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MARKER_CHARS:Ljava/util/HashSet;

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;ZLcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .param p4, "x4"    # Lcom/squareup/picasso/Transformation;
    .param p5, "x5"    # Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;
    .param p6, "x6"    # Ljava/io/File;
    .param p7, "x7"    # Z
    .param p8, "x8"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    .param p9, "x9"    # Landroid/graphics/Bitmap;

    .prologue
    .line 36
    invoke-static/range {p0 .. p9}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;ZLcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static convertNumberType(Lcom/navdy/hud/app/framework/contacts/NumberType;)Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .locals 2
    .param p0, "numberType"    # Lcom/navdy/hud/app/framework/contacts/NumberType;

    .prologue
    .line 383
    if-nez p0, :cond_0

    .line 384
    const/4 v0, 0x0

    .line 395
    :goto_0
    return-object v0

    .line 386
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$3;->$SwitchMap$com$navdy$hud$app$framework$contacts$NumberType:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/contacts/NumberType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 395
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_OTHER:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 388
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_HOME:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 391
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_WORK:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 393
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_MOBILE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    goto :goto_0

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static fromContacts(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    .local p0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    if-nez p0, :cond_1

    .line 373
    const/4 v1, 0x0

    .line 379
    :cond_0
    return-object v1

    .line 375
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 376
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/contacts/Contact;

    .line 377
    .local v0, "contact":Lcom/navdy/service/library/events/contacts/Contact;
    new-instance v3, Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {v3, v0}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Lcom/navdy/service/library/events/contacts/Contact;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static fromPhoneNumbers(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    .local p0, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    if-nez p0, :cond_1

    .line 362
    const/4 v1, 0x0

    .line 368
    :cond_0
    return-object v1

    .line 364
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 365
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .line 366
    .local v0, "phoneNumber":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    new-instance v3, Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {v3, v0}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Lcom/navdy/service/library/events/contacts/PhoneNumber;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getFirstName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    const-string p0, ""

    .line 74
    .end local p0    # "name":Ljava/lang/String;
    .local v0, "index":I
    :cond_0
    :goto_0
    return-object p0

    .line 69
    .end local v0    # "index":I
    .restart local p0    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 70
    const-string v1, " "

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 71
    .restart local v0    # "index":I
    if-lez v0, :cond_0

    .line 72
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static declared-synchronized getInitials(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 79
    const-class v4, Lcom/navdy/hud/app/framework/contacts/ContactUtil;

    monitor-enter v4

    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 80
    const-string v3, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :goto_0
    monitor-exit v4

    return-object v3

    .line 82
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/navdy/hud/app/util/GenericUtil;->removePunctuation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 83
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 84
    const-string v3, " "

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 85
    .local v1, "index":I
    if-lez v1, :cond_3

    .line 86
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "first":Ljava/lang/String;
    const-string v3, "[\\u202A]"

    const-string v5, ""

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 89
    .local v2, "last":Ljava/lang/String;
    const-string v3, "[\\u202A]"

    const-string v5, ""

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 90
    sget-object v3, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->builder:Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 91
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 92
    sget-object v3, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->builder:Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 95
    sget-object v3, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->builder:Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 97
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 99
    .end local v0    # "first":Ljava/lang/String;
    .end local v2    # "last":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 100
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 102
    :cond_4
    const-string v3, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 79
    .end local v1    # "index":I
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static getNumberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/hud/app/framework/contacts/NumberType;
    .locals 2
    .param p0, "numberType"    # Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .prologue
    .line 339
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 352
    :goto_0
    return-object v0

    .line 341
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$3;->$SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType:[I

    invoke-virtual {p0}, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 352
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 343
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->HOME:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 346
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 349
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 341
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getPhoneNumber(Ljava/lang/String;)J
    .locals 6
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 170
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 171
    .local v0, "currentLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p0, v4}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 172
    .local v1, "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;->getNationalNumber()J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 174
    .end local v0    # "currentLocale":Ljava/util/Locale;
    .end local v1    # "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :goto_0
    return-wide v4

    .line 173
    :catch_0
    move-exception v2

    .line 174
    .local v2, "t":Ljava/lang/Throwable;
    const-wide/16 v4, -0x1

    goto :goto_0
.end method

.method public static getPhoneType(Lcom/navdy/hud/app/framework/contacts/NumberType;)Ljava/lang/String;
    .locals 3
    .param p0, "numberType"    # Lcom/navdy/hud/app/framework/contacts/NumberType;

    .prologue
    const/4 v0, 0x0

    .line 108
    if-nez p0, :cond_0

    .line 123
    :goto_0
    return-object v0

    .line 111
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil$3;->$SwitchMap$com$navdy$hud$app$framework$contacts$NumberType:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/contacts/NumberType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 114
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->WORK_STR:Ljava/lang/String;

    goto :goto_0

    .line 117
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MOBILE_STR:Ljava/lang/String;

    goto :goto_0

    .line 120
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->HOME_STR:Ljava/lang/String;

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static isDisplayNameValid(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 8
    .param p0, "displayName"    # Ljava/lang/String;
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "nPhoneNum"    # J

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 141
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 165
    :cond_0
    :goto_0
    return v4

    .line 144
    :cond_1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v4, v5

    .line 145
    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 154
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 155
    .local v0, "currentLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v6

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, p0, v7}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 156
    .local v1, "dNum":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;->getNationalNumber()J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 157
    .local v2, "nDisplayNum":J
    cmp-long v6, v2, p2

    if-eqz v6, :cond_0

    .end local v0    # "currentLocale":Ljava/util/Locale;
    .end local v1    # "dNum":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .end local v2    # "nDisplayNum":J
    :goto_1
    move v4, v5

    .line 165
    goto :goto_0

    .line 160
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method public static isImageAvailable(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "contactName"    # Ljava/lang/String;
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return v0

    .line 182
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isValidNumber(Ljava/lang/String;)Z
    .locals 6
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 129
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 136
    :goto_0
    return v3

    .line 132
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 133
    .local v0, "currentLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p0, v5}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 134
    .local v1, "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    const/4 v3, 0x1

    goto :goto_0

    .line 135
    .end local v0    # "currentLocale":Ljava/util/Locale;
    .end local v1    # "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :catch_0
    move-exception v2

    .line 136
    .local v2, "t":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method public static sanitizeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 318
    if-nez p0, :cond_0

    .line 334
    .end local p0    # "str":Ljava/lang/String;
    .local v0, "chrs":[C
    .local v1, "foundMarker":Z
    .local v2, "i":I
    .local v3, "len":I
    :goto_0
    return-object p0

    .line 321
    .end local v0    # "chrs":[C
    .end local v1    # "foundMarker":Z
    .end local v2    # "i":I
    .end local v3    # "len":I
    .restart local p0    # "str":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 322
    .restart local v1    # "foundMarker":Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 323
    .restart local v3    # "len":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 324
    .restart local v0    # "chrs":[C
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    array-length v4, v0

    if-ge v2, v4, :cond_2

    .line 325
    sget-object v4, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->MARKER_CHARS:Ljava/util/HashSet;

    aget-char v5, v0, v2

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 326
    const/4 v1, 0x1

    .line 327
    const/16 v4, 0x20

    aput-char v4, v0, v2

    .line 324
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 331
    :cond_2
    if-nez v1, :cond_3

    .line 332
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 334
    :cond_3
    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v4, v0, v5, v3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V
    .locals 21
    .param p0, "contactName"    # Ljava/lang/String;
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "triggerPhotoDownload"    # Z
    .param p3, "imageView"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .param p4, "transformation"    # Lcom/squareup/picasso/Transformation;
    .param p5, "callback"    # Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

    .prologue
    .line 197
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 198
    invoke-static/range {p0 .. p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    const v2, 0x7f02021e

    const/4 v3, 0x0

    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 257
    :goto_0
    return-void

    .line 202
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 204
    const v2, 0x7f02021e

    const/4 v3, 0x0

    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_0

    .line 209
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getPhoneNumber(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isDisplayNameValid(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v2

    if-nez v2, :cond_2

    .line 210
    const/16 p0, 0x0

    .line 212
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v10

    .line 213
    .local v10, "phoneImageDownloader":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    sget-object v2, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    move-object/from16 v0, p1

    invoke-virtual {v10, v0, v2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getImagePath(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v8

    .line 214
    .local v8, "imagePath":Ljava/io/File;
    invoke-static {v8}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 215
    .local v11, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v11, :cond_3

    .line 216
    const/4 v2, 0x0

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 217
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 219
    const/4 v9, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-static/range {v2 .. v11}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;ZLcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 230
    :cond_3
    move-object/from16 v15, p0

    .line 231
    .local v15, "cName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v12, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    move-object/from16 v13, p5

    move-object v14, v8

    move-object/from16 v16, p1

    move/from16 v17, p2

    move-object/from16 v18, p3

    move-object/from16 v19, p4

    move-object/from16 v20, v10

    invoke-direct/range {v12 .. v20}, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;-><init>(Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v12, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;ZLcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p0, "contactName"    # Ljava/lang/String;
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "triggerPhotoDownload"    # Z
    .param p3, "imageView"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .param p4, "transformation"    # Lcom/squareup/picasso/Transformation;
    .param p5, "callback"    # Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;
    .param p6, "imagePath"    # Ljava/io/File;
    .param p7, "imageExists"    # Z
    .param p8, "phoneImageDownloader"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    .param p9, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 269
    if-nez p9, :cond_2

    .line 270
    invoke-interface {p5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;->isContextValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    if-nez p7, :cond_4

    .line 274
    if-eqz p0, :cond_3

    .line 276
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v0

    .line 277
    .local v0, "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v1

    .line 278
    .local v1, "index":I
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getResourceId(I)I

    move-result v2

    invoke-static {p0}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {p3, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 290
    .end local v0    # "contactImageHelper":Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;
    .end local v1    # "index":I
    :cond_2
    :goto_1
    if-eqz p2, :cond_0

    .line 292
    sget-object v2, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {p8, p1, v2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->clearPhotoCheckEntry(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    .line 293
    sget-object v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->HIGH:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    sget-object v3, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {p8, p1, v2, v3, p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->submitDownload(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V

    goto :goto_0

    .line 281
    :cond_3
    const v2, 0x7f02021e

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {p3, v2, v4, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_1

    .line 285
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-static {v2, v4, p3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->updateInitials(Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;)V

    .line 286
    invoke-static {p6, p3, p4}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setImage(Ljava/io/File;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;)V

    goto :goto_1
.end method

.method private static setImage(Ljava/io/File;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;)V
    .locals 2
    .param p0, "path"    # Ljava/io/File;
    .param p1, "imageView"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .param p2, "transformation"    # Lcom/squareup/picasso/Transformation;

    .prologue
    .line 301
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getInstance()Lcom/squareup/picasso/Picasso;

    move-result-object v0

    .line 302
    invoke-virtual {v0, p0}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 304
    invoke-virtual {v0, p2}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil$2;

    invoke-direct {v1, p1, p0}, Lcom/navdy/hud/app/framework/contacts/ContactUtil$2;-><init>(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/io/File;)V

    .line 305
    invoke-virtual {v0, p1, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 315
    return-void
.end method

.method public static toContacts(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    const/4 v4, 0x0

    .line 417
    if-nez p0, :cond_0

    .line 430
    :goto_0
    return-object v4

    .line 420
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 421
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 422
    .local v0, "c":Lcom/navdy/hud/app/framework/contacts/Contact;
    new-instance v3, Lcom/navdy/service/library/events/contacts/Contact$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/contacts/Contact$Builder;-><init>()V

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 423
    invoke-virtual {v3, v6}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v3

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    .line 424
    invoke-virtual {v3, v6}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v3

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 425
    invoke-static {v6}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->convertNumberType(Lcom/navdy/hud/app/framework/contacts/NumberType;)Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->numberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v6

    iget-object v3, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    sget-object v7, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    if-ne v3, v7, :cond_1

    iget-object v3, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 426
    :goto_2
    invoke-virtual {v6, v3}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v3

    .line 427
    invoke-virtual {v3}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->build()Lcom/navdy/service/library/events/contacts/Contact;

    move-result-object v1

    .line 428
    .local v1, "contact":Lcom/navdy/service/library/events/contacts/Contact;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v1    # "contact":Lcom/navdy/service/library/events/contacts/Contact;
    :cond_1
    move-object v3, v4

    .line 425
    goto :goto_2

    .end local v0    # "c":Lcom/navdy/hud/app/framework/contacts/Contact;
    :cond_2
    move-object v4, v2

    .line 430
    goto :goto_0
.end method

.method public static toPhoneNumbers(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    const/4 v4, 0x0

    .line 401
    if-nez p0, :cond_0

    .line 413
    :goto_0
    return-object v4

    .line 404
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 405
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/contacts/Contact;

    .line 406
    .local v0, "c":Lcom/navdy/hud/app/framework/contacts/Contact;
    new-instance v3, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;-><init>()V

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    .line 407
    invoke-virtual {v3, v6}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;

    move-result-object v3

    iget-object v6, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 408
    invoke-static {v6}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->convertNumberType(Lcom/navdy/hud/app/framework/contacts/NumberType;)Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->numberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;

    move-result-object v6

    iget-object v3, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    sget-object v7, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    if-ne v3, v7, :cond_1

    iget-object v3, v0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 409
    :goto_2
    invoke-virtual {v6, v3}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->customType(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;

    move-result-object v3

    .line 410
    invoke-virtual {v3}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->build()Lcom/navdy/service/library/events/contacts/PhoneNumber;

    move-result-object v1

    .line 411
    .local v1, "phoneNumber":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v1    # "phoneNumber":Lcom/navdy/service/library/events/contacts/PhoneNumber;
    :cond_1
    move-object v3, v4

    .line 408
    goto :goto_2

    .end local v0    # "c":Lcom/navdy/hud/app/framework/contacts/Contact;
    :cond_2
    move-object v4, v2

    .line 413
    goto :goto_0
.end method

.method private static updateInitials(Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;)V
    .locals 0
    .param p0, "style"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;
    .param p1, "initials"    # Ljava/lang/String;
    .param p2, "imageView"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .prologue
    .line 297
    invoke-virtual {p2, p1, p0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 298
    return-void
.end method
