.class Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;
.super Ljava/lang/Object;
.source "PhoneImageDownloader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->removeImage(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

.field final synthetic val$file:Ljava/io/File;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$photoType:Lcom/navdy/service/library/events/photo/PhotoType;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Ljava/io/File;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->val$file:Ljava/io/File;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->val$id:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->val$photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 386
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$700(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->val$file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removed ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->val$id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$500(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->val$id:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;->val$photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct {v1, v2, v3, v4, v4}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 390
    return-void
.end method
