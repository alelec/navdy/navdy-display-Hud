.class public final enum Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;
.super Ljava/lang/Enum;
.source "PhoneImageDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Priority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

.field public static final enum HIGH:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

.field public static final enum NORMAL:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;


# instance fields
.field private final val:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 51
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->NORMAL:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    .line 52
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->HIGH:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    .line 50
    new-array v0, v4, [Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->NORMAL:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->HIGH:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->$VALUES:[Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->val:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    const-class v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->$VALUES:[Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->val:I

    return v0
.end method
