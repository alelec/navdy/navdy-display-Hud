.class public Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;
.super Ljava/lang/Object;
.source "FavoriteContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;
    }
.end annotation


# static fields
.field private static final FAVORITE_CONTACTS_CHANGED:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;

.field private static final MAX_FAV_CONTACTS:I = 0x1e

.field private static final sInstance:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private volatile favContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private fullSync:Z

.field private isAllowedToReceiveContacts:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 33
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;-><init>(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$1;)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->FAVORITE_CONTACTS_CHANGED:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;

    .line 42
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sInstance:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->isAllowedToReceiveContacts:Z

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->fullSync:Z

    .line 55
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->bus:Lcom/squareup/otto/Bus;

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->load()V

    return-void
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->isAllowedToReceiveContacts:Z

    return p1
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->fullSync:Z

    return v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sInstance:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    return-object v0
.end method

.method private load()V
    .locals 5

    .prologue
    .line 154
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "id":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->getFavoriteContacts(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->favContacts:Ljava/util/List;

    .line 156
    sget-object v2, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "load favcontacts id["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] number of contacts["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->favContacts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    iget-object v2, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->FAVORITE_CONTACTS_CHANGED:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 163
    .end local v0    # "id":Ljava/lang/String;
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v1

    .line 158
    .local v1, "t":Ljava/lang/Throwable;
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->favContacts:Ljava/util/List;

    .line 159
    sget-object v2, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    iget-object v2, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->FAVORITE_CONTACTS_CHANGED:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->FAVORITE_CONTACTS_CHANGED:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    throw v2
.end method


# virtual methods
.method public buildFavoriteContacts()V
    .locals 3

    .prologue
    .line 73
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$1;-><init>(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->load()V

    goto :goto_0
.end method

.method public clearFavoriteContacts()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->setFavoriteContacts(Ljava/util/List;)V

    .line 70
    return-void
.end method

.method public getFavoriteContacts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->favContacts:Ljava/util/List;

    return-object v0
.end method

.method public isAllowedToReceiveContacts()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->isAllowedToReceiveContacts:Z

    return v0
.end method

.method public onFavoriteContactResponse(Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 97
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;-><init>(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 150
    return-void
.end method

.method public setFavoriteContacts(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "favContacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->favContacts:Ljava/util/List;

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->FAVORITE_CONTACTS_CHANGED:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$FavoriteContactsChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public syncFavoriteContacts(Z)V
    .locals 5
    .param p1, "fullSync"    # Z

    .prologue
    .line 87
    :try_start_0
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->fullSync:Z

    .line 88
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;-><init>(Ljava/lang/Integer;)V

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 89
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "sent fav-contact request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
