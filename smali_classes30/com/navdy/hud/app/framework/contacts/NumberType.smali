.class public final enum Lcom/navdy/hud/app/framework/contacts/NumberType;
.super Ljava/lang/Enum;
.source "NumberType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/contacts/NumberType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/contacts/NumberType;

.field public static final enum HOME:Lcom/navdy/hud/app/framework/contacts/NumberType;

.field public static final enum MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

.field public static final enum OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

.field public static final enum WORK:Lcom/navdy/hud/app/framework/contacts/NumberType;

.field public static final enum WORK_MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;


# instance fields
.field value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 9
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/NumberType;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v7, v3}, Lcom/navdy/hud/app/framework/contacts/NumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->HOME:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 10
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/NumberType;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/hud/app/framework/contacts/NumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 11
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/NumberType;

    const-string v1, "WORK"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/hud/app/framework/contacts/NumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 12
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/NumberType;

    const-string v1, "WORK_MOBILE"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/hud/app/framework/contacts/NumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK_MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 13
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/NumberType;

    const-string v1, "OTHER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/hud/app/framework/contacts/NumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/hud/app/framework/contacts/NumberType;

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/NumberType;->HOME:Lcom/navdy/hud/app/framework/contacts/NumberType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/NumberType;->MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK:Lcom/navdy/hud/app/framework/contacts/NumberType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK_MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->$VALUES:[Lcom/navdy/hud/app/framework/contacts/NumberType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/navdy/hud/app/framework/contacts/NumberType;->value:I

    .line 19
    return-void
.end method

.method public static buildFromValue(I)Lcom/navdy/hud/app/framework/contacts/NumberType;
    .locals 1
    .param p0, "n"    # I

    .prologue
    .line 26
    packed-switch p0, :pswitch_data_0

    .line 40
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    :goto_0
    return-object v0

    .line 28
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->HOME:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 31
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 34
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 37
    :pswitch_3
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK_MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 26
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/contacts/NumberType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/contacts/NumberType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/contacts/NumberType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->$VALUES:[Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/contacts/NumberType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/contacts/NumberType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/hud/app/framework/contacts/NumberType;->value:I

    return v0
.end method
