.class public Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
.super Ljava/lang/Object;
.source "PhoneImageDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoDownloadStatus"
.end annotation


# instance fields
.field public alreadyDownloaded:Z

.field public photoType:Lcom/navdy/service/library/events/photo/PhotoType;

.field public sourceIdentifier:Ljava/lang/String;

.field public success:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V
    .locals 0
    .param p1, "sourceIdentifier"    # Ljava/lang/String;
    .param p2, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;
    .param p3, "success"    # Z
    .param p4, "alreadyDownloaded"    # Z

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->sourceIdentifier:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 69
    iput-boolean p3, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->success:Z

    .line 70
    iput-boolean p4, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->alreadyDownloaded:Z

    .line 71
    return-void
.end method
