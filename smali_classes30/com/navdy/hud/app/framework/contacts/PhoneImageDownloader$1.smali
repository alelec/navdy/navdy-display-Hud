.class Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$1;
.super Ljava/lang/Object;
.source "PhoneImageDownloader.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$1;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;)I
    .locals 4
    .param p1, "lhs"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    .param p2, "rhs"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;

    .prologue
    .line 97
    instance-of v2, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;

    if-eqz v2, :cond_0

    instance-of v2, p2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;

    if-eqz v2, :cond_0

    .line 98
    move-object v0, p1

    .line 99
    .local v0, "left":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    move-object v1, p2

    .line 100
    .local v1, "right":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    iget v2, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->priority:I

    iget v3, v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->priority:I

    sub-int/2addr v2, v3

    .line 102
    .end local v0    # "left":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    .end local v1    # "right":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 94
    check-cast p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;

    check-cast p2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$1;->compare(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;)I

    move-result v0

    return v0
.end method
