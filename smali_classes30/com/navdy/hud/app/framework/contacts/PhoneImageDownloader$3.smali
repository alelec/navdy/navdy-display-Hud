.class Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;
.super Ljava/lang/Object;
.source "PhoneImageDownloader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->storePhoto(Lcom/navdy/service/library/events/photo/PhotoResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

.field final synthetic val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Lcom/navdy/service/library/events/photo/PhotoResponse;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 279
    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    const-string v14, "storing photo"

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 280
    const/4 v6, 0x0

    .line 281
    .local v6, "fos":Ljava/io/FileOutputStream;
    const/4 v8, 0x0

    .line 283
    .local v8, "fos_hash":Ljava/io/FileOutputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v13, v13, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    invoke-virtual {v13}, Lokio/ByteString;->toByteArray()[B

    move-result-object v1

    .line 284
    .local v1, "byteArray":[B
    const/4 v13, 0x0

    array-length v14, v1

    invoke-static {v1, v13, v14}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 285
    .local v11, "image":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v14, v14, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    # invokes: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;
    invoke-static {v13, v14, v15}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$200(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v5

    .line 286
    .local v5, "filename":Ljava/lang/String;
    if-eqz v11, :cond_1

    .line 287
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    .line 288
    .local v2, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->currentDriverProfileId:Ljava/lang/String;
    invoke-static {v14}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$300(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 289
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v14, v14, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    # invokes: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getPhotoPath(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
    invoke-static {v13, v2, v14}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$400(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v13

    invoke-direct {v3, v13, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 290
    .local v3, "f":Ljava/io/File;
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v7, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 293
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->hashForBytes([B)Ljava/lang/String;

    move-result-object v10

    .line 294
    .local v10, "hash":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v14, v14, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    # invokes: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getPhotoPath(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
    invoke-static {v13, v2, v14}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$400(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".md5"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v4, v13, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 295
    .local v4, "f_hash":Ljava/io/File;
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 296
    .end local v8    # "fos_hash":Ljava/io/FileOutputStream;
    .local v9, "fos_hash":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v9, v13}, Ljava/io/FileOutputStream;->write([B)V

    .line 297
    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "photo saved["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] id["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 298
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getInstance()Lcom/squareup/picasso/Picasso;

    move-result-object v13

    invoke-virtual {v13, v3}, Lcom/squareup/picasso/Picasso;->invalidate(Ljava/io/File;)V

    .line 299
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v13}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$500(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;

    move-result-object v13

    new-instance v14, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    const/16 v18, 0x0

    invoke-direct/range {v14 .. v18}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V

    invoke-virtual {v13, v14}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v8, v9

    .end local v9    # "fos_hash":Ljava/io/FileOutputStream;
    .restart local v8    # "fos_hash":Ljava/io/FileOutputStream;
    move-object v6, v7

    .line 312
    .end local v2    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "f_hash":Ljava/io/File;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .end local v10    # "hash":Ljava/lang/String;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 313
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 314
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 315
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # invokes: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V
    invoke-static {v13}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$600(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V

    .line 318
    .end local v1    # "byteArray":[B
    .end local v5    # "filename":Ljava/lang/String;
    .end local v11    # "image":Landroid/graphics/Bitmap;
    :goto_1
    return-void

    .line 301
    .restart local v1    # "byteArray":[B
    .restart local v2    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v11    # "image":Landroid/graphics/Bitmap;
    :cond_0
    :try_start_3
    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "photo cannot save ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] id["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] profile changed from ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .line 302
    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->currentDriverProfileId:Ljava/lang/String;
    invoke-static {v15}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$300(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] to ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 303
    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 301
    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 308
    .end local v1    # "byteArray":[B
    .end local v2    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v5    # "filename":Ljava/lang/String;
    .end local v11    # "image":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v12

    .line 309
    .local v12, "t":Ljava/lang/Throwable;
    :goto_2
    :try_start_4
    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v13

    invoke-virtual {v13, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 310
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v13}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$500(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;

    move-result-object v13

    new-instance v14, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v14 .. v18}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V

    invoke-virtual {v13, v14}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 312
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 313
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 314
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 315
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # invokes: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V
    invoke-static {v13}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$600(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V

    goto/16 :goto_1

    .line 306
    .end local v12    # "t":Ljava/lang/Throwable;
    .restart local v1    # "byteArray":[B
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v11    # "image":Landroid/graphics/Bitmap;
    :cond_1
    :try_start_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # getter for: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v13}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$500(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;

    move-result-object v13

    new-instance v14, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    iget-object v15, v15, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v14 .. v18}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V

    invoke-virtual {v13, v14}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 312
    .end local v1    # "byteArray":[B
    .end local v5    # "filename":Ljava/lang/String;
    .end local v11    # "image":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v13

    :goto_3
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 313
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 314
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 315
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;->this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    # invokes: Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V
    invoke-static {v14}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->access$600(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V

    throw v13

    .line 312
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "byteArray":[B
    .restart local v2    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .restart local v3    # "f":Ljava/io/File;
    .restart local v5    # "filename":Ljava/lang/String;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "image":Landroid/graphics/Bitmap;
    :catchall_1
    move-exception v13

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "fos_hash":Ljava/io/FileOutputStream;
    .restart local v4    # "f_hash":Ljava/io/File;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos_hash":Ljava/io/FileOutputStream;
    .restart local v10    # "hash":Ljava/lang/String;
    :catchall_2
    move-exception v13

    move-object v8, v9

    .end local v9    # "fos_hash":Ljava/io/FileOutputStream;
    .restart local v8    # "fos_hash":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 308
    .end local v4    # "f_hash":Ljava/io/File;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v10    # "hash":Ljava/lang/String;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v12

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_2

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "fos_hash":Ljava/io/FileOutputStream;
    .restart local v4    # "f_hash":Ljava/io/File;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos_hash":Ljava/io/FileOutputStream;
    .restart local v10    # "hash":Ljava/lang/String;
    :catch_2
    move-exception v12

    move-object v8, v9

    .end local v9    # "fos_hash":Ljava/io/FileOutputStream;
    .restart local v8    # "fos_hash":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_2
.end method
