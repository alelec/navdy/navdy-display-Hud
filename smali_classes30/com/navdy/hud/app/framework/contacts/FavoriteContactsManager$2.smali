.class Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;
.super Ljava/lang/Object;
.source "FavoriteContactsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->onFavoriteContactResponse(Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

.field final synthetic val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->this$0:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 101
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v8, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v5, v8, :cond_6

    .line 102
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->this$0:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    const/4 v8, 0x1

    # setter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->isAllowedToReceiveContacts:Z
    invoke-static {v5, v8}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$202(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;Z)Z

    .line 104
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->contacts:Ljava/util/List;

    if-eqz v5, :cond_5

    .line 105
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onFavoriteContactResponse size["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    iget-object v9, v9, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->contacts:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 106
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 107
    .local v12, "seenNumbers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->contacts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/contacts/Contact;

    .line 109
    .local v2, "c":Lcom/navdy/service/library/events/contacts/Contact;
    iget-object v5, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 110
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v8, "null number"

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 145
    .end local v2    # "c":Lcom/navdy/service/library/events/contacts/Contact;
    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .end local v12    # "seenNumbers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :catch_0
    move-exception v13

    .line 146
    .local v13, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 148
    .end local v13    # "t":Ljava/lang/Throwable;
    :goto_1
    return-void

    .line 113
    .restart local v2    # "c":Lcom/navdy/service/library/events/contacts/Contact;
    .restart local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .restart local v12    # "seenNumbers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_1
    :try_start_1
    iget-object v5, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v12, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 114
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "number already seen:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_2
    iget-object v5, v2, Lcom/navdy/service/library/events/contacts/Contact;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    invoke-static {v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getNumberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v6

    .line 118
    .local v6, "numberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    iget-object v4, v2, Lcom/navdy/service/library/events/contacts/Contact;->name:Ljava/lang/String;

    .line 119
    .local v4, "displayName":Ljava/lang/String;
    iget-object v5, v2, Lcom/navdy/service/library/events/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 120
    iget-object v5, v2, Lcom/navdy/service/library/events/contacts/Contact;->name:Ljava/lang/String;

    iget-object v8, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    iget-object v9, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v9}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getPhoneNumber(Ljava/lang/String;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v5, v8, v0, v1}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isDisplayNameValid(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v5

    if-nez v5, :cond_3

    .line 121
    const/4 v4, 0x0

    .line 124
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v5

    iget-object v8, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v7

    .line 125
    .local v7, "defaultImageIndex":I
    new-instance v3, Lcom/navdy/hud/app/framework/contacts/Contact;

    iget-object v5, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    const-wide/16 v8, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/navdy/hud/app/framework/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;IJ)V

    .line 126
    .local v3, "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v5, v2, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v12, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v5

    const/16 v8, 0x1e

    if-ne v5, v8, :cond_0

    .line 130
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v8, "exceeded max size"

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 134
    .end local v2    # "c":Lcom/navdy/service/library/events/contacts/Contact;
    .end local v3    # "contact":Lcom/navdy/hud/app/framework/contacts/Contact;
    .end local v4    # "displayName":Ljava/lang/String;
    .end local v6    # "numberType":Lcom/navdy/hud/app/framework/contacts/NumberType;
    .end local v7    # "defaultImageIndex":I
    :cond_4
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "favorite contact size["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v10

    .line 136
    .local v10, "driverId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->this$0:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->fullSync:Z
    invoke-static {v5}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$400(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;)Z

    move-result v5

    invoke-static {v10, v11, v5}, Lcom/navdy/hud/app/storage/db/helper/FavoriteContactsPersistenceHelper;->storeFavoriteContacts(Ljava/lang/String;Ljava/util/List;Z)V

    goto/16 :goto_1

    .line 138
    .end local v10    # "driverId":Ljava/lang/String;
    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/contacts/Contact;>;"
    .end local v12    # "seenNumbers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_5
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v8, "fav-contact list returned is null"

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 140
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v8, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v5, v8, :cond_7

    .line 141
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->this$0:Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    const/4 v8, 0x0

    # setter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->isAllowedToReceiveContacts:Z
    invoke-static {v5, v8}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$202(Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;Z)Z

    goto/16 :goto_1

    .line 143
    :cond_7
    # getter for: Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sent fav-contact response error:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager$2;->val$response:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    iget-object v9, v9, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method
