.class public Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
.super Ljava/lang/Object;
.source "PhoneImageDownloader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;,
        Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;,
        Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;
    }
.end annotation


# static fields
.field private static final MD5_EXTENSION:Ljava/lang/String; = ".md5"

.field private static final VERBOSE:Z

.field private static final sInstance:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private contactPhotoChecked:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private contactQueue:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private volatile currentDriverProfileId:Ljava/lang/String;

.field private downloading:Z

.field private lockObj:Ljava/lang/Object;

.field private priorityComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 44
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sInstance:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$1;-><init>(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->priorityComparator:Ljava/util/Comparator;

    .line 108
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->context:Landroid/content/Context;

    .line 109
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->priorityComparator:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactQueue:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 110
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->lockObj:Ljava/lang/Object;

    .line 119
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactPhotoChecked:Ljava/util/HashSet;

    .line 122
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 124
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->download()V

    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->currentDriverProfileId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    .param p1, "x1"    # Lcom/navdy/hud/app/profile/DriverProfile;
    .param p2, "x2"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getPhotoPath(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->context:Landroid/content/Context;

    return-object v0
.end method

.method private download()V
    .locals 9

    .prologue
    .line 188
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->lockObj:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :try_start_1
    iget-object v5, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactQueue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 190
    sget-object v5, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "no request pending"

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 191
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->downloading:Z

    .line 192
    monitor-exit v6

    .line 218
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactQueue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->remove()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;

    .line 195
    .local v2, "info":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    iget v5, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->priority:I

    sget-object v7, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->NORMAL:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->getValue()I

    move-result v7

    if-ne v5, v7, :cond_1

    .line 197
    const/16 v5, 0x1f4

    invoke-static {v5}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V

    .line 200
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 201
    .local v0, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->currentDriverProfileId:Ljava/lang/String;

    .line 202
    iget-object v5, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->sourceIdentifier:Ljava/lang/String;

    iget-object v7, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {p0, v5, v7}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->hashFor(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "hash":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 205
    sget-object v5, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sending fresh download request file ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->fileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] id["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->sourceIdentifier:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 209
    :goto_1
    new-instance v3, Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v5, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->sourceIdentifier:Ljava/lang/String;

    iget-object v7, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v8, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->displayName:Ljava/lang/String;

    invoke-direct {v3, v5, v1, v7, v8}, Lcom/navdy/service/library/events/photo/PhotoRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V

    .line 210
    .local v3, "photoRequest":Lcom/navdy/service/library/events/photo/PhotoRequest;
    iget-object v5, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;

    new-instance v7, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v7, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v5, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 211
    monitor-exit v6

    goto :goto_0

    .end local v0    # "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v1    # "hash":Ljava/lang/String;
    .end local v2    # "info":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    .end local v3    # "photoRequest":Lcom/navdy/service/library/events/photo/PhotoRequest;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 212
    :catch_0
    move-exception v4

    .line 215
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 216
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V

    goto :goto_0

    .line 207
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v0    # "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .restart local v1    # "hash":Ljava/lang/String;
    .restart local v2    # "info":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
    :cond_2
    :try_start_3
    sget-object v5, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checking for update file ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->fileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] id["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->sourceIdentifier:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public static final getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sInstance:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    return-object v0
.end method

.method private getPhotoPath(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
    .locals 2
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;
    .param p2, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 324
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$5;->$SwitchMap$com$navdy$service$library$events$photo$PhotoType:[I

    invoke-virtual {p2}, Lcom/navdy/service/library/events/photo/PhotoType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 334
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 326
    :pswitch_0
    invoke-virtual {p1}, Lcom/navdy/hud/app/profile/DriverProfile;->getMusicImageDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 329
    :pswitch_1
    invoke-virtual {p1}, Lcom/navdy/hud/app/profile/DriverProfile;->getContactsImageDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 332
    :pswitch_2
    invoke-virtual {p1}, Lcom/navdy/hud/app/profile/DriverProfile;->getPreferencesDirectory()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 324
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private invokeDownload()V
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->currentDriverProfileId:Ljava/lang/String;

    .line 152
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$2;-><init>(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 158
    return-void
.end method

.method private normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 338
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    if-ne p2, v0, :cond_0

    .line 339
    const-string v0, "DriverImage"

    .line 341
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/navdy/hud/app/util/GenericUtil;->normalizeToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private purgeQueue()V
    .locals 2

    .prologue
    .line 221
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "purging queue"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->currentDriverProfileId:Ljava/lang/String;

    .line 223
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactQueue:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->downloading:Z

    .line 226
    monitor-exit v1

    .line 227
    return-void

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private removeImage(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 377
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 378
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "null param"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 392
    :goto_0
    return-void

    .line 381
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getImagePath(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v0

    .line 382
    .local v0, "file":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getInstance()Lcom/squareup/picasso/Picasso;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/picasso/Picasso;->invalidate(Ljava/io/File;)V

    .line 383
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$4;-><init>(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Ljava/io/File;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    const/4 v3, 0x7

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private storePhoto(Lcom/navdy/service/library/events/photo/PhotoResponse;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/service/library/events/photo/PhotoResponse;

    .prologue
    const/4 v4, 0x1

    .line 270
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    if-nez v0, :cond_0

    .line 271
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "photo is upto-date type["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] id["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct {v1, v2, v3, v4, v4}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 273
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V

    .line 321
    :goto_0
    return-void

    .line 276
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$3;-><init>(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Lcom/navdy/service/library/events/photo/PhotoResponse;)V

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method


# virtual methods
.method public clearAllPhotoCheckEntries()V
    .locals 3

    .prologue
    .line 370
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 371
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactPhotoChecked:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 372
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "cleared all photo check entries"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 373
    monitor-exit v1

    .line 374
    return-void

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearPhotoCheckEntry(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 360
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v0

    .line 361
    .local v0, "filename":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 362
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactPhotoChecked:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 363
    .local v1, "ret":Z
    if-eqz v1, :cond_0

    .line 364
    sget-object v2, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "photo check entry removed ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 366
    :cond_0
    monitor-exit v3

    .line 367
    return-void

    .line 366
    .end local v1    # "ret":Z
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getImagePath(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 346
    const-string v0, ""

    invoke-virtual {p0, p1, v0, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getImagePath(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getImagePath(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
    .locals 5
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "suffix"    # Ljava/lang/String;
    .param p3, "type"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 350
    invoke-direct {p0, p1, p3}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v2

    .line 351
    .local v2, "filename":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 352
    .local v0, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-direct {p0, v0, p3}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getPhotoPath(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v1

    .line 353
    .local v1, "f":Ljava/io/File;
    if-nez v1, :cond_0

    .line 354
    const/4 v3, 0x0

    .line 356
    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public hashFor(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;
    .locals 10
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 161
    const/4 v2, 0x0

    .line 162
    .local v2, "hash":Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v1

    .line 163
    .local v1, "fileName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 164
    .local v0, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-direct {p0, v0, p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getPhotoPath(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;

    move-result-object v3

    .line 165
    .local v3, "imageDir":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 166
    .local v5, "path":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_0

    .line 168
    new-instance v4, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".md5"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v3, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 169
    .local v4, "md5":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 170
    sget-object v7, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "md5 does not exist, delete file, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 171
    iget-object v7, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->context:Landroid/content/Context;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 183
    .end local v4    # "md5":Ljava/io/File;
    :cond_0
    :goto_0
    return-object v2

    .line 175
    .restart local v4    # "md5":Ljava/io/File;
    :cond_1
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 176
    :catch_0
    move-exception v6

    .line 177
    .local v6, "t":Ljava/lang/Throwable;
    sget-object v7, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "error reading md5["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    iget-object v7, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->context:Landroid/content/Context;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 179
    iget-object v7, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->context:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onConnectionStatusChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 231
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$5;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 236
    :goto_0
    return-void

    .line 233
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->purgeQueue()V

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->purgeQueue()V

    .line 241
    return-void
.end method

.method public onPhotoResponse(Lcom/navdy/service/library/events/photo/PhotoResponse;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/photo/PhotoResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 246
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "got photoResponse:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->currentDriverProfileId:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 248
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "currentDriverProfile is null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 249
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V

    .line 267
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "filename":Ljava/lang/String;
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v1, v2, :cond_1

    .line 254
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactPhotoChecked:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 255
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->storePhoto(Lcom/navdy/service/library/events/photo/PhotoResponse;)V

    goto :goto_0

    .line 257
    :cond_1
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v1, v2, :cond_2

    .line 258
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactPhotoChecked:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 260
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->removeImage(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    .line 261
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "photo not available type["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 265
    :goto_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V

    goto :goto_0

    .line 263
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "photo not available type["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] error["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public submitDownload(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "priority"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;
    .param p3, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;
    .param p4, "displayName"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "invalid id passed"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 148
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->normalizedFilename(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "filename":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->lockObj:Ljava/lang/Object;

    monitor-enter v6

    .line 133
    :try_start_0
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactPhotoChecked:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "contact photo request has already been processed name["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 135
    monitor-exit v6

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 137
    :cond_1
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "submitting photo request name["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] type ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] displayName["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 138
    iget-object v7, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->contactQueue:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 139
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->downloading:Z

    if-eqz v0, :cond_2

    .line 147
    :goto_1
    monitor-exit v6

    goto :goto_0

    .line 144
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->downloading:Z

    .line 145
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->invokeDownload()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
