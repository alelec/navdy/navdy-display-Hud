.class Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;
.super Ljava/lang/Object;
.source "ContactUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

.field final synthetic val$imageExists:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iput-boolean p2, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->val$imageExists:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 241
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$cName:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$number:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$triggerPhotoDownload:Z

    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$imageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$transformation:Lcom/squareup/picasso/Transformation;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$callback:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

    iget-object v6, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$imagePath:Ljava/io/File;

    iget-boolean v7, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->val$imageExists:Z

    iget-object v8, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;->this$0:Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;

    iget-object v8, v8, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$phoneImageDownloader:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    const/4 v9, 0x0

    # invokes: Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;ZLcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Landroid/graphics/Bitmap;)V
    invoke-static/range {v0 .. v9}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->access$000(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;ZLcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Landroid/graphics/Bitmap;)V

    .line 251
    return-void
.end method
