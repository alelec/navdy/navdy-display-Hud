.class Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;
.super Ljava/lang/Object;
.source "PhoneImageDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Info"
.end annotation


# instance fields
.field displayName:Ljava/lang/String;

.field fileName:Ljava/lang/String;

.field photoType:Lcom/navdy/service/library/events/photo/PhotoType;

.field priority:I

.field sourceIdentifier:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "priority"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;
    .param p3, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;
    .param p4, "sourceIdentifier"    # Ljava/lang/String;
    .param p5, "displayName"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->fileName:Ljava/lang/String;

    .line 81
    invoke-virtual {p2}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->getValue()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->priority:I

    .line 82
    iput-object p3, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 83
    iput-object p4, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->sourceIdentifier:Ljava/lang/String;

    .line 84
    iput-object p5, p0, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Info;->displayName:Ljava/lang/String;

    .line 85
    return-void
.end method
