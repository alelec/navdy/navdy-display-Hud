.class public Lcom/navdy/hud/app/framework/contacts/Contact;
.super Ljava/lang/Object;
.source "Contact.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/hud/app/framework/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public defaultImageIndex:I

.field public firstName:Ljava/lang/String;

.field public formattedNumber:Ljava/lang/String;

.field public initials:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

.field public numberTypeStr:Ljava/lang/String;

.field public numericNumber:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/contacts/Contact;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/Contact;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 108
    new-instance v0, Lcom/navdy/hud/app/framework/contacts/Contact$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/contacts/Contact$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/contacts/Contact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 69
    .local v0, "i":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 70
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/NumberType;->values()[Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v1

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 72
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numericNumber:J

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->firstName:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->initials:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/contacts/Contact;)V
    .locals 2
    .param p1, "contact"    # Lcom/navdy/service/library/events/contacts/Contact;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 58
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    .line 59
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    invoke-static {v0}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getNumberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 60
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 61
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 62
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/contacts/Contact;->init(J)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/contacts/PhoneNumber;)V
    .locals 2
    .param p1, "phoneNumber"    # Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    .line 50
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    invoke-static {v0}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getNumberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 51
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 52
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;->customType:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 53
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/framework/contacts/Contact;->init(J)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;IJ)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "numberType"    # Lcom/navdy/hud/app/framework/contacts/NumberType;
    .param p4, "defaultImageIndex"    # I
    .param p5, "numericNumber"    # J

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    .line 43
    iput p4, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    .line 44
    iput-wide p5, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numericNumber:J

    .line 45
    invoke-direct {p0, p5, p6}, Lcom/navdy/hud/app/framework/contacts/Contact;->init(J)V

    .line 46
    return-void
.end method

.method private init(J)V
    .locals 7
    .param p1, "numericNumber"    # J

    .prologue
    .line 81
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 82
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getFirstName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->firstName:Ljava/lang/String;

    .line 83
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->initials:Ljava/lang/String;

    .line 85
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 86
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getPhoneType(Lcom/navdy/hud/app/framework/contacts/NumberType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    .line 88
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    .line 89
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 90
    .local v0, "currentLocale":Ljava/util/Locale;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 91
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-lez v3, :cond_3

    .line 92
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numericNumber:J

    .line 102
    :cond_2
    :goto_0
    return-void

    .line 95
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v1

    .line 96
    .local v1, "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v1}, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;->getNationalNumber()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numericNumber:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 97
    .end local v1    # "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :catch_0
    move-exception v2

    .line 98
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/framework/contacts/Contact;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->number:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/contacts/NumberType;->ordinal()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->defaultImageIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numericNumber:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 132
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->firstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->initials:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->numberTypeStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/navdy/hud/app/framework/contacts/Contact;->formattedNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    return-void

    .line 129
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
