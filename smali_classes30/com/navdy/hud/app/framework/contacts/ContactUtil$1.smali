.class final Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;
.super Ljava/lang/Object;
.source "ContactUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cName:Ljava/lang/String;

.field final synthetic val$callback:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

.field final synthetic val$imagePath:Ljava/io/File;

.field final synthetic val$imageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field final synthetic val$number:Ljava/lang/String;

.field final synthetic val$phoneImageDownloader:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

.field final synthetic val$transformation:Lcom/squareup/picasso/Transformation;

.field final synthetic val$triggerPhotoDownload:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$callback:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$imagePath:Ljava/io/File;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$cName:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$number:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$triggerPhotoDownload:Z

    iput-object p6, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$imageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object p7, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$transformation:Lcom/squareup/picasso/Transformation;

    iput-object p8, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$phoneImageDownloader:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 234
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$callback:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;->isContextValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;->val$imagePath:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    .line 238
    .local v0, "imageExists":Z
    # getter for: Lcom/navdy/hud/app/framework/contacts/ContactUtil;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->access$100()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/framework/contacts/ContactUtil$1$1;-><init>(Lcom/navdy/hud/app/framework/contacts/ContactUtil$1;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
