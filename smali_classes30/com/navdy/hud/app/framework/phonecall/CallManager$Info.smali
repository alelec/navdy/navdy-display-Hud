.class Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;
.super Ljava/lang/Object;
.source "CallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/phonecall/CallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Info"
.end annotation


# instance fields
.field answered:Z

.field callMuted:Z

.field callStartMs:J

.field callUUID:Ljava/lang/String;

.field contact:Ljava/lang/String;

.field duration:J

.field incomingCall:Z

.field normalizedNumber:Ljava/lang/String;

.field number:Ljava/lang/String;

.field putOnStack:Z

.field userCancelledCall:Z

.field userRejectedCall:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJLjava/lang/String;JZZZZ)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "normalizedNumber"    # Ljava/lang/String;
    .param p3, "contact"    # Ljava/lang/String;
    .param p4, "incomingCall"    # Z
    .param p5, "answered"    # Z
    .param p6, "callStartMs"    # J
    .param p8, "callUUID"    # Ljava/lang/String;
    .param p9, "duration"    # J
    .param p11, "userRejectedCall"    # Z
    .param p12, "userCancelledCall"    # Z
    .param p13, "putOnStack"    # Z
    .param p14, "callMuted"    # Z

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->number:Ljava/lang/String;

    .line 146
    iput-object p2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->normalizedNumber:Ljava/lang/String;

    .line 147
    iput-object p3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->contact:Ljava/lang/String;

    .line 148
    iput-boolean p4, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->incomingCall:Z

    .line 149
    iput-boolean p5, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->answered:Z

    .line 150
    iput-wide p6, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->callStartMs:J

    .line 151
    iput-object p8, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->callUUID:Ljava/lang/String;

    .line 152
    iput-wide p9, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->duration:J

    .line 153
    iput-boolean p11, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->userRejectedCall:Z

    .line 154
    iput-boolean p12, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->userCancelledCall:Z

    .line 155
    iput-boolean p13, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->putOnStack:Z

    .line 156
    iput-boolean p14, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->callMuted:Z

    .line 157
    return-void
.end method
