.class Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;
.super Ljava/lang/Object;
.source "CallNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/toast/IToastCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/phonecall/CallNotification;->showIncomingCallToast()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field cb:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

.field final synthetic this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

.field toastImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field toastView:Lcom/navdy/hud/app/view/ToastView;

.field final synthetic val$caller:Ljava/lang/String;

.field final synthetic val$number:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/phonecall/CallNotification;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 796
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->val$caller:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->val$number:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3$1;-><init>(Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->cb:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

    return-void
.end method


# virtual methods
.method public executeChoiceItem(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 834
    packed-switch p2, :pswitch_data_0

    .line 849
    :goto_0
    return-void

    .line 836
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->RINGING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-ne v0, v1, :cond_0

    .line 837
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_ACCEPT:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V

    .line 839
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const-string v1, "incomingcall#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    goto :goto_0

    .line 843
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->RINGING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-ne v0, v1, :cond_1

    .line 844
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_REJECT:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V

    .line 846
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const-string v1, "incomingcall#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    goto :goto_0

    .line 834
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 829
    const/4 v0, 0x0

    return v0
.end method

.method public onPhotoDownload(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 853
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastView:Lcom/navdy/hud/app/view/ToastView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    if-nez v0, :cond_1

    .line 866
    :cond_0
    :goto_0
    return-void

    .line 856
    :cond_1
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->alreadyDownloaded:Z

    if-nez v0, :cond_0

    .line 859
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .line 860
    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eq v0, v1, :cond_0

    .line 862
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->val$number:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->sourceIdentifier:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 863
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->val$caller:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->val$number:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/squareup/picasso/Transformation;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->cb:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto :goto_0
.end method

.method public onStart(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 6
    .param p1, "view"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 813
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 814
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getMainLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 815
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 816
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 817
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->val$caller:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->val$number:Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/squareup/picasso/Transformation;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->cb:Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    .line 818
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 822
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/squareup/otto/Bus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 823
    iput-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 824
    iput-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;->toastImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 825
    return-void
.end method
