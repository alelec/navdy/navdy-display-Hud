.class final enum Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
.super Ljava/lang/Enum;
.source "CallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/phonecall/CallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CallNotificationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum CANCELLED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum FAILED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum MISSED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum REJECTED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field public static final enum RINGING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 51
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "RINGING"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->RINGING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 52
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "MISSED"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->MISSED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 53
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 54
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->FAILED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 55
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "DIALING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 56
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "IN_PROGRESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 57
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "REJECTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->REJECTED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 58
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    const-string v1, "CANCELLED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->CANCELLED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 49
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->RINGING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->MISSED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->FAILED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->REJECTED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->CANCELLED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->$VALUES:[Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->$VALUES:[Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    return-object v0
.end method
