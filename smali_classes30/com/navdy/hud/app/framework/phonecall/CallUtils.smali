.class public Lcom/navdy/hud/app/framework/phonecall/CallUtils;
.super Ljava/lang/Object;
.source "CallUtils.java"


# static fields
.field private static callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized isPhoneCallInProgress()Z
    .locals 2

    .prologue
    .line 12
    const-class v1, Lcom/navdy/hud/app/framework/phonecall/CallUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallUtils;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    if-nez v0, :cond_0

    .line 13
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getCallManager()Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallUtils;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .line 15
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallUtils;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isCallInProgress()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 12
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
