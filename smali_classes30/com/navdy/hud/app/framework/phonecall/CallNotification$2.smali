.class Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;
.super Ljava/lang/Object;
.source "CallNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/phonecall/CallNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 5
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 205
    iget v0, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 268
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 215
    :sswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-eq v0, v1, :cond_1

    .line 217
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V

    goto :goto_0

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_MUTE:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$700(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callMuted:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$600(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$800(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0200f2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 222
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1100(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$900(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1000(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 223
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1100(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$900(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 228
    :sswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-eq v0, v1, :cond_2

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V

    goto :goto_0

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_END:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V

    goto :goto_0

    .line 237
    :sswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-eq v0, v1, :cond_3

    .line 239
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V

    goto/16 :goto_0

    .line 241
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_UNMUTE:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$700(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$800(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0200f1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 244
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1100(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1200(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1000(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 245
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1100(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1200(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 250
    :sswitch_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-ne v0, v1, :cond_4

    .line 251
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_END:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 252
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-eq v0, v1, :cond_0

    .line 253
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V

    goto/16 :goto_0

    .line 259
    :sswitch_5
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V

    goto/16 :goto_0

    .line 263
    :sswitch_6
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1300(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1300(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$1300(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    invoke-interface {v0, v3, v3}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->collapseNotification(ZZ)V

    goto/16 :goto_0

    .line 205
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_6
        0x65 -> :sswitch_0
        0x66 -> :sswitch_0
        0x67 -> :sswitch_1
        0x68 -> :sswitch_3
        0x69 -> :sswitch_4
        0x6a -> :sswitch_2
        0x6b -> :sswitch_5
    .end sparse-switch
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 271
    return-void
.end method
