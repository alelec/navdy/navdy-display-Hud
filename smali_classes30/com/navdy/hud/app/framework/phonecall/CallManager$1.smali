.class Lcom/navdy/hud/app/framework/phonecall/CallManager$1;
.super Ljava/lang/Object;
.source "CallManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/phonecall/CallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/phonecall/CallManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no response for call action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->lastCallAction:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->lastCallAction:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-nez v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->lastCallAction:Lcom/navdy/service/library/events/callcontrol/CallAction;

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallManager;->isCallActionResponseRequired(Lcom/navdy/service/library/events/callcontrol/CallAction;)Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->access$100(Lcom/navdy/hud/app/framework/phonecall/CallManager;Lcom/navdy/service/library/events/callcontrol/CallAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$3;->$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction:[I

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->lastCallAction:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/callcontrol/CallAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 104
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iput-boolean v3, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iput-boolean v3, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iput-boolean v3, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->dialSet:Z

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->access$200(Lcom/navdy/hud/app/framework/phonecall/CallManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendNotification()V

    goto :goto_0

    .line 91
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->FAILED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    iput-object v1, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    goto :goto_1

    .line 96
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    iput-object v1, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    goto :goto_1

    .line 101
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->FAILED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    iput-object v1, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    goto :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
