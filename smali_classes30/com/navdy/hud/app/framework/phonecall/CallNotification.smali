.class public Lcom/navdy/hud/app/framework/phonecall/CallNotification;
.super Ljava/lang/Object;
.source "CallNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;


# static fields
.field public static final CALL_NOTIFICATION_TOAST_ID:Ljava/lang/String; = "incomingcall#toast"

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final END_NOTIFICATION_TIMEOUT:I = 0xbb8

.field private static final FAILED_NOTIFICATION_TIMEOUT:I = 0x2710

.field private static final IMAGE_SCALE:F = 0.5f

.field private static final IN_CALL_NOTIFICATION_TIMEOUT:I = 0x7530

.field private static final MISSED_NOTIFICATION_TIMEOUT:I = 0x2710

.field private static final TAG_ACCEPT:I = 0x65

.field private static final TAG_CANCEL:I = 0x69

.field private static final TAG_DISMISS:I = 0x6b

.field private static final TAG_END:I = 0x6a

.field private static final TAG_IGNORE:I = 0x66

.field private static final TAG_MUTE:I = 0x67

.field private static final TAG_UNMUTE:I = 0x68

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final activeCall:Ljava/lang/String;

.field private activeCallChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private activeCallMutedChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private activeCallNoMuteChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private bus:Lcom/squareup/otto/Bus;

.field private final callAccept:Ljava/lang/String;

.field private final callCancel:Ljava/lang/String;

.field private final callDialing:Ljava/lang/String;

.field private final callDismiss:Ljava/lang/String;

.field private final callEnd:Ljava/lang/String;

.field private final callEnded:Ljava/lang/String;

.field private final callFailed:Ljava/lang/String;

.field private final callIgnore:Ljava/lang/String;

.field private callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

.field private final callMissed:Ljava/lang/String;

.field private final callMute:Ljava/lang/String;

.field private final callMuted:Ljava/lang/String;

.field private callStatusImage:Landroid/widget/ImageView;

.field private final callUnmute:Ljava/lang/String;

.field private callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field private callerName:Landroid/widget/TextView;

.field private callerStatus:Landroid/widget/TextView;

.field private final callerUnknown:Ljava/lang/String;

.field private choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

.field private choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

.field private container:Landroid/view/ViewGroup;

.field private contentWidth:I

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private dialingCallChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private final dismisColor:I

.field private durationRunnable:Ljava/lang/Runnable;

.field private final endColor:I

.field private endedCallChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private extendedContainer:Landroid/view/ViewGroup;

.field private extendedTitle:Landroid/widget/TextView;

.field private failedCallChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private final incomingCall:Ljava/lang/String;

.field private incomingCallChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private missedCallChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private final muteColor:I

.field private resources:Landroid/content/res/Resources;

.field private roundTransformation:Lcom/squareup/picasso/Transformation;

.field private final unmuteColor:I

.field private final unselectedColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/framework/phonecall/CallManager;Lcom/squareup/otto/Bus;)V
    .locals 12
    .param p1, "callManager"    # Lcom/navdy/hud/app/framework/phonecall/CallManager;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    const/16 v11, 0x6a

    const/4 v2, 0x2

    const v10, 0x7f020125

    const/4 v1, 0x1

    const v9, 0x7f020126

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->incomingCallChoices:Ljava/util/ArrayList;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallNoMuteChoices:Ljava/util/ArrayList;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dialingCallChoices:Ljava/util/ArrayList;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->failedCallChoices:Ljava/util/ArrayList;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endedCallChoices:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->missedCallChoices:Ljava/util/ArrayList;

    .line 130
    new-instance v0, Lcom/makeramen/RoundedTransformationBuilder;

    invoke-direct {v0}, Lcom/makeramen/RoundedTransformationBuilder;-><init>()V

    invoke-virtual {v0, v1}, Lcom/makeramen/RoundedTransformationBuilder;->oval(Z)Lcom/makeramen/RoundedTransformationBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/makeramen/RoundedTransformationBuilder;->build()Lcom/squareup/picasso/Transformation;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 141
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->handler:Landroid/os/Handler;

    .line 143
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;-><init>(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->durationRunnable:Ljava/lang/Runnable;

    .line 202
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification$2;-><init>(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 155
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .line 156
    iput-object p2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->bus:Lcom/squareup/otto/Bus;

    .line 158
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callEnded:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callDialing:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callMuted:Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerUnknown:Ljava/lang/String;

    .line 164
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callAccept:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callIgnore:Ljava/lang/String;

    .line 166
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callMute:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callEnd:Ljava/lang/String;

    .line 168
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callUnmute:Ljava/lang/String;

    .line 169
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callCancel:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callDismiss:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callFailed:Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callMissed:Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->incomingCall:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCall:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->muteColor:I

    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unmuteColor:I

    .line 178
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    .line 179
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0d0021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismisColor:I

    .line 180
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    .line 182
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0b018d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->contentWidth:I

    .line 184
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->incomingCallChoices:Ljava/util/ArrayList;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callAccept:Ljava/lang/String;

    const/16 v3, 0x65

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->incomingCallChoices:Ljava/util/ArrayList;

    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callIgnore:Ljava/lang/String;

    const/16 v3, 0x66

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v1, 0x67

    const v2, 0x7f020127

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->muteColor:I

    const v4, 0x7f020127

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callMute:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->muteColor:I

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callEnd:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    move v1, v11

    move v2, v9

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallNoMuteChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callEnd:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    move v1, v11

    move v2, v9

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v1, 0x68

    const v2, 0x7f02012d

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unmuteColor:I

    const v4, 0x7f02012d

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callUnmute:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unmuteColor:I

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callEnd:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    move v1, v11

    move v2, v9

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dialingCallChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v1, 0x69

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callEnd:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endColor:I

    move v2, v9

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->failedCallChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v1, 0x6b

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismisColor:I

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callDismiss:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismisColor:I

    move v2, v10

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endedCallChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v1, 0x6b

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismisColor:I

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callDismiss:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismisColor:I

    move v2, v10

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    iget-object v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->missedCallChoices:Ljava/util/ArrayList;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v1, 0x6b

    iget v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismisColor:I

    iget v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->unselectedColor:I

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callDismiss:Ljava/lang/String;

    iget v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismisColor:I

    move v2, v10

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/phonecall/CallNotification;Landroid/widget/TextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCallDuration(Landroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/squareup/picasso/Transformation;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->durationRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Lcom/navdy/hud/app/framework/phonecall/CallManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callMuted:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;

    return-object v0
.end method

.method private checkIfCollapseRequired()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 897
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->isAlive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 898
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 899
    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "collapse required"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 900
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1, v0, v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->collapseNotification(ZZ)V

    .line 901
    const/4 v0, 0x1

    .line 904
    :cond_0
    return v0
.end method

.method public static clearToast()V
    .locals 2

    .prologue
    .line 875
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 876
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v1, "incomingcall#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 877
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 879
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 880
    return-void
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 705
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#phone#call#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 706
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    iput-object v1, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 707
    return-void
.end method

.method private getCallerNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    return-object v0
.end method

.method private getTimeout(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)I
    .locals 4
    .param p1, "state"    # Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .prologue
    const/16 v1, 0x2710

    const/4 v0, 0x0

    .line 733
    if-nez p1, :cond_0

    .line 751
    :goto_0
    return v0

    .line 736
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallNotification$4;->$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 738
    goto :goto_0

    .line 741
    :pswitch_2
    const/16 v0, 0xbb8

    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 744
    goto :goto_0

    .line 748
    :pswitch_4
    const/16 v0, 0x7530

    goto :goto_0

    .line 736
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private isTimeoutRequired(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)Z
    .locals 4
    .param p1, "state"    # Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 710
    if-nez p1, :cond_1

    .line 728
    :cond_0
    :goto_0
    return v0

    .line 713
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallNotification$4;->$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 718
    goto :goto_0

    .line 721
    :pswitch_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    .line 724
    goto :goto_0

    .line 713
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setCallDuration(Landroid/widget/TextView;)V
    .locals 9
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 883
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-wide v2, v4, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    .line 884
    .local v2, "start":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    .line 885
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCall:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 894
    :goto_0
    return-void

    .line 887
    :cond_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    .line 888
    .local v0, "minute":J
    const-wide/16 v4, 0x1

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    .line 889
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCall:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 891
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->resources:Landroid/content/res/Resources;

    const v5, 0x7f0901f8

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setCaller()Z
    .locals 3

    .prologue
    .line 680
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getCaller()Ljava/lang/String;

    move-result-object v0

    .line 681
    .local v0, "text":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 682
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerName:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 683
    const/4 v1, 0x1

    .line 686
    :goto_0
    return v1

    .line 685
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 686
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private switchToExpandedMode()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 468
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v5}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->stopTimeout(Z)V

    .line 473
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 474
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 476
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v5}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->expandNotification(Z)V

    goto :goto_0
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x1

    return v0
.end method

.method public expandNotification()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 450
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_1

    .line 460
    :cond_0
    :goto_0
    return v1

    .line 454
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 455
    .local v0, "tag":Ljava/lang/Object;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;

    if-eq v0, v2, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;

    if-eq v0, v2, :cond_2

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallNoMuteChoices:Ljava/util/ArrayList;

    if-ne v0, v2, :cond_0

    .line 457
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->switchToExpandedMode()V

    .line 458
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCaller()Ljava/lang/String;
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 696
    :goto_0
    return-object v0

    .line 693
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    goto :goto_0

    .line 696
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerUnknown:Ljava/lang/String;

    goto :goto_0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 397
    sget v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorPhoneCall:I

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 299
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v1, p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    .line 300
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    const v2, 0x7f0e0088

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedTitle:Landroid/widget/TextView;

    .line 301
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedTitle:Landroid/widget/TextView;

    const v2, 0x7f0c0019

    invoke-virtual {v1, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 303
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    const v2, 0x7f0e0089

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 304
    .local v0, "textView":Landroid/widget/TextView;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 306
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedTitle:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCallDuration(Landroid/widget/TextView;)V

    .line 307
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedTitle:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->durationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 310
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->durationRunnable:Ljava/lang/Runnable;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 311
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    return-object v1
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    const-string v0, "navdy#phone#call#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 276
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->PHONE_CALL:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 287
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    .line 288
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e015a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerName:Landroid/widget/TextView;

    .line 289
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e015b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    .line 290
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e015c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e015d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 11
    .param p1, "viewIn"    # Z

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 433
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 434
    .local v3, "set":Landroid/animation/AnimatorSet;
    if-nez p1, :cond_0

    .line 435
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerName:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_0

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 436
    .local v0, "o1":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 437
    .local v1, "o2":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_2

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 438
    .local v2, "o3":Landroid/animation/ObjectAnimator;
    new-array v4, v10, [Landroid/animation/Animator;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    aput-object v2, v4, v7

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 445
    :goto_0
    return-object v3

    .line 440
    .end local v0    # "o1":Landroid/animation/ObjectAnimator;
    .end local v1    # "o2":Landroid/animation/ObjectAnimator;
    .end local v2    # "o3":Landroid/animation/ObjectAnimator;
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerName:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_3

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 441
    .restart local v0    # "o1":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_4

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 442
    .restart local v1    # "o2":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_5

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 443
    .restart local v2    # "o3":Landroid/animation/ObjectAnimator;
    new-array v4, v10, [Landroid/animation/Animator;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    aput-object v2, v4, v7

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_0

    .line 435
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 436
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 437
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 440
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 441
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 442
    :array_5
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public isAlive()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 366
    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallNotification$4;->$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState:[I

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 381
    :goto_0
    :pswitch_0
    return v0

    .line 378
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public isContextValid()Z
    .locals 1

    .prologue
    .line 757
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 483
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 1
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 421
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p1, v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 423
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->updateState()V

    .line 426
    :cond_0
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 429
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 491
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 496
    const/4 v0, 0x0

    .line 497
    .local v0, "handled":Z
    const/4 v1, 0x0

    .line 498
    .local v1, "ret":Z
    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallNotification$4;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 514
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 516
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->isTimeoutRequired(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 517
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getTimeout(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)I

    move-result v2

    if-lez v2, :cond_0

    .line 518
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 520
    :cond_0
    return v1

    .line 500
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    move-result v0

    .line 501
    const/4 v1, 0x1

    .line 502
    goto :goto_0

    .line 505
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    move-result v0

    .line 506
    const/4 v1, 0x1

    .line 507
    goto :goto_0

    .line 510
    :pswitch_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    .line 511
    const/4 v1, 0x1

    goto :goto_0

    .line 498
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 5
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    const/4 v4, 0x1

    .line 402
    sget-object v1, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p1, v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 404
    .local v0, "callState":Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->isTimeoutRequired(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 405
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v1, :cond_0

    .line 406
    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set timeout:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1, v4}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->stopTimeout(Z)V

    .line 408
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getTimeout(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->startTimeout(I)V

    .line 417
    .end local v0    # "callState":Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
    :cond_0
    :goto_0
    return-void

    .line 411
    .restart local v0    # "callState":Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v1, :cond_0

    .line 412
    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reset timeout:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 413
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1, v4}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->stopTimeout(Z)V

    goto :goto_0
.end method

.method public onPhotoDownload(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 666
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->alreadyDownloaded:Z

    if-eqz v0, :cond_1

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 669
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eq v0, v1, :cond_0

    .line 672
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->sourceIdentifier:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto :goto_0
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 3
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 321
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 322
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 323
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->updateState()V

    .line 325
    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTranslationX(F)V

    .line 327
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 328
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 329
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 330
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 331
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    .line 333
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 342
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->durationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 343
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 344
    iput-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 345
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-eqz v1, :cond_0

    .line 346
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clear()V

    .line 349
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 350
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 351
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_1

    .line 352
    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 354
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    invoke-static {v1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    .line 355
    iput-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedContainer:Landroid/view/ViewGroup;

    .line 357
    .end local v0    # "parent":Landroid/view/ViewGroup;
    :cond_2
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 487
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->updateState()V

    .line 338
    return-void
.end method

.method public showIncomingCallToast()V
    .locals 11

    .prologue
    const v4, 0x7f0c0009

    const/4 v5, 0x1

    .line 761
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getCaller()Ljava/lang/String;

    move-result-object v7

    .line 762
    .local v7, "caller":Ljava/lang/String;
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getCallerNumber()Ljava/lang/String;

    move-result-object v8

    .line 763
    .local v8, "n":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 764
    const/4 v8, 0x0

    .line 766
    :cond_0
    move-object v9, v8

    .line 768
    .local v9, "number":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 770
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "1"

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->incomingCall:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    const-string v0, "1_1"

    const v1, 0x7f0c0010

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 773
    const-string v0, "11"

    const v1, 0x7f0200f1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 775
    const-string v0, "20"

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->incomingCallChoices:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 776
    const-string v0, "19"

    invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 778
    const-string v0, "16"

    iget v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->contentWidth:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 780
    if-eqz v9, :cond_1

    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 781
    const-string v0, "4"

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    const-string v0, "5"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 784
    const-string v0, "6"

    invoke-static {v9}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    const-string v0, "7"

    const v1, 0x7f0c0011

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 796
    :goto_0
    new-instance v3, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;

    invoke-direct {v3, p0, v7, v9}, Lcom/navdy/hud/app/framework/phonecall/CallNotification$3;-><init>(Lcom/navdy/hud/app/framework/phonecall/CallNotification;Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    .local v3, "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v10

    .line 870
    .local v10, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->clearToast()V

    .line 871
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "incomingcall#toast"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZZ)V

    invoke-virtual {v10, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 872
    return-void

    .line 787
    .end local v3    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .end local v10    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_1
    invoke-static {v7}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 788
    const-string v0, "4"

    invoke-static {v9}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :goto_1
    const-string v0, "5"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 790
    :cond_2
    const-string v0, "4"

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x0

    return v0
.end method

.method updateState()V
    .locals 14

    .prologue
    const v4, 0x7f0200de

    const/4 v13, 0x1

    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 529
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v9, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 530
    .local v9, "callState":Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "call state:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 532
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->checkIfCollapseRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 537
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$4;->$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState:[I

    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 649
    :cond_2
    :goto_1
    :pswitch_0
    invoke-direct {p0, v9}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->isTimeoutRequired(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 650
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 651
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set timeout:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 652
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v13}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->stopTimeout(Z)V

    .line 653
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-direct {p0, v9}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getTimeout(Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->startTimeout(I)V

    goto :goto_0

    .line 540
    :pswitch_1
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#phone#call#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isCurrentNotificationId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V

    goto :goto_1

    .line 547
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCaller()Z

    .line 548
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callMissed:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 549
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 550
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->missedCallChoices:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 551
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->missedCallChoices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 552
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 553
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 554
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto :goto_1

    .line 560
    :pswitch_3
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V

    goto :goto_1

    .line 566
    :pswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dismissNotification()V

    goto/16 :goto_1

    .line 571
    :pswitch_5
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCaller()Z

    .line 572
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callEnded:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 573
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endedCallChoices:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 575
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->endedCallChoices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 576
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 577
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 578
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto/16 :goto_1

    .line 587
    :pswitch_6
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCaller()Z

    .line 588
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callDialing:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 589
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v3, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v4, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move v5, v13

    move-object v8, p0

    invoke-static/range {v3 .. v8}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    .line 591
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dialingCallChoices:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 592
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->dialingCallChoices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 593
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    goto/16 :goto_1

    .line 598
    :pswitch_7
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCaller()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 599
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 606
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    const v1, 0x7f0200dd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 607
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    .line 610
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 611
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 612
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 630
    :goto_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    goto/16 :goto_1

    .line 601
    :cond_4
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getCallerNumber()Ljava/lang/String;

    move-result-object v11

    .line 602
    .local v11, "number":Ljava/lang/String;
    invoke-static {v11}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 603
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    invoke-static {v11}, Lcom/navdy/hud/app/util/PhoneUtil;->formatPhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 614
    .end local v11    # "number":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v10

    .line 615
    .local v10, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v10, :cond_6

    iget-object v0, v10, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v0, v1, :cond_7

    .line 617
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallNoMuteChoices:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 618
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallNoMuteChoices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    goto :goto_3

    .line 620
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callMuted:Z

    if-nez v0, :cond_8

    .line 621
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 622
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallChoices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    goto :goto_3

    .line 624
    :cond_8
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 625
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->activeCallMutedChoices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    goto :goto_3

    .line 635
    .end local v10    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :pswitch_8
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCaller()Z

    .line 636
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callFailed:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 637
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 638
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->failedCallChoices:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 639
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->failedCallChoices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 640
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    const v1, 0x7f0200f3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 641
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 642
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callManager:Lcom/navdy/hud/app/framework/phonecall/CallManager;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->callerImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->setContactPhoto(Ljava/lang/String;Ljava/lang/String;ZLcom/navdy/hud/app/ui/component/image/InitialsImageView;Lcom/squareup/picasso/Transformation;Lcom/navdy/hud/app/framework/contacts/ContactUtil$IContactCallback;)V

    goto/16 :goto_1

    .line 656
    :cond_9
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 657
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset timeout:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 658
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v13}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->stopTimeout(Z)V

    goto/16 :goto_0

    .line 537
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
