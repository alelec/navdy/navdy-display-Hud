.class Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;
.super Ljava/lang/Object;
.source "CallNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/phonecall/CallNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 147
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->extendedTitle:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$000(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->setCallDuration(Landroid/widget/TextView;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$100(Lcom/navdy/hud/app/framework/phonecall/CallNotification;Landroid/widget/TextView;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$300(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->durationRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$200(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/lang/Runnable;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 151
    return-void

    .line 149
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$300(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallNotification$1;->this$0:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    # getter for: Lcom/navdy/hud/app/framework/phonecall/CallNotification;->durationRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->access$200(Lcom/navdy/hud/app/framework/phonecall/CallNotification;)Ljava/lang/Runnable;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v0
.end method
