.class public Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;
.super Ljava/lang/Object;
.source "PhoneBatteryNotification.java"


# static fields
.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final ID_BATTERY_EXTREMELY_LOW:Ljava/lang/String; = "phone-bat-exlow"

.field private static final ID_BATTERY_LOW:Ljava/lang/String; = "phone-bat-low"

.field private static final ID_NO_NETWORK:Ljava/lang/String; = "phone-no-network"

.field private static final PHONE_BATTERY_TIMEOUT:I = 0x7d0

.field private static final PHONE_BATTERY_TIMEOUT_EXTREMELY_LOW:I = 0x4e20

.field private static final PHONE_NO_NETWORK_TIMEOUT:I = 0x7d0

.field private static final noNetwork:Ljava/lang/String;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final unknown:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 38
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->resources:Landroid/content/res/Resources;

    .line 39
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090215

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->unknown:Ljava/lang/String;

    .line 40
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->noNetwork:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static dismissAllBatteryToasts()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 98
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 99
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "phone-bat-low"

    aput-object v2, v1, v3

    const-string v2, "phone-bat-exlow"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast([Ljava/lang/String;)V

    .line 100
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "phone-bat-low"

    aput-object v2, v1, v3

    const-string v2, "phone-bat-exlow"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast([Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public static dismissAllToasts()V
    .locals 2

    .prologue
    .line 104
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->dismissAllBatteryToasts()V

    .line 105
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 106
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v1, "phone-no-network"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 107
    const-string v1, "phone-no-network"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public static showBatteryToast(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V
    .locals 13
    .param p0, "phoneBatteryStatus"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 44
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[PhoneBatteryStatus] status["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] charging["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] level["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    sget-object v3, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    if-eq v0, v3, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 47
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "phone battery is ok/charging"

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 48
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->dismissAllBatteryToasts()V

    .line 95
    :cond_1
    :goto_0
    return-void

    .line 52
    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v2, "bundle":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 55
    .local v7, "level":Ljava/lang/String;
    const-string v8, ""

    .line 56
    .local v8, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 57
    .local v6, "icon":I
    const/4 v1, 0x0

    .line 58
    .local v1, "id":Ljava/lang/String;
    const/4 v10, 0x0

    .line 60
    .local v10, "tts":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v9

    .line 62
    .local v9, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification$1;->$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus:[I

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_2
    const-string v0, "8"

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 87
    const-string v0, "4"

    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v0, "5"

    const v3, 0x7f0c0009

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    const-string v0, "17"

    invoke-virtual {v2, v0, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {v9}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 93
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v11

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const/4 v3, 0x0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v11, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    goto :goto_0

    .line 53
    .end local v1    # "id":Ljava/lang/String;
    .end local v6    # "icon":I
    .end local v7    # "level":Ljava/lang/String;
    .end local v8    # "title":Ljava/lang/String;
    .end local v9    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    .end local v10    # "tts":Ljava/lang/String;
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->unknown:Ljava/lang/String;

    goto :goto_1

    .line 64
    .restart local v1    # "id":Ljava/lang/String;
    .restart local v6    # "icon":I
    .restart local v7    # "level":Ljava/lang/String;
    .restart local v8    # "title":Ljava/lang/String;
    .restart local v9    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    .restart local v10    # "tts":Ljava/lang/String;
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901fb

    new-array v5, v11, [Ljava/lang/Object;

    aput-object v7, v5, v4

    invoke-virtual {v0, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 65
    const v6, 0x7f020210

    .line 66
    const-string v0, "13"

    const/16 v3, 0x4e20

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    const-string v1, "phone-bat-exlow"

    .line 68
    const-string v0, "12"

    invoke-virtual {v2, v0, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 69
    sget-object v10, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_PHONE_BATTERY_VERY_LOW:Ljava/lang/String;

    .line 70
    const-string v0, "phone-bat-low"

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 71
    new-array v0, v12, [Ljava/lang/String;

    const-string v3, "phone-bat-low"

    aput-object v3, v0, v4

    const-string v3, "phone-bat-exlow"

    aput-object v3, v0, v11

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast([Ljava/lang/String;)V

    goto :goto_2

    .line 75
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901fc

    new-array v5, v11, [Ljava/lang/Object;

    aput-object v7, v5, v4

    invoke-virtual {v0, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 76
    const v6, 0x7f02020f

    .line 77
    const-string v0, "13"

    const/16 v3, 0x7d0

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    const-string v1, "phone-bat-low"

    .line 79
    sget-object v10, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_PHONE_BATTERY_LOW:Ljava/lang/String;

    .line 80
    const-string v0, "phone-bat-exlow"

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 81
    new-array v0, v12, [Ljava/lang/String;

    const-string v3, "phone-bat-low"

    aput-object v3, v0, v4

    const-string v3, "phone-bat-exlow"

    aput-object v3, v0, v11

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
