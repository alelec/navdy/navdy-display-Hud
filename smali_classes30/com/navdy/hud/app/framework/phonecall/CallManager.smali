.class public Lcom/navdy/hud/app/framework/phonecall/CallManager;
.super Ljava/lang/Object;
.source "CallManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;,
        Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;,
        Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;,
        Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;
    }
.end annotation


# static fields
.field private static final CALL_ACCEPTED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;

.field private static final CALL_ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;

.field private static final NO_RESPONSE_THRESHOLD:I = 0x2710

.field private static final PHONE_STATUS_REQUEST:Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field answered:Z

.field private bus:Lcom/squareup/otto/Bus;

.field callMuted:Z

.field private callNotification:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

.field callStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;",
            ">;"
        }
    .end annotation
.end field

.field callStartMs:J

.field callUUID:Ljava/lang/String;

.field contact:Ljava/lang/String;

.field dialSet:Z

.field duration:J

.field private handler:Landroid/os/Handler;

.field incomingCall:Z

.field lastCallAction:Lcom/navdy/service/library/events/callcontrol/CallAction;

.field private noResponseCheckRunnable:Ljava/lang/Runnable;

.field normalizedNumber:Ljava/lang/String;

.field number:Ljava/lang/String;

.field phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field putOnStack:Z

.field state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

.field userCancelledCall:Z

.field userRejectedCall:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/phonecall/CallManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 64
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->CALL_ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;

    .line 65
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->CALL_ACCEPTED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;

    .line 74
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;

    invoke-direct {v0}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->PHONE_STATUS_REQUEST:Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Landroid/content/Context;)V
    .locals 4
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 78
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager$1;-><init>(Lcom/navdy/hud/app/framework/phonecall/CallManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->noResponseCheckRunnable:Ljava/lang/Runnable;

    .line 114
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->handler:Landroid/os/Handler;

    .line 117
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 118
    iput-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    .line 119
    iput-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    .line 120
    iput-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 121
    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    .line 122
    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    .line 123
    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->putOnStack:Z

    .line 174
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStack:Ljava/util/Stack;

    .line 178
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    .line 179
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 180
    new-instance v0, Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;-><init>(Lcom/navdy/hud/app/framework/phonecall/CallManager;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callNotification:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    .line 181
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/phonecall/CallManager;Lcom/navdy/service/library/events/callcontrol/CallAction;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/callcontrol/CallAction;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isCallActionResponseRequired(Lcom/navdy/service/library/events/callcontrol/CallAction;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/phonecall/CallManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->PHONE_STATUS_REQUEST:Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/phonecall/CallManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/phonecall/CallManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method private isCallActionResponseRequired(Lcom/navdy/service/library/events/callcontrol/CallAction;)Z
    .locals 3
    .param p1, "callAction"    # Lcom/navdy/service/library/events/callcontrol/CallAction;

    .prologue
    const/4 v0, 0x0

    .line 515
    if-nez p1, :cond_0

    .line 525
    :goto_0
    return v0

    .line 518
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$3;->$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/callcontrol/CallAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 522
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 518
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isIos()Z
    .locals 4

    .prologue
    .line 490
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    .line 491
    .local v1, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v0

    .line 492
    .local v0, "deviceConnected":Z
    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v2

    .line 493
    .local v2, "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :goto_0
    sget-object v3, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->equals(Ljava/lang/Object;)Z

    move-result v3

    return v3

    .line 492
    .end local v2    # "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isNotificationAllowed()Z
    .locals 1

    .prologue
    .line 550
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    if-eqz v0, :cond_1

    .line 551
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isPhoneNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isPhoneNotificationsEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553
    const/4 v0, 0x1

    .line 557
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toSeconds(J)J
    .locals 5
    .param p1, "ms"    # J

    .prologue
    .line 375
    const-wide/16 v0, 0x1f4

    add-long/2addr v0, p1

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public clearCallStack()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 371
    return-void
.end method

.method public dial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "callUUID"    # Ljava/lang/String;
    .param p3, "contact"    # Ljava/lang/String;

    .prologue
    .line 465
    if-nez p1, :cond_0

    .line 466
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "null number passed contact["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 478
    :goto_0
    return-void

    .line 469
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 470
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->dialSet:Z

    .line 472
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    .line 473
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    .line 474
    invoke-static {p1}, Lcom/navdy/hud/app/util/PhoneUtil;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    .line 475
    iput-object p3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 476
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendNotification()V

    .line 477
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_DIAL:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {p0, v0, p1}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disconnect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 540
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "got disconnect event, setting to IDLE"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 541
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 542
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 543
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    .line 544
    iput-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    .line 545
    iput-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    .line 546
    iput-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 547
    return-void
.end method

.method public getCurrentCallDuration()I
    .locals 4

    .prologue
    .line 562
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 563
    const/4 v0, -0x1

    .line 565
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public getPhoneStatus()Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    return-object v0
.end method

.method public isCallInProgress()Z
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-ne v0, v1, :cond_1

    .line 533
    :cond_0
    const/4 v0, 0x1

    .line 535
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDeviceInfoAvailable(Lcom/navdy/hud/app/event/DeviceInfoAvailable;)V
    .locals 3
    .param p1, "deviceInfoAvailable"    # Lcom/navdy/hud/app/event/DeviceInfoAvailable;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 484
    iget-object v0, p1, Lcom/navdy/hud/app/event/DeviceInfoAvailable;->deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/event/DeviceInfoAvailable;->deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    iget-object v0, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v0, v1, :cond_0

    .line 485
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;

    invoke-direct {v2}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;-><init>()V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 487
    :cond_0
    return-void
.end method

.method public onPhoneBatteryEvent(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 510
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Battery-phone] status["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] level["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] charging["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 511
    invoke-static {p1}, Lcom/navdy/hud/app/framework/phonecall/PhoneBatteryNotification;->showBatteryToast(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V

    .line 512
    return-void
.end method

.method public onPhoneEvent(Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V
    .locals 25
    .param p1, "event"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 185
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->noResponseCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 186
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->lastCallAction:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 188
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-ne v5, v6, :cond_0

    .line 190
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->dialSet:Z

    if-eqz v5, :cond_2

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sget-object v6, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-ne v5, v6, :cond_2

    .line 191
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->dialSet:Z

    .line 199
    :cond_0
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "phoneEvent:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 201
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v19

    .line 202
    .local v19, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    move-object/from16 v20, v0

    .line 203
    .local v20, "lastStatus":Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 204
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    .line 206
    .local v22, "nowMs":J
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$3;->$SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus:[I

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v6}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 366
    :cond_1
    :goto_0
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "state:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 367
    .end local v19    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v20    # "lastStatus":Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    .end local v22    # "nowMs":J
    :goto_1
    return-void

    .line 193
    :cond_2
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/util/PhoneUtil;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    .line 208
    .restart local v19    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .restart local v20    # "lastStatus":Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    .restart local v22    # "nowMs":J
    :pswitch_0
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IDLE: putOnStack="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->putOnStack:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " lastStatus="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 209
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callUUID:Ljava/lang/String;

    .line 210
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callMuted:Z

    .line 211
    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    move-object/from16 v0, v20

    if-eq v0, v5, :cond_3

    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    move-object/from16 v0, v20

    if-ne v0, v5, :cond_b

    .line 213
    :cond_3
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->duration:J

    .line 214
    const/16 v24, 0x0

    .line 215
    .local v24, "rejected":Z
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    if-eqz v5, :cond_5

    .line 216
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->REJECTED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 217
    const/16 v24, 0x1

    .line 225
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->clearToast()V

    .line 226
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    .line 227
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z

    .line 228
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->putOnStack:Z

    if-eqz v5, :cond_8

    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->removeNotification()V

    .line 261
    .end local v24    # "rejected":Z
    :cond_4
    :goto_3
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->putOnStack:Z

    .line 262
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v6, Lcom/navdy/hud/app/framework/phonecall/CallManager;->CALL_ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallEnded;

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 263
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v5

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 264
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/service/library/events/callcontrol/CallEvent;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->toSeconds(J)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->duration:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-direct/range {v4 .. v10}, Lcom/navdy/service/library/events/callcontrol/CallEvent;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    invoke-virtual {v11, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 218
    .restart local v24    # "rejected":Z
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z

    if-eqz v5, :cond_6

    .line 219
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->CANCELLED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    goto :goto_2

    .line 220
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    if-eqz v5, :cond_7

    .line 221
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    goto/16 :goto_2

    .line 223
    :cond_7
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->MISSED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    goto/16 :goto_2

    .line 230
    :cond_8
    if-nez v24, :cond_4

    .line 231
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getCurrentNotification()Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v21

    .line 232
    .local v21, "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    if-eqz v21, :cond_9

    invoke-interface/range {v21 .. v21}, Lcom/navdy/hud/app/framework/notifications/INotification;->getId()Ljava/lang/String;

    move-result-object v5

    const-string v6, "navdy#phone#call#notif"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 233
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->removeNotification()V

    goto/16 :goto_3

    .line 235
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 236
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendNotification()V

    goto/16 :goto_3

    .line 240
    .end local v21    # "notification":Lcom/navdy/hud/app/framework/notifications/INotification;
    .end local v24    # "rejected":Z
    :cond_b
    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    move-object/from16 v0, v20

    if-ne v0, v5, :cond_4

    .line 241
    if-eqz v19, :cond_c

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v6, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v5, v6, :cond_c

    .line 242
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/util/PhoneUtil;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 243
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Received end for a different call than the current one"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 249
    :cond_c
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    sub-long v6, v22, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->toSeconds(J)J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->duration:J

    .line 250
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->ENDED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 251
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    if-nez v5, :cond_d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z

    if-nez v5, :cond_d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->putOnStack:Z

    if-eqz v5, :cond_f

    .line 252
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->removeNotification()V

    .line 258
    :cond_e
    :goto_4
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    .line 259
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z

    goto/16 :goto_3

    .line 254
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 255
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendNotification()V

    goto :goto_4

    .line 269
    :pswitch_1
    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    move-object/from16 v0, v20

    if-ne v0, v5, :cond_10

    .line 270
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "we are in a call"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 271
    if-eqz v19, :cond_12

    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v6, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v5, v6, :cond_12

    .line 272
    new-instance v4, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callUUID:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->duration:J

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z

    move/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callMuted:Z

    move/from16 v18, v0

    invoke-direct/range {v4 .. v18}, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJLjava/lang/String;JZZZZ)V

    .line 285
    .local v4, "info":Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStack:Ljava/util/Stack;

    invoke-virtual {v5, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "stack pushed ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 292
    .end local v4    # "info":Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;
    :cond_10
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    .line 293
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    .line 294
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    .line 295
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    .line 296
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/util/PhoneUtil;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    .line 297
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 298
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callMuted:Z

    .line 299
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 300
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callUUID:Ljava/lang/String;

    .line 303
    :cond_11
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->RINGING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 305
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 306
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callNotification:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->showIncomingCallToast()V

    goto/16 :goto_0

    .line 288
    :cond_12
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "stack not implemented on android"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 308
    :cond_13
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "incoming phone call disabled"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 314
    :pswitch_2
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    .line 315
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    .line 316
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    .line 317
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    .line 318
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/util/PhoneUtil;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    .line 319
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 320
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 321
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callUUID:Ljava/lang/String;

    .line 324
    :cond_14
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 325
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 326
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendNotification()V

    goto/16 :goto_0

    .line 332
    :pswitch_3
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->clearToast()V

    .line 333
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    .line 334
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    .line 335
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 336
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callUUID:Ljava/lang/String;

    .line 338
    :cond_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 339
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/util/PhoneUtil;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    .line 341
    :cond_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 342
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 344
    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 345
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    .line 348
    :cond_18
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 349
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 350
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendNotification()V

    .line 351
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v6, Lcom/navdy/hud/app/framework/phonecall/CallManager;->CALL_ACCEPTED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallAccepted;

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 356
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v6, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-ne v5, v6, :cond_1

    .line 357
    sget-object v5, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->FAILED:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    .line 358
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isNotificationAllowed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 359
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sendNotification()V

    goto/16 :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onPhoneStatusResponse(Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 498
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPhoneStatusResponse ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;->callStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 499
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;->callStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IDLE:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-ne v0, v1, :cond_1

    .line 502
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onPhoneStatusResponse fwding event"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 503
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;->callStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->onPhoneEvent(Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V

    .line 506
    :cond_1
    return-void
.end method

.method removeNotification()V
    .locals 2

    .prologue
    .line 457
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callNotification:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/phonecall/CallNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 458
    return-void
.end method

.method restoreInfo(Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;)V
    .locals 10
    .param p1, "info"    # Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;

    .prologue
    .line 421
    iget-object v7, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;

    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    iget-wide v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    invoke-direct {p0, v8, v9}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-wide v8, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->duration:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/callcontrol/CallEvent;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    invoke-virtual {v7, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 423
    iget-object v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    .line 424
    iget-object v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->normalizedNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    .line 425
    iget-object v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->contact:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    .line 426
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->incomingCall:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->incomingCall:Z

    .line 427
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->answered:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    .line 428
    iget-wide v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->callStartMs:J

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStartMs:J

    .line 429
    iget-object v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->callUUID:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callUUID:Ljava/lang/String;

    .line 430
    iget-wide v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->duration:J

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->duration:J

    .line 431
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->userRejectedCall:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    .line 432
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->userCancelledCall:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z

    .line 433
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->putOnStack:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->putOnStack:Z

    .line 434
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;->callMuted:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callMuted:Z

    .line 435
    sget-object v0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stack restored ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->contact:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->number:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->normalizedNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 438
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isIos()Z

    move-result v0

    if-nez v0, :cond_1

    .line 439
    iget-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/phonecall/CallManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/phonecall/CallManager$2;-><init>(Lcom/navdy/hud/app/framework/phonecall/CallManager;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->answered:Z

    if-eqz v0, :cond_0

    .line 447
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->phoneStatus:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    goto :goto_0
.end method

.method sendCallAction(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;)V
    .locals 6
    .param p1, "callAction"    # Lcom/navdy/service/library/events/callcontrol/CallAction;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    .line 380
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v3, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->RINGING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-ne v2, v3, :cond_3

    sget-object v2, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_REJECT:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-ne p1, v2, :cond_3

    .line 382
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userRejectedCall:Z

    .line 389
    :cond_0
    :goto_0
    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallManager$3;->$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/callcontrol/CallAction;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 399
    :goto_1
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->isCallActionResponseRequired(Lcom/navdy/service/library/events/callcontrol/CallAction;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 400
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->noResponseCheckRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 401
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->noResponseCheckRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 403
    :cond_1
    iput-object p1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->lastCallAction:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 404
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v4, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callUUID:Ljava/lang/String;

    invoke-direct {v4, p1, p2, v5}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;-><init>(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 406
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v0

    .line 407
    .local v0, "size":I
    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "call stack size="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " action="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/navdy/service/library/events/callcontrol/CallAction;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 408
    if-lez v0, :cond_2

    .line 409
    sget-object v2, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_REJECT:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-ne p1, v2, :cond_5

    .line 410
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->restoreInfo(Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;)V

    .line 418
    .end local v0    # "size":I
    :cond_2
    :goto_2
    return-void

    .line 383
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v3, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->DIALING:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->state:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    sget-object v3, Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;->IN_PROGRESS:Lcom/navdy/hud/app/framework/phonecall/CallManager$CallNotificationState;

    if-ne v2, v3, :cond_0

    :cond_4
    sget-object v2, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_END:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-ne p1, v2, :cond_0

    .line 386
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->userCancelledCall:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 415
    :catch_0
    move-exception v1

    .line 416
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/framework/phonecall/CallManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 391
    .end local v1    # "t":Ljava/lang/Throwable;
    :pswitch_0
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callMuted:Z

    goto/16 :goto_1

    .line 395
    :pswitch_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callMuted:Z

    goto/16 :goto_1

    .line 411
    .restart local v0    # "size":I
    :cond_5
    sget-object v2, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_END:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-ne p1, v2, :cond_2

    .line 412
    iget-object v2, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/framework/phonecall/CallManager;->restoreInfo(Lcom/navdy/hud/app/framework/phonecall/CallManager$Info;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method sendNotification()V
    .locals 2

    .prologue
    .line 453
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/phonecall/CallManager;->callNotification:Lcom/navdy/hud/app/framework/phonecall/CallNotification;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 454
    return-void
.end method
