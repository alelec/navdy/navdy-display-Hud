.class public Lcom/navdy/hud/app/framework/DriverProfileHelper;
.super Ljava/lang/Object;
.source "DriverProfileHelper.java"


# static fields
.field private static final sInstance:Lcom/navdy/hud/app/framework/DriverProfileHelper;


# instance fields
.field driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/hud/app/framework/DriverProfileHelper;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/DriverProfileHelper;->sInstance:Lcom/navdy/hud/app/framework/DriverProfileHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 24
    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/framework/DriverProfileHelper;->sInstance:Lcom/navdy/hud/app/framework/DriverProfileHelper;

    return-object v0
.end method


# virtual methods
.method public getCurrentLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/framework/DriverProfileHelper;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/framework/DriverProfileHelper;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    return-object v0
.end method

.method public getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/hud/app/framework/DriverProfileHelper;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    return-object v0
.end method
