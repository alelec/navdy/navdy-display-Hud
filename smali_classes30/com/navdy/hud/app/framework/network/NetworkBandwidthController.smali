.class public Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;
.super Ljava/lang/Object;
.source "NetworkBandwidthController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;,
        Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;,
        Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;
    }
.end annotation


# static fields
.field private static final ACTIVE:I

.field private static final BW_SETTING_CHANGED:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;

.field private static final DATA:Ljava/lang/String; = "data"

.field private static final DATA_COLLECTION_INITIAL_INTERVAL:I

.field private static final DATA_COLLECTION_INTERVAL:I

.field private static final DATA_COLLECTION_INTERVAL_ENG:I

.field private static final EVENT:Ljava/lang/String; = "event"

.field private static final EVENT_DNS_INFO:Ljava/lang/String; = "dnslookup"

.field private static final EVENT_NETSTAT:Ljava/lang/String; = "netstat"

.field private static final FD:Ljava/lang/String; = "fd"

.field private static final FROM:Ljava/lang/String; = "from"

.field private static final GOOGLE_DNS_IP:Ljava/lang/String; = "8.8.4.4"

.field private static final HERE_ROUTE_END_POINT_PATTERN:Ljava/lang/String; = "route.hybrid.api.here.com"

.field private static final HOST:Ljava/lang/String; = "host"

.field private static final IP:Ljava/lang/String; = "ip"

.field private static final NETWORK_STAT_INFO_PORT:I = 0x5c67

.field private static final PERM_DISABLE_NETWORK:Ljava/lang/String; = "persist.sys.perm_disable_nw"

.field private static final RX:Ljava/lang/String; = "rx"

.field private static final TO:Ljava/lang/String; = "to"

.field private static final TX:Ljava/lang/String; = "tx"

.field private static final networkDisabled:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private final componentInfoMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;",
            "Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private dataCollectionRunnable:Ljava/lang/Runnable;

.field private dataCollectionRunnableBk:Ljava/lang/Runnable;

.field private dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

.field private handler:Landroid/os/Handler;

.field private limitBandwidthModeOn:Z

.field private networkStatRunnable:Ljava/lang/Runnable;

.field private networkStatThread:Ljava/lang/Thread;

.field private statCache:Lcom/navdy/hud/app/framework/network/NetworkStatCache;

.field private trafficDataDownloadedOnce:Z

.field private final urlToComponentMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "NetworkBandwidthControl"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 65
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->ACTIVE:I

    .line 70
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->BW_SETTING_CHANGED:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;

    .line 92
    const-string v0, "persist.sys.perm_disable_nw"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkDisabled:Z

    .line 93
    sget-boolean v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkDisabled:Z

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "n/w disabled permanently"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 108
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x19

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INITIAL_INTERVAL:I

    .line 109
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL_ENG:I

    .line 110
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL:I

    .line 129
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->singleton:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    .line 105
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    .line 135
    new-instance v1, Lcom/navdy/hud/app/framework/network/DnsCache;

    invoke-direct {v1}, Lcom/navdy/hud/app/framework/network/DnsCache;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

    .line 136
    new-instance v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;-><init>(Lcom/navdy/hud/app/framework/network/DnsCache;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->statCache:Lcom/navdy/hud/app/framework/network/NetworkStatCache;

    .line 139
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handler:Landroid/os/Handler;

    .line 146
    new-instance v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkStatRunnable:Ljava/lang/Runnable;

    .line 252
    new-instance v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$2;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dataCollectionRunnable:Ljava/lang/Runnable;

    .line 259
    new-instance v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dataCollectionRunnableBk:Ljava/lang/Runnable;

    .line 273
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->bus:Lcom/squareup/otto/Bus;

    .line 276
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "v102-62-30-8.route.hybrid.api.here.com"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_ROUTE:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "tpeg.traffic.api.here.com"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "tpeg.hybrid.api.here.com"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "download.hybrid.api.here.com"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_MAP_DOWNLOAD:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "reverse.geocoder.api.here.com"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_REVERSE_GEO:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "analytics.localytics.com"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->LOCALYTICS:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "profile.localytics.com"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->LOCALYTICS:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "sdk.hockeyapp.net"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HOCKEY:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    const-string v2, "navdyhud.atlassian.net"

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->JIRA:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->LOCALYTICS:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_ROUTE:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_MAP_DOWNLOAD:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_REVERSE_GEO:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HOCKEY:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->JIRA:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;-><init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkStatRunnable:Ljava/lang/Runnable;

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkStatThread:Ljava/lang/Thread;

    .line 296
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkStatThread:Ljava/lang/Thread;

    const-string v2, "hudNetStatThread"

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 297
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkStatThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 298
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "networkStat thread started"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 299
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dataCollectionRunnable:Ljava/lang/Runnable;

    sget v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INITIAL_INTERVAL:I

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 301
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    .line 302
    .local v0, "profileManager":Lcom/navdy/hud/app/profile/DriverProfileManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->isLimitBandwidthModeOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->limitBandwidthModeOn:Z

    .line 304
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 305
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "registered bus"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 306
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Lcom/navdy/hud/app/framework/network/DnsCache;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Lcom/navdy/hud/app/framework/network/NetworkStatCache;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->statCache:Lcom/navdy/hud/app/framework/network/NetworkStatCache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->trafficDataDownloadedOnce:Z

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->trafficDataDownloadedOnce:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dataCollectionRunnableBk:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL:I

    return v0
.end method

.method static synthetic access$800()I
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL_ENG:I

    return v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->singleton:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    return-object v0
.end method

.method private handleBandwidthPreferenceChange()V
    .locals 5

    .prologue
    .line 405
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    .line 406
    .local v0, "profileManager":Lcom/navdy/hud/app/profile/DriverProfileManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->isLimitBandwidthModeOn()Z

    move-result v1

    .line 407
    .local v1, "val":Z
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->limitBandwidthModeOn:Z

    if-ne v1, v2, :cond_0

    .line 409
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "limitbandwidth: no-op current["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->limitBandwidthModeOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] new ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 425
    :goto_0
    return-void

    .line 412
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "limitbandwidth: changed current["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->limitBandwidthModeOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] new ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 414
    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->limitBandwidthModeOn:Z

    .line 416
    if-eqz v1, :cond_1

    .line 417
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "limitbandwidth: on"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 422
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->handleBandwidthPreferenceChange()V

    .line 423
    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->BW_SETTING_CHANGED:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 419
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "limitbandwidth: off"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private isComponentActive(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)Z
    .locals 7
    .param p1, "component"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getLastActivityTime(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)J

    move-result-wide v2

    .line 364
    .local v2, "l":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 365
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v0, v4, v2

    .line 366
    .local v0, "diff":J
    sget v4, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->ACTIVE:I

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-gtz v4, :cond_0

    .line 367
    sget-object v4, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "component is ACTIVE:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " diff:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 368
    const/4 v4, 0x1

    .line 371
    .end local v0    # "diff":J
    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getBootStat()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->statCache:Lcom/navdy/hud/app/framework/network/NetworkStatCache;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->getBootStat()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLastActivityTime(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)J
    .locals 4
    .param p1, "component"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .prologue
    .line 314
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    .line 315
    .local v0, "componentInfo":Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;
    if-nez v0, :cond_0

    .line 316
    const-wide/16 v2, 0x0

    .line 319
    :goto_0
    return-wide v2

    .line 318
    :cond_0
    monitor-enter v0

    .line 319
    :try_start_0
    iget-wide v2, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;->lastActivity:J

    monitor-exit v0

    goto :goto_0

    .line 320
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getSessionStat()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->statCache:Lcom/navdy/hud/app/framework/network/NetworkStatCache;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->getSessionStat()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isLimitBandwidthModeOn()Z
    .locals 1

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->limitBandwidthModeOn:Z

    return v0
.end method

.method public isNetworkAccessAllowed(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)Z
    .locals 5
    .param p1, "component"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 328
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 329
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "n/w access: not connected to network:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 358
    :goto_0
    return v1

    .line 332
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$4;->$SwitchMap$com$navdy$hud$app$framework$network$NetworkBandwidthController$Component:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 357
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "n/w access allowed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move v1, v2

    .line 358
    goto :goto_0

    .line 341
    :pswitch_0
    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_ROUTE:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isComponentActive(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)Z

    move-result v0

    .line 342
    .local v0, "isActive":Z
    if-eqz v0, :cond_1

    .line 343
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "n/w access not allowed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 347
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isComponentActive(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)Z

    move-result v0

    .line 348
    if-eqz v0, :cond_2

    .line 349
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "n/w access not allowed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 353
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "n/w access allowed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move v1, v2

    .line 354
    goto/16 :goto_0

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isNetworkDisabled()Z
    .locals 1

    .prologue
    .line 383
    sget-boolean v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->networkDisabled:Z

    return v0
.end method

.method public isTrafficDataDownloadedOnce()Z
    .locals 1

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->trafficDataDownloadedOnce:Z

    return v0
.end method

.method public netStat()V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dataCollectionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 310
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dataCollectionRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 311
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 2
    .param p1, "profileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 392
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "driver profile changed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 393
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handleBandwidthPreferenceChange()V

    .line 394
    return-void
.end method

.method public onDriverProfileUpdated(Lcom/navdy/hud/app/event/DriverProfileUpdated;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileUpdated;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 398
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "driver profile updated"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 399
    iget-object v0, p1, Lcom/navdy/hud/app/event/DriverProfileUpdated;->state:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    sget-object v1, Lcom/navdy/hud/app/event/DriverProfileUpdated$State;->UPDATED:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    if-ne v0, v1, :cond_0

    .line 400
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handleBandwidthPreferenceChange()V

    .line 402
    :cond_0
    return-void
.end method
