.class public Lcom/navdy/hud/app/framework/network/NetworkStatCache;
.super Ljava/lang/Object;
.source "NetworkStatCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    }
.end annotation


# instance fields
.field private dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

.field private networkStatMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;"
        }
    .end annotation
.end field

.field private networkStatMapBoot:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/network/DnsCache;)V
    .locals 1
    .param p1, "dnsCache"    # Lcom/navdy/hud/app/framework/network/DnsCache;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMapBoot:Ljava/util/HashMap;

    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

    .line 44
    return-void
.end method

.method private dumpBootStat(Lcom/navdy/service/library/log/Logger;)V
    .locals 5
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;

    .prologue
    .line 106
    iget-object v3, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMapBoot:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 121
    :cond_0
    return-void

    .line 111
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMapBoot:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 112
    .local v1, "bootStatIterstor":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dump-boot total endpoints:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMapBoot:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 113
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    .line 115
    .local v0, "bootStat":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

    iget-object v4, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->destIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/framework/network/DnsCache;->getHostnamefromIP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "host":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 117
    iget-object v2, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->destIP:Ljava/lang/String;

    .line 119
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dump-boot  dest["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] tx["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] rx["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private dumpSessionStat(Lcom/navdy/service/library/log/Logger;Z)V
    .locals 6
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "clearAll"    # Z

    .prologue
    .line 84
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 90
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dump-session total connections:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 91
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 92
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 93
    .local v0, "fd":I
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    .line 94
    .local v2, "info":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

    iget-object v5, v2, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->destIP:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/framework/network/DnsCache;->getHostnamefromIP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "host":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 96
    iget-object v1, v2, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->destIP:Ljava/lang/String;

    .line 98
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "dump-session fd["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] dest["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] tx["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] rx["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 100
    .end local v0    # "fd":I
    .end local v1    # "host":Ljava/lang/String;
    .end local v2    # "info":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    :cond_3
    if-eqz p2, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->clear()V

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized addStat(Ljava/lang/String;III)V
    .locals 5
    .param p1, "destIP"    # Ljava/lang/String;
    .param p2, "txBytes"    # I
    .param p3, "rxBytes"    # I
    .param p4, "fd"    # I

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    .line 52
    .local v1, "stat":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    if-nez v1, :cond_0

    .line 53
    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v4, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    invoke-direct {v4, p2, p3, p1}, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;-><init>(IILjava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :goto_0
    if-nez p1, :cond_1

    .line 71
    :goto_1
    monitor-exit p0

    return-void

    .line 55
    :cond_0
    :try_start_1
    iget v2, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    add-int/2addr v2, p2

    iput v2, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    .line 56
    iget v2, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I

    add-int/2addr v2, p3

    iput v2, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 51
    .end local v1    # "stat":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 64
    .restart local v1    # "stat":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMapBoot:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    .line 65
    .local v0, "bootstat":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    if-nez v0, :cond_2

    .line 66
    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMapBoot:Ljava/util/HashMap;

    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    invoke-direct {v3, p2, p3, p1}, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;-><init>(IILjava/lang/String;)V

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 68
    :cond_2
    iget v2, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    add-int/2addr v2, p2

    iput v2, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    .line 69
    iget v2, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I

    add-int/2addr v2, p3

    iput v2, v0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized dump(Lcom/navdy/service/library/log/Logger;Z)V
    .locals 1
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "clearAll"    # Z

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->dumpSessionStat(Lcom/navdy/service/library/log/Logger;Z)V

    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->dumpBootStat(Lcom/navdy/service/library/log/Logger;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    monitor-exit p0

    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getBootStat()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMapBoot:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->getStat(Ljava/util/Iterator;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSessionStat()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->networkStatMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->getStat(Ljava/util/Iterator;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getStat(Ljava/util/Iterator;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;>;"
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;>;"
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    .line 135
    .local v1, "info":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;

    iget-object v4, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->destIP:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/framework/network/DnsCache;->getHostnamefromIP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "host":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 137
    iget-object v0, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->destIP:Ljava/lang/String;

    .line 139
    :cond_0
    new-instance v3, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;

    iget v4, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    iget v5, v1, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I

    invoke-direct {v3, v4, v5, v0}, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;-><init>(IILjava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 132
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "info":Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 141
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;>;"
    :cond_1
    monitor-exit p0

    return-object v2
.end method
