.class public Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;
.super Ljava/lang/Object;
.source "NetworkStatCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/network/NetworkStatCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NetworkStatCacheInfo"
.end annotation


# instance fields
.field public destIP:Ljava/lang/String;

.field public rxBytes:I

.field public txBytes:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1, "txBytes"    # I
    .param p2, "rxBytes"    # I
    .param p3, "destIP"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->txBytes:I

    .line 22
    iput p2, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->rxBytes:I

    .line 23
    iput-object p3, p0, Lcom/navdy/hud/app/framework/network/NetworkStatCache$NetworkStatCacheInfo;->destIP:Ljava/lang/String;

    .line 24
    return-void
.end method
