.class Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;
.super Ljava/lang/Object;
.source "NetworkBandwidthController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 263
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->statCache:Lcom/navdy/hud/app/framework/network/NetworkStatCache;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$200(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Lcom/navdy/hud/app/framework/network/NetworkStatCache;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->dump(Lcom/navdy/service/library/log/Logger;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$900(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v2

    if-eqz v2, :cond_0

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$700()I

    move-result v2

    int-to-long v2, v2

    :goto_0
    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 269
    :goto_1
    return-void

    .line 267
    :cond_0
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL_ENG:I
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$800()I

    move-result v2

    int-to-long v2, v2

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_1
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267
    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$900(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v2

    if-eqz v2, :cond_1

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$700()I

    move-result v2

    int-to-long v2, v2

    :goto_2
    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_1
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL_ENG:I
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$800()I

    move-result v2

    int-to-long v2, v2

    goto :goto_2

    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$3;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$900(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Landroid/os/Handler;

    move-result-object v4

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v2

    if-eqz v2, :cond_2

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$700()I

    move-result v2

    int-to-long v2, v2

    :goto_3
    invoke-virtual {v4, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v1

    :cond_2
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->DATA_COLLECTION_INTERVAL_ENG:I
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$800()I

    move-result v2

    int-to-long v2, v2

    goto :goto_3
.end method
