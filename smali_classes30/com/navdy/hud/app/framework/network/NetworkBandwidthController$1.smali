.class Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;
.super Ljava/lang/Object;
.source "NetworkBandwidthController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 33

    .prologue
    .line 149
    const/16 v24, 0x0

    .line 151
    .local v24, "socket":Ljava/net/DatagramSocket;
    :try_start_0
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    const-string v31, "networkStat thread enter"

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 152
    new-instance v25, Ljava/net/DatagramSocket;

    const/16 v30, 0x5c67

    const-string v31, "127.0.0.1"

    invoke-static/range {v31 .. v31}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v31

    move-object/from16 v0, v25

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Ljava/net/DatagramSocket;-><init>(ILjava/net/InetAddress;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 153
    .end local v24    # "socket":Ljava/net/DatagramSocket;
    .local v25, "socket":Ljava/net/DatagramSocket;
    const/16 v30, 0x4000

    :try_start_1
    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setReceiveBufferSize(I)V

    .line 154
    const/16 v30, 0x800

    move/from16 v0, v30

    new-array v6, v0, [B

    .line 155
    .local v6, "data":[B
    new-instance v22, Ljava/net/DatagramPacket;

    array-length v0, v6

    move/from16 v30, v0

    move-object/from16 v0, v22

    move/from16 v1, v30

    invoke-direct {v0, v6, v1}, Ljava/net/DatagramPacket;-><init>([BI)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 158
    .local v22, "packet":Ljava/net/DatagramPacket;
    :cond_0
    :goto_0
    :try_start_2
    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 159
    invoke-virtual/range {v22 .. v22}, Ljava/net/DatagramPacket;->getLength()I

    move-result v18

    .line 160
    .local v18, "len":I
    new-instance v26, Ljava/lang/String;

    const/16 v30, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v30

    move/from16 v2, v18

    invoke-direct {v0, v6, v1, v2}, Ljava/lang/String;-><init>([BII)V

    .line 161
    .local v26, "str":Ljava/lang/String;
    new-instance v17, Lorg/json/JSONObject;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 162
    .local v17, "jsonObject":Lorg/json/JSONObject;
    const-string v30, "event"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 163
    .local v10, "event":Ljava/lang/String;
    const/16 v30, -0x1

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v31

    sparse-switch v31, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v30, :pswitch_data_0

    .line 233
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "invalid command:"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 237
    .end local v10    # "event":Ljava/lang/String;
    .end local v17    # "jsonObject":Lorg/json/JSONObject;
    .end local v18    # "len":I
    .end local v26    # "str":Ljava/lang/String;
    :catch_0
    move-exception v27

    .line 238
    .local v27, "t":Ljava/lang/Throwable;
    :try_start_3
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    const-string v31, "[networkStat]"

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 241
    .end local v6    # "data":[B
    .end local v22    # "packet":Ljava/net/DatagramPacket;
    .end local v27    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v27

    move-object/from16 v24, v25

    .line 242
    .end local v25    # "socket":Ljava/net/DatagramSocket;
    .restart local v24    # "socket":Ljava/net/DatagramSocket;
    .restart local v27    # "t":Ljava/lang/Throwable;
    :goto_2
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 245
    if-eqz v24, :cond_2

    .line 246
    invoke-static/range {v24 .. v24}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 248
    :cond_2
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    const-string v31, "networkStat thread exit"

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 249
    return-void

    .line 163
    .end local v24    # "socket":Ljava/net/DatagramSocket;
    .end local v27    # "t":Ljava/lang/Throwable;
    .restart local v6    # "data":[B
    .restart local v10    # "event":Ljava/lang/String;
    .restart local v17    # "jsonObject":Lorg/json/JSONObject;
    .restart local v18    # "len":I
    .restart local v22    # "packet":Ljava/net/DatagramPacket;
    .restart local v25    # "socket":Ljava/net/DatagramSocket;
    .restart local v26    # "str":Ljava/lang/String;
    :sswitch_0
    :try_start_4
    const-string v31, "dnslookup"

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_1

    const/16 v30, 0x0

    goto :goto_1

    :sswitch_1
    const-string v31, "netstat"

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_1

    const/16 v30, 0x1

    goto :goto_1

    .line 165
    :pswitch_0
    const-string v30, "host"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 166
    .local v13, "host":Ljava/lang/String;
    const-string v30, "localhost"

    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_0

    .line 169
    const-string v30, "ip"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 170
    .local v16, "ips":Lorg/json/JSONArray;
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->length()I

    move-result v19

    .line 171
    .local v19, "nIps":I
    if-eqz v19, :cond_0

    .line 174
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_3
    move/from16 v0, v19

    if-ge v14, v0, :cond_0

    .line 175
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 176
    .local v15, "ip":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$100(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Lcom/navdy/hud/app/framework/network/DnsCache;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v15, v13}, Lcom/navdy/hud/app/framework/network/DnsCache;->addEntry(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    const/16 v31, 0x2

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 178
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "dnslookup "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " : "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 174
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 184
    .end local v13    # "host":Ljava/lang/String;
    .end local v14    # "i":I
    .end local v15    # "ip":Ljava/lang/String;
    .end local v16    # "ips":Lorg/json/JSONArray;
    .end local v19    # "nIps":I
    :pswitch_1
    const-string v30, "data"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v21

    .line 185
    .local v21, "netStatData":Lorg/json/JSONArray;
    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v20

    .line 186
    .local v20, "nStreams":I
    if-eqz v20, :cond_0

    .line 189
    const/16 v30, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 190
    .local v7, "dataObj":Lorg/json/JSONObject;
    const-string v30, "from"

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 191
    .local v12, "from":Ljava/lang/String;
    const-string v30, "to"

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 192
    .local v28, "to":Ljava/lang/String;
    const-string v30, "tx"

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v29

    .line 193
    .local v29, "tx":I
    const-string v30, "rx"

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v23

    .line 194
    .local v23, "rx":I
    const-string v30, "fd"

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 195
    .local v11, "fd":I
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    const/16 v31, 0x2

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 196
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "] tx["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "] rx["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "] from["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "] to["

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "]"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 199
    :cond_4
    move-object/from16 v8, v28

    .line 200
    .local v8, "destIP":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 201
    move-object v8, v12

    .line 204
    :cond_5
    const-string v30, "8.8.4.4"

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_0

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->statCache:Lcom/navdy/hud/app/framework/network/NetworkStatCache;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$200(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Lcom/navdy/hud/app/framework/network/NetworkStatCache;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v29

    move/from16 v2, v23

    invoke-virtual {v0, v8, v1, v2, v11}, Lcom/navdy/hud/app/framework/network/NetworkStatCache;->addStat(Ljava/lang/String;III)V

    .line 206
    if-gtz v29, :cond_6

    if-lez v23, :cond_0

    .line 207
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->dnsCache:Lcom/navdy/hud/app/framework/network/DnsCache;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$100(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Lcom/navdy/hud/app/framework/network/DnsCache;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v8}, Lcom/navdy/hud/app/framework/network/DnsCache;->getHostnamefromIP(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 208
    .local v9, "dnsHost":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->urlToComponentMap:Ljava/util/HashMap;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$300(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Ljava/util/HashMap;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 210
    .local v4, "component":Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;
    if-nez v4, :cond_7

    .line 212
    const-string v30, "route.hybrid.api.here.com"

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 213
    sget-object v4, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_ROUTE:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 216
    :cond_7
    if-eqz v4, :cond_0

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->trafficDataDownloadedOnce:Z
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$400(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Z

    move-result v30

    if-nez v30, :cond_8

    sget-object v30, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    move-object/from16 v0, v30

    if-ne v4, v0, :cond_8

    if-lez v23, :cond_8

    .line 219
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v30

    const-string v31, "traffic data downloaded once"

    invoke-virtual/range {v30 .. v31}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    # setter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->trafficDataDownloadedOnce:Z
    invoke-static/range {v30 .. v31}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$402(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;Z)Z

    .line 222
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$1;->this$0:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->componentInfoMap:Ljava/util/HashMap;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->access$500(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;)Ljava/util/HashMap;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;

    .line 223
    .local v5, "componentInfo":Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;
    monitor-enter v5
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 224
    :try_start_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v30

    move-wide/from16 v0, v30

    iput-wide v0, v5, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;->lastActivity:J

    .line 225
    monitor-exit v5

    goto/16 :goto_0

    :catchall_0
    move-exception v30

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v30
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0

    .line 241
    .end local v4    # "component":Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;
    .end local v5    # "componentInfo":Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$ComponentInfo;
    .end local v6    # "data":[B
    .end local v7    # "dataObj":Lorg/json/JSONObject;
    .end local v8    # "destIP":Ljava/lang/String;
    .end local v9    # "dnsHost":Ljava/lang/String;
    .end local v10    # "event":Ljava/lang/String;
    .end local v11    # "fd":I
    .end local v12    # "from":Ljava/lang/String;
    .end local v17    # "jsonObject":Lorg/json/JSONObject;
    .end local v18    # "len":I
    .end local v20    # "nStreams":I
    .end local v21    # "netStatData":Lorg/json/JSONArray;
    .end local v22    # "packet":Ljava/net/DatagramPacket;
    .end local v23    # "rx":I
    .end local v25    # "socket":Ljava/net/DatagramSocket;
    .end local v26    # "str":Ljava/lang/String;
    .end local v28    # "to":Ljava/lang/String;
    .end local v29    # "tx":I
    .restart local v24    # "socket":Ljava/net/DatagramSocket;
    :catch_2
    move-exception v27

    goto/16 :goto_2

    .line 163
    :sswitch_data_0
    .sparse-switch
        0xf84ebe3 -> :sswitch_0
        0x6ddf9971 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
