.class Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;
.super Ljava/lang/Object;
.source "NetworkStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/network/NetworkStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    .line 72
    :try_start_0
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "checking n/w state"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 74
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v2

    .line 75
    .local v2, "networkInfos":[Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    array-length v4, v2

    if-nez v4, :cond_2

    .line 76
    :cond_0
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "no n/w info"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$000(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 108
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$600(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$500(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 111
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "networkInfos":[Landroid/net/NetworkInfo;
    :cond_1
    :goto_0
    return-void

    .line 79
    .restart local v0    # "cm":Landroid/net/ConnectivityManager;
    .restart local v2    # "networkInfos":[Landroid/net/NetworkInfo;
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_1
    array-length v4, v2

    if-ge v1, v4, :cond_5

    .line 80
    aget-object v4, v2, v1

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_a

    .line 81
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found dummy network we are up:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v1

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " count down="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->countDown:I
    invoke-static {v6}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$300(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 82
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # --operator for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->countDown:I
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$306(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)I

    .line 83
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->countDown:I
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$300(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-lez v4, :cond_3

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$000(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 108
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$600(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$500(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 86
    :cond_3
    :try_start_2
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "found dummy network starting init"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 87
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    const/4 v5, 0x1

    # setter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z
    invoke-static {v4, v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$002(Lcom/navdy/hud/app/framework/network/NetworkStateManager;Z)Z

    .line 88
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isNetworkLinkReady()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 89
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->lastPhoneNetworkState:Lcom/navdy/service/library/events/settings/NetworkStateChange;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$400(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Lcom/navdy/service/library/events/settings/NetworkStateChange;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->lastPhoneNetworkState:Lcom/navdy/service/library/events/settings/NetworkStateChange;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$400(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Lcom/navdy/service/library/events/settings/NetworkStateChange;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_7

    .line 90
    :cond_4
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkNotAvailable()V

    .line 97
    :goto_2
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/framework/network/NetworkStateManager$HudNetworkInitialized;

    invoke-direct {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager$HudNetworkInitialized;-><init>()V

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 101
    :cond_5
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$000(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 102
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "network state not initialized"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    :cond_6
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$000(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 108
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$600(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$500(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 92
    :cond_7
    :try_start_3
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkAvailable()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 104
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v1    # "i":I
    .end local v2    # "networkInfos":[Landroid/net/NetworkInfo;
    :catch_0
    move-exception v3

    .line 105
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_4
    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$000(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 108
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$600(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$500(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 95
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v0    # "cm":Landroid/net/ConnectivityManager;
    .restart local v1    # "i":I
    .restart local v2    # "networkInfos":[Landroid/net/NetworkInfo;
    :cond_8
    :try_start_5
    iget-object v4, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkNotAvailable()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 107
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v1    # "i":I
    .end local v2    # "networkInfos":[Landroid/net/NetworkInfo;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z
    invoke-static {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$000(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 108
    iget-object v5, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$600(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;->this$0:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    # getter for: Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->access$500(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_9
    throw v4

    .line 79
    .restart local v0    # "cm":Landroid/net/ConnectivityManager;
    .restart local v1    # "i":I
    .restart local v2    # "networkInfos":[Landroid/net/NetworkInfo;
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method
