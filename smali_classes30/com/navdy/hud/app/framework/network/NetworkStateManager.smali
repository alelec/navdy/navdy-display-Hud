.class public Lcom/navdy/hud/app/framework/network/NetworkStateManager;
.super Ljava/lang/Object;
.source "NetworkStateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/network/NetworkStateManager$HudNetworkInitialized;
    }
.end annotation


# static fields
.field private static final NAVDY_NETWORK_DOWN_INTENT:Ljava/lang/String; = "com.navdy.hud.NetworkDown"

.field private static final NAVDY_NETWORK_UP_INTENT:Ljava/lang/String; = "com.navdy.hud.NetworkUp"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/framework/network/NetworkStateManager;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private countDown:I

.field private handler:Landroid/os/Handler;

.field private lastPhoneNetworkState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

.field private networkCheck:Ljava/lang/Runnable;

.field private volatile networkStateInitialized:Z

.field private triggerNetworkCheck:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 46
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->singleton:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;

    .line 57
    const/4 v0, 0x3

    iput v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->countDown:I

    .line 59
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager$1;-><init>(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;

    .line 68
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager$2;-><init>(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkCheck:Ljava/lang/Runnable;

    .line 115
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 117
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 123
    :goto_0
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/framework/network/NetworkStateManager$HudNetworkInitialized;

    invoke-direct {v1}, Lcom/navdy/hud/app/framework/network/NetworkStateManager$HudNetworkInitialized;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/framework/network/NetworkStateManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkCheck:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 30
    iget v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->countDown:I

    return v0
.end method

.method static synthetic access$306(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 30
    iget v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->countDown:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->countDown:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Lcom/navdy/service/library/events/settings/NetworkStateChange;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->lastPhoneNetworkState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->triggerNetworkCheck:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/network/NetworkStateManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/network/NetworkStateManager;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->singleton:Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    return-object v0
.end method

.method public static isConnectedToCellNetwork(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 224
    const-string v3, "connectivity"

    .line 225
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 226
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 227
    .local v1, "wifiNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public static isConnectedToNetwork(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 197
    const-string v2, "connectivity"

    .line 198
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 200
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 201
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setEngineOnlineState active network="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 202
    if-eqz v0, :cond_1

    .line 203
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 201
    :cond_0
    const-string v2, " no active network "

    goto :goto_0

    .line 203
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static isConnectedToWifi(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 212
    const-string v3, "connectivity"

    .line 213
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 214
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 215
    .local v1, "wifiNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getLastPhoneNetworkState()Lcom/navdy/service/library/events/settings/NetworkStateChange;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->lastPhoneNetworkState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    return-object v0
.end method

.method public isNetworkStateInitialized()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z

    return v0
.end method

.method public networkAvailable()V
    .locals 4

    .prologue
    .line 162
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z

    if-nez v2, :cond_0

    .line 163
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "network not initialized yet"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 177
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isNetworkDisabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 168
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "network is permanently disabled"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkNotAvailable()V

    goto :goto_0

    .line 173
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.navdy.hud.NetworkUp"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 174
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 175
    .local v0, "handle":Landroid/os/UserHandle;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 176
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "DummyNetNetworkFactory:sent:com.navdy.hud.NetworkUp"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public networkNotAvailable()V
    .locals 4

    .prologue
    .line 180
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkStateInitialized:Z

    if-nez v2, :cond_0

    .line 181
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "network not initialized yet"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 189
    :goto_0
    return-void

    .line 185
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.navdy.hud.NetworkDown"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 187
    .local v0, "handle":Landroid/os/UserHandle;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 188
    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "DummyNetNetworkFactory:sent:com.navdy.hud.NetworkDown"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 149
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConnectionStateChange - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 150
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkStateManager$3;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 155
    :goto_0
    return-void

    .line 152
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->lastPhoneNetworkState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onNetworkStateChange(Lcom/navdy/service/library/events/settings/NetworkStateChange;)V
    .locals 4
    .param p1, "networkStateChange"    # Lcom/navdy/service/library/events/settings/NetworkStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 128
    :try_start_0
    iput-object p1, p0, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->lastPhoneNetworkState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .line 130
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NetworkStateChange available["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] cell["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] wifi["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] reach["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 135
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "DummyNetNetworkFactory:phone n/w avaialble"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkAvailable()V

    .line 145
    :goto_0
    return-void

    .line 139
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "DummyNetNetworkFactory:phone n/w lost"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->networkNotAvailable()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
