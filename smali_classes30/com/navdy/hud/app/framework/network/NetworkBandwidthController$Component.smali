.class public final enum Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;
.super Ljava/lang/Enum;
.source "NetworkBandwidthController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Component"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

.field public static final enum HERE_MAP_DOWNLOAD:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

.field public static final enum HERE_REVERSE_GEO:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

.field public static final enum HERE_ROUTE:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

.field public static final enum HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

.field public static final enum HOCKEY:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

.field public static final enum JIRA:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

.field public static final enum LOCALYTICS:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 74
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    const-string v1, "LOCALYTICS"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->LOCALYTICS:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 75
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    const-string v1, "HERE_ROUTE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_ROUTE:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 76
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    const-string v1, "HERE_TRAFFIC"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 77
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    const-string v1, "HERE_MAP_DOWNLOAD"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_MAP_DOWNLOAD:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 78
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    const-string v1, "HERE_REVERSE_GEO"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_REVERSE_GEO:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 79
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    const-string v1, "HOCKEY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HOCKEY:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 80
    new-instance v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    const-string v1, "JIRA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->JIRA:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    .line 73
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->LOCALYTICS:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_ROUTE:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_TRAFFIC:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_MAP_DOWNLOAD:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HERE_REVERSE_GEO:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->HOCKEY:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->JIRA:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->$VALUES:[Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 73
    const-class v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->$VALUES:[Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    return-object v0
.end method
