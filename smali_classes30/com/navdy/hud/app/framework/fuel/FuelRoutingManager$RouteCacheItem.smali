.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RouteCacheItem"
.end annotation


# instance fields
.field gasStation:Lcom/here/android/mpa/search/Place;

.field route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Lcom/here/android/mpa/search/Place;)V
    .locals 0
    .param p1, "route"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .param p2, "gasStation"    # Lcom/here/android/mpa/search/Place;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 83
    iput-object p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    .line 84
    return-void
.end method
