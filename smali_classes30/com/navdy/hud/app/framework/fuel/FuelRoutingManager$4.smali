.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->calculateGasStationRoutes(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

.field final synthetic val$gasCalculationCoords:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 397
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$gasCalculationCoords:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/util/List;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 401
    .local p1, "places":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Place;>;"
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 403
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    sget-object v3, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->INVALID_STATE:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    .line 465
    :goto_0
    return-void

    .line 407
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 408
    new-instance v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4$1;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;)V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 419
    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/here/android/mpa/search/Place;

    .line 420
    .local v16, "p":Lcom/here/android/mpa/search/Place;
    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v15

    .line 421
    .local v15, "location":Lcom/here/android/mpa/search/Location;
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->getPlaceEntry(Lcom/here/android/mpa/search/Place;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v14

    .line 422
    .local v14, "gasNavGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v15}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/search/Address;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "<br/>"

    const-string v8, ", "

    invoke-virtual {v3, v4, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    const-string v8, ""

    invoke-virtual {v3, v4, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 423
    .local v12, "address":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v13

    .line 424
    .local v13, "gasDisplayGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "gas station name ["

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "]"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " address ["

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "]"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " distance ["

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$gasCalculationCoords:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 426
    invoke-virtual {v8, v14}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v8

    double-to-int v8, v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "] meters"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " displayPos ["

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "]"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " navPos ["

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "]"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 424
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 461
    .end local v12    # "address":Ljava/lang/String;
    .end local v13    # "gasDisplayGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v14    # "gasNavGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v15    # "location":Lcom/here/android/mpa/search/Location;
    .end local v16    # "p":Lcom/here/android/mpa/search/Place;
    :catch_0
    move-exception v18

    .line 462
    .local v18, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "calculateGasStationRoutes"

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    sget-object v3, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->UNKNOWN_ERROR:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    goto/16 :goto_0

    .line 432
    .end local v18    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 433
    .local v6, "routingQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;>;"
    new-instance v7, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 435
    .local v7, "routeCache":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/here/android/mpa/search/Place;

    .line 436
    .local v5, "place":Lcom/here/android/mpa/search/Place;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 438
    .local v10, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->getPlaceEntry(Lcom/here/android/mpa/search/Place;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v17

    .line 440
    .local v17, "placeEntry":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 441
    move-object/from16 v0, v17

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 442
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v11

    .line 447
    .local v11, "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_3
    new-instance v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .line 448
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$gasCalculationCoords:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct/range {v2 .. v11}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;ILcom/here/android/mpa/search/Place;Ljava/util/Queue;Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 447
    invoke-interface {v6, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 444
    .end local v11    # "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_3
    move-object/from16 v11, v17

    .restart local v11    # "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    goto :goto_3

    .line 459
    .end local v5    # "place":Lcom/here/android/mpa/search/Place;
    .end local v10    # "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    .end local v11    # "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v17    # "placeEntry":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_4
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "posting to handler"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 460
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Landroid/os/Handler;

    move-result-object v3

    invoke-interface {v6}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    .prologue
    .line 469
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 470
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calculateGasStationRoutes error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    .line 472
    return-void
.end method
