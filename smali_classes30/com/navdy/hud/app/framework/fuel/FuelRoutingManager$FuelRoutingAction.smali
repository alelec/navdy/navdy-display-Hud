.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FuelRoutingAction"
.end annotation


# instance fields
.field private currentRouteCacheSize:I

.field private final endPoint:Lcom/here/android/mpa/common/GeoCoordinate;

.field private final onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

.field private final place:Lcom/here/android/mpa/search/Place;

.field private final routeCache:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;",
            ">;"
        }
    .end annotation
.end field

.field private final routingQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;",
            ">;"
        }
    .end annotation
.end field

.field private final routingStart:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

.field private final waypoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;ILcom/here/android/mpa/search/Place;Ljava/util/Queue;Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p2, "placesSize"    # I
    .param p3, "place"    # Lcom/here/android/mpa/search/Place;
    .param p6, "onRouteToGasStationCallback"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;
    .param p7, "routingStart"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p9, "endPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/here/android/mpa/search/Place;",
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;",
            ">;",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ")V"
        }
    .end annotation

    .prologue
    .line 647
    .local p4, "routingQueue":Ljava/util/Queue;, "Ljava/util/Queue<Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;>;"
    .local p5, "routeCache":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;>;"
    .local p8, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648
    iput-object p3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->place:Lcom/here/android/mpa/search/Place;

    .line 649
    iput-object p4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routingQueue:Ljava/util/Queue;

    .line 650
    iput-object p5, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routeCache:Ljava/util/List;

    .line 651
    iput-object p6, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    .line 652
    iput-object p7, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routingStart:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 653
    iput-object p8, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->waypoints:Ljava/util/List;

    .line 654
    iput-object p9, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->endPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 656
    iput p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->currentRouteCacheSize:I

    .line 657
    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/here/android/mpa/search/Place;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    .prologue
    .line 628
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->place:Lcom/here/android/mpa/search/Place;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    .prologue
    .line 628
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routeCache:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    .prologue
    .line 628
    iget v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->currentRouteCacheSize:I

    return v0
.end method

.method static synthetic access$1602(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;
    .param p1, "x1"    # I

    .prologue
    .line 628
    iput p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->currentRouteCacheSize:I

    return p1
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    .prologue
    .line 628
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    .prologue
    .line 628
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routingQueue:Ljava/util/Queue;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 661
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 715
    :goto_0
    return-void

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$2100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routingStart:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->waypoints:Ljava/util/List;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->endPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    new-instance v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;

    invoke-direct {v6, p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)V

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .line 710
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;
    invoke-static {v8}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$2000(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v8

    move v9, v5

    move v10, v5

    move v11, v5

    .line 665
    invoke-virtual/range {v0 .. v11}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;ZZZ)V

    goto :goto_0
.end method
