.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->findGasStations(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

.field final synthetic val$fuelLevel:I

.field final synthetic val$postNotificationOnError:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;IZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 719
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iput p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->val$fuelLevel:I

    iput-boolean p3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->val$postNotificationOnError:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 722
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->available:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 723
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Fuel manager not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 807
    :goto_0
    return-void

    .line 727
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$602(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Z)Z

    .line 729
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;)V

    # invokes: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->calculateGasStationRoutes(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$700(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V

    goto :goto_0
.end method
