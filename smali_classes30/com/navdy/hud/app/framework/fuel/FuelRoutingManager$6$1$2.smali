.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

.field final synthetic val$error:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    .prologue
    .line 774
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->val$error:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 777
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "received an error on calculateGasStationRoutes "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->val$error:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 779
    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v2, v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$602(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Z)Z

    .line 781
    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-boolean v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->val$postNotificationOnError:Z

    if-nez v2, :cond_0

    .line 803
    :goto_0
    return-void

    .line 785
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 786
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v2, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v3, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->val$fuelLevel:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 788
    new-instance v2, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v3, Lcom/navdy/service/library/events/glances/FuelConstants;->NO_ROUTE:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 790
    new-instance v2, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 791
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v2

    const-string v3, "low#fuel#level"

    .line 792
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v2

    .line 793
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v2

    const-string v3, "com.navdy.fuel"

    .line 794
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v2

    .line 795
    invoke-virtual {v2, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v1

    .line 796
    .local v1, "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$2400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/squareup/otto/Bus;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 798
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 799
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "posting low fuel glance with no routes"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 802
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset()V

    goto/16 :goto_0
.end method
