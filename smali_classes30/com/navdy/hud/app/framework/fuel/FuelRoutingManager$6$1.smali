.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    .prologue
    .line 729
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    .prologue
    .line 774
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$2;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 805
    return-void
.end method

.method public onOptimalRouteCalculationComplete(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 3
    .param p1, "optimal"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
    .param p3, "navigationRouteRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 734
    .local p2, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 770
    return-void
.end method
