.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4$1;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->onCompleted(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/here/android/mpa/search/Place;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;

    .prologue
    .line 408
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/here/android/mpa/search/Place;Lcom/here/android/mpa/search/Place;)I
    .locals 6
    .param p1, "lhs"    # Lcom/here/android/mpa/search/Place;
    .param p2, "rhs"    # Lcom/here/android/mpa/search/Place;

    .prologue
    .line 411
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$gasCalculationCoords:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p1}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v0

    .line 412
    .local v0, "distanceLhs":D
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;->val$gasCalculationCoords:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p2}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v2

    .line 413
    .local v2, "distanceRhs":D
    sub-double v4, v0, v2

    double-to-int v4, v4

    return v4
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 408
    check-cast p1, Lcom/here/android/mpa/search/Place;

    check-cast p2, Lcom/here/android/mpa/search/Place;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4$1;->compare(Lcom/here/android/mpa/search/Place;Lcom/here/android/mpa/search/Place;)I

    move-result v0

    return v0
.end method
