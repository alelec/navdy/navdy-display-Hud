.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2$1;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V
    .locals 2
    .param p1, "error"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->val$callback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    .line 291
    return-void
.end method

.method public onOptimalRouteCalculationComplete(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 2
    .param p1, "optimal"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
    .param p3, "navigationRouteRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 284
    .local p2, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 285
    .local v0, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->val$callback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;

    invoke-interface {v1, v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;->onComplete(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V

    .line 286
    return-void
.end method
