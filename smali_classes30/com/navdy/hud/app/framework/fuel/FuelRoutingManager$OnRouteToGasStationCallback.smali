.class public interface abstract Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnRouteToGasStationCallback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;
    }
.end annotation


# virtual methods
.method public abstract onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V
.end method

.method public abstract onOptimalRouteCalculationComplete(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            ")V"
        }
    .end annotation
.end method
