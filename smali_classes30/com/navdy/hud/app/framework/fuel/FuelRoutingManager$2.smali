.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->findNearestGasStation(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

.field final synthetic val$callback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->val$callback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->available:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Fuel manager not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 293
    :goto_0
    return-void

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->val$callback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;

    if-nez v0, :cond_1

    .line 271
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->val$callback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->INVALID_STATE:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    goto :goto_0

    .line 279
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$602(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Z)Z

    .line 281
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2$1;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;)V

    # invokes: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->calculateGasStationRoutes(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$700(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V

    goto :goto_0
.end method
