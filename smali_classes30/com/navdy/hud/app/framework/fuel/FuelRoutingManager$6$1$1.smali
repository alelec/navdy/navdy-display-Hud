.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->onOptimalRouteCalculationComplete(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

.field final synthetic val$navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field final synthetic val$optimal:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

.field final synthetic val$outgoingResults:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    .prologue
    .line 734
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$optimal:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$outgoingResults:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    .line 737
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$optimal:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentGasStation:Lcom/here/android/mpa/search/Place;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$2202(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/here/android/mpa/search/Place;)Lcom/here/android/mpa/search/Place;

    .line 738
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v4, v10}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$602(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Z)Z

    .line 739
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$outgoingResults:Ljava/util/ArrayList;

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$202(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 740
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iget-object v5, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentNavigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$502(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 742
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$optimal:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v4

    .line 743
    invoke-virtual {v4}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$2300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v4

    mul-double/2addr v4, v8

    const-wide v6, 0x4099255c28f5c28fL    # 1609.34

    div-double/2addr v4, v6

    .line 742
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-double v4, v4

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    .line 745
    .local v2, "distanceString":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$optimal:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v4

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v4

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Address;->getText()Ljava/lang/String;

    move-result-object v4

    const-string v5, "<br/>"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aget-object v0, v4, v10

    .line 747
    .local v0, "addressString":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 748
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->val$fuelLevel:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 749
    new-instance v4, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v5, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {v6}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$800(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 752
    :cond_0
    new-instance v4, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v5, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_NAME:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->val$optimal:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iget-object v6, v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v6}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 753
    new-instance v4, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v5, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_ADDRESS:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 754
    new-instance v4, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v5, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_DISTANCE:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 757
    new-instance v4, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v5, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 758
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    const-string v5, "low#fuel#level"

    .line 759
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    .line 760
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    const-string v5, "com.navdy.fuel"

    .line 761
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    .line 762
    invoke-virtual {v4, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v3

    .line 763
    .local v3, "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1$1;->this$2:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    iget-object v4, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$2400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/squareup/otto/Bus;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 765
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 766
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "state is now LOW_FUEL, posting low fuel glance"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 768
    :cond_1
    return-void
.end method
