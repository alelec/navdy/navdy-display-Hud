.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->checkFuelLevel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 321
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 324
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$800(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/obd/ObdManager;->getFuelLevel()I

    move-result v2

    .line 326
    .local v2, "fuelLevel":I
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 327
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "checkFuelLevel:fuel level is not available from OBD Manager"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 328
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 333
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "checkFuelLevel: already calculating fuel route"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 337
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isOnGasRoute()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 338
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "checkFuelLevel: already on fuel route"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$900(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)J

    move-result-wide v4

    cmp-long v3, v4, v8

    if-lez v3, :cond_5

    .line 343
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$900(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 344
    .local v0, "elapsed":J
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->FUEL_GLANCE_REDISPLAY_THRESHOLD:J
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1000()J

    move-result-wide v4

    cmp-long v3, v0, v4

    if-gez v3, :cond_4

    .line 345
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkFuelLevel: fuel glance dismiss threshold still valid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 349
    :cond_4
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkFuelLevel: fuel glance dismiss threshold expired:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 350
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J
    invoke-static {v3, v8, v9}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$902(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;J)J

    .line 353
    .end local v0    # "elapsed":J
    :cond_5
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkFuelLevel: Fuel level:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " threshold:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentLowFuelThreshold:I
    invoke-static {v5}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " testMode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z
    invoke-static {v5}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 355
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentLowFuelThreshold:I
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)I

    move-result v3

    if-gt v2, v3, :cond_9

    .line 356
    :cond_6
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isFuelNotificationEnabled()Z

    move-result v3

    if-nez v3, :cond_7

    .line 357
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "checkFuelLevel:fuel notifications are not enabled"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 360
    :cond_7
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 361
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "checkFuelLevel:low fuel glance already shown to user"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 364
    :cond_8
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->findGasStations(IZ)V

    goto/16 :goto_0

    .line 365
    :cond_9
    const/16 v3, 0x14

    if-le v2, v3, :cond_a

    .line 366
    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # invokes: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->setFuelLevelBackToNormal()V
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1200(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V

    goto/16 :goto_0

    .line 368
    :cond_a
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 369
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "checkFuelLevel: no-op"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
