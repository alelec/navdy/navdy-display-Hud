.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->routeToGasStation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 197
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "routeToGasStation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->available:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Fuel manager not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 247
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$200(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$200(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 205
    :cond_1
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "outgoing results not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset()V

    goto :goto_0

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 211
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "navon: saving current non-gas route to storage"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-wide/16 v4, 0x14

    .line 214
    .local v4, "simulationSpeed":J
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 215
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "navon: already arrived, set ignore flag"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setIgnoreArrived(Z)V

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .line 220
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$200(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_FUEL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .line 222
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentNavigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v3

    .line 219
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->addNewRoute(Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;J)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v13

    .line 225
    .local v13, "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v0, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    if-ne v13, v0, :cond_5

    .line 226
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "navon:gas route navigation started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 212
    .end local v4    # "simulationSpeed":J
    .end local v13    # "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_4
    const-wide/16 v4, -0x1

    goto :goto_1

    .line 229
    .restart local v4    # "simulationSpeed":J
    .restart local v13    # "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_5
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "navon:arrival route navigation started failed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset()V

    .line 231
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    .end local v4    # "simulationSpeed":J
    .end local v13    # "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$200(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 235
    .local v14, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v0

    iget-object v1, v14, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v12

    .line 236
    .local v12, "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-nez v12, :cond_7

    .line 237
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "navoff:route not available in cache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v14, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset()V

    goto/16 :goto_0

    .line 241
    :cond_7
    new-instance v6, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    sget-object v7, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v8, v14, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    iget-object v9, v14, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    .line 242
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-direct/range {v6 .. v11}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 243
    .local v6, "request":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handleNavigationSessionRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V

    .line 244
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 245
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "navoff:gas route navigation started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
