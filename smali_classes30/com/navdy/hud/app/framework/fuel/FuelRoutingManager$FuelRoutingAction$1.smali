.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    .prologue
    .line 671
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/routing/RoutingError;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 692
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calculated route to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->place:Lcom/here/android/mpa/search/Place;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/here/android/mpa/search/Place;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/RoutingError;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 693
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->currentRouteCacheSize:I
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    # setter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->currentRouteCacheSize:I
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1602(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;I)I

    .line 695
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routeCache:Ljava/util/List;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->currentRouteCacheSize:I
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 696
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routeCache:Ljava/util/List;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1700(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    move-result-object v2

    # invokes: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->calculateOptimalGasStation(Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1800(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V

    .line 700
    :goto_0
    return-void

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routingQueue:Ljava/util/Queue;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1900(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public postSuccess(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 677
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calculated route to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->place:Lcom/here/android/mpa/search/Place;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/here/android/mpa/search/Place;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with no errors"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 680
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 681
    .local v0, "route":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routeCache:Ljava/util/List;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->place:Lcom/here/android/mpa/search/Place;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/here/android/mpa/search/Place;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Lcom/here/android/mpa/search/Place;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 683
    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routeCache:Ljava/util/List;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->currentRouteCacheSize:I
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 684
    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routeCache:Ljava/util/List;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1700(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->calculateOptimalGasStation(Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1800(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V

    .line 688
    :goto_0
    return-void

    .line 686
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$1300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->routingQueue:Ljava/util/Queue;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1900(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public preSuccess()V
    .locals 0

    .prologue
    .line 673
    return-void
.end method

.method public progress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 704
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Calculating route to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction$1;->this$1:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;

    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->place:Lcom/here/android/mpa/search/Place;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;->access$1400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;)Lcom/here/android/mpa/search/Place;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; progress %: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 707
    :cond_0
    return-void
.end method
