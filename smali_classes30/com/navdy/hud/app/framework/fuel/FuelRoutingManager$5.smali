.class Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->requestOptimalRoute(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

.field final synthetic val$gasRoute:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

.field final synthetic val$navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field final synthetic val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 589
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->this$0:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->val$gasRoute:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->val$navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "error"    # Lcom/here/android/mpa/routing/RoutingError;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    .line 601
    return-void
.end method

.method public postSuccess(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 595
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->val$onRouteToGasStationCallback:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->val$gasRoute:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;->val$navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-interface {v0, v1, p1, v2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onOptimalRouteCalculationComplete(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    .line 596
    return-void
.end method

.method public preSuccess()V
    .locals 0

    .prologue
    .line 591
    return-void
.end method

.method public progress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 605
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    # getter for: Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calculating route to optimal gas station progress %: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 608
    :cond_0
    return-void
.end method
