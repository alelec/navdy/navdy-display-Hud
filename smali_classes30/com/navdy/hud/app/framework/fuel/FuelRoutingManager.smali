.class public final Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
.super Ljava/lang/Object;
.source "FuelRoutingManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelRoutingAction;,
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelAddedTestEvent;,
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$ClearTestObdLowFuelLevel;,
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$TestObdLowFuelLevel;,
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;,
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;,
        Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
    }
.end annotation


# static fields
.field private static final CANCEL_NOTIFICATION_THRESHOLD:I = 0x14

.field private static final DEFAULT_LOW_FUEL_THRESHOLD:I = 0xf

.field public static final FINDING_FAST_STATION_TOAST_ID:Ljava/lang/String; = "toast#find#gas"

.field private static final FUEL_GLANCE_REDISPLAY_THRESHOLD:J

.field public static final GAS_CATEGORY:Lcom/here/android/mpa/search/CategoryFilter;

.field private static final GAS_STATION_CATEGORY:Ljava/lang/String; = "petrol-station"

.field public static final LOW_FUEL_ID:Ljava/lang/String;

.field private static final LOW_FUEL_ID_STR:Ljava/lang/String; = "low#fuel#level"

.field private static final N_GAS_STATIONS_REQUESTED:I = 0x3

.field private static final ONE_MINUTE_SECONDS:I = 0x3c

.field private static final TEST_SIMULATION_SPEED:J = 0x14L

.field private static sInstance:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private available:Z

.field private final bus:Lcom/squareup/otto/Bus;

.field private currentGasStation:Lcom/here/android/mpa/search/Place;

.field private currentLowFuelThreshold:I

.field private currentNavigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field private currentOutgoingResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;"
        }
    .end annotation
.end field

.field private fuelGlanceDismissTime:J

.field private gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;

.field private final handler:Landroid/os/Handler;

.field private final hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

.field private hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private hereRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

.field private isCalculatingFuelRoute:Z

.field private isInTestMode:Z

.field private final obdManager:Lcom/navdy/hud/app/obd/ObdManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 75
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->FUEL_GLANCE_REDISPLAY_THRESHOLD:J

    .line 103
    sget-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    const-string v1, "low#fuel#level"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getNotificationId(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    .line 105
    new-instance v0, Lcom/here/android/mpa/search/CategoryFilter;

    invoke-direct {v0}, Lcom/here/android/mpa/search/CategoryFilter;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->GAS_CATEGORY:Lcom/here/android/mpa/search/CategoryFilter;

    .line 112
    new-instance v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sInstance:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .line 115
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->GAS_CATEGORY:Lcom/here/android/mpa/search/CategoryFilter;

    const-string v1, "petrol-station"

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    .line 116
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    .line 144
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 146
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->bus:Lcom/squareup/otto/Bus;

    .line 147
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->handler:Landroid/os/Handler;

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z

    .line 150
    const/16 v0, 0xf

    iput v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentLowFuelThreshold:I

    .line 151
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->available:Z

    return v0
.end method

.method static synthetic access$1000()J
    .locals 2

    .prologue
    .line 70
    sget-wide v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->FUEL_GLANCE_REDISPLAY_THRESHOLD:J

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentLowFuelThreshold:I

    return v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->setFuelLevelBackToNormal()V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->calculateOptimalGasStation(Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/here/android/mpa/routing/RouteOptions;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/here/android/mpa/search/Place;)Lcom/here/android/mpa/search/Place;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .param p1, "x1"    # Lcom/here/android/mpa/search/Place;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentGasStation:Lcom/here/android/mpa/search/Place;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->checkFuelLevel()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentNavigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentNavigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z

    return p1
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->calculateGasStationRoutes(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)Lcom/navdy/hud/app/obd/ObdManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->obdManager:Lcom/navdy/hud/app/obd/ObdManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J

    return-wide v0
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .param p1, "x1"    # J

    .prologue
    .line 70
    iput-wide p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J

    return-wide p1
.end method

.method private calculateGasStationRoutes(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    .locals 4
    .param p1, "onRouteToGasStationCallback"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    .prologue
    .line 385
    if-nez p1, :cond_0

    .line 386
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 389
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->getBestInitialGeo()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 391
    .local v0, "gasCalculationCoords":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_1

    .line 392
    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "No user location while calculating routes to gas station"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 393
    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->NO_USER_LOCATION:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {p1, v1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    .line 474
    :goto_0
    return-void

    .line 397
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->GAS_CATEGORY:Lcom/here/android/mpa/search/CategoryFilter;

    const/4 v2, 0x3

    new-instance v3, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;

    invoke-direct {v3, p0, p1, v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$4;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;Lcom/here/android/mpa/common/GeoCoordinate;)V

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V

    goto :goto_0
.end method

.method private calculateOptimalGasStation(Ljava/util/List;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    .locals 10
    .param p2, "onRouteToGasStationCallback"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;",
            ">;",
            "Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 521
    .local p1, "routeCache":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;>;"
    const/4 v3, 0x0

    .line 522
    .local v3, "optimal":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
    const/4 v1, -0x1

    .line 524
    .local v1, "minTta":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;

    .line 525
    .local v4, "routeCacheItem":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
    iget-object v7, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v7, v7, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 526
    .local v5, "tta":I
    iget-object v7, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v2

    .line 527
    .local v2, "name":Ljava/lang/String;
    iget-object v7, v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v7

    invoke-virtual {v7}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v8

    double-to-int v0, v8

    .line 529
    .local v0, "distance":I
    sget-object v7, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "evaluating ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] distance ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] tta ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 531
    if-ltz v1, :cond_1

    if-ge v5, v1, :cond_0

    .line 532
    :cond_1
    move v1, v5

    .line 533
    move-object v3, v4

    goto :goto_0

    .line 537
    .end local v0    # "distance":I
    .end local v2    # "name":Ljava/lang/String;
    .end local v4    # "routeCacheItem":Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
    .end local v5    # "tta":I
    :cond_2
    if-eqz v3, :cond_3

    .line 538
    sget-object v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Optimal route: \n\tName:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v8}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n\tAddress: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    .line 539
    invoke-virtual {v8}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v8

    invoke-virtual {v8}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n\tTTA: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " s\n\tGas station distance: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    .line 541
    invoke-virtual {v8}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v8

    invoke-virtual {v8}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " m"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 538
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 543
    invoke-direct {p0, v3, p2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->requestOptimalRoute(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V

    .line 548
    :goto_1
    return-void

    .line 545
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "No routes to any gas stations"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 546
    sget-object v6, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->NO_ROUTES:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    invoke-interface {p2, v6}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    goto :goto_1
.end method

.method private checkFuelLevel()V
    .locals 3

    .prologue
    .line 321
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$3;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 374
    return-void
.end method

.method private getBestInitialGeo()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 18

    .prologue
    .line 477
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 478
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .line 479
    .local v2, "bestInitialGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v6

    .line 481
    .local v6, "route":Lcom/here/android/mpa/routing/Route;
    if-eqz v6, :cond_3

    .line 482
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 483
    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereNavController;->getElapsedDistance()J

    move-result-wide v12

    long-to-int v11, v12

    .line 482
    invoke-virtual {v6, v11}, Lcom/here/android/mpa/routing/Route;->getRouteElementsFromLength(I)Lcom/here/android/mpa/routing/RouteElements;

    move-result-object v8

    .line 486
    .local v8, "routeElements":Lcom/here/android/mpa/routing/RouteElements;
    if-eqz v8, :cond_3

    .line 487
    invoke-virtual {v8}, Lcom/here/android/mpa/routing/RouteElements;->getElements()Ljava/util/List;

    move-result-object v9

    .line 489
    .local v9, "routeElementsList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    if-eqz v9, :cond_3

    .line 490
    const/4 v4, 0x0

    .line 491
    .local v4, "oneMinuteFromNowGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    const/4 v10, 0x0

    .line 493
    .local v10, "timeCount":I
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/here/android/mpa/routing/RouteElement;

    .line 494
    .local v7, "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    invoke-virtual {v7}, Lcom/here/android/mpa/routing/RouteElement;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v5

    .line 496
    .local v5, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/here/android/mpa/common/RoadElement;->getDefaultSpeed()F

    move-result v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_1

    .line 497
    int-to-long v12, v10

    invoke-virtual {v5}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v14

    invoke-virtual {v5}, Lcom/here/android/mpa/common/RoadElement;->getDefaultSpeed()F

    move-result v16

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    add-long/2addr v12, v14

    long-to-int v10, v12

    .line 500
    :cond_1
    const/16 v12, 0x3c

    if-le v10, v12, :cond_0

    .line 501
    invoke-virtual {v7}, Lcom/here/android/mpa/routing/RouteElement;->getGeometry()Ljava/util/List;

    move-result-object v3

    .line 503
    .local v3, "geometry":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_0

    .line 504
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "oneMinuteFromNowGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    check-cast v4, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 510
    .end local v3    # "geometry":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    .end local v5    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v7    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    .restart local v4    # "oneMinuteFromNowGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_2
    if-eqz v4, :cond_3

    .line 511
    move-object v2, v4

    .line 517
    .end local v4    # "oneMinuteFromNowGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v8    # "routeElements":Lcom/here/android/mpa/routing/RouteElements;
    .end local v9    # "routeElementsList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    .end local v10    # "timeCount":I
    :cond_3
    return-object v2
.end method

.method public static getInstance()Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sInstance:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;

    return-object v0
.end method

.method private requestOptimalRoute(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;)V
    .locals 22
    .param p1, "gasRoute"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;
    .param p2, "onRouteToGasStationCallback"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;

    .prologue
    .line 553
    :try_start_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->getPlaceEntry(Lcom/here/android/mpa/search/Place;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v8

    .line 554
    .local v8, "gasNavGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v4

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v19

    .line 556
    .local v19, "gasDisplayGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    .line 557
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 558
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 559
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v20

    .line 561
    .local v20, "navigationPosition":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v4, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    .line 562
    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 563
    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v4

    .line 564
    invoke-virtual {v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v18

    .line 566
    .local v18, "displayPosition":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v16, Ljava/util/ArrayList;

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 567
    .local v16, "attrs":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    sget-object v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_GAS:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569
    new-instance v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;-><init>()V

    .line 570
    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    .line 571
    invoke-virtual {v6}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    .line 572
    invoke-virtual {v6}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/search/Address;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->streetAddress(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;->gasStation:Lcom/here/android/mpa/search/Place;

    .line 573
    invoke-virtual {v6}, Lcom/here/android/mpa/search/Place;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination_identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    const/4 v6, 0x1

    .line 574
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    const/4 v6, 0x0

    .line 575
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->geoCodeStreetAddress(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    sget-object v6, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 576
    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationType(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 577
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 578
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationDisplay(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v4

    .line 579
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->routeAttributes(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v17

    .line 581
    .local v17, "builder":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v5

    .line 583
    .local v5, "navigationRouteRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 585
    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v9, 0x1

    new-instance v10, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v10, v0, v1, v2, v5}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$5;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$RouteCacheItem;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;

    const/4 v13, 0x1

    const/4 v14, 0x1

    const/4 v15, 0x0

    .line 583
    invoke-virtual/range {v4 .. v15}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;ZZZ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 620
    .end local v5    # "navigationRouteRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v8    # "gasNavGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v16    # "attrs":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    .end local v17    # "builder":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .end local v18    # "displayPosition":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v19    # "gasDisplayGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v20    # "navigationPosition":Lcom/navdy/service/library/events/location/Coordinate;
    :goto_0
    return-void

    .line 616
    :catch_0
    move-exception v21

    .line 617
    .local v21, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "requestOptimalRoute"

    move-object/from16 v0, v21

    invoke-virtual {v4, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 618
    sget-object v4, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;->UNKNOWN_ERROR:Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback;->onError(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnRouteToGasStationCallback$Error;)V

    goto :goto_0
.end method

.method private reset(I)V
    .locals 2
    .param p1, "fuelThreshold"    # I

    .prologue
    const/4 v1, 0x0

    .line 298
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 300
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isOnGasRoute()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->arrived()V

    .line 304
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentLowFuelThreshold:I

    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z

    .line 306
    iput-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentOutgoingResults:Ljava/util/ArrayList;

    .line 307
    iput-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentNavigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 308
    iput-object v1, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentGasStation:Lcom/here/android/mpa/search/Place;

    .line 310
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "reset, state is now TRACKING"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 313
    :cond_1
    return-void
.end method

.method private setFuelLevelBackToNormal()V
    .locals 2

    .prologue
    .line 377
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "checkFuelLevel:Fuel level normal"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 379
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset()V

    .line 380
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 381
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J

    .line 382
    return-void
.end method


# virtual methods
.method public dismissGasRoute()V
    .locals 2

    .prologue
    .line 252
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dismissGasRoute"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 253
    iget v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentLowFuelThreshold:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentLowFuelThreshold:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset(I)V

    .line 254
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J

    .line 255
    return-void
.end method

.method public findGasStations(IZ)V
    .locals 3
    .param p1, "fuelLevel"    # I
    .param p2, "postNotificationOnError"    # Z

    .prologue
    .line 719
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$6;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;IZ)V

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 809
    return-void
.end method

.method public findNearestGasStation(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;

    .prologue
    .line 262
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$2;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$OnNearestGasStationCallback;)V

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 295
    return-void
.end method

.method public getCurrentGasStation()Lcom/here/android/mpa/search/Place;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->currentGasStation:Lcom/here/android/mpa/search/Place;

    return-object v0
.end method

.method public getFuelGlanceDismissTime()J
    .locals 2

    .prologue
    .line 860
    iget-wide v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J

    return-wide v0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 851
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->available:Z

    return v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 855
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isCalculatingFuelRoute:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isOnGasRoute()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public markAvailable()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 828
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->available:Z

    if-nez v0, :cond_0

    .line 829
    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->available:Z

    .line 830
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 831
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;-><init>(Lcom/navdy/service/library/log/Logger;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->hereRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 833
    new-instance v0, Lcom/here/android/mpa/routing/RouteOptions;

    invoke-direct {v0}, Lcom/here/android/mpa/routing/RouteOptions;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 834
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;

    invoke-virtual {v0, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteCount(I)Lcom/here/android/mpa/routing/RouteOptions;

    .line 835
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;

    sget-object v1, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/routing/RouteOptions;->setTransportMode(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 836
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->gasRouteOptions:Lcom/here/android/mpa/routing/RouteOptions;

    sget-object v1, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 838
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 839
    iget-object v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$7;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$7;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 846
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "mark available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 848
    :cond_0
    return-void
.end method

.method public onClearTestObdLowFuelLevel(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$ClearTestObdLowFuelLevel;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$ClearTestObdLowFuelLevel;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset()V

    .line 177
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->fuelGlanceDismissTime:J

    .line 178
    return-void
.end method

.method public onFuelAddedTestEvent(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelAddedTestEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$FuelAddedTestEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->setFuelLevelBackToNormal()V

    .line 172
    return-void
.end method

.method public onNewRouteAdded(Lcom/navdy/hud/app/maps/MapEvents$NewRouteAdded;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$NewRouteAdded;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 182
    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "new route added, resetting gas routes"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 186
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    .line 188
    .local v0, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$NewRouteAdded;->rerouteReason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    if-nez v1, :cond_1

    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    sget-object v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->LOW_FUEL_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 191
    :cond_1
    return-void
.end method

.method public onObdPidChangeEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 155
    iget-object v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;->pid:Lcom/navdy/obd/Pid;

    invoke-virtual {v0}, Lcom/navdy/obd/Pid;->getId()I

    move-result v0

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_0

    .line 159
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->checkFuelLevel()V

    goto :goto_0
.end method

.method public onTestObdLowFuelLevel(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$TestObdLowFuelLevel;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$TestObdLowFuelLevel;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->isInTestMode:Z

    .line 165
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->checkFuelLevel()V

    .line 167
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 316
    sget-object v0, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "reset"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 317
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;->reset(I)V

    .line 318
    return-void
.end method

.method public routeToGasStation()V
    .locals 3

    .prologue
    .line 194
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager$1;-><init>(Lcom/navdy/hud/app/framework/fuel/FuelRoutingManager;)V

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 249
    return-void
.end method

.method public showFindingGasStationToast()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 812
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 814
    .local v7, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v8

    .line 815
    .local v8, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v0, "toast#find#gas"

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 816
    const-string v0, "toast#find#gas"

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 818
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 819
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0x5dc

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 820
    const-string v0, "8"

    const v1, 0x7f020118

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 821
    const-string v0, "4"

    const v1, 0x7f090256

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 824
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "toast#find#gas"

    const/4 v3, 0x0

    const/4 v4, 0x1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZZ)V

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 825
    return-void
.end method
