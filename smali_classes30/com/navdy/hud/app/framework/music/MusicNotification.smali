.class public Lcom/navdy/hud/app/framework/music/MusicNotification;
.super Ljava/lang/Object;
.source "MusicNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;


# static fields
.field private static final ARTWORK_ALPHA_NOT_PLAYING:F = 0.5f

.field private static final ARTWORK_ALPHA_PLAYING:F = 1.0f

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final MUSIC_TIMEOUT:I = 0x1388

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

.field private bus:Lcom/squareup/otto/Bus;

.field private choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

.field private container:Landroid/view/ViewGroup;

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private handler:Landroid/os/Handler;

.field private image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

.field private isKeyPressedDown:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private lastEventIdentifier:Ljava/lang/String;

.field private musicManager:Lcom/navdy/hud/app/manager/MusicManager;

.field private progressBar:Landroid/widget/ProgressBar;

.field private subTitle:Landroid/widget/TextView;

.field private title:Landroid/widget/TextView;

.field private trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/music/MusicNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/manager/MusicManager;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "musicManager"    # Lcom/navdy/hud/app/manager/MusicManager;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    sget-object v0, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .line 72
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->isKeyPressedDown:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 76
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    .line 77
    iput-object p2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->bus:Lcom/squareup/otto/Bus;

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->handler:Landroid/os/Handler;

    .line 79
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/music/MusicNotification;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/MusicNotification;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/music/MusicNotification;->setDefaultImage(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/music/MusicNotification;)Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/MusicNotification;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/music/MusicNotification;Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/MusicNotification;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/framework/music/MusicNotification;->setImage(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/music/MusicNotification;)Lcom/navdy/hud/app/framework/music/AlbumArtImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/MusicNotification;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    return-object v0
.end method

.method private setDefaultImage(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 367
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/music/MusicNotification$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/framework/music/MusicNotification$2;-><init>(Lcom/navdy/hud/app/framework/music/MusicNotification;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 373
    return-void
.end method

.method private setImage(Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1, "artwork"    # Landroid/graphics/Bitmap;
    .param p2, "animate"    # Z

    .prologue
    .line 376
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/music/MusicNotification$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/framework/music/MusicNotification$3;-><init>(Lcom/navdy/hud/app/framework/music/MusicNotification;Landroid/graphics/Bitmap;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 382
    return-void
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "navdy#music#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 137
    const/16 v0, 0x1388

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->MUSIC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 94
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00c3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->title:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->subTitle:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00cf

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0159

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->progressBar:Landroid/widget/ProgressBar;

    .line 99
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 174
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 399
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAlbumArtUpdate(Lokio/ByteString;Z)V
    .locals 3
    .param p1, "photo"    # Lokio/ByteString;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "animate"    # Z

    .prologue
    .line 327
    if-nez p1, :cond_0

    .line 328
    sget-object v0, Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "ByteString image to set in notification is null"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 329
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/framework/music/MusicNotification;->setDefaultImage(Z)V

    .line 364
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/music/MusicNotification$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/framework/music/MusicNotification$1;-><init>(Lcom/navdy/hud/app/framework/music/MusicNotification;Lokio/ByteString;Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 386
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 166
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 394
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 9
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 190
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v1, :cond_0

    move v2, v0

    .line 241
    :goto_0
    return v2

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    if-eqz v1, :cond_1

    .line 194
    sget-object v1, Lcom/navdy/hud/app/framework/music/MusicNotification$4;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    :cond_1
    move v2, v0

    .line 241
    goto :goto_0

    .line 196
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->moveSelectionLeft()V

    goto :goto_0

    .line 200
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 201
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->moveSelectionRight()V

    goto :goto_0

    .line 204
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 205
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->getSelectedItem()Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    move-result-object v7

    .line 206
    .local v7, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    if-nez v7, :cond_2

    .line 207
    sget-object v1, Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Choice layout selected item is null, returning"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    move v2, v0

    .line 208
    goto :goto_0

    .line 210
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager;->CONTROLS:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    iget v3, v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    aget-object v6, v1, v3

    .line 211
    .local v6, "action":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    sget-object v1, Lcom/navdy/hud/app/framework/music/MusicNotification$4;->$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl:[I

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_1

    move v2, v0

    .line 233
    goto :goto_0

    .line 214
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v0, :cond_3

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->isKeyPressedDown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v6, v1}, Lcom/navdy/hud/app/manager/MusicManager;->executeMediaControl(Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)V

    .line 217
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->executeSelectedItem(Z)V

    goto :goto_0

    .line 222
    :pswitch_4
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->executeSelectedItem(Z)V

    .line 223
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 225
    .local v4, "args":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->getMusicMenuPath()Ljava/lang/String;

    move-result-object v8

    .line 226
    .local v8, "path":Ljava/lang/String;
    if-nez v8, :cond_4

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->MUSIC:Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 229
    :cond_4
    const-string v0, "MENU_PATH"

    invoke-virtual {v4, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicNotification;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;ZLcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 236
    .end local v4    # "args":Landroid/os/Bundle;
    .end local v6    # "action":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .end local v7    # "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    .end local v8    # "path":Ljava/lang/String;
    :pswitch_5
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    goto/16 :goto_0

    .line 194
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 211
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onKeyEvent(Landroid/view/KeyEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 247
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-static {v2}, Lcom/navdy/hud/app/manager/InputManager;->isCenterKey(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 251
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->getSelectedItem()Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    move-result-object v1

    .line 252
    .local v1, "selectedItem":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    if-nez v1, :cond_2

    .line 253
    sget-object v2, Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Choice layout selected item is null, returning"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager;->CONTROLS:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    iget v3, v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    aget-object v0, v2, v3

    .line 261
    .local v0, "action":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    if-ne v0, v2, :cond_0

    .line 264
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 265
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-eqz v2, :cond_4

    .line 266
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->isKeyPressedDown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v2, p1, v0, v3}, Lcom/navdy/hud/app/manager/MusicManager;->handleKeyEvent(Landroid/view/KeyEvent;Lcom/navdy/hud/app/manager/MusicManager$MediaControl;Z)V

    .line 268
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->isKeyPressedDown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 269
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->keyDownSelectedItem()V

    goto :goto_0

    .line 270
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->isKeyPressedDown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->keyUpSelectedItem()V

    goto :goto_0
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 162
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 1
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/manager/MusicManager;->addMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/music/MusicNotification;->updateProgressBar(I)V

    .line 120
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/manager/MusicManager;->removeMusicUpdateListener(Lcom/navdy/hud/app/manager/MusicManager$MusicUpdateListener;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 133
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 390
    return-void
.end method

.method public onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V
    .locals 3
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "willOpenNotification"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 285
    .local p2, "mediaControls":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/hud/app/manager/MusicManager$MediaControl;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 287
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v1, :cond_0

    .line 323
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-static {p1}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v0

    .line 293
    .local v0, "songIdentifier":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->lastEventIdentifier:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->lastEventIdentifier:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 294
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 297
    :cond_1
    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->lastEventIdentifier:Ljava/lang/String;

    .line 299
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 300
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->subTitle:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    :goto_1
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 306
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->title:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    :goto_2
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->currentPosition:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 312
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->progressBar:Landroid/widget/ProgressBar;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 313
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->progressBar:Landroid/widget/ProgressBar;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 316
    :cond_2
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-static {v1}, Lcom/navdy/hud/app/manager/MusicManager;->tryingToPlay(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 317
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setAlpha(F)V

    .line 322
    :goto_3
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->choiceLayout:Lcom/navdy/hud/app/framework/music/MediaControllerLayout;

    invoke-virtual {v1, p2}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->updateControls(Ljava/util/Set;)V

    goto :goto_0

    .line 302
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->subTitle:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 308
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->title:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 319
    :cond_5
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->image:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setAlpha(F)V

    goto :goto_3
.end method

.method public onUpdate()V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentTrack()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentControls()Ljava/util/Set;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/hud/app/framework/music/MusicNotification;->onTrackUpdated(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/util/Set;Z)V

    .line 125
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->musicManager:Lcom/navdy/hud/app/manager/MusicManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->getCurrentPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/music/MusicNotification;->updateProgressBar(I)V

    .line 126
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 278
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicNotification;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 281
    :cond_0
    return-void
.end method
