.class Lcom/navdy/hud/app/framework/music/MusicNotification$1;
.super Ljava/lang/Object;
.source "MusicNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/music/MusicNotification;->onAlbumArtUpdate(Lokio/ByteString;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/music/MusicNotification;

.field final synthetic val$animate:Z

.field final synthetic val$photo:Lokio/ByteString;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/music/MusicNotification;Lokio/ByteString;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/music/MusicNotification;

    .prologue
    .line 333
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicNotification;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->val$photo:Lokio/ByteString;

    iput-boolean p3, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->val$animate:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 336
    # getter for: Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "PhotoUpdate runnable "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->val$photo:Lokio/ByteString;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 337
    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->val$photo:Lokio/ByteString;

    invoke-virtual {v5}, Lokio/ByteString;->toByteArray()[B

    move-result-object v1

    .line 339
    .local v1, "byteArray":[B
    if-eqz v1, :cond_0

    array-length v5, v1

    if-gtz v5, :cond_2

    .line 340
    :cond_0
    # getter for: Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Received photo has null or empty byte array"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 341
    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicNotification;

    const/4 v6, 0x1

    # invokes: Lcom/navdy/hud/app/framework/music/MusicNotification;->setDefaultImage(Z)V
    invoke-static {v5, v6}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$100(Lcom/navdy/hud/app/framework/music/MusicNotification;Z)V

    .line 362
    :cond_1
    :goto_0
    return-void

    .line 345
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00e1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 347
    .local v4, "size":I
    const/4 v0, 0x0

    .line 348
    .local v0, "artwork":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 351
    .local v3, "scaled":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicNotification;

    # getter for: Lcom/navdy/hud/app/framework/music/MusicNotification;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$200(Lcom/navdy/hud/app/framework/music/MusicNotification;)Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    move-result-object v5

    invoke-static {v1, v4, v4, v5}, Lcom/navdy/service/library/util/ScalingUtilities;->decodeByteArray([BIILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 352
    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicNotification;

    # getter for: Lcom/navdy/hud/app/framework/music/MusicNotification;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;
    invoke-static {v5}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$200(Lcom/navdy/hud/app/framework/music/MusicNotification;)Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    move-result-object v5

    invoke-static {v0, v4, v4, v5}, Lcom/navdy/service/library/util/ScalingUtilities;->createScaledBitmap(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 353
    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicNotification;

    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->val$animate:Z

    # invokes: Lcom/navdy/hud/app/framework/music/MusicNotification;->setImage(Landroid/graphics/Bitmap;Z)V
    invoke-static {v5, v3, v6}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$300(Lcom/navdy/hud/app/framework/music/MusicNotification;Landroid/graphics/Bitmap;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    if-eqz v0, :cond_1

    if-eq v0, v3, :cond_1

    .line 359
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 354
    :catch_0
    move-exception v2

    .line 355
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    # getter for: Lcom/navdy/hud/app/framework/music/MusicNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Error updating the artwork received "

    invoke-virtual {v5, v6, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 356
    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicNotification;

    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/music/MusicNotification$1;->val$animate:Z

    # invokes: Lcom/navdy/hud/app/framework/music/MusicNotification;->setDefaultImage(Z)V
    invoke-static {v5, v6}, Lcom/navdy/hud/app/framework/music/MusicNotification;->access$100(Lcom/navdy/hud/app/framework/music/MusicNotification;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358
    if-eqz v0, :cond_1

    if-eq v0, v3, :cond_1

    .line 359
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 358
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    .line 359
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    throw v5
.end method
