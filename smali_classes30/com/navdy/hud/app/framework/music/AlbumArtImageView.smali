.class public Lcom/navdy/hud/app/framework/music/AlbumArtImageView;
.super Landroid/widget/ImageView;
.source "AlbumArtImageView.java"


# static fields
.field private static final ARTWORK_TRANSITION_DURATION:I = 0x3e8

.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final artworkLock:Ljava/lang/Object;

.field private defaultArtwork:Landroid/graphics/drawable/BitmapDrawable;

.field private handler:Landroid/os/Handler;

.field private isAnimatingArtwork:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mask:Z

.field private nextArtwork:Landroid/graphics/Bitmap;

.field private nextArtworkHash:Ljava/lang/String;

.field private paint:Landroid/graphics/Paint;

.field private radialGradient:Landroid/graphics/RadialGradient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->isAnimatingArtwork:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->artworkLock:Ljava/lang/Object;

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->handler:Landroid/os/Handler;

    .line 61
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->init(Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->artworkLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtworkHash:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->isAnimatingArtwork:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->animateArtwork()V

    return-void
.end method

.method private addMask(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "src"    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 184
    if-nez p1, :cond_0

    move-object v6, v2

    .line 198
    :goto_0
    return-object v6

    .line 188
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 189
    .local v8, "w":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 190
    .local v7, "h":I
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v7, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 191
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 192
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, p1, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 194
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->paint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->radialGradient:Landroid/graphics/RadialGradient;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 195
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->paint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 196
    int-to-float v3, v8

    int-to-float v4, v7

    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->paint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private animateArtwork()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 123
    iget-object v7, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->artworkLock:Ljava/lang/Object;

    monitor-enter v7

    .line 124
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtworkHash:Ljava/lang/String;

    .line 125
    .local v1, "artworkHash":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtwork:Landroid/graphics/Bitmap;

    .line 126
    .local v0, "artwork":Landroid/graphics/Bitmap;
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    const/4 v6, 0x2

    new-array v4, v6, [Landroid/graphics/drawable/Drawable;

    .line 130
    .local v4, "drawables":[Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 131
    .local v2, "currentDrawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_0

    .line 132
    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v6, v8}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v6, v4, v8

    .line 138
    :goto_0
    if-eqz v0, :cond_1

    .line 139
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v6, v4, v9

    .line 144
    :goto_1
    new-instance v3, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v3, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 145
    .local v3, "drawable":Landroid/graphics/drawable/TransitionDrawable;
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 146
    invoke-virtual {v3, v9}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 147
    const/16 v6, 0x3e8

    invoke-virtual {v3, v6}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 150
    iget-object v6, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->handler:Landroid/os/Handler;

    new-instance v7, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;

    invoke-direct {v7, p0, v4, v1}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;-><init>(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;[Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    const-wide/16 v8, 0x3e8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 167
    return-void

    .line 126
    .end local v0    # "artwork":Landroid/graphics/Bitmap;
    .end local v1    # "artworkHash":Ljava/lang/String;
    .end local v2    # "currentDrawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "drawable":Landroid/graphics/drawable/TransitionDrawable;
    .end local v4    # "drawables":[Landroid/graphics/drawable/Drawable;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .restart local v0    # "artwork":Landroid/graphics/Bitmap;
    .restart local v1    # "artworkHash":Ljava/lang/String;
    .restart local v2    # "currentDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v4    # "drawables":[Landroid/graphics/drawable/Drawable;
    :cond_0
    move-object v5, v2

    .line 134
    check-cast v5, Landroid/graphics/drawable/LayerDrawable;

    .line 135
    .local v5, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    invoke-virtual {v5, v8}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v8

    goto :goto_0

    .line 141
    .end local v5    # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    :cond_1
    sget-object v6, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Bitmap image is null"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 142
    iget-object v6, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->defaultArtwork:Landroid/graphics/drawable/BitmapDrawable;

    aput-object v6, v4, v9

    goto :goto_1
.end method

.method private drawImmediately()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 170
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->isAnimatingArtwork:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 171
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 172
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/graphics/drawable/Drawable;

    .line 173
    .local v0, "layers":[Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtwork:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 174
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtwork:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v1, v0, v4

    .line 179
    :goto_0
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 180
    return-void

    .line 176
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Bitmap image is null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->defaultArtwork:Landroid/graphics/drawable/BitmapDrawable;

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method private init(Landroid/util/AttributeSet;)V
    .locals 13
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 65
    const v8, 0x7f020181

    .line 66
    .local v8, "defaultArtworkResource":I
    if-eqz p1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/navdy/hud/app/R$styleable;->AlbumArtImageView:[I

    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 68
    .local v7, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v7, v11, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->mask:Z

    .line 69
    const v0, 0x7f020181

    invoke-virtual {v7, v12, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    .line 70
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 73
    .end local v7    # "a":Landroid/content/res/TypedArray;
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    .line 74
    .local v9, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->mask:Z

    if-eqz v0, :cond_1

    .line 75
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->paint:Landroid/graphics/Paint;

    .line 76
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0011

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    .line 79
    .local v4, "colors":[I
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v2, v12, [I

    const v3, 0x10100f4

    aput v3, v2, v11

    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 80
    .restart local v7    # "a":Landroid/content/res/TypedArray;
    const/16 v0, 0x82

    invoke-virtual {v7, v11, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v0

    .line 81
    .local v1, "radius":F
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 83
    new-instance v0, Landroid/graphics/RadialGradient;

    const/4 v5, 0x0

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->radialGradient:Landroid/graphics/RadialGradient;

    .line 84
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v9}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->addMask(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->defaultArtwork:Landroid/graphics/drawable/BitmapDrawable;

    .line 89
    .end local v1    # "radius":F
    .end local v4    # "colors":[I
    .end local v7    # "a":Landroid/content/res/TypedArray;
    :goto_0
    new-array v10, v12, [Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->defaultArtwork:Landroid/graphics/drawable/BitmapDrawable;

    aput-object v0, v10, v11

    .line 90
    .local v10, "layers":[Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v10}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 91
    return-void

    .line 86
    .end local v10    # "layers":[Landroid/graphics/drawable/Drawable;
    :cond_1
    iput-object v9, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->defaultArtwork:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method


# virtual methods
.method public setArtworkBitmap(Landroid/graphics/Bitmap;Z)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "animate"    # Z

    .prologue
    .line 94
    invoke-static {p1}, Lcom/navdy/service/library/util/IOUtils;->hashForBitmap(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "hash":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtworkHash:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    sget-object v1, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Already set this artwork, ignoring"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 116
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 100
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    monitor-enter p0

    .line 101
    :try_start_0
    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtworkHash:Ljava/lang/String;

    .line 102
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->mask:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->addMask(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtwork:Landroid/graphics/Bitmap;

    .line 103
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    if-nez p2, :cond_2

    .line 106
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->drawImmediately()V

    goto :goto_0

    .line 103
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->isAnimatingArtwork:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-nez v1, :cond_3

    .line 111
    sget-object v1, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Already animating"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_3
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->animateArtwork()V

    goto :goto_0
.end method
