.class Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;
.super Ljava/lang/Object;
.source "MusicDetailsView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/music/MusicDetailsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/music/MusicDetailsView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->close()V

    .line 93
    return-void
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z

    move-result v0

    return v0
.end method

.method public onBindToView(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Landroid/view/View;ILcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;)V
    .locals 0
    .param p1, "model"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "state"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;

    .prologue
    .line 73
    return-void
.end method

.method public onFastScrollEnd()V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public onFastScrollStart()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public onItemSelected(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 76
    return-void
.end method

.method public onLoad()V
    .locals 2

    .prologue
    .line 63
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsView;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onLoad"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->resetSelectedItem()V

    .line 65
    return-void
.end method

.method public onScrollIdle()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public select(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;->this$0:Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z

    .line 59
    return-void
.end method

.method public showToolTip()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method
