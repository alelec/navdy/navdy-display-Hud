.class public Lcom/navdy/hud/app/framework/music/MusicDetailsView;
.super Landroid/widget/RelativeLayout;
.source "MusicDetailsView.java"

# interfaces
.implements Lcom/navdy/hud/app/manager/InputManager$IInputHandler;


# static fields
.field private static final musicRightContainerLeftMargin:I

.field private static final musicRightContainerSize:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field albumArt:Landroid/widget/ImageView;

.field albumArtContainer:Landroid/view/ViewGroup;

.field private callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

.field counter:Landroid/widget/TextView;

.field public presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 49
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b00df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->musicRightContainerSize:I

    .line 52
    const v1, 0x7f0b00de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->musicRightContainerLeftMargin:I

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    new-instance v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsView$1;-><init>(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    .line 111
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    invoke-static {p1, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 114
    :cond_0
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 167
    invoke-static {p0}, Lcom/navdy/hud/app/manager/InputManager;->nextContainingHandler(Landroid/view/View;)Lcom/navdy/hud/app/manager/InputManager$IInputHandler;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 145
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 150
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->clear()V

    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->presenter:Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->dropView(Landroid/view/View;)V

    .line 153
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 118
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 119
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 120
    new-instance v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->callback:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;

    const/4 v4, 0x1

    invoke-direct {v2, p0, v3, v4}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;-><init>(Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent$Callback;Z)V

    iput-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    .line 121
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 122
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 125
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->rightContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 126
    .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget v2, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->musicRightContainerSize:I

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 127
    sget v2, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->musicRightContainerLeftMargin:I

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 130
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 131
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 132
    const v2, 0x7f030033

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->albumArtContainer:Landroid/view/ViewGroup;

    .line 133
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->albumArtContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0e013f

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->albumArt:Landroid/widget/ImageView;

    .line 134
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->albumArtContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0e0140

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->counter:Landroid/widget/TextView;

    .line 135
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->albumArt:Landroid/widget/ImageView;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const v4, -0xbbbbbc

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->leftContainer:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->albumArtContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 137
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handleGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z

    move-result v0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->handleKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z

    move-result v0

    return v0
.end method

.method performSelectionAnimation(Ljava/lang/Runnable;I)V
    .locals 1
    .param p1, "endAction"    # Ljava/lang/Runnable;
    .param p2, "delay"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->performSelectionAnimation(Ljava/lang/Runnable;I)V

    .line 172
    return-void
.end method
