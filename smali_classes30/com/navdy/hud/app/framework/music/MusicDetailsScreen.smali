.class public Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;
.super Lcom/navdy/hud/app/screen/BaseScreen;
.source "MusicDetailsScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;,
        Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Module;
    }
.end annotation

.annotation runtime Lflow/Layout;
    value = 0x7f030060
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/navdy/hud/app/screen/BaseScreen;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public getAnimationIn(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 61
    const/4 v0, -0x1

    return v0
.end method

.method public getAnimationOut(Lflow/Flow$Direction;)I
    .locals 1
    .param p1, "direction"    # Lflow/Flow$Direction;

    .prologue
    .line 66
    const/4 v0, -0x1

    return v0
.end method

.method public getDaggerModule()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Module;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Module;-><init>(Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;)V

    return-object v0
.end method

.method public getMortarScopeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreen()Lcom/navdy/service/library/events/ui/Screen;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MUSIC_DETAILS:Lcom/navdy/service/library/events/ui/Screen;

    return-object v0
.end method
