.class public Lcom/navdy/hud/app/framework/music/MusicCollection;
.super Ljava/lang/Object;
.source "MusicCollection.java"


# instance fields
.field public collections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public musicCollectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

.field public tracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicCollection;->collections:Ljava/util/List;

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicCollection;->tracks:Ljava/util/List;

    return-void
.end method
