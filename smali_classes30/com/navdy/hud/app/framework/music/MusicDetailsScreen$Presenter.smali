.class public Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;
.super Lcom/navdy/hud/app/ui/framework/BasePresenter;
.source "MusicDetailsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/hud/app/ui/framework/BasePresenter",
        "<",
        "Lcom/navdy/hud/app/framework/music/MusicDetailsView;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final MUSIC_CONTROL_TRAY_POS:I

.field private static final music_selected_color:I

.field private static final music_unselected_color:I

.field private static final subTitles:[Ljava/lang/String;

.field private static final thumbsDown:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final thumbsUp:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

.field private static final titles:[Ljava/lang/String;


# instance fields
.field private animateIn:Z

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private closed:Z

.field private currentCounter:I

.field private currentTitleSelection:I

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;",
            ">;"
        }
    .end annotation
.end field

.field private handledSelection:Z

.field private keyPressed:Z

.field private registered:Z

.field private thumbPos:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 88
    .local v7, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0d008c

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    .line 89
    const v0, 0x7f0d003e

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    .line 91
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "The Mother We Both Share"

    aput-object v1, v0, v2

    const-string v1, "Tomorrow, Tomorrow, I love you"

    aput-object v1, v0, v3

    const-string v1, "Who Let the Dogs out"

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->titles:[Ljava/lang/String;

    .line 97
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Churches Your Vibe, Your Tribe Vol. 1"

    aput-object v1, v0, v2

    const-string v1, "Old Classics, Vol 2"

    aput-object v1, v0, v3

    const-string v1, "woof woof"

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->subTitles:[Ljava/lang/String;

    .line 103
    const v0, 0x7f0e005e

    const v1, 0x7f02020c

    sget v2, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    sget v3, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    sget v4, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    const-string v5, "Like"

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbsUp:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 111
    const v0, 0x7f0e005d

    const v1, 0x7f02020b

    sget v2, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    sget v3, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    sget v4, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    const-string v5, "Dislike"

    invoke-static/range {v0 .. v6}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbsDown:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    .line 118
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method close()V
    .locals 3

    .prologue
    .line 261
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->closed:Z

    if-eqz v1, :cond_1

    .line 262
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "already closed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->closed:Z

    .line 267
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "close"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 268
    const-string v1, "exit"

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordMenuSelection(Ljava/lang/String;)V

    .line 271
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .line 272
    .local v0, "view":Lcom/navdy/hud/app/framework/music/MusicDetailsView;
    if-eqz v0, :cond_0

    .line 273
    iget-object v1, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    new-instance v2, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter$1;-><init>(Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateOut(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method init(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V
    .locals 23
    .param p1, "view"    # Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .prologue
    .line 188
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    sget-object v8, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    const/4 v9, 0x1

    invoke-virtual {v2, v8, v9}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->setOverrideDefaultKeyEvents(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;Z)V

    .line 189
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v19, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;>;"
    sget-object v2, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->titles:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    aget-object v2, v2, v8

    sget-object v8, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->subTitles:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    aget-object v8, v8, v9

    invoke-static {v2, v8}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleSubtitleViewHolder;->buildModel(Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    .line 196
    const/4 v2, 0x3

    new-array v3, v2, [I

    fill-array-data v3, :array_0

    .line 197
    .local v3, "icons":[I
    const/4 v2, 0x3

    new-array v4, v2, [I

    fill-array-data v4, :array_1

    .line 198
    .local v4, "iconId":[I
    const/4 v2, 0x3

    new-array v5, v2, [I

    const/4 v2, 0x0

    sget v8, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    aput v8, v5, v2

    const/4 v2, 0x1

    sget v8, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    aput v8, v5, v2

    const/4 v2, 0x2

    sget v8, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    aput v8, v5, v2

    .line 199
    .local v5, "iconSelectedColor":[I
    const/4 v2, 0x3

    new-array v6, v2, [I

    const/4 v2, 0x0

    sget v8, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    aput v8, v6, v2

    const/4 v2, 0x1

    sget v8, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    aput v8, v6, v2

    const/4 v2, 0x2

    sget v8, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    aput v8, v6, v2

    .line 200
    .local v6, "iconUnselectedColor":[I
    const v2, 0x7f0e0058

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v7, v5

    invoke-static/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->buildModel(I[I[I[I[I[IIZ)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v17

    .line 201
    .local v17, "iconTray":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    const/4 v7, 0x2

    .line 206
    .local v7, "id":I
    add-int/lit8 v18, v7, 0x1

    .end local v7    # "id":I
    .local v18, "id":I
    const v8, 0x7f0201c4

    sget v9, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    sget v10, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    sget v11, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    const-string v12, "Playlist/Album name"

    const/4 v13, 0x0

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v15

    .line 213
    .local v15, "albumInfo":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v19

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    add-int/lit8 v7, v18, 0x1

    .end local v18    # "id":I
    .restart local v7    # "id":I
    const v9, 0x7f020133

    sget v10, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    sget v11, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    sget v12, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    const-string v13, "Love"

    const/4 v14, 0x0

    move/from16 v8, v18

    invoke-static/range {v8 .. v14}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v20

    .line 224
    .local v20, "love":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbPos:I

    .line 227
    sget-object v2, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbsUp:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    add-int/lit8 v18, v7, 0x1

    .end local v7    # "id":I
    .restart local v18    # "id":I
    const v8, 0x7f0201e1

    sget v9, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    sget v10, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    sget v11, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    const-string v12, "Shuffle"

    const/4 v13, 0x0

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v21

    .line 236
    .local v21, "shuffle":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    add-int/lit8 v7, v18, 0x1

    .end local v18    # "id":I
    .restart local v7    # "id":I
    const v9, 0x7f0201f2

    sget v10, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    sget v11, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    sget v12, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    const-string v13, "Stations"

    const/4 v14, 0x0

    move/from16 v8, v18

    invoke-static/range {v8 .. v14}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v22

    .line 245
    .local v22, "stations":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    add-int/lit8 v18, v7, 0x1

    .end local v7    # "id":I
    .restart local v18    # "id":I
    const v8, 0x7f0200e9

    sget v9, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    sget v10, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_unselected_color:I

    sget v11, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->music_selected_color:I

    const-string v12, "Bookmark"

    const/4 v13, 0x0

    invoke-static/range {v7 .. v13}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v16

    .line 254
    .local v16, "bookmark":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->data:Ljava/util/List;

    .line 257
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->data:Ljava/util/List;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v2, v8, v9, v10}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->updateView(Ljava/util/List;IZ)V

    .line 258
    return-void

    .line 196
    :array_0
    .array-data 4
        0x7f0201c9
        0x7f0201c2
        0x7f020195
    .end array-data

    .line 197
    :array_1
    .array-data 4
        0x7f0e005c
        0x7f0e005b
        0x7f0e0059
    .end array-data
.end method

.method isClosed()Z
    .locals 1

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->closed:Z

    return v0
.end method

.method isItemClickable(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 346
    const/4 v0, 0x1

    return v0
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 351
    sget-object v0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$1;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 357
    :goto_0
    return-void

    .line 353
    :pswitch_0
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "disconnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->close()V

    goto :goto_0

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyEvent(Landroid/view/KeyEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/KeyEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 362
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    invoke-static {v8}, Lcom/navdy/hud/app/manager/InputManager;->isCenterKey(I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .line 367
    .local v5, "view":Lcom/navdy/hud/app/framework/music/MusicDetailsView;
    if-eqz v5, :cond_0

    .line 371
    iget-object v8, v5, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->isCloseMenuVisible()Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, v5, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentPosition()I

    move-result v8

    if-nez v8, :cond_0

    .line 375
    iget-object v8, v5, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v8, v8, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    invoke-virtual {v8}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->getCurrentViewHolder()Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;

    move-result-object v4

    .line 376
    .local v4, "vh":Lcom/navdy/hud/app/ui/component/vlist/viewholder/VerticalViewHolder;
    if-eqz v4, :cond_0

    instance-of v8, v4, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    if-eqz v8, :cond_0

    move-object v1, v4

    .line 380
    check-cast v1, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;

    .line 381
    .local v1, "iconOptionsViewHolder":Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->getCurrentSelection()I

    move-result v2

    .line 382
    .local v2, "selection":I
    if-eq v2, v6, :cond_0

    .line 387
    const/4 v8, 0x2

    if-ne v2, v8, :cond_4

    move v0, v6

    .line 390
    .local v0, "fwd":Z
    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_6

    .line 391
    iget-boolean v7, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->keyPressed:Z

    if-nez v7, :cond_2

    .line 392
    iput-boolean v6, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->keyPressed:Z

    .line 393
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->selectionDown()V

    .line 395
    :cond_2
    iget v7, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentCounter:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 396
    .local v3, "text":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 397
    iget v6, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentCounter:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentCounter:I

    .line 403
    :cond_3
    :goto_2
    iget-object v6, v5, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->counter:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .end local v0    # "fwd":Z
    .end local v3    # "text":Ljava/lang/String;
    :cond_4
    move v0, v7

    .line 387
    goto :goto_1

    .line 399
    .restart local v0    # "fwd":Z
    .restart local v3    # "text":Ljava/lang/String;
    :cond_5
    iget v7, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentCounter:I

    if-le v7, v6, :cond_3

    .line 400
    iget v6, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentCounter:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentCounter:I

    goto :goto_2

    .line 404
    .end local v3    # "text":Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-ne v8, v6, :cond_0

    .line 405
    iget-boolean v6, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->keyPressed:Z

    if-eqz v6, :cond_0

    .line 406
    iput-boolean v7, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->keyPressed:Z

    .line 407
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconOptionsViewHolder;->selectionUp()V

    .line 408
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    goto :goto_0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 138
    iput v1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentCounter:I

    .line 139
    iput v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    .line 140
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->animateIn:Z

    .line 141
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->keyPressed:Z

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    .line 143
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 144
    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->registered:Z

    .line 145
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->updateView()V

    .line 146
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onLoad(Landroid/os/Bundle;)V

    .line 147
    return-void
.end method

.method public onUnload()V
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->registered:Z

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->registered:Z

    .line 153
    iget-object v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 155
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    .line 156
    invoke-super {p0}, Lcom/navdy/hud/app/ui/framework/BasePresenter;->onUnload()V

    .line 157
    return-void
.end method

.method reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V
    .locals 1
    .param p1, "view"    # Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .prologue
    const/4 v0, 0x0

    .line 160
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->closed:Z

    .line 161
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->handledSelection:Z

    .line 162
    if-eqz p1, :cond_0

    .line 163
    iget-object v0, p1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->unlock()V

    .line 165
    :cond_0
    return-void
.end method

.method resetSelectedItem()V
    .locals 3

    .prologue
    .line 168
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "resetSelectedItem"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 169
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->handledSelection:Z

    .line 170
    iget-boolean v1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->animateIn:Z

    if-nez v1, :cond_0

    .line 171
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->animateIn:Z

    .line 172
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .line 173
    .local v0, "view":Lcom/navdy/hud/app/framework/music/MusicDetailsView;
    if-eqz v0, :cond_0

    .line 174
    iget-object v1, v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->animateIn(Landroid/animation/Animator$AnimatorListener;)V

    .line 177
    .end local v0    # "view":Lcom/navdy/hud/app/framework/music/MusicDetailsView;
    :cond_0
    return-void
.end method

.method selectItem(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;)Z
    .locals 5
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;

    .prologue
    .line 284
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " subid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 285
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->handledSelection:Z

    if-eqz v2, :cond_0

    .line 286
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "already handled ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 287
    const/4 v2, 0x1

    .line 337
    :goto_0
    return v2

    .line 289
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .line 290
    .local v1, "view":Lcom/navdy/hud/app/framework/music/MusicDetailsView;
    if-eqz v1, :cond_1

    .line 291
    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->id:I

    sparse-switch v2, :sswitch_data_0

    .line 332
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->close()V

    .line 337
    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->handledSelection:Z

    goto :goto_0

    .line 293
    :sswitch_0
    iget v2, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->subId:I

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 306
    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    goto :goto_1

    .line 295
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    .line 297
    iget v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    sget-object v3, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->titles:[Ljava/lang/String;

    array-length v3, v3

    if-ne v2, v3, :cond_2

    .line 298
    const/4 v2, 0x0

    iput v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    .line 300
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->titles:[Ljava/lang/String;

    iget v3, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    aget-object v2, v2, v3

    sget-object v3, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->subTitles:[Ljava/lang/String;

    iget v4, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/TitleSubtitleViewHolder;->buildModel(Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v0

    .line 301
    .local v0, "titleModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    iget v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->currentTitleSelection:I

    .line 302
    iget-object v2, v1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget v3, p1, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;->pos:I

    invoke-virtual {v2, v3, v0}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_1

    .line 310
    .end local v0    # "titleModel":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    goto :goto_1

    .line 314
    :pswitch_3
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    goto :goto_1

    .line 320
    :sswitch_1
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "thumbsup"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 321
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    .line 322
    iget-object v2, v1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget v3, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbPos:I

    sget-object v4, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbsUp:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_1

    .line 326
    :sswitch_2
    # getter for: Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "thumbsdown"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->reset(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    .line 328
    iget-object v2, v1, Lcom/navdy/hud/app/framework/music/MusicDetailsView;->vmenuComponent:Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;

    iget-object v2, v2, Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;->verticalList:Lcom/navdy/hud/app/ui/component/vlist/VerticalList;

    iget v3, p0, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbPos:I

    sget-object v4, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->thumbsDown:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->refreshData(ILcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;)V

    goto :goto_1

    .line 291
    :sswitch_data_0
    .sparse-switch
        0x7f0e0058 -> :sswitch_0
        0x7f0e005d -> :sswitch_1
        0x7f0e005e -> :sswitch_2
    .end sparse-switch

    .line 293
    :pswitch_data_0
    .packed-switch 0x7f0e0059
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected updateView()V
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/music/MusicDetailsView;

    .line 181
    .local v0, "view":Lcom/navdy/hud/app/framework/music/MusicDetailsView;
    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/framework/music/MusicDetailsScreen$Presenter;->init(Lcom/navdy/hud/app/framework/music/MusicDetailsView;)V

    .line 184
    :cond_0
    return-void
.end method
