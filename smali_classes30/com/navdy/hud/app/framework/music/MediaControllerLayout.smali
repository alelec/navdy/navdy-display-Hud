.class public Lcom/navdy/hud/app/framework/music/MediaControllerLayout;
.super Lcom/navdy/hud/app/ui/component/ChoiceLayout;
.source "MediaControllerLayout.java"


# static fields
.field public static final MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private availableMediaControls:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    .line 30
    sget-object v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const v2, 0x7f0201c3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const v2, 0x7f0201a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const v2, 0x7f0201ca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const v2, 0x7f020196

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const v2, 0x7f020193

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU_DEEP:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    const v2, 0x7f020192

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;-><init>(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method private getChoicesForControls()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v0, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->isMusicBrowsingEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 109
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/MusicManager;->getMusicMenuPath()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 110
    sget-object v4, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU_DEEP:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 114
    .local v3, "iconRes":I
    :goto_0
    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    invoke-direct {v2, v3, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;-><init>(II)V

    .line 115
    .local v2, "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    new-instance v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v5

    invoke-direct {v4, v2, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    .end local v2    # "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    .end local v3    # "iconRes":I
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 119
    sget-object v4, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 120
    .restart local v3    # "iconRes":I
    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    invoke-direct {v2, v3, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;-><init>(II)V

    .line 121
    .restart local v2    # "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    new-instance v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v5

    invoke-direct {v4, v2, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    .end local v2    # "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    .end local v3    # "iconRes":I
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PLAY:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 125
    .local v1, "control":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    :goto_1
    sget-object v4, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 126
    .restart local v3    # "iconRes":I
    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    invoke-direct {v2, v3, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;-><init>(II)V

    .line 127
    .restart local v2    # "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    new-instance v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v5

    invoke-direct {v4, v2, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v4, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 130
    sget-object v4, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 131
    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    .end local v2    # "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    invoke-direct {v2, v3, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;-><init>(II)V

    .line 132
    .restart local v2    # "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    new-instance v4, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->NEXT:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v5

    invoke-direct {v4, v2, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_2
    return-object v0

    .line 112
    .end local v1    # "control":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .end local v2    # "icon":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;
    .end local v3    # "iconRes":I
    :cond_3
    sget-object v4, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->MEDIA_CONTROL_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .restart local v3    # "iconRes":I
    goto/16 :goto_0

    .line 123
    .end local v3    # "iconRes":I
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PAUSE:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    goto :goto_1
.end method

.method private getDefaultSelection(Ljava/util/List;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    const/4 v1, 0x0

    .line 92
    .local v1, "selection":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    .line 93
    .local v0, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    iget v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    sget-object v4, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->MUSIC_MENU:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v4

    if-eq v3, v4, :cond_1

    iget v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->id:I

    sget-object v4, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->PREVIOUS:Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    .line 94
    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/MusicManager$MediaControl;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 95
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    .end local v0    # "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    :cond_2
    return v1
.end method

.method private updateChoices(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->choices:Ljava/util/List;

    .line 143
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->choices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 144
    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->choices:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    .line 145
    .local v0, "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->choiceContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 146
    .local v2, "imageView":Landroid/widget/ImageView;
    iget-object v3, v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;->icon:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;

    iget v3, v3, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Icon;->resIdSelected:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 143
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 148
    .end local v0    # "choice":Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;
    .end local v2    # "imageView":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method


# virtual methods
.method protected isHighlightPersistent()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x1

    return v0
.end method

.method public updateControls(Ljava/util/Set;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/manager/MusicManager$MediaControl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "controls":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/hud/app/manager/MusicManager$MediaControl;>;"
    const/4 v4, 0x0

    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "changed":Z
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 57
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    .line 58
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->setVisibility(I)V

    .line 87
    :cond_1
    :goto_0
    return-void

    .line 60
    :cond_2
    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v6

    if-eq v5, v6, :cond_4

    .line 62
    :cond_3
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    .line 63
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->getChoicesForControls()Ljava/util/List;

    move-result-object v1

    .line 64
    .local v1, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    sget-object v5, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;->ICON:Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->getDefaultSelection(Ljava/util/List;)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {p0, v5, v1, v6, v7}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->setChoices(Lcom/navdy/hud/app/ui/component/ChoiceLayout$Mode;Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout$IListener;)V

    .line 65
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->setHighlightVisibility(I)V

    .line 66
    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->setVisibility(I)V

    goto :goto_0

    .line 68
    .end local v1    # "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    :cond_4
    sget-object v5, Lcom/navdy/hud/app/manager/MusicManager;->CONTROLS:[Lcom/navdy/hud/app/manager/MusicManager$MediaControl;

    array-length v6, v5

    :goto_1
    if-ge v4, v6, :cond_5

    aget-object v2, v5, v4

    .line 69
    .local v2, "control":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    iget-object v8, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    invoke-interface {v8, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eq v7, v8, :cond_6

    .line 70
    const/4 v0, 0x1

    .line 71
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->availableMediaControls:Ljava/util/Set;

    .line 75
    .end local v2    # "control":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    :cond_5
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->getChoicesForControls()Ljava/util/List;

    move-result-object v1

    .line 76
    .restart local v1    # "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    if-eqz v0, :cond_7

    .line 78
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->updateChoices(Ljava/util/List;)V

    goto :goto_0

    .line 68
    .end local v1    # "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    .restart local v2    # "control":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 80
    .end local v2    # "control":Lcom/navdy/hud/app/manager/MusicManager$MediaControl;
    .restart local v1    # "choices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;>;"
    :cond_7
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->getSelectedItemIndex()I

    move-result v3

    .line 81
    .local v3, "selected":I
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->isMusicBrowsingEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v3, :cond_1

    .line 82
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->getDefaultSelection(Ljava/util/List;)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/framework/music/MediaControllerLayout;->setSelectedItem(I)Z

    goto :goto_0
.end method
