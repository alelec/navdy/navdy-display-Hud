.class Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;
.super Ljava/lang/Object;
.source "AlbumArtImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->animateArtwork()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

.field final synthetic val$artworkHash:Ljava/lang/String;

.field final synthetic val$drawables:[Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;[Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->this$0:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->val$drawables:[Landroid/graphics/drawable/Drawable;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->val$artworkHash:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 154
    new-array v0, v3, [Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->val$drawables:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v2, v3

    aput-object v2, v0, v5

    .line 155
    .local v0, "layers":[Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->this$0:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    new-instance v3, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->this$0:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    # getter for: Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->artworkLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->access$000(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 158
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->val$artworkHash:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->this$0:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    # getter for: Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->nextArtworkHash:Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->access$100(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    .line 159
    .local v1, "sameArtwork":Z
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    if-eqz v1, :cond_0

    .line 161
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->this$0:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    # getter for: Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->isAnimatingArtwork:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->access$200(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 165
    :goto_0
    return-void

    .line 159
    .end local v1    # "sameArtwork":Z
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 163
    .restart local v1    # "sameArtwork":Z
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/music/AlbumArtImageView$1;->this$0:Lcom/navdy/hud/app/framework/music/AlbumArtImageView;

    # invokes: Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->animateArtwork()V
    invoke-static {v2}, Lcom/navdy/hud/app/framework/music/AlbumArtImageView;->access$300(Lcom/navdy/hud/app/framework/music/AlbumArtImageView;)V

    goto :goto_0
.end method
