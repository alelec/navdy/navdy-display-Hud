.class synthetic Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;
.super Ljava/lang/Object;
.source "VoiceAssistNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 277
    invoke-static {}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->values()[Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_STOPPED:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->VOICE_ASSIST_LISTENING:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 236
    :goto_3
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LONG_PRESS:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    .line 122
    :goto_4
    invoke-static {}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->values()[Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    .line 236
    :catch_2
    move-exception v0

    goto :goto_4

    .line 277
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_1

    :catch_6
    move-exception v0

    goto :goto_0
.end method
