.class public Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;
.super Ljava/lang/Object;
.source "VoiceSearchHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;
    }
.end annotation


# static fields
.field private static final searchAgainSubTitle:Ljava/lang/String;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private context:Landroid/content/Context;

.field private handler:Landroid/os/Handler;

.field private response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

.field private showVoiceSearchTipsToUser:Z

.field private voiceSearchUserInterface:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 60
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f09022e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->searchAgainSubTitle:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/util/FeatureUtil;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;
    .param p3, "featureUtil"    # Lcom/navdy/hud/app/util/FeatureUtil;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showVoiceSearchTipsToUser:Z

    .line 68
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->handler:Landroid/os/Handler;

    .line 71
    iput-object p2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->bus:Lcom/squareup/otto/Bus;

    .line 72
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->context:Landroid/content/Context;

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method


# virtual methods
.method public go()V
    .locals 7

    .prologue
    .line 251
    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    sget-object v5, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    .line 254
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 256
    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/destination/Destination;

    .line 257
    .local v1, "destination":Lcom/navdy/service/library/events/destination/Destination;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transformToInternalDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v0

    .line 260
    .local v0, "d":Lcom/navdy/hud/app/framework/destinations/Destination;
    const/4 v2, 0x0

    .line 261
    .local v2, "iconRes":I
    iget-object v4, v1, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v3

    .line 262
    .local v3, "resourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    if-eqz v3, :cond_0

    .line 263
    iget v2, v3, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->iconRes:I

    .line 266
    :cond_0
    iget v4, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIcon:I

    if-nez v4, :cond_1

    if-eqz v2, :cond_1

    .line 267
    new-instance v4, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    invoke-direct {v4, v0}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;-><init>(Lcom/navdy/hud/app/framework/destinations/Destination;)V

    .line 268
    invoke-virtual {v4, v2}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIcon(I)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->context:Landroid/content/Context;

    const v6, 0x7f0d003e

    .line 269
    invoke-static {v5, v6}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->destinationIconBkColor(I)Lcom/navdy/hud/app/framework/destinations/Destination$Builder;

    move-result-object v4

    .line 270
    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/destinations/Destination$Builder;->build()Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v0

    .line 273
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->requestNavigationWithNavLookup(Lcom/navdy/hud/app/framework/destinations/Destination;)V

    .line 275
    .end local v0    # "d":Lcom/navdy/hud/app/framework/destinations/Destination;
    .end local v1    # "destination":Lcom/navdy/service/library/events/destination/Destination;
    .end local v2    # "iconRes":I
    .end local v3    # "resourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    :cond_2
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 1
    .param p1, "driverProfileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showVoiceSearchTipsToUser:Z

    .line 84
    return-void
.end method

.method public onVoiceSearchResponse(Lcom/navdy/service/library/events/audio/VoiceSearchResponse;)V
    .locals 1
    .param p1, "voiceSearchResponse"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->voiceSearchUserInterface:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;

    if-eqz v0, :cond_0

    .line 97
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->voiceSearchUserInterface:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;->onVoiceSearchResponse(Lcom/navdy/service/library/events/audio/VoiceSearchResponse;)V

    .line 100
    :cond_0
    return-void
.end method

.method public setVoiceSearchUserInterface(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;)V
    .locals 0
    .param p1, "voiceSearchUserInterface"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->voiceSearchUserInterface:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;

    .line 78
    return-void
.end method

.method public shouldShowVoiceSearchTipsToUser()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showVoiceSearchTipsToUser:Z

    return v0
.end method

.method public showResults()V
    .locals 51

    .prologue
    .line 138
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 139
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v0, v5, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    move-object/from16 v50, v0

    .line 140
    .local v50, "returnedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    new-instance v46, Landroid/os/Bundle;

    invoke-direct/range {v46 .. v46}, Landroid/os/Bundle;-><init>()V

    .line 141
    .local v46, "args":Landroid/os/Bundle;
    const/16 v49, -0x1

    .line 142
    .local v49, "icon":I
    const-string v5, "PICKER_SHOW_DESTINATION_MAP"

    const/4 v8, 0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v5, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 143
    const-string v5, "PICKER_DESTINATION_ICON"

    move-object/from16 v0, v46

    move/from16 v1, v49

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 144
    const-string v5, "PICKER_INITIAL_SELECTION"

    const/4 v8, 0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v5, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->context:Landroid/content/Context;

    const v8, 0x7f0d009d

    invoke-static {v5, v8}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v22

    .line 147
    .local v22, "defaultColor":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->context:Landroid/content/Context;

    const v8, 0x7f0d003e

    invoke-static {v5, v8}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v23

    .line 150
    .local v23, "deselectedColor":I
    const/4 v7, 0x0

    .line 151
    .local v7, "subTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 152
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\""

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v8, v8, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\""

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 153
    .local v6, "title":Ljava/lang/String;
    sget-object v7, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->searchAgainSubTitle:Ljava/lang/String;

    .line 158
    :goto_0
    new-instance v4, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    const v5, 0x7f0e0070

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    const v20, 0x7f02018d

    const/16 v21, 0x0

    sget-object v24, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->NONE:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    const/16 v25, 0x0

    invoke-direct/range {v4 .. v25}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;DDDDIIIILcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;Lcom/navdy/service/library/events/places/PlaceType;)V

    .line 177
    .local v4, "searchAgain":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->context:Landroid/content/Context;

    const/4 v8, 0x1

    move-object/from16 v0, v50

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v5, v0, v1, v2, v8}, Lcom/navdy/hud/app/maps/util/DestinationUtil;->convert(Landroid/content/Context;Ljava/util/List;IIZ)Ljava/util/List;

    move-result-object v48

    .line 179
    .local v48, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;>;"
    const/4 v5, 0x0

    move-object/from16 v0, v48

    invoke-interface {v0, v5, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 182
    new-instance v24, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    const v25, 0x7f0e006f

    sget-object v26, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->searchAgainSubTitle:Ljava/lang/String;

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const-wide/16 v32, 0x0

    const-wide/16 v34, 0x0

    const-wide/16 v36, 0x0

    const-wide/16 v38, 0x0

    const v40, 0x7f02018d

    const/16 v41, 0x0

    sget-object v44, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->NONE:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    sget-object v45, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    move/from16 v42, v22

    move/from16 v43, v23

    invoke-direct/range {v24 .. v45}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;DDDDIIIILcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;Lcom/navdy/service/library/events/places/PlaceType;)V

    .line 201
    .local v24, "retry":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    invoke-interface/range {v48 .. v48}, Ljava/util/List;->size()I

    move-result v5

    new-array v0, v5, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    move-object/from16 v47, v0

    .line 204
    .local v47, "destinationParcelables":[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    move-object/from16 v0, v48

    move-object/from16 v1, v47

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 206
    const-string v5, "PICKER_DESTINATIONS"

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 207
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v8

    const-string v9, "navdy#voicesearch#notif"

    const/4 v10, 0x1

    sget-object v11, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    new-instance v13, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$2;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;)V

    move-object/from16 v12, v46

    invoke-virtual/range {v8 .. v13}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;ZLcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 248
    .end local v4    # "searchAgain":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "subTitle":Ljava/lang/String;
    .end local v22    # "defaultColor":I
    .end local v23    # "deselectedColor":I
    .end local v24    # "retry":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v46    # "args":Landroid/os/Bundle;
    .end local v47    # "destinationParcelables":[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v48    # "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;>;"
    .end local v49    # "icon":I
    .end local v50    # "returnedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    :cond_0
    return-void

    .line 155
    .restart local v7    # "subTitle":Ljava/lang/String;
    .restart local v22    # "defaultColor":I
    .restart local v23    # "deselectedColor":I
    .restart local v46    # "args":Landroid/os/Bundle;
    .restart local v49    # "icon":I
    .restart local v50    # "returnedDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    :cond_1
    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->searchAgainSubTitle:Ljava/lang/String;

    .restart local v6    # "title":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public showedVoiceSearchTipsToUser()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showVoiceSearchTipsToUser:Z

    .line 88
    return-void
.end method

.method public startVoiceSearch()V
    .locals 6

    .prologue
    .line 104
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v1

    .line 105
    .local v1, "remotePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    if-nez v1, :cond_0

    .line 124
    :goto_0
    return-void

    .line 109
    :cond_0
    sget-object v2, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v1, v2, :cond_2

    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->supportsVoiceSearchNewIOSPauseBehaviors()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 110
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v0

    .line 111
    .local v0, "musicManager":Lcom/navdy/hud/app/manager/MusicManager;
    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->softPause()V

    .line 115
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$1;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;)V

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 122
    .end local v0    # "musicManager":Lcom/navdy/hud/app/manager/MusicManager;
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v4, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public stopVoiceSearch()V
    .locals 5

    .prologue
    .line 127
    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->response:Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    iget-object v1, v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    sget-object v2, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    if-eq v1, v2, :cond_0

    .line 129
    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 130
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v0

    .line 131
    .local v0, "musicManager":Lcom/navdy/hud/app/manager/MusicManager;
    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/MusicManager;->acceptResumes()V

    .line 135
    .end local v0    # "musicManager":Lcom/navdy/hud/app/manager/MusicManager;
    :cond_0
    return-void
.end method
