.class final enum Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;
.super Ljava/lang/Enum;
.source "VoiceSearchNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "VoiceSearchStateInternal"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

.field public static final enum FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

.field public static final enum IDLE:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

.field public static final enum LISTENING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

.field public static final enum RESULTS:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

.field public static final enum SEARCHING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

.field public static final enum STARTING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;


# instance fields
.field private statusTextResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 142
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const-string v1, "IDLE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->IDLE:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 143
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const-string v1, "STARTING"

    const v2, 0x7f0902fb

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->STARTING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 144
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const-string v1, "LISTENING"

    const v2, 0x7f0902f3

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->LISTENING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 145
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const-string v1, "SEARCHING"

    const v2, 0x7f0902f9

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->SEARCHING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 146
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const-string v1, "FAILED"

    const v2, 0x7f0902f1

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 147
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const-string v1, "RESULTS"

    const/4 v2, 0x5

    const v3, 0x7f0902f4

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->RESULTS:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 141
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->IDLE:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->STARTING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->LISTENING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->SEARCHING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->RESULTS:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->$VALUES:[Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "statusTextResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 155
    iput p3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->statusTextResId:I

    .line 156
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 141
    const-class v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->$VALUES:[Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    return-object v0
.end method


# virtual methods
.method public getStatusTextResId()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->statusTextResId:I

    return v0
.end method
