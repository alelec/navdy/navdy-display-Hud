.class public Lcom/navdy/hud/app/framework/voice/TTSUtils;
.super Ljava/lang/Object;
.source "TTSUtils.java"


# static fields
.field public static final DEBUG_GPS_CALIBRATION_IMU_DONE:Ljava/lang/String; = "Ublox IMU Calibration Done"

.field public static final DEBUG_GPS_CALIBRATION_SENSOR_DONE:Ljava/lang/String; = "Ublox Sensor Calibration Done"

.field public static final DEBUG_GPS_CALIBRATION_SENSOR_NOT_NEEDED:Ljava/lang/String; = "Ublox Sensor Calibration Not Needed"

.field public static final DEBUG_GPS_CALIBRATION_STARTED:Ljava/lang/String; = "Ublox Calibration started"

.field private static final DEBUG_GPS_SWITCH_TOAST_ID:Ljava/lang/String; = "debug-tts-gps-switch"

.field public static final DEBUG_TTS_FASTER_ROUTE_AVAILABLE:Ljava/lang/String; = "Faster route is available"

.field public static final DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC:Ljava/lang/String; = "Traffic Not used for Route Calculation"

.field public static final DEBUG_TTS_UBLOX_DR_ENDED:Ljava/lang/String; = "Dead Reckoning has Stopped"

.field public static final DEBUG_TTS_UBLOX_DR_STARTED:Ljava/lang/String; = "Dead Reckoning has Started"

.field public static final DEBUG_TTS_UBLOX_HAS_LOCATION_FIX:Ljava/lang/String; = "Ublox Has Location Fix"

.field public static final DEBUG_TTS_USING_GPS_SPEED:Ljava/lang/String; = "Using GPS RawSpeed"

.field public static final DEBUG_TTS_USING_OBD_SPEED:Ljava/lang/String; = "Using OBD RawSpeed"

.field public static final TTS_AUTOMATIC_ZOOM:Ljava/lang/String;

.field public static final TTS_DIAL_BATTERY_EXTREMELY_LOW:Ljava/lang/String;

.field public static final TTS_DIAL_BATTERY_LOW:Ljava/lang/String;

.field public static final TTS_DIAL_BATTERY_VERY_LOW:Ljava/lang/String;

.field public static final TTS_DIAL_CONNECTED:Ljava/lang/String;

.field public static final TTS_DIAL_DISCONNECTED:Ljava/lang/String;

.field public static final TTS_DIAL_FORGOTTEN:Ljava/lang/String;

.field public static final TTS_FUEL_GAUGE_ADDED:Ljava/lang/String;

.field public static final TTS_FUEL_RANGE_GAUGE_ADDED:Ljava/lang/String;

.field public static final TTS_GLANCES_DISABLED:Ljava/lang/String;

.field public static final TTS_HEADING_GAUGE_ADDED:Ljava/lang/String;

.field public static final TTS_MANUAL_ZOOM:Ljava/lang/String;

.field public static final TTS_NAV_STOPPED:Ljava/lang/String;

.field public static final TTS_PHONE_BATTERY_LOW:Ljava/lang/String;

.field public static final TTS_PHONE_BATTERY_VERY_LOW:Ljava/lang/String;

.field public static final TTS_PHONE_OFFLINE:Ljava/lang/String;

.field public static final TTS_RPM_GAUGE_ADDED:Ljava/lang/String;

.field public static final TTS_TBT_DISABLED:Ljava/lang/String;

.field public static final TTS_TBT_ENABLED:Ljava/lang/String;

.field public static final TTS_TRAFFIC_DISABLED:Ljava/lang/String;

.field public static final TTS_TRAFFIC_ENABLED:Ljava/lang/String;

.field private static final bus:Lcom/squareup/otto/Bus;

.field private static final isDebugTTSEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 78
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->bus:Lcom/squareup/otto/Bus;

    .line 80
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 82
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f090116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_GLANCES_DISABLED:Ljava/lang/String;

    .line 83
    const v1, 0x7f0902b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_DISCONNECTED:Ljava/lang/String;

    .line 84
    const v1, 0x7f0902b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_CONNECTED:Ljava/lang/String;

    .line 85
    const v1, 0x7f0902b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_FORGOTTEN:Ljava/lang/String;

    .line 86
    const v1, 0x7f0902c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_TRAFFIC_DISABLED:Ljava/lang/String;

    .line 87
    const v1, 0x7f0902c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_TRAFFIC_ENABLED:Ljava/lang/String;

    .line 88
    const v1, 0x7f0902be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_NAV_STOPPED:Ljava/lang/String;

    .line 89
    const v1, 0x7f0902c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_TBT_DISABLED:Ljava/lang/String;

    .line 90
    const v1, 0x7f0902c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_TBT_ENABLED:Ljava/lang/String;

    .line 91
    const v1, 0x7f0902b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_FUEL_RANGE_GAUGE_ADDED:Ljava/lang/String;

    .line 92
    const v1, 0x7f0902b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_FUEL_GAUGE_ADDED:Ljava/lang/String;

    .line 93
    const v1, 0x7f0902c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_RPM_GAUGE_ADDED:Ljava/lang/String;

    .line 94
    const v1, 0x7f0902b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_HEADING_GAUGE_ADDED:Ljava/lang/String;

    .line 95
    const v1, 0x7f0902b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_BATTERY_VERY_LOW:Ljava/lang/String;

    .line 96
    const v1, 0x7f0902b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_BATTERY_LOW:Ljava/lang/String;

    .line 97
    const v1, 0x7f0902b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_BATTERY_EXTREMELY_LOW:Ljava/lang/String;

    .line 98
    const v1, 0x7f0902c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_PHONE_BATTERY_VERY_LOW:Ljava/lang/String;

    .line 99
    const v1, 0x7f0902bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_PHONE_BATTERY_LOW:Ljava/lang/String;

    .line 100
    const v1, 0x7f0901fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_PHONE_OFFLINE:Ljava/lang/String;

    .line 101
    const v1, 0x7f0902bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_MANUAL_ZOOM:Ljava/lang/String;

    .line 102
    const v1, 0x7f0902ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_AUTOMATIC_ZOOM:Ljava/lang/String;

    .line 104
    new-instance v1, Ljava/io/File;

    const-string v2, "/sdcard/debug_tts"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    .line 105
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static debugShowDREnded()V
    .locals 7

    .prologue
    .line 191
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 193
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    const-string v0, "4"

    const-string v1, "Dead Reckoning has Stopped"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 197
    const-string v0, "17"

    const-string v1, "Dead Reckoning has Stopped"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-ublox-dr-end"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 201
    :cond_0
    return-void
.end method

.method public static debugShowDRStarted(Ljava/lang/String;)V
    .locals 7
    .param p0, "drType"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0c0009

    .line 176
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 178
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 180
    const-string v0, "2"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "3"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v0, "4"

    const-string v1, "Dead Reckoning has Started"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "5"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string v0, "17"

    const-string v1, "Dead Reckoning has Started"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-ublox-dr-start"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 188
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public static debugShowFasterRouteToast(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "info"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 217
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    .line 218
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 219
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0x1b58

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 220
    const-string v0, "8"

    const v1, 0x7f020199

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 221
    const-string v0, "4"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    const-string v0, "6"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v0, "7"

    const v1, 0x7f0c000b

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 225
    const-string v0, "17"

    const-string v1, "Faster route is available"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v0, "12"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 228
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-faster-route"

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 230
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public static debugShowGotUbloxFix()V
    .locals 7

    .prologue
    .line 145
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 147
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    const-string v0, "4"

    const-string v1, "Ublox Has Location Fix"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    const-string v0, "17"

    const-string v1, "Ublox Has Location Fix"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-ublox-fix"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 155
    :cond_0
    return-void
.end method

.method public static debugShowGpsCalibrationStarted()V
    .locals 7

    .prologue
    .line 247
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 249
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 250
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 251
    const-string v0, "2"

    const-string v1, "Ublox Calibration started"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v0, "3"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 253
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-gps-calibration_start"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 255
    :cond_0
    return-void
.end method

.method public static debugShowGpsImuCalibrationDone()V
    .locals 7

    .prologue
    .line 258
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 260
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 261
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 262
    const-string v0, "2"

    const-string v1, "Ublox IMU Calibration Done"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v0, "3"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 264
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-gps-calibration_imu"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 266
    :cond_0
    return-void
.end method

.method public static debugShowGpsReset(Ljava/lang/String;)V
    .locals 7
    .param p0, "title"    # Ljava/lang/String;

    .prologue
    .line 233
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 235
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 236
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 237
    const-string v0, "4"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 239
    const-string v0, "16"

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 240
    const-string v0, "17"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-gps-reset"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 244
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public static debugShowGpsSensorCalibrationDone()V
    .locals 7

    .prologue
    .line 269
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 271
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 272
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 273
    const-string v0, "2"

    const-string v1, "Ublox Sensor Calibration Done"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v0, "3"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-gps-calibration_sensor"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 277
    :cond_0
    return-void
.end method

.method public static debugShowGpsSensorCalibrationNotNeeded()V
    .locals 7

    .prologue
    .line 280
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 282
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 283
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 284
    const-string v0, "2"

    const-string v1, "Ublox Sensor Calibration Not Needed"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v0, "3"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 286
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-gps-calibration_not_needed"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 288
    :cond_0
    return-void
.end method

.method public static debugShowGpsSwitch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "info"    # Ljava/lang/String;

    .prologue
    .line 158
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 160
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 161
    const-string v0, "8"

    const v1, 0x7f020132

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 162
    const-string v0, "4"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 164
    const-string v0, "6"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v0, "7"

    const v1, 0x7f0c000b

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    const-string v0, "16"

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 167
    const-string v0, "17"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    .line 170
    .local v6, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v0, "debug-tts-gps-switch"

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 171
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-gps-switch"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 173
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v6    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_0
    return-void
.end method

.method public static debugShowNoTrafficToast()V
    .locals 7

    .prologue
    .line 132
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    .line 133
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 134
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    const-string v0, "8"

    const v1, 0x7f020199

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 136
    const-string v0, "4"

    const-string v1, "Traffic Not used for Route Calculation"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 138
    const-string v0, "17"

    const-string v1, "Traffic Not used for Route Calculation"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-no-traffic"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 142
    :cond_0
    return-void
.end method

.method public static debugShowSpeedInput(Ljava/lang/String;)V
    .locals 7
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 204
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 206
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0xbb8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 207
    const-string v0, "8"

    const v1, 0x7f020173

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 208
    const-string v0, "4"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 210
    const-string v0, "17"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "debug-tts-speed-input"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 214
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public static isDebugTTSEnabled()Z
    .locals 1

    .prologue
    .line 128
    sget-boolean v0, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled:Z

    return v0
.end method

.method public static sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V
    .locals 5
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "category"    # Lcom/navdy/service/library/events/audio/SpeechRequest$Category;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 125
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 116
    .local v1, "locale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->toLanguageTag()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "language":Ljava/lang/String;
    new-instance v3, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;-><init>()V

    .line 118
    invoke-virtual {v3, p0}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v3

    .line 119
    invoke-virtual {v3, p1}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category(Lcom/navdy/service/library/events/audio/SpeechRequest$Category;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v3

    .line 120
    invoke-virtual {v3, p2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v3

    const/4 v4, 0x0

    .line 121
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->sendStatus(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v3

    .line 122
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->language(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v3

    .line 123
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->build()Lcom/navdy/service/library/events/audio/SpeechRequest;

    move-result-object v2

    .line 124
    .local v2, "request":Lcom/navdy/service/library/events/audio/SpeechRequest;
    sget-object v3, Lcom/navdy/hud/app/framework/voice/TTSUtils;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/hud/app/event/LocalSpeechRequest;

    invoke-direct {v4, v2}, Lcom/navdy/hud/app/event/LocalSpeechRequest;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequest;)V

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static sendSpeechRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 108
    sget-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_NOTIFICATION:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    invoke-static {p0, v0, p1}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 109
    return-void
.end method
