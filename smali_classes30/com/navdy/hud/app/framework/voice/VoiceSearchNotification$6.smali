.class final Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VoiceSearchNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->getListeningFeedbackDefaultAnimation(Landroid/view/View;)Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic val$animationSet:Landroid/animation/AnimatorSet;


# direct methods
.method constructor <init>(Landroid/animation/AnimatorSet;)V
    .locals 1

    .prologue
    .line 839
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;->val$animationSet:Landroid/animation/AnimatorSet;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    .line 840
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;->canceled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 855
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;->canceled:Z

    .line 856
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 848
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;->canceled:Z

    if-nez v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;->val$animationSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 851
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 843
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;->canceled:Z

    .line 844
    return-void
.end method
