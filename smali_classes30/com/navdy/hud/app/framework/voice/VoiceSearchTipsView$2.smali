.class Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VoiceSearchTipsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 120
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->TIPS_COUNT:I
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$300()I

    move-result v4

    rem-int/2addr v3, v4

    # setter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$202(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;I)I

    .line 121
    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_TEXTS:[Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$400()[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)I

    move-result v3

    aget-object v1, v2, v3

    .line 122
    .local v1, "tipText":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)I

    move-result v3

    # invokes: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->getIconIdForIndex(I)I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$500(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;I)I

    move-result v0

    .line 123
    .local v0, "icon":I
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsText1:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsIcon1:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 125
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder1:Landroid/view/ViewGroup;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 126
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v2, v2, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder2:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 127
    return-void
.end method
