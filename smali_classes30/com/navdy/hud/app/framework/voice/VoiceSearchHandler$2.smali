.class Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$2;
.super Ljava/lang/Object;
.source "VoiceSearchHandler.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showResults()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field retrySelected:Z

.field final synthetic this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$2;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$2;->retrySelected:Z

    return-void
.end method


# virtual methods
.method public onDestinationPickerClosed()V
    .locals 3

    .prologue
    .line 237
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$2;->retrySelected:Z

    if-eqz v2, :cond_1

    .line 238
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    .line 239
    .local v0, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v2, "navdy#voicesearch#notif"

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .line 240
    .local v1, "voiceSearchNotification":Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    if-nez v1, :cond_0

    .line 241
    new-instance v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .end local v1    # "voiceSearchNotification":Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    invoke-direct {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;-><init>()V

    .line 243
    .restart local v1    # "voiceSearchNotification":Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    :cond_0
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 245
    .end local v0    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    .end local v1    # "voiceSearchNotification":Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    :cond_1
    return-void
.end method

.method public onItemClicked(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 2
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    const/4 v0, 0x1

    .line 212
    packed-switch p1, :pswitch_data_0

    .line 224
    invoke-static {p2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchListItemSelection(I)V

    .line 227
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 215
    :pswitch_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$2;->retrySelected:Z

    .line 216
    const v1, 0x7f0e006f

    if-ne p1, v1, :cond_0

    .line 217
    const/4 v1, -0x1

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchListItemSelection(I)V

    goto :goto_0

    .line 219
    :cond_0
    const/4 v1, -0x2

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchListItemSelection(I)V

    goto :goto_0

    .line 212
    :pswitch_data_0
    .packed-switch 0x7f0e006f
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onItemSelected(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method
