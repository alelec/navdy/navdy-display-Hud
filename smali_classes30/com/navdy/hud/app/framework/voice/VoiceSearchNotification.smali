.class public Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
.super Ljava/lang/Object;
.source "VoiceSearchNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;
.implements Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$SinusoidalInterpolator;,
        Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;
    }
.end annotation


# static fields
.field public static final ANIMATION_DURATION:I = 0x12c

.field private static final BADGE_ICON_MAPPING:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final ERROR_TEXT_MAPPING:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final INITIALIZATION_DELAY_MILLIS:I

.field public static final INPUT_DELAY_MILLIS:I

.field public static final RESULT_SELECTION_TIMEOUT:I = 0x2710

.field public static final SEARCH_DELAY_MILLIS:I

.field private static final STATE_NAME_MAPPING:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public animatorSet:Landroid/animation/AnimatorSet;

.field public choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field private handler:Landroid/os/Handler;

.field public imageActive:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e016f
    .end annotation
.end field

.field public imageInactive:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e016e
    .end annotation
.end field

.field public imageProcessing:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0170
    .end annotation
.end field

.field public imageStatus:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0171
    .end annotation
.end field

.field private isListeningOverBluetooth:Z

.field private layout:Landroid/view/ViewGroup;

.field private listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

.field public listeningFeedbackView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e016d
    .end annotation
.end field

.field private locale:Ljava/util/Locale;

.field private mainImageAnimation:Landroid/animation/AnimatorSet;

.field private multipleResultsMode:Z

.field private processingImageAnimation:Landroid/animation/AnimatorSet;

.field private volatile responseFromClientTimedOut:Z

.field private results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public resultsCount:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e016c
    .end annotation
.end field

.field public resultsCountContainer:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e016b
    .end annotation
.end field

.field private statusImageAnimation:Landroid/animation/AnimatorSet;

.field public subStatus:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0169
    .end annotation
.end field

.field private voiceSearchActiveChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private voiceSearchFailedChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

.field private voiceSearchState:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

.field private voiceSearchSuccessResultsChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private waitingTimeoutRunnable:Ljava/lang/Runnable;

.field private words:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x2

    const v5, 0x7f0902f1

    const v4, 0x7f0200d9

    .line 61
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 63
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->INITIALIZATION_DELAY_MILLIS:I

    .line 64
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->SEARCH_DELAY_MILLIS:I

    .line 65
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->INPUT_DELAY_MILLIS:I

    .line 70
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    .line 118
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->OFFLINE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const v2, 0x7f0902f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NEED_PERMISSION:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const v2, 0x7f0902f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const v2, 0x7f0902f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->AMBIENT_NOISE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const v2, 0x7f0902f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    .line 127
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->FAILED_TO_START:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->OFFLINE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NEED_PERMISSION:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    const v2, 0x7f0200e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->AMBIENT_NOISE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->STATE_NAME_MAPPING:Ljava/util/HashMap;

    .line 135
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->STATE_NAME_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->STARTING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const v2, 0x7f0902fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->STATE_NAME_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->LISTENING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const v2, 0x7f0902fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->STATE_NAME_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->STATE_NAME_MAPPING:Ljava/util/HashMap;

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->SEARCHING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    const v2, 0x7f0902f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    .line 159
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->IDLE:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchState:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 167
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    .line 179
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    return-object p1
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->responseFromClientTimedOut:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    .param p1, "x1"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 732
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#voicesearch#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 733
    return-void
.end method

.method public static getBadgeAnimation(Landroid/view/View;Z)Landroid/animation/AnimatorSet;
    .locals 8
    .param p0, "view"    # Landroid/view/View;
    .param p1, "show"    # Z

    .prologue
    const-wide/16 v6, 0xc8

    const/4 v4, 0x2

    .line 795
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 796
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 797
    .local v1, "animationSet":Landroid/animation/AnimatorSet;
    new-instance v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$5;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$5;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 804
    if-eqz p1, :cond_0

    .line 805
    const/high16 v3, -0x3ee00000    # -10.0f

    invoke-virtual {p0, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 806
    const-string v3, "translationY"

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 807
    .local v2, "translateY":Landroid/animation/ObjectAnimator;
    invoke-virtual {v1, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 808
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 809
    new-instance v3, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v3}, Landroid/view/animation/BounceInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 816
    .end local v2    # "translateY":Landroid/animation/ObjectAnimator;
    :goto_0
    return-object v1

    .line 811
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 812
    const-string v3, "alpha"

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 813
    .local v0, "alpha":Landroid/animation/ObjectAnimator;
    invoke-virtual {v1, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 814
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 806
    :array_0
    .array-data 4
        -0x3ee00000    # -10.0f
        0x0
    .end array-data

    .line 812
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public static getListeningFeedbackDefaultAnimation(Landroid/view/View;)Landroid/animation/AnimatorSet;
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 836
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x7f050000

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    .line 838
    .local v0, "animationSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, p0}, Landroid/animation/AnimatorSet;->setTarget(Ljava/lang/Object;)V

    .line 839
    new-instance v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;

    invoke-direct {v1, v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$6;-><init>(Landroid/animation/AnimatorSet;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 858
    return-object v0
.end method

.method public static getProcessingBadgeAnimation(Landroid/view/View;)Landroid/animation/Animator;
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 820
    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    aput v4, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 821
    .local v0, "processingAnimator":Landroid/animation/Animator;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 822
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 823
    return-object v0
.end method

.method private setChoices(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V
    .locals 3
    .param p1, "newState"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .prologue
    const/4 v2, 0x0

    .line 635
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 652
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 638
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 639
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchActiveChoices:Ljava/util/List;

    invoke-virtual {v0, v1, v2, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    goto :goto_0

    .line 642
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 643
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchFailedChoices:Ljava/util/List;

    invoke-virtual {v0, v1, v2, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    goto :goto_0

    .line 646
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->results:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->results:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 647
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchSuccessResultsChoices:Ljava/util/List;

    invoke-virtual {v0, v1, v2, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    goto :goto_0

    .line 635
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showHideVoiceSearchTipsIfNeeded(Z)V
    .locals 9
    .param p1, "show"    # Z

    .prologue
    .line 330
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "showHideVoiceSearchTipsIfNeeded, show : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 331
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v3

    .line 332
    .local v3, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v1

    .line 333
    .local v1, "main":Lcom/navdy/hud/app/ui/activity/Main;
    if-eqz p1, :cond_2

    .line 334
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Should show voice search tips :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v7}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->shouldShowVoiceSearchTipsToUser()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 335
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->shouldShowVoiceSearchTipsToUser()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 336
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03008e

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->getNotificationExtensionViewHolder()Landroid/view/ViewGroup;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 337
    .local v2, "notificationExtensionHolder":Landroid/view/ViewGroup;
    const v5, 0x7f0e023e

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .line 338
    .local v4, "voiceSearchTipsView":Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    iget-boolean v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->isListeningOverBluetooth:Z

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->setListeningOverBluetooth(Z)V

    .line 339
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->locale:Ljava/util/Locale;

    if-eqz v5, :cond_1

    .line 340
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->locale:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->setVoiceSearchLocale(Ljava/util/Locale;)V

    .line 345
    :goto_0
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/activity/Main;->addNotificationExtensionView(Landroid/view/View;)V

    .line 346
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showedVoiceSearchTipsToUser()V

    .line 352
    .end local v2    # "notificationExtensionHolder":Landroid/view/ViewGroup;
    .end local v4    # "voiceSearchTipsView":Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    :cond_0
    :goto_1
    return-void

    .line 342
    .restart local v2    # "notificationExtensionHolder":Landroid/view/ViewGroup;
    .restart local v4    # "voiceSearchTipsView":Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 343
    .local v0, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocale()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->setVoiceSearchLocale(Ljava/util/Locale;)V

    goto :goto_0

    .line 349
    .end local v0    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v2    # "notificationExtensionHolder":Landroid/view/ViewGroup;
    .end local v4    # "voiceSearchTipsView":Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    :cond_2
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Remove notification extension"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 350
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/activity/Main;->removeNotificationExtensionView()V

    goto :goto_1
.end method

.method private startVoiceSearch()V
    .locals 4

    .prologue
    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->responseFromClientTimedOut:Z

    .line 223
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->STARTING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    .line 224
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->startVoiceSearch()V

    .line 225
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->INITIALIZATION_DELAY_MILLIS:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 226
    return-void
.end method

.method private updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V
    .locals 2
    .param p1, "state"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->results:Ljava/util/List;

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;Ljava/util/List;)V

    .line 327
    return-void
.end method

.method private updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;Ljava/util/List;)V
    .locals 13
    .param p1, "newState"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;
    .param p2, "error"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;",
            "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 355
    .local p3, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "updateUI newState :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " , Error :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 356
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchState:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    if-eq v9, p1, :cond_10

    const/4 v8, 0x1

    .line 357
    .local v8, "stateChanged":Z
    :goto_0
    if-eqz v8, :cond_0

    .line 358
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "State Changed From : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchState:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " , New state : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 361
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchState:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    .line 362
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 363
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v1, 0x0

    .line 365
    .local v1, "animatorSetBuilder":Landroid/animation/AnimatorSet$Builder;
    if-eqz v8, :cond_1

    .line 366
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->setChoices(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    .line 370
    :cond_1
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 397
    if-eqz v8, :cond_2

    .line 398
    iget-object v10, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->STATE_NAME_MAPPING:Ljava/util/HashMap;

    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(I)V

    .line 403
    :cond_2
    :goto_1
    const/4 v5, 0x0

    .line 405
    .local v5, "mainImageAnimationReference":Landroid/animation/AnimatorSet;
    if-eqz v8, :cond_5

    .line 406
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCountContainer:Landroid/view/ViewGroup;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 407
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->mainImageAnimation:Landroid/animation/AnimatorSet;

    if-eqz v9, :cond_3

    .line 408
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->mainImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 409
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->mainImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->cancel()V

    .line 411
    :cond_3
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->mainImageAnimation:Landroid/animation/AnimatorSet;

    .line 412
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->mainImageAnimation:Landroid/animation/AnimatorSet;

    .line 413
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_1

    .line 426
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageInactive:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 427
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageActive:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getAlpha()F

    move-result v9

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-lez v9, :cond_4

    .line 428
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageActive:Landroid/widget/ImageView;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_0

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 429
    .local v3, "hideActive":Landroid/animation/ObjectAnimator;
    new-instance v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$3;

    invoke-direct {v9, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$3;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)V

    invoke-virtual {v3, v9}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 435
    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 438
    .end local v3    # "hideActive":Landroid/animation/ObjectAnimator;
    :cond_4
    :goto_2
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_2

    .line 455
    :cond_5
    :goto_3
    :pswitch_0
    const/4 v6, 0x0

    .line 457
    .local v6, "processingAnimator":Landroid/animation/Animator;
    if-eqz v8, :cond_8

    .line 458
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    if-eqz v9, :cond_6

    .line 459
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 460
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->cancel()V

    .line 461
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    .line 463
    :cond_6
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 464
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    if-eqz v9, :cond_7

    .line 465
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->cancel()V

    .line 467
    :cond_7
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageProcessing:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 468
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    .line 469
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_3

    .line 537
    :cond_8
    :goto_4
    :pswitch_1
    if-eqz v5, :cond_9

    .line 538
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    .line 541
    :cond_9
    if-eqz v8, :cond_a

    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    if-eqz v9, :cond_a

    .line 542
    if-eqz v1, :cond_1d

    .line 543
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v9}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 548
    :cond_a
    :goto_5
    if-eqz v8, :cond_b

    if-eqz v6, :cond_b

    .line 549
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->start()V

    .line 551
    :cond_b
    const-wide/16 v10, 0x12c

    invoke-virtual {v0, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 552
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 555
    if-eqz v8, :cond_d

    .line 556
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchState:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    sget-object v10, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->LISTENING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    if-ne v9, v10, :cond_1e

    .line 557
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackView:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 558
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    if-nez v9, :cond_c

    .line 559
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackView:Landroid/view/View;

    invoke-static {v9}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->getListeningFeedbackDefaultAnimation(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v9

    iput-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    .line 561
    :cond_c
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->start()V

    .line 570
    :cond_d
    :goto_6
    if-eqz v8, :cond_f

    .line 571
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    if-ne p1, v9, :cond_f

    sget-object v9, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    if-eq p2, v9, :cond_e

    sget-object v9, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    if-ne p2, v9, :cond_f

    .line 572
    :cond_e
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "VoiceSearch error , error :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " , showing voice search"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 573
    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->showHideVoiceSearchTipsIfNeeded(Z)V

    .line 576
    :cond_f
    return-void

    .line 356
    .end local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v1    # "animatorSetBuilder":Landroid/animation/AnimatorSet$Builder;
    .end local v5    # "mainImageAnimationReference":Landroid/animation/AnimatorSet;
    .end local v6    # "processingAnimator":Landroid/animation/Animator;
    .end local v8    # "stateChanged":Z
    :cond_10
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 372
    .restart local v0    # "animatorSet":Landroid/animation/AnimatorSet;
    .restart local v1    # "animatorSetBuilder":Landroid/animation/AnimatorSet$Builder;
    .restart local v8    # "stateChanged":Z
    :pswitch_2
    if-eqz p2, :cond_11

    iget-boolean v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->responseFromClientTimedOut:Z

    if-eqz v9, :cond_12

    .line 373
    :cond_11
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchState:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v10}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->getStatusTextResId()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 375
    :cond_12
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError:[I

    invoke-virtual {p2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_4

    .line 380
    if-nez p2, :cond_14

    const/4 v2, 0x0

    .line 381
    .local v2, "err":Ljava/lang/Integer;
    :goto_7
    if-nez v2, :cond_13

    .line 382
    const v9, 0x7f0902f1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 383
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "unhandled error response: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p2, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 385
    :cond_13
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 377
    .end local v2    # "err":Ljava/lang/Integer;
    :pswitch_3
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 380
    :cond_14
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->ERROR_TEXT_MAPPING:Ljava/util/HashMap;

    invoke-virtual {v9, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    move-object v2, v9

    goto :goto_7

    .line 390
    :pswitch_4
    if-eqz v8, :cond_2

    .line 391
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 394
    :pswitch_5
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    const-string v10, ""

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 415
    .restart local v5    # "mainImageAnimationReference":Landroid/animation/AnimatorSet;
    :pswitch_6
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageActive:Landroid/widget/ImageView;

    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_1

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 416
    .local v7, "showActive":Landroid/animation/ObjectAnimator;
    new-instance v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$2;

    invoke-direct {v9, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$2;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)V

    invoke-virtual {v7, v9}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 422
    invoke-virtual {v5, v7}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_2

    .line 440
    .end local v7    # "showActive":Landroid/animation/ObjectAnimator;
    :pswitch_7
    sget-object v9, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    if-ne p2, v9, :cond_5

    .line 441
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCountContainer:Landroid/view/ViewGroup;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 442
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageInactive:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 446
    :pswitch_8
    if-eqz p3, :cond_15

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_15

    .line 447
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCount:Landroid/widget/TextView;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    :cond_15
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCountContainer:Landroid/view/ViewGroup;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 450
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageInactive:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 472
    .restart local v6    # "processingAnimator":Landroid/animation/Animator;
    :pswitch_9
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageProcessing:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 473
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    if-nez v9, :cond_16

    .line 474
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    .line 475
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    new-instance v10, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;

    invoke-direct {v10, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)V

    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 497
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageProcessing:Landroid/widget/ImageView;

    invoke-static {v10}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->getProcessingBadgeAnimation(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 499
    :cond_16
    iget-object v6, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    .line 500
    goto/16 :goto_4

    .line 502
    :pswitch_a
    iget-boolean v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->isListeningOverBluetooth:Z

    if-eqz v9, :cond_17

    .line 503
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const v10, 0x7f0200da

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 507
    :goto_8
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->getBadgeAnimation(Landroid/view/View;Z)Landroid/animation/AnimatorSet;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_4

    .line 505
    :cond_17
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const v10, 0x7f0200e2

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_8

    .line 510
    :pswitch_b
    if-eqz p2, :cond_1c

    .line 511
    sget-object v9, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_WORDS_RECOGNIZED:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    if-ne v9, p2, :cond_19

    .line 512
    iget-boolean v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->isListeningOverBluetooth:Z

    if-eqz v9, :cond_18

    .line 513
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const v10, 0x7f0200da

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 525
    :goto_9
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->getBadgeAnimation(Landroid/view/View;Z)Landroid/animation/AnimatorSet;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_4

    .line 515
    :cond_18
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const v10, 0x7f0200e2

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_9

    .line 518
    :cond_19
    if-nez p2, :cond_1b

    const/4 v4, 0x0

    .line 519
    .local v4, "icon":Ljava/lang/Integer;
    :goto_a
    if-nez v4, :cond_1a

    .line 520
    const v9, 0x7f0200d9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 521
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "no icon for error: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p2, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 523
    :cond_1a
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_9

    .line 518
    .end local v4    # "icon":Ljava/lang/Integer;
    :cond_1b
    sget-object v9, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->BADGE_ICON_MAPPING:Ljava/util/HashMap;

    invoke-virtual {v9, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    move-object v4, v9

    goto :goto_a

    .line 527
    :cond_1c
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 531
    :pswitch_c
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const v10, 0x7f0200e7

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 532
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    iget-object v10, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->getBadgeAnimation(Landroid/view/View;Z)Landroid/animation/AnimatorSet;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_4

    .line 545
    :cond_1d
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->statusImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v9}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_5

    .line 563
    :cond_1e
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackView:Landroid/view/View;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 564
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 565
    iget-object v9, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->cancel()V

    goto/16 :goto_6

    .line 370
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 413
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_6
    .end packed-switch

    .line 428
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 438
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch

    .line 469
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_b
        :pswitch_1
        :pswitch_c
        :pswitch_a
        :pswitch_9
        :pswitch_9
    .end packed-switch

    .line 375
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch

    .line 415
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 678
    const/4 v0, 0x1

    return v0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->dismissNotification()V

    .line 323
    return-void
.end method

.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 1
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 767
    iget v0, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    sparse-switch v0, :sswitch_data_0

    .line 789
    :goto_0
    return-void

    .line 769
    :sswitch_0
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordRouteSelectionCancelled()V

    .line 770
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->dismissNotification()V

    goto :goto_0

    .line 773
    :sswitch_1
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchRetry()V

    .line 774
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->showHideVoiceSearchTipsIfNeeded(Z)V

    .line 775
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->startVoiceSearch()V

    goto :goto_0

    .line 778
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_LIST:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchAdditionalResultsAction(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;)V

    .line 780
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showResults()V

    goto :goto_0

    .line 783
    :sswitch_3
    sget-object v0, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_GO:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchAdditionalResultsAction(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;)V

    .line 785
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->go()V

    .line 786
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->dismissNotification()V

    goto :goto_0

    .line 767
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e000b -> :sswitch_0
        0x7f0e0010 -> :sswitch_3
        0x7f0e0013 -> :sswitch_2
        0x7f0e006f -> :sswitch_1
    .end sparse-switch
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 705
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 683
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 193
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    const-string v0, "navdy#voicesearch#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 663
    const/4 v0, 0x0

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 183
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->VOICE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 602
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->layout:Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    .line 603
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030044

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->layout:Landroid/view/ViewGroup;

    .line 605
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->layout:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 607
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 608
    .local v20, "resources":Landroid/content/res/Resources;
    const/high16 v6, -0x1000000

    .line 609
    .local v6, "unselectedColor":I
    const v2, 0x7f0d0021

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 610
    .local v4, "dismissColor":I
    const v2, 0x7f0d0024

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 611
    .local v10, "retryColor":I
    const v2, 0x7f0d0025

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    .line 612
    .local v19, "goColor":I
    const v2, 0x7f0d0024

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    .line 613
    .local v21, "showColor":I
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v2, 0x7f0e000b

    const v3, 0x7f020125

    const v5, 0x7f020125

    const v7, 0x7f09004f

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move v8, v4

    invoke-direct/range {v1 .. v8}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    .line 616
    .local v1, "dismissChoice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchFailedChoices:Ljava/util/List;

    .line 617
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchFailedChoices:Ljava/util/List;

    new-instance v7, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v8, 0x7f0e006f

    const v9, 0x7f02012c

    const v11, 0x7f02012c

    const v3, 0x7f09022d

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    move v12, v6

    move v14, v10

    invoke-direct/range {v7 .. v14}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 618
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchFailedChoices:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchActiveChoices:Ljava/util/List;

    .line 622
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchActiveChoices:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 625
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchSuccessResultsChoices:Ljava/util/List;

    .line 626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchSuccessResultsChoices:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v12, 0x7f0e0013

    const v13, 0x7f02012a

    const v15, 0x7f02012a

    const v3, 0x7f0902ea

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move/from16 v14, v21

    move/from16 v16, v6

    move/from16 v18, v21

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchSuccessResultsChoices:Ljava/util/List;

    new-instance v11, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v12, 0x7f0e0010

    const v13, 0x7f02012e

    const v15, 0x7f02012e

    const v3, 0x7f09011c

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move/from16 v14, v19

    move/from16 v16, v6

    move/from16 v18, v19

    invoke-direct/range {v11 .. v18}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchActiveChoices:Ljava/util/List;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v5, v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 630
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 631
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->layout:Landroid/view/ViewGroup;

    return-object v2
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 700
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 668
    const/4 v0, 0x0

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 673
    const/4 v0, 0x0

    return v0
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 792
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 762
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 737
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 692
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 696
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v1, 0x1

    .line 745
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    if-eqz v2, :cond_0

    .line 746
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 747
    .local v0, "gesture":Lcom/navdy/service/library/events/input/Gesture;
    sget-object v2, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    invoke-virtual {v0}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 757
    .end local v0    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 749
    .restart local v0    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showResults()V

    goto :goto_0

    .line 752
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->go()V

    .line 753
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->dismissNotification()V

    goto :goto_0

    .line 747
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 715
    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 728
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 717
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    goto :goto_0

    .line 721
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    goto :goto_0

    .line 725
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    goto :goto_0

    .line 715
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 688
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 2
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    const/4 v1, 0x0

    .line 203
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    .line 204
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->layout:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getVoiceSearchHandler()Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    .line 208
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->setVoiceSearchUserInterface(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;)V

    .line 209
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 212
    iput-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->isListeningOverBluetooth:Z

    .line 214
    iput-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->words:Ljava/util/List;

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 216
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchEntered()V

    .line 217
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->startVoiceSearch()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 580
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 581
    iput-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 582
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->stopVoiceSearch()V

    .line 583
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->setVoiceSearchUserInterface(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$VoiceSearchUserInterface;)V

    .line 584
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->layout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 585
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 587
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 588
    iput-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 592
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 593
    iput-object v2, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackAnimation:Landroid/animation/AnimatorSet;

    .line 596
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 597
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "startVoiceSearch: enabled toast"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 598
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 741
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 659
    return-void
.end method

.method public onVoiceSearchResponse(Lcom/navdy/service/library/events/audio/VoiceSearchResponse;)V
    .locals 9
    .param p1, "response"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    .prologue
    const/4 v8, 0x0

    .line 229
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onVoiceSearchResponse:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 230
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 231
    iget-boolean v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->responseFromClientTimedOut:Z

    if-eqz v3, :cond_0

    .line 232
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Not processing further response as the client did not respond in time"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 235
    :cond_0
    invoke-static {p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchResult(Lcom/navdy/service/library/events/audio/VoiceSearchResponse;)V

    .line 236
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getMusicManager()Lcom/navdy/hud/app/manager/MusicManager;

    move-result-object v1

    .line 237
    .local v1, "musicManager":Lcom/navdy/hud/app/manager/MusicManager;
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 239
    :pswitch_0
    iput-boolean v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    .line 240
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iput-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 241
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 242
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/hud/app/profile/HudLocale;->getLocaleForID(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->locale:Ljava/util/Locale;

    .line 244
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->STARTING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    .line 245
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    sget v5, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->INITIALIZATION_DELAY_MILLIS:I

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 248
    :pswitch_1
    if-eqz v1, :cond_2

    .line 249
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager;->acceptResumes()V

    .line 251
    :cond_2
    iput-boolean v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    .line 252
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iput-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 253
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->SEARCHING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    .line 254
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    sget v5, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->SEARCH_DELAY_MILLIS:I

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 257
    :pswitch_2
    iput-boolean v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    .line 258
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->isListeningOverBluetooth:Z

    .line 259
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iput-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 260
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->LISTENING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    .line 261
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->waitingTimeoutRunnable:Ljava/lang/Runnable;

    sget v5, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->INPUT_DELAY_MILLIS:I

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 264
    :pswitch_3
    if-eqz v1, :cond_3

    .line 265
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/MusicManager;->acceptResumes()V

    .line 267
    :cond_3
    iput-boolean v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    .line 268
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iput-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 269
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 270
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    goto/16 :goto_0

    .line 273
    :pswitch_4
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iput-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 274
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    if-eqz v3, :cond_6

    .line 278
    const/4 v2, 0x0

    .line 279
    .local v2, "startTrip":Z
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onVoiceSearchResponse: size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 280
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 282
    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/destination/Destination;

    .line 283
    .local v0, "d":Lcom/navdy/service/library/events/destination/Destination;
    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    if-eqz v3, :cond_4

    .line 284
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 300
    .end local v0    # "d":Lcom/navdy/service/library/events/destination/Destination;
    :cond_4
    :goto_1
    if-nez v2, :cond_5

    .line 301
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onVoiceSearchResponse: show picker"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 302
    sget-object v3, Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;->ADDITIONAL_RESULTS_LIST:Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordVoiceSearchAdditionalResultsAction(Lcom/navdy/hud/app/analytics/AnalyticsSupport$VoiceSearchAdditionalResultsAction;)V

    .line 304
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->showResults()V

    goto/16 :goto_0

    .line 288
    .restart local v0    # "d":Lcom/navdy/service/library/events/destination/Destination;
    :pswitch_5
    const/4 v2, 0x1

    .line 289
    goto :goto_1

    .line 292
    :pswitch_6
    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    .line 293
    const/4 v2, 0x1

    goto :goto_1

    .line 306
    .end local v0    # "d":Lcom/navdy/service/library/events/destination/Destination;
    :cond_5
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onVoiceSearchResponse: start trip"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 307
    iput-boolean v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    .line 308
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->voiceSearchHandler:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->go()V

    .line 309
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->dismissNotification()V

    goto/16 :goto_0

    .line 312
    .end local v2    # "startTrip":Z
    :cond_6
    iput-boolean v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z

    .line 313
    sget-object v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->IDLE:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    .line 314
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->dismissNotification()V

    goto/16 :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 284
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 710
    const/4 v0, 0x0

    return v0
.end method
