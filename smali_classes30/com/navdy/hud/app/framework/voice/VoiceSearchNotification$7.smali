.class synthetic Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;
.super Ljava/lang/Object;
.source "VoiceSearchNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$input$Gesture:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 747
    invoke-static {}, Lcom/navdy/service/library/events/input/Gesture;->values()[Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_14

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_13

    .line 715
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_12

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_11

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_10

    .line 370
    :goto_4
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->values()[Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_f

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->IDLE:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_e

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->RESULTS:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_d

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->LISTENING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_c

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->STARTING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_b

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal:[I

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->SEARCHING:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    .line 375
    :goto_a
    invoke-static {}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->values()[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError:[I

    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NO_RESULTS_FOUND:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_9

    .line 237
    :goto_b
    invoke-static {}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->values()[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_STARTING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_8

    :goto_c
    :try_start_d
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_7

    :goto_d
    :try_start_e
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_6

    :goto_e
    :try_start_f
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_5

    :goto_f
    :try_start_10
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState:[I

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_4

    .line 284
    :goto_10
    invoke-static {}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->values()[Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

    :try_start_11
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_3

    :goto_11
    :try_start_12
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_2

    :goto_12
    :try_start_13
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_1

    :goto_13
    :try_start_14
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$7;->$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType:[I

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_0

    :goto_14
    return-void

    :catch_0
    move-exception v0

    goto :goto_14

    :catch_1
    move-exception v0

    goto :goto_13

    :catch_2
    move-exception v0

    goto :goto_12

    :catch_3
    move-exception v0

    goto :goto_11

    .line 237
    :catch_4
    move-exception v0

    goto :goto_10

    :catch_5
    move-exception v0

    goto :goto_f

    :catch_6
    move-exception v0

    goto :goto_e

    :catch_7
    move-exception v0

    goto :goto_d

    :catch_8
    move-exception v0

    goto :goto_c

    .line 375
    :catch_9
    move-exception v0

    goto/16 :goto_b

    .line 370
    :catch_a
    move-exception v0

    goto/16 :goto_a

    :catch_b
    move-exception v0

    goto/16 :goto_9

    :catch_c
    move-exception v0

    goto/16 :goto_8

    :catch_d
    move-exception v0

    goto/16 :goto_7

    :catch_e
    move-exception v0

    goto/16 :goto_6

    :catch_f
    move-exception v0

    goto/16 :goto_5

    .line 715
    :catch_10
    move-exception v0

    goto/16 :goto_4

    :catch_11
    move-exception v0

    goto/16 :goto_3

    :catch_12
    move-exception v0

    goto/16 :goto_2

    .line 747
    :catch_13
    move-exception v0

    goto/16 :goto_1

    :catch_14
    move-exception v0

    goto/16 :goto_0
.end method
