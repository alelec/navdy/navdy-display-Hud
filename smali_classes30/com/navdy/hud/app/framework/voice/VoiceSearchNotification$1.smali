.class Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;
.super Ljava/lang/Object;
.source "VoiceSearchNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->access$000(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-nez v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->multipleResultsMode:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->access$102(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Z)Z

    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->access$202(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 175
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->responseFromClientTimedOut:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->access$302(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Z)Z

    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;->FAILED:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;

    # invokes: Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->access$400(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;)V

    goto :goto_0
.end method
