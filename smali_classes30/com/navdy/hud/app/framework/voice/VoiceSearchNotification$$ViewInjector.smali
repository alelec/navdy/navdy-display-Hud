.class public Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$$ViewInjector;
.super Ljava/lang/Object;
.source "VoiceSearchNotification$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e0169

    const-string v2, "field \'subStatus\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e016e

    const-string v2, "field \'imageInactive\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageInactive:Landroid/widget/ImageView;

    .line 14
    const v1, 0x7f0e016f

    const-string v2, "field \'imageActive\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageActive:Landroid/widget/ImageView;

    .line 16
    const v1, 0x7f0e0171

    const-string v2, "field \'imageStatus\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    .line 18
    const v1, 0x7f0e0170

    const-string v2, "field \'imageProcessing\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageProcessing:Landroid/widget/ImageView;

    .line 20
    const v1, 0x7f0e00d2

    const-string v2, "field \'choiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 22
    const v1, 0x7f0e016b

    const-string v2, "field \'resultsCountContainer\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCountContainer:Landroid/view/ViewGroup;

    .line 24
    const v1, 0x7f0e016c

    const-string v2, "field \'resultsCount\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCount:Landroid/widget/TextView;

    .line 26
    const v1, 0x7f0e016d

    const-string v2, "field \'listeningFeedbackView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackView:Landroid/view/View;

    .line 28
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .prologue
    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->subStatus:Landroid/widget/TextView;

    .line 32
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageInactive:Landroid/widget/ImageView;

    .line 33
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageActive:Landroid/widget/ImageView;

    .line 34
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageStatus:Landroid/widget/ImageView;

    .line 35
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageProcessing:Landroid/widget/ImageView;

    .line 36
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 37
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCountContainer:Landroid/view/ViewGroup;

    .line 38
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->resultsCount:Landroid/widget/TextView;

    .line 39
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->listeningFeedbackView:Landroid/view/View;

    .line 40
    return-void
.end method
