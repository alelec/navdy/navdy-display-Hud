.class Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;
.super Ljava/lang/Object;
.source "VoiceSearchTipsView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 135
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runningForFirstTime:Z
    invoke-static {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$600(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 136
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # setter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v3, v5}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$202(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;I)I

    .line 137
    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_TEXTS:[Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$400()[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v4}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)I

    move-result v4

    aget-object v2, v3, v4

    .line 138
    .local v2, "tipText":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v4, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v4}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)I

    move-result v4

    # invokes: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->getIconIdForIndex(I)I
    invoke-static {v3, v4}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$500(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;I)I

    move-result v0

    .line 139
    .local v0, "icon":I
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsText1:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsIcon1:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 141
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder1:Landroid/view/ViewGroup;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 142
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder2:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 143
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # setter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runningForFirstTime:Z
    invoke-static {v3, v5}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$602(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;Z)Z

    .line 152
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$100(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 153
    return-void

    .line 145
    .end local v0    # "icon":I
    .end local v2    # "tipText":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I
    invoke-static {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->TIPS_COUNT:I
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$300()I

    move-result v4

    rem-int v1, v3, v4

    .line 146
    .local v1, "nextTipIndex":I
    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_TEXTS:[Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$400()[Ljava/lang/String;

    move-result-object v3

    aget-object v2, v3, v1

    .line 147
    .restart local v2    # "tipText":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # invokes: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->getIconIdForIndex(I)I
    invoke-static {v3, v1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$500(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;I)I

    move-result v0

    .line 148
    .restart local v0    # "icon":I
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsText2:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    iget-object v3, v3, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsIcon2:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 150
    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->animatorSet:Landroid/animation/AnimatorSet;
    invoke-static {v3}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->access$700(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Landroid/animation/AnimatorSet;

    move-result-object v3

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method
