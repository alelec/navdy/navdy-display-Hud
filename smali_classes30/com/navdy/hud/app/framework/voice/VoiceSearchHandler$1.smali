.class Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$1;
.super Ljava/lang/Object;
.source "VoiceSearchHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->startVoiceSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;->access$000(Lcom/navdy/hud/app/framework/voice/VoiceSearchHandler;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 119
    return-void
.end method
