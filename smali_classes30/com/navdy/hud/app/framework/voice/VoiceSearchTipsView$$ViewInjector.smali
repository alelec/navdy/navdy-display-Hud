.class public Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$$ViewInjector;
.super Ljava/lang/Object;
.source "VoiceSearchTipsView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e0242

    const-string v2, "field \'tipsText1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsText1:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e0245

    const-string v2, "field \'tipsText2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsText2:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e0241

    const-string v2, "field \'tipsIcon1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsIcon1:Landroid/widget/ImageView;

    .line 16
    const v1, 0x7f0e0244

    const-string v2, "field \'tipsIcon2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsIcon2:Landroid/widget/ImageView;

    .line 18
    const v1, 0x7f0e0240

    const-string v2, "field \'holder1\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder1:Landroid/view/ViewGroup;

    .line 20
    const v1, 0x7f0e0243

    const-string v2, "field \'holder2\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder2:Landroid/view/ViewGroup;

    .line 22
    const v1, 0x7f0e0246

    const-string v2, "field \'voiceSearchInformationView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->voiceSearchInformationView:Landroid/view/View;

    .line 24
    const v1, 0x7f0e0249

    const-string v2, "field \'audioSourceText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceText:Landroid/widget/TextView;

    .line 26
    const v1, 0x7f0e0248

    const-string v2, "field \'audioSourceIcon\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceIcon:Landroid/widget/ImageView;

    .line 28
    const v1, 0x7f0e024b

    const-string v2, "field \'localeTagText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->localeTagText:Landroid/widget/TextView;

    .line 30
    const v1, 0x7f0e024c

    const-string v2, "field \'localeFullNameText\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->localeFullNameText:Landroid/widget/TextView;

    .line 32
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsText1:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsText2:Landroid/widget/TextView;

    .line 37
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsIcon1:Landroid/widget/ImageView;

    .line 38
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->tipsIcon2:Landroid/widget/ImageView;

    .line 39
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder1:Landroid/view/ViewGroup;

    .line 40
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder2:Landroid/view/ViewGroup;

    .line 41
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->voiceSearchInformationView:Landroid/view/View;

    .line 42
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceText:Landroid/widget/TextView;

    .line 43
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceIcon:Landroid/widget/ImageView;

    .line 44
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->localeTagText:Landroid/widget/TextView;

    .line 45
    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->localeFullNameText:Landroid/widget/TextView;

    .line 46
    return-void
.end method
