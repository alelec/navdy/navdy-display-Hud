.class public Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
.super Landroid/widget/RelativeLayout;
.source "VoiceSearchTipsView.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/activity/Main$INotificationExtensionView;


# static fields
.field public static final ANIMATION_DURATION:I = 0x258

.field public static final ANIMATION_INTERVAL:I = 0xbb8

.field private static TIPS_COUNT:I

.field private static VOICE_SEARCH_TIPS_ICONS:Landroid/content/res/TypedArray;

.field private static VOICE_SEARCH_TIPS_TEXTS:[Ljava/lang/String;


# instance fields
.field private animatorSet:Landroid/animation/AnimatorSet;

.field public audioSourceIcon:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0248
    .end annotation
.end field

.field public audioSourceText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0249
    .end annotation
.end field

.field private currentTipIndex:I

.field private fadeIn:Landroid/animation/Animator;

.field private fadeOut:Landroid/animation/Animator;

.field private fadeOutVoiceSearchInformationView:Landroid/animation/Animator;

.field private handler:Landroid/os/Handler;

.field holder1:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0240
    .end annotation
.end field

.field holder2:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0243
    .end annotation
.end field

.field private isListeningOverBluetooth:Z

.field private langauge:Ljava/lang/String;

.field private languageName:Ljava/lang/String;

.field public localeFullNameText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e024c
    .end annotation
.end field

.field public localeTagText:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e024b
    .end annotation
.end field

.field private runnable:Ljava/lang/Runnable;

.field private runningForFirstTime:Z

.field tipsIcon1:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0241
    .end annotation
.end field

.field tipsIcon2:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0244
    .end annotation
.end field

.field tipsText1:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0242
    .end annotation
.end field

.field tipsText2:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0245
    .end annotation
.end field

.field public voiceSearchInformationView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0246
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const/4 v1, 0x0

    sput v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->TIPS_COUNT:I

    .line 41
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0a0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_TEXTS:[Ljava/lang/String;

    .line 43
    sget-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_TEXTS:[Ljava/lang/String;

    array-length v1, v1

    sput v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->TIPS_COUNT:I

    .line 44
    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_ICONS:Landroid/content/res/TypedArray;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 91
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->isListeningOverBluetooth:Z

    .line 77
    iput v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runningForFirstTime:Z

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 30
    iget v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I

    return v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I

    return p1
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->TIPS_COUNT:I

    return v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_TEXTS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->getIconIdForIndex(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runningForFirstTime:Z

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runningForFirstTime:Z

    return p1
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->animatorSet:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)Landroid/animation/Animator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeOutVoiceSearchInformationView:Landroid/animation/Animator;

    return-object v0
.end method

.method private getIconIdForIndex(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 167
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->VOICE_SEARCH_TIPS_ICONS:Landroid/content/res/TypedArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 4

    .prologue
    const v2, 0x10b0001

    .line 100
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 101
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/view/View;)V

    .line 102
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x10b0000

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeIn:Landroid/animation/Animator;

    .line 103
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeOut:Landroid/animation/Animator;

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeIn:Landroid/animation/Animator;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder2:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeOut:Landroid/animation/Animator;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->holder1:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 106
    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeOutVoiceSearchInformationView:Landroid/animation/Animator;

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeOutVoiceSearchInformationView:Landroid/animation/Animator;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->voiceSearchInformationView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeOutVoiceSearchInformationView:Landroid/animation/Animator;

    new-instance v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$1;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 115
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->animatorSet:Landroid/animation/AnimatorSet;

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->animatorSet:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$2;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->animatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->animatorSet:Landroid/animation/AnimatorSet;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeIn:Landroid/animation/Animator;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->fadeOut:Landroid/animation/Animator;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 131
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->handler:Landroid/os/Handler;

    .line 132
    new-instance v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$3;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runnable:Ljava/lang/Runnable;

    .line 155
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 172
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->currentTipIndex:I

    .line 173
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->isListeningOverBluetooth:Z

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceIcon:Landroid/widget/ImageView;

    const v1, 0x7f0200dc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 175
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceText:Landroid/widget/TextView;

    const v1, 0x7f090031

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 180
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->localeTagText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->langauge:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->localeFullNameText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->languageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView$4;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 188
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceIcon:Landroid/widget/ImageView;

    const v1, 0x7f0200e3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->audioSourceText:Landroid/widget/TextView;

    const v1, 0x7f0901fd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 193
    return-void
.end method

.method public setListeningOverBluetooth(Z)V
    .locals 0
    .param p1, "isListeningOverBluetooth"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->isListeningOverBluetooth:Z

    .line 159
    return-void
.end method

.method public setVoiceSearchLocale(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 162
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->langauge:Ljava/lang/String;

    .line 163
    invoke-virtual {p1}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchTipsView;->languageName:Ljava/lang/String;

    .line 164
    return-void
.end method
