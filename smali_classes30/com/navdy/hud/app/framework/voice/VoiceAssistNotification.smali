.class public Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;
.super Ljava/lang/Object;
.source "VoiceAssistNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;


# static fields
.field private static final DEFAULT_VOICE_ASSIST_TIMEOUT:I = 0xfa0

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final SIRI_STARTING_TIMEOUT:I = 0x1388

.field private static final SIRI_TIMEOUT:I = 0x30d40

.field private static final bus:Lcom/squareup/otto/Bus;

.field private static gnow_color:I

.field private static google_now:Ljava/lang/String;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static siri:Ljava/lang/String;

.field private static siriIsActive:Ljava/lang/String;

.field private static siriIsStarting:Ljava/lang/String;

.field private static siri_color:I

.field private static voice_assist_disabled:Ljava/lang/String;

.field private static voice_assist_na:Ljava/lang/String;


# instance fields
.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

.field private image:Landroid/widget/ImageView;

.field private layout:Landroid/view/ViewGroup;

.field private mSiriTimeoutHandler:Landroid/os/Handler;

.field private mSiriTimeoutRunnable:Ljava/lang/Runnable;

.field private status:Landroid/widget/TextView;

.field private subStatus:Landroid/widget/TextView;

.field private waitingAfterTrigger:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 41
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 60
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f09011d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->google_now:Ljava/lang/String;

    .line 62
    const v1, 0x7f090268

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siri:Ljava/lang/String;

    .line 63
    const v1, 0x7f0902ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_na:Ljava/lang/String;

    .line 64
    const v1, 0x7f0902ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_disabled:Ljava/lang/String;

    .line 65
    const v1, 0x7f090269

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siriIsActive:Ljava/lang/String;

    .line 66
    const v1, 0x7f09026a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siriIsStarting:Ljava/lang/String;

    .line 69
    const v1, 0x7f0d0031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->gnow_color:I

    .line 70
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siri_color:I

    .line 72
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->bus:Lcom/squareup/otto/Bus;

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->waitingAfterTrigger:Z

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_disabled:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->subStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->image:Landroid/widget/ImageView;

    return-object v0
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 365
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#voiceassist#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 366
    return-void
.end method

.method private sendSiriKeyEvents()V
    .locals 5

    .prologue
    .line 247
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "sending siri key events"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 248
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    sget-object v3, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_SIRI:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    sget-object v4, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_DOWN:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKey;Lcom/navdy/service/library/events/input/KeyEvent;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 250
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    sget-object v3, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_SIRI:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    sget-object v4, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_UP:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKey;Lcom/navdy/service/library/events/input/KeyEvent;)V

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 252
    return-void
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    const-string v0, "navdy#voiceassist#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 6

    .prologue
    .line 183
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    .line 184
    .local v1, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v0

    .line 185
    .local v0, "deviceConnected":Z
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v2

    .line 186
    .local v2, "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :goto_0
    const/16 v3, 0xfa0

    .line 187
    .local v3, "timeout":I
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 188
    sget-object v4, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    invoke-virtual {v2}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 195
    :cond_0
    :goto_1
    return v3

    .line 185
    .end local v2    # "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .end local v3    # "timeout":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 190
    .restart local v2    # "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .restart local v3    # "timeout":I
    :pswitch_0
    const v3, 0x30d40

    goto :goto_1

    .line 188
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->VOICE_ASSIST:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 169
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    .line 170
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    const v1, 0x7f0e0168

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->status:Landroid/widget/TextView;

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    const v1, 0x7f0e0169

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->subStatus:Landroid/widget/TextView;

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    const v1, 0x7f0e00ce

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    const v1, 0x7f0e00cf

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->image:Landroid/widget/ImageView;

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 369
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 220
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 375
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 236
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 243
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 238
    :pswitch_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->waitingAfterTrigger:Z

    if-nez v0, :cond_0

    .line 239
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sendSiriKeyEvents()V

    .line 241
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 236
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 217
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 10
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    const/4 v9, 0x0

    .line 105
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    if-nez v5, :cond_0

    .line 150
    :goto_0
    return-void

    .line 109
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 110
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v5, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 112
    const/4 v0, 0x0

    .line 113
    .local v0, "color":I
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    .line 114
    .local v2, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v1

    .line 115
    .local v1, "deviceConnected":Z
    const/4 v4, 0x0

    .line 116
    .local v4, "startFluctuating":Z
    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v3

    .line 117
    .local v3, "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :goto_1
    if-eqz v1, :cond_1

    if-nez v3, :cond_3

    .line 118
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->status:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_na:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->subStatus:Landroid/widget/TextView;

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->image:Landroid/widget/ImageView;

    const v6, 0x7f020221

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 141
    :goto_2
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v5, v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setFillColor(I)V

    .line 142
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v5, v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setStrokeColor(I)V

    .line 143
    if-eqz v4, :cond_4

    .line 144
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v5, v9}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 145
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->start()V

    goto :goto_0

    .line 116
    .end local v3    # "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 122
    .restart local v3    # "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :cond_3
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform:[I

    invoke-virtual {v3}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 137
    :goto_3
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "sending VoiceAssistRequest"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 138
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v7, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {v6, v7}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_2

    .line 124
    :pswitch_0
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->status:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->google_now:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->subStatus:Landroid/widget/TextView;

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->image:Landroid/widget/ImageView;

    const v6, 0x7f020130

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 127
    sget v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->gnow_color:I

    .line 128
    goto :goto_3

    .line 131
    :pswitch_1
    sget v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siri_color:I

    .line 132
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->status:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siri:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->subStatus:Landroid/widget/TextView;

    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siriIsStarting:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->image:Landroid/widget/ImageView;

    const v6, 0x7f0201e3

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 147
    :cond_4
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 148
    iget-object v5, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    goto/16 :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 155
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 162
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 163
    sget-object v0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "startVoiceAssistance: enabled toast"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 372
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public onVoiceAssistResponse(Lcom/navdy/service/library/events/audio/VoiceAssistResponse;)V
    .locals 14
    .param p1, "response"    # Lcom/navdy/service/library/events/audio/VoiceAssistResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x0

    .line 256
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;

    if-nez v7, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutHandler:Landroid/os/Handler;

    if-eqz v7, :cond_2

    .line 260
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 262
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    .line 263
    .local v2, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v0

    .line 264
    .local v0, "deviceConnected":Z
    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v3

    .line 265
    .local v3, "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :goto_1
    const-string v6, ""

    .line 266
    .local v6, "title":Ljava/lang/String;
    const-string v5, ""

    .line 267
    .local v5, "subTitle":Ljava/lang/String;
    const/4 v4, 0x0

    .line 268
    .local v4, "startFluctuating":Z
    const/4 v1, 0x0

    .line 269
    .local v1, "icon":I
    if-eqz v0, :cond_3

    if-nez v3, :cond_6

    .line 271
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_na:Ljava/lang/String;

    .line 272
    const v1, 0x7f020221

    .line 273
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 274
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7, v13}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 351
    :cond_4
    :goto_2
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->status:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->subStatus:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->image:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 355
    if-eqz v4, :cond_b

    .line 356
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7, v12}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 357
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->start()V

    goto :goto_0

    .line 264
    .end local v1    # "icon":I
    .end local v3    # "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .end local v4    # "startFluctuating":Z
    .end local v5    # "subTitle":Ljava/lang/String;
    .end local v6    # "title":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 276
    .restart local v1    # "icon":I
    .restart local v3    # "remoteDevicePlatform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    .restart local v4    # "startFluctuating":Z
    .restart local v5    # "subTitle":Ljava/lang/String;
    .restart local v6    # "title":Ljava/lang/String;
    :cond_6
    sget-object v7, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "received response "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;->state:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 277
    sget-object v7, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$2;->$SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState:[I

    iget-object v8, p1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;->state:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto :goto_2

    .line 279
    :pswitch_0
    sget-object v7, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v3, v7, :cond_4

    .line 280
    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siri:Ljava/lang/String;

    .line 281
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siriIsStarting:Ljava/lang/String;

    .line 282
    const v1, 0x7f0201e3

    .line 283
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->waitingAfterTrigger:Z

    .line 284
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sendSiriKeyEvents()V

    .line 285
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutHandler:Landroid/os/Handler;

    if-nez v7, :cond_7

    .line 286
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    iput-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutHandler:Landroid/os/Handler;

    .line 288
    :cond_7
    new-instance v7, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;-><init>(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)V

    iput-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutRunnable:Ljava/lang/Runnable;

    .line 300
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->mSiriTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v10, 0x1388

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 305
    :pswitch_1
    sget-object v7, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v3, v7, :cond_8

    .line 307
    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->google_now:Ljava/lang/String;

    .line 308
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_disabled:Ljava/lang/String;

    .line 309
    const v1, 0x7f020130

    .line 316
    :goto_3
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7, v13}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 317
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 318
    iput-boolean v12, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->waitingAfterTrigger:Z

    goto/16 :goto_2

    .line 312
    :cond_8
    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siri:Ljava/lang/String;

    .line 313
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_disabled:Ljava/lang/String;

    .line 314
    const v1, 0x7f0201e3

    goto :goto_3

    .line 322
    :pswitch_2
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7, v13}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 323
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 324
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->dismissNotification()V

    .line 325
    iput-boolean v12, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->waitingAfterTrigger:Z

    goto/16 :goto_0

    .line 329
    :pswitch_3
    sget-object v7, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_Android:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v3, v7, :cond_9

    .line 331
    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->google_now:Ljava/lang/String;

    .line 332
    const v1, 0x7f020131

    .line 333
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 336
    :cond_9
    sget-object v6, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siri:Ljava/lang/String;

    .line 337
    sget-object v5, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->siriIsActive:Ljava/lang/String;

    .line 338
    const v1, 0x7f0201e3

    .line 339
    const/4 v4, 0x1

    .line 340
    iget-boolean v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->waitingAfterTrigger:Z

    if-eqz v7, :cond_a

    .line 341
    iput-boolean v12, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->waitingAfterTrigger:Z

    goto/16 :goto_2

    .line 344
    :cond_a
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sendSiriKeyEvents()V

    goto/16 :goto_2

    .line 359
    :cond_b
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7, v13}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 360
    iget-object v7, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    invoke-virtual {v7}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    goto/16 :goto_0

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method
