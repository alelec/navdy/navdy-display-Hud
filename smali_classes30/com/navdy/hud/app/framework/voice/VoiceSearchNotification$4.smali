.class Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "VoiceSearchNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->updateUI(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$VoiceSearchStateInternal;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    .prologue
    .line 475
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->canceled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 494
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->canceled:Z

    .line 495
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->canceled:Z

    if-nez v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->access$500(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)Landroid/animation/AnimatorSet;

    move-result-object v0

    const-wide/16 v2, 0x21

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 488
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->processingImageAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->access$500(Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 490
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 480
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification;->imageProcessing:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceSearchNotification$4;->canceled:Z

    .line 482
    return-void
.end method
