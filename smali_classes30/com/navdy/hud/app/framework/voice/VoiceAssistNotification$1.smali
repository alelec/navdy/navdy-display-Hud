.class Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;
.super Ljava/lang/Object;
.source "VoiceAssistNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->onVoiceAssistResponse(Lcom/navdy/service/library/events/audio/VoiceAssistResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->layout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->access$000(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 292
    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Siri request did not get any response and timed out"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->fluctuator:Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->access$200(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/FluctuatorAnimatorView;->stop()V

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->subStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->access$400(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Landroid/widget/TextView;

    move-result-object v0

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->voice_assist_disabled:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->access$300()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v0, p0, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification$1;->this$0:Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;

    # getter for: Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->image:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;->access$500(Lcom/navdy/hud/app/framework/voice/VoiceAssistNotification;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201e3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 298
    :cond_0
    return-void
.end method
