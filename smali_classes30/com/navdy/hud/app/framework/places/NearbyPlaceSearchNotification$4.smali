.class Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$4;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "NearbyPlaceSearchNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->startLoadingAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 768
    iput-object p1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 771
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$1000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 772
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$1000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x21

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 773
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$4;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$1000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 777
    :goto_0
    return-void

    .line 775
    :cond_0
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "abandon loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
