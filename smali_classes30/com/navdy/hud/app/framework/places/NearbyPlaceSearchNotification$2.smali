.class Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;
.super Ljava/lang/Object;
.source "NearbyPlaceSearchNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->launchPicker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private itemSelected:Z

.field private retrySelected:Z

.field final synthetic this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    const/4 v0, 0x0

    .line 499
    iput-object p1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 500
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->itemSelected:Z

    .line 501
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->retrySelected:Z

    return-void
.end method


# virtual methods
.method public onDestinationPickerClosed()V
    .locals 5

    .prologue
    .line 525
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->itemSelected:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->retrySelected:Z

    if-nez v2, :cond_0

    .line 526
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchResultsClose()V

    .line 528
    :cond_0
    iget-boolean v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->retrySelected:Z

    if-eqz v2, :cond_2

    .line 529
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    .line 530
    .local v1, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v2, "navdy#place#type#search#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .line 531
    .local v0, "notif":Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
    if-nez v0, :cond_1

    .line 532
    new-instance v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .end local v0    # "notif":Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;
    invoke-static {v2}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$300(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;-><init>(Lcom/navdy/service/library/events/places/PlaceType;)V

    .line 534
    .restart local v0    # "notif":Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
    :cond_1
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "launching notif search again:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;
    invoke-static {v4}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$300(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 535
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    .line 537
    .end local v0    # "notif":Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
    .end local v1    # "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    :cond_2
    return-void
.end method

.method public onItemClicked(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 3
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    const/4 v0, 0x1

    .line 506
    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$300(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchSelection(Lcom/navdy/service/library/events/places/PlaceType;I)V

    .line 507
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->itemSelected:Z

    .line 508
    const v1, 0x7f0e0070

    if-ne p1, v1, :cond_0

    .line 510
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "search again"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 511
    iput-boolean v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;->retrySelected:Z

    .line 514
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemSelected(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    .line 520
    const/4 v0, 0x0

    return v0
.end method
