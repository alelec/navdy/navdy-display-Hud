.class Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;
.super Ljava/lang/Object;
.source "NearbyPlaceSearchNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    .prologue
    .line 642
    iput-object p1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;->this$1:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 645
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;->this$1:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-nez v0, :cond_0

    .line 651
    :goto_0
    return-void

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;->this$1:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$700(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;->this$1:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->timeoutForFailure:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$600(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 649
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;->this$1:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # invokes: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->stopLoadingAnimation()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$100(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V

    .line 650
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;->this$1:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # invokes: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->goToFailedState()V
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$200(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V

    goto :goto_0
.end method
