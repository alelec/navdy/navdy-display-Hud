.class synthetic Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;
.super Ljava/lang/Object;
.source "NearbyPlaceSearchNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$RequestStatus:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$input$Gesture:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$places$PlaceType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 660
    invoke-static {}, Lcom/navdy/service/library/events/places/PlaceType;->values()[Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_11

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_10

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_f

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_e

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_d

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_c

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_b

    .line 431
    :goto_6
    invoke-static {}, Lcom/navdy/service/library/events/RequestStatus;->values()[Lcom/navdy/service/library/events/RequestStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/RequestStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_a

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/RequestStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_9

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/RequestStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_8

    .line 365
    :goto_9
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->values()[Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState:[I

    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCHING:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_7

    :goto_a
    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_6

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->NO_RESULTS:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_5

    :goto_c
    :try_start_d
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState:[I

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_4

    .line 341
    :goto_d
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_e
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_3

    :goto_e
    :try_start_f
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_2

    :goto_f
    :try_start_10
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_1

    .line 325
    :goto_10
    invoke-static {}, Lcom/navdy/service/library/events/input/Gesture;->values()[Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    :try_start_11
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_0

    :goto_11
    return-void

    :catch_0
    move-exception v0

    goto :goto_11

    .line 341
    :catch_1
    move-exception v0

    goto :goto_10

    :catch_2
    move-exception v0

    goto :goto_f

    :catch_3
    move-exception v0

    goto :goto_e

    .line 365
    :catch_4
    move-exception v0

    goto :goto_d

    :catch_5
    move-exception v0

    goto :goto_c

    :catch_6
    move-exception v0

    goto :goto_b

    :catch_7
    move-exception v0

    goto :goto_a

    .line 431
    :catch_8
    move-exception v0

    goto/16 :goto_9

    :catch_9
    move-exception v0

    goto/16 :goto_8

    :catch_a
    move-exception v0

    goto/16 :goto_7

    .line 660
    :catch_b
    move-exception v0

    goto/16 :goto_6

    :catch_c
    move-exception v0

    goto/16 :goto_5

    :catch_d
    move-exception v0

    goto/16 :goto_4

    :catch_e
    move-exception v0

    goto/16 :goto_3

    :catch_f
    move-exception v0

    goto/16 :goto_2

    :catch_10
    move-exception v0

    goto/16 :goto_1

    :catch_11
    move-exception v0

    goto/16 :goto_0
.end method
