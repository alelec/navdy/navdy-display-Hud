.class final enum Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;
.super Ljava/lang/Enum;
.source "NearbyPlaceSearchNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PlaceTypeSearchState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

.field public static final enum ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

.field public static final enum NO_RESULTS:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

.field public static final enum SEARCHING:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

.field public static final enum SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 754
    new-instance v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    const-string v1, "SEARCHING"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCHING:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 755
    new-instance v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 756
    new-instance v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    const-string v1, "NO_RESULTS"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->NO_RESULTS:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 757
    new-instance v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    const-string v1, "SEARCH_COMPLETE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 753
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCHING:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->NO_RESULTS:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->$VALUES:[Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 753
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 753
    const-class v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;
    .locals 1

    .prologue
    .line 753
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->$VALUES:[Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    return-object v0
.end method
