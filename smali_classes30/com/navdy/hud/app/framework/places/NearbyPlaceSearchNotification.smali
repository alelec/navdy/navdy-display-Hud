.class public Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
.super Ljava/lang/Object;
.source "NearbyPlaceSearchNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;
    }
.end annotation


# static fields
.field private static final ICON_SCALE:F = 1.38f

.field private static final N_OFFLINE_RESULTS:I = 0x7

.field private static final TIMEOUT:J = 0x2710L

.field private static final choicesMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final searchAgain:Ljava/lang/String;

.field private static final searchColor:I

.field private static final secondaryColor:I


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00d2
    .end annotation
.end field

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

.field private final handler:Landroid/os/Handler;

.field iconColorView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e015e
    .end annotation
.end field

.field iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0163
    .end annotation
.end field

.field private loadingAnimator:Landroid/animation/ObjectAnimator;

.field private final notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

.field private final placeType:Lcom/navdy/service/library/events/places/PlaceType;

.field private final requestId:Ljava/lang/String;

.field resultsCount:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e015f
    .end annotation
.end field

.field resultsLabel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0160
    .end annotation
.end field

.field private returnedDestinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field private final speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field statusBadge:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0162
    .end annotation
.end field

.field subTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e0111
    .end annotation
.end field

.field private final timeoutForFailure:Ljava/lang/Runnable;

.field title:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f0e00c3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 26

    .prologue
    .line 80
    new-instance v2, Lcom/navdy/service/library/log/Logger;

    const-class v3, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    .line 95
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v20

    .line 96
    .local v20, "context":Landroid/content/Context;
    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 98
    .local v23, "resources":Landroid/content/res/Resources;
    const v2, 0x7f0900dc

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 99
    .local v7, "dismiss":Ljava/lang/String;
    const/high16 v6, -0x1000000

    .line 100
    .local v6, "unselectedColor":I
    const v2, 0x7f0d0021

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 101
    .local v4, "dismissColor":I
    const v2, 0x7f0d0024

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 102
    .local v11, "retryColor":I
    new-instance v1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v2, 0x7f0e000b

    const v3, 0x7f020125

    const v5, 0x7f020125

    move v8, v4

    invoke-direct/range {v1 .. v8}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    .line 103
    .local v1, "dismissChoice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v9, 0x7f0e006f

    const v10, 0x7f02012c

    const v12, 0x7f02012c

    const v2, 0x7f09022d

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    move v13, v6

    move v15, v11

    invoke-direct/range {v8 .. v15}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    .line 105
    .local v8, "retryChoice":Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v25, "searchingChoices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    new-instance v12, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v13, 0x7f0e000b

    const v14, 0x7f020125

    const v16, 0x7f020125

    const v2, 0x7f09004f

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move v15, v4

    move/from16 v17, v6

    move/from16 v19, v4

    invoke-direct/range {v12 .. v19}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    move-object/from16 v0, v25

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v21, "errorChoices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    move-object/from16 v0, v21

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    move-object/from16 v0, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v22, "noResultsChoices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    move-object/from16 v0, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    const v2, 0x7f0d0024

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    .line 117
    .local v15, "readColor":I
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v24, "searchCompleteChoices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;>;"
    new-instance v12, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const v13, 0x7f0e000b

    const v14, 0x7f02012a

    const v16, 0x7f02012a

    const v2, 0x7f0902ea

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move/from16 v17, v6

    move/from16 v19, v15

    invoke-direct/range {v12 .. v19}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    move-object/from16 v0, v24

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    move-object/from16 v0, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    const v2, 0x7f0d0099

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->secondaryColor:I

    .line 123
    new-instance v2, Ljava/util/HashMap;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    .line 125
    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    sget-object v3, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCHING:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    move-object/from16 v0, v25

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    sget-object v3, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    move-object/from16 v0, v21

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    sget-object v3, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->NO_RESULTS:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    move-object/from16 v0, v22

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    sget-object v3, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    move-object/from16 v0, v24

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const v2, 0x7f0d0073

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->searchColor:I

    .line 131
    const v2, 0x7f090254

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->searchAgain:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/PlaceType;)V
    .locals 2
    .param p1, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    new-instance v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$1;-><init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->timeoutForFailure:Ljava/lang/Runnable;

    .line 186
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->bus:Lcom/squareup/otto/Bus;

    .line 187
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 188
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    .line 190
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->handler:Landroid/os/Handler;

    .line 192
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->requestId:Ljava/lang/String;

    .line 193
    iput-object p1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 194
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->stopLoadingAnimation()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->goToFailedState()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/service/library/events/places/PlaceType;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    return-object v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->timeoutForFailure:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->goToSuccessfulState(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->goToZeroResultsState()V

    return-void
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 750
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->notificationManager:Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    const-string v1, "navdy#place#type#search#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 751
    return-void
.end method

.method private getHereCategoryFilter(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/here/android/mpa/search/CategoryFilter;
    .locals 5
    .param p1, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 658
    new-instance v0, Lcom/here/android/mpa/search/CategoryFilter;

    invoke-direct {v0}, Lcom/here/android/mpa/search/CategoryFilter;-><init>()V

    .line 660
    .local v0, "categoryFilter":Lcom/here/android/mpa/search/CategoryFilter;
    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 685
    :cond_0
    :goto_0
    return-object v0

    .line 662
    :pswitch_0
    const-string v2, "petrol-station"

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    goto :goto_0

    .line 665
    :pswitch_1
    const-string v2, "parking-facility"

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    goto :goto_0

    .line 668
    :pswitch_2
    sget-object v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->HERE_PLACE_TYPE_RESTAURANT:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 669
    .local v1, "restaurantPlaceType":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    .line 668
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 673
    .end local v1    # "restaurantPlaceType":Ljava/lang/String;
    :pswitch_3
    const-string v2, "shopping"

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    goto :goto_0

    .line 676
    :pswitch_4
    const-string v2, "coffee-tea"

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    goto :goto_0

    .line 679
    :pswitch_5
    const-string v2, "atm-bank-exchange"

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    goto :goto_0

    .line 682
    :pswitch_6
    const-string v2, "hospital-health-care-facility"

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/search/CategoryFilter;->add(Ljava/lang/String;)Lcom/here/android/mpa/search/CategoryFilter;

    goto :goto_0

    .line 660
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private goToFailedState()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 738
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconColorView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v1}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v1

    iget v1, v1, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->iconRes:I

    sget v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->secondaryColor:I

    const/4 v3, 0x0

    const v4, 0x3fb0a3d7    # 1.38f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 739
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->subTitle:Landroid/widget/TextView;

    const v1, 0x7f090204

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 740
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    const v1, 0x7f0200d9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 741
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 742
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 744
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 746
    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0, v5, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 747
    return-void
.end method

.method private goToSuccessfulState(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 689
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 693
    .local v3, "resources":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconColorView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    sget v5, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->secondaryColor:I

    const v6, 0x3fb0a3d7    # 1.38f

    invoke-virtual {v4, v7, v5, v8, v6}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 695
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsCount:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 696
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsCount:Landroid/widget/TextView;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 697
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsLabel:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 699
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v2

    .line 700
    .local v2, "holder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    iget v1, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->iconRes:I

    .line 701
    .local v1, "badgeRes":I
    iget v4, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->colorRes:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 703
    .local v0, "badgeColor":I
    if-eqz v1, :cond_0

    .line 704
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 705
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v4, v1, v0, v8, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 706
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v4, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 709
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    iput-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 710
    iget-object v5, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v4, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    iget-object v6, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-virtual {v5, v4, v7, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 712
    iput-object p1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->returnedDestinations:Ljava/util/List;

    .line 713
    return-void
.end method

.method private goToZeroResultsState()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 716
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 718
    .local v3, "resources":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconColorView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    sget v5, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->secondaryColor:I

    const v6, 0x3fb0a3d7    # 1.38f

    invoke-virtual {v4, v7, v5, v8, v6}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 720
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsCount:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 721
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsCount:Landroid/widget/TextView;

    const v5, 0x7f090321

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 722
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsLabel:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 724
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v4}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v2

    .line 725
    .local v2, "holder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    iget v1, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->iconRes:I

    .line 726
    .local v1, "badgeRes":I
    iget v4, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->colorRes:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 728
    .local v0, "badgeColor":I
    if-eqz v1, :cond_0

    .line 729
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 730
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v4, v1, v0, v8, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 731
    iget-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v4, v7}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 733
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->NO_RESULTS:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    iput-object v4, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 734
    iget-object v5, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v4, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    iget-object v6, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-virtual {v5, v4, v7, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 735
    return-void
.end method

.method private launchPicker()V
    .locals 35

    .prologue
    .line 445
    sget-object v5, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "launch picker screen"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 447
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v27

    .line 448
    .local v27, "context":Landroid/content/Context;
    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    .line 450
    .local v33, "resources":Landroid/content/res/Resources;
    new-instance v26, Landroid/os/Bundle;

    invoke-direct/range {v26 .. v26}, Landroid/os/Bundle;-><init>()V

    .line 451
    .local v26, "args":Landroid/os/Bundle;
    const-string v5, "PICKER_SHOW_DESTINATION_MAP"

    const/4 v6, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 452
    const/16 v31, -0x1

    .line 453
    .local v31, "icon":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v30

    .line 454
    .local v30, "holder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    if-eqz v30, :cond_0

    .line 455
    move-object/from16 v0, v30

    iget v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->destinationIcon:I

    move/from16 v31, v0

    .line 457
    :cond_0
    const-string v5, "PICKER_DESTINATION_ICON"

    move-object/from16 v0, v26

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 458
    const-string v5, "PICKER_LEFT_TITLE"

    const v6, 0x7f090218

    move-object/from16 v0, v33

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v5, "PICKER_LEFT_ICON"

    const v6, 0x7f020186

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 460
    const-string v5, "PICKER_LEFT_ICON_BKCOLOR"

    const v6, 0x7f0d0073

    move-object/from16 v0, v27

    invoke-static {v0, v6}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 461
    const-string v5, "PICKER_INITIAL_SELECTION"

    const/4 v6, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 463
    move-object/from16 v0, v30

    iget v5, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->colorRes:I

    move-object/from16 v0, v27

    invoke-static {v0, v5}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v34

    .line 464
    .local v34, "selectedColor":I
    const v5, 0x7f0d003e

    move-object/from16 v0, v27

    invoke-static {v0, v5}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v23

    .line 467
    .local v23, "unselectedColor":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v32

    .line 468
    .local v32, "placeTypeResourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    new-instance v4, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    const v5, 0x7f0e0070

    move-object/from16 v0, v32

    iget v6, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->titleRes:I

    .line 470
    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->searchAgain:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    const v20, 0x7f020186

    const/16 v21, 0x0

    sget v22, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->searchColor:I

    sget-object v24, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->NONE:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    const/16 v25, 0x0

    invoke-direct/range {v4 .. v25}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;DDDDIIIILcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;Lcom/navdy/service/library/events/places/PlaceType;)V

    .line 487
    .local v4, "searchAgainItem":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->returnedDestinations:Ljava/util/List;

    const/4 v6, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v34

    move/from16 v2, v23

    invoke-static {v0, v5, v1, v2, v6}, Lcom/navdy/hud/app/maps/util/DestinationUtil;->convert(Landroid/content/Context;Ljava/util/List;IIZ)Ljava/util/List;

    move-result-object v29

    .line 490
    .local v29, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;>;"
    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-interface {v0, v5, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 491
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v5

    new-array v0, v5, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    move-object/from16 v28, v0

    .line 492
    .local v28, "destinationParcelables":[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 494
    const-string v5, "PICKER_DESTINATIONS"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 495
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v5

    const-string v6, "navdy#place#type#search#notif"

    const/4 v7, 0x1

    sget-object v8, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    new-instance v10, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$2;-><init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V

    move-object/from16 v9, v26

    invoke-virtual/range {v5 .. v10}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;ZLcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 539
    return-void
.end method

.method private performOfflineSearch()V
    .locals 4

    .prologue
    .line 559
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->getHereCategoryFilter(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/here/android/mpa/search/CategoryFilter;

    move-result-object v0

    const/4 v1, 0x7

    new-instance v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;-><init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Z)V

    .line 655
    return-void
.end method

.method private recordSelection(II)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    const v2, 0x7f0e000b

    .line 365
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState:[I

    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 386
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 367
    :pswitch_1
    if-ne p2, v2, :cond_0

    .line 368
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchDismiss()V

    goto :goto_0

    .line 374
    :pswitch_2
    const v0, 0x7f0e006f

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchNoResult(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 377
    :pswitch_3
    if-ne p2, v2, :cond_0

    .line 378
    if-nez p1, :cond_2

    .line 379
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchResultsView()V

    goto :goto_0

    .line 381
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNearbySearchResultsDismiss()V

    goto :goto_0

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private sendRequest()V
    .locals 6

    .prologue
    .line 542
    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCHING:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    iput-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    .line 543
    new-instance v1, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->requestId:Ljava/lang/String;

    .line 544
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;->request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    .line 545
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;->place_type(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;

    move-result-object v1

    .line 546
    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest$Builder;->build()Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;

    move-result-object v0

    .line 548
    .local v0, "placeTypeSearchRequest":Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isAppConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 549
    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "performing online quick search"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 550
    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 555
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->timeoutForFailure:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 556
    return-void

    .line 552
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "performing offline quick search"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 553
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->performOfflineSearch()V

    goto :goto_0
.end method

.method private startLoadingAnimation()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 761
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    const v1, 0x7f02022e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 763
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 764
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 765
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/high16 v3, 0x43b40000    # 360.0f

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    .line 766
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 767
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 768
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$4;-><init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 780
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 781
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "started loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 782
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 784
    :cond_1
    return-void
.end method

.method private stopLoadingAnimation()V
    .locals 2

    .prologue
    .line 787
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 788
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cancelled loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 789
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 790
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 791
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 792
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    .line 794
    :cond_0
    return-void
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 3
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 389
    iget v1, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->pos:I

    iget v2, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->recordSelection(II)V

    .line 390
    iget v1, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    sparse-switch v1, :sswitch_data_0

    .line 410
    :goto_0
    return-void

    .line 392
    :sswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    if-ne v1, v2, :cond_2

    .line 393
    iget v1, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->pos:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 395
    .local v0, "hasSelectedViewOption":Z
    :goto_1
    if-eqz v0, :cond_1

    .line 396
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->launchPicker()V

    goto :goto_0

    .line 393
    .end local v0    # "hasSelectedViewOption":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 398
    .restart local v0    # "hasSelectedViewOption":Z
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->dismissNotification()V

    goto :goto_0

    .line 401
    .end local v0    # "hasSelectedViewOption":Z
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->dismissNotification()V

    goto :goto_0

    .line 406
    :sswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->startLoadingAnimation()V

    .line 407
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->sendRequest()V

    goto :goto_0

    .line 390
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0e000b -> :sswitch_0
        0x7f0e006f -> :sswitch_1
    .end sparse-switch
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 229
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const-string v0, "navdy#place#type#search#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->PLACE_TYPE_SEARCH:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 208
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003e

    invoke-virtual {v2, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 209
    .local v1, "view":Landroid/view/View;
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 211
    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v2}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v0

    .line 212
    .local v0, "placeTypeResourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->title:Landroid/widget/TextView;

    iget v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->titleRes:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconColorView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->iconRes:I

    .line 216
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->colorRes:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const v5, 0x3fb0a3d7    # 1.38f

    .line 214
    invoke-virtual {v2, v3, v4, v7, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 221
    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v2, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 222
    iget-object v3, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choicesMapping:Ljava/util/Map;

    sget-object v4, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCHING:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-virtual {v3, v2, v6, p0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 224
    return-object v1
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 294
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    return v0
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 413
    return-void
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 309
    return-void
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 286
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v0, 0x0

    .line 317
    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v1, :cond_1

    .line 332
    :cond_0
    :goto_0
    return v0

    .line 321
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->SEARCH_COMPLETE:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    if-ne v1, v2, :cond_0

    .line 325
    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 327
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "show route picker:gesture"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 328
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->launchPicker()V

    .line 329
    const/4 v0, 0x1

    goto :goto_0

    .line 325
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 337
    iget-object v2, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_0

    .line 354
    :goto_0
    return v0

    .line 341
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 343
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    move v0, v1

    .line 344
    goto :goto_0

    .line 347
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    move v0, v1

    .line 348
    goto :goto_0

    .line 351
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    move v0, v1

    .line 352
    goto :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 282
    return-void
.end method

.method public onPlaceTypeSearchResponse(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 417
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlaceTypeSearchResponse:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 418
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->timeoutForFailure:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 419
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->stopLoadingAnimation()V

    .line 421
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_id:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->requestId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 422
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "received wrong request_id on the PlaceTypeSearchResponse, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 442
    :goto_0
    return-void

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    sget-object v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    if-ne v0, v1, :cond_1

    .line 427
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "received response after place type search has already failed, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 431
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$5;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/RequestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 433
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->goToFailedState()V

    goto :goto_0

    .line 436
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->goToZeroResultsState()V

    goto :goto_0

    .line 439
    :pswitch_2
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;->destinations:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->goToSuccessfulState(Ljava/util/List;)V

    goto :goto_0

    .line 431
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 1
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 240
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 241
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->startLoadingAnimation()V

    .line 242
    invoke-direct {p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->sendRequest()V

    .line 243
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 252
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 253
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 313
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method
