.class Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;
.super Ljava/lang/Object;
.source "NearbyPlaceSearchNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->performOfflineSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    .line 559
    iput-object p1, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 562
    .local p1, "places":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Place;>;"
    iget-object v10, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v10}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v10

    if-nez v10, :cond_0

    .line 633
    :goto_0
    return-void

    .line 566
    :cond_0
    iget-object v10, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->currentState:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;
    invoke-static {v10}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$500(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    move-result-object v10

    sget-object v11, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;->ERROR:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$PlaceTypeSearchState;

    if-ne v10, v11, :cond_1

    .line 567
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    const-string v11, "received response after place type search has already failed, no-op"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 571
    :cond_1
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    const-string v11, "performing offline search, returned places: "

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 573
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/here/android/mpa/search/Place;

    .line 574
    .local v7, "place":Lcom/here/android/mpa/search/Place;
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v11

    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 576
    .end local v7    # "place":Lcom/here/android/mpa/search/Place;
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 578
    .local v4, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/here/android/mpa/search/Place;

    .line 579
    .restart local v7    # "place":Lcom/here/android/mpa/search/Place;
    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v10

    invoke-virtual {v10}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .line 581
    .local v2, "coords":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v5, Lcom/navdy/service/library/events/location/LatLong;

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-direct {v5, v10, v12}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    .line 584
    .local v5, "displayCoords":Lcom/navdy/service/library/events/location/LatLong;
    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v10

    invoke-virtual {v10}, Lcom/here/android/mpa/search/Location;->getAccessPoints()Ljava/util/List;

    move-result-object v1

    .line 586
    .local v1, "accessPoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/NavigationPosition;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_3

    .line 587
    const/4 v10, 0x0

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/here/android/mpa/search/NavigationPosition;

    invoke-virtual {v10}, Lcom/here/android/mpa/search/NavigationPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 588
    .local v0, "accessPointCoords":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v6, Lcom/navdy/service/library/events/location/LatLong;

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-direct {v6, v10, v12}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    .line 593
    .end local v0    # "accessPointCoords":Lcom/here/android/mpa/common/GeoCoordinate;
    .local v6, "navigationCoords":Lcom/navdy/service/library/events/location/LatLong;
    :goto_3
    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v10

    invoke-virtual {v10}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v8

    .line 595
    .local v8, "placeAddress":Lcom/here/android/mpa/search/Address;
    invoke-virtual {v8}, Lcom/here/android/mpa/search/Address;->getHouseNumber()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_4

    invoke-virtual {v8}, Lcom/here/android/mpa/search/Address;->getStreet()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 596
    invoke-virtual {v8}, Lcom/here/android/mpa/search/Address;->getHouseNumber()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Lcom/here/android/mpa/search/Address;->getStreet()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 599
    .local v9, "subtitle":Ljava/lang/String;
    :goto_4
    new-instance v10, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v10}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    .line 600
    invoke-virtual {v10, v6}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    .line 601
    invoke-virtual {v10, v5}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    .line 602
    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v12

    invoke-virtual {v12}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v12

    invoke-virtual {v12}, Lcom/here/android/mpa/search/Address;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    .line 603
    invoke-virtual {v7}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    .line 604
    invoke-virtual {v10, v9}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    sget-object v12, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 605
    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    .line 606
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    sget-object v12, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_NONE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 607
    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    const/4 v12, 0x0

    .line 608
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    const-wide/16 v12, 0x0

    .line 609
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    iget-object v12, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .line 610
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->placeType:Lcom/navdy/service/library/events/places/PlaceType;
    invoke-static {v12}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$300(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_type(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v10

    .line 611
    invoke-virtual {v10}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v3

    .line 613
    .local v3, "destination":Lcom/navdy/service/library/events/destination/Destination;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 590
    .end local v3    # "destination":Lcom/navdy/service/library/events/destination/Destination;
    .end local v6    # "navigationCoords":Lcom/navdy/service/library/events/location/LatLong;
    .end local v8    # "placeAddress":Lcom/here/android/mpa/search/Address;
    .end local v9    # "subtitle":Ljava/lang/String;
    :cond_3
    move-object v6, v5

    .restart local v6    # "navigationCoords":Lcom/navdy/service/library/events/location/LatLong;
    goto/16 :goto_3

    .line 597
    .restart local v8    # "placeAddress":Lcom/here/android/mpa/search/Address;
    :cond_4
    invoke-virtual {v8}, Lcom/here/android/mpa/search/Address;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_4

    .line 616
    .end local v1    # "accessPoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/NavigationPosition;>;"
    .end local v2    # "coords":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v5    # "displayCoords":Lcom/navdy/service/library/events/location/LatLong;
    .end local v6    # "navigationCoords":Lcom/navdy/service/library/events/location/LatLong;
    .end local v7    # "place":Lcom/here/android/mpa/search/Place;
    .end local v8    # "placeAddress":Lcom/here/android/mpa/search/Address;
    :cond_5
    iget-object v10, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->handler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$700(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/os/Handler;

    move-result-object v10

    new-instance v11, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$1;

    invoke-direct {v11, p0, v4}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$1;-><init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;Ljava/util/List;)V

    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method

.method public onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    .prologue
    .line 637
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$000(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-nez v0, :cond_0

    .line 653
    :goto_0
    return-void

    .line 640
    :cond_0
    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error while performing offline search: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 642
    iget-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;->this$0:Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    # getter for: Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->access$700(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3$2;-><init>(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
