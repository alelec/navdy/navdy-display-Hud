.class public Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification$$ViewInjector;
.super Ljava/lang/Object;
.source "NearbyPlaceSearchNotification$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f0e00c3

    const-string v2, "field \'title\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->title:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f0e0111

    const-string v2, "field \'subTitle\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->subTitle:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f0e015e

    const-string v2, "field \'iconColorView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconColorView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 16
    const v1, 0x7f0e015f

    const-string v2, "field \'resultsCount\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsCount:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f0e0160

    const-string v2, "field \'resultsLabel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsLabel:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0e0162

    const-string v2, "field \'statusBadge\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    .line 22
    const v1, 0x7f0e0163

    const-string v2, "field \'iconSide\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 24
    const v1, 0x7f0e00d2

    const-string v2, "field \'choiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 26
    return-void
.end method

.method public static reset(Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;

    .prologue
    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->title:Landroid/widget/TextView;

    .line 30
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->subTitle:Landroid/widget/TextView;

    .line 31
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconColorView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 32
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsCount:Landroid/widget/TextView;

    .line 33
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->resultsLabel:Landroid/widget/TextView;

    .line 34
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->statusBadge:Landroid/widget/ImageView;

    .line 35
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->iconSide:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 36
    iput-object v0, p0, Lcom/navdy/hud/app/framework/places/NearbyPlaceSearchNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 37
    return-void
.end method
