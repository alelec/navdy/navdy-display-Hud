.class final Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;
.super Ljava/lang/Object;
.source "ConnectionNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/toast/IToastCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showDisconnectedToast(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field toastView:Lcom/navdy/hud/app/view/ToastView;

.field final synthetic val$loadImagePath:Ljava/io/File;

.field final synthetic val$loadImageRequired:Z

.field final synthetic val$prevProfile:Lcom/navdy/hud/app/profile/DriverProfile;


# direct methods
.method constructor <init>(ZLjava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 0

    .prologue
    .line 242
    iput-boolean p1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->val$loadImageRequired:Z

    iput-object p2, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->val$loadImagePath:Ljava/io/File;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->val$prevProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeChoiceItem(II)V
    .locals 5
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 271
    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    if-nez v1, :cond_0

    .line 298
    :goto_0
    return-void

    .line 274
    :cond_0
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 276
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ToastView;->dismissToast()V

    .line 277
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 278
    .local v0, "args":Landroid/os/Bundle;
    sget-object v1, Lcom/navdy/hud/app/screen/WelcomeScreen;->ARG_ACTION:Ljava/lang/String;

    sget-object v2, Lcom/navdy/hud/app/screen/WelcomeScreen;->ACTION_RECONNECT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    # getter for: Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->access$100()Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_WELCOME:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 284
    .end local v0    # "args":Landroid/os/Bundle;
    :pswitch_1
    # getter for: Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->access$100()Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/event/Disconnect;

    invoke-direct {v2}, Lcom/navdy/hud/app/event/Disconnect;-><init>()V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 285
    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ToastView;->dismissToast()V

    goto :goto_0

    .line 289
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ToastView;->dismissToast()V

    goto :goto_0

    .line 293
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/view/ToastView;->dismissToast()V

    .line 294
    # getter for: Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->access$100()Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/service/ConnectionHandler$ApplicationLaunchAttempted;

    invoke-direct {v2}, Lcom/navdy/hud/app/service/ConnectionHandler$ApplicationLaunchAttempted;-><init>()V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 295
    # getter for: Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->access$100()Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/input/LaunchAppEvent;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/input/LaunchAppEvent;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method

.method public onStart(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 4
    .param p1, "view"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 249
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getMainLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 250
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const-string v1, "disconnection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 252
    iget-boolean v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->val$loadImageRequired:Z

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const-string v1, "disconnection#toast"

    iget-object v2, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->val$loadImagePath:Ljava/io/File;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->val$prevProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    # invokes: Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->loadDriveImagefromProfile(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V
    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->access$000(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 255
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 259
    iput-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->toastView:Lcom/navdy/hud/app/view/ToastView;

    .line 260
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 261
    iput-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 262
    return-void
.end method
