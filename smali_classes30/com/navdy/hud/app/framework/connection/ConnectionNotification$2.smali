.class final Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;
.super Ljava/lang/Object;
.source "ConnectionNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/toast/IToastCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->showConnectedToast()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field final synthetic val$imagePath:Ljava/io/File;

.field final synthetic val$profile:Lcom/navdy/hud/app/profile/DriverProfile;


# direct methods
.method constructor <init>(Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->val$imagePath:Ljava/io/File;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->val$profile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeChoiceItem(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 373
    return-void
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 369
    const/4 v0, 0x0

    return v0
.end method

.method public onStart(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 4
    .param p1, "view"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    .line 356
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getMainLayout()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 357
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const-string v1, "connection#toast"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 358
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const-string v1, "connection#toast"

    iget-object v2, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->val$imagePath:Ljava/io/File;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->val$profile:Lcom/navdy/hud/app/profile/DriverProfile;

    # invokes: Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->loadDriveImagefromProfile(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V
    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->access$000(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 359
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 363
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setTag(Ljava/lang/Object;)V

    .line 364
    iput-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;->driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 365
    return-void
.end method
