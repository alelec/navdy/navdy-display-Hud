.class Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3$1;
.super Ljava/lang/Object;
.source "ConnectionNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3$1;->this$0:Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 412
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3$1;->this$0:Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;->val$driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3$1;->this$0:Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;->val$tag:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3$1;->this$0:Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    iget-object v0, v0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;->val$driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 414
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getInstance()Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3$1;->this$0:Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;->val$imagePath:Ljava/io/File;

    .line 415
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 416
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 417
    # getter for: Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;
    invoke-static {}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->access$200()Lcom/squareup/picasso/Transformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3$1;->this$0:Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    iget-object v1, v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;->val$driverImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 418
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 420
    :cond_0
    return-void
.end method
