.class public Lcom/navdy/hud/app/framework/connection/ConnectionNotification;
.super Ljava/lang/Object;
.source "ConnectionNotification.java"


# static fields
.field private static final CONNECTED_TIMEOUT:I = 0x7d0

.field public static final CONNECT_ID:Ljava/lang/String; = "connection#toast"

.field public static final DISCONNECT_ID:Ljava/lang/String; = "disconnection#toast"

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final NOT_CONNECTED_TIMEOUT:I = 0x7530

.field private static final TAG_CONNECT:I = 0x0

.field private static final TAG_DISMISS:I = 0x1

.field private static final TAG_DONT_LAUNCH:I = 0x3

.field private static final TAG_LAUNCH_APP:I = 0x2

.field private static final appClosedChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final appClosedLaunchFailedChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final appNotConnectedWidth:I

.field private static final bus:Lcom/squareup/otto/Bus;

.field private static final connect:Ljava/lang/String;

.field private static final connectedTitle:Ljava/lang/String;

.field private static final contentWidth:I

.field private static final disconnectedChoices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final disconnectedTitle:Ljava/lang/String;

.field private static final dismiss:Ljava/lang/String;

.field private static handler:Landroid/os/Handler;

.field private static final iphoneAppClosedLaunchFailedMessage:Ljava/lang/String;

.field private static final iphoneAppClosedMessage1:Ljava/lang/String;

.field private static final iphoneAppClosedMessage2:Ljava/lang/String;

.field private static final iphoneAppClosedTitle:Ljava/lang/String;

.field private static final launch:Ljava/lang/String;

.field private static roundTransformation:Lcom/squareup/picasso/Transformation;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 55
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 72
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->handler:Landroid/os/Handler;

    .line 92
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appClosedChoices:Ljava/util/ArrayList;

    .line 93
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appClosedLaunchFailedChoices:Ljava/util/ArrayList;

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->disconnectedChoices:Ljava/util/ArrayList;

    .line 96
    new-instance v1, Lcom/makeramen/RoundedTransformationBuilder;

    invoke-direct {v1}, Lcom/makeramen/RoundedTransformationBuilder;-><init>()V

    invoke-virtual {v1, v5}, Lcom/makeramen/RoundedTransformationBuilder;->oval(Z)Lcom/makeramen/RoundedTransformationBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/makeramen/RoundedTransformationBuilder;->build()Lcom/squareup/picasso/Transformation;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    .line 101
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 102
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0901f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->connectedTitle:Ljava/lang/String;

    .line 104
    const v1, 0x7f0901fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->disconnectedTitle:Ljava/lang/String;

    .line 106
    const v1, 0x7f090018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->iphoneAppClosedTitle:Ljava/lang/String;

    .line 107
    const v1, 0x7f09001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->iphoneAppClosedMessage1:Ljava/lang/String;

    .line 108
    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->iphoneAppClosedMessage2:Ljava/lang/String;

    .line 109
    const v1, 0x7f09001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->iphoneAppClosedLaunchFailedMessage:Ljava/lang/String;

    .line 111
    const v1, 0x7f0900a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->connect:Ljava/lang/String;

    .line 112
    const v1, 0x7f0900dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->dismiss:Ljava/lang/String;

    .line 113
    const v1, 0x7f090199

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->launch:Ljava/lang/String;

    .line 115
    sget-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->disconnectedChoices:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->connect:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    sget-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->disconnectedChoices:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->dismiss:Ljava/lang/String;

    invoke-direct {v2, v3, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    sget-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appClosedChoices:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->launch:Ljava/lang/String;

    invoke-direct {v2, v3, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    sget-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appClosedChoices:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->dismiss:Ljava/lang/String;

    invoke-direct {v2, v3, v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    sget-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appClosedLaunchFailedChoices:Ljava/util/ArrayList;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;

    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->dismiss:Ljava/lang/String;

    invoke-direct {v2, v3, v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout$Choice;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    const v1, 0x7f0b0191

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->contentWidth:I

    .line 124
    const v1, 0x7f0b018d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appNotConnectedWidth:I

    .line 126
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->bus:Lcom/squareup/otto/Bus;

    .line 127
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/io/File;
    .param p3, "x3"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 54
    invoke-static {p0, p1, p2, p3}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->loadDriveImagefromProfile(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V

    return-void
.end method

.method static synthetic access$100()Lcom/squareup/otto/Bus;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$200()Lcom/squareup/picasso/Transformation;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->roundTransformation:Lcom/squareup/picasso/Transformation;

    return-object v0
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private static getName(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/device/NavdyDeviceId;)Ljava/lang/String;
    .locals 2
    .param p0, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "n":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 386
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 388
    :cond_0
    if-nez v0, :cond_1

    .line 389
    const-string v0, ""

    .line 391
    :cond_1
    return-object v0
.end method

.method private static loadDriveImagefromProfile(Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 4
    .param p0, "driverImage"    # Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "imagePath"    # Ljava/io/File;
    .param p3, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 398
    invoke-static {p2}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 400
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 401
    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->DEFAULT:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setInitials(Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 402
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 432
    :goto_0
    return-void

    .line 404
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;

    invoke-direct {v2, p2, p0, p1, p3}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$3;-><init>(Ljava/io/File;Lcom/navdy/hud/app/ui/component/image/InitialsImageView;Ljava/lang/String;Lcom/navdy/hud/app/profile/DriverProfile;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static showConnectedToast()V
    .locals 16

    .prologue
    const v5, 0x7f0c0009

    const/4 v4, 0x1

    .line 308
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v13

    .line 309
    .local v13, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v6

    .line 311
    .local v6, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    if-nez v6, :cond_0

    .line 381
    :goto_0
    return-void

    .line 314
    :cond_0
    invoke-virtual {v13}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverImageFile()Ljava/io/File;

    move-result-object v10

    .line 315
    .local v10, "imagePath":Ljava/io/File;
    invoke-static {v10}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->isImageAvailableInCache(Ljava/io/File;)Z

    move-result v9

    .line 316
    .local v9, "imageInCache":Z
    invoke-static {v13, v6}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->getName(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/device/NavdyDeviceId;)Ljava/lang/String;

    move-result-object v12

    .line 317
    .local v12, "name":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    .line 319
    .local v7, "deviceName":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 320
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0x7d0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 321
    if-eqz v9, :cond_2

    .line 322
    const-string v0, "9"

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :goto_1
    const-string v0, "11"

    const v1, 0x7f0200cf

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 331
    const-string v0, "1"

    sget-object v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->connectedTitle:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v0, "1_1"

    const v1, 0x7f0c0010

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 334
    invoke-virtual {v13}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v8

    .line 335
    .local v8, "driverName":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 336
    const-string v0, "4"

    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v0, "5"

    invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 339
    const-string v0, "6"

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string v0, "7"

    const v1, 0x7f0c0011

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 346
    :goto_2
    const-string v0, "16"

    sget v1, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->contentWidth:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 348
    const/4 v3, 0x0

    .line 350
    .local v3, "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    if-nez v9, :cond_1

    .line 351
    new-instance v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;

    .end local v3    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    invoke-direct {v3, v10, v13}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$2;-><init>(Ljava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 377
    .restart local v3    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v15

    .line 378
    .local v15, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    const-string v0, "disconnection#toast"

    invoke-virtual {v15, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 379
    const-string v0, "disconnection#toast"

    invoke-virtual {v15, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 380
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "connection#toast"

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v15, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    goto/16 :goto_0

    .line 324
    .end local v3    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .end local v8    # "driverName":Ljava/lang/String;
    .end local v15    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v0

    invoke-virtual {v6}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getDriverImageResId(Ljava/lang/String;)I

    move-result v14

    .line 325
    .local v14, "resId":I
    invoke-static {v12}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 326
    .local v11, "initials":Ljava/lang/String;
    const-string v0, "8"

    invoke-virtual {v2, v0, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 327
    const-string v0, "10"

    invoke-virtual {v2, v0, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 342
    .end local v11    # "initials":Ljava/lang/String;
    .end local v14    # "resId":I
    .restart local v8    # "driverName":Ljava/lang/String;
    :cond_3
    const-string v0, "4"

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const-string v0, "5"

    invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2
.end method

.method public static showDisconnectedToast(Z)V
    .locals 30
    .param p0, "showAppLaunch"    # Z

    .prologue
    .line 130
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v26

    .line 131
    .local v26, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual/range {v26 .. v26}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v9

    .line 132
    .local v9, "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    invoke-virtual/range {v26 .. v26}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v12

    .line 133
    .local v12, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    const/4 v11, 0x0

    .line 135
    .local v11, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 137
    .local v5, "bundle":Landroid/os/Bundle;
    const/16 v19, 0x0

    .line 138
    .local v19, "load":Z
    const/16 v23, 0x0

    .line 140
    .local v23, "path":Ljava/io/File;
    const-string v3, "13"

    const/16 v4, 0x7530

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 141
    const-string v3, "11"

    const v4, 0x7f020212

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 142
    const-string v3, "1_1"

    const v4, 0x7f0c0010

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    const-string v3, "19"

    const/4 v4, 0x1

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 145
    const/16 v28, 0x0

    .line 147
    .local v28, "setDisconnectedData":Z
    if-nez p0, :cond_0

    if-eqz v12, :cond_3

    invoke-virtual {v9}, Lcom/navdy/hud/app/service/ConnectionHandler;->isAppClosed()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 149
    :cond_0
    const-string v3, "1"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->iphoneAppClosedTitle:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v3, "8"

    const v4, 0x7f020194

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    const-string v3, "16"

    sget v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appNotConnectedWidth:I

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 152
    invoke-virtual {v9}, Lcom/navdy/hud/app/service/ConnectionHandler;->isAppLaunchAttempted()Z

    move-result v3

    if-nez v3, :cond_2

    .line 153
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "App launch is not attempted yet so showing the app launch prompt"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 154
    const-string v3, "20"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appClosedChoices:Ljava/util/ArrayList;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 155
    const-string v3, "6"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->iphoneAppClosedMessage1:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v3, "7"

    const v4, 0x7f0c0013

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    :goto_0
    const/16 v25, 0x0

    .line 195
    .local v25, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-eqz v11, :cond_1

    .line 196
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v3

    invoke-virtual {v3, v11}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v25

    .line 197
    invoke-virtual {v11}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v13

    .line 198
    .local v13, "deviceName":Ljava/lang/String;
    const/4 v14, 0x0

    .line 200
    .local v14, "driverName":Ljava/lang/String;
    if-nez v25, :cond_6

    .line 202
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "driver profile not found:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 203
    const-string v3, "8"

    const v4, 0x7f02021e

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 224
    :goto_1
    if-eqz v28, :cond_1

    .line 225
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 226
    const-string v3, "4"

    invoke-virtual {v5, v3, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v3, "5"

    const v4, 0x7f0c0009

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 229
    const-string v3, "6"

    invoke-virtual {v5, v3, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v3, "7"

    const v4, 0x7f0c0011

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 238
    .end local v13    # "deviceName":Ljava/lang/String;
    .end local v14    # "driverName":Ljava/lang/String;
    :cond_1
    :goto_2
    move/from16 v21, v19

    .line 239
    .local v21, "loadImageRequired":Z
    move-object/from16 v20, v23

    .line 240
    .local v20, "loadImagePath":Ljava/io/File;
    move-object/from16 v24, v25

    .line 242
    .local v24, "prevProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    new-instance v6, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;

    move/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v24

    invoke-direct {v6, v0, v1, v2}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification$1;-><init>(ZLjava/io/File;Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 301
    .local v6, "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v29

    .line 302
    .local v29, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 303
    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 304
    new-instance v3, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v4, "disconnection#toast"

    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 305
    return-void

    .line 158
    .end local v6    # "callback":Lcom/navdy/hud/app/framework/toast/IToastCallback;
    .end local v20    # "loadImagePath":Ljava/io/File;
    .end local v21    # "loadImageRequired":Z
    .end local v24    # "prevProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v25    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v29    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "App launch has already been attempted but app is not launched yet"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 159
    const-string v3, "20"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->appClosedLaunchFailedChoices:Ljava/util/ArrayList;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 160
    const-string v3, "6"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->iphoneAppClosedLaunchFailedMessage:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v3, "7"

    const v4, 0x7f0c0013

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 166
    :cond_3
    invoke-virtual {v9}, Lcom/navdy/hud/app/service/ConnectionHandler;->getLastConnectedDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v18

    .line 167
    .local v18, "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-nez v18, :cond_5

    .line 169
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "last connected device not found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getLastPairedDevice()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v10

    .line 171
    .local v10, "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-nez v10, :cond_4

    .line 173
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "no last paired device found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 174
    const-string v3, "8"

    const v4, 0x7f02021e

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    .end local v10    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :goto_3
    const-string v3, "1"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->disconnectedTitle:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v3, "4"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->disconnectedTitle:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v3, "20"

    sget-object v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->disconnectedChoices:Ljava/util/ArrayList;

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 189
    const-string v3, "16"

    sget v4, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->contentWidth:I

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 191
    const/16 v28, 0x1

    goto/16 :goto_0

    .line 176
    .restart local v10    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_4
    invoke-virtual {v10}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v11

    .line 177
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "last paired device found:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_3

    .line 180
    .end local v10    # "connectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_5
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "last connected device found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 181
    new-instance v11, Lcom/navdy/service/library/device/NavdyDeviceId;

    .end local v11    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-direct {v11, v3}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    .restart local v11    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    goto :goto_3

    .line 206
    .end local v18    # "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .restart local v13    # "deviceName":Ljava/lang/String;
    .restart local v14    # "driverName":Ljava/lang/String;
    .restart local v25    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    :cond_6
    invoke-virtual/range {v25 .. v25}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v14

    .line 207
    sget-object v3, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "driver profile found:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 208
    invoke-virtual/range {v25 .. v25}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverImageFile()Ljava/io/File;

    move-result-object v16

    .line 209
    .local v16, "imagePath":Ljava/io/File;
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->isImageAvailableInCache(Ljava/io/File;)Z

    move-result v15

    .line 210
    .local v15, "imageInCache":Z
    move-object/from16 v0, v25

    invoke-static {v0, v11}, Lcom/navdy/hud/app/framework/connection/ConnectionNotification;->getName(Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/device/NavdyDeviceId;)Ljava/lang/String;

    move-result-object v22

    .line 212
    .local v22, "name":Ljava/lang/String;
    if-eqz v15, :cond_7

    .line 213
    const-string v3, "9"

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 215
    :cond_7
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getInstance()Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/navdy/hud/app/framework/contacts/ContactImageHelper;->getDriverImageResId(Ljava/lang/String;)I

    move-result v27

    .line 216
    .local v27, "resId":I
    invoke-static/range {v22 .. v22}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 217
    .local v17, "initials":Ljava/lang/String;
    const-string v3, "8"

    move/from16 v0, v27

    invoke-virtual {v5, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 218
    const-string v3, "10"

    move-object/from16 v0, v17

    invoke-virtual {v5, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const/16 v19, 0x1

    .line 220
    move-object/from16 v23, v16

    goto/16 :goto_1

    .line 232
    .end local v15    # "imageInCache":Z
    .end local v16    # "imagePath":Ljava/io/File;
    .end local v17    # "initials":Ljava/lang/String;
    .end local v22    # "name":Ljava/lang/String;
    .end local v27    # "resId":I
    :cond_8
    const-string v3, "4"

    invoke-virtual {v5, v3, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v3, "5"

    const v4, 0x7f0c0009

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_2
.end method
