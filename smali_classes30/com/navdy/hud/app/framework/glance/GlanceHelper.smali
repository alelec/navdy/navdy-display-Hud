.class public Lcom/navdy/hud/app/framework/glance/GlanceHelper;
.super Ljava/lang/Object;
.source "GlanceHelper.java"


# static fields
.field private static final APP_ID_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            ">;"
        }
    .end annotation
.end field

.field public static final DELETE_ALL_GLANCES_TOAST_ID:Ljava/lang/String; = "glance-deleted"

.field public static final FUEL_PACKAGE:Ljava/lang/String; = "com.navdy.fuel"

.field private static final ID:Ljava/util/concurrent/atomic/AtomicLong;

.field public static final IOS_PHONE_PACKAGE:Ljava/lang/String; = "com.apple.mobilephone"

.field public static final MICROSOFT_OUTLOOK:Ljava/lang/String; = "com.microsoft.Office.Outlook"

.field public static final MUSIC_PACKAGE:Ljava/lang/String; = "com.navdy.music"

.field public static final PHONE_PACKAGE:Ljava/lang/String; = "com.navdy.phone"

.field public static final SMS_PACKAGE:Ljava/lang/String; = "com.navdy.sms"

.field public static final TRAFFIC_PACKAGE:Ljava/lang/String; = "com.navdy.traffic"

.field public static final VOICE_SEARCH_PACKAGE:Ljava/lang/String; = "com.navdy.voice.search"

.field private static final builder:Ljava/lang/StringBuilder;

.field private static final gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/framework/glance/GlanceHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 45
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->ID:Ljava/util/concurrent/atomic/AtomicLong;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    .line 70
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.navdy.fuel"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FUEL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.google.android.calendar"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.google.android.gm"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.google.android.talk"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_HANGOUT:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.Slack"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SLACK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.whatsapp"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->WHATS_APP:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.facebook.orca"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK_MESSENGER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.facebook.katana"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.twitter.android"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->TWITTER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.navdy.sms"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SMS:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.google.android.apps.inbox"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_INBOX:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.google.calendar"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.google.Gmail"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.google.hangouts"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_HANGOUT:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.tinyspeck.chatlyio"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SLACK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "net.whatsapp.WhatsApp"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->WHATS_APP:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.facebook.Messenger"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK_MESSENGER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.facebook.Facebook"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.atebits.Tweetie2"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->TWITTER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.apple.MobileSMS"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->IMESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.apple.mobilemail"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.apple.mobilecal"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    const-string v1, "com.microsoft.Office.Outlook"

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    .line 99
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getGestureServiceConnector()Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .line 100
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildDataMap(Lcom/navdy/service/library/events/glances/GlanceEvent;)Ljava/util/Map;
    .locals 5
    .param p0, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/glances/GlanceEvent;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 140
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    if-nez v2, :cond_1

    .line 148
    :cond_0
    return-object v1

    .line 144
    :cond_1
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/KeyValue;

    .line 145
    .local v0, "keyValue":Lcom/navdy/service/library/events/glances/KeyValue;
    iget-object v3, v0, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->sanitizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static getCalendarTime(JLjava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;
    .locals 8
    .param p0, "time"    # J
    .param p2, "timeUnit"    # Ljava/lang/StringBuilder;
    .param p3, "ampmMarker"    # Ljava/lang/StringBuilder;
    .param p4, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;

    .prologue
    const/4 v6, 0x0

    .line 736
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, p0, v4

    long-to-float v0, v4

    .line 737
    .local v0, "diff":F
    const v4, 0x476a6000    # 60000.0f

    div-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-long v2, v4

    .line 740
    .local v2, "minutes":J
    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 741
    invoke-virtual {p3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 746
    const-wide/16 v4, 0x3c

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    const-wide/16 v4, -0x5

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    .line 747
    :cond_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p0, p1}, Ljava/util/Date;-><init>(J)V

    const/4 v5, 0x1

    invoke-virtual {p4, v4, p3, v5}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime12Hour(Ljava/util/Date;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v1

    .line 755
    .local v1, "str":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 748
    .end local v1    # "str":Ljava/lang/String;
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_2

    .line 749
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->nowStr:Ljava/lang/String;

    .restart local v1    # "str":Ljava/lang/String;
    goto :goto_0

    .line 751
    .end local v1    # "str":Ljava/lang/String;
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 752
    .restart local v1    # "str":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->minute:Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getFrom(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .param p0, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 362
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 364
    .local v0, "from":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v1, v0

    .line 376
    .end local v0    # "from":Ljava/lang/String;
    .local v1, "from":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 368
    .end local v1    # "from":Ljava/lang/String;
    .restart local v0    # "from":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    :goto_1
    move-object v1, v0

    .line 376
    .end local v0    # "from":Ljava/lang/String;
    .restart local v1    # "from":Ljava/lang/String;
    goto :goto_0

    .line 372
    .end local v1    # "from":Ljava/lang/String;
    .restart local v0    # "from":Ljava/lang/String;
    :sswitch_0
    sget-object v2, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "from":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "from":Ljava/lang/String;
    goto :goto_1

    .line 368
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x9 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getGestureService()Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .locals 1

    .prologue
    .line 800
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->gestureServiceConnector:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    return-object v0
.end method

.method public static declared-synchronized getGlanceMessage(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .param p0, "glanceApp"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 394
    .local p1, "dataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-class v3, Lcom/navdy/hud/app/framework/glance/GlanceHelper;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 436
    :cond_0
    :goto_0
    :pswitch_0
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 437
    .local v1, "message":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    monitor-exit v3

    return-object v1

    .line 399
    .end local v1    # "message":Ljava/lang/String;
    :pswitch_1
    :try_start_1
    sget-object v2, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 400
    .local v0, "body":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 401
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 394
    .end local v0    # "body":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 412
    :pswitch_2
    :try_start_2
    sget-object v2, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 413
    .restart local v0    # "body":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 414
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 421
    .end local v0    # "body":Ljava/lang/String;
    :pswitch_3
    sget-object v2, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422
    .restart local v0    # "body":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 423
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 429
    .end local v0    # "body":Ljava/lang/String;
    :pswitch_4
    sget-object v2, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 430
    .restart local v0    # "body":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 431
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 394
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getGlancesApp(Lcom/navdy/service/library/events/glances/GlanceEvent;)Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .locals 4
    .param p0, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    const/4 v1, 0x0

    .line 111
    if-nez p0, :cond_1

    move-object v0, v1

    .line 134
    :cond_0
    :goto_0
    return-object v0

    .line 114
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 115
    .local v0, "app":Lcom/navdy/hud/app/framework/glance/GlanceApp;
    if-nez v0, :cond_0

    .line 117
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType:[I

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    .line 131
    goto :goto_0

    .line 119
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    goto :goto_0

    .line 122
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    goto :goto_0

    .line 125
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    goto :goto_0

    .line 128
    :pswitch_3
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_SOCIAL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    goto :goto_0

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getGlancesApp(Ljava/lang/String;)Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;

    .prologue
    .line 107
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->APP_ID_MAP:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/glance/GlanceApp;

    return-object v0
.end method

.method public static getIcon(Ljava/lang/String;)I
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v0, -0x1

    .line 804
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 816
    :goto_0
    return v0

    .line 808
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    move v1, v0

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 810
    :pswitch_0
    const v0, 0x7f02021d

    goto :goto_0

    .line 808
    :sswitch_0
    const-string v1, "GLANCE_ICON_NAVDY_MAIN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "GLANCE_ICON_MESSAGE_SIDE_BLUE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    .line 813
    :pswitch_1
    const v0, 0x7f02016d

    goto :goto_0

    .line 808
    :sswitch_data_0
    .sparse-switch
        -0x1d42571 -> :sswitch_1
        -0x908245 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->ID:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLargeViewType(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .locals 2
    .param p0, "glanceApp"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .prologue
    .line 609
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 634
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_GLANCE_MESSAGE_SINGLE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    :goto_0
    return-object v0

    .line 613
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    goto :goto_0

    .line 616
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    goto :goto_0

    .line 609
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getNotificationId(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "glanceType"    # Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 157
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType:[I

    invoke-virtual {p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 179
    const-string v0, "glance_gen_"

    .line 183
    .local v0, "prefix":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 159
    .end local v0    # "prefix":Ljava/lang/String;
    :pswitch_0
    const-string v0, "glance_cal_"

    .line 160
    .restart local v0    # "prefix":Ljava/lang/String;
    goto :goto_0

    .line 163
    .end local v0    # "prefix":Ljava/lang/String;
    :pswitch_1
    const-string v0, "glance_email_"

    .line 164
    .restart local v0    # "prefix":Ljava/lang/String;
    goto :goto_0

    .line 167
    .end local v0    # "prefix":Ljava/lang/String;
    :pswitch_2
    const-string v0, "glance_msg_"

    .line 168
    .restart local v0    # "prefix":Ljava/lang/String;
    goto :goto_0

    .line 171
    .end local v0    # "prefix":Ljava/lang/String;
    :pswitch_3
    const-string v0, "glance_soc_"

    .line 172
    .restart local v0    # "prefix":Ljava/lang/String;
    goto :goto_0

    .line 175
    .end local v0    # "prefix":Ljava/lang/String;
    :pswitch_4
    const-string v0, "glance_fuel_"

    .line 176
    .restart local v0    # "prefix":Ljava/lang/String;
    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getNotificationId(Lcom/navdy/service/library/events/glances/GlanceEvent;)Ljava/lang/String;
    .locals 2
    .param p0, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    iget-object v1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getNotificationId(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNumber(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .param p0, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 346
    .local v0, "number":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v1, v0

    .line 358
    .end local v0    # "number":Ljava/lang/String;
    .local v1, "number":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 350
    .end local v1    # "number":Ljava/lang/String;
    .restart local v0    # "number":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    :goto_1
    move-object v1, v0

    .line 358
    .end local v0    # "number":Ljava/lang/String;
    .restart local v1    # "number":Ljava/lang/String;
    goto :goto_0

    .line 354
    .end local v1    # "number":Ljava/lang/String;
    .restart local v0    # "number":Ljava/lang/String;
    :sswitch_0
    sget-object v2, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "number":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "number":Ljava/lang/String;
    goto :goto_1

    .line 350
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x9 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getSmallViewType(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .locals 2
    .param p0, "glanceApp"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .prologue
    .line 581
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 604
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    :goto_0
    return-object v0

    .line 585
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    goto :goto_0

    .line 581
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getSubTitle(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;
    .locals 6
    .param p0, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, ""

    .line 274
    .local v0, "subTitle":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v1, v0

    .line 340
    .end local v0    # "subTitle":Ljava/lang/String;
    .local v1, "subTitle":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 278
    .end local v1    # "subTitle":Ljava/lang/String;
    .restart local v0    # "subTitle":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 336
    :cond_1
    :goto_1
    :pswitch_0
    if-nez v0, :cond_2

    .line 337
    const-string v0, ""

    :cond_2
    move-object v1, v0

    .line 340
    .end local v0    # "subTitle":Ljava/lang/String;
    .restart local v1    # "subTitle":Ljava/lang/String;
    goto :goto_0

    .line 282
    .end local v1    # "subTitle":Ljava/lang/String;
    .restart local v0    # "subTitle":Ljava/lang/String;
    :pswitch_1
    sget-object v2, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "subTitle":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 283
    .restart local v0    # "subTitle":Ljava/lang/String;
    goto :goto_1

    .line 289
    :pswitch_2
    sget-object v2, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "subTitle":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 290
    .restart local v0    # "subTitle":Ljava/lang/String;
    goto :goto_1

    .line 296
    :pswitch_3
    sget-object v2, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_DOMAIN:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "subTitle":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 297
    .restart local v0    # "subTitle":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 298
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09026b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 326
    :pswitch_4
    sget-object v2, Lcom/navdy/service/library/events/glances/FuelConstants;->NO_ROUTE:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 327
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelNotificationSubtitle:Ljava/lang/String;

    goto :goto_1

    .line 332
    :pswitch_5
    sget-object v2, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "subTitle":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "subTitle":Ljava/lang/String;
    goto :goto_1

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getTimeStr(JLjava/util/Date;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;
    .locals 6
    .param p0, "now"    # J
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;

    .prologue
    .line 664
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v0, p0, v4

    .line 666
    .local v0, "seconds":J
    const-wide/32 v4, 0xea60

    cmp-long v3, v0, v4

    if-gtz v3, :cond_0

    .line 667
    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->nowStr:Ljava/lang/String;

    .line 671
    .local v2, "str":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 669
    .end local v2    # "str":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p3, p2, v3}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "str":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getTitle(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;)Ljava/lang/String;
    .locals 7
    .param p0, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, ""

    .line 189
    .local v1, "title":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v2, v1

    .line 268
    .end local v1    # "title":Ljava/lang/String;
    .local v2, "title":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 193
    .end local v2    # "title":Ljava/lang/String;
    .restart local v1    # "title":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 260
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->notification:Ljava/lang/String;

    .line 264
    :cond_1
    :goto_1
    if-nez v1, :cond_2

    .line 265
    const-string v1, ""

    :cond_2
    move-object v2, v1

    .line 268
    .end local v1    # "title":Ljava/lang/String;
    .restart local v2    # "title":Ljava/lang/String;
    goto :goto_0

    .line 195
    .end local v2    # "title":Ljava/lang/String;
    .restart local v1    # "title":Ljava/lang/String;
    :pswitch_0
    sget-object v3, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 196
    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_1

    .line 202
    :pswitch_1
    sget-object v3, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 203
    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_1

    .line 206
    :pswitch_2
    sget-object v3, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_FROM:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 207
    .restart local v1    # "title":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 208
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->facebook:Ljava/lang/String;

    goto :goto_1

    .line 214
    :pswitch_3
    sget-object v3, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_FROM:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 215
    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_1

    .line 218
    :pswitch_4
    sget-object v3, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 219
    .restart local v1    # "title":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 220
    sget-object v3, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_1

    .line 227
    :pswitch_5
    sget-object v3, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 228
    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_1

    .line 234
    :pswitch_6
    sget-object v3, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 235
    .restart local v1    # "title":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 236
    sget-object v3, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "title":Ljava/lang/String;
    goto/16 :goto_1

    .line 242
    :pswitch_7
    sget-object v3, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 243
    .restart local v1    # "title":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 244
    sget-object v3, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "title":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "title":Ljava/lang/String;
    goto/16 :goto_1

    .line 249
    :pswitch_8
    sget-object v3, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    .local v0, "fuel":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 251
    const-string v3, "%s %s%%"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->fuelNotificationTitle:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    .line 252
    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    .line 251
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 254
    :cond_3
    sget-object v3, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v4, 0x7f090109

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 256
    goto/16 :goto_1

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static declared-synchronized getTtsMessage(Lcom/navdy/hud/app/framework/glance/GlanceApp;Ljava/util/Map;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;
    .locals 27
    .param p0, "glanceApp"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;
    .param p2, "stringBuilder1"    # Ljava/lang/StringBuilder;
    .param p3, "stringBuilder2"    # Ljava/lang/StringBuilder;
    .param p4, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/framework/glance/GlanceApp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/StringBuilder;",
            "Lcom/navdy/hud/app/common/TimeHelper;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 449
    .local p1, "dataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-class v21, Lcom/navdy/hud/app/framework/glance/GlanceHelper;

    monitor-enter v21

    :try_start_0
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v22

    aget v20, v20, v22

    packed-switch v20, :pswitch_data_0

    .line 574
    :cond_0
    :goto_0
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 575
    .local v17, "tts":Ljava/lang/String;
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 577
    monitor-exit v21

    return-object v17

    .line 453
    .end local v17    # "tts":Ljava/lang/String;
    :pswitch_0
    :try_start_1
    sget-object v20, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_LOCATION:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 454
    .local v7, "location":Ljava/lang/String;
    sget-object v20, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 456
    .local v16, "title":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 457
    .local v8, "millis":J
    sget-object v20, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 458
    .local v11, "str":Ljava/lang/String;
    if-eqz v11, :cond_1

    .line 460
    :try_start_2
    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v8

    .line 465
    :cond_1
    :goto_1
    const/4 v15, 0x0

    .line 467
    .local v15, "time":Ljava/lang/String;
    const-wide/16 v22, 0x0

    cmp-long v20, v8, v22

    if-lez v20, :cond_2

    .line 468
    :try_start_3
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v8, v9, v0, v1, v2}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getCalendarTime(JLjava/lang/StringBuilder;Ljava/lang/StringBuilder;Lcom/navdy/hud/app/common/TimeHelper;)Ljava/lang/String;

    move-result-object v6

    .line 469
    .local v6, "data":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 471
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 473
    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 474
    .local v10, "pmMarker":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_6

    .line 476
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->pmMarker:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-static {v10, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_5

    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->pm:Ljava/lang/String;

    .line 477
    .local v4, "ampmMarker":Ljava/lang/String;
    :goto_2
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v22, 0x7f0902ad

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v6, v23, v24

    const/16 v24, 0x1

    aput-object v4, v23, v24

    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 497
    .end local v4    # "ampmMarker":Ljava/lang/String;
    .end local v6    # "data":Ljava/lang/String;
    .end local v10    # "pmMarker":Ljava/lang/String;
    :cond_2
    :goto_3
    if-eqz v16, :cond_3

    .line 498
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    sget-object v22, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v23, 0x7f0902b0

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v16, v24, v25

    invoke-virtual/range {v22 .. v24}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    :cond_3
    if-eqz v15, :cond_4

    .line 502
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    const-string v22, " "

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    :cond_4
    if-eqz v7, :cond_0

    .line 507
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    const-string v22, " "

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    sget-object v22, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v23, 0x7f0902ac

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v7, v24, v25

    invoke-virtual/range {v22 .. v24}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 449
    .end local v7    # "location":Ljava/lang/String;
    .end local v8    # "millis":J
    .end local v11    # "str":Ljava/lang/String;
    .end local v15    # "time":Ljava/lang/String;
    .end local v16    # "title":Ljava/lang/String;
    :catchall_0
    move-exception v20

    monitor-exit v21

    throw v20

    .line 461
    .restart local v7    # "location":Ljava/lang/String;
    .restart local v8    # "millis":J
    .restart local v11    # "str":Ljava/lang/String;
    .restart local v16    # "title":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 462
    .local v13, "t":Ljava/lang/Throwable;
    :try_start_4
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 476
    .end local v13    # "t":Ljava/lang/Throwable;
    .restart local v6    # "data":Ljava/lang/String;
    .restart local v10    # "pmMarker":Ljava/lang/String;
    .restart local v15    # "time":Ljava/lang/String;
    :cond_5
    sget-object v4, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->am:Ljava/lang/String;

    goto/16 :goto_2

    .line 480
    :cond_6
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v22, "."

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v15

    goto/16 :goto_3

    .line 485
    .end local v10    # "pmMarker":Ljava/lang/String;
    :cond_7
    :try_start_5
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 486
    .local v18, "val":J
    const-wide/16 v22, 0x1

    cmp-long v20, v18, v22

    if-lez v20, :cond_8

    .line 487
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v22, 0x7f0902af

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v6, v23, v24

    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_3

    .line 489
    :cond_8
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v22, 0x7f0902ae

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v6, v23, v24

    move-object/from16 v0, v20

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v15

    goto/16 :goto_3

    .line 491
    .end local v18    # "val":J
    :catch_1
    move-exception v14

    .line 492
    .local v14, "th":Ljava/lang/Throwable;
    :try_start_6
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 516
    .end local v6    # "data":Ljava/lang/String;
    .end local v7    # "location":Ljava/lang/String;
    .end local v8    # "millis":J
    .end local v11    # "str":Ljava/lang/String;
    .end local v14    # "th":Ljava/lang/Throwable;
    .end local v15    # "time":Ljava/lang/String;
    .end local v16    # "title":Ljava/lang/String;
    :pswitch_1
    sget-object v20, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 517
    .local v12, "subject":Ljava/lang/String;
    if-eqz v12, :cond_9

    .line 518
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    :cond_9
    sget-object v20, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 521
    .local v5, "body":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 522
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    if-lez v20, :cond_a

    .line 523
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    const-string v22, "."

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    const-string v22, " "

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    :cond_a
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 537
    .end local v5    # "body":Ljava/lang/String;
    .end local v12    # "subject":Ljava/lang/String;
    :pswitch_2
    sget-object v20, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 538
    .restart local v5    # "body":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 539
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 546
    .end local v5    # "body":Ljava/lang/String;
    :pswitch_3
    sget-object v20, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 547
    .restart local v5    # "body":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 548
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 553
    .end local v5    # "body":Ljava/lang/String;
    :pswitch_4
    sget-object v20, Lcom/navdy/service/library/events/glances/FuelConstants;->FUEL_LEVEL:Lcom/navdy/service/library/events/glances/FuelConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 554
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    sget-object v22, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v23, 0x7f090320

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    :cond_b
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    sget-object v22, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v23, 0x7f090184

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    sget-object v26, Lcom/navdy/service/library/events/glances/FuelConstants;->GAS_STATION_NAME:Lcom/navdy/service/library/events/glances/FuelConstants;

    .line 559
    invoke-virtual/range {v26 .. v26}, Lcom/navdy/service/library/events/glances/FuelConstants;->name()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    aput-object v26, v24, v25

    .line 557
    invoke-virtual/range {v22 .. v24}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    const-string v22, "."

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 567
    :pswitch_5
    sget-object v20, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 568
    .restart local v5    # "body":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 569
    sget-object v20, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->builder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 449
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static initMessageAttributes()V
    .locals 11

    .prologue
    .line 760
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    .line 762
    .local v8, "context":Landroid/content/Context;
    new-instance v10, Landroid/widget/TextView;

    invoke-direct {v10, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 763
    .local v10, "textView":Landroid/widget/TextView;
    const v1, 0x7f0c0018

    invoke-virtual {v10, v8, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 764
    invoke-virtual {v10}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 765
    .local v2, "paint":Landroid/text/TextPaint;
    new-instance v0, Landroid/text/StaticLayout;

    const-string v1, "RockgOn"

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageWidthBound:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 767
    .local v0, "layout":Landroid/text/StaticLayout;
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    .line 768
    .local v9, "height":I
    sget v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageTitleMarginTop:I

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageTitleMarginBottom:I

    add-int/2addr v1, v3

    add-int/2addr v9, v1

    .line 769
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message-title-ht = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " layout="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 770
    invoke-static {v9}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->setMessageTitleHeight(I)V

    .line 771
    return-void
.end method

.method public static isCalendarApp(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z
    .locals 2
    .param p0, "glanceApp"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .prologue
    .line 721
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 728
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 725
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 721
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static isDeleteAllView(Landroid/view/View;)Z
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 675
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->DELETE_ALL_VIEW_TAG:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 676
    const/4 v0, 0x1

    .line 678
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFuelNotificationEnabled()Z
    .locals 1

    .prologue
    .line 699
    const-string v0, "com.navdy.fuel"

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isMusicNotificationEnabled()Z
    .locals 1

    .prologue
    .line 708
    const-string v0, "com.navdy.music"

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isPackageEnabled(Ljava/lang/String;)Z
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 712
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v0

    .line 713
    .local v0, "preferences":Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    iget-object v2, v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 714
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationSettings()Lcom/navdy/hud/app/profile/NotificationSettings;

    move-result-object v1

    .line 715
    .local v1, "settings":Lcom/navdy/hud/app/profile/NotificationSettings;
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, p0}, Lcom/navdy/hud/app/profile/NotificationSettings;->enabled(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 717
    .end local v1    # "settings":Lcom/navdy/hud/app/profile/NotificationSettings;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isPhoneNotificationEnabled()Z
    .locals 1

    .prologue
    .line 704
    const-string v0, "com.navdy.phone"

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.apple.mobilephone"

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPhotoCheckRequired(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z
    .locals 2
    .param p0, "app"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .prologue
    .line 381
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 388
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 385
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 381
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x9 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public static isTrafficNotificationEnabled()Z
    .locals 1

    .prologue
    .line 695
    const-string v0, "com.navdy.traffic"

    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static needsScrollLayout(Ljava/lang/String;)Z
    .locals 14
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 774
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796
    :goto_0
    return v7

    .line 778
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    .line 780
    .local v8, "context":Landroid/content/Context;
    new-instance v12, Landroid/widget/TextView;

    invoke-direct {v12, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 781
    .local v12, "textView":Landroid/widget/TextView;
    const v1, 0x7f0c0017

    invoke-virtual {v12, v8, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 782
    invoke-virtual {v12}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 783
    .local v2, "paint":Landroid/text/TextPaint;
    new-instance v0, Landroid/text/StaticLayout;

    sget v3, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageWidthBound:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 786
    .local v0, "layout":Landroid/text/StaticLayout;
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v9, v1

    .line 787
    .local v9, "height":F
    sget v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->messageHeightBound:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float v10, v1, v9

    .line 789
    .local v10, "left":F
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->getMessageTitleHeight()I

    move-result v13

    .line 790
    .local v13, "titleHt":I
    int-to-float v1, v13

    cmpl-float v1, v10, v1

    if-lez v1, :cond_1

    .line 791
    const/4 v11, 0x0

    .line 795
    .local v11, "needsScroll":Z
    :goto_1
    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message-tittle-calc left["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] titleHt ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move v7, v11

    .line 796
    goto :goto_0

    .line 793
    .end local v11    # "needsScroll":Z
    :cond_1
    const/4 v11, 0x1

    .restart local v11    # "needsScroll":Z
    goto :goto_1
.end method

.method public static showGlancesDeletedToast()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 683
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 684
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0x3e8

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 685
    const-string v0, "8"

    const v1, 0x7f0201cb

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 686
    const-string v0, "4"

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090115

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    const-string v0, "5"

    const v1, 0x7f0c0009

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 688
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v6

    .line 689
    .local v6, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    .line 690
    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 691
    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "glance-deleted"

    const/4 v3, 0x0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v6, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 692
    return-void
.end method

.method public static usesMessageLayout(Lcom/navdy/hud/app/framework/glance/GlanceApp;)Z
    .locals 2
    .param p0, "glanceApp"    # Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .prologue
    .line 640
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceHelper$1;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 659
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 656
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 640
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
