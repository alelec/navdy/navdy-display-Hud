.class Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;
.super Ljava/lang/Object;
.source "GlanceHandler.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/framework/glance/GlanceHandler;

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$name:Ljava/lang/String;

.field final synthetic val$number:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/framework/glance/GlanceHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    .prologue
    .line 340
    iput-object p1, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    iput-object p2, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->val$number:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->val$message:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->val$name:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public result(Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;)V
    .locals 4
    .param p1, "code"    # Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    .prologue
    .line 343
    sget-object v0, Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;->SUCCESS:Lcom/navdy/hud/app/framework/twilio/TwilioSmsManager$ErrorCode;

    if-eq p1, v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->this$0:Lcom/navdy/hud/app/framework/glance/GlanceHandler;

    iget-object v1, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->val$number:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->val$message:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/framework/glance/GlanceHandler$2;->val$name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/framework/glance/GlanceHandler;->sendSmsFailedNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_0
    return-void
.end method
